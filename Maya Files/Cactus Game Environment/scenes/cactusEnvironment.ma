//Maya ASCII 2017 scene
//Name: cactusEnvironment.ma
//Last modified: Sun, Sep 25, 2016 02:41:52 PM
//Codeset: 1252
requires maya "2017";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "F4752E32-436C-96B4-671C-E393C116702E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2796.5980505161388 903.09186809118955 422.49472759722357 ;
	setAttr ".r" -type "double3" -19.505266387077899 1354.1999999998595 -2.1713769633261972e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "625611E5-4767-EDE3-CA07-26A3A15D7474";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 2811.2621522264812;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -3.814697265625e-006 0 -5.7220458984375e-006 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "45C04804-413A-02A6-DD28-A2BF76C9FA7C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 160.42731579764143 1000.1000000000001 100.07098695520847 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9222DDF4-4036-06A6-41AB-47A831583AF8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2771.2855976277428;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "64AED913-4E77-0D80-3E46-86B24323D441";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "72AF731D-4580-8576-B29F-6E8017BE34D5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 614.51097727355318;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "38F8A8EE-4491-0F4B-DB42-A99848C9B355";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 74.984826891899687 14.467263942585122 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6574913D-482D-6B89-2F89-14903D406F72";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 598.23844921455623;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "HimanProptions";
	rename -uid "08C35CBE-4800-9045-EF78-A4920AEEEC5A";
createNode mesh -n "HimanProptionsShape" -p "HimanProptions";
	rename -uid "515BE987-4E41-80E8-4232-688C394CA799";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -15.239998 15.24 15.240002 
		15.240002 15.24 15.240002 -15.239998 167.64001 15.240002 15.240002 167.64001 15.240002 
		-15.239998 167.64001 -15.24 15.240002 167.64001 -15.24 -15.239998 15.24 -15.24 15.240002 
		15.24 -15.24;
	setAttr -s 8 ".vt[0:7]"  -15.23999977 -15.23999977 15.23999977 15.23999977 -15.23999977 15.23999977
		 -15.23999977 15.23999977 15.23999977 15.23999977 15.23999977 15.23999977 -15.23999977 15.23999977 -15.23999977
		 15.23999977 15.23999977 -15.23999977 -15.23999977 -15.23999977 -15.23999977 15.23999977 -15.23999977 -15.23999977;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout";
	rename -uid "EC5C2BF8-4542-1CB2-1FE4-F99FB6C6E5F9";
createNode transform -n "Brick10" -p "Rock_Floor_layout";
	rename -uid "E77D67C7-4327-4514-8671-E1BE5CD731F2";
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout|Brick10";
	rename -uid "32E7EA5E-4172-D3B3-D288-A8BE6D4F1843";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout";
	rename -uid "88B08CA0-4FCD-9688-EE4A-A89DD4B911F9";
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout|Brick14";
	rename -uid "2B0E77F9-4239-DCCB-827A-568720A8648D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout";
	rename -uid "CBEFF4DB-48B6-9A5F-0DA5-98BB1E7EAFAD";
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout|Brick7";
	rename -uid "37480052-44A6-AB4B-B36D-90B0B09CF48E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout";
	rename -uid "60C72114-4F99-BCF4-A826-ACA8C151B007";
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout|Brick3";
	rename -uid "8A891926-433F-E777-5D23-15B1034A84D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout";
	rename -uid "314C16B8-488C-1FC9-F480-5087F31879D7";
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout|Brick2";
	rename -uid "BDBB47F3-48F5-09BE-6561-979F8DDCE176";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout";
	rename -uid "172E7DC0-4780-4004-1CDB-4ABCF96C3233";
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout|Brick1";
	rename -uid "66134C84-4F98-5DDB-E138-B792A76CF114";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout";
	rename -uid "1914E477-4F70-D5F1-C22E-72B4945E648D";
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout|Brick20";
	rename -uid "AD6BD70E-43AD-C073-40CB-9397FBAF024F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout";
	rename -uid "95F6C667-4A6B-5AD4-5BE1-5285B63804F5";
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout|Brick11";
	rename -uid "A8D92491-47C8-DBC5-2138-91918A6002A6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout";
	rename -uid "D9C72B7E-45B3-4D55-D9FB-88B357DF913A";
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout|Brick5";
	rename -uid "09962D2C-4738-3CCE-568F-8EA73661E58E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout";
	rename -uid "0E8EDB23-4049-A73D-94AA-A1BB295A543A";
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout|Brick8";
	rename -uid "30134995-41CA-7C7F-9567-05A1CE856576";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout";
	rename -uid "A94F52FB-47A5-B58A-FAFC-BF8A2F5B517F";
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout|Brick6";
	rename -uid "6F0A9DF7-47FD-7FEC-3F25-99800565C1C5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout";
	rename -uid "32844D83-4944-C6A4-12BF-47B63D06CFA7";
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout|Brick17";
	rename -uid "144E94FB-4D6D-6B1B-3928-55AA7DDB4D71";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout";
	rename -uid "9290A86D-4C1B-E70D-873F-91B13D084CF0";
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout|Brick16";
	rename -uid "D80E1644-4652-8D3F-67DC-4EB2E970E3A4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout";
	rename -uid "578C856A-457E-2BD5-02EB-E8A416F21A8B";
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout|Brick18";
	rename -uid "2333060A-420D-86D2-D27B-9F9C4ACAFFFD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout";
	rename -uid "E6E7FA80-4567-4175-36CD-BAA813366943";
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout|Brick21";
	rename -uid "BAA1E2FD-4E71-E1C5-90DE-DCA7F8B1A2AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout";
	rename -uid "A06EEC9E-4017-056E-2C8A-44AFA21730E6";
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout|Brick22";
	rename -uid "7D0F6691-4C60-4C91-9D8F-E7A21235FBAF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout";
	rename -uid "9F2E5543-4ACA-C476-C3B4-4D81E14986B4";
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout|Brick23";
	rename -uid "8139CC90-468D-0ABA-4B25-FC89439CDD86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout";
	rename -uid "BF3B8912-4A1E-54A0-AE61-558B228D8419";
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout|Brick24";
	rename -uid "BBFB28F2-4C20-77DE-4EC0-D8BCF2CF0713";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout1";
	rename -uid "BFDA1226-42E1-176B-5329-EE8E234581B2";
createNode transform -n "Brick10" -p "Rock_Floor_layout1";
	rename -uid "890376CF-4B00-FB17-FB1F-2CB10CCD365A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout1|Brick10";
	rename -uid "F25A09BB-408C-C10B-A07D-798BAD5B74B3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 3.1428046 221.42856 -264.28577 3.1428046 
		221.42856 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout1";
	rename -uid "0AE754EF-4559-6794-CAD7-BFA251435EBF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout1|Brick14";
	rename -uid "3E04574A-4340-0A06-F8D1-4595147D22A9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout1";
	rename -uid "DF4F80EC-4AF3-F269-A71C-EC97424AE86E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout1|Brick7";
	rename -uid "FFD58161-4D30-6F83-8C6A-F8BD57990CBF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout1";
	rename -uid "23060B5B-43FA-9EC5-03F2-B8B811C08787";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout1|Brick3";
	rename -uid "CDFCDEF7-460E-B1DB-AE09-4BB11745F86B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout1";
	rename -uid "208CD181-4B5F-906A-380B-7FBE2697531E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout1|Brick2";
	rename -uid "20C232BF-443C-FF0E-941D-149DDEDF5A00";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout1";
	rename -uid "D9849537-4282-3E5A-10E8-EEB598929AEB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout1|Brick1";
	rename -uid "20E7F403-4C13-731A-FEF5-519D662E9E77";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout1";
	rename -uid "2D61C96D-443E-12B4-1A7B-28B0A31A8327";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout1|Brick20";
	rename -uid "1DB44D76-4E6D-9A50-E6DE-C59C1C4DBFA3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout1";
	rename -uid "ECD41451-45BE-E20D-4E00-299BBCE996F7";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout1|Brick11";
	rename -uid "CA1867A3-4043-C9D7-D167-D792702ED3AC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout1";
	rename -uid "BB2872FF-4AC9-BA2C-85A1-3982AA173237";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout1|Brick5";
	rename -uid "B73321E4-410F-CCAA-67B4-478D7313C0A5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout1";
	rename -uid "6DDB86F2-4F12-A09C-246F-5E91FEAD8687";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout1|Brick8";
	rename -uid "16F89CDD-4F5C-E2CB-6A20-4F8B26D94D7E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout1";
	rename -uid "E5C9391C-4246-5828-5C7F-75831CE23763";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout1|Brick6";
	rename -uid "4D8C2276-46ED-28ED-DF87-B3B29A23B194";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout1";
	rename -uid "9328322D-4007-901C-B6A1-CC8B3D48CAB0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout1|Brick17";
	rename -uid "1AAFFE78-4A62-5D3F-79B9-6C8EA26FA688";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout1";
	rename -uid "1FFE2340-4FBD-F8A5-A9E8-72A58D87457A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout1|Brick16";
	rename -uid "F78687F3-44B2-A1FA-5AE6-E88D6C51B3D5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout1";
	rename -uid "FD932505-40A4-B6A1-B8E2-919163AF5414";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout1|Brick18";
	rename -uid "55BC47C9-4A78-D0CB-14DB-89B9BECFDF2A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout1";
	rename -uid "E3077180-425D-52C5-4C02-9988C2ACAE33";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout1|Brick21";
	rename -uid "0F4AC4A9-47E9-CA8A-F7DF-8CA288C6F17F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout1";
	rename -uid "0DE9A00D-48CE-4CF7-ABF8-2DB82626A158";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout1|Brick22";
	rename -uid "393D8327-4A6E-7ADA-C7F2-BF9E9A13BCE1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout1";
	rename -uid "B166C623-4B9B-7EDC-99AF-D3B94C656DDC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout1|Brick23";
	rename -uid "B85B953B-42B3-BD1F-072E-9DB59E09F68A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout1";
	rename -uid "EF6030C6-47B3-DC15-5438-F7AE8CAEDE98";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout1|Brick24";
	rename -uid "A93A4959-4C2C-0F56-3ED3-18B1533E5BEF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout2";
	rename -uid "75DA81B1-497C-734E-4317-09A6672598DC";
	setAttr ".t" -type "double3" 0 0 -300 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout2";
	rename -uid "1133AD2F-4961-7B1B-6E07-7E92999DCB7C";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout2|Brick10";
	rename -uid "7B6CF5C4-451D-9ED0-9F12-66A8F42C07C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout2";
	rename -uid "0DA0B71B-4849-74A3-6C9D-80885DED3A45";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout2|Brick14";
	rename -uid "B1D06E9C-412A-B11B-94D2-90971C58F72F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout2";
	rename -uid "9FDC81E9-497E-609E-6248-99BEB9666B3D";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout2|Brick7";
	rename -uid "83AF1086-4A1B-1420-CF86-E4A6FE658820";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout2";
	rename -uid "B9D3CE22-4A7A-5C3F-215C-00987FA65AAF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout2|Brick3";
	rename -uid "41D3F75A-4958-3EDF-EB9B-379F89AA7048";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout2";
	rename -uid "1C404385-4F5C-975F-42D3-B590349143A0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout2|Brick2";
	rename -uid "64EE87D5-4A79-BCA3-4F0E-C5A63CBC4079";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout2";
	rename -uid "99A3876F-4E07-90BB-E640-97B61A51F0F3";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout2|Brick1";
	rename -uid "EE01D393-426B-B717-3852-5E8412AF59CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout2";
	rename -uid "E56A472F-4862-A67F-44F3-8E868C78BE4F";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout2|Brick20";
	rename -uid "AA90D602-4DC4-0765-92B2-44A034F69744";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout2";
	rename -uid "5F933579-41F5-771A-AD06-239532D48143";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout2|Brick11";
	rename -uid "F1BC87D2-47A8-F033-C5F5-6C92C0D1184A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout2";
	rename -uid "679F5924-4365-EA27-E51C-238A9946EF53";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout2|Brick5";
	rename -uid "E81ADAD3-4BBA-B2FF-CA07-699678C738F2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout2";
	rename -uid "468DA0EB-4F0C-1B19-7EEC-0DABA8FE8542";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout2|Brick8";
	rename -uid "B59FA4F7-4F10-B521-46FA-9082A3495F87";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout2";
	rename -uid "4BFAF4A2-4ACB-5FDE-EF19-E8BFAB046B76";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout2|Brick6";
	rename -uid "08C849EA-4C3E-8178-4B40-3190732E03D8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout2";
	rename -uid "3D49F3EB-48BE-765B-C7B4-288B6C3FC3C5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout2|Brick17";
	rename -uid "B8804467-4731-8642-BCEA-D8AA6D67F380";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout2";
	rename -uid "5063A224-4B7C-86C5-FF14-A7A5CF931298";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout2|Brick16";
	rename -uid "8564DD6C-44F0-2154-E44A-D8BA3CE22A02";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout2";
	rename -uid "2FBE6E7E-4A6A-30DD-7922-F692720BAFFC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout2|Brick18";
	rename -uid "897474CF-434C-78F2-BBF8-15B88F128902";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout2";
	rename -uid "EAF16A20-4F15-1845-D24E-B1A38EDD06CA";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout2|Brick21";
	rename -uid "8A1BF4E4-4F75-37AF-6D55-0398F28DCBE4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout2";
	rename -uid "BD3D2615-484C-32FE-439D-4DB0671A6DE6";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout2|Brick22";
	rename -uid "B59F9869-4C8A-7B8D-B8E5-ACB3BD729848";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout2";
	rename -uid "22DE8762-4198-A49B-C093-9992CB2FEC61";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout2|Brick23";
	rename -uid "1FC24E0A-489E-6155-17A1-F38422069F6B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout2";
	rename -uid "D931ECA0-4092-764E-5D0D-61A928F975C7";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout2|Brick24";
	rename -uid "81489D64-4022-7FDB-D0BF-28AF53309DD9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout3";
	rename -uid "57AD6F6C-4AB6-6047-21ED-A4957CE735DC";
	setAttr ".t" -type "double3" 300 0 0 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout3";
	rename -uid "9AF96F9E-4400-85FB-A5FC-0B88E5D36FA5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout3|Brick10";
	rename -uid "AF3860AC-4935-570D-0B1E-5CA5985E65C4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout3";
	rename -uid "6C35EBD8-46BB-F269-20DD-EBA5C392A559";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout3|Brick14";
	rename -uid "4CB028B4-43CA-A27B-F0B5-879B3CE7032F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout3";
	rename -uid "B904086C-492B-9353-E9AA-4291DAEB45B1";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout3|Brick7";
	rename -uid "5DF7CA37-4794-AA96-2AFD-3D9233031670";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout3";
	rename -uid "A9254395-43DE-FC0D-8DB7-869B60EA73AD";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout3|Brick3";
	rename -uid "B01C454F-458C-E34A-2CEA-FEA6DF276B86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout3";
	rename -uid "4ACA4BA2-496B-2059-A9F4-4682507442F5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout3|Brick2";
	rename -uid "5E3F8963-4CEB-ECC9-7DC6-15835AEBC4EE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout3";
	rename -uid "C0215B6C-4054-BF93-708C-72B889865E31";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout3|Brick1";
	rename -uid "939EF14E-40C3-5AE2-966A-9A88DE4E314A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout3";
	rename -uid "1ADA7AFD-46E0-E9C6-D9E0-E4890ADDC7CE";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout3|Brick20";
	rename -uid "F715C5DA-47D5-1710-B343-2D8D1E419181";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout3";
	rename -uid "E0164022-4B37-F9F6-3BD2-A28E54AB1FFF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout3|Brick11";
	rename -uid "955E1DF3-4F08-20EC-AC99-A09CC5664361";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout3";
	rename -uid "0D829644-4869-4FD2-0C51-69AE11386848";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout3|Brick5";
	rename -uid "88ACF151-4780-E34A-CE78-059A1BDBEFDA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout3";
	rename -uid "A35853CD-4A45-6606-5062-37AA4AF79045";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout3|Brick8";
	rename -uid "AE7FD4AB-49FE-C7D3-A074-BDBF8997350C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout3";
	rename -uid "CE7E9469-49B1-66AA-E875-03BCDE776679";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout3|Brick6";
	rename -uid "0F4EB47A-4510-9406-CEAF-23AC23F34D2A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout3";
	rename -uid "3A828E87-4E38-8213-C6B3-0EB1F8A532D4";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout3|Brick17";
	rename -uid "72DD970E-46A6-1CFB-8B84-0897D1291559";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout3";
	rename -uid "94D3FC3B-4F7E-B111-5272-83A930F356F4";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout3|Brick16";
	rename -uid "872AC20B-4365-C753-FC97-878422F13F5F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout3";
	rename -uid "5B13C228-4F80-26CC-3E16-B8A2F5A7DFF1";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout3|Brick18";
	rename -uid "357B57DC-4959-BA44-A2BB-91BC6EB3835F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout3";
	rename -uid "BBE7E047-475E-DD2F-3115-779BC6DBDA34";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout3|Brick21";
	rename -uid "1335CEDE-4B27-F9E8-676E-92A373D34ED8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout3";
	rename -uid "89588AE1-429F-49BC-3D13-CE8878FD7C7C";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout3|Brick22";
	rename -uid "A7818893-4CD9-1430-A9DA-8DA56E043EFC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout3";
	rename -uid "E00D1B5E-4D47-1C35-AD75-2DA581193322";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout3|Brick23";
	rename -uid "4EE9AED9-40A2-E3FE-29B6-A384FAF7C387";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout3";
	rename -uid "3573E9DD-4005-7B4F-E632-329DC9CA0D36";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout3|Brick24";
	rename -uid "B659B4D7-402A-5C9B-1FCA-25AEE611D72F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout4";
	rename -uid "857595AB-459F-E4C4-51F4-6B8FCF4F7DA2";
	setAttr ".t" -type "double3" 0 0 300 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout4";
	rename -uid "25950246-4BF6-BC72-1211-78A9DD16876E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout4|Brick10";
	rename -uid "D154B178-4BA0-01D5-330C-7EA2FABA6D02";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout4";
	rename -uid "A8C934FF-4D82-C807-CB9E-658FCF36BE40";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout4|Brick14";
	rename -uid "9126DE05-4585-37EC-3006-F1A2CB404123";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout4";
	rename -uid "9BCF9C3A-409B-EDD3-EDE2-A1923DA6F01E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout4|Brick7";
	rename -uid "26448482-4CE2-3F7A-CD06-B0B56A8D79B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout4";
	rename -uid "51B6DE65-4FD0-38BC-E3DB-289776A2B0BF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout4|Brick3";
	rename -uid "D55F3171-4C0E-37AF-5268-029E95A5A7DE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout4";
	rename -uid "31F80507-4FF9-0046-310A-49A26CB23947";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout4|Brick2";
	rename -uid "41495597-4AEE-0B09-D4DE-A48D4243360A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout4";
	rename -uid "52F12E18-4708-E436-E268-9CA7028DFE33";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout4|Brick1";
	rename -uid "0D262A8A-433C-B210-FD0F-DB8C54390FBD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout4";
	rename -uid "897E8DC8-4953-BCDB-65B2-40A850B9B46F";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout4|Brick20";
	rename -uid "A24A1776-49CB-47B7-3432-DAA2B21C6C26";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout4";
	rename -uid "A3A38CA8-474E-1B69-2D41-38ADD9873528";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout4|Brick11";
	rename -uid "7F438C27-4D47-7FC4-0E80-CD966E10BE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout4";
	rename -uid "6EA32F08-4669-DFD0-4A5A-D1BACEC26666";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout4|Brick5";
	rename -uid "7CE9CCE5-4A52-D61D-38AF-A1B4B9F74762";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout4";
	rename -uid "A96167A9-490C-7C65-7FC6-40A7224F9F27";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout4|Brick8";
	rename -uid "752B3949-42D4-B393-DF3D-04B1DD0F5A3A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout4";
	rename -uid "57AF0152-49F3-6AE7-04A6-6E8D115E22E1";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout4|Brick6";
	rename -uid "07D8D4AF-40F4-5E5C-7C4E-E6AB783A9DA9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout4";
	rename -uid "FAF68E73-4D75-F3DA-9F79-4BA9149A83B7";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout4|Brick17";
	rename -uid "C93391D8-434E-E264-9B64-0E82133E05C0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout4";
	rename -uid "CF505DD8-428D-800A-3B6E-F5B88C4BB013";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout4|Brick16";
	rename -uid "AC299815-44B1-571A-3983-9ABEFD3FFECC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout4";
	rename -uid "E5D4C5C2-4794-EE5A-C824-D4A85EB141EB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout4|Brick18";
	rename -uid "2E9B5545-4B1A-F793-FF55-5F9D8014A430";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout4";
	rename -uid "A587A8F6-49FB-7E51-03AD-97967B19465E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout4|Brick21";
	rename -uid "ECB73D0E-482A-BC7C-6017-71A410DD21B7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout4";
	rename -uid "90B2E8ED-4F0C-38F7-8409-748BBC319650";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout4|Brick22";
	rename -uid "16689AA7-4521-71E0-5C29-9C8FD95523D5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout4";
	rename -uid "EA6F4964-4C4B-8B2B-911E-02A1CCB8A3CB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout4|Brick23";
	rename -uid "930212F8-49DE-AEAB-A325-6AB79384C81D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout4";
	rename -uid "8C6C0FA6-4131-9765-BDA5-BEAC4628F1A0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout4|Brick24";
	rename -uid "FA9E2A65-4316-3090-56BB-F88BE50971EA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout5";
	rename -uid "B311F308-4A90-B995-51AF-F19D746FF12A";
	setAttr ".t" -type "double3" -300 0 0 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout5";
	rename -uid "22E968CC-41B4-307E-9283-CE8B9B2BB237";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout5|Brick10";
	rename -uid "544F95E4-4351-6420-34AA-4DA2EC833182";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout5";
	rename -uid "E84A97B8-435A-624C-CA1E-67B41BB21185";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout5|Brick14";
	rename -uid "FBA7A6D3-4856-175D-42FD-9DADD905344D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout5";
	rename -uid "47D63EBE-45FB-92D6-8562-D69D1AD741F5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout5|Brick7";
	rename -uid "CEFBAF28-48BB-4E33-F0C6-AD8A57B2BCDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout5";
	rename -uid "EEE37669-4BE0-07CE-C912-B1833A0E175D";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout5|Brick3";
	rename -uid "E940C5C3-4111-720F-1D5F-4AB0CDE11E42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout5";
	rename -uid "8F8FBE15-456A-4E6F-8724-C3B5C7E27070";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout5|Brick2";
	rename -uid "4DF09EDE-4E94-85A4-BD60-089CADE3692D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout5";
	rename -uid "0FBD81A2-4728-44E3-99F6-FAB4186E0908";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout5|Brick1";
	rename -uid "6D7DCC66-48B6-549A-066C-999C801909E8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout5";
	rename -uid "05A3DBD5-4942-77A1-3896-0F828A6677D8";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout5|Brick20";
	rename -uid "005EBD50-471F-0D71-C42A-B9B202333C9E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout5";
	rename -uid "6C8C752B-4E9D-934F-A2D4-74B1CA190D38";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout5|Brick11";
	rename -uid "317C711E-4F03-15B1-41C2-D4A541681548";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout5";
	rename -uid "A4CF6BDF-4E1B-E0DB-FB7A-A981046FD511";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout5|Brick5";
	rename -uid "4A512E94-4A6F-4D87-1A8B-E1A2B223BAD3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout5";
	rename -uid "ACA6DB19-4EB3-85A1-AE7A-ACB5CE196FBB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout5|Brick8";
	rename -uid "353B276A-40AE-873B-4E54-C7996066C1D2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout5";
	rename -uid "B955596A-45DB-357D-CC12-CF861F4F6EC3";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout5|Brick6";
	rename -uid "6F5E7AAF-4DFD-73E5-5A22-9BA8449C8DB4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout5";
	rename -uid "7C466759-435C-563C-07C4-DFB3B75576E6";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout5|Brick17";
	rename -uid "E0B4869D-493C-1B7B-0F74-838F15A0C6BB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout5";
	rename -uid "F585F69F-4444-5004-C36C-518C7D64BEDC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout5|Brick16";
	rename -uid "E227C623-43B1-47D1-FDE1-D7B1638262FC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout5";
	rename -uid "9BC37160-48E5-C3D8-6CBD-39B6077AC444";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout5|Brick18";
	rename -uid "A191C346-4B76-C4ED-76E5-C28D4A442511";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout5";
	rename -uid "96355926-44BB-C382-9736-FAA09DE1879A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout5|Brick21";
	rename -uid "19029F9A-4F67-FF53-6055-C0A55BE8794F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout5";
	rename -uid "56E7FC43-40B6-1A00-845E-91B863CA1D0E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout5|Brick22";
	rename -uid "208D9F69-4E7D-7DCD-DA7F-9FA39139D748";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout5";
	rename -uid "270E43F0-4648-E55B-C351-96B62B8A7044";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout5|Brick23";
	rename -uid "9891F704-450D-B462-66F6-EDACD46D3F7F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout5";
	rename -uid "E4FE5FDA-43E0-4D5C-3857-8685E0E72147";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout5|Brick24";
	rename -uid "EE151831-4FC9-6D8F-4632-0DAF1C666DF1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout6";
	rename -uid "1E8EB36D-4D44-C37C-834F-76814E06809E";
	setAttr ".t" -type "double3" 300.00000000000028 0 300.00000000000006 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout6";
	rename -uid "7172B52D-4B12-07F9-AB0D-35BD0A72EB27";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout6|Brick10";
	rename -uid "1D8053B3-4D4D-A943-A171-71AC4381FAE6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout6";
	rename -uid "5CAD3B9A-4090-8253-F651-39B86AF1BD82";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout6|Brick14";
	rename -uid "3E192E5B-4D29-BF7A-83FC-1CB5921D76C7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout6";
	rename -uid "36AF05F4-4EDC-B307-25B7-D285003218E0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout6|Brick7";
	rename -uid "5155378C-4EB5-BD04-ED78-0495D7AE31F9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout6";
	rename -uid "6987825D-49EE-C790-1CD8-93B533AF04BA";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout6|Brick3";
	rename -uid "8CEAA1C6-44A2-1E23-B913-DC81E76C9094";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout6";
	rename -uid "1CF150D4-439A-5756-6B20-4E9848E4751B";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout6|Brick2";
	rename -uid "5DF26C2D-4C4A-4958-A2DE-79A0C9BB6288";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout6";
	rename -uid "96B20998-477C-657D-B56A-BFB03C31F08E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout6|Brick1";
	rename -uid "386BE01F-4245-3D01-855C-C1894B4E59AA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout6";
	rename -uid "83238811-41D0-FEF1-2491-A7BCD81ADDC3";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout6|Brick20";
	rename -uid "E3564D0E-4D58-BF84-A46C-77917F378B78";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout6";
	rename -uid "BD2E0F7E-4F8E-2780-5AEE-CFA82E28CB8A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout6|Brick11";
	rename -uid "678E7F27-43D1-0A7C-4ED0-CFA6C9972807";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout6";
	rename -uid "5AFB2298-4785-4A9B-FA0D-2E9E88EE22CF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout6|Brick5";
	rename -uid "979FE894-45BF-925F-6516-16ADA0A99A70";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout6";
	rename -uid "F5FEB052-4939-09CE-8842-76B7594998F0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout6|Brick8";
	rename -uid "E4806CB8-460A-9FFE-61DD-B1991BAECFE8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout6";
	rename -uid "686BB881-404F-F709-0721-84BC427724E5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout6|Brick6";
	rename -uid "78D34A3E-4733-855A-AD26-9DBFF4F7C48B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout6";
	rename -uid "976C7575-47FE-3642-FAF4-349DEF4EC943";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout6|Brick17";
	rename -uid "A45A365C-4EC4-9024-B2E1-34BDDE10C565";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout6";
	rename -uid "4631C550-4884-0EEC-DB66-EC86E3E59865";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout6|Brick16";
	rename -uid "C600B92A-463E-07CB-26EA-7A812DB30D7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout6";
	rename -uid "495195B0-4202-4BA6-1000-B88EB1E650DA";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout6|Brick18";
	rename -uid "1ED312C4-4CA1-D150-CF75-EFB49FE66974";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout6";
	rename -uid "8958A63F-495D-DE21-9BB9-1493A2791600";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout6|Brick21";
	rename -uid "F3502573-4806-51A9-F3C9-899B6FD1B4D1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout6";
	rename -uid "3A708C5E-4101-9620-CE62-D0AB56B706B0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout6|Brick22";
	rename -uid "5B8B2BBA-4901-E977-F472-C8B1B8C13E9E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout6";
	rename -uid "46A5887B-42FC-2257-AC52-FFA7BBA9B2C2";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout6|Brick23";
	rename -uid "93EAEED4-4193-32BF-B3D5-3DA3CE4DE90A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout6";
	rename -uid "9A0216B9-42C9-9C4B-E87B-62A98DAC13EB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout6|Brick24";
	rename -uid "BFC92B92-44AF-AD5F-81B0-DABEDDDF1AC9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout7";
	rename -uid "703C96AB-4A3E-CCF9-A1E3-57BF349F8605";
	setAttr ".t" -type "double3" 300 0 -300 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout7";
	rename -uid "89CE115D-4666-A807-D921-AF9D71725779";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout7|Brick10";
	rename -uid "4633088B-4B96-7748-D066-D8B736AB53EF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout7";
	rename -uid "10D9F9A3-443E-6A9F-7D4D-CC841B4AD5EB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout7|Brick14";
	rename -uid "19CEDA19-47DA-9506-B508-04A40EA216E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout7";
	rename -uid "BF44AD01-43E3-0484-62D8-F0BCF91EBAAC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout7|Brick7";
	rename -uid "4EB414CD-4CAC-3F3A-AA7A-46B4CB7A5CC9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout7";
	rename -uid "3AF0066A-455A-ADFC-90D9-B3BCA964E751";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout7|Brick3";
	rename -uid "ECC7B124-4C7D-02F4-5CAB-2E9ACDC0174A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout7";
	rename -uid "12CABC96-4F73-F6B8-9B56-4F9EF1D5A5B2";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout7|Brick2";
	rename -uid "3816D2ED-44B1-ED5D-FF24-9FB379917EAC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout7";
	rename -uid "E056C399-4923-8C99-DBE7-F0AF3B6D8982";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout7|Brick1";
	rename -uid "6D452F8C-4ED7-85C9-23FC-7AA767042DCE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout7";
	rename -uid "D20B0F74-452B-4DFE-AF2F-7EB4A3072624";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout7|Brick20";
	rename -uid "262D2A37-4D6A-7DEF-4209-42894BF4F712";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout7";
	rename -uid "92C1B03D-4741-3851-A9E8-18AB30795C6A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout7|Brick11";
	rename -uid "AC3D8E80-4377-C2AF-C863-02AE55EAD240";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout7";
	rename -uid "466F814F-491F-74C0-6293-10ABF517083B";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout7|Brick5";
	rename -uid "61D1DDBE-4BB7-F561-6F75-D5AB3CE9127B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout7";
	rename -uid "1E4C00DB-4D90-9BBB-9F37-E1AF8281B9C1";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout7|Brick8";
	rename -uid "21DF2642-4D73-B349-7DB3-DC83BB1873CE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout7";
	rename -uid "29547355-455C-3567-DA65-A5A020111B68";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout7|Brick6";
	rename -uid "C1AA6B12-460E-CA10-E837-4899B432CE28";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout7";
	rename -uid "F6B73DF0-4844-98B2-36AD-28B03CB1283E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout7|Brick17";
	rename -uid "861E5DBA-4925-EC47-BAE8-B8BB4F6E37E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout7";
	rename -uid "991D0EFC-40F4-BE49-581A-5A9E140EEA4F";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout7|Brick16";
	rename -uid "F3B48EC0-4021-51B0-5B70-7E9E11CE93E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout7";
	rename -uid "0E482C8A-4C62-1272-27C5-AFA1A636CB21";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout7|Brick18";
	rename -uid "D6B53169-444B-5F2C-C07A-49B6C44717C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout7";
	rename -uid "AC83259E-406C-9B05-7DEB-55BC73D2CE5D";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout7|Brick21";
	rename -uid "70261AEA-4ADD-4642-5133-3785D7B1F2A8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout7";
	rename -uid "C920D1C4-4BC0-ED5A-621C-CBAE0DA7F8AE";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout7|Brick22";
	rename -uid "AEEC2860-442E-51B9-E8A3-43B86FD39974";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout7";
	rename -uid "A91EEC54-4065-1F13-01C2-4FBC319CE25D";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout7|Brick23";
	rename -uid "A245A313-49FC-5681-C1EF-8B8A286AF711";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout7";
	rename -uid "E2610EDE-4408-2E8C-A1DC-80BCC165BE70";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout7|Brick24";
	rename -uid "0179799A-4677-0A6F-393D-05891C77A5A7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout8";
	rename -uid "44C71DD2-439F-D56C-14E5-D49D14D93B09";
	setAttr ".t" -type "double3" -300 0 -300 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout8";
	rename -uid "14894120-46C8-F1A3-746A-C1BF5EF7DA87";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout8|Brick10";
	rename -uid "F89AA1CE-4B4D-0A10-6057-89A81B7D97A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout8";
	rename -uid "24C1E2F4-4483-4FBC-2960-9C861A759EF5";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout8|Brick14";
	rename -uid "8D5C3DBA-4695-3AAF-2746-F4AFBF1ABC44";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout8";
	rename -uid "45ECC5FA-4752-FC2D-BC82-95A949957F88";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout8|Brick7";
	rename -uid "07F1F8D7-45D2-9A49-0E80-2A9C6FFC2022";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout8";
	rename -uid "CBD433F8-4D70-6BAC-C327-31853C75C919";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout8|Brick3";
	rename -uid "192FDB42-44F0-73A8-A9FF-57A6CAF43470";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout8";
	rename -uid "1BAF52EB-4E24-5113-A2A3-30AE3B5839EA";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout8|Brick2";
	rename -uid "3893BA67-4712-4D81-014B-39A26C4F1E1D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout8";
	rename -uid "84E9B6B6-47E4-BB05-E959-E6B4A25C45FB";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout8|Brick1";
	rename -uid "57C9E6FD-40C4-488A-BA5E-BEBEAB90F060";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout8";
	rename -uid "5AB76A4B-462F-D686-DF1A-F19C51F2644A";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout8|Brick20";
	rename -uid "E08AFE30-4511-5DBD-8108-D8B5AD477820";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout8";
	rename -uid "6905D9DE-4BA0-754F-D13A-69BECDE5E276";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout8|Brick11";
	rename -uid "79E6524D-4DE8-7B2E-4E39-51A14E8D4820";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout8";
	rename -uid "BD8150C2-47E0-5B72-1629-4B87E3C4970B";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout8|Brick5";
	rename -uid "0D8F4689-48AC-348B-C3BD-7A924370BE16";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout8";
	rename -uid "8D77CF57-4D8C-A630-D3E7-4BA7A1896468";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout8|Brick8";
	rename -uid "20A16D7D-43E8-B7FB-43D8-06948CFE1D3A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout8";
	rename -uid "AE7ED830-4A0D-080E-689B-D8A207E2B3B9";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout8|Brick6";
	rename -uid "82A936EE-4DB9-A790-FEF5-6EBB02A1EDA5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout8";
	rename -uid "333DA038-4491-38BB-1012-B280F0D3D97B";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout8|Brick17";
	rename -uid "B01F7172-4F00-82A9-6547-1F8AAE7B604E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout8";
	rename -uid "3673C203-40E4-2438-FA91-07BB98D7AF71";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout8|Brick16";
	rename -uid "4D1D064F-4459-CB12-C140-93B927EA1CFD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout8";
	rename -uid "2D540502-4E54-F44F-E07B-74A488F783CC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout8|Brick18";
	rename -uid "60DD5CEA-45DD-A10B-A9C8-FFA339580B31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout8";
	rename -uid "F332020B-4B2E-AA7E-9875-D29E4393D097";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout8|Brick21";
	rename -uid "A1E75DC1-4F93-07E6-8FE2-D5A4CF878AD7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout8";
	rename -uid "AA2E63FD-48C5-E040-EA1C-BBAE1B0C5151";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout8|Brick22";
	rename -uid "4CB9AC8F-4A48-736C-63D1-02B89D64B2BF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout8";
	rename -uid "8809706C-4582-FB80-1787-EF9037610708";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout8|Brick23";
	rename -uid "18A14C07-4722-16AA-9176-53B84F06D0C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout8";
	rename -uid "4165E4CD-45DD-D70F-C73D-89919B148FA1";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout8|Brick24";
	rename -uid "3A846249-4582-1C2F-9A26-66ABA199B9EB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Rock_Floor_layout9";
	rename -uid "8DE36693-495C-9D16-E394-489FE89B8C25";
	setAttr ".t" -type "double3" -300 0 300 ;
createNode transform -n "Brick10" -p "Rock_Floor_layout9";
	rename -uid "91D6F164-45F2-3CE9-06FE-8592986B88A0";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
	setAttr ".sp" -type "double3" -257.14288330078125 0 214.28572845458984 ;
createNode mesh -n "BrickShape10" -p "|Rock_Floor_layout9|Brick10";
	rename -uid "F306659A-4C43-5712-73DA-ABAB75C65803";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -250.00003 -3.5527137e-014 
		207.14287 -264.28574 -3.5527137e-014 207.14287 -250.00003 2.1316282e-014 207.14287 
		-264.28574 2.1316282e-014 207.14287 -250.00003 2.1316282e-014 221.42857 -264.28577 
		2.1316282e-014 221.42857 -250.00003 -3.5527137e-014 221.42857 -264.28577 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick14" -p "Rock_Floor_layout9";
	rename -uid "C92D94AC-4CF6-F5CD-C2CB-1382DEBEE097";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
	setAttr ".sp" -type "double3" -171.42855834960937 0 3.0517578125e-005 ;
createNode mesh -n "BrickShape14" -p "|Rock_Floor_layout9|Brick14";
	rename -uid "32A8031E-47C9-463A-DF63-3DA776BD218A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -3.5527137e-014 
		-7.1428223 -178.57141 -3.5527137e-014 -7.1428223 -164.28571 2.1316282e-014 -7.1428223 
		-178.57141 2.1316282e-014 -7.1428223 -164.28571 2.1316282e-014 7.1428833 -178.57143 
		2.1316282e-014 7.1428833 -164.28571 -3.5527137e-014 7.1428833 -178.57143 -3.5527137e-014 
		7.1428833;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick7" -p "Rock_Floor_layout9";
	rename -uid "ACE53CB3-4142-44E9-48AA-EBBA53DC1DBF";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
	setAttr ".sp" -type "double3" -192.85715484619141 0 85.71429443359375 ;
createNode mesh -n "BrickShape7" -p "|Rock_Floor_layout9|Brick7";
	rename -uid "B9F1061F-42FD-9B9D-4C62-F4AB89B95F66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		78.571442 -178.57143 -4.9737992e-014 -7.1428528 -78.571426 7.1054274e-015 78.571442 
		-178.57143 7.1054274e-015 -7.1428528 -207.14285 7.1054274e-015 178.57144 -307.14288 
		7.1054274e-015 92.857162 -207.14285 -4.9737992e-014 178.57144 -307.14288 -4.9737992e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick3" -p "Rock_Floor_layout9";
	rename -uid "8C468DDF-4F8E-A7FD-74B1-10957BB8FB5F";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -214.28572845458984 0 150 ;
	setAttr ".sp" -type "double3" -214.28572845458984 0 150 ;
createNode mesh -n "BrickShape3" -p "|Rock_Floor_layout9|Brick3";
	rename -uid "163022E1-4FC2-FF2A-7DD7-98A32130B8C6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14287 -4.9737992e-014 
		78.571426 -307.14288 -4.9737992e-014 121.42857 -207.14287 7.1054274e-015 78.571426 
		-307.14288 7.1054274e-015 121.42857 -121.42857 7.1054274e-015 178.57143 -221.42857 
		7.1054274e-015 221.42857 -121.42857 -4.9737992e-014 178.57143 -221.42857 -4.9737992e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick2" -p "Rock_Floor_layout9";
	rename -uid "09BD455D-4E42-EBF0-4FDE-C7805000B1D6";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
	setAttr ".sp" -type "double3" -107.14286804199219 0 107.14286804199219 ;
createNode mesh -n "BrickShape2" -p "|Rock_Floor_layout9|Brick2";
	rename -uid "EAC7187C-4752-4435-ACE9-09B0446E2D4D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571442 -1.4210855e-014 
		78.571442 -135.71429 -1.4210855e-014 78.571442 -78.571442 4.2632564e-014 78.571442 
		-135.71429 4.2632564e-014 78.571442 -78.571442 4.2632564e-014 135.71429 -135.71429 
		4.2632564e-014 135.71429 -78.571442 -1.4210855e-014 135.71429 -135.71429 -1.4210855e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick1" -p "Rock_Floor_layout9";
	rename -uid "D8A96E9B-413A-5986-D6A8-4385A7DDCE07";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
	setAttr ".sp" -type "double3" -128.57142639160156 0 171.42857360839844 ;
createNode mesh -n "BrickShape1" -p "|Rock_Floor_layout9|Brick1";
	rename -uid "BA92A8B2-4B62-5BAA-71F9-409347A921AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -171.42857361 -61.14501953 214.28572083 -85.71427917 -61.14501953 214.28572083
		 -171.42857361 61.14501953 214.28572083 -85.71427917 61.14501953 214.28572083 -171.42857361 61.14501953 128.57142639
		 -85.71429443 61.14501953 128.57142639 -171.42857361 -61.14501953 128.57142639 -85.71429443 -61.14501953 128.57142639;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick20" -p "Rock_Floor_layout9";
	rename -uid "E6E4CD6E-476A-732A-967A-41B02E3C45CC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 192.85714721679687 ;
createNode mesh -n "BrickShape20" -p "|Rock_Floor_layout9|Brick20";
	rename -uid "15CD2BF9-4798-2364-AC12-8B982B5F7411";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -3.5527137e-014 
		164.28572 -50 -3.5527137e-014 164.28572 7.1428528 2.1316282e-014 164.28572 -50 2.1316282e-014 
		164.28572 7.1428528 2.1316282e-014 221.42857 -50 2.1316282e-014 221.42857 7.1428528 
		-3.5527137e-014 221.42857 -50 -3.5527137e-014 221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick11" -p "Rock_Floor_layout9";
	rename -uid "44A200DE-44F2-BF67-5473-66BF60A03F4D";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
	setAttr ".sp" -type "double3" -300.00003051757812 0 128.57144165039063 ;
createNode mesh -n "BrickShape11" -p "|Rock_Floor_layout9|Brick11";
	rename -uid "BEE7F8BD-49A8-9FBA-A9BB-3DAB8E1DD005";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -292.85718 -3.5527137e-014 
		121.42859 -307.14288 -3.5527137e-014 121.42859 -292.85718 2.1316282e-014 121.42859 
		-307.14288 2.1316282e-014 121.42859 -292.85718 2.1316282e-014 135.71429 -307.14288 
		2.1316282e-014 135.71429 -292.85718 -3.5527137e-014 135.71429 -307.14288 -3.5527137e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick5" -p "Rock_Floor_layout9";
	rename -uid "5697D7D7-4543-1D6E-8EF4-B2B3CD538725";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 192.85714721679687 ;
createNode mesh -n "BrickShape5" -p "|Rock_Floor_layout9|Brick5";
	rename -uid "8A1A625A-4B6B-31FF-E808-9BB4B0B968C8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -35.714294 -3.5527137e-014 
		164.28572 -92.857147 -3.5527137e-014 164.28572 -35.714294 2.1316282e-014 164.28572 
		-92.857147 2.1316282e-014 164.28572 -35.714294 2.1316282e-014 221.42857 -92.857147 
		2.1316282e-014 221.42857 -35.714294 -3.5527137e-014 221.42857 -92.857147 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick8" -p "Rock_Floor_layout9";
	rename -uid "998FE2EC-44A1-6EDE-07BB-0DBEC909023E";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
	setAttr ".sp" -type "double3" -85.714279174804688 0 278.57141876220703 ;
createNode mesh -n "BrickShape8" -p "|Rock_Floor_layout9|Brick8";
	rename -uid "DB99679F-4BF4-6190-7A31-B0842A9AFD23";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.142868 -4.9737992e-014 
		164.28572 -178.57143 -4.9737992e-014 164.28572 7.142868 7.1054274e-015 164.28572 
		-178.57143 7.1054274e-015 164.28572 7.142868 7.1054274e-015 392.85712 -178.57141 
		7.1054274e-015 392.85712 7.142868 -4.9737992e-014 392.85712 -178.57141 -4.9737992e-014 
		392.85712;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick6" -p "Rock_Floor_layout9";
	rename -uid "2AAE88EC-488D-16CF-B3D6-32BE12910548";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
	setAttr ".sp" -type "double3" -192.85714721679687 0 192.85714721679687 ;
createNode mesh -n "BrickShape6" -p "|Rock_Floor_layout9|Brick6";
	rename -uid "4F8CB133-46D7-F405-44F2-FDB506BD039E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28572 -3.5527137e-014 
		164.28572 -221.42857 -3.5527137e-014 164.28572 -164.28572 2.1316282e-014 164.28572 
		-221.42857 2.1316282e-014 164.28572 -164.28572 2.1316282e-014 221.42857 -221.42857 
		2.1316282e-014 221.42857 -164.28572 -3.5527137e-014 221.42857 -221.42857 -3.5527137e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick17" -p "Rock_Floor_layout9";
	rename -uid "301580AD-441A-20C6-049E-63B9048A7A69";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
	setAttr ".sp" -type "double3" -171.42857360839844 0 235.71427917480469 ;
createNode mesh -n "BrickShape17" -p "|Rock_Floor_layout9|Brick17";
	rename -uid "93BBA681-4751-365F-A52D-EE86431DE035";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -78.571426 -4.9737992e-014 
		207.14285 -178.57143 -4.9737992e-014 164.28571 -78.571426 7.1054274e-015 207.14285 
		-178.57143 7.1054274e-015 164.28571 -164.28572 7.1054274e-015 307.14285 -264.28571 
		7.1054274e-015 264.28571 -164.28572 -4.9737992e-014 307.14285 -264.28571 -4.9737992e-014 
		264.28571;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick16" -p "Rock_Floor_layout9";
	rename -uid "17808486-47DA-9D72-EF13-34BEA43C3FE9";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
	setAttr ".sp" -type "double3" -278.57145690917969 0 21.428581237792969 ;
createNode mesh -n "BrickShape16" -p "|Rock_Floor_layout9|Brick16";
	rename -uid "8EB07F2D-4F82-0257-8D8A-F788BF33214F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -207.14288 -4.9737992e-014 
		-92.857132 -350.00003 -4.9737992e-014 -92.857132 -207.14288 7.1054274e-015 -92.857132 
		-350.00003 7.1054274e-015 -92.857132 -207.14288 7.1054274e-015 135.71429 -350.00003 
		7.1054274e-015 135.71429 -207.14288 -4.9737992e-014 135.71429 -350.00003 -4.9737992e-014 
		135.71429;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick18" -p "Rock_Floor_layout9";
	rename -uid "9E855A34-4161-B5B8-8D03-D69F13B48BBC";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
	setAttr ".sp" -type "double3" -64.285720825195313 0 128.57142639160156 ;
createNode mesh -n "BrickShape18" -p "|Rock_Floor_layout9|Brick18";
	rename -uid "BD0CBE0B-4F07-7BEE-1368-A9B5E6AF3948";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		35.714275 -135.71429 -2.8421709e-014 35.714275 7.1428528 2.8421709e-014 35.714275 
		-135.71429 2.8421709e-014 35.714275 7.1428528 2.8421709e-014 221.42857 -135.71429 
		2.8421709e-014 221.42857 7.1428528 -2.8421709e-014 221.42857 -135.71429 -2.8421709e-014 
		221.42857;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick21" -p "Rock_Floor_layout9";
	rename -uid "86A9A21A-4C54-0CA1-50B6-A588E938CE3C";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 257.14288330078125 ;
createNode mesh -n "BrickShape21" -p "|Rock_Floor_layout9|Brick21";
	rename -uid "CF23FB6D-4C78-68C3-C40F-CD82E979F03F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -2.8421709e-014 
		250.00003 -50 -2.8421709e-014 250.00003 7.1428528 2.8421709e-014 250.00003 -50 2.8421709e-014 
		250.00003 7.1428528 2.8421709e-014 264.28574 -50 2.8421709e-014 264.28574 7.1428528 
		-2.8421709e-014 264.28574 -50 -2.8421709e-014 264.28574;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick22" -p "Rock_Floor_layout9";
	rename -uid "9C747B59-4187-9920-C90D-0DA1F6C1B33F";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
	setAttr ".sp" -type "double3" -64.28571891784668 0 64.285751342773438 ;
createNode mesh -n "BrickShape22" -p "|Rock_Floor_layout9|Brick22";
	rename -uid "AC808520-4116-50D8-895E-7C879A335E3A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  50.000004 -2.8421709e-014 
		35.714325 -49.999996 -2.8421709e-014 -7.1428223 50.000004 2.8421709e-014 35.714325 
		-49.999996 2.8421709e-014 -7.1428223 -78.571442 2.8421709e-014 135.71432 -178.57144 
		2.8421709e-014 92.857178 -78.571442 -2.8421709e-014 135.71432 -178.57144 -2.8421709e-014 
		92.857178;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick23" -p "Rock_Floor_layout9";
	rename -uid "316DB198-4DA4-2C0D-A1F1-A4B7C01BF140";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
	setAttr ".sp" -type "double3" -21.428573608398438 0 21.428604125976563 ;
createNode mesh -n "BrickShape23" -p "|Rock_Floor_layout9|Brick23";
	rename -uid "65920386-4A31-ECA1-D731-6FB1AED8519D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.1428528 -1.4210855e-014 
		-7.1428223 -50 -1.4210855e-014 -7.1428223 7.1428528 4.2632564e-014 -7.1428223 -50 
		4.2632564e-014 -7.1428223 7.1428528 4.2632564e-014 50.000031 -50 4.2632564e-014 50.000031 
		7.1428528 -1.4210855e-014 50.000031 -50 -1.4210855e-014 50.000031;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Brick24" -p "Rock_Floor_layout9";
	rename -uid "76A46377-4556-ECD1-ED9E-C0BDD6C9C220";
	setAttr ".s" -type "double3" 0.92857507243598625 1 0.92857507243598625 ;
	setAttr ".rp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
	setAttr ".sp" -type "double3" -235.71427917480469 0 1.52587890625e-005 ;
createNode mesh -n "BrickShape24" -p "|Rock_Floor_layout9|Brick24";
	rename -uid "697273E7-47EB-80D6-5FA2-DA86522F6C20";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -164.28571 -2.8421709e-014 
		-92.857132 -307.14285 -2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 -92.857132 
		-307.14285 2.8421709e-014 -92.857132 -164.28571 2.8421709e-014 92.857162 -307.14285 
		2.8421709e-014 92.857162 -164.28571 -2.8421709e-014 92.857162 -307.14285 -2.8421709e-014 
		92.857162;
	setAttr -s 8 ".vt[0:7]"  -50 -61.14501953 50 50 -61.14501953 50 -50 61.14501953 50
		 50 61.14501953 50 -50 61.14501953 -50 50 61.14501953 -50 -50 -61.14501953 -50 50 -61.14501953 -50;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "D3C48AE4-49E8-EFE4-4121-6787C46D2DCC";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "110A66D8-4DD0-92E0-EA64-33AE0BCD60F1";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "74CF4476-4363-33D2-F1C8-6585A74CEB89";
createNode displayLayerManager -n "layerManager";
	rename -uid "30349431-4BE5-0A35-A14C-03AEE17E3B0E";
	setAttr ".cdl" 3;
	setAttr -s 4 ".dli[1:3]"  1 2 3;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "2B3707BB-4B57-A8C1-EB98-A2A607266ED9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "33E8C155-4B73-9976-187B-149C572304EE";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "FEAB6308-4EDC-B769-A2B1-E39E9EB39044";
	setAttr ".g" yes;
createNode displayLayer -n "Human_Proportions";
	rename -uid "F08F774F-4581-0104-0EF9-77AA735C2C18";
	setAttr ".v" no;
	setAttr ".c" 6;
	setAttr ".do" 1;
createNode displayLayer -n "floor";
	rename -uid "A5248690-4174-A7B9-EB8F-7694534AA6E4";
	setAttr ".dt" 2;
	setAttr ".v" no;
	setAttr ".c" 9;
	setAttr ".do" 2;
createNode polyCube -n "polyCube1";
	rename -uid "50704C23-477A-28C3-2CC2-7A8B63D84B1B";
	setAttr ".ax" -type "double3" 0 1.0000000000000002 2.2204460492503131e-016 ;
	setAttr ".w" 100.00000000000011;
	setAttr ".h" 122.29003764547528;
	setAttr ".d" 100.00000000000003;
	setAttr ".cuv" 4;
createNode displayLayer -n "RockGroup";
	rename -uid "4C201212-4EAE-EAC6-5722-A4996D639940";
	setAttr ".dt" 1;
	setAttr ".v" no;
	setAttr ".c" 4;
	setAttr ".do" 3;
createNode polyTweak -n "polyTweak1";
	rename -uid "36778108-40F7-8F58-D3A0-B8B812E06CCB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7.14285278 0 -7.14285278 -7.14285278
		 0 -7.14285278 7.14285278 0 -7.14285278 -7.14285278 0 -7.14285278 7.14285278 0 7.14285278
		 -7.14286804 0 7.14285278 7.14285278 0 7.14285278 -7.14286804 0 7.14285278;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "E21B08FC-4AC0-D943-AF21-D0B63C59C5B1";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1.0000000000000004 0 0 0 0 1 0 -128.57142639160156 -4.7580991609837836e-015 171.42857360839844 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "69C9AF9C-42F1-870A-F64D-3E979CC07F7A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 559\n                -height 396\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 559\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 558\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 558\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 559\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 559\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1124\n                -height 836\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1124\n            -height 836\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n"
		+ "                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n"
		+ "            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n"
		+ "                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n"
		+ "                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n"
		+ "            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n"
		+ "\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1124\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1124\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 1000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "E30CE858-4BC8-2184-C47C-6BAA806009AD";
	setAttr ".b" -type "string" "playbackOptions -min 1.25 -max 150 -ast 1.25 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1.25;
	setAttr ".unw" 1.25;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 181 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "Human_Proportions.di" "HimanProptions.do";
connectAttr "RockGroup.di" "Rock_Floor_layout.do";
connectAttr "transformGeometry1.og" "|Rock_Floor_layout|Brick1|BrickShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "Human_Proportions.id";
connectAttr "layerManager.dli[2]" "floor.id";
connectAttr "layerManager.dli[3]" "RockGroup.id";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry1.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "HimanProptionsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|Rock_Floor_layout|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout1|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout2|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout3|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout4|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout5|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout6|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout7|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout8|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick10|BrickShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick14|BrickShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick7|BrickShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick3|BrickShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick2|BrickShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick1|BrickShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick20|BrickShape20.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick11|BrickShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick5|BrickShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick8|BrickShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick6|BrickShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick17|BrickShape17.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick16|BrickShape16.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick18|BrickShape18.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick21|BrickShape21.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick22|BrickShape22.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick23|BrickShape23.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Rock_Floor_layout9|Brick24|BrickShape24.iog" ":initialShadingGroup.dsm"
		 -na;
// End of cactusEnvironment.ma
