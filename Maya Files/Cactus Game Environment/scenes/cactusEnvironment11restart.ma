//Maya ASCII 2017 scene
//Name: cactusEnvironment11restart.ma
//Last modified: Sat, Oct 08, 2016 10:47:03 AM
//Codeset: 1252
requires maya "2017";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "F4752E32-436C-96B4-671C-E393C116702E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1644.3935407786446 188.22727583623885 -1534.1478865388895 ;
	setAttr ".r" -type "double3" 365.09473422081265 8556.2000000013741 359.99999999570991 ;
	setAttr ".rp" -type "double3" 2.2737367544323206e-013 -2.8421709430404007e-014 -1.1368683772161603e-013 ;
	setAttr ".rpt" -type "double3" -2.2432309974541269e-013 -2.1719255950202117e-014 
		-3.0225186346280341e-013 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "625611E5-4767-EDE3-CA07-26A3A15D7474";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 1955.1973228558343;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 3574.7592034786067 600.00000000000034 -1950.0000000000016 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "45C04804-413A-02A6-DD28-A2BF76C9FA7C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3567.5217376750452 3244.1837365278329 -2155.333376498198 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9222DDF4-4036-06A6-41AB-47A831583AF8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 2869.1837392640409;
	setAttr ".ow" 637.05011668261352;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 3567.5217376750452 374.99999726379156 -2155.3333764981985 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "64AED913-4E77-0D80-3E46-86B24323D441";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 124.49403104461771 180.71417360236035 1052.0302590453587 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "72AF731D-4580-8576-B29F-6E8017BE34D5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".pze" yes;
	setAttr ".coi" 902.03023749416866;
	setAttr ".ow" 4338.761414184235;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 235.3425737291501 116.79418574342738 150.00002155118997 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "38F8A8EE-4491-0F4B-DB42-A99848C9B355";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7178.863527896925 755.27315366219102 -1772.7338286603413 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6574913D-482D-6B89-2F89-14903D406F72";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 3598.8290300595254;
	setAttr ".ow" 3472.1109463483231;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 3580.0344978373987 834.27740204894781 -1800.6885171842337 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "door_lock";
	rename -uid "A47AC204-4B19-7D34-619E-C09CEFA33A46";
	setAttr ".t" -type "double3" -63.060245035467176 0 0 ;
	setAttr ".rp" -type "double3" 232.47406114369846 117.71319961547852 150.00008258634622 ;
	setAttr ".sp" -type "double3" 232.47406114369846 117.71319961547852 150.00008258634622 ;
createNode transform -n "group1";
	rename -uid "A34102AA-44DC-E57B-CD52-8D939F76F188";
	setAttr ".rp" -type "double3" -200 0 -249.47872924804687 ;
	setAttr ".sp" -type "double3" -200 0 -249.47872924804687 ;
createNode transform -n "group2" -p "group1";
	rename -uid "B9DFABA6-406D-389E-4E07-E6A5CF3AF2E7";
	setAttr ".rp" -type "double3" -201.38310241699219 0 -249.47872924804687 ;
	setAttr ".sp" -type "double3" -201.38310241699219 0 -249.47872924804687 ;
createNode transform -n "group47";
	rename -uid "64598507-4ED2-6409-BAB7-27AFB0A3B946";
	setAttr ".r" -type "double3" -35 0 0 ;
	setAttr ".rp" -type "double3" -500 0 -230.44215393066406 ;
	setAttr ".sp" -type "double3" -500 0 -230.44215393066406 ;
createNode transform -n "group48";
	rename -uid "5DA4DA47-4921-F48F-FF7B-DFBDEAC0571B";
	setAttr ".r" -type "double3" 0 0 35 ;
	setAttr ".rp" -type "double3" -1100 0 450.00000000000045 ;
	setAttr ".sp" -type "double3" -1100 0 450.00000000000045 ;
createNode transform -n "group49";
	rename -uid "B4282F86-4F18-241D-C179-2EAAC6053E31";
	setAttr ".r" -type "double3" 0 0 -35 ;
	setAttr ".rp" -type "double3" 200.00000000000023 0 399.99999999999989 ;
	setAttr ".sp" -type "double3" 200.00000000000023 0 399.99999999999989 ;
createNode transform -n "group51";
	rename -uid "31F307A4-4059-F959-B05C-43B352971248";
	setAttr ".r" -type "double3" 35 0 0 ;
	setAttr ".rp" -type "double3" -400.00000000000017 0 1100.0000000000005 ;
	setAttr ".sp" -type "double3" -400.00000000000017 0 1100.0000000000005 ;
createNode transform -n "wall_post";
	rename -uid "6FD4E68F-44B6-74C7-E5DE-01B4285BC6B8";
	setAttr ".t" -type "double3" -2694.7883148193359 0 -2400 ;
	setAttr ".rp" -type "double3" 2994.7883148193359 0 600 ;
	setAttr ".sp" -type "double3" 2994.7883148193359 0 600 ;
createNode transform -n "wall_lantern_post";
	rename -uid "46122819-403F-CEB3-D7CC-BDA9BAACAE49";
	setAttr ".t" -type "double3" -2694.7883148193359 0 0 ;
	setAttr ".rp" -type "double3" 2994.7883148193359 0 600 ;
	setAttr ".sp" -type "double3" 2994.7883148193359 0 600 ;
createNode transform -n "group53" -p "wall_lantern_post";
	rename -uid "62DADA50-4436-CE6D-586B-BDAA6810F6C6";
createNode transform -n "wall_lantern_post1";
	rename -uid "810AE521-4F17-3103-1D96-1BA33AC44AAE";
	setAttr ".t" -type "double3" 2105.2116851806641 0 0 ;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".rp" -type "double3" 2994.7883148193359 0 600 ;
	setAttr ".sp" -type "double3" 2994.7883148193359 0 600 ;
createNode transform -n "floor_tile1";
	rename -uid "4ECE201C-496F-FAD1-8BFE-B0A682360482";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile1Shape" -p "floor_tile1";
	rename -uid "D5B9A815-4954-F18B-539E-3E90FC548539";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile2";
	rename -uid "A350C9DA-4B71-42A5-E575-509F32601027";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile2Shape" -p "floor_tile2";
	rename -uid "17B3A42B-4089-F757-2657-2FA43BF53F43";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile3";
	rename -uid "30C2D117-4EFE-2538-ECAF-0CA79658B30A";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile3Shape" -p "floor_tile3";
	rename -uid "AE45AF5F-4A19-AB7E-F71E-39B74D447AF3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile4";
	rename -uid "AD450AFD-4110-301F-22CF-A58FAF1A3164";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile4Shape" -p "floor_tile4";
	rename -uid "21A72344-4C1A-B27F-7643-5FBBBDEFD902";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile6";
	rename -uid "32CD4D97-4961-939E-D88C-BB9A1447F100";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 -450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile6Shape" -p "floor_tile6";
	rename -uid "F25A62EF-43EA-4326-7601-6BBCD87A4E94";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile7";
	rename -uid "DFDBC589-4BA9-7584-B3CC-4692B598AC44";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile7Shape" -p "floor_tile7";
	rename -uid "6ACA3F81-4E05-F9AA-D4FF-FFB2E050890F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile8";
	rename -uid "C8601F86-41DA-51CF-D27B-58B9B0FFFB07";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile8Shape" -p "floor_tile8";
	rename -uid "87F0A352-4496-5877-31CB-CA966C909176";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile9";
	rename -uid "CE31D2B9-45E9-DDEB-A801-BDADCAB7D052";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 -450 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile9Shape" -p "floor_tile9";
	rename -uid "CE0FC367-4074-A59B-264A-BDA2577993A7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile11";
	rename -uid "91572FDE-4AC9-C9B9-181F-BB991D987517";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile11Shape" -p "floor_tile11";
	rename -uid "E94FC1D7-43B3-CF99-CBB3-9EB4340431DC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile12";
	rename -uid "A69137DA-4AD7-A2B0-856E-A9B47526FBE9";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile12Shape" -p "floor_tile12";
	rename -uid "C63065C8-4420-463A-9850-28ABBECCF340";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile13";
	rename -uid "75CD5DFA-4D29-673A-231D-62B22E37F622";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile13Shape" -p "floor_tile13";
	rename -uid "CCC382D7-4D5F-8524-7468-A9AEA457066E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile14";
	rename -uid "5624820F-4266-9E4E-E887-A0B7AC07C1AE";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile14Shape" -p "floor_tile14";
	rename -uid "A74F8606-4AA7-2829-2376-7DA8F2750C3C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile16";
	rename -uid "9CAD91B5-4373-E170-B348-35B497685DB3";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile16Shape" -p "floor_tile16";
	rename -uid "DA820866-4036-6770-0A50-A2AE6E70A011";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile17";
	rename -uid "3D20A9E3-400C-C788-6C78-128E13564939";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile17Shape" -p "floor_tile17";
	rename -uid "D75C02EB-4499-744D-CBFA-C7825B05C947";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile18";
	rename -uid "9524C947-497F-FB20-9D27-00BDE21F86FB";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile18Shape" -p "floor_tile18";
	rename -uid "E8E1A508-47BF-5643-FC81-1C8F65E7AB60";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile19";
	rename -uid "6389654C-49D5-FAA4-2D30-4F9F33014D12";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile19Shape" -p "floor_tile19";
	rename -uid "9D0D6E64-4167-1DF7-2D80-6DBD8F5252CB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile21";
	rename -uid "FEE256D6-42F9-B4BE-C4FA-F8A8D23FCB48";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 -3150 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile21Shape" -p "floor_tile21";
	rename -uid "2F45216A-4DED-7554-9146-6E9D8070C678";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile22";
	rename -uid "B5C29F7D-414D-4068-89A6-2885FD98949F";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -3150 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile22Shape" -p "floor_tile22";
	rename -uid "68B76190-4E73-FB2E-EBA5-0D9ADCEDF607";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile23";
	rename -uid "BCFF621F-46ED-F0F2-6446-5B8C8519DF4B";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -3150 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile23Shape" -p "floor_tile23";
	rename -uid "62CF6359-4E58-39C3-EFCB-AB85DE22AB3F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile24";
	rename -uid "B606B5A4-4970-6284-8AAB-608899C1EA1C";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 -3150 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile24Shape" -p "floor_tile24";
	rename -uid "75AFA67F-4850-E4DA-C94F-F59208A9E780";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile26";
	rename -uid "6E7A02EE-4824-5056-A942-E5B63C52C90C";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 -4050 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile26Shape" -p "floor_tile26";
	rename -uid "8E9315B5-47B6-D3F9-BA9D-859F8FF54358";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile27";
	rename -uid "CFB46246-4AF8-1B70-1A02-6EBC7997406E";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -4050 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile27Shape" -p "floor_tile27";
	rename -uid "164D84AA-4AED-E6D0-13B7-A9A4048F3CF9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile28";
	rename -uid "B5854462-401A-E7DC-7341-0FB45CD08B44";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -4050 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile28Shape" -p "floor_tile28";
	rename -uid "8201CE4C-4943-C325-FEF9-D6A233348D19";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile29";
	rename -uid "FEBBE1C6-4D84-6EE5-302D-7E8492545DD2";
	setAttr ".t" -type "double3" 3150 -9.9920072216264089e-014 -4050 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile29Shape" -p "floor_tile29";
	rename -uid "DFFE895B-4636-7BD5-EC09-0AA35238DB20";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile30";
	rename -uid "095A07FD-4BBC-1B48-F2C3-3D9FC9F727A8";
	setAttr ".t" -type "double3" 4050 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile30Shape" -p "floor_tile30";
	rename -uid "EA47AD5F-44B7-91A1-835E-388FD1029005";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile31";
	rename -uid "28CA16D4-4AEC-DB1D-05D6-D4A73BEDBDC8";
	setAttr ".t" -type "double3" 4050 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile31Shape" -p "floor_tile31";
	rename -uid "A20EF1C7-4930-EFC0-0ACC-DE85FA964D88";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile32";
	rename -uid "505A883B-40AB-9F40-B11B-8DA79EE2F418";
	setAttr ".t" -type "double3" 4950 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile32Shape" -p "floor_tile32";
	rename -uid "42D47752-4566-148C-30AF-3C9E429F6EE8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile33";
	rename -uid "0B88B787-4908-F6A3-F97D-9CBF04311C8F";
	setAttr ".t" -type "double3" 4950 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile33Shape" -p "floor_tile33";
	rename -uid "D752E822-412A-2E7C-B072-D3BAF3A7114C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile34";
	rename -uid "A48AFAE9-44C1-9678-D498-A2AB64DF8EB1";
	setAttr ".t" -type "double3" 5850 -9.9920072216264089e-014 -1350 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile34Shape" -p "floor_tile34";
	rename -uid "874F1ACF-41EF-2050-6A9D-A2A3D73215EB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile35";
	rename -uid "C5F15390-4E79-F807-A11D-45AC1C094E60";
	setAttr ".t" -type "double3" 5850 -9.9920072216264089e-014 -2250 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile35Shape" -p "floor_tile35";
	rename -uid "0F97F297-4347-F491-BDF7-1EBA03B456A3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile36";
	rename -uid "814BE4D7-42CE-3B8C-9E16-84BE0D403B1B";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -4950 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile36Shape" -p "floor_tile36";
	rename -uid "041FC103-4607-26F0-2FE6-68A1593853BF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile37";
	rename -uid "B85FB069-41A9-24FB-0D1E-25912EF3CAAA";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -4950 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile37Shape" -p "floor_tile37";
	rename -uid "EB1B5275-49FB-50E3-02B7-FA97A36D91D3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile38";
	rename -uid "38E97327-4DAA-0CBE-F90F-13BE0FF74ADD";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 -5850 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile38Shape" -p "floor_tile38";
	rename -uid "F1B050CB-4378-5FA4-C013-4E8282E2AFBD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile39";
	rename -uid "ECA8A561-4B31-06DB-B12B-52BB3B0B51E6";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -5850 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile39Shape" -p "floor_tile39";
	rename -uid "0FDC991F-4C36-2960-0F61-CEAD68FE4AE0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile40";
	rename -uid "C5559691-4B90-F5FE-1E89-A4B5000B430E";
	setAttr ".t" -type "double3" 1349.9999999999998 -9.9920072216264089e-014 -6750 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".rp" -type "double3" 449.99999999999989 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" -1.1368683772161602e-013 0 0 ;
createNode mesh -n "floor_tile40Shape" -p "floor_tile40";
	rename -uid "F7692509-4065-2CD2-259B-50B8A65FFD9A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile41";
	rename -uid "FA023512-4BD7-3A37-CEE8-BA8E13D4FA57";
	setAttr ".t" -type "double3" 2250 -9.9920072216264089e-014 -6750 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile41Shape" -p "floor_tile41";
	rename -uid "4FAC4B0C-4688-E28B-ED71-71BC8A7E18EC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile42";
	rename -uid "9C8CBDCE-44B1-DC8D-7030-FE96D966A551";
	setAttr ".t" -type "double3" 450 -9.9920072216264089e-014 1350 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile42Shape" -p "floor_tile42";
	rename -uid "FD8C50E1-46AC-1FDD-1E99-17803040B563";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile43";
	rename -uid "363B01D3-40A1-036D-874F-FF98F1DC140F";
	setAttr ".t" -type "double3" 1350 -9.9920072216264089e-014 1350 ;
	setAttr -av ".tx";
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile43Shape" -p "floor_tile43";
	rename -uid "EE789ABD-476F-DF18-1AB9-298ED3FCA788";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile44";
	rename -uid "D52B698A-4435-8BB7-85B6-278892FCDF7F";
	setAttr ".t" -type "double3" 2250 449.99999999999994 900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 0 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile44Shape" -p "floor_tile44";
	rename -uid "9FBF2444-472C-67AB-CCAE-1C8120B2CBC4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile45";
	rename -uid "3E7F8F3F-432B-3AFD-123F-46B894AE7684";
	setAttr ".t" -type "double3" 3150 449.99999999999994 900.00000000000091 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile45Shape" -p "floor_tile45";
	rename -uid "8376B654-4167-40D4-010F-0B836FBBE1CC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile46";
	rename -uid "CEF944F4-412F-24BC-19C8-5FA98F3F9079";
	setAttr ".t" -type "double3" 3150 449.99999999999994 -2.2737367544323206e-013 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile46Shape" -p "floor_tile46";
	rename -uid "A005265A-4C48-8BCE-82AE-FEB99BA72BA3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile47";
	rename -uid "78502DA1-4778-6892-04F2-38B478AB1E54";
	setAttr ".t" -type "double3" 3150 449.99999999999994 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile47Shape" -p "floor_tile47";
	rename -uid "C350B9EF-476A-628F-57C6-0B8C74E25F60";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile48";
	rename -uid "AB03B61C-4CE8-D583-B304-F581634DDAA8";
	setAttr ".t" -type "double3" 4050 449.99999999999994 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile48Shape" -p "floor_tile48";
	rename -uid "C5B8CDB2-40BA-0FB4-78DC-B585CF5ACEF0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile49";
	rename -uid "DA14D508-4ADF-BC09-E5D2-C491BCB1F80D";
	setAttr ".t" -type "double3" 4950 449.99999999999994 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile49Shape" -p "floor_tile49";
	rename -uid "B5D0B823-43A4-6EA3-EC7E-028A61D739A9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile50";
	rename -uid "644024B3-42B9-7B48-DA63-A58713FF1CF2";
	setAttr ".t" -type "double3" 5850 450 -899.99999999999989 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -450 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000023 450.00000000000023 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 0 ;
createNode mesh -n "floor_tile50Shape" -p "floor_tile50";
	rename -uid "050BD226-4B5B-AB88-1830-36B3C0753311";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile51";
	rename -uid "B72C6C3C-4AEB-4D1C-4D3C-4C9E106FC059";
	setAttr ".t" -type "double3" 5850 450 -1800.0000000000002 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile51Shape" -p "floor_tile51";
	rename -uid "D920E191-49EC-6E41-4D5C-80A608BDA6B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile52";
	rename -uid "353B2E85-43DB-3EC7-85D7-038C8C8B4CDB";
	setAttr ".t" -type "double3" 5850 450 -2700 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile52Shape" -p "floor_tile52";
	rename -uid "87220516-491A-8DD9-8C8F-AF8E12F75684";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile53";
	rename -uid "F3234544-4012-7025-E477-0DB3EA9EC975";
	setAttr ".t" -type "double3" 3150 449.99999999999994 -3600 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile53Shape" -p "floor_tile53";
	rename -uid "A449900C-46F6-CAD0-FFC0-1FAB12CB0FA2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile54";
	rename -uid "0AE737FE-4FDE-E13C-21B4-76B9FC7D8987";
	setAttr ".t" -type "double3" 3600 449.99999999999994 -4050 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile54Shape" -p "floor_tile54";
	rename -uid "391D620A-4DF4-C7BB-DF3D-3482D49AAB7B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile55";
	rename -uid "2351EC42-4350-6E87-9C91-A5B13413D04B";
	setAttr ".t" -type "double3" 2700.0000000000005 450.00000000000017 -4950 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile55Shape" -p "floor_tile55";
	rename -uid "576C4BEC-4879-111E-0B71-3E841509466A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile56";
	rename -uid "16CCF491-483B-FBA9-4087-64918B046867";
	setAttr ".t" -type "double3" 2700.0000000000005 450.00000000000017 -5850 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile56Shape" -p "floor_tile56";
	rename -uid "BE04764A-4504-E86D-FA6A-20A9F9840B2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile57";
	rename -uid "130BFAD8-4C3A-D2CD-3A1B-72948A58B619";
	setAttr ".t" -type "double3" 2699.9999999999995 450.00000000000006 -6750 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile57Shape" -p "floor_tile57";
	rename -uid "1FAA66A5-48D5-F6C9-29D1-E79CEC4328F8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile58";
	rename -uid "5D847C18-4811-0D04-9285-32A92CAE7C3F";
	setAttr ".t" -type "double3" -450 449.99999999999994 -3600 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile58Shape" -p "floor_tile58";
	rename -uid "6E74F73B-4A14-3499-4AC0-0D9DC255A45D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile59";
	rename -uid "9AF57E18-497F-CC17-998D-85B4039BA24E";
	setAttr ".t" -type "double3" -450 449.99999999999994 -2700 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile59Shape" -p "floor_tile59";
	rename -uid "4C868321-4B01-9D28-4184-5EB26C9152C0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile60";
	rename -uid "300A2CD8-4326-F736-DCC3-59BDA26FDEE1";
	setAttr ".t" -type "double3" -450 449.99999999999994 -1800 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile60Shape" -p "floor_tile60";
	rename -uid "E14F41E7-43DA-1085-F4BE-3B9945DCECBE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile61";
	rename -uid "74321880-47D3-F43D-51C3-FDA588B16F57";
	setAttr ".t" -type "double3" -1.4988010832439616e-013 450.00000000000006 -1350 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile61Shape" -p "floor_tile61";
	rename -uid "337F2F4F-4216-5D2B-BD3F-46B074460235";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile62";
	rename -uid "A27097B2-4D5B-D36D-B55B-3AB661D2F0D9";
	setAttr ".t" -type "double3" 4.4098058538111208e-013 450.00000000000034 -450 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile62Shape" -p "floor_tile62";
	rename -uid "F9256B9A-48CB-25A3-290C-3A8E59EFD9AF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile63";
	rename -uid "834AA759-4B26-3139-904D-D3839301E825";
	setAttr ".t" -type "double3" 1.0662581928500003e-012 450.00000000000063 450 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile63Shape" -p "floor_tile63";
	rename -uid "6050A5D2-4E09-5636-E3E6-37916B402FFF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile64";
	rename -uid "D9EC4DAF-4B00-9FF3-5E3B-55B8BD1DBC28";
	setAttr ".t" -type "double3" 450 450 -4500 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile64Shape" -p "floor_tile64";
	rename -uid "2BF9961C-45DE-16FD-EB1F-38B34C75AFA0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile65";
	rename -uid "F521EFBF-423A-99E7-9B99-0CBBEB1FA290";
	setAttr ".t" -type "double3" 899.99999999999977 450.00000000000017 -5850 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile65Shape" -p "floor_tile65";
	rename -uid "65D8FA9F-4951-6905-74A4-3FAE2E963006";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile66";
	rename -uid "94E6035A-4ACE-1D01-FB4C-E5BF1CE6314C";
	setAttr ".t" -type "double3" 899.99999999999875 450.0000000000004 -6750 ;
	setAttr ".r" -type "double3" -89.999999999999957 -89.999999999999929 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450.00000000000011 9.9920072216264089e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999903 -449.99999999999989 900 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 1.1368683772161605e-013 0 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile66Shape" -p "floor_tile66";
	rename -uid "964DCE0D-440F-AC1C-F228-20BB31CE48C4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile67";
	rename -uid "33258170-4E6D-909B-7038-A29F7CB3BEC8";
	setAttr ".t" -type "double3" 4950 449.99999999999994 -2700.0000000000005 ;
	setAttr ".r" -type "double3" -89.999999999999986 180 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile67Shape" -p "floor_tile67";
	rename -uid "ACAF3FDF-435A-F341-58DB-2BA0FA24647F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile68";
	rename -uid "02C4AEF7-47D3-98E9-6C69-FFBD426D2513";
	setAttr ".t" -type "double3" 4050 449.99999999999994 -2700.0000000000005 ;
	setAttr ".r" -type "double3" -89.999999999999986 180 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile68Shape" -p "floor_tile68";
	rename -uid "3282B0AD-4843-BBBF-2710-C3B554A706D2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile69";
	rename -uid "8B38532C-4956-758B-5A84-4886D304F505";
	setAttr ".t" -type "double3" 4050 449.99999999999994 -2700.0000000000005 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 0 ;
createNode mesh -n "floor_tile69Shape" -p "floor_tile69";
	rename -uid "6B5CC5CB-4E31-8132-6112-D191E90CE9C0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile70";
	rename -uid "9F648DF4-4D8E-A029-4644-E982821ECC28";
	setAttr ".t" -type "double3" 3150 449.99999999999989 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile70Shape" -p "floor_tile70";
	rename -uid "539292B9-4F23-BD2F-0671-B1992F45C200";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile71";
	rename -uid "1BAFA287-4FC7-328F-00A5-A6B5705F9A19";
	setAttr ".t" -type "double3" 2250 449.99999999999972 -7200 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264063e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -2.5243548967072372e-029 0 ;
createNode mesh -n "floor_tile71Shape" -p "floor_tile71";
	rename -uid "45DA8193-4215-EC6E-6D90-A5A2E0B64905";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile72";
	rename -uid "10304B7E-46C0-0BA3-8F4C-A790B162ACBB";
	setAttr ".t" -type "double3" 1350 449.99999999999937 -7200 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264038e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -5.0487097934144739e-029 0 ;
createNode mesh -n "floor_tile72Shape" -p "floor_tile72";
	rename -uid "E6D90794-4545-F603-E649-88802A334E26";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile73";
	rename -uid "8FD7EB3A-4FB2-8B33-E7E8-088374248526";
	setAttr ".t" -type "double3" 450.00000000000006 449.99999999999937 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264038e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -5.0487097934144739e-029 0 ;
createNode mesh -n "floor_tile73Shape" -p "floor_tile73";
	rename -uid "A49EC317-4507-A72C-C17D-F099991D2E3E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile74";
	rename -uid "70A32749-4EC4-B543-CDD7-9CA14CA95401";
	setAttr ".t" -type "double3" 3150 449.99999999999994 -3300 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" -450 9.9920072216264076e-014 -450 ;
	setAttr ".rpt" -type "double3" 900.00000000000023 -450.00000000000011 1350 ;
	setAttr ".sp" -type "double3" -450 9.9920072216264051e-014 -450.00000000000011 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072512e-029 1.4210854715202002e-012 ;
createNode mesh -n "floor_tile74Shape" -p "floor_tile74";
	rename -uid "F172C2AD-4767-3C69-4DC6-F7ADC89B6AB3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -150 -1.2621774e-029 -5.6843419e-014 
		-600 -2.5243549e-029 -5.6843419e-014 -150 -1.2621774e-029 -5.6843419e-014 -600 -2.5243549e-029 
		-5.6843419e-014;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile75";
	rename -uid "5BD333A1-4130-1FEF-5FCD-84A77530349B";
	setAttr ".t" -type "double3" 3150 449.99999999999994 -1800 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" -450 9.9920072216264076e-014 -450 ;
	setAttr ".rpt" -type "double3" 900.00000000000023 -450.00000000000011 1350 ;
	setAttr ".sp" -type "double3" -450 9.9920072216264051e-014 -450.00000000000011 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072512e-029 1.4210854715202002e-012 ;
createNode mesh -n "floor_tile75Shape" -p "floor_tile75";
	rename -uid "AB76A1C3-4597-36A0-BD26-B2A8E8821603";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[1]" -type "float3" -450 -2.5243549e-029 -5.6843419e-014 ;
	setAttr ".pt[3]" -type "float3" -450 -2.5243549e-029 -5.6843419e-014 ;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile76";
	rename -uid "74D4F3B6-4999-A424-72AE-788AEBEBC2BA";
	setAttr ".t" -type "double3" -450 1350.0000000000002 -3600 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile76Shape" -p "floor_tile76";
	rename -uid "6656F856-497A-63B1-10CE-F38AA2A6BDD4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile77";
	rename -uid "2732F044-48CD-90FA-916E-A3953BAE2F00";
	setAttr ".t" -type "double3" 1.0662581928500003e-012 1350 450 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile77Shape" -p "floor_tile77";
	rename -uid "A6959B6A-4B6B-666F-A080-8084EFE62D98";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile78";
	rename -uid "B6503FCF-4EB0-7702-5B1E-C5B4F8DF38B0";
	setAttr ".t" -type "double3" -1.4988010832439616e-013 1350 -1350 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile78Shape" -p "floor_tile78";
	rename -uid "15D57625-4CA6-16CB-5780-7286A15A3B96";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile79";
	rename -uid "4C4B8FB3-47F6-09CA-C002-5DBA81E8B70D";
	setAttr ".t" -type "double3" 4.4098058538111208e-013 1350 -450 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile79Shape" -p "floor_tile79";
	rename -uid "7E1C6E6A-4F52-30E8-F134-829E3DCAFC13";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile80";
	rename -uid "550D02E2-4754-82E9-AC16-18829A396509";
	setAttr ".t" -type "double3" 450 1350.0000000000002 -4500 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile80Shape" -p "floor_tile80";
	rename -uid "282315E3-41FD-DC69-FAEB-5B9623F91284";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile81";
	rename -uid "7C58D961-4DF5-A9D2-ACF2-E4B8124E5B43";
	setAttr ".t" -type "double3" -450 1350.0000000000002 -1800 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile81Shape" -p "floor_tile81";
	rename -uid "1C3E5D94-4B34-86CD-D6B6-10B12756DC8A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile82";
	rename -uid "AB949FD5-4425-CADD-B955-FD8BC6F7FD44";
	setAttr ".t" -type "double3" 2699.9999999999995 1350.0000000000002 -6750 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile82Shape" -p "floor_tile82";
	rename -uid "8B51CA9E-4C66-CEC9-D297-B499F6E89479";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile83";
	rename -uid "39E6A022-4162-0EB4-B67A-8A94C1D331FF";
	setAttr ".t" -type "double3" -450 1350.0000000000002 -2700 ;
	setAttr ".r" -type "double3" -89.999999999999986 270 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile83Shape" -p "floor_tile83";
	rename -uid "8CB900D8-437E-6CE4-389B-14AEE978035E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile84";
	rename -uid "CAE4220E-4FED-7B2A-F9A7-50AA2903DA3F";
	setAttr ".t" -type "double3" 2700.0000000000005 1350.0000000000002 -5850 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile84Shape" -p "floor_tile84";
	rename -uid "408B48F5-4A2E-CA69-B065-24A30978AD59";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile85";
	rename -uid "F506AE4E-4F1A-6C4E-5124-05A24C38353F";
	setAttr ".t" -type "double3" 2700.0000000000005 1350.0000000000002 -4950 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile85Shape" -p "floor_tile85";
	rename -uid "77F102F6-4F2A-B2E1-C989-3CB1EA0D85AF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile86";
	rename -uid "A42508F5-49AC-0A0E-D53E-D8A99E4B1511";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -3600 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile86Shape" -p "floor_tile86";
	rename -uid "A5B4B110-4C07-9A06-9A8B-46A6993C3FB1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile87";
	rename -uid "8F6D7AE0-4830-B3FB-A496-C6984B4D8CF2";
	setAttr ".t" -type "double3" 5850 1350.0000000000002 -2700 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile87Shape" -p "floor_tile87";
	rename -uid "47B0CEE1-4667-4900-2440-CBAC0287FB8A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile88";
	rename -uid "E1C91903-4388-E7A4-8CF2-3FAFA1F45C5B";
	setAttr ".t" -type "double3" 5850 1350.0000000000002 -1800.0000000000002 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile88Shape" -p "floor_tile88";
	rename -uid "652569A3-4C43-1AF3-7CA5-948BE4C7D690";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile89";
	rename -uid "70960A45-4595-B184-4037-778606A09735";
	setAttr ".t" -type "double3" 5850 1350 -899.99999999999989 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -450 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000023 450.00000000000023 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 0 ;
createNode mesh -n "floor_tile89Shape" -p "floor_tile89";
	rename -uid "ABA8A107-4858-DA81-91B3-90AA16EF0D86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile90";
	rename -uid "E26D6EBD-4E64-648F-E56C-D18E45056996";
	setAttr ".t" -type "double3" 4950 1350 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile90Shape" -p "floor_tile90";
	rename -uid "B32D4B7D-4625-13F6-E2DE-9E9A55BDF494";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile91";
	rename -uid "D214018C-4279-B8CB-BE8C-FAA385EC5D07";
	setAttr ".t" -type "double3" 4050 1350 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile91Shape" -p "floor_tile91";
	rename -uid "54030427-46F3-0085-A03E-8C8F1C4C2678";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile92";
	rename -uid "D5BACFAE-4964-B58E-E44D-8785DFD358DC";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile92Shape" -p "floor_tile92";
	rename -uid "42AD998F-4882-631E-48A2-8891DC1248D1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile93";
	rename -uid "9B225EB8-4363-C4E8-12E6-7BBC033A128C";
	setAttr ".t" -type "double3" 3600 1350.0000000000002 -4050 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264139e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -450.00000000000023 -450.00000000000028 -1.0680345496893996e-013 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 5.0487097934144778e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile93Shape" -p "floor_tile93";
	rename -uid "5017D2B9-43B4-8E60-6C75-A1A0856BE92F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile94";
	rename -uid "1AB89B43-4FB4-7B21-74CB-588E9172C40A";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -2.2737367544323206e-013 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile94Shape" -p "floor_tile94";
	rename -uid "96ECE622-4B33-8B2C-5BB3-9B87D8C5EE66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile95";
	rename -uid "ED767BB0-4C8C-0CEB-90C3-07BDF673DAC0";
	setAttr ".t" -type "double3" 3150 1350 900.00000000000091 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile95Shape" -p "floor_tile95";
	rename -uid "10DFAB25-433E-12BA-6949-2CA7F6FD7F6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile96";
	rename -uid "8816E9C7-4AD9-D6EF-3AD2-CF95498F04A4";
	setAttr ".t" -type "double3" 2250 1350 900.00000000000034 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 0 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile96Shape" -p "floor_tile96";
	rename -uid "44F5D134-4254-FCB6-1AC0-F4A334D81704";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile97";
	rename -uid "5F2B09DB-4D56-5733-5849-4C90C504F0A5";
	setAttr ".t" -type "double3" 1350 900 1350 ;
	setAttr -av ".tx";
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile97Shape" -p "floor_tile97";
	rename -uid "98158EB0-41D4-CB3F-FE56-46A1F4668826";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile98";
	rename -uid "2FB081DF-48DD-E0A9-503A-5AAE9B60E4D3";
	setAttr ".t" -type "double3" 450 900 1350 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile98Shape" -p "floor_tile98";
	rename -uid "FA725590-496D-DB5F-F7B7-E1A7946963AE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile99";
	rename -uid "921F8484-4E3F-00E6-313F-B1AB7E56FA95";
	setAttr ".t" -type "double3" 450.00000000000006 1350.0000000000002 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264038e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -5.0487097934144739e-029 0 ;
createNode mesh -n "floor_tile99Shape" -p "floor_tile99";
	rename -uid "00C95041-48C5-EEB7-B2B4-3AA1CEEA3B7C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile100";
	rename -uid "95209AF0-4B8F-ED62-DC7D-4CB3F155AF6E";
	setAttr ".t" -type "double3" 1350 1350.0000000000002 -7200 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264038e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -5.0487097934144739e-029 0 ;
createNode mesh -n "floor_tile100Shape" -p "floor_tile100";
	rename -uid "A99C900B-40F8-ADD2-9950-F5BCB4F1F430";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile101";
	rename -uid "CE86700B-486F-9D48-95DA-1FAF20B6E217";
	setAttr ".t" -type "double3" 2250 1350.0000000000002 -7200 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264063e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 -2.5243548967072372e-029 0 ;
createNode mesh -n "floor_tile101Shape" -p "floor_tile101";
	rename -uid "4E6BB8EE-4577-38E6-D220-BBA6B2A49E95";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile102";
	rename -uid "BEAE2ECF-418A-E75D-9055-C287C3462D19";
	setAttr ".t" -type "double3" 4050 1350.0000000000002 -2700.0000000000005 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 0 ;
createNode mesh -n "floor_tile102Shape" -p "floor_tile102";
	rename -uid "5462B708-44EF-B235-0B60-DFA22AD0336F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile103";
	rename -uid "ED57D3BA-424F-C8D3-B3F0-FFB315228CA0";
	setAttr ".t" -type "double3" 4050 1350 -2700.0000000000005 ;
	setAttr ".r" -type "double3" -89.999999999999986 180 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile103Shape" -p "floor_tile103";
	rename -uid "D2FE5385-4CA1-37FA-495E-20B5B1BFA188";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile104";
	rename -uid "F164754E-4E77-F456-15D5-99A7DADC9648";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile104Shape" -p "floor_tile104";
	rename -uid "94009399-4128-79D9-F5DA-9091B3703452";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile105";
	rename -uid "2C1A1FB2-43FE-6C86-F302-ED8AECA0ED50";
	setAttr ".t" -type "double3" 4950 1350 -2700.0000000000005 ;
	setAttr ".r" -type "double3" -89.999999999999986 180 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile105Shape" -p "floor_tile105";
	rename -uid "9D797315-45B6-0C0B-84AD-C38FE060C175";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile106";
	rename -uid "01B4BD45-42FA-2D8A-7156-88807B2237D2";
	setAttr ".t" -type "double3" 899.99999999999875 1350 -6750 ;
	setAttr ".r" -type "double3" -89.999999999999957 -89.999999999999929 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450.00000000000011 9.9920072216264089e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999903 -449.99999999999989 900 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 1.1368683772161605e-013 0 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile106Shape" -p "floor_tile106";
	rename -uid "858E0789-4886-4D91-41DA-ABB0520C5D60";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile107";
	rename -uid "9CEF2098-45EE-C898-94AD-4698B3BDD05E";
	setAttr ".t" -type "double3" 899.99999999999977 1350 -5850 ;
	setAttr ".r" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" -449.99999999999949 -450 899.99999999999989 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile107Shape" -p "floor_tile107";
	rename -uid "FFED85E6-4F91-969F-5F3E-9B93E3734AFF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile108";
	rename -uid "FC9746B8-419F-58C5-3F6D-E9A3626A2FB1";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -2700 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile108Shape" -p "floor_tile108";
	rename -uid "F1E69F87-42F8-06B0-DD09-9C86FBF7C5E6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile109";
	rename -uid "A1E668E2-45C4-7D6D-A943-76848D8C48C5";
	setAttr ".t" -type "double3" 3150 1350.0000000000002 -1800 ;
	setAttr ".r" -type "double3" -89.999999999999986 90 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264114e-014 -449.99999999999989 ;
	setAttr ".rpt" -type "double3" 0 -450.00000000000011 450.00000000000011 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".spt" -type "double3" 0 2.5243548967072383e-029 1.1368683772161602e-013 ;
createNode mesh -n "floor_tile109Shape" -p "floor_tile109";
	rename -uid "3CEC38D6-4F65-58D5-6554-7CBF2AE32657";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile110";
	rename -uid "3B51EBCC-4268-F8CA-656D-50A868844C87";
	setAttr ".t" -type "double3" 1350 1350.0000000000002 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile110Shape" -p "floor_tile110";
	rename -uid "07458A99-45CB-EB3C-E840-92B93602B5D9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "floor_tile111";
	rename -uid "55E68B87-498A-9AE2-A28E-2399B485BC90";
	setAttr ".t" -type "double3" 2250 1350.0000000000002 -4500 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 180 ;
	setAttr ".rp" -type "double3" 450 9.9920072216264089e-014 -450 ;
	setAttr ".rpt" -type "double3" -900 -450.00000000000017 450.0000000000004 ;
	setAttr ".sp" -type "double3" 450 9.9920072216264089e-014 -450 ;
createNode mesh -n "floor_tile111Shape" -p "floor_tile111";
	rename -uid "2950495F-476C-5E0A-C39B-33BABC9CC500";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -450 -9.9920072e-014 450 450 -9.9920072e-014 450
		 -450 9.9920072e-014 -450 450 9.9920072e-014 -450;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube3";
	rename -uid "1735B945-488E-E60A-6656-46A9872456A2";
	setAttr ".t" -type "double3" 3590.1248550415039 417.13869965636974 -2211.355227084136 ;
	setAttr ".rp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "F89F550A-4FB2-E776-F648-EC87B1D39463";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 0 -339.33325 -20.92347 
		0 -339.33325 -23.35494 0 -339.33325 -20.92347 0 -339.33325 -23.35494 0 -1.8332481 
		-23.35494 0 -1.8332481 -20.92347 0 -1.8332481 -20.92347 0 -1.8332481 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube4";
	rename -uid "766336C2-49D3-8F39-2334-318F80F0A5E4";
	setAttr ".t" -type "double3" 3590.1248550415039 417.13869965636974 -1500.688479037261 ;
	setAttr ".rp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "250BBD43-4DE1-D5B1-4BEC-998E29352A4F";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53125 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 0 -337.5 -20.92347 
		0 -337.5 -23.35494 0 -337.5 -20.92347 0 -337.5 -23.35494 0 0 -23.35494 0 0 -20.92347 
		0 0 -20.92347 0 0 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5";
	rename -uid "485BA7DE-488A-5517-A2EA-72AE7DED98A1";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -1950.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "FDF0F338-40CA-5388-1789-EE8801016A50";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 -11.826741 0 0 -11.826741 
		0 0 3.3589051 225 0 0 225 0 0 225 0 3.3589051 225;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube5";
	rename -uid "F98E8442-42E1-9F82-95A6-009CFDA7B940";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6";
	rename -uid "5DB8DA66-4CB8-E158-6C91-EE94D1EA6668";
	setAttr ".t" -type "double3" 3590.1248550415039 792.13870239257812 -1800.688479037261 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 1.1525534338695507 1 1 ;
	setAttr ".rp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 -42.138702392578125 0.68847903726100412 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "C2A3D567-4216-7232-3B3B-3DB822A2FAEA";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.24970842897891998 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[24:27]" -type "float3"  0 4.3835297 3.5539823 0 4.3835297 
		3.5539823 0 4.5986252 3.5539823 0 4.5986252 3.5539823;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "pCube6";
	rename -uid "7B9EC083-4360-366F-6C09-1BA34A748E90";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.209821417927742 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.354939 -37.5 -339.33325 
		43.105232 -37.5 -339.33325 -23.354939 37.5 -339.33325 43.105232 37.5 -339.33325 -23.354939 
		37.5 -8.3266727e-015 -23.354939 -37.5 8.3266727e-015 43.105232 -37.5 8.3266727e-015 
		43.105232 37.5 -8.3266727e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7";
	rename -uid "02B18907-4173-AAE1-2475-9F80987BA95F";
	setAttr ".t" -type "double3" 3590.1248550415039 42.138702392578125 -1050.688479037261 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1 1 ;
	setAttr ".rp" -type "double3" 9.8751449584961151 107.86129760742185 0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -149.99999999999994 -149.99999999999994 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" 1.1901590823981678e-013 0 0 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "E1A754EF-48A2-D746-8464-2E92012DC37C";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8";
	rename -uid "AE8707F2-4D82-3BE0-990A-1185A43A85EC";
	setAttr ".t" -type "double3" 3590.1248550415039 192.13870239257807 -1050.688479037261 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1 1 ;
	setAttr ".rp" -type "double3" 9.8751449584961151 107.86129760742185 0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -149.99999999999994 -149.99999999999994 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" 1.1901590823981678e-013 0 0 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "AF5DB7FE-4B59-0E8B-73D1-B39EF1DD0F5C";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9";
	rename -uid "082EA3BF-4174-89EE-FEF0-68B67860FF94";
	setAttr ".t" -type "double3" 3598.172710112297 299.31152096273883 -1092.1387023925781 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1 1 ;
	setAttr ".rp" -type "double3" 1.8272898877029675 107.86129760742185 0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -107.17281857016094 -108.54977664468285 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 0 0 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "F6DB09B5-459D-985B-4F56-408B87C6FD39";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10";
	rename -uid "58665097-43A0-38B6-6D98-7B80DE18E177";
	setAttr ".t" -type "double3" 3598.172710112297 449.31152096273865 -1092.1387023925781 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1.0000000000000002 1.0000000000000002 ;
	setAttr ".rp" -type "double3" 1.8272898877029684 107.86129760742187 0.68847903726097093 ;
	setAttr ".rpt" -type "double3" 0 -107.17281857016097 -108.54977664468288 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 2.8421709430404014e-014 1.1102230246251568e-016 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	rename -uid "4515A5DF-46EC-7D43-C0C1-7394FBC8A438";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11";
	rename -uid "AF40556A-45E0-3A19-102C-99A411913BE2";
	setAttr ".t" -type "double3" 3598.172710112297 599.31152096273854 -1092.1387023925781 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1.0000000000000004 1.0000000000000004 ;
	setAttr ".rp" -type "double3" 1.8272898877029693 107.86129760742189 0.68847903726097115 ;
	setAttr ".rpt" -type "double3" 0 -107.17281857016098 -108.54977664468289 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 4.263256414560603e-014 3.3306690738754711e-016 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "8B7260F1-445F-4385-5BA5-B8A848719C71";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13";
	rename -uid "A707C6F8-4E9D-717E-85E0-B796EA6D512F";
	setAttr ".t" -type "double3" 3590.1248550415039 42.138702392578125 -2250.688479037261 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 -1 1 ;
	setAttr ".rp" -type "double3" 9.8751449584961151 107.86129760742185 0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -149.99999999999994 -149.99999999999994 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" 1.1901590823981678e-013 0 0 ;
createNode mesh -n "pCubeShape13" -p "pCube13";
	rename -uid "4E783457-45DD-6B8A-95E7-D480CFBC98EB";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14";
	rename -uid "557B5BEE-4F73-F729-203B-ED8DABF37AC2";
	setAttr ".t" -type "double3" 3590.1248550415039 192.13870239257812 -2250.688479037261 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 -1 1 ;
	setAttr ".rp" -type "double3" 9.8751449584961151 107.86129760742185 0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -149.99999999999994 -149.99999999999994 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" 1.1901590823981678e-013 0 0 ;
createNode mesh -n "pCubeShape14" -p "pCube14";
	rename -uid "1E67F633-44F7-73DF-A9F6-43B49A1686B5";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15";
	rename -uid "D4FFF8CC-48DC-CCCB-BDC3-9E87768A3712";
	setAttr ".t" -type "double3" 3598.172710112297 299.31152096273911 -2507.8612976074219 ;
	setAttr ".r" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1 -1 ;
	setAttr ".rp" -type "double3" 1.8272898877029675 107.86129760742185 -0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -107.17281857016091 108.54977664468282 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 0 -1.3769580745219416 ;
createNode mesh -n "pCubeShape15" -p "pCube15";
	rename -uid "87A95BE8-4D73-5E1B-D000-CA8BED964458";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube16";
	rename -uid "DF65E423-4AB6-E060-30C2-1D98A330435F";
	setAttr ".t" -type "double3" 3598.172710112297 449.31152096273911 -2507.8612976074219 ;
	setAttr ".r" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 1 -1 ;
	setAttr ".rp" -type "double3" 1.8272898877029684 107.86129760742185 -0.68847903726097082 ;
	setAttr ".rpt" -type "double3" 0 -107.1728185701609 108.54977664468282 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 0 -1.3769580745219416 ;
createNode mesh -n "pCubeShape16" -p "pCube16";
	rename -uid "B709194C-4404-8796-36DC-3F965E45D9C5";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube18";
	rename -uid "6500CD09-4A7D-67FD-FB3D-94AA22B6AF9B";
	setAttr ".t" -type "double3" 3590.1248550415039 417.13869965636974 -1500.688479037261 ;
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" 41.889495849609375 -42.138702392578125 56.9384765625 ;
	setAttr ".sp" -type "double3" -22.139205932617188 -42.138702392578125 56.9384765625 ;
	setAttr ".spt" -type "double3" 64.028701782226563 0 0 ;
createNode mesh -n "pCubeShape18" -p "pCube18";
	rename -uid "BDA9B41F-4319-9B48-4352-0EB54B8BC069";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 0 -337.5 -20.92347 
		0 -337.5 -23.35494 0 -337.5 -20.92347 0 -337.5 -23.35494 0 0 -23.35494 0 0 -20.92347 
		0 0 -20.92347 0 0 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube19";
	rename -uid "E2A7C20C-467A-E3D6-1CE1-4E8297FBA42C";
	setAttr ".t" -type "double3" 3590.1248550415039 417.13869965636974 -2211.355227084136 ;
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" 41.889495849609375 -42.138702392578125 55.105226516723633 ;
	setAttr ".sp" -type "double3" -22.139205932617188 -42.138702392578125 55.105226516723633 ;
	setAttr ".spt" -type "double3" 64.028701782226563 0 0 ;
createNode mesh -n "pCubeShape19" -p "pCube19";
	rename -uid "EA1A80B3-4066-77E4-F420-C6A92FB0948C";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 0 -339.33325 -20.92347 
		0 -339.33325 -23.35494 0 -339.33325 -20.92347 0 -339.33325 -23.35494 0 -1.8332481 
		-23.35494 0 -1.8332481 -20.92347 0 -1.8332481 -20.92347 0 -1.8332481 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube23";
	rename -uid "C5109FD6-4B93-8919-28CA-B2AA0C825778";
	setAttr ".t" -type "double3" 3567.3740939328522 118.98028820303495 -1888.1946716308594 ;
	setAttr ".r" -type "double3" 81.484646090656383 0 0 ;
	setAttr ".s" -type "double3" 0.082175522335352538 1.0336419752132013 -0.54815443033363509 ;
	setAttr ".rp" -type "double3" 11.825124817147675 238.19467163085932 -31.019711796964682 ;
	setAttr ".rpt" -type "double3" 0 -207.17495983389915 269.21438342782733 ;
	setAttr ".sp" -type "double3" 63.906018693307558 238.19467163085937 56.0218505859375 ;
	setAttr ".spt" -type "double3" -52.080893876159784 -7.567280135845067e-013 -87.041562382902626 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "4674B0CF-4581-A6CE-0602-F1B70EF486F2";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube24";
	rename -uid "E1EE3AFC-4173-5945-FD96-A1B07FB1BCA5";
	setAttr ".t" -type "double3" 3567.3740939328522 118.98028820303986 -2188.1946716308635 ;
	setAttr ".r" -type "double3" 90.480430434151799 0 0 ;
	setAttr ".s" -type "double3" 0.082175522335352538 1.0336419752132013 -0.54815443033363509 ;
	setAttr ".rp" -type "double3" 11.825124817147675 238.19467163085932 -31.019711796964682 ;
	setAttr ".rpt" -type "double3" 0 -207.17495983389915 269.21438342782733 ;
	setAttr ".sp" -type "double3" 63.906018693307558 238.19467163085937 56.0218505859375 ;
	setAttr ".spt" -type "double3" -52.080893876159784 -7.567280135845067e-013 -87.041562382902626 ;
createNode mesh -n "pCubeShape24" -p "pCube24";
	rename -uid "612407F1-4C96-BDC4-BB4E-CD8911932084";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube25";
	rename -uid "763A79E2-4F49-3594-C01D-B1A81C7A9996";
	setAttr ".t" -type "double3" 3567.3740939328522 568.98028820303989 -2188.1946716308635 ;
	setAttr ".r" -type "double3" 83.091910103323144 0 0 ;
	setAttr ".s" -type "double3" 0.082175522335352538 1.0336419752132013 -0.54815443033363509 ;
	setAttr ".rp" -type "double3" 11.825124817147675 238.19467163085932 -31.019711796964682 ;
	setAttr ".rpt" -type "double3" 0 -207.17495983389915 269.21438342782733 ;
	setAttr ".sp" -type "double3" 63.906018693307558 238.19467163085937 56.0218505859375 ;
	setAttr ".spt" -type "double3" -52.080893876159784 -7.567280135845067e-013 -87.041562382902626 ;
createNode mesh -n "pCubeShape25" -p "pCube25";
	rename -uid "412DDC35-492C-3CF4-A649-0D92A60BF47F";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube26";
	rename -uid "9AC2706F-40F2-E49E-67DB-CEBCDE798A14";
	setAttr ".t" -type "double3" 3567.3740939328522 568.98028820303989 -1888.1946716308594 ;
	setAttr ".r" -type "double3" 100.06532146245381 0 0 ;
	setAttr ".s" -type "double3" 0.082175522335352538 1.0336419752132013 -0.54815443033363509 ;
	setAttr ".rp" -type "double3" 11.825124817147675 238.19467163085932 -31.019711796964682 ;
	setAttr ".rpt" -type "double3" 0 -207.17495983389915 269.21438342782733 ;
	setAttr ".sp" -type "double3" 63.906018693307558 238.19467163085937 56.0218505859375 ;
	setAttr ".spt" -type "double3" -52.080893876159784 -7.567280135845067e-013 -87.041562382902626 ;
createNode mesh -n "pCubeShape26" -p "pCube26";
	rename -uid "3F9C8203-4779-6B2E-F30A-42939CA2CE9E";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube27";
	rename -uid "EE69A4E5-44D7-332B-1BCB-9BB9F575B348";
	setAttr ".t" -type "double3" 3598.172710112297 599.31152096273854 -2292.1387023925781 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 0.18503929768959523 -1 1.0000000000000004 ;
	setAttr ".rp" -type "double3" 1.8272898877029693 107.86129760742189 0.68847903726097115 ;
	setAttr ".rpt" -type "double3" 0 -107.17281857016098 -108.54977664468289 ;
	setAttr ".sp" -type "double3" 9.8751449584960937 107.86129760742185 0.68847903726097082 ;
	setAttr ".spt" -type "double3" -8.0478550707930285 4.263256414560603e-014 3.3306690738754711e-016 ;
createNode mesh -n "pCubeShape27" -p "pCube27";
	rename -uid "C5202D90-4D44-32BD-B231-1CB282ED81F6";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.71875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt[0:8]" -type "float3"  -23.35494 525 -339.33325 
		43.105232 525 -339.33325 -23.35494 35.666748 -339.33325 43.105232 35.666748 -339.33325 
		-23.35494 35.666748 -7.8825835e-015 -23.35494 525 -1.1657342e-013 43.105232 525 -1.1657342e-013 
		43.105232 35.666748 -7.8825835e-015 0 0 0;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube28";
	rename -uid "AE5D6657-4908-6456-FF32-C98C96D3D2D8";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2025.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape28" -p "pCube28";
	rename -uid "E8C5D1E7-43BE-7031-DCEB-65AB2FB1A2B0";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 4.6712284 0 0 4.6712284 
		0 0 17.039484 225 0 0 225 0 0 225 0 17.039484 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube28";
	rename -uid "168E09FA-494C-D60A-6BAD-D3924AC2F694";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube29";
	rename -uid "4C7E346B-4A57-725D-B0F0-31849202FC7A";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2100.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape29" -p "pCube29";
	rename -uid "25E34E6B-400B-68BC-1CB1-EB8968699D73";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.28125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 19.723351 -2.2737368e-013 
		0 19.723351 -2.2737368e-013 0 29.380764 225 0 0 225 0 0 225 0 29.380764 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube29";
	rename -uid "3571520E-42C5-FCD3-AE39-E7BDBE04DE26";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube30";
	rename -uid "050A0FF2-4EA8-8830-2572-D88C17AAC237";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2175.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape30" -p "pCube30";
	rename -uid "7A991210-4BE3-0216-C76D-27B2FFD34685";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 29.380764 -2.2737368e-013 
		0 29.380764 -2.2737368e-013 0 33.614677 225 0 0 225 0 0 225 0 33.614677 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube30";
	rename -uid "E9A92739-4B01-770B-EA7D-3EB36056DC5A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube31";
	rename -uid "4F4E6B4F-4429-655C-CB65-0296152061F5";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2250.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape31" -p "pCube31";
	rename -uid "0AE69DD4-403E-93C2-2228-7687A3C024D0";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 33.614677 -2.2737368e-013 
		0 33.614677 -2.2737368e-013 0 29.380764 225 0 0 225 0 0 225 0 29.380764 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube31";
	rename -uid "3F34B96E-46BA-ABC7-F6E2-1FAF26E18C7A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube32";
	rename -uid "60CADB7E-409F-723D-87C9-FDA7A08E3A54";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2325.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape32" -p "pCube32";
	rename -uid "5080179F-4BE5-CF21-28C7-14BB2BC5D0B8";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.28125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 29.380764 -2.2737368e-013 
		0 29.380764 -2.2737368e-013 0 19.723351 225 0 0 225 0 0 225 0 19.723351 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube32";
	rename -uid "26FCFE22-44A0-5A17-B96F-ABA57843AE7F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube33";
	rename -uid "02344B29-4214-871D-6807-2080A81CC410";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2400.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape33" -p "pCube33";
	rename -uid "23FDE9DB-40B0-8225-B4E6-7DA7AA67A245";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.28125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 17.039484 0 0 17.039484 
		0 0 4.6712284 225 0 0 225 0 0 225 0 4.6712284 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube33";
	rename -uid "A28A3BD2-459C-B4BD-B221-709D7686E0FE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube34";
	rename -uid "17C6FEC8-4FDC-CF6D-24D1-7EB50BB69506";
	setAttr ".t" -type "double3" 3600 417.13869965636974 -2475.6884689331055 ;
	setAttr ".s" -type "double3" 1 1 0.92870049691709611 ;
	setAttr ".rp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
	setAttr ".sp" -type "double3" 0 -42.138702392578125 413.1884765625 ;
createNode mesh -n "pCubeShape34" -p "pCube34";
	rename -uid "118D4D33-4C7D-6C93-CC3A-EA9DE44EE26D";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[7:10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.28125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[2:7]" -type "float3"  0 3.3589051 0 0 3.3589051 
		0 0 -11.826741 225 0 0 225 0 0 225 0 -11.826741 225;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 150.6884613
		 -20.80078125 -417.13870239 150.6884613 20.80078125 -417.13870239 150.6884613 20.80078125 332.86129761 150.6884613;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 5 6 0 6 7 0 7 4 0 6 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 10 -5 1 5
		mu 0 4 9 6 2 3
		f 4 -7 8 11 -1
		mu 0 4 4 7 8 5
		f 4 -12 9 -6 -4
		mu 0 4 1 8 9 3
		f 4 7 6 2 4
		mu 0 4 6 7 0 2
		f 4 -8 -11 -10 -9
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube34";
	rename -uid "A451CFF6-44BC-6AAA-174E-10BF5D0CAF68";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[7]" "e[9:11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.34375 0.21875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 1 0.625 1 0.3125 0.3125 0.3125 0.4375 0.6875 0.4375 0.6875
		 0.3125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  0 0 149.99998 0 0 149.99998 
		0 0 149.99998 0 0 149.99998;
	setAttr -s 8 ".vt[0:7]"  -20.80078125 -417.13870239 450.68847656 20.80078125 -417.13870239 450.68847656
		 -20.80078125 332.86129761 450.68847656 20.80078125 332.86129761 450.68847656 -20.80078125 332.86129761 0.68847656
		 -20.80078125 -417.13870239 0.68847656 20.80078125 -417.13870239 0.68847656 20.80078125 332.86129761 0.68847656;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 2 4 0 3 7 0 5 0 0
		 4 5 0 6 1 0 5 6 0 6 7 0 7 4 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2
		f 4 1 5 11 -5
		mu 0 4 2 3 9 6
		f 4 9 8 -1 -7
		mu 0 4 7 8 5 4
		f 4 -9 10 -6 -4
		mu 0 4 1 8 9 3
		f 4 6 2 4 7
		mu 0 4 7 0 2 6
		f 4 -8 -12 -11 -10
		mu 0 4 7 6 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "522B92A4-4EED-6B4F-5EF9-FAB6C97629CE";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A2200769-4FA7-C436-19D0-FCB8802EAB0B";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "0A96DE8C-4C27-757A-40E4-ACBDD73D2507";
createNode displayLayerManager -n "layerManager";
	rename -uid "A8DF3E49-4A68-9312-6C62-F7ABDF17CCE5";
	setAttr ".cdl" 10;
	setAttr -s 12 ".dli[1:11]"  1 2 3 4 5 9 6 8 
		7 10 11;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "2B3707BB-4B57-A8C1-EB98-A2A607266ED9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "5C285BBF-43BF-5495-A64B-9FB34DD4B7A4";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "FEAB6308-4EDC-B769-A2B1-E39E9EB39044";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "69C9AF9C-42F1-870A-F64D-3E979CC07F7A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 699\n                -height 444\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 699\n            -height 444\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1404\n                -height 932\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1404\n            -height 932\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 699\n                -height 443\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 699\n            -height 443\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 1\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1404\n                -height 932\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 1\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1404\n            -height 932\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n"
		+ "            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n"
		+ "                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n"
		+ "                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n"
		+ "            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n"
		+ "                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n"
		+ "                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n"
		+ "                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n"
		+ "                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n"
		+ "            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n"
		+ "            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n"
		+ "                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n"
		+ "                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n"
		+ "            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n"
		+ "            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1404\\n    -height 932\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1404\\n    -height 932\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 300 -size 5000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "E30CE858-4BC8-2184-C47C-6BAA806009AD";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 80 -ast 1 -aet 250 ";
	setAttr ".st" 6;
createNode lambert -n "rockTexture";
	rename -uid "9C929B93-4E2C-7B3B-363F-0BA1AD676EE5";
	setAttr ".c" -type "float3" 0.61299998 0.43148965 0.19493401 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "96ED2578-4E46-52C9-E59E-42B1B5819E2D";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "093C52E1-4805-C040-CB34-58A7E7BDA488";
createNode lambert -n "woodmat";
	rename -uid "D1782CEE-4C6F-572E-11B4-C097F08A5B82";
	setAttr ".c" -type "float3" 0.259 0.23695971 0.10619001 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "4A0AD8E3-4CB0-FF35-293F-C9980FC9486D";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "6523AA9D-4300-10EA-03A1-60AAE992B2C3";
createNode displayLayer -n "Floor_Tiles";
	rename -uid "5809C7B1-43D2-1238-03B6-D19D3EAF729E";
	setAttr ".dt" 2;
	setAttr ".c" 6;
	setAttr ".do" 1;
createNode displayLayer -n "Wall_Tiles";
	rename -uid "A8F30E94-40AE-13F4-4CF0-14B72E883F51";
	setAttr ".dt" 2;
	setAttr ".c" 4;
	setAttr ".do" 2;
createNode animCurveTL -n "floor_tile43_translateX";
	rename -uid "CD43FA20-40DF-7F52-891B-169ED78C895A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 450;
createNode animCurveTL -n "floor_tile43_translateY";
	rename -uid "48AD534E-4F4D-D305-196D-EB8486E2FFBD";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 -9.9920072216264089e-014;
createNode animCurveTL -n "floor_tile43_translateZ";
	rename -uid "A0736E7B-4260-67B9-962E-2A8349A37C7E";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1350;
createNode animCurveTU -n "floor_tile43_visibility";
	rename -uid "3BF36D51-4929-011C-43DA-B5B48B73BD45";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "floor_tile43_rotateX";
	rename -uid "F8D5A095-4AB5-7217-282E-F0A512C2BD26";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 -90;
createNode animCurveTA -n "floor_tile43_rotateY";
	rename -uid "A0575FF3-4053-105F-E797-0B8F02DC0040";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "floor_tile43_rotateZ";
	rename -uid "A6A9204A-4732-3A5C-B52F-F996E0F47C80";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "floor_tile43_scaleX";
	rename -uid "2BB04350-47BC-AE25-EE97-18B2D6FDADC7";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "floor_tile43_scaleY";
	rename -uid "1CFCA1D5-47A0-7EC3-41AE-6B94A2BE738D";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "floor_tile43_scaleZ";
	rename -uid "7AB947CF-4AC9-665F-337A-DD825BEE349E";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode displayLayer -n "Door_Tiles";
	rename -uid "D9DA711D-4041-AE25-0534-CD933BDCCB35";
	setAttr ".dt" 2;
	setAttr ".c" 19;
	setAttr ".do" 3;
createNode objectSet -n "set1";
	rename -uid "A68177F5-46DF-4147-446D-64B17EECE304";
	setAttr ".ihi" 0;
	setAttr -s 27 ".dsm";
	setAttr -s 27 ".gn";
createNode groupId -n "groupId3";
	rename -uid "A1F29C8F-47A8-EF23-4D55-8C8B0269C29F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "8FCBBABF-47AE-F4B3-5351-149B598F2DFD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "801B4F83-4810-573C-2B21-D28CB8786A80";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "CA909780-4419-495E-F5C0-75AE30C15F5A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "19D9E286-4FF7-0857-2BFB-A1A76A1341F9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "FFA88852-4B5F-F500-ABF2-A3B9C5BA259A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "C2FE3AA4-41C5-DC10-9D84-5ABE15311DA4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "58E019A7-4140-9C2B-DD49-54821B1C94C6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "D696C058-4DA6-7C35-CA21-5C9DF811CD04";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "79224852-4A42-4F3F-C542-90ACFD08A4C1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "B0A2AEB9-4F6A-899C-2A32-31AA233762BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "19BF66CC-4FFF-7230-98ED-908114A30F47";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "2E991DF9-480D-62E1-C073-A7860BC71973";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "A29FB35E-4577-844C-1149-9A99A31313AE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "91B0D311-4817-FEA7-022B-58B8F8AD16B8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "EDA742DB-43EA-78AB-81BE-A8A3E4FA3705";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "F8FEB0E4-4F0F-9711-589A-76BB32EA4F9B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "8F5B9C90-4ECC-243E-30B6-358E7239BCC2";
	setAttr ".ihi" 0;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "BFAA2E8E-4BDC-C2BE-A2AE-E0BBE8550BBD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:6]" "e[8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3600 417.13869965636974 -1950.6884689331055 1;
	setAttr ".wt" 0.43114200234413147;
	setAttr ".re" 6;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 3;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId28";
	rename -uid "C2A4BC52-437A-3081-2632-B9B28A75AD43";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "B102C453-4E53-3F4F-8E1F-AB9C02165960";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[7]" "e[9:11]";
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "421956BD-4236-0970-329E-04A5A74F1EA4";
	setAttr ".ics" -type "componentList" 3 "e[30]" "e[32]" "e[34:35]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "86FFC9CC-4783-60CD-98F8-37BB8B9B9045";
	setAttr ".ics" -type "componentList" 3 "e[21]" "e[23]" "e[25:26]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "75B76FF5-4DA1-DE8B-10CF-3DAC53F2E6B7";
	setAttr ".ics" -type "componentList" 3 "e[13]" "e[15]" "e[17:18]";
	setAttr ".cv" yes;
createNode groupId -n "groupId29";
	rename -uid "4BAEB809-4645-87C5-300D-BE8C06EC78AC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId30";
	rename -uid "05B88C90-4A47-4833-5BE3-2CB9C44490BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "84AAA7CB-4CA1-3654-F28E-0493BD542E27";
	setAttr ".ihi" 0;
createNode groupId -n "groupId32";
	rename -uid "CB7E2D58-42BB-F333-E8DF-EE9806611256";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "5C42F208-4361-2C3F-01EC-72A8B2C4EF93";
	setAttr ".ihi" 0;
createNode groupId -n "groupId34";
	rename -uid "31532559-4383-D272-E951-BCABBD4033B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "CD61912C-4F03-ABFC-5D23-32AAE72700B1";
	setAttr ".ihi" 0;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "CC9DA462-4500-309E-EFA0-B986627D32DC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[2:3]" "e[7]" "e[10]";
	setAttr ".ix" -type "matrix" 1.1525534338695507 0 0 0 0 2.2204460492503131e-016 -1 0
		 0 1 2.2204460492503131e-016 0 3588.6183677681256 749.311520962739 -1842.1387023925781 1;
	setAttr ".wt" 0.20892606675624847;
	setAttr ".re" 2;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 6;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId36";
	rename -uid "79BEEF7C-4D0C-7C37-D66D-9F815E3477DC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "5B9E5245-41BE-EB90-9EE8-8D9FA1758E9F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[7]" "e[9:11]";
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "416EE0A6-49D5-8D0D-D032-A0AB61922A71";
	setAttr ".ics" -type "componentList" 12 "e[22]" "e[24]" "e[26:27]" "e[30]" "e[32]" "e[34:35]" "e[38]" "e[40]" "e[42:43]" "e[46]" "e[48]" "e[50:51]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "3DBCE8DE-445F-6157-BE91-189AA731FAC8";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[8]" -type "float3" 0 5.357132 0 ;
	setAttr ".tk[9]" -type "float3" 0 5.357132 -1.2212453e-015 ;
	setAttr ".tk[10]" -type "float3" 0 5.357132 -1.2212453e-015 ;
	setAttr ".tk[11]" -type "float3" 0 5.357132 0 ;
	setAttr ".tk[28]" -type "float3" 0 -5.3571496 0 ;
	setAttr ".tk[29]" -type "float3" 0 -5.3571496 1.2212453e-015 ;
	setAttr ".tk[30]" -type "float3" 0 -5.3571496 1.2212453e-015 ;
	setAttr ".tk[31]" -type "float3" 0 -5.3571496 0 ;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "7CF26FEA-47E3-326B-03B9-129B4FE7C306";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[13]" "e[20]" "e[23]" "e[25]";
	setAttr ".ix" -type "matrix" 1.1525534338695507 0 0 0 0 2.2204460492503131e-016 -1 0
		 0 1 2.2204460492503131e-016 0 3588.6183677681256 749.311520962739 -1842.1387023925781 1;
	setAttr ".wt" 0.57848107814788818;
	setAttr ".dr" no;
	setAttr ".re" 25;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "B39C7C9E-4FA6-5DB1-E5B0-90AACD747A87";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 -31.094046 ;
	setAttr ".tk[1]" -type "float3" 0 0 -31.094046 ;
	setAttr ".tk[2]" -type "float3" 0 0 -31.094046 ;
	setAttr ".tk[3]" -type "float3" 0 0 -31.094046 ;
	setAttr ".tk[8]" -type "float3" 0 19.230965 -6.2782321 ;
	setAttr ".tk[11]" -type "float3" 0 19.230965 -6.2782321 ;
	setAttr ".tk[12]" -type "float3" 0 -19.230965 -6.2782321 ;
	setAttr ".tk[15]" -type "float3" 0 -19.230965 -6.2782321 ;
	setAttr ".tk[16]" -type "float3" 0 28.229031 35.672382 ;
	setAttr ".tk[17]" -type "float3" 0 28.228996 35.672371 ;
	setAttr ".tk[18]" -type "float3" 0 11.864202 39.333252 ;
	setAttr ".tk[19]" -type "float3" 0 11.864202 39.333252 ;
	setAttr ".tk[20]" -type "float3" 0 -27.408791 35.885387 ;
	setAttr ".tk[21]" -type "float3" 0 -27.408791 35.885387 ;
	setAttr ".tk[22]" -type "float3" 0 -11.864201 39.333252 ;
	setAttr ".tk[23]" -type "float3" 0 -11.864201 39.333252 ;
createNode polySplit -n "polySplit1";
	rename -uid "CB75F3FB-48FB-8955-0B83-0280AC2B6270";
	setAttr -s 5 ".e[0:4]"  0.519593 0.480407 0.480407 0.480407 0.519593;
	setAttr -s 5 ".d[0:4]"  -2147483617 -2147483607 -2147483612 -2147483611 -2147483617;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 133 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".mcfr" 30;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr ".hwfr" 30;
connectAttr "Floor_Tiles.di" "floor_tile1.do";
connectAttr "Floor_Tiles.di" "floor_tile2.do";
connectAttr "Floor_Tiles.di" "floor_tile3.do";
connectAttr "Floor_Tiles.di" "floor_tile4.do";
connectAttr "Floor_Tiles.di" "floor_tile6.do";
connectAttr "Floor_Tiles.di" "floor_tile7.do";
connectAttr "Floor_Tiles.di" "floor_tile8.do";
connectAttr "Floor_Tiles.di" "floor_tile9.do";
connectAttr "Floor_Tiles.di" "floor_tile11.do";
connectAttr "Floor_Tiles.di" "floor_tile12.do";
connectAttr "Floor_Tiles.di" "floor_tile13.do";
connectAttr "Floor_Tiles.di" "floor_tile14.do";
connectAttr "Floor_Tiles.di" "floor_tile16.do";
connectAttr "Floor_Tiles.di" "floor_tile17.do";
connectAttr "Floor_Tiles.di" "floor_tile18.do";
connectAttr "Floor_Tiles.di" "floor_tile19.do";
connectAttr "Floor_Tiles.di" "floor_tile21.do";
connectAttr "Floor_Tiles.di" "floor_tile22.do";
connectAttr "Floor_Tiles.di" "floor_tile23.do";
connectAttr "Floor_Tiles.di" "floor_tile24.do";
connectAttr "Floor_Tiles.di" "floor_tile26.do";
connectAttr "Floor_Tiles.di" "floor_tile27.do";
connectAttr "Floor_Tiles.di" "floor_tile28.do";
connectAttr "Floor_Tiles.di" "floor_tile29.do";
connectAttr "Floor_Tiles.di" "floor_tile30.do";
connectAttr "Floor_Tiles.di" "floor_tile31.do";
connectAttr "Floor_Tiles.di" "floor_tile32.do";
connectAttr "Floor_Tiles.di" "floor_tile33.do";
connectAttr "Floor_Tiles.di" "floor_tile34.do";
connectAttr "Floor_Tiles.di" "floor_tile35.do";
connectAttr "Floor_Tiles.di" "floor_tile36.do";
connectAttr "Floor_Tiles.di" "floor_tile37.do";
connectAttr "Floor_Tiles.di" "floor_tile38.do";
connectAttr "Floor_Tiles.di" "floor_tile39.do";
connectAttr "Floor_Tiles.di" "floor_tile40.do";
connectAttr "Floor_Tiles.di" "floor_tile41.do";
connectAttr "Wall_Tiles.di" "floor_tile42.do";
connectAttr "floor_tile43_translateX.o" "floor_tile43.tx";
connectAttr "floor_tile43_translateY.o" "floor_tile43.ty";
connectAttr "floor_tile43_translateZ.o" "floor_tile43.tz";
connectAttr "Wall_Tiles.di" "floor_tile43.do";
connectAttr "floor_tile43_visibility.o" "floor_tile43.v";
connectAttr "floor_tile43_rotateX.o" "floor_tile43.rx";
connectAttr "floor_tile43_rotateY.o" "floor_tile43.ry";
connectAttr "floor_tile43_rotateZ.o" "floor_tile43.rz";
connectAttr "floor_tile43_scaleX.o" "floor_tile43.sx";
connectAttr "floor_tile43_scaleY.o" "floor_tile43.sy";
connectAttr "floor_tile43_scaleZ.o" "floor_tile43.sz";
connectAttr "Wall_Tiles.di" "floor_tile44.do";
connectAttr "Wall_Tiles.di" "floor_tile45.do";
connectAttr "Wall_Tiles.di" "floor_tile46.do";
connectAttr "Wall_Tiles.di" "floor_tile47.do";
connectAttr "Wall_Tiles.di" "floor_tile48.do";
connectAttr "Wall_Tiles.di" "floor_tile49.do";
connectAttr "Wall_Tiles.di" "floor_tile50.do";
connectAttr "Wall_Tiles.di" "floor_tile51.do";
connectAttr "Wall_Tiles.di" "floor_tile52.do";
connectAttr "Wall_Tiles.di" "floor_tile53.do";
connectAttr "Wall_Tiles.di" "floor_tile54.do";
connectAttr "Wall_Tiles.di" "floor_tile55.do";
connectAttr "Wall_Tiles.di" "floor_tile56.do";
connectAttr "Wall_Tiles.di" "floor_tile57.do";
connectAttr "Wall_Tiles.di" "floor_tile58.do";
connectAttr "Wall_Tiles.di" "floor_tile59.do";
connectAttr "Wall_Tiles.di" "floor_tile60.do";
connectAttr "Wall_Tiles.di" "floor_tile61.do";
connectAttr "Wall_Tiles.di" "floor_tile62.do";
connectAttr "Wall_Tiles.di" "floor_tile63.do";
connectAttr "Wall_Tiles.di" "floor_tile64.do";
connectAttr "Wall_Tiles.di" "floor_tile65.do";
connectAttr "Wall_Tiles.di" "floor_tile66.do";
connectAttr "Wall_Tiles.di" "floor_tile67.do";
connectAttr "Wall_Tiles.di" "floor_tile68.do";
connectAttr "Wall_Tiles.di" "floor_tile69.do";
connectAttr "Wall_Tiles.di" "floor_tile70.do";
connectAttr "Wall_Tiles.di" "floor_tile71.do";
connectAttr "Wall_Tiles.di" "floor_tile72.do";
connectAttr "Wall_Tiles.di" "floor_tile73.do";
connectAttr "Door_Tiles.di" "floor_tile74.do";
connectAttr "Door_Tiles.di" "floor_tile75.do";
connectAttr "Wall_Tiles.di" "floor_tile76.do";
connectAttr "Wall_Tiles.di" "floor_tile77.do";
connectAttr "Wall_Tiles.di" "floor_tile78.do";
connectAttr "Wall_Tiles.di" "floor_tile79.do";
connectAttr "Wall_Tiles.di" "floor_tile80.do";
connectAttr "Wall_Tiles.di" "floor_tile81.do";
connectAttr "Wall_Tiles.di" "floor_tile82.do";
connectAttr "Wall_Tiles.di" "floor_tile83.do";
connectAttr "Wall_Tiles.di" "floor_tile84.do";
connectAttr "Wall_Tiles.di" "floor_tile85.do";
connectAttr "Wall_Tiles.di" "floor_tile86.do";
connectAttr "Wall_Tiles.di" "floor_tile87.do";
connectAttr "Wall_Tiles.di" "floor_tile88.do";
connectAttr "Wall_Tiles.di" "floor_tile89.do";
connectAttr "Wall_Tiles.di" "floor_tile90.do";
connectAttr "Wall_Tiles.di" "floor_tile91.do";
connectAttr "Wall_Tiles.di" "floor_tile92.do";
connectAttr "Wall_Tiles.di" "floor_tile93.do";
connectAttr "Wall_Tiles.di" "floor_tile94.do";
connectAttr "Wall_Tiles.di" "floor_tile95.do";
connectAttr "Wall_Tiles.di" "floor_tile96.do";
connectAttr "Wall_Tiles.di" "floor_tile97.do";
connectAttr "Wall_Tiles.di" "floor_tile98.do";
connectAttr "Wall_Tiles.di" "floor_tile99.do";
connectAttr "Wall_Tiles.di" "floor_tile100.do";
connectAttr "Wall_Tiles.di" "floor_tile101.do";
connectAttr "Wall_Tiles.di" "floor_tile102.do";
connectAttr "Wall_Tiles.di" "floor_tile103.do";
connectAttr "Wall_Tiles.di" "floor_tile104.do";
connectAttr "Wall_Tiles.di" "floor_tile105.do";
connectAttr "Wall_Tiles.di" "floor_tile106.do";
connectAttr "Wall_Tiles.di" "floor_tile107.do";
connectAttr "Wall_Tiles.di" "floor_tile108.do";
connectAttr "Wall_Tiles.di" "floor_tile109.do";
connectAttr "Wall_Tiles.di" "floor_tile110.do";
connectAttr "Wall_Tiles.di" "floor_tile111.do";
connectAttr "groupId3.id" "pCubeShape3.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape4.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupId28.id" "pCubeShape5.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "polyDelEdge3.out" "pCubeShape5.i";
connectAttr "groupId36.id" "pCubeShape6.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape6.iog.og[0].gco";
connectAttr "polySplit1.out" "pCubeShape6.i";
connectAttr "groupId7.id" "pCubeShape7.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape7.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape8.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupId9.id" "pCubeShape9.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape9.iog.og[0].gco";
connectAttr "groupId10.id" "pCubeShape10.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape10.iog.og[0].gco";
connectAttr "groupId11.id" "pCubeShape11.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape11.iog.og[0].gco";
connectAttr "groupId13.id" "pCubeShape13.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape13.iog.og[0].gco";
connectAttr "groupId14.id" "pCubeShape14.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape14.iog.og[0].gco";
connectAttr "groupId15.id" "pCubeShape15.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape15.iog.og[0].gco";
connectAttr "groupId16.id" "pCubeShape16.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape16.iog.og[0].gco";
connectAttr "groupId18.id" "pCubeShape18.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape18.iog.og[0].gco";
connectAttr "groupId19.id" "pCubeShape19.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape19.iog.og[0].gco";
connectAttr "groupId23.id" "pCubeShape23.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape23.iog.og[0].gco";
connectAttr "groupId24.id" "pCubeShape24.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape24.iog.og[0].gco";
connectAttr "groupId25.id" "pCubeShape25.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape25.iog.og[0].gco";
connectAttr "groupId26.id" "pCubeShape26.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape26.iog.og[0].gco";
connectAttr "groupId27.id" "pCubeShape27.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape27.iog.og[0].gco";
connectAttr "groupId29.id" "pCubeShape28.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape28.iog.og[0].gco";
connectAttr "groupId30.id" "pCubeShape29.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape29.iog.og[0].gco";
connectAttr "groupId31.id" "pCubeShape30.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape30.iog.og[0].gco";
connectAttr "groupId32.id" "pCubeShape31.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape31.iog.og[0].gco";
connectAttr "groupId33.id" "pCubeShape32.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape32.iog.og[0].gco";
connectAttr "groupId34.id" "pCubeShape33.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape33.iog.og[0].gco";
connectAttr "groupId35.id" "pCubeShape34.iog.og[0].gid";
connectAttr "set1.mwc" "pCubeShape34.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rockTexture.oc" "lambert2SG.ss";
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "rockTexture.msg" "materialInfo1.m";
connectAttr "woodmat.oc" "lambert3SG.ss";
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "woodmat.msg" "materialInfo2.m";
connectAttr "layerManager.dli[9]" "Floor_Tiles.id";
connectAttr "layerManager.dli[10]" "Wall_Tiles.id";
connectAttr "layerManager.dli[11]" "Door_Tiles.id";
connectAttr "groupId3.msg" "set1.gn" -na;
connectAttr "groupId4.msg" "set1.gn" -na;
connectAttr "groupId7.msg" "set1.gn" -na;
connectAttr "groupId8.msg" "set1.gn" -na;
connectAttr "groupId9.msg" "set1.gn" -na;
connectAttr "groupId10.msg" "set1.gn" -na;
connectAttr "groupId11.msg" "set1.gn" -na;
connectAttr "groupId13.msg" "set1.gn" -na;
connectAttr "groupId14.msg" "set1.gn" -na;
connectAttr "groupId15.msg" "set1.gn" -na;
connectAttr "groupId16.msg" "set1.gn" -na;
connectAttr "groupId18.msg" "set1.gn" -na;
connectAttr "groupId19.msg" "set1.gn" -na;
connectAttr "groupId23.msg" "set1.gn" -na;
connectAttr "groupId24.msg" "set1.gn" -na;
connectAttr "groupId25.msg" "set1.gn" -na;
connectAttr "groupId26.msg" "set1.gn" -na;
connectAttr "groupId27.msg" "set1.gn" -na;
connectAttr "groupId28.msg" "set1.gn" -na;
connectAttr "groupId29.msg" "set1.gn" -na;
connectAttr "groupId30.msg" "set1.gn" -na;
connectAttr "groupId31.msg" "set1.gn" -na;
connectAttr "groupId32.msg" "set1.gn" -na;
connectAttr "groupId33.msg" "set1.gn" -na;
connectAttr "groupId34.msg" "set1.gn" -na;
connectAttr "groupId35.msg" "set1.gn" -na;
connectAttr "groupId36.msg" "set1.gn" -na;
connectAttr "pCubeShape3.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape7.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape8.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape9.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape10.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape11.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape13.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape14.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape15.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape16.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape18.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape19.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape23.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape24.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape25.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape26.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape27.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape28.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape29.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape30.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape31.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape32.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape33.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape34.iog.og[0]" "set1.dsm" -na;
connectAttr "pCubeShape6.iog.og[0]" "set1.dsm" -na;
connectAttr "groupParts2.og" "polySplitRing2.ip";
connectAttr "pCubeShape5.wm" "polySplitRing2.mp";
connectAttr "|pCube5|polySurfaceShape1.o" "groupParts2.ig";
connectAttr "groupId28.id" "groupParts2.gi";
connectAttr "polySplitRing2.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "polyDelEdge2.out" "polyDelEdge3.ip";
connectAttr "groupParts3.og" "polySplitRing3.ip";
connectAttr "pCubeShape6.wm" "polySplitRing3.mp";
connectAttr "polySurfaceShape2.o" "groupParts3.ig";
connectAttr "groupId36.id" "groupParts3.gi";
connectAttr "polyTweak1.out" "polyDelEdge4.ip";
connectAttr "polySplitRing3.out" "polyTweak1.ip";
connectAttr "polyDelEdge4.out" "polySplitRing4.ip";
connectAttr "pCubeShape6.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit1.ip";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "rockTexture.msg" ":defaultShaderList1.s" -na;
connectAttr "woodmat.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "floor_tile1Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile2Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile3Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile4Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile6Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile7Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile8Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile9Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile11Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile12Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile13Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile14Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile16Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile17Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile18Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile19Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile21Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile22Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile23Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile24Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile26Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile27Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile28Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile29Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile30Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile31Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile32Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile33Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile34Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile35Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile36Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile37Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile38Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile39Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile40Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile41Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile42Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile43Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile44Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile45Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile46Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile47Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile48Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile49Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile50Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile51Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile52Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile53Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile54Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile55Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile56Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile57Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile58Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile59Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile60Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile61Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile62Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile63Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile64Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile65Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile66Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile67Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile68Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile69Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile70Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile71Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile72Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile73Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile74Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile75Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile76Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile77Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile78Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile79Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile80Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile81Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile82Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile83Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile84Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile85Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile86Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile87Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile88Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile89Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile90Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile91Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile92Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile93Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile94Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile95Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile96Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile97Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile98Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile99Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile100Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile101Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile102Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile103Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile104Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile105Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile106Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile107Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile108Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile109Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile110Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "floor_tile111Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape14.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape16.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape24.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape25.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape26.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape27.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape28.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape29.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape30.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape31.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape32.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape33.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape34.iog" ":initialShadingGroup.dsm" -na;
// End of cactusEnvironment11restart.ma
