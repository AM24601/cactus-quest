//Maya ASCII 2017 scene
//Name: scene5.ma
//Last modified: Sat, Sep 17, 2016 02:40:33 PM
//Codeset: 1252
requires maya "2017";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "8C9A702E-4A25-E321-C6BF-A0A7165F1EA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -952.05919492554119 -1545.5310950781191 390.76438468968951 ;
	setAttr ".r" -type "double3" 76.882924915818194 0 24079.194903665397 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D54102B7-4615-DD35-5E2F-EEAFEF4B3B81";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 2635.951808657469;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 2200 300 103.75 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "49D2A323-4D5D-CA14-B1FF-A193FBE60DBF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 944.53592141791455 323.44993083618016 1292.4207738425698 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "74CFCAA5-4EE9-DC2B-4118-019EC0822F4B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1292.4207738425698;
	setAttr ".ow" 649.06117938160764;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 500.00000000000006 0 0 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "3E8FAFD9-4B95-A61B-9B6B-C68BB06C0E1C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 950.41543962735693 -1000.1000000000003 124.37646069062515 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "05574B54-4206-1F4F-61DC-34A1E67DA07A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 992.78889222430087;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "C8D6090B-4386-2CA8-4378-B8A6EDE7522E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1000000000003 587.17148319319642 212.50530143195704 ;
	setAttr ".r" -type "double3" 90 1.2722218725854067e-014 89.999999999999986 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "1AAD4DEF-48F8-AFAA-8086-C78D0A329991";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 963.40917048241408;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube23";
	rename -uid "C9AAB5E5-4C00-22CC-242D-8DAA6525175C";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 399.99999745686853 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "1D54CD3D-4D93-B10D-81F9-4B85F6A84164";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr -av ".iog[0].og[0].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube23";
	rename -uid "C4D7CED8-45FF-9F80-4E59-8E9C067D70A6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube25";
	rename -uid "2342A84D-40B1-0DD1-9381-9D83D5B92BF2";
	setAttr ".t" -type "double3" -200 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape25" -p "pCube25";
	rename -uid "C3F882A4-4619-562A-CA46-FA875C9B3E31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube26";
	rename -uid "5B9CC957-4B72-1026-9709-CABB465C39ED";
	setAttr ".t" -type "double3" 200 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape26" -p "pCube26";
	rename -uid "ED3B155A-49DD-A53C-26F0-43A87E942FB6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube27";
	rename -uid "CB5C42F4-49B5-FE0D-4199-EBBD9CEF13FA";
	setAttr ".t" -type "double3" -200 -200.37467465211478 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape27" -p "pCube27";
	rename -uid "72E0339E-4906-840D-DA69-478E3C4EE628";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube28";
	rename -uid "1F458384-4EA7-64EC-9C47-00BEE607A5A9";
	setAttr ".t" -type "double3" 200 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape28" -p "pCube28";
	rename -uid "85083C01-40A2-721C-D39E-F3913020627A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube29";
	rename -uid "4CC3EF9A-497D-9849-16C4-EDBFBF8FA00A";
	setAttr ".t" -type "double3" -200 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape29" -p "pCube29";
	rename -uid "1F1BA746-4EF7-74D4-2748-759CD551158A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube30";
	rename -uid "6D7D19D5-4262-212D-5B9E-F380269AE3D5";
	setAttr ".t" -type "double3" 200 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape30" -p "pCube30";
	rename -uid "BEC8F1D1-43B7-4195-481C-26AC734671E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "pCube30";
	rename -uid "1366D246-4DA4-6BDF-9B06-D1B477CD55D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube31";
	rename -uid "CCEA3AAD-40FA-E360-8AF0-A29DC19EFA91";
	setAttr ".t" -type "double3" -200 400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape31" -p "pCube31";
	rename -uid "66BC1F02-47B7-631F-560A-6C96ED0116D0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube32";
	rename -uid "535EA409-4C5C-D4F6-1629-089663BE6F5C";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -2.5431314725210541e-006 
		-0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape32" -p "pCube32";
	rename -uid "F70DB8EC-4CC7-35E5-5C40-7EACFF0967A3";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube32";
	rename -uid "70C7550F-4C99-8B9B-9A42-0082D19D64AE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube33";
	rename -uid "AAC46097-482B-9576-98B3-13B9D84B164F";
	setAttr ".t" -type "double3" -200 100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape33" -p "pCube33";
	rename -uid "7A019566-4105-FC57-2F58-3BA60ECF1DC3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube34";
	rename -uid "7429FB78-4973-F7FC-2DC2-C8995501157E";
	setAttr ".t" -type "double3" 300.00001217297995 200.00000000000045 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
	setAttr ".rpt" -type "double3" -100.00001217297996 -100.00001217298063 0 ;
	setAttr ".sp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
createNode mesh -n "pCubeShape34" -p "pCube34";
	rename -uid "F365D67B-47FB-D6D1-4143-95BB0E6C662D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape3" -p "pCube34";
	rename -uid "84593712-49C2-EE98-E2FC-C185A6B3BE8D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[0:5]" -type "float3"  0 0 88.75 0 0 88.75 0 0 -11.25 
		0 0 -11.25 0 0 0 0 0 0;
	setAttr -s 4 ".vt[0:3]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube35";
	rename -uid "6AAD1C66-4E1D-7A52-25ED-829E4F763DC7";
	setAttr ".t" -type "double3" -200 -100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape35" -p "pCube35";
	rename -uid "0C0F421D-4590-997E-FE69-F49F2B719631";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube36";
	rename -uid "D0B99DA6-46EE-FC76-EC18-1EA822E9A71C";
	setAttr ".t" -type "double3" 200 -100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape36" -p "pCube36";
	rename -uid "07451007-43E5-9250-CE74-3983D10DAD8E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube37";
	rename -uid "E82024EB-4E50-04B4-06A5-A299719EFB62";
	setAttr ".t" -type "double3" -200 300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape37" -p "pCube37";
	rename -uid "960B7A2B-4B07-7C2D-2DDE-EC8F81C832FF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube38";
	rename -uid "90B8DBA8-4DFD-B8A0-55D7-6B80D0C7768F";
	setAttr ".t" -type "double3" -200 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape38" -p "pCube38";
	rename -uid "864F83D8-4C03-DAB2-3CA5-DAA091E90FA7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube39";
	rename -uid "6292D38B-4BFE-3D81-626F-E8B292F91D1E";
	setAttr ".t" -type "double3" 0 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape39" -p "pCube39";
	rename -uid "E58EA463-4EF3-D3C7-E9AC-41B78AADAA11";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube40";
	rename -uid "8E4A4334-4D34-C2E8-05C1-4FBE326B8B7A";
	setAttr ".t" -type "double3" 200 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape40" -p "pCube40";
	rename -uid "0BB755A4-413D-55B1-7B3A-CCA75380619B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube41";
	rename -uid "70036698-4E1B-D496-3762-5A80DDAC26E7";
	setAttr ".t" -type "double3" -200 500.00000000000006 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape41" -p "pCube41";
	rename -uid "C0AC5543-437C-8A58-31F9-7BB85E1352A9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube42";
	rename -uid "AA6328FE-42D5-1F21-5A02-FB91222A2334";
	setAttr ".t" -type "double3" -100 600 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape42" -p "pCube42";
	rename -uid "F54E3ED2-4BF6-CF8F-F1A6-88B93DC6493B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube43";
	rename -uid "BC03C7F2-4F58-024C-F621-3C9F0C0FCBEC";
	setAttr ".t" -type "double3" 100 600 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape43" -p "pCube43";
	rename -uid "02802235-40FE-75DF-04F6-43A72313584D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube44";
	rename -uid "D5397314-4D81-ECB2-B399-738F6D1990B6";
	setAttr ".t" -type "double3" 399.99999745686853 399.99999745686853 -0.5 ;
	setAttr ".r" -type "double3" 0 -35 0 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape44" -p "pCube44";
	rename -uid "BA4C526C-4052-2708-0A5C-CCB266218491";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 2 "e[0:2]" "e[5]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "e[3:4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 6 ".uvst[0].uvsp[0:5]" -type "float2" 0.375 0 0.375 0.25
		 0.5 0.25 0.5 0 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[2]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr ".pt[3]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr ".pt[4]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr -s 6 ".vt[0:5]"  -200 -200 0.5 -200 200 0.5 3.44259357 200 0.47166538
		 3.44259357 -200 0.47166538 3.44259357 3.9790393e-013 0.47166538 -200 0 0.5;
	setAttr -s 7 ".ed[0:6]"  0 3 0 1 2 0 0 5 0 2 4 0 4 3 0 5 1 0 4 5 1;
	setAttr -s 2 -ch 8 ".fc[0:1]" -type "polyFaces" 
		f 4 0 -5 6 -3
		mu 0 4 0 3 4 5
		f 4 -7 -4 -2 -6
		mu 0 4 5 4 2 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube44";
	rename -uid "49CD582F-4E31-80FE-C95A-F7ADC13CC1A2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube47";
	rename -uid "27521F4B-4478-4DEE-B6D0-8799C0F0E77A";
	setAttr ".t" -type "double3" 400 600.00000000000023 108.2499999999999 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape47" -p "pCube47";
	rename -uid "48FC8B69-4E83-35CB-B692-B5A86E5B9C75";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube48";
	rename -uid "C65C8EDA-4C17-B456-F641-0AA5DECE5FA3";
	setAttr ".t" -type "double3" 400 200 108.2499999999999 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape48" -p "pCube48";
	rename -uid "7DFC00C3-46DC-CC9F-9CC9-1A9831406D63";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube49";
	rename -uid "8384BC55-4385-927A-3F01-76BA876A9DEC";
	setAttr ".t" -type "double3" 300 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "polySurfaceShape2" -p "pCube49";
	rename -uid "454DE9F3-41F5-39FA-9737-6BB4BA3AC1C0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform1" -p "pCube49";
	rename -uid "20667E40-407D-792F-2236-F9B61923EE91";
	setAttr ".v" no;
createNode mesh -n "pCubeShape49" -p "transform1";
	rename -uid "8BFF920C-4084-06C5-0395-5D96B07BA5C8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59375 0.8125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.4375 0.5 0.5 0.5
		 0.75 0.5 0.8125 0.5625 0 0.5625 1 0.5625 0.25 0.5625 0.4375 0.5625 0.5 0.5625 0.75
		 0.5625 0.8125 0.625 0.90625 0.71875 0 0.5625 0.90625 0.5 0.90625 0.28125 0 0.375
		 0.90625 0.28125 0.25 0.375 0.34375 0.5 0.34375 0.5625 0.34375 0.625 0.34375 0.71875
		 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[31]" -type "float3" 0 0 -5.4166679 ;
	setAttr -s 32 ".vt[0:31]"  -16.5 -16.5 25.083335876 16.5 -16.5 25.083335876
		 -16.5 16.5 25.083335876 16.5 16.5 25.083335876 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5
		 16.5 16.5 -16.5 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25
		 -6.45272827 -16.5 25.083335876 -6.45272827 16.5 25.083335876 -6.45272827 16.5 -8.25
		 -6.45272827 16.5 -16.5 -6.45272827 -16.5 -16.5 -6.45272827 -16.5 -8.25 7.5 -16.5 25.083335876
		 7.5 16.5 25.083335876 7.5 16.5 -8.25 7.5 16.5 -16.5 7.5 -16.5 -16.5 7.5 -16.5 -8.25
		 16.5 -16.5 8.41666794 7.5 -16.5 8.41666794 -6.45272827 -16.5 8.41666794 -16.5 -16.5 8.41666794
		 -16.5 16.5 8.41666794 -6.45272827 16.5 8.41666794 7.5 16.5 8.41666794 16.5 16.5 8.41666794;
	setAttr -s 60 ".ed[0:59]"  0 12 0 2 13 0 4 14 1 6 15 0 8 16 0 10 17 1
		 0 2 0 1 3 0 3 31 0 2 28 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 24 0 10 27 0 11 5 1
		 10 4 1 12 18 0 13 19 0 12 13 1 14 20 1 13 29 1 15 21 0 14 15 1 16 22 0 15 16 1 17 23 1
		 16 17 1 17 26 1 18 1 0 19 3 0 18 19 1 20 5 1 19 30 1 21 7 0 20 21 1 22 9 0 21 22 1
		 23 11 1 22 23 1 23 25 1 24 1 0 25 18 1 24 25 1 26 12 1 25 26 1 27 0 0 26 27 1 28 4 0
		 27 28 1 29 14 1 28 29 1 30 20 1 29 30 1 31 5 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 22 -2 -7
		mu 0 4 0 22 24 2
		f 4 -3 -52 54 53
		mu 0 4 25 4 43 44
		f 4 2 26 -4 -11
		mu 0 4 4 25 26 6
		f 4 3 28 -5 -13
		mu 0 4 6 26 27 8
		f 4 4 30 -6 -15
		mu 0 4 8 27 28 10
		f 4 31 50 -18 5
		mu 0 4 28 39 41 10
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -58 59 -17 18
		mu 0 4 17 47 37 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 51 -20 17 52
		mu 0 4 42 21 19 40
		f 4 20 34 -22 -23
		mu 0 4 22 29 31 24
		f 4 -24 -54 56 55
		mu 0 4 32 25 44 45
		f 4 -27 23 38 -26
		mu 0 4 26 25 32 33
		f 4 -29 25 40 -28
		mu 0 4 27 26 33 34
		f 4 -31 27 42 -30
		mu 0 4 28 27 34 35
		f 4 43 48 -32 29
		mu 0 4 35 38 39 28
		f 4 32 7 -34 -35
		mu 0 4 29 1 3 31
		f 4 -36 -56 58 57
		mu 0 4 5 32 45 46
		f 4 -39 35 11 -38
		mu 0 4 33 32 5 7
		f 4 -41 37 13 -40
		mu 0 4 34 33 7 9
		f 4 -43 39 15 -42
		mu 0 4 35 34 9 11
		f 4 46 -44 41 16
		mu 0 4 36 38 35 11
		f 4 -33 -46 -47 44
		mu 0 4 13 30 38 36
		f 4 -49 45 -21 -48
		mu 0 4 39 38 30 23
		f 4 -51 47 -1 -50
		mu 0 4 41 39 23 12
		f 4 9 -53 49 6
		mu 0 4 2 42 40 0
		f 4 -55 -10 1 24
		mu 0 4 44 43 2 24
		f 4 -57 -25 21 36
		mu 0 4 45 44 24 31
		f 4 -59 -37 33 8
		mu 0 4 46 45 31 3
		f 4 -60 -9 -8 -45
		mu 0 4 37 47 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube50";
	rename -uid "F65AE577-410B-CE90-4F4B-C2B562C86782";
	setAttr ".t" -type "double3" 300 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "polySurfaceShape2" -p "pCube50";
	rename -uid "F09072D3-4516-322B-F735-519CBF7D8D20";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform2" -p "pCube50";
	rename -uid "B22A5369-4E55-65F0-F271-74A148E38C98";
	setAttr ".v" no;
createNode mesh -n "pCubeShape50" -p "transform2";
	rename -uid "E35C2843-4C92-9754-E412-5492FCAD8726";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59375 0.8125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.4375 0.5 0.5 0.5
		 0.75 0.5 0.8125 0.5625 0 0.5625 1 0.5625 0.25 0.5625 0.4375 0.5625 0.5 0.5625 0.75
		 0.5625 0.8125 0.625 0.90625 0.71875 0 0.5625 0.90625 0.5 0.90625 0.28125 0 0.375
		 0.90625 0.28125 0.25 0.375 0.34375 0.5 0.34375 0.5625 0.34375 0.625 0.34375 0.71875
		 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[31]" -type "float3" 0 0 -5.4166679 ;
	setAttr -s 32 ".vt[0:31]"  -16.5 -16.5 25.083335876 16.5 -16.5 25.083335876
		 -16.5 16.5 25.083335876 16.5 16.5 25.083335876 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5
		 16.5 16.5 -16.5 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25
		 -6.45272827 -16.5 25.083335876 -6.45272827 16.5 25.083335876 -6.45272827 16.5 -8.25
		 -6.45272827 16.5 -16.5 -6.45272827 -16.5 -16.5 -6.45272827 -16.5 -8.25 7.5 -16.5 25.083335876
		 7.5 16.5 25.083335876 7.5 16.5 -8.25 7.5 16.5 -16.5 7.5 -16.5 -16.5 7.5 -16.5 -8.25
		 16.5 -16.5 8.41666794 7.5 -16.5 8.41666794 -6.45272827 -16.5 8.41666794 -16.5 -16.5 8.41666794
		 -16.5 16.5 8.41666794 -6.45272827 16.5 8.41666794 7.5 16.5 8.41666794 16.5 16.5 8.41666794;
	setAttr -s 60 ".ed[0:59]"  0 12 0 2 13 0 4 14 1 6 15 0 8 16 0 10 17 1
		 0 2 0 1 3 0 3 31 0 2 28 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 24 0 10 27 0 11 5 1
		 10 4 1 12 18 0 13 19 0 12 13 1 14 20 1 13 29 1 15 21 0 14 15 1 16 22 0 15 16 1 17 23 1
		 16 17 1 17 26 1 18 1 0 19 3 0 18 19 1 20 5 1 19 30 1 21 7 0 20 21 1 22 9 0 21 22 1
		 23 11 1 22 23 1 23 25 1 24 1 0 25 18 1 24 25 1 26 12 1 25 26 1 27 0 0 26 27 1 28 4 0
		 27 28 1 29 14 1 28 29 1 30 20 1 29 30 1 31 5 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 22 -2 -7
		mu 0 4 0 22 24 2
		f 4 -3 -52 54 53
		mu 0 4 25 4 43 44
		f 4 2 26 -4 -11
		mu 0 4 4 25 26 6
		f 4 3 28 -5 -13
		mu 0 4 6 26 27 8
		f 4 4 30 -6 -15
		mu 0 4 8 27 28 10
		f 4 31 50 -18 5
		mu 0 4 28 39 41 10
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -58 59 -17 18
		mu 0 4 17 47 37 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 51 -20 17 52
		mu 0 4 42 21 19 40
		f 4 20 34 -22 -23
		mu 0 4 22 29 31 24
		f 4 -24 -54 56 55
		mu 0 4 32 25 44 45
		f 4 -27 23 38 -26
		mu 0 4 26 25 32 33
		f 4 -29 25 40 -28
		mu 0 4 27 26 33 34
		f 4 -31 27 42 -30
		mu 0 4 28 27 34 35
		f 4 43 48 -32 29
		mu 0 4 35 38 39 28
		f 4 32 7 -34 -35
		mu 0 4 29 1 3 31
		f 4 -36 -56 58 57
		mu 0 4 5 32 45 46
		f 4 -39 35 11 -38
		mu 0 4 33 32 5 7
		f 4 -41 37 13 -40
		mu 0 4 34 33 7 9
		f 4 -43 39 15 -42
		mu 0 4 35 34 9 11
		f 4 46 -44 41 16
		mu 0 4 36 38 35 11
		f 4 -33 -46 -47 44
		mu 0 4 13 30 38 36
		f 4 -49 45 -21 -48
		mu 0 4 39 38 30 23
		f 4 -51 47 -1 -50
		mu 0 4 41 39 23 12
		f 4 9 -53 49 6
		mu 0 4 2 42 40 0
		f 4 -55 -10 1 24
		mu 0 4 44 43 2 24
		f 4 -57 -25 21 36
		mu 0 4 45 44 24 31
		f 4 -59 -37 33 8
		mu 0 4 46 45 31 3
		f 4 -60 -9 -8 -45
		mu 0 4 37 47 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube51";
	rename -uid "EDED6133-4F7F-2197-A0F1-54AB6D29D4AC";
	setAttr ".t" -type "double3" 599.99999745686853 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape51" -p "pCube51";
	rename -uid "8AD2B6BC-454A-E0B8-2130-2D9FBDA4D271";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube51";
	rename -uid "DC09B2C0-4B34-5D6B-366B-3780D47807B4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube52";
	rename -uid "AEB8353F-4710-400E-9E46-1F87EE5A88AE";
	setAttr ".t" -type "double3" 200 100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape52" -p "pCube52";
	rename -uid "B1FECF68-4E5C-762B-14D2-0897DD0B6FEA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube53";
	rename -uid "D5351627-4412-327B-F073-43B1E75B2F1C";
	setAttr ".t" -type "double3" 300.00001217297995 600.00000000000045 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
	setAttr ".rpt" -type "double3" -100.00001217297996 -100.00001217298063 0 ;
	setAttr ".sp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
createNode mesh -n "pCubeShape53" -p "pCube53";
	rename -uid "11E6ABEE-4BDB-A7D7-6F97-2286C1BF8E77";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 16 ".uvst[0].uvsp[0:15]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.375 0 0.625 0 0.625 0.25
		 0.375 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 96.25 7.5 -100 96.25 -7.5 100 -3.75
		 7.5 100 -3.75 -7.5 -100 96.25 7.5 -100 96.25 7.5 100 -3.75 -7.5 100 -3.75 -7.5 -93.29180908 109.66641235
		 7.5 -93.29180908 109.66641235 7.5 106.70819092 9.66640759 -7.5 106.70819092 9.66640759;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 0 2 0 1 3 0 0 4 0 1 5 0 4 5 0
		 3 6 0 5 6 0 2 7 0 7 6 0 4 7 0 4 8 0 5 9 0 8 9 0 6 10 0 9 10 0 7 11 0 11 10 0 8 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 14 16 -19 -20
		mu 0 4 12 13 14 15
		f 4 2 1 -4 -1
		mu 0 4 4 7 6 5
		f 4 0 5 -7 -5
		mu 0 4 0 1 9 8
		f 4 3 7 -9 -6
		mu 0 4 1 3 10 9
		f 4 -2 9 10 -8
		mu 0 4 3 2 11 10
		f 4 -3 4 11 -10
		mu 0 4 2 0 8 11
		f 4 6 13 -15 -13
		mu 0 4 8 9 13 12
		f 4 8 15 -17 -14
		mu 0 4 9 10 14 13
		f 4 -11 17 18 -16
		mu 0 4 10 11 15 14
		f 4 -12 12 19 -18
		mu 0 4 11 8 12 15;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape3" -p "pCube53";
	rename -uid "CF8D34A3-4F47-3A1C-17D1-BFB26D922545";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[0:5]" -type "float3"  0 0 88.75 0 0 88.75 0 0 -11.25 
		0 0 -11.25 0 0 0 0 0 0;
	setAttr -s 4 ".vt[0:3]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube54";
	rename -uid "1465DDC3-49A0-01B1-3F92-B0A6F528C5B5";
	setAttr ".t" -type "double3" 500 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape54" -p "pCube54";
	rename -uid "42353868-4BD2-E752-C01F-2CAB2A6F1E4C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube55";
	rename -uid "9A4D9D7C-48B8-3598-078E-CEBC4075845F";
	setAttr ".t" -type "double3" 500 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape55" -p "pCube55";
	rename -uid "35F1D2D2-4C35-E135-38DD-FBBFFF9EB28E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube56";
	rename -uid "F2799103-418A-53A5-70CD-CB830F9EE713";
	setAttr ".t" -type "double3" 799.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape56" -p "pCube56";
	rename -uid "B6560EFD-4EEA-715B-90E7-4F8B87A10BA7";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 600 0 0 600 0 0 600 0 0 
		600 0;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube56";
	rename -uid "12C20FF5-47D3-0A14-C1AD-B2B43F40AF9D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube57";
	rename -uid "84A7C9DC-4855-1B2E-2E1C-A5B7BCFD2324";
	setAttr ".t" -type "double3" 799.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape57" -p "pCube57";
	rename -uid "DF62DEFD-4B09-B929-1145-3FA8430B0002";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube57";
	rename -uid "A2019BAF-47E2-05F6-AC3F-E3ABF77AC179";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube58";
	rename -uid "1D605A42-465C-6879-2002-B8993DF4478B";
	setAttr ".t" -type "double3" 999.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape58" -p "pCube58";
	rename -uid "23912E8D-4207-5255-6DD3-05996722C73F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube58";
	rename -uid "40569976-4DC2-3BB9-9EAF-A89CC7CDE176";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube59";
	rename -uid "677F1B8C-4151-C1B4-9EF2-A2B59A6268CC";
	setAttr ".t" -type "double3" 999.99999745686853 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape59" -p "pCube59";
	rename -uid "BE9195EF-4AD8-C2BB-3FCA-22882B2D11DB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube59";
	rename -uid "E950CF3A-4C6E-61C3-72A9-6DA1319EC63D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube60";
	rename -uid "5F2D7326-459A-8E68-026F-05B74B3C563B";
	setAttr ".t" -type "double3" 999.99999745686853 599.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape60" -p "pCube60";
	rename -uid "E7481C46-485B-033B-8EE5-9CAB4B712BD0";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube60";
	rename -uid "71FF8E3B-42BB-56CE-4008-3D80B0ACF8AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube61";
	rename -uid "43ED6C75-4145-1353-E007-6DBD950C00B5";
	setAttr ".t" -type "double3" 999.99999745686853 799.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape61" -p "pCube61";
	rename -uid "32F87ED1-458E-3EA3-558A-FEA991C4CE8A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube61";
	rename -uid "6203D8F9-4326-C920-FE79-0AA7C12CBF5C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube62";
	rename -uid "AEAFC6FB-46B2-BCD8-B5B9-97BC99C462CE";
	setAttr ".t" -type "double3" 600 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape62" -p "pCube62";
	rename -uid "3C8FF7D1-4362-27C7-6CA1-549627E8C16D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube63";
	rename -uid "8EE060AE-4D35-E63D-B0DC-77909952035B";
	setAttr ".t" -type "double3" 600 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape63" -p "pCube63";
	rename -uid "433D9727-4AB8-56C2-5829-F5BEB4508D54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube64";
	rename -uid "1944D513-4382-854B-60A2-AB917026265A";
	setAttr ".t" -type "double3" 600 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape64" -p "pCube64";
	rename -uid "2C813450-4BCC-8E50-B57D-1FA2E0D81413";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube65";
	rename -uid "9BBF90AF-42A9-9D92-8DE5-FBB6165352FF";
	setAttr ".t" -type "double3" 600 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape65" -p "pCube65";
	rename -uid "C49E06A8-43DE-5B18-AC0B-44807B913215";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube66";
	rename -uid "206AAE98-4CE0-84B5-88F3-F2BF0BEADF43";
	setAttr ".t" -type "double3" 1000 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape66" -p "pCube66";
	rename -uid "2F8965C4-48A9-9459-F2D6-E0A41BA75181";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube67";
	rename -uid "49C78F79-4AF4-579E-8509-6E89438A2E88";
	setAttr ".t" -type "double3" 800 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape67" -p "pCube67";
	rename -uid "0FB48533-4785-487E-15DF-2BA1A4374E14";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube68";
	rename -uid "41316515-40AA-7E9C-3353-32B7AD8ABBB8";
	setAttr ".t" -type "double3" 800 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape68" -p "pCube68";
	rename -uid "2E42B045-4D66-3741-95A1-F7B08751EA47";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube69";
	rename -uid "C9AD7D4B-410E-1B8E-20AE-FB8E67FD0237";
	setAttr ".t" -type "double3" 1000 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape69" -p "pCube69";
	rename -uid "AD0BB4BE-41E6-4934-2ED7-6B87CB74649B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube70";
	rename -uid "0A697A83-4060-9187-1078-EA81855185EF";
	setAttr ".t" -type "double3" 600 100 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape70" -p "pCube70";
	rename -uid "3C4D4C86-4A0A-EF0C-7395-F5B21FB2E9D8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube71";
	rename -uid "80AA484A-4579-7B58-68C3-EAA847F54448";
	setAttr ".t" -type "double3" 600 700 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape71" -p "pCube71";
	rename -uid "86D55959-4423-EE15-2E08-68A0F1BEB81F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube72";
	rename -uid "EA8B5758-4A2A-E1C8-9588-539E97B1AF87";
	setAttr ".t" -type "double3" 900 800 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape72" -p "pCube72";
	rename -uid "085AE877-443E-756F-1116-D997AD65C7D9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube73";
	rename -uid "4BD7B4DE-4FB5-97CD-FE97-4284FE6D36BF";
	setAttr ".t" -type "double3" 700 800 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape73" -p "pCube73";
	rename -uid "F387FB5B-48FA-281F-3168-8F99CCC5CBC3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube74";
	rename -uid "446EEBF7-447B-3351-F5AB-95BF7313354D";
	setAttr ".t" -type "double3" 700 0 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape74" -p "pCube74";
	rename -uid "E8191174-435F-0895-C930-7389728B5C2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube75";
	rename -uid "18A69D26-459E-C8F7-D0C2-27A6414754C7";
	setAttr ".t" -type "double3" 900 0 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape75" -p "pCube75";
	rename -uid "92B569A1-4D75-FDA8-18B9-86B40A304966";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube76";
	rename -uid "4A802615-47C3-919D-1A8A-BD8A3903C1FA";
	setAttr ".t" -type "double3" 1199.9999974568686 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape76" -p "pCube76";
	rename -uid "92CBC984-4E9F-AF8D-8970-C9883FD8A779";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube76";
	rename -uid "DDCF9EA9-461D-B866-0F91-258E3DA6F3D0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube79";
	rename -uid "2D94739E-4FAA-EADF-05C2-ECAF3BDFDF19";
	setAttr ".t" -type "double3" 1000 700 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape79" -p "pCube79";
	rename -uid "8FB4E7DA-446B-9E43-4A8D-3EA0DDC4ED55";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube80";
	rename -uid "143FD86D-4FEE-FCE7-CAAA-BCB075FA6FA6";
	setAttr ".t" -type "double3" 1000 100 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape80" -p "pCube80";
	rename -uid "72D2861C-41E4-3B01-B48A-358FA8612D4A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube81";
	rename -uid "44B0B5F1-43D1-1EE7-BA02-2DB6A96554C3";
	setAttr ".rp" -type "double3" 1000 400 100 ;
	setAttr ".sp" -type "double3" 1000 400 100 ;
createNode mesh -n "pCubeShape81" -p "pCube81";
	rename -uid "0C78F203-4B86-0481-D45E-CF95384EE616";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube82";
	rename -uid "695D3A20-40DD-C566-1B53-899FE569F190";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape82" -p "pCube82";
	rename -uid "5223984C-42C8-FA2C-B847-388E4F5D4996";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube82";
	rename -uid "5A35EE3E-46D9-95F2-FCCE-EBB4651731DA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube83";
	rename -uid "709D8024-429D-024A-52C4-4983614DEDCC";
	setAttr ".t" -type "double3" 399.99999745686853 -200.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape83" -p "pCube83";
	rename -uid "061AE160-4C96-DCE8-BF1C-728894CCB160";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube83";
	rename -uid "39B35676-499A-4EA3-DFC4-F5BAEAC4EBE0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube84";
	rename -uid "899D3AD8-43F4-A278-0AF1-10B6189183CF";
	setAttr ".t" -type "double3" 399.99999745686853 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape84" -p "pCube84";
	rename -uid "A2496AD3-47DF-C191-FC84-5C876B139149";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube84";
	rename -uid "5C939D03-44AE-706A-DE33-B29D4DDD822A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube85";
	rename -uid "203D579A-419A-FD61-70BC-E68F13347162";
	setAttr ".t" -type "double3" 399.99999745686853 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape85" -p "pCube85";
	rename -uid "893A0271-43F2-2BAA-0853-B684376DC62F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube85";
	rename -uid "560C33E2-4865-A3F7-11DD-B7827565FC84";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube86";
	rename -uid "30757124-45DB-56C5-7E40-BBB5F544B243";
	setAttr ".t" -type "double3" 199.99999745686853 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape86" -p "pCube86";
	rename -uid "EC1D9A28-46B5-CF2B-5E01-A6AF41C1D392";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube86";
	rename -uid "8D921317-4116-2836-199B-4BBD0003B0F6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube87";
	rename -uid "1D9DC85B-4400-D76B-D1AF-51BB0C8BD945";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape87" -p "pCube87";
	rename -uid "76D6448A-4A93-08BD-16E6-CF93B26D60A6";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube87";
	rename -uid "3193ACE3-41CD-8449-8D1F-02B5F1D17703";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube88";
	rename -uid "EC166A8B-4622-87B0-B0C1-C0B0F6A260AB";
	setAttr ".t" -type "double3" -200.00000254313147 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape88" -p "pCube88";
	rename -uid "8029877A-4AC9-8373-7E48-C084B158A3AF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube88";
	rename -uid "EBFCFE4F-41D7-A47F-518A-12AE08F17221";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube89";
	rename -uid "121A133D-4930-B620-83CA-509418E2C51D";
	setAttr ".t" -type "double3" -200.00000254313147 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape89" -p "pCube89";
	rename -uid "9FA2DCC3-467C-60D6-EF7C-528911BA87FF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube89";
	rename -uid "DA5EA4E2-481E-E61F-45F9-E9A4B281B0A0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube90";
	rename -uid "72CF47FC-4B8C-D33E-EBF3-A0A7FEEDDB43";
	setAttr ".t" -type "double3" -200.00000254313147 -200.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape90" -p "pCube90";
	rename -uid "4A94D19C-4FC2-AA83-12E1-9EA0AC4B3621";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube90";
	rename -uid "F6E50C7A-40BA-4EA6-DBBD-26A57E7D681C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube91";
	rename -uid "BBE28AE6-458C-1609-58F6-18BACAAD9A8F";
	setAttr ".t" -type "double3" -400 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape91" -p "pCube91";
	rename -uid "9AAB023B-4667-44F0-9F33-E998AFAF3755";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube92";
	rename -uid "F83FE607-473A-A583-A66B-D2B3BE441EB4";
	setAttr ".t" -type "double3" -400 -700 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape92" -p "pCube92";
	rename -uid "3C2D63B3-4E47-0DAC-CB08-E5A285B78EF5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube93";
	rename -uid "CE312F37-42D6-FB0E-E116-3BA6719743C7";
	setAttr ".t" -type "double3" -400 -600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape93" -p "pCube93";
	rename -uid "F1439FE0-4B4B-BB45-D8D5-9A803FDC790C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube94";
	rename -uid "1ED28946-4D8F-540E-52A5-6F8D2CA20FC8";
	setAttr ".t" -type "double3" -400 -500 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape94" -p "pCube94";
	rename -uid "882D6DC6-4593-DF56-2A7C-F6A34DCDAA8D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube95";
	rename -uid "8E162955-4763-EA26-69BB-D5A2B155FFAE";
	setAttr ".t" -type "double3" -400 -400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape95" -p "pCube95";
	rename -uid "A828FCA8-4A64-16EE-5BED-D9B4E585F18F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube96";
	rename -uid "58D4A99D-416C-C784-CEFB-CEB844C4F6B0";
	setAttr ".t" -type "double3" -400 -300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape96" -p "pCube96";
	rename -uid "54AEB064-4CBB-9B59-55D9-B79FA362C6FF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube97";
	rename -uid "B000F82C-47C2-710A-E1D1-E2AFCC3CFCD1";
	setAttr ".t" -type "double3" -400 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape97" -p "pCube97";
	rename -uid "C446EF23-4246-3B97-284F-DC957A085879";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube98";
	rename -uid "6DBBBF2E-46B0-6698-173E-8389EC77B200";
	setAttr ".t" -type "double3" 400 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape98" -p "pCube98";
	rename -uid "E94BF5B5-40AF-0590-ECEB-8A9113912124";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube99";
	rename -uid "5B46085B-4E9B-1C32-2E0D-DC8BAFF1CC7D";
	setAttr ".t" -type "double3" 400 -300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape99" -p "pCube99";
	rename -uid "CFE93CC5-4CD9-C4AC-153C-AB8D9BBA0ECD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube100";
	rename -uid "702BCF6D-4470-BBF5-B553-89B8385658E3";
	setAttr ".t" -type "double3" 400 -400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape100" -p "pCube100";
	rename -uid "D7A4A5DF-4FD7-4762-152F-83A3EFCCE685";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube101";
	rename -uid "2CC41129-43E7-935C-5F7C-D4BC70025FE4";
	setAttr ".t" -type "double3" 400 -500 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape101" -p "pCube101";
	rename -uid "90BC9095-487F-D99E-D481-DF9AA1157CE0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube102";
	rename -uid "58FDC524-41AC-B4AD-926D-81A0513FBA92";
	setAttr ".t" -type "double3" 400 -600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape102" -p "pCube102";
	rename -uid "C6F476E9-40E2-C12B-4157-36B08E99DA52";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube103";
	rename -uid "4D64445B-43D9-0B9E-505F-079FE7FD4EE6";
	setAttr ".t" -type "double3" 400 -700 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape103" -p "pCube103";
	rename -uid "EE629DCE-4DB6-2B96-9AD4-319D9804AA2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube104";
	rename -uid "9FFB892D-4657-9AFE-D08A-418D19532C9B";
	setAttr ".t" -type "double3" 400 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape104" -p "pCube104";
	rename -uid "3218409F-4301-76CB-4EA8-0CA9DF27BA5D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube105";
	rename -uid "AF5819FD-4EF3-6CA9-D4B2-4A8D096508C1";
	setAttr ".t" -type "double3" -300 -200 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape105" -p "pCube105";
	rename -uid "07047953-4FD2-004F-64D8-618F9F33F0B9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube106";
	rename -uid "6D129E60-4114-797B-B7A7-D1A19D546C39";
	setAttr ".t" -type "double3" 300 -200 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape106" -p "pCube106";
	rename -uid "13E0B3AC-46BD-A48F-6F49-C9BC218B47F1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube107";
	rename -uid "EF9CBC0F-493E-098A-4B55-81A00DD46524";
	setAttr ".t" -type "double3" 100 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape107" -p "pCube107";
	rename -uid "E36A1D82-49A5-4771-4B31-AF8180EB7BC2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube108";
	rename -uid "8FEED2D3-4A66-0368-EABB-05B6CCF8F5D6";
	setAttr ".t" -type "double3" 0 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape108" -p "pCube108";
	rename -uid "82E7F982-4E67-FF73-475C-3CB7B3B3E115";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube109";
	rename -uid "91561E9C-47E4-D200-C0E7-19A7DFE7D1E1";
	setAttr ".t" -type "double3" -100 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape109" -p "pCube109";
	rename -uid "CC53E852-4569-C69A-75A7-5A8742E221FC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube110";
	rename -uid "685162BD-4B59-656D-78B4-318F74030DFD";
	setAttr ".t" -type "double3" -200 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape110" -p "pCube110";
	rename -uid "8B38F883-4202-44A7-AFCD-8AABCF559E3C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube111";
	rename -uid "5C40EB15-42B4-C51D-A1A2-10BC24A5EA91";
	setAttr ".t" -type "double3" 200 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape111" -p "pCube111";
	rename -uid "D3F85173-4E8E-DE42-7D7C-DDBB181876C8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube112";
	rename -uid "02154B6B-40C5-0ABD-2FA9-D69B204A7E70";
	setAttr ".t" -type "double3" 300 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape112" -p "pCube112";
	rename -uid "73A7B5D8-4E56-35A6-EC22-D99D199CF1E6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube113";
	rename -uid "11C613FC-4088-1138-3A7D-BD97D63483F9";
	setAttr ".t" -type "double3" -300 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape113" -p "pCube113";
	rename -uid "A4D9E96B-4C25-FD87-06C6-98927CD02EFA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube114";
	rename -uid "786502B1-4BF1-FCBB-6B8C-1880B0AD2081";
	setAttr ".t" -type "double3" 1100 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape114" -p "pCube114";
	rename -uid "59D878D8-474E-2702-652F-05A2686C72D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube115";
	rename -uid "7C7D22A0-4A66-AE1E-25CC-54B366CB4061";
	setAttr ".t" -type "double3" 1300 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape115" -p "pCube115";
	rename -uid "F321A362-46DE-328E-3C24-288E6C4FA5BC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube116";
	rename -uid "9AE70EEA-4339-3667-3DC0-63995E57E623";
	setAttr ".t" -type "double3" 1100 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape116" -p "pCube116";
	rename -uid "FD845EE1-4A7C-3B2B-9FF4-D58D592F7A90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube117";
	rename -uid "9DE1981C-43D6-748C-6791-58B84438D009";
	setAttr ".t" -type "double3" 1400 600 3.75 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape117" -p "pCube117";
	rename -uid "4FFE48FE-41C1-827A-2372-C78D8E4E641C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube118";
	rename -uid "0216ED96-439C-F919-0AB0-A79E422290DB";
	setAttr ".t" -type "double3" 1200 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape118" -p "pCube118";
	rename -uid "07304AC4-48B4-9694-02FF-4B9F3245649E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube119";
	rename -uid "B5A408E4-4703-CEF7-71EB-53A23956920D";
	setAttr ".t" -type "double3" 1200 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape119" -p "pCube119";
	rename -uid "04D6D65F-4E37-9E4D-03F3-DFA59870E24A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube120";
	rename -uid "8994995B-4C8A-2337-BEBF-0BAEBAE62089";
	setAttr ".t" -type "double3" 1400 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape120" -p "pCube120";
	rename -uid "5416B3DB-4A71-7D8B-7E1A-03AC242FDE67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube121";
	rename -uid "A437E837-469C-A324-1C42-99BB15546EE8";
	setAttr ".t" -type "double3" 1400 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape121" -p "pCube121";
	rename -uid "CF0B87DC-4B0A-CEF4-67B2-A9B614F57430";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube122";
	rename -uid "76E4CC45-49AA-71B9-4D08-27B6F089820C";
	setAttr ".t" -type "double3" 1000 225 150 ;
	setAttr ".rp" -type "double3" 0 50 -50 ;
	setAttr ".sp" -type "double3" 0 50 -50 ;
createNode mesh -n "pCubeShape122" -p "pCube122";
	rename -uid "529F4461-4834-EC61-639B-22A2D12E3A37";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4166666567325592 0.1666666567325592 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube123";
	rename -uid "50C9EA98-4815-1E1B-886F-32B4E11CD8DA";
	setAttr ".t" -type "double3" 1000 475 150 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".rp" -type "double3" 0 50 -50 ;
	setAttr ".sp" -type "double3" 0 50 -50 ;
createNode mesh -n "pCubeShape123" -p "pCube123";
	rename -uid "52A1A085-473E-0757-6A41-04BBAD8F2F6E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4166666567325592 0.1666666567325592 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 42 ".uvst[0].uvsp[0:41]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.54166663 0 0.54166663 1 0.54166663 0.25 0.54166663
		 0.5 0.54166663 0.75 0.45833331 0 0.45833331 1 0.45833331 0.25 0.45833331 0.5 0.45833331
		 0.75 0.375 0.25 0.45833331 0.25 0.45833331 0.5 0.375 0.5 0.54166663 0.5 0.54166663
		 0.25 0.625 0.25 0.625 0.5 0.375 0.16666666 0.125 0.16666666 0.375 0.58333337 0.45833331
		 0.58333337 0.54166663 0.58333337 0.625 0.58333337 0.875 0.16666666 0.625 0.16666666
		 0.54166663 0.16666666 0.45833331 0.16666666;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -32.78930664 -50 16.66667175 32.78930664 -50 16.66667175
		 -32.78930664 50 116.66668701 32.78930664 50 116.66668701 -32.78930664 50 -50 32.78930664 50 -50
		 -32.78930664 -50 -50 32.78930664 -50 -50 16.66668701 -50 16.66667175 16.66668701 50 116.66668701
		 16.66668701 50 -50 16.66668701 -50 -50 -16.66668701 -50 16.66667175 -16.66668701 50 116.66668701
		 -16.66668701 50 -50 -16.66668701 -50 -50 -32.78930664 75 116.66668701 -16.66668701 75 116.66668701
		 -16.66668701 75 -50 -32.78930664 75 -50 16.66668701 75 116.66668701 16.66668701 75 -50
		 32.78930664 75 116.66668701 32.78930664 75 -50 -32.78930664 8.33333588 116.66668701
		 -32.78930664 8.33333588 -50 -16.66668701 8.33333588 -50 16.66668701 8.33333588 -50
		 32.78930664 8.33333588 -50 32.78930664 8.33333588 116.66668701 16.66668701 8.33333588 116.66668701
		 -16.66668701 8.33333588 116.66668701;
	setAttr -s 60 ".ed[0:59]"  0 12 0 2 13 1 4 14 1 6 15 0 0 24 0 2 4 1
		 3 5 1 4 25 0 5 28 0 6 0 0 7 1 0 8 1 0 9 3 1 10 5 1 9 10 0 11 7 0 10 27 1 11 8 1 12 8 0
		 13 9 0 14 10 0 13 14 0 15 11 0 14 26 1 15 12 1 2 16 0 13 17 0 16 17 0 14 18 0 17 18 0
		 4 19 0 19 18 0 16 19 0 9 20 0 10 21 0 20 21 0 3 22 0 20 22 0 5 23 0 22 23 0 21 23 0
		 24 2 0 25 6 0 24 25 1 26 15 1 25 26 1 27 11 1 26 27 1 28 7 0 27 28 1 29 3 0 28 29 1
		 30 9 1 29 30 0 31 13 1 30 31 0 31 24 0 1 29 0 8 30 1 12 31 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 -5 0 59 56
		mu 0 4 32 0 19 41
		f 4 27 29 -32 -33
		mu 0 4 24 25 26 27
		f 4 -4 -43 45 44
		mu 0 4 23 6 34 35
		f 4 3 24 -1 -10
		mu 0 4 6 23 20 8
		f 4 -11 -49 51 -58
		mu 0 4 1 10 38 39
		f 4 9 4 43 42
		mu 0 4 12 0 32 33
		f 4 11 57 53 -59
		mu 0 4 14 1 39 40
		f 4 -36 37 39 -41
		mu 0 4 28 29 30 31
		f 4 -16 -47 49 48
		mu 0 4 7 18 36 37
		f 4 -18 15 10 -12
		mu 0 4 15 18 7 9
		f 4 18 58 55 -60
		mu 0 4 19 14 40 41
		f 4 -22 19 14 -21
		mu 0 4 22 21 16 17
		f 4 -23 -45 47 46
		mu 0 4 18 23 35 36
		f 4 -25 22 17 -19
		mu 0 4 20 23 18 15
		f 4 1 26 -28 -26
		mu 0 4 2 21 25 24
		f 4 21 28 -30 -27
		mu 0 4 21 22 26 25
		f 4 -3 30 31 -29
		mu 0 4 22 4 27 26
		f 4 -6 25 32 -31
		mu 0 4 4 2 24 27
		f 4 -15 33 35 -35
		mu 0 4 17 16 29 28
		f 4 12 36 -38 -34
		mu 0 4 16 3 30 29
		f 4 6 38 -40 -37
		mu 0 4 3 5 31 30
		f 4 -14 34 40 -39
		mu 0 4 5 17 28 31
		f 4 -44 41 5 7
		mu 0 4 33 32 2 13
		f 4 2 23 -46 -8
		mu 0 4 4 22 35 34
		f 4 -48 -24 20 16
		mu 0 4 36 35 22 17
		f 4 -50 -17 13 8
		mu 0 4 37 36 17 5
		f 4 -52 -9 -7 -51
		mu 0 4 39 38 11 3
		f 4 -54 50 -13 -53
		mu 0 4 40 39 3 16
		f 4 -56 52 -20 -55
		mu 0 4 41 40 16 21
		f 4 -57 54 -2 -42
		mu 0 4 32 41 21 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube124";
	rename -uid "76C7C82F-4357-4025-23A5-D99BA600D497";
	setAttr ".t" -type "double3" 1000 400 200 ;
	setAttr ".s" -type "double3" 3.8492894976326011 1.2150767524380277 0.04324446896417506 ;
	setAttr ".rp" -type "double3" 0 0 -100 ;
	setAttr ".sp" -type "double3" 0 0 -100 ;
createNode mesh -n "pCubeShape124" -p "pCube124";
	rename -uid "33A7263E-476E-15E9-C5C9-DEB7A1F50230";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000074505805969 0.12499999953433871 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt";
	setAttr ".pt[49]" -type "float3" 0.63051897 -1.8537235 0 ;
	setAttr ".pt[50]" -type "float3" -0.63051897 -1.8537235 0 ;
	setAttr ".pt[53]" -type "float3" -0.63051897 1.8537235 0 ;
	setAttr ".pt[54]" -type "float3" 0.63051897 1.8537235 0 ;
	setAttr ".pt[56]" -type "float3" 5.9604645e-008 -1.9073486e-006 0 ;
	setAttr ".pt[57]" -type "float3" -5.9604645e-008 -1.9073486e-006 0 ;
	setAttr ".pt[58]" -type "float3" 5.9604645e-008 1.9073486e-006 0 ;
	setAttr ".pt[59]" -type "float3" -5.9604645e-008 1.9073486e-006 0 ;
	setAttr ".db" yes;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape4" -p "pCube124";
	rename -uid "4687D5BD-4587-13A2-6028-C39C0A1CC31A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 -6.0692978 0 0 -6.0692978 
		0 0 6.0693054 0 0 6.0693054 0 0 6.0693016 0 0 6.0693016 0 0 -6.0692978 0 0 -6.0692978 
		0;
	setAttr -s 8 ".vt[0:7]"  -16.5 -125 100 16.5 -125 100 -16.5 125 100
		 16.5 125 100 -16.5 125 -100 16.5 125 -100 -16.5 -125 -100 16.5 -125 -100;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube126";
	rename -uid "E1434AA3-4AC0-E10B-2D7C-2E8ACE67168E";
	setAttr ".t" -type "double3" 1999.9999974568686 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape126" -p "pCube126";
	rename -uid "946A6F62-4CF6-128A-BD44-1FBA2AA76999";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube126";
	rename -uid "3C1F306D-474A-BCE4-3936-32A38EAB2DC6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube127";
	rename -uid "372F5E91-4AC0-1C01-F496-87BB7CD7FC69";
	setAttr ".t" -type "double3" 1800.0000000000002 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape127" -p "pCube127";
	rename -uid "7E470591-42D7-E418-08F9-63A7D07630B3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube128";
	rename -uid "9A724CBB-4F5C-E5DA-A2AE-A59DA73401AE";
	setAttr ".t" -type "double3" 1800.0000000000002 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape128" -p "pCube128";
	rename -uid "F19632D8-441A-6C22-2696-93925B60C0C8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube129";
	rename -uid "94DBE1D0-4390-1FA8-F580-979631A39EC7";
	setAttr ".t" -type "double3" 1900.0000000000002 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape129" -p "pCube129";
	rename -uid "F1AE18D7-48D3-0409-70D7-A484F94093E0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube130";
	rename -uid "DB84F247-4BB8-9D78-F561-F9A750A6E8F7";
	setAttr ".t" -type "double3" 2100 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape130" -p "pCube130";
	rename -uid "2DFF9237-4568-4675-25D9-64A4BCB4B02C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube131";
	rename -uid "DB9FAF2D-4E1D-F6C3-AA94-AFACBE8AFA4F";
	setAttr ".t" -type "double3" 1900.0000000000002 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape131" -p "pCube131";
	rename -uid "08A2F779-473F-8F48-649D-FD907CC60599";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube132";
	rename -uid "421F2F86-4369-F4CE-C29F-1FA82430216B";
	setAttr ".t" -type "double3" 2000.0000000000002 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape132" -p "pCube132";
	rename -uid "A12000F1-4298-940B-CFD0-EBA7170C18D5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube133";
	rename -uid "E7188A59-4BE4-21E7-FAEA-4EBD9C1B743C";
	setAttr ".t" -type "double3" 2000.0000000000002 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape133" -p "pCube133";
	rename -uid "297E8E50-403E-371B-87F4-29B0B20269B9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube134";
	rename -uid "8C1B0EC0-46AA-DD9D-1015-C495D1705E60";
	setAttr ".t" -type "double3" 2100 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape134" -p "pCube134";
	rename -uid "13E8FB9B-438F-DAA1-D0DD-EC9C76FA0C36";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube135";
	rename -uid "7C106497-4E3B-F471-1712-75A154673C9A";
	setAttr ".t" -type "double3" 2200 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape135" -p "pCube135";
	rename -uid "B0C3E506-4E73-254A-55B1-638D9C5EA45F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube136";
	rename -uid "A42B6972-44C8-247D-3F01-BB93E6759134";
	setAttr ".t" -type "double3" 2200 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape136" -p "pCube136";
	rename -uid "7A3EA2C3-47EB-C2C5-0FED-328248AB262C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube137";
	rename -uid "7A51C65B-4345-90BB-DFE0-D79E5108FD25";
	setAttr ".t" -type "double3" 2200 300 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape137" -p "pCube137";
	rename -uid "5FCD9E4C-49E4-6B6B-8B03-41AB4A7EB275";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube138";
	rename -uid "78A97D93-47E7-F2D9-E493-7AB86B7707EE";
	setAttr ".t" -type "double3" 2200 500.00000000000006 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape138" -p "pCube138";
	rename -uid "79611235-4A01-346B-F2C9-4C83D548EC16";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube139";
	rename -uid "67182441-45C4-E1BB-653B-C78B0403C302";
	setAttr ".t" -type "double3" 2200 400 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape139" -p "pCube139";
	rename -uid "54177B00-4D18-AC4B-390B-E4AA4CC97293";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube140";
	rename -uid "474DDF31-4474-341F-FB74-2C968F308334";
	setAttr ".t" -type "double3" 1599.9999974568686 399.99999745686853 99.5 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape140" -p "pCube140";
	rename -uid "E9A91BA6-401A-8922-2A21-E2B39BA473BF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube140";
	rename -uid "F114E1BE-49ED-5404-0BA0-73B38A603AC0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube141";
	rename -uid "AC58093F-4B56-58AC-7EF2-A1BBFBCBD9DD";
	setAttr ".t" -type "double3" 1999.9999974568686 799.99999745686853 99.5 ;
	setAttr ".r" -type "double3" 0 90 180 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape141" -p "pCube141";
	rename -uid "CB637F2E-4628-D648-8560-2FA86A2FBC58";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube141";
	rename -uid "6E510106-43FB-AC5F-A243-7FB42739BBF0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube142";
	rename -uid "0FB170FD-4752-9D2B-F330-6C82F61F2872";
	setAttr ".t" -type "double3" 1400 200 3.75 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape142" -p "pCube142";
	rename -uid "01C02310-4321-5CF0-0D26-47A93A0737A3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube143";
	rename -uid "16EDF2FE-49B3-8018-51A1-BCB26630B08D";
	setAttr ".t" -type "double3" 1800.0000000000002 200 3.75 ;
	setAttr ".r" -type "double3" -90 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape143" -p "pCube143";
	rename -uid "EBAD918C-4057-60FF-9FA3-28A68D2F8BDE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube144";
	rename -uid "6961A4A2-480B-1DEE-F6F0-E5A5E43356ED";
	setAttr ".t" -type "double3" 1800.0000000000002 600 3.75 ;
	setAttr ".r" -type "double3" -90 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape144" -p "pCube144";
	rename -uid "C6ED2F72-4089-D21E-BB4C-DC92E8C7BA5A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube145";
	rename -uid "5F7175AA-4697-02E1-53EE-BB86A580B6F1";
	setAttr ".t" -type "double3" 1300 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape145" -p "pCube145";
	rename -uid "6D8D0A82-4FF1-58A0-C385-009AA7CD8968";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "54A743F2-4900-B3C9-DB83-52BC77806DAF";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "00DFEFA2-41F4-C5E2-BF30-4CAA5391F4DF";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8AE30D7C-4C73-2C40-5CBD-2787313914A4";
createNode displayLayerManager -n "layerManager";
	rename -uid "6EFF0C15-43D9-7851-67AF-67B7AB1539DB";
	setAttr ".cdl" 1;
	setAttr -s 4 ".dli[1:3]"  1 0 0;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "B93A9790-4572-D259-7F0B-75B3BD89CC54";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "66934461-425C-A3AA-2EED-40B4AAAF05EB";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1E65B464-4F38-6DFD-5EB7-8BAB0C7DFAA0";
	setAttr ".g" yes;
createNode objectSet -n "set1";
	rename -uid "A19DAB60-44EC-91BF-3BDB-D992E3AF69DE";
	setAttr ".ihi" 0;
	setAttr -s 23 ".dsm";
	setAttr -s 23 ".gn";
createNode polyCube -n "polyCube1";
	rename -uid "67390C7D-48DC-5406-71F9-629D8FF3EC20";
	setAttr ".w" 33;
	setAttr ".h" 33;
	setAttr ".d" 33;
	setAttr ".sd" 4;
	setAttr ".cuv" 4;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "D80DA864-413A-8F66-060C-A4B40BDA0218";
	setAttr ".ics" -type "componentList" 4 "e[2:3]" "e[8:9]" "e[31:32]" "e[34:35]";
	setAttr ".cv" yes;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "237D3091-486C-2C57-325A-FEBBA997764C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.5431314725210541e-006 399.99999745686853 -0.5 1;
	setAttr ".wt" 0.53279906511306763;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId3";
	rename -uid "DE55DFF8-42FF-F2AE-D8C1-838C0724B3EF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "1BA44530-4757-8233-592B-569A447B186A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "e[0:3]";
createNode polySplitRing -n "polySplitRing2";
	rename -uid "26FE3A55-4F12-0E5C-F661-B4A4720328A3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[2:3]" "e[6]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.5431314725210541e-006 399.99999745686853 -0.5 1;
	setAttr ".wt" 0.46319913864135742;
	setAttr ".re" 3;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId4";
	rename -uid "2C6B874E-4536-163D-177D-8B932F9E19FA";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube2";
	rename -uid "937486F9-467F-53A6-53A4-D7A9DA7581B8";
	setAttr ".w" 15;
	setAttr ".h" 200;
	setAttr ".d" 15;
	setAttr ".sd" 4;
	setAttr ".cuv" 4;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "49D0D1FA-4A38-A474-ED2A-CEB3AE06D843";
	setAttr ".ics" -type "componentList" 4 "e[2:3]" "e[8:9]" "e[31:32]" "e[34:35]";
	setAttr ".cv" yes;
createNode objectSet -n "set2";
	rename -uid "7F8A473A-4F78-A0F9-0071-02A8634A32D6";
	setAttr ".ihi" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D4CCAE8B-4560-DEB1-5079-B49457664F06";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 790\n                -height 396\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n"
		+ "            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n"
		+ "            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n"
		+ "            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 790\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n"
		+ "                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n"
		+ "                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 789\n                -height 395\n"
		+ "                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n"
		+ "            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n"
		+ "            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n"
		+ "            -captureSequenceNumber -1\n            -width 789\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 790\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 790\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n"
		+ "                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n"
		+ "                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1586\n                -height 836\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1586\n            -height 836\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n"
		+ "                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n"
		+ "            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n"
		+ "                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n"
		+ "                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n"
		+ "                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n"
		+ "                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n"
		+ "                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n"
		+ "                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n"
		+ "            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1586\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1586\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 10000 -divisions 3 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "487FEB85-49C4-952F-3E2A-3C86B5033114";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 250 ";
	setAttr ".st" 6;
createNode groupId -n "groupId5";
	rename -uid "4C146A48-4F58-8E17-9866-A5AE6CCF6BDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "08869E1C-4696-28EB-D679-D39626FED4AB";
	setAttr ".ihi" 0;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "D6DD56ED-4BD4-C11B-9F75-9891DFB9C4E8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.71522927284240723;
	setAttr ".dr" no;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "922D6A40-4C16-07DD-3DAC-478020E558DB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[20:21]" "e[23]" "e[25]" "e[27]" "e[29]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.48539397120475769;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "63DB301C-44FF-37EE-3ABC-D79297A6A20A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[8:9]" "e[16:17]" "e[24]" "e[31]" "e[36]" "e[43]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.4409109354019165;
	setAttr ".re" 16;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "78D22434-44EC-4176-0D3C-CFBDC7601B40";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[12]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[13]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[14]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[15]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[16]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[17]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[20]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[21]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[22]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.75 0 0 ;
createNode groupId -n "groupId7";
	rename -uid "961A7870-4701-0CD7-309E-58B148A6CEF7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "8FF5333F-4E3D-4464-C037-1BA179397DA5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "396A1027-4C3D-C56E-F77E-A6B57374CBB9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "5B7BC0D3-4E24-58F6-E402-7BA1F4B600BB";
	setAttr ".ihi" 0;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "A4778372-4F1D-B9A1-AF6E-1D9BC3E7F45A";
	setAttr ".ics" -type "componentList" 7 "e[34]" "e[36]" "e[38]" "e[40]" "e[42:43]" "e[45]" "e[55]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "AAD14829-48A3-E71F-E6CB-968FD56F04EA";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[31]" -type "float3" 0 0 -5.4166679 ;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "CCE37D80-4444-CA9B-5B40-AC8D611FA5EC";
	setAttr ".ics" -type "componentList" 4 "e[21]" "e[23:27]" "e[33]" "e[39]";
	setAttr ".cv" yes;
createNode groupId -n "groupId11";
	rename -uid "50AE1E8F-4426-DCF9-E088-C89B7D2D4D12";
	setAttr ".ihi" 0;
createNode objectSet -n "set3";
	rename -uid "6A87856D-4538-91ED-DCFB-ABB8BE41B546";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr -s 2 ".gn";
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "E4183424-45E3-B694-1512-12B2283C6A48";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 300.00001217298018 200 3.75 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 300 200 50 ;
	setAttr ".rs" 57880;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 200.00001217298018 192.49999999999997 0 ;
	setAttr ".cbx" -type "double3" 400.00001217298018 207.50000000000003 100 ;
createNode groupId -n "groupId12";
	rename -uid "63452582-451A-296B-D298-E19AE77E131B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "459EA8D1-42FD-0D79-96DA-10B674492979";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "e[0:3]";
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "381B00F7-411E-6E76-1691-F293E6955F6C";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 300.00001217298018 200 3.75 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 300 200 50 ;
	setAttr ".rs" 33266;
	setAttr ".lt" -type "double3" 0 0 15 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 200.00001217298018 192.49999999999997 0 ;
	setAttr ".cbx" -type "double3" 400.00001217298018 207.50000000000003 100 ;
createNode groupId -n "groupId13";
	rename -uid "8807B28E-4D30-7964-0EEE-638C88851059";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "A884AF02-4392-3A69-D4EE-4F9D5B5ED734";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "5577C312-4B95-1147-CACD-E08BC7310D1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "495B92F3-42D1-EB74-E6FC-319D8914AF1F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "1EA7FB89-446B-ECC8-554C-A49ECBA41F27";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "A37E596D-4FF1-0781-C5C4-7BA80743A3DD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "5A71D228-4528-30A1-AAC1-D2A29A4A2606";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "264D067A-47C0-1126-F071-94BB3606642F";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube3";
	rename -uid "20D3E0F0-437A-EF02-6E19-C58B2962313D";
	setAttr ".w" 33;
	setAttr ".h" 250;
	setAttr ".d" 200;
	setAttr ".cuv" 4;
createNode groupId -n "groupId21";
	rename -uid "BC66C701-4802-649D-5D59-169F29A17E43";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "AE7B0E44-4D7C-02F9-7772-C283062316FE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "A7B947B5-4F04-FB10-A238-67B08C942634";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "FA15EB4A-410D-D758-6885-2AAFB6F82098";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "B3491FE0-4FE0-7340-F997-419FD148C6A4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "CEB573CA-4B99-8AAA-1DBB-489A0A6F95B2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "5FC0D3CC-4A08-A7DC-BD45-33AF5B08B528";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "DD8D3E5F-408C-C2D0-6AE5-B0B7AA206A13";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "30C7C7DD-422A-F73A-2E18-8C81ED64FB0D";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube4";
	rename -uid "D8DD4AFB-4C38-A0A8-AA7A-A88A65D07EB7";
	setAttr ".w" 100;
	setAttr ".h" 100;
	setAttr ".d" 100;
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "341118C6-4FEF-A95A-84F9-2CBE17482C7F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 200 100 1;
	setAttr ".wt" 0.73183989524841309;
	setAttr ".dr" no;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "CA5338EE-4E12-48FC-BC19-38839F10747D";
	setAttr ".ics" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 200 100 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1000 250 100 ;
	setAttr ".rs" 64990;
	setAttr ".lt" -type "double3" 0 5.5511144613812548e-015 24.999997019767591 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 967.210693359375 250 50 ;
	setAttr ".cbx" -type "double3" 1032.789306640625 250 150 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "D2916072-478B-B66D-F9CC-139B6D5CD0AD";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[0:8]" -type "float3"  17.21069145 0 0 -17.21069145
		 0 0 17.21069145 0 0 -17.21069145 0 0 17.21069145 0 0 -17.21069145 0 0 17.21069145
		 0 0 -17.21069145 0 0 0 0 0;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "3A3CEA0E-428E-E2D8-7805-B2B75C4B157C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[4:5]" "e[8:9]" "e[14]" "e[18]" "e[22]" "e[26]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 225 150 1;
	setAttr ".wt" 0.74279630184173584;
	setAttr ".dr" no;
	setAttr ".re" 4;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "C2870C1A-42E6-CC67-3672-E0806D9DE476";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[1]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[2]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[3]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[8]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[9]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[12]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[13]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[16]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[17]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[20]" -type "float3" 0 0 66.666687 ;
	setAttr ".tk[22]" -type "float3" 0 0 66.666687 ;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "2F8329BF-436E-4882-D617-CA9A9B0FC585";
	setAttr ".ics" -type "componentList" 7 "e[62]" "e[64]" "e[66]" "e[68]" "e[70]" "e[72]" "e[74:75]";
	setAttr ".cv" yes;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "87307FBF-4291-FB5E-2F29-3B8E9D0E0CA7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[53]" "e[55:56]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 225 150 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak5";
	rename -uid "4D531F45-46E9-FC1E-76E0-D1A637C5C472";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 -100.00002 ;
	setAttr ".tk[1]" -type "float3" 0 0 -100.00002 ;
	setAttr ".tk[8]" -type "float3" 0 0 -100.00002 ;
	setAttr ".tk[12]" -type "float3" 0 0 -100.00002 ;
	setAttr ".tk[24]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[25]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[26]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[27]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[28]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[29]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[30]" -type "float3" 0 -8.3333282 0 ;
	setAttr ".tk[31]" -type "float3" 0 -8.3333282 0 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "9B1FB4DE-4435-BBA8-29B2-F38996D4AE70";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[10:11]";
	setAttr ".ix" -type "matrix" 3.2160360816553615 0 0 0 0 1.2715919516266956 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "AB82D870-47CB-881B-46CA-BA9F86BD030D";
	setAttr ".ics" -type "componentList" 2 "f[0:3]" "f[6:9]";
	setAttr ".ix" -type "matrix" 3.8492894976326011 0 0 0 0 1.2715919516266956 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1000.0001 400.00003 104.32444 ;
	setAttr ".rs" 59684;
	setAttr ".lt" -type "double3" 0 1.1878969604529101e-014 10.501876915497292 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 936.48684076005509 233.33336492863285 99.99998944226833 ;
	setAttr ".cbx" -type "double3" 1063.5132767109378 566.66671268318055 108.64888323510334 ;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "D2DBA4B0-4319-6DDA-B927-CE8925132D47";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[0:1]" "e[5]" "e[8:10]" "e[13:14]";
	setAttr ".ix" -type "matrix" 3.8492894976326011 0 0 0 0 1.2715919516266956 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak6";
	rename -uid "CD3C7C69-464F-0B31-7EDA-D6AA64B0F710";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[17]" -type "float3" -3.0198066e-014 0 -53.361038 ;
	setAttr ".tk[18]" -type "float3" -3.0198066e-014 0 -53.361038 ;
	setAttr ".tk[20]" -type "float3" -3.0198066e-014 0 -53.361038 ;
	setAttr ".tk[23]" -type "float3" -3.0198066e-014 0 -53.361038 ;
	setAttr ".tk[24]" -type "float3" -2.8421709e-014 0 -53.361038 ;
	setAttr ".tk[27]" -type "float3" -2.8421709e-014 0 -53.361038 ;
	setAttr ".tk[29]" -type "float3" -2.8421709e-014 0 -53.361038 ;
	setAttr ".tk[30]" -type "float3" -2.8421709e-014 0 -53.361038 ;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "BE817E61-42BA-E5EE-1ED8-6ABD44EA1EF5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[17]" "e[19]" "e[24]" "e[29]" "e[32]" "e[37]" "e[41]" "e[43]";
	setAttr ".ix" -type "matrix" 3.8492894976326011 0 0 0 0 1.2715919516266956 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".a" 0;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "CFDC464E-42A3-7608-25BF-91A1FAF2FC07";
	setAttr ".ics" -type "componentList" 1 "f[4:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 400 200 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1000 400 200 ;
	setAttr ".rs" 57799;
	setAttr ".ls" -type "double3" 0.71266292049724023 0.71266292049724023 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 983.5 275 100 ;
	setAttr ".cbx" -type "double3" 1016.5 525 300 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "F02DC0E9-430F-D6B9-8BC9-2E809555C42F";
	setAttr ".ics" -type "componentList" 1 "f[4:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 400 200 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1000 400 200 ;
	setAttr ".rs" 47034;
	setAttr ".lt" -type "double3" -5.8227454609933809e-014 -7.6333984815558958e-016 
		-6.2331428840303715 ;
	setAttr ".ls" -type "double3" 1 1 0.51625717756085576 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 983.5 310.9171142578125 128.73370361328125 ;
	setAttr ".cbx" -type "double3" 1016.5 489.0828857421875 271.26629638671875 ;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "3554E48B-4C8D-1761-C82F-D696613B5578";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 400 200 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.19999999999999996;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak7";
	rename -uid "CC963A28-4E02-5E4F-4FDD-D780D0A47B1A";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[16]" -type "float3" -0.18306167 1.5883813 1.2707047 ;
	setAttr ".tk[17]" -type "float3" -0.18306167 1.5883813 -1.2707047 ;
	setAttr ".tk[18]" -type "float3" -0.18306167 -1.5883813 1.2707047 ;
	setAttr ".tk[19]" -type "float3" -0.18306167 -1.5883813 -1.2707047 ;
	setAttr ".tk[20]" -type "float3" 0.18306167 1.5883813 1.2707047 ;
	setAttr ".tk[21]" -type "float3" 0.18306167 1.5883813 -1.2707047 ;
	setAttr ".tk[22]" -type "float3" 0.18306167 -1.5883813 -1.2707047 ;
	setAttr ".tk[23]" -type "float3" 0.18306167 -1.5883813 1.2707047 ;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "9A9CC911-4375-A9C8-833C-7E94833C3DB8";
	setAttr ".ics" -type "componentList" 4 "e[34]" "e[38]" "e[42]" "e[46]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit1";
	rename -uid "41037B77-466B-60C7-3703-CBABCD0592E8";
	setAttr -s 3 ".e[0:2]"  0.99258399 0 0;
	setAttr -s 3 ".d[0:2]"  -2147483614 -2147483632 -2147483606;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "553B3FC8-4254-168F-6A7E-67AD8414E224";
	setAttr -s 3 ".e[0:2]"  0 1 0;
	setAttr -s 3 ".d[0:2]"  -2147483603 -2147483637 -2147483605;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "18D83497-4E12-A10B-8F99-6CA8FCD71ED4";
	setAttr -s 3 ".e[0:2]"  0 1 0;
	setAttr -s 3 ".d[0:2]"  -2147483611 -2147483641 -2147483609;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "58BDA337-4158-52AD-09C3-D985735AF8F6";
	setAttr -s 3 ".e[0:2]"  0 0 1;
	setAttr -s 3 ".d[0:2]"  -2147483612 -2147483634 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "3377C555-435F-3C8B-B448-3EB86E4759F7";
	setAttr ".dc" -type "componentList" 4 "e[35:36]" "e[38:39]" "e[41:42]" "e[44:45]";
createNode groupId -n "groupId31";
	rename -uid "7F0B7CA6-4273-54E7-399B-4BAD51F28DF1";
	setAttr ".ihi" 0;
createNode polySplit -n "polySplit5";
	rename -uid "222F9A83-404F-D07B-C988-1BA4AF61CE75";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483638 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "FBB44BA4-4A72-C79A-005B-5F8BDD5B98C1";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483643 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "8DF5B071-4678-D06D-AD5F-4BAA6BFF9805";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[0]" "e[3]" "e[8]" "e[11]" "e[20]" "e[23]" "e[28]" "e[31]";
	setAttr ".ix" -type "matrix" 3.8492894976326011 0 0 0 0 1.2150767524380277 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".wt" 0.91057807207107544;
	setAttr ".dr" no;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode displayLayer -n "layer1";
	rename -uid "9AB1EB06-4D3E-5811-BF30-53BC868D5A6F";
	setAttr ".c" 6;
	setAttr ".do" 1;
createNode polyTweak -n "polyTweak8";
	rename -uid "968D03F0-42F4-8A54-8CD9-988A49FC0A44";
	setAttr ".uopa" yes;
	setAttr -s 22 ".tk";
	setAttr ".tk[32]" -type "float3" 0.94904864 0 0 ;
	setAttr ".tk[33]" -type "float3" 3.2063334 0 0 ;
	setAttr ".tk[34]" -type "float3" 3.2063334 0 0 ;
	setAttr ".tk[35]" -type "float3" 2.970685 0 0 ;
	setAttr ".tk[36]" -type "float3" 2.970685 0 0 ;
	setAttr ".tk[37]" -type "float3" 3.2063334 0 0 ;
	setAttr ".tk[38]" -type "float3" 3.2063334 0 0 ;
	setAttr ".tk[39]" -type "float3" 0.94904864 0 0 ;
	setAttr ".tk[40]" -type "float3" -0.96799272 0 0 ;
	setAttr ".tk[41]" -type "float3" -1.2036411 0 0 ;
	setAttr ".tk[42]" -type "float3" -1.2036411 0 0 ;
	setAttr ".tk[43]" -type "float3" -0.96799272 0 0 ;
	setAttr ".tk[44]" -type "float3" -0.96799272 0 0 ;
	setAttr ".tk[45]" -type "float3" -1.2036411 0 0 ;
	setAttr ".tk[46]" -type "float3" -1.2036411 0 0 ;
	setAttr ".tk[47]" -type "float3" -0.96799272 0 0 ;
createNode polySplit -n "polySplit7";
	rename -uid "031575A8-46A7-CD3A-0402-F8BA25E964BE";
	setAttr -s 6 ".e[0:5]"  1 0.95224702 0.047753301 0.047753301 0.047753301
		 0;
	setAttr -s 6 ".d[0:5]"  -2147483634 -2147483591 -2147483575 -2147483559 -2147483592 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit8";
	rename -uid "F09DD0EE-4FC3-DB9C-6166-D5B9C58FAFD4";
	setAttr -s 6 ".e[0:5]"  1 0.94985199 0.94985199 0.94985199 0.0501481
		 1;
	setAttr -s 6 ".d[0:5]"  -2147483647 -2147483555 -2147483556 -2147483557 -2147483591 -2147483635;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "11700E4E-45A1-57FF-996A-DA91EDF053A3";
	setAttr ".ics" -type "componentList" 1 "f[51]";
	setAttr ".ix" -type "matrix" 3.8492894976326011 0 0 0 0 1.2150767524380277 0 0 0 0 0.04324446896417506 0
		 1000 400 104.32444689641751 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 999.96368 400.00006 108.64888 ;
	setAttr ".rs" 52447;
	setAttr ".lt" -type "double3" -2.2737367544323206e-013 -5.6843418860808015e-014 
		-5.3618211536592497 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 981.45369614929507 255.95109915026353 108.64888323510334 ;
	setAttr ".cbx" -type "double3" 1018.4736737383329 544.04899355273574 108.64888356503245 ;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "6A0C034A-4A65-9587-568C-9EAF7EE5DD58";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1000 400 200 1;
createNode animCurveTL -n "pCube81_translateZ";
	rename -uid "3E45E203-4B7D-4525-C3A4-768720F68281";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 45 -185 90 0;
createNode groupId -n "groupId32";
	rename -uid "A088E748-4C52-F17B-B008-83BE67B05769";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "A65D5AD5-47D1-1976-92ED-0D943B8A883E";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 119 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 4 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".mcfr" 30;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr ".hwfr" 30;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId3.id" "pCubeShape23.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape23.iog.og[1].gco";
connectAttr "polySplitRing2.out" "pCubeShape23.i";
connectAttr "polyDelEdge1.out" "pCubeShape25.i";
connectAttr "polyDelEdge4.out" "pCubeShape30.i";
connectAttr "groupId4.id" "pCubeShape32.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape32.iog.og[1].gco";
connectAttr "polyDelEdge2.out" "pCubeShape33.i";
connectAttr "groupId12.id" "pCubeShape34.iog.og[0].gid";
connectAttr "set3.mwc" "pCubeShape34.iog.og[0].gco";
connectAttr "polyExtrudeFace2.out" "pCubeShape34.i";
connectAttr "groupId5.id" "pCubeShape44.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape44.iog.og[1].gco";
connectAttr "groupId6.id" "pCubeShape44.iog.og[2].gid";
connectAttr "set2.mwc" "pCubeShape44.iog.og[2].gco";
connectAttr "groupId9.id" "pCubeShape49.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape49.iog.og[0].gco";
connectAttr "groupId10.id" "pCubeShape49.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pCubeShape50.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape50.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape50.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pCubeShape51.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape51.iog.og[1].gco";
connectAttr "groupId13.id" "pCubeShape53.iog.og[0].gid";
connectAttr "set3.mwc" "pCubeShape53.iog.og[0].gco";
connectAttr "groupId14.id" "pCubeShape56.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape56.iog.og[1].gco";
connectAttr "groupId15.id" "pCubeShape57.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape57.iog.og[1].gco";
connectAttr "groupId16.id" "pCubeShape58.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape58.iog.og[1].gco";
connectAttr "groupId17.id" "pCubeShape59.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape59.iog.og[1].gco";
connectAttr "groupId18.id" "pCubeShape60.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape60.iog.og[1].gco";
connectAttr "groupId19.id" "pCubeShape61.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape61.iog.og[1].gco";
connectAttr "groupId20.id" "pCubeShape76.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape76.iog.og[1].gco";
connectAttr "layer1.di" "pCube81.do";
connectAttr "pCube81_translateZ.o" "pCube81.tz";
connectAttr "transformGeometry1.og" "pCubeShape81.i";
connectAttr "groupId21.id" "pCubeShape82.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape82.iog.og[1].gco";
connectAttr "groupId22.id" "pCubeShape83.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape83.iog.og[1].gco";
connectAttr "groupId23.id" "pCubeShape84.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape84.iog.og[1].gco";
connectAttr "groupId24.id" "pCubeShape85.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape85.iog.og[1].gco";
connectAttr "groupId25.id" "pCubeShape86.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape86.iog.og[1].gco";
connectAttr "groupId26.id" "pCubeShape87.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape87.iog.og[1].gco";
connectAttr "groupId27.id" "pCubeShape88.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape88.iog.og[1].gco";
connectAttr "groupId28.id" "pCubeShape89.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape89.iog.og[1].gco";
connectAttr "groupId29.id" "pCubeShape90.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape90.iog.og[1].gco";
connectAttr "layer1.di" "pCube122.do";
connectAttr "polySoftEdge1.out" "pCubeShape122.i";
connectAttr "layer1.di" "pCube123.do";
connectAttr "polyExtrudeFace7.out" "pCubeShape124.i";
connectAttr "groupId31.id" "pCubeShape126.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape126.iog.og[1].gco";
connectAttr "groupId32.id" "pCubeShape140.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape140.iog.og[1].gco";
connectAttr "groupId33.id" "pCubeShape141.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape141.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupId3.msg" "set1.gn" -na;
connectAttr "groupId4.msg" "set1.gn" -na;
connectAttr "groupId5.msg" "set1.gn" -na;
connectAttr "groupId11.msg" "set1.gn" -na;
connectAttr "groupId14.msg" "set1.gn" -na;
connectAttr "groupId15.msg" "set1.gn" -na;
connectAttr "groupId16.msg" "set1.gn" -na;
connectAttr "groupId17.msg" "set1.gn" -na;
connectAttr "groupId18.msg" "set1.gn" -na;
connectAttr "groupId19.msg" "set1.gn" -na;
connectAttr "groupId20.msg" "set1.gn" -na;
connectAttr "groupId21.msg" "set1.gn" -na;
connectAttr "groupId22.msg" "set1.gn" -na;
connectAttr "groupId23.msg" "set1.gn" -na;
connectAttr "groupId24.msg" "set1.gn" -na;
connectAttr "groupId25.msg" "set1.gn" -na;
connectAttr "groupId26.msg" "set1.gn" -na;
connectAttr "groupId27.msg" "set1.gn" -na;
connectAttr "groupId28.msg" "set1.gn" -na;
connectAttr "groupId29.msg" "set1.gn" -na;
connectAttr "groupId31.msg" "set1.gn" -na;
connectAttr "groupId32.msg" "set1.gn" -na;
connectAttr "groupId33.msg" "set1.gn" -na;
connectAttr "pCubeShape23.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape32.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape44.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape51.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape56.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape57.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape58.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape59.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape60.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape61.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape76.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape82.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape83.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape84.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape85.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape86.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape87.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape88.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape89.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape90.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape126.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape140.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape141.iog.og[1]" "set1.dsm" -na;
connectAttr "polyCube1.out" "polyDelEdge1.ip";
connectAttr "groupParts1.og" "polySplitRing1.ip";
connectAttr "pCubeShape23.wm" "polySplitRing1.mp";
connectAttr "|pCube23|polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId3.id" "groupParts1.gi";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape23.wm" "polySplitRing2.mp";
connectAttr "polyCube2.out" "polyDelEdge2.ip";
connectAttr "groupId6.msg" "set2.gn" -na;
connectAttr "pCubeShape44.iog.og[2]" "set2.dsm" -na;
connectAttr "|pCube30|polySurfaceShape2.o" "polySplitRing3.ip";
connectAttr "pCubeShape30.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape30.wm" "polySplitRing4.mp";
connectAttr "polyTweak1.out" "polySplitRing5.ip";
connectAttr "pCubeShape30.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyDelEdge3.ip";
connectAttr "polySplitRing5.out" "polyTweak2.ip";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "groupId12.msg" "set3.gn" -na;
connectAttr "groupId13.msg" "set3.gn" -na;
connectAttr "pCubeShape34.iog.og[0]" "set3.dsm" -na;
connectAttr "pCubeShape53.iog.og[0]" "set3.dsm" -na;
connectAttr "groupParts2.og" "polyExtrudeFace1.ip";
connectAttr "pCubeShape34.wm" "polyExtrudeFace1.mp";
connectAttr "|pCube34|polySurfaceShape3.o" "groupParts2.ig";
connectAttr "groupId12.id" "groupParts2.gi";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape34.wm" "polyExtrudeFace2.mp";
connectAttr "polyCube4.out" "polySplitRing6.ip";
connectAttr "pCubeShape122.wm" "polySplitRing6.mp";
connectAttr "polyTweak3.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape122.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing6.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing7.ip";
connectAttr "pCubeShape122.wm" "polySplitRing7.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak4.ip";
connectAttr "polySplitRing7.out" "polyDelEdge5.ip";
connectAttr "polyTweak5.out" "polySoftEdge1.ip";
connectAttr "pCubeShape122.wm" "polySoftEdge1.mp";
connectAttr "polyDelEdge5.out" "polyTweak5.ip";
connectAttr "polySurfaceShape4.o" "polyBevel1.ip";
connectAttr "pCubeShape124.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape124.wm" "polyExtrudeFace4.mp";
connectAttr "polyTweak6.out" "polySoftEdge2.ip";
connectAttr "pCubeShape124.wm" "polySoftEdge2.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak6.ip";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCubeShape124.wm" "polySoftEdge3.mp";
connectAttr "polyCube3.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape81.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace5.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape81.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak7.out" "polyBevel2.ip";
connectAttr "pCubeShape81.wm" "polyBevel2.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak7.ip";
connectAttr "polyBevel2.out" "polyDelEdge6.ip";
connectAttr "polyDelEdge6.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "deleteComponent1.ig";
connectAttr "polySoftEdge3.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polySplitRing8.ip";
connectAttr "pCubeShape124.wm" "polySplitRing8.mp";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "polySplitRing8.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape124.wm" "polyExtrudeFace7.mp";
connectAttr "deleteComponent1.og" "transformGeometry1.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape25.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape26.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape27.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape28.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape29.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape30.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape31.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape32.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape33.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape34.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape35.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape36.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape37.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape38.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape39.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape40.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape41.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape42.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape43.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape44.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape47.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape48.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape50.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape50.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape49.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape49.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape51.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape52.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape53.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape54.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape55.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape56.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape57.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape58.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape59.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape60.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape61.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape62.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape63.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape64.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape65.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape66.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape67.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape68.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape69.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape70.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape71.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape72.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape73.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape74.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape75.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape76.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape79.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape80.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape81.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape82.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape83.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape84.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape85.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape86.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape87.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape88.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape89.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape90.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape91.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape92.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape93.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape94.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape95.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape96.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape97.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape98.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape99.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape100.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape101.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape102.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape103.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape104.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape105.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape106.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape107.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape108.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape109.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape110.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape111.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape112.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape113.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape114.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape115.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape116.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape117.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape118.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape119.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape120.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape121.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape122.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape123.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape124.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape126.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape127.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape128.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape129.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape130.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape131.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape132.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape133.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape134.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape135.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape136.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape137.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape138.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape139.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape140.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape141.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape142.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape143.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape144.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape145.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
// End of scene5.ma
