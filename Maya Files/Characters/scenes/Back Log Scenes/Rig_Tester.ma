//Maya ASCII 2017 scene
//Name: Rig_Tester.ma
//Last modified: Tue, Nov 08, 2016 03:00:38 PM
//Codeset: 1252
requires maya "2017";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "01FF834D-442F-B620-9516-84847C362338";
	setAttr ".t" -type "double3" -243.73526184978411 313.38491394357322 -475.19689022646992 ;
	setAttr ".r" -type "double3" 698.72811783237114 5611.400000000147 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D14C102C-47D7-0BB4-7596-4B9ECE836592";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 586.71899453570745;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 54.166061401367188 7.1054273576010019e-015 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "A051BDF6-48B4-B5F6-9826-D09A06412DCA";
	setAttr ".t" -type "double3" 30.002847730169076 1000.6378204329206 -0.33930168144895639 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "B166A376-4C7F-CC25-C5F3-0BBBCC12D420";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 837.24528973233043;
	setAttr ".ow" 129.25162265707806;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 30.002847730169076 163.39253070059002 -0.3393016814491423 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "9CBB52ED-4C72-225C-7BF3-99BE8D05829D";
	setAttr ".t" -type "double3" -2.6107323095778945 86.284807366186897 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "990BD43B-49C3-402C-32DA-29879BB7BDAB";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 144.78870945468384;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "28A847AE-4F8A-55D2-75DE-84AAB63A089F";
	setAttr ".t" -type "double3" 1000.1 118.79764358653698 -22.98628199602647 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "CB086974-4745-072B-3C81-E8A19ADBBA18";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 237.66869274075094;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "back";
	rename -uid "49DC2761-490F-1F9F-719F-81A8B1702B19";
	setAttr ".t" -type "double3" 2.929338271460967 117.68773252040739 -1000.1 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode camera -n "backShape" -p "back";
	rename -uid "E6C039E1-49ED-5B78-F92D-C899BC5099A9";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 402.13594345231979;
	setAttr ".imn" -type "string" "back1";
	setAttr ".den" -type "string" "back1_depth";
	setAttr ".man" -type "string" "back1_mask";
	setAttr ".hc" -type "string" "viewSet -b %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "Controls";
	rename -uid "CAAB0DD2-4769-C696-A2C7-C8AE452D3797";
createNode transform -n "ctrl_master" -p "Controls";
	rename -uid "2E7A16C8-43E8-BE2F-B1D7-A79854EB3FB8";
createNode nurbsCurve -n "ctrl_masterShape" -p "ctrl_master";
	rename -uid "D6B76FD1-4E42-E23D-E89E-6497CA3AD661";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		47.51197518820878 2.9092694167704131e-015 -47.511975188208709
		-7.6658128738565139e-015 4.1143282657939848e-015 -67.192079686298797
		-47.511975188208737 2.9092694167704146e-015 -47.511975188208737
		-67.192079686298797 1.1922293990609863e-030 -1.9470583679981244e-014
		-47.511975188208751 -2.9092694167704139e-015 47.511975188208723
		-2.0246284938060909e-014 -4.1143282657939856e-015 67.192079686298797
		47.511975188208709 -2.909269416770415e-015 47.511975188208744
		67.192079686298797 -2.2098137375199025e-030 3.6088997073416771e-014
		47.51197518820878 2.9092694167704131e-015 -47.511975188208709
		-7.6658128738565139e-015 4.1143282657939848e-015 -67.192079686298797
		-47.511975188208737 2.9092694167704146e-015 -47.511975188208737
		;
createNode transform -n "ctrl_potBody" -p "ctrl_master";
	rename -uid "7FBA99D2-4D08-91FC-9FE4-7CBE13483FE4";
createNode transform -n "ctrl_pot" -p "ctrl_potBody";
	rename -uid "77307B1B-4DB3-52BC-34A2-259E5237B62A";
createNode nurbsCurve -n "ctrl_potShape" -p "ctrl_pot";
	rename -uid "64CBD235-4827-1AB1-6AC1-ABBDFA4FF2C7";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		37.308590869285588 0 -37.308590869285581
		37.308590869285588 0 37.308590869285581
		-37.308590869285574 0 37.308590869285581
		-37.308590869285574 0 -37.308590869285581
		37.308590869285588 0 -37.308590869285581
		;
createNode transform -n "ctrl_body" -p "ctrl_potBody";
	rename -uid "0BED8E96-42E1-C280-1DF3-5CB3258A44F1";
	setAttr ".rp" -type "double3" 0 54.166061401367188 0 ;
	setAttr ".sp" -type "double3" 0 54.166061401367188 0 ;
createNode nurbsCurve -n "ctrl_bodyShape" -p "ctrl_body";
	rename -uid "ED38B5FC-404E-0B6E-39DE-07B505E7EA35";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		43.160365191772165 54.166061401367188 -43.160365191772101
		-6.9637029783924066e-015 54.166061401367195 -61.037973811179803
		-43.160365191772122 54.166061401367188 -43.160365191772122
		-61.037973811179803 54.166061401367188 -1.7687277760944449e-014
		-43.160365191772136 54.166061401367188 43.160365191772115
		-1.8391932733628983e-014 54.16606140136718 61.037973811179803
		43.160365191772101 54.166061401367188 43.160365191772129
		61.037973811179803 54.166061401367188 3.2783614802863924e-014
		43.160365191772165 54.166061401367188 -43.160365191772101
		-6.9637029783924066e-015 54.166061401367195 -61.037973811179803
		-43.160365191772122 54.166061401367188 -43.160365191772122
		;
createNode transform -n "ctrl_mid_back" -p "ctrl_body";
	rename -uid "D1382E3C-485A-55D5-3EAD-46A0D088EDE7";
	setAttr ".rp" -type "double3" -0.32828587293624878 96.857864379882813 0.25 ;
	setAttr ".sp" -type "double3" -0.32828587293624878 96.857864379882813 0.25 ;
createNode nurbsCurve -n "ctrl_mid_backShape" -p "ctrl_mid_back";
	rename -uid "821077D7-4D75-616B-A0C6-CBA3FFE4AF97";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.32828586525130576 102.75604770588399 32.014449888326808
		-0.32828586525130327 96.857861855010484 29.571341315497889
		-0.32828586525130049 90.959676004136981 32.014449888326808
		-0.32828586525129905 88.51656743130809 37.912635739200297
		-0.32828586525129977 90.959676004136981 43.810821590073786
		-0.32828586525130227 96.857861855010484 46.253930162902691
		-0.32828586525130504 102.75604770588397 43.810821590073786
		-0.32828586525130649 105.19915627871288 37.912635739200297
		-0.32828586525130576 102.75604770588399 32.014449888326808
		-0.32828586525130327 96.857861855010484 29.571341315497889
		-0.32828586525130049 90.959676004136981 32.014449888326808
		;
createNode nurbsCurve -n "ctrl_mid_backShape1" -p "ctrl_mid_back";
	rename -uid "A1FC634A-4651-9D8C-E122-EABD14A7C192";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.5698999856221985 96.857861855010484 32.014449888326808
		-0.32828586525130371 96.857861855010484 29.571341315497889
		-6.2264717161247995 96.857861855010484 32.014449888326808
		-8.6695802889536964 96.857861855010484 37.912635739200297
		-6.2264717161248004 96.857861855010484 43.810821590073786
		-0.32828586525130526 96.857861855010484 46.253930162902691
		5.5698999856221905 96.857861855010484 43.810821590073786
		8.0130085584510908 96.857861855010484 37.912635739200297
		5.5698999856221985 96.857861855010484 32.014449888326808
		-0.32828586525130371 96.857861855010484 29.571341315497889
		-6.2264717161247995 96.857861855010484 32.014449888326808
		;
createNode nurbsCurve -n "ctrl_mid_back2Shape" -p "ctrl_mid_back";
	rename -uid "E2A5F1CE-43FC-A365-C03D-BAABE52F35ED";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-6.2264717161248004 102.75604770588399 37.912635739200297
		-8.6695802889536981 96.857861855010484 37.912635739200297
		-6.2264717161247969 90.959676004136981 37.912635739200297
		-0.32828586525129966 88.51656743130809 37.912635739200297
		5.5698999856221967 90.959676004136981 37.912635739200297
		8.0130085584510926 96.857861855010484 37.912635739200297
		5.5698999856221922 102.75604770588399 37.912635739200297
		-0.32828586525130388 105.19915627871288 37.912635739200297
		-6.2264717161248004 102.75604770588399 37.912635739200297
		-8.6695802889536981 96.857861855010484 37.912635739200297
		-6.2264717161247969 90.959676004136981 37.912635739200297
		;
createNode transform -n "mid_back_clstr" -p "ctrl_mid_back";
	rename -uid "727E9A62-4134-DB21-02C7-F1A9FEDF3C06";
	setAttr ".rp" -type "double3" -1.0148019021405439e-014 96.607867882842569 0 ;
	setAttr ".sp" -type "double3" -1.0148019021405439e-014 96.607867882842569 0 ;
createNode clusterHandle -n "mid_back_clstrShape" -p "mid_back_clstr";
	rename -uid "54307A1A-458F-7672-3938-808868EB18AF";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -1.0148019021405439e-014 96.607867882842569 0 ;
createNode transform -n "ctrl_bot_back" -p "ctrl_body";
	rename -uid "1295B487-4216-4D93-25B5-C398E9A70525";
	setAttr ".t" -type "double3" 0 -2.1316282072803006e-014 0 ;
	setAttr ".rp" -type "double3" 0 54.166061401367188 0 ;
	setAttr ".sp" -type "double3" 0 54.166061401367188 0 ;
createNode nurbsCurve -n "ctrl_bot_backShape" -p "ctrl_bot_back";
	rename -uid "A1FDEC64-465D-B96B-71BF-38B644898D03";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		34.925632339366118 54.166061401367188 -34.925632339366068
		-5.6350711784581115e-015 54.166061401367188 -49.392302928787892
		-34.925632339366089 54.166061401367188 -34.925632339366089
		-49.392302928787892 54.166061401367188 -1.431265369090887e-014
		-34.925632339366089 54.166061401367188 34.925632339366075
		-1.4882864818473141e-014 54.166061401367188 49.392302928787899
		34.925632339366068 54.166061401367188 34.925632339366089
		49.392302928787892 54.166061401367188 2.652870225431966e-014
		34.925632339366118 54.166061401367188 -34.925632339366068
		-5.6350711784581115e-015 54.166061401367188 -49.392302928787892
		-34.925632339366089 54.166061401367188 -34.925632339366089
		;
createNode transform -n "lower_back_cstr" -p "ctrl_bot_back";
	rename -uid "56D36DF6-4C67-7A3D-091D-8BB309C91941";
	setAttr ".rp" -type "double3" -0.32200164244302343 61.376479170721595 0 ;
	setAttr ".sp" -type "double3" -0.32200164244302343 61.376479170721595 0 ;
createNode clusterHandle -n "lower_back_cstrShape" -p "lower_back_cstr";
	rename -uid "5D5B94AB-4916-EC2D-CFBB-648341359008";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -0.32200164244302343 61.376479170721595 0 ;
createNode transform -n "ctrl_upper_torso" -p "ctrl_body";
	rename -uid "AF71263B-4406-269E-8AA0-FDAA4B7AC864";
	setAttr ".rp" -type "double3" -0.1517108827829361 97.428298950195313 0 ;
	setAttr ".sp" -type "double3" -0.1517108827829361 97.428298950195313 0 ;
createNode nurbsCurve -n "ctrl_upper_torsoShape" -p "ctrl_upper_torso";
	rename -uid "3C341423-4D29-DD32-D9DF-5F9935ADB249";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		28.752724255586138 131.67890930175781 -40.247775410203751
		-4.6391042023363276e-015 144.40346257480894 -37.906454195173595
		-28.752724255586102 131.67890930175781 -40.247775410203772
		-40.66249259742375 96.746120074777338 0.2499999999999834
		-28.752724255586109 131.67890930175781 40.747775410203758
		-1.2252402593621561e-014 144.40346257480894 38.406454195173609
		28.752724255586088 131.67890930175781 40.747775410203779
		40.66249259742375 96.746120074777338 0.25000000000003075
		28.752724255586138 131.67890930175781 -40.247775410203751
		-4.6391042023363276e-015 144.40346257480894 -37.906454195173595
		-28.752724255586102 131.67890930175781 -40.247775410203772
		;
createNode transform -n "upper_torso_cstr" -p "ctrl_upper_torso";
	rename -uid "CD847FEA-4550-A13D-A234-29A5A2A022D2";
	setAttr ".rp" -type "double3" -7.8886090522101181e-031 131.42890764157616 0 ;
	setAttr ".sp" -type "double3" -7.8886090522101181e-031 131.42890764157616 0 ;
createNode clusterHandle -n "upper_torso_cstrShape" -p "upper_torso_cstr";
	rename -uid "0B9CCE6E-436D-4597-E3E9-9AB79AF5F83F";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -7.8886090522101181e-031 131.42890764157616 0 ;
createNode transform -n "ctrl_shoulder_l" -p "ctrl_upper_torso";
	rename -uid "29D887D5-48A5-CB9C-B9CF-FAA39B30D96E";
	setAttr ".rp" -type "double3" -30 139.97804260253906 1.5777218104420236e-030 ;
	setAttr ".sp" -type "double3" -30 139.97804260253906 -1.2621774483536189e-029 ;
createNode nurbsCurve -n "ctrl_shoulder_lShape" -p "ctrl_shoulder_l";
	rename -uid "604AF53B-4A0B-CDB4-0798-80BD5AE7F14C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-30 139.97804260253935 2.5360585763261473e-014
		-39.762669302347661 160.91415448407008 3.0732507453543923e-014
		-58.971707874130288 170.14151588308772 3.4149413992818705e-014
		-34.483906238783106 181.56036531703074 3.4149413992818731e-014
		-39.762669302347661 160.91415448407008 3.0489914626914679e-014
		;
createNode transform -n "ctrl_elbow_l" -p "ctrl_shoulder_l";
	rename -uid "287C048A-468B-90D8-EC38-92B6D85B7164";
	setAttr ".r" -type "double3" 0.012248297361743533 -0.047840411976204977 0.16773844846160146 ;
	setAttr ".rp" -type "double3" -68 139.74610900878906 -4.6537447018820538e-015 ;
	setAttr ".sp" -type "double3" -68 139.74610900878906 -4.6537447018820538e-015 ;
createNode nurbsCurve -n "ctrl_elbow_lShape" -p "ctrl_elbow_l";
	rename -uid "1CB33089-43D1-13D6-6D0B-C2AC9F57DDB0";
	addAttr -ci true -k true -sn "ll" -ln "lockLength" -min 0 -max 1 -at "bool";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-68 157.05646271698549 -17.31035370819642
		-68 139.74610900878906 -24.48053698360679
		-68 122.43575530059263 -17.31035370819643
		-68 115.26557202518228 -1.1747591865052773e-014
		-68 122.43575530059263 17.310353708196416
		-68 139.74610900878906 24.48053698360679
		-68 157.05646271698549 17.310353708196427
		-68 164.22664599239585 8.4947994670403935e-015
		-68 157.05646271698549 -17.31035370819642
		-68 139.74610900878906 -24.48053698360679
		-68 122.43575530059263 -17.31035370819643
		;
createNode transform -n "ctrl_shoulder_r" -p "ctrl_upper_torso";
	rename -uid "0E65F50D-446B-B5F2-5D83-20AC7E066ED9";
	setAttr ".r" -type "double3" 0 0 -0.18525105347517867 ;
	setAttr ".rp" -type "double3" 29.999999999999996 139.97804260253906 1.5777218104420236e-030 ;
	setAttr ".sp" -type "double3" 29.999999999999996 139.97804260253906 -1.2621774483536189e-029 ;
createNode nurbsCurve -n "ctrl_shoulder_rShape" -p "ctrl_shoulder_r";
	rename -uid "CC1E9C47-4967-B05D-B2F1-B89FB1D995B7";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		30.000000000000192 139.97804260253923 2.5360585763261473e-014
		39.762669302347831 160.91415448406997 3.0732507453543923e-014
		34.483906238783185 181.56036531703057 3.4149413992818705e-014
		58.971707874130459 170.14151588308778 3.4149413992818731e-014
		39.762669302347831 160.91415448406997 3.0489914626914679e-014
		;
createNode transform -n "ctrl_elbow_r" -p "ctrl_shoulder_r";
	rename -uid "252F645A-46BB-FEFA-37CA-5BAD9A38D2C9";
	setAttr ".rp" -type "double3" 68 139.74610900878906 -4.6537447018820538e-015 ;
	setAttr ".sp" -type "double3" 68 139.74610900878906 -4.6537447018820538e-015 ;
createNode nurbsCurve -n "ctrl_elbow_rShape" -p "ctrl_elbow_r";
	rename -uid "D0F18D5C-4B3D-5F51-65B7-9B82782C0C5C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		68 157.05646271698549 -17.31035370819642
		68 139.74610900878906 -24.48053698360679
		68 122.43575530059263 -17.31035370819643
		68 115.26557202518228 -1.1747591865052773e-014
		68 122.43575530059263 17.310353708196416
		68 139.74610900878906 24.48053698360679
		68 157.05646271698549 17.310353708196427
		68 164.22664599239585 8.4947994670403935e-015
		68 157.05646271698549 -17.31035370819642
		68 139.74610900878906 -24.48053698360679
		68 122.43575530059263 -17.31035370819643
		;
createNode transform -n "ctrl_head" -p "ctrl_upper_torso";
	rename -uid "94B15F58-4353-9322-47C9-2788E1F4DB13";
	setAttr ".rp" -type "double3" 7.9028450450380185e-013 138.22842407226562 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 7.9028450450380185e-013 138.22842407226562 -3.5527136788005009e-015 ;
createNode nurbsCurve -n "ctrl_headShape" -p "ctrl_head";
	rename -uid "A8263619-44CD-8272-FB8B-A2A21384D209";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		29.684451929978803 197.80676369416832 -25.707489468779563
		-8.3421473501394033e-015 203.95461498467304 -36.355880261311619
		-29.684451929978781 197.80676369416832 -25.707489468779578
		-41.9801545109882 182.96453772917894 3.9202966955788665e-014
		-29.684451929978792 168.12231176418956 25.70748946877967
		-1.6202153522625272e-014 161.97446047368484 36.355880261311725
		29.684451929978756 168.12231176418953 25.707489468779681
		41.980154510988186 182.96453772917894 6.9264805712226185e-014
		29.684451929978803 197.80676369416832 -25.707489468779563
		-8.3421473501394033e-015 203.95461498467304 -36.355880261311619
		-29.684451929978781 197.80676369416832 -25.707489468779578
		;
createNode transform -n "rig_grp" -p "Controls";
	rename -uid "DE893098-4C39-A32B-AF53-4FB4015322F9";
createNode ikHandle -n "ik_spine_handle" -p "rig_grp";
	rename -uid "846E8847-43D2-CBAD-547A-65815546C2DD";
	setAttr ".t" -type "double3" -0.0020263523682997886 138.22842703193083 0.024713019019702152 ;
	setAttr ".r" -type "double3" 0.025512768619650541 0.00010781771353092175 -0.21095528072148839 ;
	setAttr ".roc" yes;
createNode transform -n "ik_spine_curve" -p "rig_grp";
	rename -uid "E3C12477-4EAA-19BB-BC8E-9CAF31A4DFDF";
	setAttr ".t" -type "double3" -1.0148019021405438e-014 0 0 ;
createNode nurbsCurve -n "ik_spine_curveShape" -p "ik_spine_curve";
	rename -uid "D62BE14E-429E-DB09-FBE8-00A4B361999B";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "ik_spine_curveShapeOrig" -p "ik_spine_curve";
	rename -uid "D3C0B217-4D02-6E00-69D1-909E1E229C96";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 43.262503942680354 84.062914165045569 84.062914165045569 84.062914165045569
		
		5
		-1.5777218104420234e-030 54.16606184693309 0
		-0.64400328488604686 68.5868964945101 0
		-1.5777218104421746e-030 96.607867882842569 0
		0 124.62883927117365 0
		-1.5777218104420236e-030 138.22897601197869 0
		;
createNode joint -n "jnt_origin";
	rename -uid "12E452EE-464F-427A-42BE-1888C8485690";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.57230964874220991 -1.3926860721763035 -0.26257625752009517 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_body" -p "jnt_origin";
	rename -uid "B19E00E6-4E15-A9B7-FCC6-27A2DFBBB8ED";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 40.352460557501651 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_pot" -p "jnt_body";
	rename -uid "5002F0F8-4999-EE17-30D8-CD9B16DFD758";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.0148019021405439e-014 13.813601289431418 0 ;
	setAttr ".r" -type "double3" 0.0086684834422310481 -1.5252930293233399e-005 0.2016333383420067 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 0.99999385132493424 0.0035067523900789263 0 0 -0.0035067523900789263 0.99999385132493424 0 0
		 0 0 1 0 -1.0148019021405439e-014 54.166061846933069 0 1;
	setAttr ".radi" 10;
createNode joint -n "spine01" -p "jnt_pot";
	rename -uid "3412A027-4B4B-2515-E32A-B7BC3A2CCC45";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 43.262503942680354 0 ;
	setAttr ".r" -type "double3" 0.016844400091960022 6.0648778566477117e-005 -0.41258863296111048 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 0.99999308685042898 -0.0037183667584653763 0 0 0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 -0.15171088910180347 97.42829978253414 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_torso" -p "spine01";
	rename -uid "0F0AFE81-4688-4EF1-13D5-4A8658684D74";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 40.800410222365215 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 0.99999308685042898 -0.0037183667584653763 0 0 0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 7.9028450450380205e-013 138.22842794556092 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_head" -p "jnt_torso";
	rename -uid "2A05E8D9-42D4-CCD5-E6C3-A2B9DD27A71E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -129.80461727264321 7.2055924544757293 84.033024310498547 ;
	setAttr ".bps" -type "matrix" 0.10680253104629521 0.98633691854694849 -0.12543007005266105 0
		 0.6260779411993912 -0.16471561548401611 -0.76216479685121463 0 -0.77241156834011537 0.0028721293550296443 -0.63511583192936638 0
		 0.14338550111459436 176.7895810491255 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_eye_r" -p "jnt_head";
	rename -uid "112CF605-4A33-F488-FBEA-57A045196F0B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 2.5600364406079656e-014 38.050047542593475 -7.6882152712063823e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999994 50.57142396597051 80.654556167534864 ;
	setAttr ".bps" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 23.965680929120474 170.52214404895116 -29.000406755479819 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_eye_l" -p "jnt_head";
	rename -uid "6B81655C-498D-6B5A-5691-A88A18B0636C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -4.9238195782919192 8.1308569417116949 37.109166324320405 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999994 50.57142396597051 80.654556167534864 ;
	setAttr ".bps" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 -23.95549007837117 170.70033923916614 -29.148076936850142 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_mouth" -p "jnt_head";
	rename -uid "CD23EA6B-43D7-29D8-3978-A98CA7EB4427";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -8.772693246739399 26.885707230811629 20.641875523607148 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999994 50.57142396597051 80.654556167534864 ;
	setAttr ".bps" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 0.094964442355380596 163.76754014716695 -32.500962007041927 1;
	setAttr ".radi" 10;
createNode parentConstraint -n "jnt_head_parentConstraint1" -p "jnt_head";
	rename -uid "86BD8EDA-4C80-3751-5A1B-1A9F81803C8F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_headW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.14338550111380391 38.561156976859877 3.5527136788005009e-015 ;
	setAttr ".tg[0].tor" -type "double3" -129.80461727264321 7.2055924544757293 83.8199770976141 ;
	setAttr ".lr" -type "double3" -9.2519360515663891 1.5209609919396521 -0.13022211957375052 ;
	setAttr ".rst" -type "double3" 0 38.561419684426539 0 ;
	setAttr ".rsrr" -type "double3" 3.1805546814635176e-015 -3.1805546814635176e-015 
		-6.3611093629270351e-015 ;
	setAttr -k on ".w0";
createNode joint -n "jnt_shoulder_r" -p "jnt_torso";
	rename -uid "8353B888-4C42-E45B-13B7-C18A8245008D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 180 7.0622500768802529e-031 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.0069515601636593915 0.99997583761373521 3.9595711295478152e-019 0
		 0.99997583761373521 -0.0069515601636596135 1.2246403980182624e-016 0 1.2246383329808592e-016 -4.5536859488110364e-019 -1 0
		 30.004321461590944 139.63398368078037 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_elbow_r" -p "jnt_shoulder_r";
	rename -uid "EC469AA1-4E65-E386-3B49-CDA97B9522AF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 0.0069515601636593915 0.99997583761373521 3.9595711295478152e-019 0
		 0.99997583761373521 -0.0069515601636596135 1.2246403980182624e-016 0 1.2246383329808592e-016 -4.5536859488110364e-019 -1 0
		 68.003403290912885 139.36982439456128 -2.4324290544609439e-020 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_paw_r" -p "jnt_elbow_r";
	rename -uid "9DBF717C-48C1-55A1-66A0-6A9E372C063E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -5.2180482157382334e-015 47 -1.0436096431476475e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 7.0622500768802529e-031 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.99997583761373543 -0.0069515601636593932 1.2246403980182629e-016 0
		 0.0069515601636589491 0.99997583761373543 -1.2206872280178063e-016 0 -1.2161251270773539e-016 1.2291708947071655e-016 1 0
		 115.00226765875844 139.04310106686927 1.6191881977871764e-014 1;
	setAttr ".radi" 10;
createNode parentConstraint -n "jnt_elbow_r_parentConstraint1" -p "jnt_elbow_r";
	rename -uid "10F0C180-4432-5557-383D-67B2F4CE7401";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_elbow_rW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.0051711627334753985 -0.25340749416076847 
		4.6537203775915084e-015 ;
	setAttr ".tg[0].tor" -type "double3" 180 7.0166607909739984e-015 89.786952787115524 ;
	setAttr ".lr" -type "double3" -6.9906185999253843e-015 7.0166607909739984e-015 1.9083328088781101e-014 ;
	setAttr ".rst" -type "double3" -2.8421709430404007e-014 38 4.6536578367599419e-015 ;
	setAttr ".rsrr" -type "double3" -6.9906185999253843e-015 7.0166607909739984e-015 
		-4.2804897543588047e-031 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "jnt_shoulder_r_parentConstraint1" -p "jnt_shoulder_r";
	rename -uid "CD413E1E-4018-AA03-3338-EA81E157044B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_shoulder_rW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.0054338624171919037 -0.34404315108901073 
		-1.5777218104420236e-030 ;
	setAttr ".tg[0].tor" -type "double3" 180 7.0622500768802529e-031 89.786952787115538 ;
	setAttr ".lr" -type "double3" -9.3749674992391174 0.039368186369626391 0.18202301882615629 ;
	setAttr ".rst" -type "double3" 30.000000000000021 1.5171329968104033 0 ;
	setAttr -k on ".w0";
createNode joint -n "jnt_shoulder_l" -p "jnt_torso";
	rename -uid "0EB557EF-44FA-9302-638D-4587BDACC54B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 90.349701343084391 ;
	setAttr ".bps" -type "matrix" -0.0023850622471857535 0.9999971557349937 0 0 -0.9999971557349937 -0.0023850622471857535 0 0
		 0 0 1 0 -29.99328893444342 140.0890334473163 0 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_elbow_l" -p "jnt_shoulder_l";
	rename -uid "6CCB2177-4531-6268-E011-E7B7A5A67DDB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -0.34970134308385187 ;
	setAttr ".bps" -type "matrix" 0.0037183667584559468 0.99999308685042909 0 0 -0.99999308685042909 0.0037183667584559468 0 0
		 0 0 1 0 -67.993888648924923 139.99839939377961 -4.6537445170846357e-015 1;
	setAttr ".radi" 10;
createNode joint -n "jnt_paw_l" -p "jnt_elbow_l";
	rename -uid "725E9A8D-4A69-1BEF-4E0A-9299D0AB5082";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 4.5990577929911875e-013 49.041367281439577 1.5879248213175413e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.99999308685042931 -0.0037183667584557256 0 0 0.0037183667584557256 0.99999308685042931 0 0
		 0 0 1 0 -117.03491690005731 140.1807531836686 1.1225503696090778e-014 1;
	setAttr ".radi" 10;
createNode parentConstraint -n "jnt_elbow_l_parentConstraint1" -p "jnt_elbow_l";
	rename -uid "09699B5A-421A-BB3E-E818-F0B46C91E126";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_elbow_lW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.0061113510750772093 0.25229038499054468 1.847974181196489e-022 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 89.786952787116093 ;
	setAttr ".lr" -type "double3" -0.04779453871082541 -0.012426096598886946 0.16774874471055501 ;
	setAttr ".rst" -type "double3" 0 38.000707798564918 -4.6537445170846357e-015 ;
	setAttr ".rsrr" -type "double3" 0 0 -2.2860236773019026e-015 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "jnt_shoulder_l_parentConstraint1" -p "jnt_shoulder_l";
	rename -uid "B19A1902-4D7A-9343-5CF9-7D870EED32A7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_shoulder_lW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.0067110655565869592 0.11099084477723409 -1.5777218104420236e-030 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 90.136654130199943 ;
	setAttr ".lr" -type "double3" -9.3749287852194616 0.047773276038764162 -0.0039171174425044484 ;
	setAttr ".rst" -type "double3" -30.000000000000021 1.7490665905603748 0 ;
	setAttr ".rsrr" -type "double3" 0 0 -6.3611093629270335e-015 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "spine01";
	rename -uid "8CA209BD-4B5A-5C4D-0B60-78B3CD2845A7";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode parentConstraint -n "jnt_body_parentConstraint1" -p "jnt_body";
	rename -uid "13D85811-48AC-24B4-AEE3-AEA6AEC7702E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_bot_backW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 -13.813600843865515 0 ;
	setAttr ".rst" -type "double3" 0 40.352460557501651 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "jnt_origin_parentConstraint1" -p "jnt_origin";
	rename -uid "BB6EE777-4E8B-22BF-A193-7896B075D52B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ctrl_potW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "sagaro_body";
	rename -uid "011C43EB-445C-9418-4D2A-3E83DF3A683F";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -2.1240942478179932 126.05982780456543 0.51991462707519531 ;
	setAttr ".sp" -type "double3" -2.1240942478179932 126.05982780456543 0.51991462707519531 ;
createNode mesh -n "sagaro_bodyShape" -p "sagaro_body";
	rename -uid "945F833F-4A15-D5C6-D20C-059ABA858B46";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "sagaro_bodyShapeOrig" -p "sagaro_body";
	rename -uid "30CC8534-418A-E816-D806-738C25093691";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 2239 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.82527405 0.86411792 0.84720749
		 0.85276783 0.85222089 0.86968207 0.83258116 0.88134676 0.89461732 0.88410997 0.88718486
		 0.90641528 0.97479051 0.85269451 0.99666935 0.86422795 0.98922741 0.88137317 0.96972823
		 0.86950207 0.934708 0.90556121 0.92714429 0.88475502 0.89479309 0.88206327 0.92700696
		 0.88267517 0.89550799 0.91790205 0.89255047 0.84499562 0.93716538 0.94362271 0.89214969
		 0.94389206 0.92903918 0.84667146 0.89294177 0.84669852 0.98000515 0.92492181 0.9894613
		 0.94831699 0.83983779 0.94840562 0.84927642 0.92505211 0.93485367 0.98704481 0.89429057
		 0.98646164 0.98649526 0.96677369 0.84278405 0.96680415 0.92497993 0.8771807 0.89691192
		 0.87674123 0.89574808 0.85338449 0.92624211 0.85334897 0.86494893 0.86647248 0.86235863
		 0.85838807 0.95960587 0.85834885 0.9569822 0.86647892 0.85431874 0.86916262 0.96761513
		 0.86902606 0.85006601 0.85340649 0.97192603 0.85333467 0.92943764 0.84496045 0.93383574
		 0.91824549 0.61699325 0.71539783 0.63284415 0.71528602 0.63246185 0.73044753 0.6162371
		 0.73065281 0.64817423 0.7152378 0.64807469 0.73024994 0.64854616 0.74525589 0.63322002
		 0.74559516 0.61737162 0.74588418 0.66377419 0.71511286 0.66391832 0.73004746 0.67976552
		 0.71510279 0.68005049 0.72985077 0.68013126 0.74459267 0.66414291 0.74498856 0.67920786
		 0.69731295 0.69293392 0.6970374 0.69317102 0.71501791 0.6780082 0.67949903 0.69266903
		 0.67885703 0.70735025 0.6792568 0.7066654 0.6970433 0.70659047 0.71479905 0.67532438
		 0.66272378 0.69264317 0.66184777 0.67321903 0.64627582 0.69263619 0.64539802 0.71198636
		 0.64608687 0.70996845 0.66249382 0.72261316 0.71443719 0.72285032 0.72932792 0.70668358
		 0.729514 0.73827463 0.7142114 0.73875076 0.72914457 0.7386691 0.74408621 0.72300297
		 0.74420702 0.70696503 0.74422944 0.75368047 0.71385205 0.75442052 0.72896051 0.7695151
		 0.71351218 0.7706489 0.72875905 0.76989716 0.74403548 0.75406373 0.74408889 0.67527515
		 0.81357569 0.67696446 0.79706866 0.69426805 0.79750359 0.69469368 0.81399059 0.67924476
		 0.78022265 0.69391251 0.78048337 0.70859241 0.7796855 0.71149349 0.79639423 0.71403235
		 0.81290913 0.68001503 0.76238561 0.69374049 0.76231313 0.69354486 0.74434185 0.70749032
		 0.76195955 0.69336247 0.72968102 0.6635837 0.69793099 0.66205269 0.68133801 0.64890414
		 0.69861805 0.65022111 0.68590015 0.63463634 0.69730538 0.62028098 0.69595325 0.62785035
		 0.67502534 0.64095622 0.67953628 0.64727956 0.65487325 0.65587145 0.66701627 0.75744444
		 0.67363679 0.76571304 0.69401443 0.75155854 0.69599527 0.74429989 0.67846668 0.73712486
		 0.69773239 0.73540443 0.68521541 0.72234613 0.69734234 0.72335339 0.6808483 0.72939491
		 0.66650778 0.73783278 0.65419948 0.65742844 0.79326868 0.66325539 0.77879226 0.64914459
		 0.80562741 0.62922609 0.78597021 0.64221436 0.781129 0.65131766 0.77453202 0.62114066
		 0.76523972 0.63545722 0.76352477 0.64968735 0.76185167 0.66437835 0.76216662 0.75238645
		 0.76202518 0.73797232 0.76060081 0.7665723 0.76364774 0.75875765 0.78425324 0.7454834
		 0.77984321 0.73665911 0.77318674 0.73962188 0.80389619 0.73058701 0.79211432 0.72465944
		 0.77761316 0.72319561 0.76127899 0.20415184 0.9327184 0.13051847 0.93282855 0.1435703
		 0.99070245 0.12456703 0.99058121 0.22142754 0.93282855 0.14779383 0.9327184 0.16257322
		 0.99058121 0.32411152 0.9327184 0.25096047 0.93282855 0.27605653 0.99070245 0.25652266
		 0.99058121 0.34186953 0.93282855 0.26875722 0.9327184 0.29563296 0.99058121 0.44625133
		 0.9327184 0.37296584 0.93282855 0.41026241 0.99070245 0.39087647 0.99058121 0.46387491
		 0.93282855 0.39058957 0.9327184 0.42964852 0.99058121 0.56808352 0.9327184 0.49497128
		 0.93282855 0.5444684 0.99070245 0.52489185 0.99058121 0.5858804 0.93282855 0.51272959
		 0.9327184 0.56400251 0.99058121 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1
		 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1
		 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001
		 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001
		 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001
		 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001
		 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001
		 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001
		 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1;
	setAttr ".uvst[0].uvsp[250:499]" 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0 0.35409001
		 1 0.35409001 1 1 0 1 0 0.35409001 1 0.35409001 1 1 0 1 0.2597205 0.93238819 0.19022942
		 0.93216813 0.20925236 0.98997587 0.18569255 0.99021798 0.23870292 0.9327184 0.16881141
		 0.93238819 0.18313438 0.93238819 0.11324276 0.9327184 0.10144782 0.99021798 0.16171637
		 0.93216813 0.092225291 0.93238819 0.077888012 0.98997587 0.14016551 0.93238819 0.070807286
		 0.93216813 0.054182053 0.99021798 0.11874337 0.9327184 0.04925641 0.93238819 0.030617714
		 0.99058121 0.54647952 0.93238819 0.47717443 0.9327184 0.50112748 0.99021798 0.52464318
		 0.93216813 0.45557043 0.93238819 0.47710747 0.98997587 0.50290275 0.93238819 0.43373406
		 0.93216813 0.45319307 0.99021798 0.48149866 0.9327184 0.41199371 0.93238819 0.42484719
		 0.93238819 0.35534227 0.9327184 0.36733186 0.99021798 0.40310672 0.93216813 0.33393806
		 0.93238819 0.34341741 0.98997587 0.38127041 0.93238819 0.31219766 0.93216813 0.31939745
		 0.99021798 0.35966635 0.9327184 0.29036134 0.93238819 0.30268925 0.93238819 0.23320241
		 0.9327184 0.2329582 0.99021798 0.28113848 0.93216813 0.21178019 0.93238819 0.0092225326
		 0.41191983 0.91617161 0.41189781 0.01132428 0.41195285 0.91831338 0.41191983 0.013051851
		 0.41196388 0.92041516 0.41195285 0.014779389 0.41195285 0.92214274 0.41196388 0.016881147
		 0.41191983 0.92387027 0.41195285 0.019022949 0.41189781 0.92597204 0.41191983 0.021178026
		 0.41191983 0.92811382 0.41189781 0.02332025 0.41195285 0.93026888 0.41191983 0.025096057
		 0.41196388 0.93241113 0.41195285 0.026875732 0.41195285 0.93418694 0.41196388 0.029036142
		 0.41191983 0.93596661 0.41195285 0.031219775 0.41189781 0.93812704 0.41191983 0.033393819
		 0.41191983 0.94031066 0.41189781 0.035534237 0.41195285 0.94248468 0.41191983 0.037296597
		 0.41196388 0.94462514 0.41195285 0.039058968 0.41195285 0.94638747 0.41196388 0.041199386
		 0.41191983 0.94814986 0.41195285 0.043373421 0.41189781 0.95029026 0.41191983 0.045557059
		 0.41191983 0.95246428 0.41189781 0.047717459 0.41195285 0.95464796 0.41191983 0.049497146
		 0.41196388 0.95680833 0.41195285 0.051272973 0.41195285 0.958588 0.41196388 0.0049256431
		 0.41191983 0.91187429 0.41195285 0.007080731 0.41189781 0.91401654 0.41191983 0.018445063
		 0.46974966 0.83234322 0.46970564 0.022648558 0.4698157 0.83662677 0.46974966 0.0261037
		 0.46983773 0.84083033 0.4698157 0.029558774 0.4698157 0.84428549 0.46983773 0.033762291
		 0.46974966 0.84774059 0.4698157 0.038045894 0.46970564 0.85194403 0.46974966 0.042356048
		 0.46974966 0.85622764 0.46970564 0.046640493 0.4698157 0.86053777 0.46974966 0.05019211
		 0.46983773 0.86482227 0.4698157 0.053751461 0.4698157 0.86837387 0.46983773 0.05807228
		 0.46974966 0.87193322 0.4698157 0.062439546 0.46970564 0.87625408 0.46974966 0.06678763
		 0.46974966 0.88062131 0.46970564 0.071068466 0.4698157 0.88496941 0.46974966 0.074593186
		 0.46983773 0.88925028 0.4698157 0.078117937 0.4698157 0.89277494 0.46983773 0.082398765
		 0.46974966 0.89629972 0.4698157 0.086746834 0.46970564 0.90058053 0.46974966 0.091114119
		 0.46974966 0.90492862 0.46970564 0.095434912 0.4698157 0.90929592 0.46974966 0.098994285
		 0.46983773 0.91361672 0.4698157 0.10254594 0.4698157 0.91717607 0.46983773 0.0098512853
		 0.46974966 0.82374859 0.4698157 0.01416146 0.46970564 0.82803303 0.46974966 0.027667591
		 0.52757943 0.74851489 0.52751344 0.033972833 0.52767855 0.75494021 0.52757943 0.039155543
		 0.52771157 0.76124555 0.52767855 0.044338159 0.52767855 0.76642823 0.52771157 0.050643429
		 0.52757943 0.77161086 0.52767855 0.057068832 0.52751344 0.77791607 0.52757943 0.063534066
		 0.52757943 0.78434151 0.52751344 0.069960728 0.52767855 0.79080671 0.52757943 0.075288154
		 0.52771157 0.79723346 0.52767855 0.080627181 0.52767855 0.80256087 0.52771157 0.087108403
		 0.52757943 0.80789989 0.52767855 0.093659312 0.52751344 0.81438112 0.52757943 0.10018143
		 0.52757943 0.82093197 0.52751344 0.10660268 0.52767855 0.82745415 0.52757943 0.11188976
		 0.52771157 0.83387542 0.52767855 0.11717689 0.52767855 0.83916247 0.52771157 0.12359813
		 0.52757943 0.84444958 0.52767855 0.13012025 0.52751344 0.85087079 0.52757943 0.13667116
		 0.52757943 0.85739297 0.52751344 0.14315236 0.52767855 0.86394387 0.52757943 0.14849141
		 0.52771157 0.87042505 0.52767855 0.15381889 0.52767855 0.87576413 0.52771157 0.014776926
		 0.52757943 0.73562294 0.52767855 0.021242186 0.52751344 0.74204963 0.52757943 0.036890119
		 0.58540928 0.6646865 0.58532125 0.045297109 0.58554137 0.67325366 0.58540928 0.052207388
		 0.58558542 0.68166071 0.58554137 0.059117541 0.58554137 0.68857104 0.58558542 0.067524567
		 0.58540928 0.69548118 0.58554137 0.076091774 0.58532125 0.70388818 0.58540928 0.084712081
		 0.58540928 0.71245539 0.58532125 0.093280971 0.58554137 0.72107565 0.58540928 0.1003842
		 0.58558542 0.7296446 0.58554137 0.1075029 0.58554137 0.73674786 0.58558542 0.11614454
		 0.58540928 0.7438665 0.58554137 0.12487908 0.58532125 0.75250816 0.58540928 0.13357523
		 0.58540928 0.76124263 0.58532125 0.1421369 0.58554137 0.76993889 0.58540928 0.14918634
		 0.58558542 0.7785005 0.58554137 0.15623584 0.58554137 0.78555 0.58558542 0.1647975
		 0.58540928 0.79259944 0.58554137 0.17349365 0.58532125 0.80116111 0.58540928 0.18222819
		 0.58540928 0.80985731 0.58532125 0.19086979 0.58554137 0.81859183 0.58540928 0.19798854
		 0.58558542 0.82723337 0.58554137 0.20509185 0.58554137 0.8343522 0.58558542 0.019702567
		 0.58540928 0.6474973 0.58554137 0.028322915 0.58532125 0.65606618 0.58540928;
	setAttr ".uvst[0].uvsp[500:749]" 0.046112649 0.64323908 0.58085817 0.64312905
		 0.05662138 0.64340425 0.59156716 0.64323908 0.065259233 0.64345926 0.60207587 0.64340425
		 0.073896922 0.64340425 0.61071378 0.64345926 0.084405705 0.64323908 0.61935151 0.64340425
		 0.095114708 0.64312905 0.62986028 0.64323908 0.1058901 0.64323908 0.64056927 0.64312905
		 0.11660121 0.64340425 0.6513446 0.64323908 0.12548023 0.64345926 0.66205573 0.64340425
		 0.13437863 0.64340425 0.6709348 0.64345926 0.14518067 0.64323908 0.67983317 0.64340425
		 0.15609884 0.64312905 0.6906352 0.64323908 0.16696903 0.64323908 0.70155334 0.64312905
		 0.17767113 0.64340425 0.71242362 0.64323908 0.18648294 0.64345926 0.72312564 0.64340425
		 0.1952948 0.64340425 0.73193747 0.64345926 0.20599687 0.64323908 0.7407493 0.64340425
		 0.21686706 0.64312905 0.75145137 0.64323908 0.22778523 0.64323908 0.76232165 0.64312905
		 0.23858723 0.64340425 0.77323979 0.64323908 0.24748567 0.64345926 0.78404176 0.64340425
		 0.25636482 0.64340425 0.79294026 0.64345926 0.024628207 0.64323908 0.55937165 0.64340425
		 0.035403643 0.64312905 0.57008278 0.64323908 0.055335179 0.70106888 0.49702978 0.70093691
		 0.067945659 0.70126706 0.5098806 0.70106888 0.078311078 0.70133311 0.5224911 0.70126706
		 0.088676304 0.70126706 0.53285652 0.70133311 0.10128684 0.70106888 0.54322177 0.70126706
		 0.11413765 0.70093691 0.55583233 0.70106888 0.12706812 0.70106888 0.56868309 0.70093691
		 0.13992146 0.70126706 0.58161348 0.70106888 0.15057628 0.70133311 0.59446687 0.70126706
		 0.16125435 0.70126706 0.60512173 0.70133311 0.17421681 0.70106888 0.61579978 0.70126706
		 0.18731861 0.70093691 0.62876225 0.70106888 0.20036285 0.70106888 0.641864 0.70093691
		 0.21320537 0.70126706 0.6549083 0.70106888 0.22377951 0.70133311 0.66775078 0.70126706
		 0.23435375 0.70126706 0.67832494 0.70133311 0.24719626 0.70106888 0.68889916 0.70126706
		 0.26024047 0.70093691 0.70174164 0.70106888 0.27334228 0.70106888 0.71478593 0.70093691
		 0.28630468 0.70126706 0.72788775 0.70106888 0.2969828 0.70133311 0.74085009 0.70126706
		 0.30763778 0.70126706 0.75152826 0.70133311 0.029553849 0.70106888 0.471246 0.70126706
		 0.042484373 0.70093691 0.48409933 0.70106888 0.064557701 0.75889868 0.41320145 0.75874472
		 0.079269931 0.75912988 0.42819405 0.75889868 0.091362923 0.75920701 0.44290629 0.75912988
		 0.10345569 0.75912988 0.4549993 0.75920701 0.11816798 0.75889868 0.46709207 0.75912988
		 0.13316059 0.75874472 0.48180437 0.75889868 0.14824612 0.75889868 0.49679697 0.75874472
		 0.16324168 0.75912988 0.51188242 0.75889868 0.17567232 0.75920701 0.52687806 0.75912988
		 0.18813007 0.75912988 0.53930867 0.75920701 0.20325294 0.75889868 0.55176646 0.75912988
		 0.21853836 0.75874472 0.56688929 0.75889868 0.23375666 0.75889868 0.58217466 0.75874472
		 0.24873959 0.75912988 0.59739304 0.75889868 0.26107609 0.75920701 0.61237592 0.75912988
		 0.2734127 0.75912988 0.62471241 0.75920701 0.28839561 0.75889868 0.63704902 0.75912988
		 0.30361387 0.75874472 0.6520319 0.75889868 0.31889933 0.75889868 0.66725028 0.75874472
		 0.3340221 0.75912988 0.68253571 0.75889868 0.34647992 0.75920701 0.69765848 0.75912988
		 0.35891074 0.75912988 0.71011633 0.75920701 0.034479491 0.75889868 0.38312036 0.75912988
		 0.049565099 0.75874472 0.3981159 0.75889868 0.073780231 0.81672847 0.32937309 0.81655252
		 0.090594202 0.8169927 0.34650749 0.81672847 0.10441477 0.81708086 0.36332148 0.8169927
		 0.11823507 0.8169927 0.37714204 0.81708086 0.13504912 0.81672847 0.39096236 0.8169927
		 0.15218353 0.81655252 0.40777642 0.81672847 0.16942415 0.81672847 0.42491081 0.81655252
		 0.18656193 0.8169927 0.44215137 0.81672847 0.20076838 0.81708086 0.45928919 0.8169927
		 0.21500579 0.8169927 0.47349563 0.81708086 0.23228908 0.81672847 0.4877331 0.8169927
		 0.24975812 0.81655252 0.50501633 0.81672847 0.26715046 0.81672847 0.52248538 0.81655252
		 0.2842738 0.8169927 0.53987777 0.81672847 0.29837269 0.81708086 0.55700105 0.8169927
		 0.31247166 0.8169927 0.57109988 0.81708086 0.32959497 0.81672847 0.58519888 0.8169927
		 0.34698725 0.81655252 0.60232222 0.81672847 0.36445636 0.81672847 0.61971462 0.81655252
		 0.38173956 0.8169927 0.63718367 0.81672847 0.39597705 0.81708086 0.65446681 0.8169927
		 0.41018367 0.8169927 0.66870433 0.81708086 0.03940513 0.81672847 0.29499471 0.8169927
		 0.056645826 0.81655252 0.31213245 0.81672847 0.083002761 0.87455833 0.24554473 0.87436032
		 0.10191848 0.87485552 0.26482093 0.87455833 0.11746661 0.8749547 0.28373665 0.87485552
		 0.13301446 0.87485552 0.29928479 0.8749547 0.15193027 0.87455833 0.31483263 0.87485552
		 0.17120647 0.87436032 0.33374843 0.87455833 0.19060217 0.87455833 0.35302463 0.87436032
		 0.20988217 0.87485552 0.37242031 0.87455833 0.22586444 0.8749547 0.39170036 0.87485552
		 0.2418815 0.87485552 0.40768257 0.8749547 0.26132521 0.87455833 0.42369971 0.87485552
		 0.2809779 0.87436032 0.44314337 0.87455833 0.30054426 0.87455833 0.46279603 0.87436032
		 0.31980804 0.87485552 0.48236248 0.87455833 0.33566928 0.8749547 0.50162619 0.87485552
		 0.35153061 0.87485552 0.51748741 0.8749547 0.37079436 0.87455833 0.5333488 0.87485552
		 0.39036065 0.87436032 0.55261248 0.87455833 0.41001338 0.87455833 0.5721789 0.87436032
		 0.42945701 0.87485552 0.59183156 0.87455833 0.44547415 0.8749547 0.61127514 0.87485552
		 0.46145663 0.87485552 0.62729234 0.8749547 0.044330768 0.87455833 0.20686904 0.87485552
		 0.063726559 0.87436032 0.22614898 0.87455833 0.85281605 0.41324943 0.86629122 0.41315454
		 0.86596626 0.42604372 0.85217303 0.42621824 0.87932342 0.41311353 0.87923878 0.4258756
		 0.87963957 0.43863231 0.86661071 0.43892092 0.85313755 0.43916664 0.89258546 0.41300732;
	setAttr ".uvst[0].uvsp[750:999]" 0.89270765 0.42570344 0.90617985 0.41299874
		 0.90642196 0.42553607 0.90649074 0.43806863 0.89289874 0.43840516 0.90570575 0.39787525
		 0.91737455 0.397641 0.9175759 0.41292644 0.90468603 0.38273126 0.91714936 0.38218552
		 0.92963022 0.38252556 0.92904788 0.39764613 0.92898434 0.41274053 0.90240437 0.36847019
		 0.91712731 0.36772561 0.90061444 0.35448766 0.91712159 0.35374141 0.93357128 0.35432696
		 0.93185598 0.36827481 0.94260532 0.41243285 0.9428069 0.42509165 0.9290635 0.42524996
		 0.95591944 0.41224092 0.95632404 0.42493588 0.9562549 0.43763798 0.94293684 0.43774074
		 0.92930263 0.43775985 0.96901637 0.41193551 0.96964568 0.42477942 0.98247749 0.41164666
		 0.98344141 0.42460805 0.98280245 0.43759483 0.96934229 0.43764055 0.90236241 0.49671245
		 0.90379852 0.48267943 0.91850871 0.48304936 0.91887063 0.49706504 0.90573734 0.46835837
		 0.91820639 0.4685801 0.93068606 0.46790162 0.9331525 0.48210609 0.93531078 0.4961457
		 0.9063918 0.45319471 0.91806012 0.45313299 0.91789395 0.43785554 0.92974919 0.45283252
		 0.91773885 0.42539194 0.89242333 0.39840055 0.89112192 0.38429457 0.87994403 0.39898467
		 0.88106364 0.38817292 0.86781472 0.39786893 0.85561109 0.39671934 0.86204576 0.37892824
		 0.87318724 0.38276297 0.87856287 0.3617965 0.88586706 0.37211955 0.97221607 0.37774777
		 0.97924548 0.39507115 0.9672125 0.39675498 0.96104187 0.38185382 0.95494217 0.3982318
		 0.95347959 0.38759083 0.94237858 0.39790028 0.94323462 0.38387829 0.94837075 0.37168729
		 0.95554405 0.36122358 0.88719064 0.47944897 0.89214426 0.46714225 0.88014835 0.48995548
		 0.86321509 0.47324449 0.87425691 0.46912882 0.88199586 0.46352053 0.85634154 0.45562103
		 0.86851257 0.45416325 0.88060981 0.4527407 0.89309913 0.45300853 0.96791631 0.45288834
		 0.95566243 0.45167741 0.97997588 0.45426771 0.97333246 0.47178474 0.96204787 0.46803573
		 0.95454615 0.46237698 0.95706457 0.48848358 0.94938391 0.47846758 0.94434518 0.46614012
		 0.94310063 0.452254 0.5510118 0.57168806 0.56832761 0.57151026 0.56825006 0.59570581
		 0.55095792 0.59588361 0.5340147 0.57195461 0.53398919 0.59615016 0.58574766 0.57168806
		 0.60305631 0.57195461 0.60294437 0.59615016 0.58564913 0.59588361 0.6491909 0.57168806
		 0.66679966 0.57151026 0.66672212 0.59570581 0.64909238 0.59588361 0.63176191 0.57195461
		 0.63165003 0.59615016 0.68432844 0.57168806 0.70158076 0.57195461 0.70155525 0.59615016
		 0.68427449 0.59588361 0.50606823 0.57195461 0.52004135 0.57204354 0.52004135 0.59623909
		 0.50609374 0.59615016 0.6173963 0.57204354 0.61728156 0.59623909 0.7157836 0.57204354
		 0.7157836 0.59623909 0.72998691 0.57195461 0.73001242 0.59615016 0.4890711 0.57168806
		 0.48912501 0.59588361 0.47175542 0.57151026 0.47183299 0.59570581 0.45433539 0.57168806
		 0.45443392 0.59588361 0.43702668 0.57195461 0.43713856 0.59615016 0.79980558 0.57195461
		 0.81417108 0.57204354 0.81428587 0.59623909 0.79991746 0.59615016 0.82851118 0.57195461
		 0.82862306 0.59615016 0.78237665 0.57168806 0.78247523 0.59588361 0.764768 0.57151026
		 0.76484561 0.59570581 0.74723935 0.57168806 0.74729323 0.59588361 0.74670005 0.32973242
		 0.76399231 0.3295548 0.76406986 0.35375035 0.74675399 0.353928 0.78139126 0.32973242
		 0.78148979 0.353928 0.79868662 0.32999909 0.79879856 0.35419464 0.81302381 0.33008802
		 0.81313848 0.35428357 0.82739234 0.32999909 0.82750428 0.35419464 0.43590784 0.32999909
		 0.45335007 0.32973242 0.45344859 0.353928 0.43601972 0.35419464 0.47097969 0.3295548
		 0.47105727 0.35375035 0.48853207 0.32973242 0.48858598 0.353928 0.50581288 0.32999909
		 0.50583839 0.35419464 0.52004135 0.33008802 0.52004135 0.35428357 0.53427017 0.32999909
		 0.5342446 0.35419464 0.55155075 0.32973242 0.55149686 0.353928 0.56910336 0.3295548
		 0.56902575 0.35375035 0.58673298 0.32973242 0.58663446 0.353928 0.60417509 0.32999909
		 0.60406321 0.35419464 0.61854351 0.33008802 0.61842877 0.35428357 0.63288075 0.32999909
		 0.63276887 0.35419464 0.65017635 0.32973242 0.65007782 0.353928 0.66757542 0.3295548
		 0.66749787 0.35375035 0.68486756 0.32973242 0.68481368 0.353928 0.70183611 0.32999909
		 0.7018106 0.35419464 0.7157836 0.33008802 0.7157836 0.35428357 0.72973132 0.32999909
		 0.72975683 0.35419464 0.7641474 0.3779459 0.74680787 0.37812355 0.78158832 0.37812355
		 0.79891044 0.37839019 0.81325322 0.37847912 0.82761616 0.37839019 0.45354715 0.37812355
		 0.4361316 0.37839019 0.47113484 0.3779459 0.48863989 0.37812355 0.50586396 0.37839019
		 0.52004135 0.37847912 0.53421903 0.37839019 0.55144298 0.37812355 0.56894821 0.3779459
		 0.58653587 0.37812355 0.60395133 0.37839019 0.61831403 0.37847912 0.63265699 0.37839019
		 0.64997923 0.37812355 0.66742027 0.3779459 0.68475974 0.37812355 0.70178509 0.37839019
		 0.7157836 0.37847912 0.7297824 0.37839019 0.76422501 0.40214145 0.74686182 0.4023191
		 0.7816869 0.4023191 0.79902232 0.40258574 0.81336796 0.40267467 0.82772803 0.40258574
		 0.45364568 0.4023191 0.4362435 0.40258574 0.47121242 0.40214145 0.4886938 0.4023191
		 0.50588953 0.40258574 0.52004135 0.40267467 0.53419346 0.40258574 0.5513891 0.4023191
		 0.56887066 0.40214145 0.58643734 0.4023191 0.60383946 0.40258574 0.61819929 0.40267467
		 0.63254511 0.40258574 0.64988071 0.4023191 0.66734272 0.40214145 0.68470579 0.4023191
		 0.70175958 0.40258574 0.7157836 0.40267467 0.72980797 0.40258574 0.76430261 0.42633697
		 0.7469157 0.42651466 0.78178549 0.42651466 0.79913425 0.4267813 0.8134827 0.42687023
		 0.82783985 0.4267813 0.4537442 0.42651466 0.43635538 0.4267813 0.47128999 0.42633697
		 0.48874772 0.42651466 0.50591505 0.4267813 0.52004135 0.42687023 0.53416789 0.4267813;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.55133522 0.42651466 0.56879312 0.42633697
		 0.58633882 0.42651466 0.60372758 0.4267813 0.61808455 0.42687023 0.63243324 0.4267813
		 0.64978212 0.42651466 0.66726518 0.42633697 0.68465191 0.42651466 0.70173407 0.4267813
		 0.7157836 0.42687023 0.72983354 0.4267813 0.76438022 0.45053253 0.74696964 0.45071021
		 0.78188407 0.45071021 0.79924613 0.45097685 0.81359744 0.45106578 0.82795173 0.45097685
		 0.45384276 0.45071021 0.43646726 0.45097685 0.47136757 0.45053253 0.48880163 0.45071021
		 0.50594056 0.45097685 0.52004135 0.45106578 0.53414237 0.45097685 0.55128133 0.45071021
		 0.56871557 0.45053253 0.58624029 0.45071021 0.6036157 0.45097685 0.61796987 0.45106578
		 0.63232136 0.45097685 0.64968359 0.45071021 0.66718763 0.45053253 0.68459797 0.45071021
		 0.7017085 0.45097685 0.7157836 0.45106578 0.72985911 0.45097685 0.76445782 0.47472808
		 0.74702358 0.47490579 0.78198254 0.47490579 0.79935801 0.4751724 0.81371212 0.47526133
		 0.82806361 0.4751724 0.45394129 0.47490579 0.43657914 0.4751724 0.47144514 0.47472808
		 0.48885554 0.47490579 0.50596607 0.4751724 0.52004135 0.47526133 0.53411686 0.4751724
		 0.55122745 0.47490579 0.56863797 0.47472808 0.58614177 0.47490579 0.60350382 0.4751724
		 0.61785519 0.47526133 0.63220948 0.4751724 0.64958507 0.47490579 0.66711003 0.47472808
		 0.68454403 0.47490579 0.70168298 0.4751724 0.7157836 0.47526133 0.72988468 0.4751724
		 0.76453537 0.49892363 0.74707752 0.49910134 0.78208107 0.49910134 0.79946995 0.49936795
		 0.81382686 0.49945688 0.82817554 0.49936795 0.45403981 0.49910134 0.43669102 0.49936795
		 0.47152269 0.49892363 0.48890942 0.49910134 0.50599158 0.49936795 0.52004135 0.49945688
		 0.53409135 0.49936795 0.55117351 0.49910134 0.56856036 0.49892363 0.58604324 0.49910134
		 0.60339195 0.49936795 0.61774045 0.49945688 0.6320976 0.49936795 0.64948654 0.49910134
		 0.66703242 0.49892363 0.68449014 0.49910134 0.70165747 0.49936795 0.7157836 0.49945688
		 0.72991025 0.49936795 0.76461291 0.52311915 0.74713147 0.52329689 0.78217959 0.52329689
		 0.79958183 0.5235635 0.8139416 0.52365243 0.82828742 0.5235635 0.45413834 0.52329689
		 0.43680289 0.5235635 0.47160026 0.52311915 0.48896331 0.52329689 0.50601709 0.5235635
		 0.52004135 0.52365243 0.53406584 0.5235635 0.55111963 0.52329689 0.56848276 0.52311915
		 0.58594471 0.52329689 0.60328007 0.5235635 0.61762571 0.52365243 0.63198572 0.5235635
		 0.64938802 0.52329689 0.66695482 0.52311915 0.6844362 0.52329689 0.7016319 0.5235635
		 0.7157836 0.52365243 0.72993577 0.5235635 0.76469046 0.5473147 0.74718541 0.5474925
		 0.78227812 0.5474925 0.7996937 0.54775906 0.81405634 0.54784799 0.8283993 0.54775906
		 0.45423687 0.5474925 0.4369148 0.54775906 0.47167784 0.5473147 0.48901719 0.5474925
		 0.50604266 0.54775906 0.52004135 0.54784799 0.53404027 0.54775906 0.55106574 0.5474925
		 0.56840515 0.5473147 0.58584619 0.5474925 0.60316819 0.54775906 0.61751103 0.54784799
		 0.63187385 0.54775906 0.64928949 0.5474925 0.66687727 0.5473147 0.68438232 0.5474925
		 0.70160633 0.54775906 0.7157836 0.54784799 0.72996134 0.54775906 0.83880234 0.2343663
		 0.85227704 0.23427117 0.85195208 0.24716043 0.83815956 0.24733484 0.86530948 0.23423028
		 0.86522508 0.24699247 0.86562586 0.25974917 0.85259676 0.26003754 0.83912373 0.26028347
		 0.87857127 0.23412406 0.87869382 0.24682033 0.8921659 0.23411548 0.89240789 0.24665296
		 0.8924768 0.25918555 0.87888479 0.25952196 0.89169168 0.21899211 0.90336061 0.21875787
		 0.90356231 0.23404336 0.89067197 0.20384812 0.90313554 0.20330226 0.91561627 0.20364225
		 0.91503382 0.21876276 0.91497016 0.23385727 0.8883903 0.189587 0.9031136 0.1888423
		 0.88660049 0.17560446 0.90310764 0.17485821 0.91955757 0.17544377 0.91784191 0.18939161
		 0.92859149 0.23354971 0.92879295 0.24620843 0.91504955 0.24636674 0.94190574 0.23335767
		 0.94231033 0.24605274 0.94224095 0.25875485 0.92892289 0.25885761 0.91528893 0.25887656
		 0.95500231 0.23305225 0.95563173 0.2458961 0.9684639 0.23276329 0.96942759 0.24572492
		 0.96878862 0.2587117 0.95532846 0.25875735 0.88834858 0.31782919 0.88978481 0.30379617
		 0.904495 0.30416596 0.90485692 0.31818187 0.89172316 0.2894752 0.90419269 0.28969681
		 0.91667199 0.28901851 0.91913843 0.30322289 0.92129683 0.31726253 0.89237809 0.27431154
		 0.90404654 0.27424979 0.90388012 0.25897217 0.91573548 0.27394927 0.90372491 0.24650884
		 0.87840939 0.21951723 0.8771081 0.20541131 0.86593008 0.22010148 0.86704969 0.20928979
		 0.85380077 0.21898568 0.84159708 0.2178359 0.848032 0.20004499 0.8591733 0.20387959
		 0.86454892 0.1829133 0.87185311 0.19323623 0.95820236 0.19886458 0.96523142 0.21618795
		 0.95319843 0.2178719 0.94702768 0.2029705 0.94092798 0.21934867 0.93946576 0.20870757
		 0.92836452 0.21901703 0.92922068 0.20499492 0.93435693 0.19280398 0.94152999 0.18234038
		 0.87317681 0.30056584 0.8781302 0.28825915 0.86613464 0.31107223 0.8492012 0.29436135
		 0.86024284 0.29024565 0.86798191 0.28463745 0.84232807 0.27673781 0.85449862 0.27527988
		 0.86659575 0.27385759 0.87908506 0.27412534 0.95390224 0.27400506 0.94164848 0.27279425
		 0.96596193 0.27538455 0.95931864 0.29290164 0.94803381 0.28915262 0.94053221 0.28349376
		 0.94305086 0.30960047 0.93537021 0.29958451 0.93033099 0.28725672 0.92908645 0.27337074
		 0.15787695 0.35687006 0.14035548 0.3570478 0.14042604 0.32539129 0.15797842 0.32521355
		 0.12311179 0.35731432 0.12314522 0.32565784 0.19290388 0.35731432 0.17547913 0.3570478
		 0.17560804 0.32539129 0.19305027 0.32565784 0.25634903 0.35687006 0.23892231 0.3570478
		 0.23905122 0.32539129 0.25645053 0.32521355 0.22160947 0.35731432 0.22175586 0.32565784;
	setAttr ".uvst[0].uvsp[1250:1499]" 0.29067764 0.35731432 0.27367201 0.3570478
		 0.27374256 0.32539129 0.29071105 0.32565784 0.10891688 0.35740328 0.094721705 0.35731432
		 0.094688296 0.32565784 0.10891688 0.32574677 0.20726848 0.35740328 0.20741856 0.32574677
		 0.30465877 0.35740328 0.30465877 0.32574677 0.3186397 0.35731432 0.31860626 0.32565784
		 0.077478006 0.3570478 0.077407479 0.32539129 0.059956357 0.35687006 0.059854865 0.32521355
		 0.042353947 0.3570478 0.042225003 0.32539129 0.024929401 0.35731432 0.024783015 0.32565784
		 0.40204906 0.35740328 0.38770807 0.35731432 0.38756168 0.32565784 0.40189898 0.32574677
		 0.41641366 0.35731432 0.41626728 0.32565784 0.37039524 0.3570478 0.37026632 0.32539129
		 0.35296875 0.35687006 0.35286725 0.32521355 0.33564553 0.3570478 0.33557498 0.32539129
		 0.13988036 0.5702247 0.15719369 0.5700469 0.15712535 0.59136456 0.13983285 0.59154236
		 0.12288664 0.57049119 0.12286413 0.59180892 0.10891688 0.57058018 0.10891688 0.59189785
		 0.09494666 0.57049119 0.094969153 0.59180892 0.077952936 0.5702247 0.078000426 0.59154236
		 0.060639817 0.5700469 0.060708165 0.59136456 0.04322226 0.5702247 0.043309093 0.59154236
		 0.025915155 0.57049119 0.026013732 0.59180892 0.40305984 0.57058018 0.41739964 0.57049119
		 0.41749823 0.59180892 0.40316093 0.59189785 0.38869381 0.57049119 0.3887924 0.59180892
		 0.37126356 0.5702247 0.37135041 0.59154236 0.35365218 0.5700469 0.35372055 0.59136456
		 0.33612067 0.5702247 0.33616817 0.59154236 0.31886506 0.57049119 0.31888759 0.59180892
		 0.30465877 0.57058018 0.30465877 0.59189785 0.29045269 0.57049119 0.29043019 0.59180892
		 0.27319688 0.5702247 0.27314937 0.59154236 0.25566536 0.5700469 0.255597 0.59136456
		 0.23805419 0.5702247 0.23796737 0.59154236 0.22062372 0.57049119 0.22052515 0.59180892
		 0.2062577 0.57058018 0.20615661 0.59189785 0.19191812 0.57049119 0.19181955 0.59180892
		 0.174611 0.5702247 0.17452419 0.59154236 0.077525496 0.37836549 0.060024701 0.37818775
		 0.094744198 0.37863201 0.10891688 0.378721 0.12308928 0.37863201 0.14030796 0.37836549
		 0.15780863 0.37818775 0.17539233 0.37836549 0.19280529 0.37863201 0.2071674 0.378721
		 0.2215109 0.37863201 0.2388355 0.37836549 0.25628066 0.37818775 0.27362451 0.37836549
		 0.29065514 0.37863201 0.30465877 0.378721 0.31866223 0.37863201 0.33569306 0.37836549
		 0.35303712 0.37818775 0.37048209 0.37836549 0.38780665 0.37863201 0.40215015 0.378721
		 0.41651225 0.37863201 0.04244078 0.37836549 0.025027975 0.37863201 0.077572986 0.39968318
		 0.060093049 0.39950544 0.094766699 0.3999497 0.10891688 0.40003869 0.12306676 0.3999497
		 0.14026046 0.39968318 0.15774031 0.39950544 0.17530552 0.39968318 0.1927067 0.3999497
		 0.20706633 0.40003869 0.22141233 0.3999497 0.23874868 0.39968318 0.25621229 0.39950544
		 0.273577 0.39968318 0.29063264 0.3999497 0.30465877 0.40003869 0.31868476 0.3999497
		 0.33574057 0.39968318 0.35310549 0.39950544 0.37056893 0.39968318 0.38790521 0.3999497
		 0.40225124 0.40003869 0.41661084 0.3999497 0.042527612 0.39968318 0.02512655 0.3999497
		 0.077620484 0.42100087 0.060161397 0.42082313 0.0947892 0.42126739 0.10891688 0.42135638
		 0.12304425 0.42126739 0.14021295 0.42100087 0.15767199 0.42082313 0.1752187 0.42100087
		 0.19260813 0.42126739 0.20696525 0.42135638 0.22131376 0.42126739 0.23866187 0.42100087
		 0.25614393 0.42082313 0.2735295 0.42100087 0.29061013 0.42126739 0.30465877 0.42135638
		 0.31870729 0.42126739 0.3357881 0.42100087 0.35317382 0.42082313 0.37065578 0.42100087
		 0.3880038 0.42126739 0.40235233 0.42135638 0.41670942 0.42126739 0.042614445 0.42100087
		 0.025225125 0.42126739 0.077667974 0.44231856 0.060229741 0.44214082 0.094811693
		 0.44258505 0.10891688 0.44267404 0.12302174 0.44258505 0.14016545 0.44231856 0.15760367
		 0.44214082 0.17513189 0.44231856 0.19250956 0.44258505 0.20686418 0.44267404 0.22121519
		 0.44258505 0.23857506 0.44231856 0.25607556 0.44214082 0.27348197 0.44231856 0.29058766
		 0.44258505 0.30465877 0.44267404 0.31872982 0.44258505 0.33583564 0.44231856 0.35324216
		 0.44214082 0.37074262 0.44231856 0.38810235 0.44258505 0.40245342 0.44267404 0.41680801
		 0.44258505 0.042701274 0.44231856 0.0253237 0.44258505 0.077715464 0.46363622 0.060298089
		 0.46345848 0.094834186 0.46390274 0.10891688 0.4639917 0.12299922 0.46390274 0.14011793
		 0.46363622 0.15753534 0.46345848 0.17504507 0.46363622 0.19241099 0.46390274 0.20676309
		 0.4639917 0.2211166 0.46390274 0.23848826 0.46363622 0.25600719 0.46345848 0.27343446
		 0.46363622 0.29056516 0.46390274 0.30465877 0.4639917 0.31875235 0.46390274 0.33588314
		 0.46363622 0.3533105 0.46345848 0.37082946 0.46363622 0.38820094 0.46390274 0.40255451
		 0.4639917 0.41690663 0.46390274 0.042788107 0.46363622 0.025422277 0.46390274 0.077762954
		 0.48495391 0.060366437 0.48477617 0.094856679 0.48522046 0.10891688 0.48530939 0.12297671
		 0.48522046 0.14007042 0.48495391 0.15746701 0.48477617 0.17495826 0.48495391 0.19231242
		 0.48522046 0.20666201 0.48530939 0.22101802 0.48522046 0.23840144 0.48495391 0.25593883
		 0.48477617 0.27338696 0.48495391 0.29054266 0.48522046 0.30465877 0.48530939 0.31877488
		 0.48522046 0.33593065 0.48495391 0.35337883 0.48477617 0.37091628 0.48495391 0.38829952
		 0.48522046 0.4026556 0.48530939 0.41700524 0.48522046 0.042874936 0.48495391 0.025520854
		 0.48522046 0.077810444 0.5062716 0.060434781 0.50609386 0.09487918 0.50653815 0.10891688
		 0.50662708 0.12295419 0.50653815 0.1400229 0.5062716 0.15739869 0.50609386 0.17487144
		 0.5062716 0.19221385 0.50653815 0.20656094 0.50662708 0.22091945 0.50653815 0.23831463
		 0.5062716 0.25587046 0.50609386 0.27333945 0.5062716 0.29052016 0.50653815 0.30465877
		 0.50662708;
	setAttr ".uvst[0].uvsp[1500:1749]" 0.31879741 0.50653815 0.33597815 0.5062716
		 0.35344717 0.50609386 0.37100309 0.5062716 0.38839811 0.50653815 0.40275666 0.50662708
		 0.41710383 0.50653815 0.042961769 0.5062716 0.025619429 0.50653815 0.077857941 0.52758932
		 0.060503125 0.52741152 0.094901673 0.52785587 0.10891688 0.5279448 0.12293167 0.52785587
		 0.1399754 0.52758932 0.15733035 0.52741152 0.17478463 0.52758932 0.19211528 0.52785587
		 0.20645985 0.5279448 0.22082087 0.52785587 0.23822781 0.52758932 0.25580209 0.52741152
		 0.27329195 0.52758932 0.29049766 0.52785587 0.30465877 0.5279448 0.31881994 0.52785587
		 0.33602566 0.52758932 0.35351551 0.52741152 0.37108991 0.52758932 0.38849667 0.52785587
		 0.40285772 0.5279448 0.41720244 0.52785587 0.043048598 0.52758932 0.025718004 0.52785587
		 0.077905439 0.54890704 0.060571469 0.54872918 0.094924167 0.54917353 0.10891688 0.54926252
		 0.12290916 0.54917353 0.13992788 0.54890704 0.15726201 0.54872918 0.17469782 0.54890704
		 0.19201669 0.54917353 0.20635878 0.54926252 0.22072229 0.54917353 0.238141 0.54890704
		 0.25573373 0.54872918 0.27324441 0.54890704 0.29047519 0.54917353 0.30465877 0.54926252
		 0.3188425 0.54917353 0.33607316 0.54890704 0.35358384 0.54872918 0.37117672 0.54890704
		 0.38859525 0.54917353 0.40295878 0.54926252 0.41730103 0.54917353 0.043135427 0.54890704
		 0.025816578 0.54917353 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0;
	setAttr ".uvst[0].uvsp[1750:1999]" 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5
		 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0;
	setAttr ".uvst[0].uvsp[2000:2238]" 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5
		 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5
		 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375
		 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5 0.25 0.24999999
		 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0 0.75 0.25 0.5 0.5
		 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75 0.5 0.50000006 0
		 0.75 0.25 0.5 0.5 0.25 0.24999999 0.25 0.5 0.375 0.5 0.5 1 0.5 0.5 0.625 0.5 0.75
		 0.5 0.61247987 0.97363126 0.61146474 0.9566946 0.62929249 0.95541173 0.62929249 0.97384697
		 0.70996779 0.93112564 0.69593132 0.92753267 0.69489288 0.92614222 0.71045989 0.9291985
		 0.69122696 0.96256465 0.69228578 0.94751823 0.70628595 0.94610214 0.70621586 0.96483493
		 0.70264566 0.98889583 0.69143611 0.97571838 0.6934399 0.97583193 0.70357299 0.98729426
		 0.68646437 0.96266919 0.68761903 0.94549429 0.68950444 0.94590104 0.6883527 0.96302766
		 0.740412 0.96376687 0.74060273 0.9482044 0.74256313 0.94813752 0.74234331 0.96376985
		 0.72637856 0.98953074 0.72546411 0.9879114 0.72408652 0.94803584 0.72372115 0.96493912
		 0.72518897 0.93585145 0.72569144 0.93391526 0.64764416 0.95541173 0.64764416 0.97191721
		 0.73641217 0.97886378 0.73830605 0.97910792 0.73749077 0.94918478 0.73729575 0.96310145
		 0.7385602 0.93733883 0.73724294 0.93855786 0.66415894 0.9562279 0.66409445 0.97192627
		 0.66033924 0.94109666 0.64764416 0.93298262 0.62929249 0.93298262 0.61698252 0.9435513
		 0.70676422 0.98544663 0.69627184 0.97469538 0.72250503 0.98560864 0.73395669 0.97684914
		 0.73440546 0.94051206 0.72378922 0.93858504 0.70991844 0.93418491 0.69791341 0.93055165
		 0.63252574 0.98803341 0.61896425 0.99232346 0.64810628 0.98318255 0.65987861 0.98158395
		 0.82445073 0.96120524 0.82865131 0.99246395 0.79959893 0.97998822 0.80378771 0.95264649
		 0.77367973 0.95081574 0.7570172 0.97673023 0.7523616 0.97189498 0.7651521 0.947662
		 0.79564881 0.95063895 0.79256654 0.97764373 0.79203176 0.98701942 0.75617224 0.98701954
		 0.80317354 0.9145602 0.78704786 0.9151839 0.78722858 0.90557915 0.80288565 0.90557915
		 0.81205881 0.91682202 0.77858472 0.91375786 0.8263582 0.92249531 0.84919715 0.81262159
		 0.82860869 0.81294048 0.83429527 0.78811777 0.86339271 0.78729099 0.8022179 0.80429769
		 0.79384947 0.80410522 0.79111904 0.77902591 0.79851121 0.77686405 0.82096189 0.81189865
		 0.82582134 0.78683317 0.82840544 0.77912337 0.80115098 0.76832539 0.81516755 0.84533101
		 0.81187624 0.85292053 0.79847485 0.84761125 0.80157667 0.83932877 0.82354009 0.84640813
		 0.79384947 0.83767962 0.83770311 0.84640121 0.99087942 0.79792416 0.97344375 0.79814792
		 0.97344375 0.77902949 0.99193203 0.78035963 0.904351 0.76548845 0.90476036 0.76186794
		 0.91929251 0.75927722 0.91624415 0.76246399 0.92238003 0.79341191 0.90789413 0.79548705
		 0.90801144 0.77701092 0.9216485 0.77866435 0.91986489 0.80715537 0.90934074 0.81844616
		 0.90704072 0.81493759 0.91737771 0.80487925 0.9249531 0.77737319 0.92545307 0.79431462
		 0.87711686 0.79413998 0.87371629 0.79511666 0.87332612 0.77960479 0.87685496 0.78051615
		 0.88946939 0.81870103 0.8917954 0.81519389 0.89030051 0.79567063 0.88990009 0.77906561
		 0.89045858 0.76995891 0.88875687 0.76695186 0.95441175 0.79614657 0.95441175 0.77902949
		 0.88086849 0.80728298 0.8783595 0.80995148 0.87674004 0.77024686 0.88035649 0.77211446
		 0.93728471 0.77987611 0.93735176 0.79615617 0.94124621 0.76418364 0.95441175 0.75576913
		 0.97344375 0.75576913 0.98620975 0.76672935 0.97009063 0.81286025 0.98415446 0.81730944
		 0.95393252 0.80782962 0.94172382 0.80617177 0.87499267 0.7692107 0.8881737 0.76480681
		 0.90410841 0.75985456 0.92084146 0.75808591 0.92710131 0.77750939 0.92735821 0.79509491
		 0.92190987 0.80776203 0.91085827 0.81993961 0.88809401 0.82020742 0.87647706 0.8108058
		 0.87173468 0.79581428 0.87121576 0.77989352;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 1540 ".vt";
	setAttr ".vt[0:165]"  -3.43343306 160.96606445 -30.60772324 2.56926656 161.0025482178 -30.62749863
		 -3.4699688 166.96856689 -30.62137032 2.53273106 167.0050506592 -30.64114571 -3.29842639 166.75709534 -34.2371521
		 2.33995271 166.79136658 -34.25572968 -3.26407981 161.11872864 -34.21599579 2.37429929 161.15299988 -34.23457336
		 8.81045628 160.66181946 -32.044261932 9.42081642 160.48008728 -28.43925095 8.79491138 163.21369934 -32.053836823
		 9.40428066 163.19680786 -28.44542503 -9.67921543 160.54940796 -31.98334503 -10.26355553 160.36042786 -28.37439728
		 -10.28009129 163.077148438 -28.38057327 -9.69476032 163.10128784 -31.99291992 -3.29467368 166.47889709 -34.41053391
		 2.33844018 166.51313782 -34.42909241 2.36977458 161.36923218 -34.40979004 -3.26333928 161.33499146 -34.39123154
		 8.47288322 163.19522095 -32.22712708 8.48706532 160.86711121 -32.2183876 -9.35950089 160.75862122 -32.15959167
		 -9.37368298 163.086730957 -32.16832733 -2.85004473 165.80519104 -33.98044968 1.90487814 165.83409119 -33.99611664
		 1.92659378 162.26924133 -33.98273849 -2.82832909 162.24034119 -33.96707153 6.75454521 163.49304199 -31.79363251
		 6.76437378 161.8795929 -31.78757858 -7.64644527 161.79199219 -31.74010086 -7.65627384 163.40544128 -31.74615479
		 -20.66794777 204.12792969 6.18872023 -27.5527153 191.39503479 6.42680502 -20.66794395 191.18473816 20.84230614
		 -6.11134148 191.39503479 27.66033554 -5.87092686 204.12792969 20.84230614 -6.11134148 210.150177 6.42680311
		 21.41914368 204.12792969 6.18872023 6.86253643 210.15014648 6.42680502 6.62212276 204.12792969 20.84230614
		 6.86253643 191.39503479 27.66033554 21.41914558 191.18473816 20.84230614 28.30391121 191.39503479 6.42680311
		 -20.66794777 191.18473816 -20.8368187 -27.5527153 191.39503479 -6.42131281 -20.66794395 204.12792969 -6.18322849
		 -6.11134148 210.15014648 -6.42131233 -5.87092686 204.12792969 -20.8368187 -6.11134148 191.39503479 -27.65485382
		 21.41914368 191.18473816 -20.8368187 6.86253643 191.39503479 -27.65485382 6.62212276 204.12792969 -20.8368187
		 6.86253643 210.15014648 -6.42131281 21.41914558 204.12792969 -6.18322849 28.30391121 191.39503479 -6.42131233
		 -17.48045921 201.33978271 17.68571663 18.23165894 201.33978271 17.68571663 -17.48045921 201.33978271 -17.68022537
		 18.23165894 201.33978271 -17.68022537 29.11055946 191.43711853 0.002745284 25.88396645 198.056289673 -6.27846193
		 -25.13277054 191.26885986 -13.96281433 -13.72666168 191.26885986 -25.25836182 -5.96709156 198.056289673 -25.25836182
		 0.37559786 204.47509766 -21.2298317 6.71828747 198.056289673 -25.25836182 0.37559786 191.43711853 -28.45368195
		 -5.96709156 208.03338623 -13.96281433 0.37559786 210.85577393 -6.46892977 6.71828747 208.03338623 -13.96281433
		 14.47785759 191.26885986 -25.25836182 25.88396645 191.26885986 -13.96281433 -19.47735405 203.086517334 13.015974998
		 -12.76500702 203.086517334 19.66325569 -19.47735405 197.21510315 19.66325569 20.22855377 203.086517334 13.015974998
		 20.22855377 197.21510315 19.66325569 13.51620293 203.086517334 19.66325569 -19.47735405 197.21510315 -19.65776253
		 -12.76500702 203.086517334 -19.65776253 -19.47735405 203.086517334 -13.010478973
		 20.22855377 197.21510315 -19.65776253 20.22855377 203.086517334 -13.010478973 13.51620293 203.086517334 -19.65776253
		 -25.13277054 191.26885986 13.9683075 -13.72666168 191.26885986 25.26384735 14.47785759 191.26885986 25.26384735
		 25.88396645 191.26885986 13.9683075 -25.13277054 198.056289673 6.28395319 -21.064807892 204.47509766 0.002745284
		 -25.13277054 198.056289673 -6.27846193 -28.35936546 191.43711853 0.002745284 -13.72666168 208.03338623 6.28395319
		 -6.1594243 210.85577393 0.002745284 -13.72666168 208.03338623 -6.27846193 -5.96709156 208.03338623 13.9683075
		 0.37559786 204.47509766 21.23532104 6.71828747 208.03338623 13.9683075 0.37559786 210.85577393 6.47441912
		 -5.96709156 198.056289673 25.26384735 0.37559786 191.43711853 28.45916367 6.71828747 198.056289673 25.26384735
		 14.47785759 208.03338623 6.28395319 21.81600761 204.47509766 0.002745284 14.47785759 208.03338623 -6.27846193
		 6.91061974 210.85577393 0.002745284 25.88396645 198.056289673 6.28395319 -25.75857162 198.22451782 0.002745284
		 -13.918993 208.58082581 0.002745284 0.37559786 208.58076477 14.15877628 0.37559786 198.22451782 25.88357925
		 14.6701889 208.58076477 0.002745284 26.50976181 198.22451782 0.002745284 0.37559786 198.22451782 -25.87809372
		 0.37559786 208.58082581 -14.15328217 0.37559786 211.61410522 0.002745284 -13.14966583 206.39122009 13.3969059
		 -23.25537491 197.55157471 13.3969059 -13.14966774 197.55157471 23.40465546 24.0065746307 197.55157471 13.3969059
		 13.90086365 206.39122009 13.3969059 13.90086365 197.55157471 23.40465546 -13.14966583 197.55157471 -23.39916801
		 -23.25537491 197.55157471 -13.39141464 -13.14966774 206.39122009 -13.39141464 24.0065746307 197.55157471 -13.39141464
		 13.90086365 197.55157471 -23.39916801 13.90086365 206.39122009 -13.39141464 -20.09154129 38.46523666 20.84230614
		 -24.55636787 38.54935074 13.9683075 -26.97631454 38.67553329 6.42680502 -5.53493738 38.67553329 27.66033554
		 -13.15025806 38.54935074 25.26384735 21.99554825 38.46523666 20.84230614 15.054263115 38.54935074 25.26384735
		 7.43894148 38.67553329 27.66033554 28.88031578 38.67553329 6.42680311 26.4603672 38.54935074 13.9683075
		 -26.97631454 38.67553329 -6.42131281 -27.7829628 38.71759415 0.002745284 0.95200175 38.71759415 28.45916367
		 29.68696594 38.71759415 0.002745284 28.88031578 38.67553329 -6.42131233 -24.55636787 38.54935074 -13.96281433
		 -20.091545105 38.46523666 -20.8368187 -13.15025806 38.54935074 -25.25836182 -5.53493738 38.67553329 -27.65485382
		 7.43894148 38.67553329 -27.65485382 0.95200175 38.71759415 -28.45368195 15.054263115 38.54935074 -25.25836182
		 21.99554825 38.46523666 -20.8368187 26.4603672 38.54935074 -13.96281433 -20.09154129 55.74500275 20.84230614
		 -24.55636978 55.82912064 13.96830845 -26.97631454 55.95529938 6.4268055 -27.7829628 55.99736023 0.002745284
		 -26.97631454 55.95529938 -6.42131281 -24.55636978 55.82912064 -13.96281433 -20.091545105 55.74500275 -20.8368187
		 -13.15025806 55.82912064 -25.25836182 -5.53493786 55.95529938 -27.65485382 0.95200181 55.99736023 -28.45368385
		 7.43894196 55.95529938 -27.65485382 15.054264069 55.82912064 -25.25836182 21.99554825 55.74500275 -20.8368187;
	setAttr ".vt[166:331]" 26.46036911 55.82912064 -13.96281433 28.88031769 55.95529938 -6.42131233
		 29.68696594 55.99736023 0.002745284 28.88031769 55.95529938 6.42680311 26.46036911 55.82912064 13.96830845
		 21.99554825 55.74500275 20.84230614 15.054264069 55.82912064 25.26384735 7.43894196 55.95529938 27.66033745
		 0.95200181 55.99736023 28.45916367 -5.53493786 55.95529938 27.66033745 -13.15025806 55.82912064 25.26384735
		 -20.14394379 68.05770874 -20.8368187 -24.60876846 68.14183044 -13.96281433 -27.028715134 68.26800537 -6.42131281
		 -27.83536339 68.31007385 0.002745284 -27.028715134 68.26800537 6.4268055 -24.60876846 68.14183044 13.96830845
		 -20.14394188 68.05770874 20.84230614 -13.20265865 68.14183044 25.26384735 -5.58733845 68.26800537 27.66033554
		 0.89960146 68.31007385 28.45916367 7.38654137 68.26800537 27.66033554 15.0018634796 68.14183044 25.26384735
		 21.94314766 68.05770874 20.84230614 26.40796852 68.14183044 13.96830845 28.8279171 68.26800537 6.42680311
		 29.63456535 68.31007385 0.002745284 28.8279171 68.26800537 -6.42131233 26.40796852 68.14183044 -13.96281433
		 21.94314766 68.05770874 -20.8368187 15.0018634796 68.14183044 -25.25836182 7.38654137 68.26800537 -27.65485382
		 0.89960146 68.31007385 -28.45368385 -5.58733845 68.26800537 -27.65485382 -13.20265865 68.14183044 -25.25836182
		 -20.19634438 80.37041473 -20.8368187 -24.66116714 80.45453644 -13.96281433 -27.081115723 80.58071136 -6.42131281
		 -27.88776398 80.62277985 0.002745284 -27.081115723 80.58071136 6.4268055 -24.66116714 80.45453644 13.9683075
		 -20.19634247 80.37041473 20.84230614 -13.25505924 80.45453644 25.26384735 -5.63973904 80.58071136 27.66033554
		 0.84720111 80.62277985 28.45916367 7.33414078 80.58071136 27.66033554 14.94946289 80.45453644 25.26384735
		 21.89074707 80.37041473 20.84230614 26.35556984 80.45453644 13.9683075 28.77551651 80.58071136 6.42680311
		 29.58216476 80.62277985 0.002745284 28.77551651 80.58071136 -6.42131233 26.35556984 80.45453644 -13.96281433
		 21.89074707 80.37041473 -20.8368187 14.94946289 80.45453644 -25.25836182 7.33414078 80.58071136 -27.65485382
		 0.84720111 80.62277985 -28.45368385 -5.63973904 80.58071136 -27.65485382 -13.25505924 80.45453644 -25.25836182
		 -20.24874496 92.68312073 -20.8368187 -24.71356773 92.76724243 -13.96281433 -27.13351631 92.89340973 -6.42131281
		 -27.94016457 92.93548584 0.0027452842 -27.13351631 92.89340973 6.4268055 -24.71356773 92.76724243 13.9683075
		 -20.24874306 92.68312073 20.84230614 -13.30745983 92.76724243 25.26384735 -5.69213963 92.89340973 27.66033554
		 0.79480076 92.93548584 28.45916367 7.28174019 92.89340973 27.66033554 14.8970623 92.76724243 25.26384735
		 21.83834839 92.68312073 20.84230614 26.30316925 92.76724243 13.9683075 28.72311592 92.89340973 6.42680311
		 29.52976418 92.93548584 0.0027452842 28.72311592 92.89340973 -6.42131233 26.30316925 92.76724243 -13.96281433
		 21.83834839 92.68312073 -20.8368187 14.8970623 92.76724243 -25.25836182 7.28174019 92.89340973 -27.65485382
		 0.79480076 92.93548584 -28.45368385 -5.69213963 92.89340973 -27.65485382 -13.30745983 92.76724243 -25.25836182
		 -20.30114555 104.99582672 -20.8368187 -24.76596832 105.079948425 -13.96281433 -27.1859169 105.20610809 -6.42131281
		 -27.99256325 105.24819183 0.0027452842 -27.1859169 105.20610809 6.4268055 -24.76596832 105.079948425 13.9683075
		 -20.30114174 104.99582672 20.84230614 -13.35986042 105.079948425 25.26384735 -5.74454021 105.20610809 27.66033554
		 0.74240041 105.24819183 28.45916367 7.22934008 105.20610809 27.66033554 14.84466171 105.079948425 25.26384735
		 21.7859478 104.99582672 20.84230614 26.25077057 105.079948425 13.9683075 28.67071533 105.20610809 6.42680311
		 29.47736359 105.24819183 0.0027452842 28.67071533 105.20610809 -6.42131233 26.25077057 105.079948425 -13.96281433
		 21.7859478 104.99582672 -20.8368187 14.84466171 105.079948425 -25.25836182 7.22934008 105.20610809 -27.65485382
		 0.74240041 105.24819183 -28.45368195 -5.74454021 105.20610809 -27.65485382 -13.35986042 105.079948425 -25.25836182
		 -20.35354614 117.30852509 -20.8368187 -24.81836891 117.39264679 -13.96281433 -27.23831749 117.51881409 -6.42131281
		 -28.044963837 117.56089783 0.0027452842 -27.23831749 117.51881409 6.4268055 -24.81836891 117.39264679 13.9683075
		 -20.35354233 117.30852509 20.84230614 -13.41226101 117.39264679 25.26384735 -5.79694033 117.51881409 27.66033554
		 0.69000006 117.56089783 28.45916367 7.17693949 117.51881409 27.66033554 14.79226112 117.39264679 25.26384735
		 21.73354721 117.30852509 20.84230614 26.19836998 117.39264679 13.9683075 28.61831474 117.51881409 6.42680311
		 29.424963 117.56089783 0.0027452842 28.61831474 117.51881409 -6.42131233 26.19836998 117.39264679 -13.96281433
		 21.73354721 117.30852509 -20.8368187 14.79226112 117.39264679 -25.25836182 7.17693949 117.51881409 -27.65485382
		 0.69000006 117.56089783 -28.45368195 -5.79694033 117.51881409 -27.65485382 -13.41226101 117.39264679 -25.25836182
		 -20.40594673 129.62123108 -20.8368187 -24.8707695 129.70535278 -13.96281433 -27.29071617 129.83151245 -6.42131281
		 -28.097362518 129.87361145 0.0027452842 -27.29071617 129.83151245 6.4268055 -24.8707695 129.70535278 13.9683075
		 -20.40594292 129.62123108 20.84230614 -13.46466064 129.70535278 25.26384735 -5.84934044 129.83151245 27.66033554
		 0.63759971 129.87361145 28.45916367 7.12453938 129.83151245 27.66033554 14.73986053 129.70535278 25.26384735
		 21.68114662 129.62123108 20.84230614 26.14596939 129.70535278 13.9683075 28.56591415 129.83151245 6.42680311
		 29.37256241 129.87361145 0.0027452842 28.56591415 129.83151245 -6.42131233 26.14596939 129.70535278 -13.96281433
		 21.68114471 129.62123108 -20.8368187 14.73986053 129.70535278 -25.25836182 7.12453938 129.83151245 -27.65485382
		 0.63759971 129.87361145 -28.45368195 -5.84934044 129.83151245 -27.65485382 -13.46466064 129.70535278 -25.25836182
		 -20.45834732 141.93392944 -20.8368187 -24.92317009 142.018051147 -13.96281433 -27.34311676 142.14421082 -6.42131281
		 -28.14976311 142.18630981 0.0027452842 -27.34311676 142.14421082 6.4268055 -24.92317009 142.018051147 13.9683075
		 -20.45834351 141.93392944 20.84230614 -13.51706123 142.018051147 25.26384735 -5.90174055 142.14421082 27.66033554
		 0.58519936 142.18630981 28.45916367 7.072138786 142.14421082 27.66033554;
	setAttr ".vt[332:497]" 14.68745995 142.018051147 25.26384735 21.62874603 141.93392944 20.84230614
		 26.093570709 142.018051147 13.9683075 28.51351357 142.14421082 6.42680359 29.32016182 142.18630981 0.0027452842
		 28.51351357 142.14421082 -6.42131233 26.093570709 142.018051147 -13.96281433 21.62874413 141.93392944 -20.8368187
		 14.68745995 142.018051147 -25.25836182 7.072138786 142.14421082 -27.65485382 0.58519936 142.18630981 -28.45368195
		 -5.90174055 142.14421082 -27.65485382 -13.51706123 142.018051147 -25.25836182 -20.51074791 154.24662781 -20.8368187
		 -24.97557068 154.33074951 -13.96281433 -27.39551544 154.45690918 -6.42131281 -28.2021637 154.49900818 0.0027452842
		 -27.39551544 154.45690918 6.4268055 -24.97557068 154.33074951 13.9683075 -20.51074409 154.24662781 20.84230614
		 -13.56946182 154.33074951 25.26384735 -5.95414066 154.45690918 27.66033554 0.53279901 154.49900818 28.45916367
		 7.019738197 154.45690918 27.66033554 14.63505936 154.33074951 25.26384735 21.57634544 154.24662781 20.84230614
		 26.04117012 154.33074951 13.9683075 28.46111298 154.45690918 6.42680359 29.26776123 154.49900818 0.0027452842
		 28.46111298 154.45690918 -6.42131233 26.04117012 154.33074951 -13.96281433 21.57634354 154.24662781 -20.8368187
		 14.63505936 154.33074951 -25.25836182 7.019738197 154.45690918 -27.65485382 0.53279901 154.49900818 -28.45368195
		 -5.95414066 154.45690918 -27.65485382 -13.56946182 154.33074951 -25.25836182 -20.5631485 166.55932617 -20.8368187
		 -25.02796936 166.64346313 -13.96281433 -27.44791412 166.7696228 -6.42131233 -28.25456429 166.8117218 0.0027452842
		 -27.44791412 166.7696228 6.4268055 -25.02796936 166.64346313 13.9683075 -20.56314468 166.55932617 20.84230614
		 -13.62186241 166.64346313 25.26384735 -6.0065412521 166.7696228 27.66033554 0.4803986 166.8117218 28.45916367
		 6.96733761 166.7696228 27.66033554 14.58265877 166.64346313 25.26384735 21.52394485 166.55932617 20.84230614
		 25.98876953 166.64346313 13.9683075 28.40871048 166.7696228 6.42680359 29.21535873 166.8117218 0.0027452842
		 28.40871048 166.7696228 -6.42131233 25.98876953 166.64346313 -13.96281433 21.52394295 166.55932617 -20.8368187
		 14.58265877 166.64346313 -25.25836182 6.96733761 166.7696228 -27.65485382 0.4803986 166.8117218 -28.45368195
		 -6.0065412521 166.7696228 -27.65485382 -13.62186241 166.64346313 -25.25836182 -20.61554718 178.87203979 -20.8368187
		 -25.080369949 178.9561615 -13.96281433 -27.50031471 179.082336426 -6.42131233 -28.30696487 179.12442017 0.0027452842
		 -27.50031471 179.082336426 6.4268055 -25.080369949 178.9561615 13.9683075 -20.61554337 178.87203979 20.84230614
		 -13.67426205 178.9561615 25.26384735 -6.058941364 179.082336426 27.66033554 0.42799824 179.12442017 28.45916367
		 6.91493702 179.082336426 27.66033554 14.53025818 178.9561615 25.26384735 21.47154617 178.87203979 20.84230614
		 25.93636703 178.9561615 13.9683075 28.3563118 179.082336426 6.42680359 29.16296005 179.12442017 0.0027452842
		 28.3563118 179.082336426 -6.42131233 25.93636703 178.9561615 -13.96281433 21.47154236 178.87203979 -20.8368187
		 14.53025818 178.9561615 -25.25836182 6.91493702 179.082336426 -27.65485382 0.42799824 179.12442017 -28.45368195
		 -6.058941364 179.082336426 -27.65485382 -13.67426205 178.9561615 -25.25836182 -123.90559387 126.79549408 6.68662357
		 -116.1126709 122.58179474 6.83233881 -115.98396301 126.79549408 15.65507126 -116.1126709 135.70458984 19.82791519
		 -123.90558624 135.85173035 15.65507126 -127.59139252 135.70458984 6.83233786 -123.90558624 152.55409241 6.68662357
		 -127.59137726 143.64500427 6.83233881 -123.90559387 143.49787903 15.65507126 -116.11267853 143.64500427 19.82791519
		 -115.98395538 152.55410767 15.65507126 -116.1126709 156.76779175 6.83233786 -115.98396301 126.79549408 -9.85384178
		 -116.1126709 122.58179474 -1.031106591 -123.90559387 126.79549408 -0.88539153 -127.59137726 135.70458984 -1.031105995
		 -123.90558624 135.85173035 -9.85384178 -116.1126709 135.70458984 -14.026689529 -115.98395538 152.55410767 -9.85384178
		 -116.11267853 143.64500427 -14.026689529 -123.90559387 143.49787903 -9.85384178 -127.59137726 143.64500427 -1.031106591
		 -123.90558624 152.55409241 -0.88539153 -116.1126709 156.76779175 -1.031105995 -122.19915771 128.74633789 13.72314072
		 -122.19916534 150.60325623 13.72314072 -122.19915771 128.74633789 -7.92190933 -122.19916534 150.60325623 -7.92190933
		 -116.1384201 157.26148987 2.90061569 -120.18956757 155.28671265 -0.94367731 -116.035446167 124.06288147 -5.64673853
		 -116.035438538 131.043792725 -12.55996323 -120.18955231 135.7928772 -12.55996323
		 -124.11807251 139.67480469 -10.094377518 -120.18956757 143.55671692 -12.55996323
		 -116.1384201 139.67480469 -14.51559639 -126.29585266 135.7928772 -5.64673853 -128.023239136 139.67478943 -1.060249329
		 -126.29585266 143.55673218 -5.64673853 -116.035461426 148.30581665 -12.55996323 -116.035446167 155.28671265 -5.64673853
		 -123.26821136 127.52416992 10.86511517 -123.26821136 131.63233948 14.93345642 -119.67472839 127.52416229 14.93345642
		 -123.26821136 151.82542419 10.86511517 -119.67472839 151.82542419 14.93345642 -123.26821136 147.7172699 14.93345642
		 -119.67472839 127.52416229 -9.13222313 -123.26821136 131.63233948 -9.13222313 -123.26821136 127.52416992 -5.063879967
		 -119.67472839 151.82542419 -9.13222313 -123.26821136 151.82542419 -5.063879967 -123.26821136 147.7172699 -9.13222313
		 -116.035446167 124.06288147 11.44797134 -116.035438538 131.043792725 18.3611908 -116.035461426 148.30581665 18.3611908
		 -116.035446167 155.28671265 11.44797134 -120.18955231 124.06288147 6.74490929 -124.11806488 126.55259705 2.90061569
		 -120.18955231 124.06288147 -0.94367731 -116.1384201 122.088111877 2.90061569 -126.29584503 131.043777466 6.74490929
		 -128.023254395 135.67515564 2.90061569 -126.29584503 131.043777466 -0.94367731 -126.29585266 135.7928772 11.44797134
		 -124.11807251 139.67480469 15.89560795 -126.29585266 143.55673218 11.44797134 -128.023239136 139.67478943 6.86148024
		 -120.18955231 135.7928772 18.3611908 -116.1384201 139.67480469 20.31682396 -120.18956757 143.55671692 18.3611908
		 -126.29584503 148.30581665 6.74490929 -124.11807251 152.79699707 2.90061569 -126.29584503 148.30581665 -0.94367731
		 -128.023239136 143.67442322 2.90061569 -120.18956757 155.28671265 6.74490929 -120.29251862 123.67987823 2.90061569
		 -126.63090515 130.92607117 2.90061569 -126.630867 139.67478943 11.56454468 -120.29251862 139.67478943 18.74048615
		 -126.63087463 148.42352295 2.90061569;
	setAttr ".vt[498:663]" -120.29251862 155.66972351 2.90061569 -120.29251862 139.67478943 -12.93925858
		 -126.63091278 139.67480469 -5.76331043 -128.48736572 139.67478943 2.90061569 -125.29079437 131.39692688 11.098257065
		 -119.88066864 125.21190643 11.098257065 -119.88066101 131.39692688 17.22330666 -119.88066101 154.13768005 11.098257065
		 -125.29079437 147.9526825 11.098257065 -119.88066864 147.9526825 17.22330666 -119.88066101 131.39692688 -11.42207813
		 -119.88066864 125.21190643 -5.29702377 -125.29079437 131.39692688 -5.29702377 -119.88066101 154.13768005 -5.29702377
		 -119.88066864 147.9526825 -11.42207813 -125.29079437 147.9526825 -5.29702377 -5.96544504 127.14826965 15.65507126
		 -6.016924381 124.41565704 11.44797134 -6.094152927 122.93457031 6.83233881 -6.094152927 136.057357788 19.82791519
		 -6.016924381 131.39656067 18.3611908 -5.96544313 152.90687561 15.65507126 -6.016926289 148.65859985 18.3611908
		 -6.09415102 143.99778748 19.82791519 -6.09415102 157.12055969 6.83233786 -6.016926289 155.63948059 11.44797134
		 -6.094152927 122.93457031 -1.031106591 -6.11989641 122.44087982 2.90061569 -6.11989641 140.027572632 20.31682396
		 -6.11989641 157.61425781 2.90061569 -6.09415102 157.12055969 -1.031105995 -6.016924381 124.41565704 -5.64673853
		 -5.96544504 127.14826965 -9.85384178 -6.016924381 131.39656067 -12.55996323 -6.094152927 136.057357788 -14.026689529
		 -6.09415102 143.99778748 -14.026689529 -6.11989641 140.027572632 -14.51559639 -6.016926289 148.65859985 -12.55996323
		 -5.96544504 152.90687561 -9.85384178 -6.016926289 155.63948059 -5.64673853 -16.018611908 155.60740662 -5.64673853
		 -15.96713066 152.87480164 -9.85384178 -16.018613815 148.62652588 -12.55996323 -16.095838547 143.9657135 -14.026689529
		 -16.12158394 139.99549866 -14.51559639 -16.095840454 136.025283813 -14.026689529
		 -16.018611908 131.36448669 -12.55996323 -15.96713257 127.11620331 -9.85384178 -16.018611908 124.3835907 -5.64673853
		 -16.095840454 122.90250397 -1.031106591 -16.12158394 122.40880585 2.90061569 -16.095840454 122.90250397 6.83233881
		 -16.018611908 124.3835907 11.44797134 -15.96713257 127.11620331 15.65507126 -16.018611908 131.36448669 18.3611908
		 -16.095840454 136.025283813 19.82791519 -16.12158394 139.99549866 20.31682396 -16.095838547 143.9657135 19.82791519
		 -16.018613815 148.62652588 18.3611908 -15.96712875 152.87480164 15.65507126 -16.018611908 155.60740662 11.44797134
		 -16.095838547 157.088485718 6.83233786 -16.12158394 157.58218384 2.90061569 -16.095838547 157.088485718 -1.031105995
		 -26.020298004 155.57533264 -5.64673853 -25.96881485 152.84274292 -9.85384178 -26.020299911 148.5944519 -12.55996323
		 -26.097522736 143.93363953 -14.026689529 -26.12326813 139.96342468 -14.51559639 -26.097524643 135.99320984 -14.026689529
		 -26.020296097 131.33241272 -12.55996323 -25.96881676 127.084129333 -9.85384178 -26.020298004 124.35151672 -5.64673853
		 -26.097524643 122.87042999 -1.031106591 -26.12326813 122.3767395 2.90061569 -26.097524643 122.87042999 6.83233881
		 -26.020298004 124.35151672 11.44797134 -25.96881676 127.084129333 15.65507126 -26.020296097 131.33241272 18.3611908
		 -26.097524643 135.99320984 19.82791519 -26.12326813 139.96342468 20.31682396 -26.097522736 143.93363953 19.82791519
		 -26.020299911 148.5944519 18.3611908 -25.96881485 152.84274292 15.65507126 -26.020298004 155.57533264 11.44797134
		 -26.097522736 157.056427002 6.83233833 -26.12326813 157.55010986 2.90061569 -26.097522736 157.056427002 -1.031105995
		 -36.021980286 155.54327393 -5.64673853 -35.97049332 152.8106842 -9.85384178 -36.0219841 148.56237793 -12.55996323
		 -36.099205017 143.90156555 -14.026689529 -36.12495041 139.93135071 -14.51559639 -36.099208832 135.96115112 -14.026689529
		 -36.021976471 131.30033875 -12.55996323 -35.97049713 127.052055359 -9.85384178 -36.021980286 124.31945038 -5.64673853
		 -36.099208832 122.83836365 -1.031106591 -36.12495041 122.34467316 2.90061569 -36.099208832 122.83836365 6.83233881
		 -36.021980286 124.31945038 11.44797134 -35.97049713 127.052055359 15.65507126 -36.021976471 131.30033875 18.3611908
		 -36.099208832 135.96115112 19.82791519 -36.12495041 139.93135071 20.31682396 -36.099205017 143.90156555 19.82791519
		 -36.0219841 148.56237793 18.3611908 -35.97049332 152.8106842 15.65507126 -36.021980286 155.54327393 11.44797134
		 -36.099205017 157.024368286 6.83233833 -36.12495041 157.51805115 2.90061569 -36.099205017 157.024368286 -1.031105995
		 -46.023662567 155.51119995 -5.64673853 -45.9721756 152.77861023 -9.85384178 -46.023670197 148.53030396 -12.55996323
		 -46.1008873 143.86949158 -14.026689529 -46.12663269 139.89927673 -14.51559639 -46.10089111 135.92907715 -14.026689529
		 -46.023658752 131.26828003 -12.55996323 -45.97217941 127.019989014 -9.85384178 -46.023662567 124.28738403 -5.64673853
		 -46.10089111 122.8062973 -1.031106591 -46.12663269 122.31259918 2.90061569 -46.10089111 122.8062973 6.83233881
		 -46.023662567 124.28738403 11.44797134 -45.97217941 127.019989014 15.65507126 -46.023658752 131.26828003 18.3611908
		 -46.10089111 135.92907715 19.82791519 -46.12663269 139.89927673 20.31682396 -46.1008873 143.86949158 19.82791519
		 -46.023670197 148.53030396 18.3611908 -45.9721756 152.77861023 15.65507126 -46.023662567 155.51119995 11.44797134
		 -46.1008873 156.99229431 6.83233833 -46.12663269 157.48597717 2.90061569 -46.1008873 156.99229431 -1.031105995
		 -56.025348663 155.47912598 -5.64673853 -55.97385788 152.74653625 -9.85384178 -56.025352478 148.49822998 -12.55996323
		 -56.10257339 143.8374176 -14.026689529 -56.12831497 139.86720276 -14.51559639 -56.10257339 135.89700317 -14.026689529
		 -56.025341034 131.23620605 -12.55996323 -55.97386551 126.98791504 -9.85384178 -56.025348663 124.25531006 -5.64673853
		 -56.10257339 122.77422333 -1.031106591 -56.12831497 122.28052521 2.90061569 -56.10257339 122.77422333 6.83233881
		 -56.025348663 124.25531006 11.44797134 -55.97386551 126.98791504 15.65507126 -56.025341034 131.23620605 18.3611908
		 -56.10257339 135.89700317 19.82791519 -56.12831497 139.86720276 20.31682396 -56.10257339 143.8374176 19.82791519
		 -56.025352478 148.49822998 18.3611908 -55.97385788 152.74653625 15.65507126 -56.025348663 155.47912598 11.44797134
		 -56.10257339 156.9602356 6.83233833 -56.12831497 157.45391846 2.90061569 -56.10257339 156.9602356 -1.031105995
		 -66.027038574 155.447052 -5.64673853 -65.97554016 152.71446228 -9.85384178 -66.027046204 148.46615601 -12.55996323
		 -66.10426331 143.80534363 -14.026689529 -66.13000488 139.83514404 -14.51559639 -66.10426331 135.8649292 -14.026689529;
	setAttr ".vt[664:829]" -66.027030945 131.20413208 -12.55996323 -65.97555542 126.95584869 -9.85384178
		 -66.027038574 124.22324371 -5.64673853 -66.10426331 122.74214935 -1.031106591 -66.13000488 122.24845123 2.90061569
		 -66.10426331 122.74214935 6.83233881 -66.027038574 124.22324371 11.44797134 -65.97555542 126.95584869 15.65507126
		 -66.027030945 131.20413208 18.3611908 -66.10426331 135.8649292 19.82791519 -66.13000488 139.83514404 20.31682396
		 -66.10426331 143.80534363 19.82791519 -66.027046204 148.46615601 18.3611908 -65.97554016 152.71446228 15.65507126
		 -66.027038574 155.447052 11.44797134 -66.10426331 156.92816162 6.83233833 -66.13000488 157.42184448 2.90061569
		 -66.10426331 156.92816162 -1.031105995 -76.028717041 155.41497803 -5.64673853 -75.97722626 152.68238831 -9.85384178
		 -76.0287323 148.43408203 -12.55996323 -76.10594177 143.77326965 -14.026689529 -76.13169098 139.80307007 -14.51559639
		 -76.10594177 135.83285522 -14.026689529 -76.028709412 131.17207336 -12.55996323 -75.97723389 126.92377472 -9.85384178
		 -76.028717041 124.19116974 -5.64673853 -76.10594177 122.71008301 -1.031106591 -76.13169098 122.21638489 2.90061569
		 -76.10594177 122.71008301 6.83233929 -76.028717041 124.19116974 11.44797134 -75.97723389 126.92377472 15.65507126
		 -76.028709412 131.17207336 18.3611908 -76.10594177 135.83285522 19.82791519 -76.13169098 139.80307007 20.31682396
		 -76.10594177 143.77326965 19.82791519 -76.0287323 148.43408203 18.3611908 -75.97722626 152.68238831 15.65507126
		 -76.028717041 155.41497803 11.44797134 -76.10594177 156.89608765 6.83233833 -76.13169098 157.38977051 2.90061569
		 -76.10594177 156.89608765 -1.031105995 -86.030395508 155.38291931 -5.64673853 -85.97891235 152.65031433 -9.85384178
		 -86.030410767 148.40202332 -12.55996323 -86.10762787 143.74119568 -14.026689529 -86.13336945 139.77099609 -14.51559639
		 -86.10762024 135.80078125 -14.026689529 -86.030395508 131.13999939 -12.55996323 -85.97891235 126.89170074 -9.85384178
		 -86.030395508 124.15909576 -5.64673853 -86.10762024 122.67800903 -1.031106591 -86.13336945 122.18431854 2.90061569
		 -86.10762024 122.67800903 6.83233881 -86.030395508 124.15909576 11.44797134 -85.97891235 126.89170074 15.65507126
		 -86.030395508 131.13999939 18.3611908 -86.10762024 135.80078125 19.82791519 -86.13336945 139.77099609 20.31682396
		 -86.10762787 143.74119568 19.82791519 -86.030410767 148.40202332 18.3611908 -85.97891235 152.65031433 15.65507126
		 -86.030395508 155.38291931 11.44797134 -86.10762024 156.86401367 6.83233833 -86.13336945 157.35771179 2.90061569
		 -86.10762024 156.86401367 -1.031105995 -96.032073975 155.35084534 -5.64673853 -95.98059082 152.61824036 -9.85384178
		 -96.032096863 148.36994934 -12.55996323 -96.10931396 143.70913696 -14.026689529 -96.13505554 139.73893738 -14.51559639
		 -96.10929871 135.76872253 -14.026689529 -96.032073975 131.10792542 -12.55996323 -95.98059845 126.85962677 -9.85384178
		 -96.032073975 124.12702179 -5.64673853 -96.10929871 122.64593506 -1.031106591 -96.13505554 122.15224457 2.90061569
		 -96.10929871 122.64593506 6.83233833 -96.032073975 124.12702179 11.44797134 -95.98059845 126.85962677 15.65507126
		 -96.032073975 131.10792542 18.3611908 -96.10929871 135.76872253 19.82791519 -96.13505554 139.73893738 20.31682396
		 -96.10931396 143.70913696 19.82791519 -96.032096863 148.36994934 18.3611908 -95.98059082 152.61824036 15.65507126
		 -96.032073975 155.35084534 11.44797134 -96.10929871 156.8319397 6.83233833 -96.13505554 157.32563782 2.90061569
		 -96.10929871 156.8319397 -1.031105995 -106.033760071 155.31877136 -5.64673853 -105.98226929 152.58616638 -9.85384178
		 -106.033782959 148.33787537 -12.55996323 -106.11099243 143.67707825 -14.026689529
		 -106.13673401 139.7068634 -14.51559639 -106.1109848 135.73664856 -14.026689529 -106.033752441 131.075866699 -12.55996323
		 -105.98228455 126.82756042 -9.85384178 -106.033760071 124.094947815 -5.64673853 -106.1109848 122.61386871 -1.031106591
		 -106.13673401 122.12017822 2.90061569 -106.1109848 122.61386871 6.83233833 -106.033760071 124.094947815 11.44797134
		 -105.98228455 126.82756042 15.65507126 -106.033752441 131.075866699 18.3611908 -106.1109848 135.73664856 19.82791519
		 -106.13673401 139.7068634 20.31682396 -106.11099243 143.67707825 19.82791519 -106.033782959 148.33787537 18.3611908
		 -105.98226929 152.58616638 15.65507126 -106.033760071 155.31877136 11.44797134 -106.1109848 156.79986572 6.83233833
		 -106.13673401 157.29356384 2.90061569 -106.1109848 156.79986572 -1.031105995 119.65740967 152.90686035 4.96943617
		 111.86450195 157.12055969 5.11515141 111.73577881 152.90687561 13.93788433 111.86450195 143.99777222 18.11072922
		 119.65740967 143.85064697 13.93788433 123.34322357 143.99778748 5.11515045 119.65742493 127.14826965 4.96943617
		 123.34319305 136.057357788 5.11515141 119.65740967 136.20449829 13.93788433 111.86449432 136.057357788 18.11072922
		 111.73577881 127.14827728 13.93788433 111.86450195 122.93458557 5.11515045 111.73577881 152.90687561 -11.57102966
		 111.86450195 157.12055969 -2.74829412 119.65740967 152.90686035 -2.60257912 123.34320068 143.99777222 -2.7482934
		 119.65740967 143.85064697 -11.57102966 111.86450195 143.99777222 -15.74387646 111.73577881 127.14827728 -11.57102966
		 111.86449432 136.057357788 -15.74387646 119.65740967 136.20449829 -11.57102966 123.34319305 136.057357788 -2.74829412
		 119.65742493 127.14826965 -2.60257912 111.86450195 122.93458557 -2.7482934 117.95098114 150.95602417 12.0059528351
		 117.95098114 129.099121094 12.0059528351 117.95098114 150.95602417 -9.63909721 117.95098114 129.099121094 -9.63909721
		 111.89024353 122.44088745 1.18342829 115.94138336 124.41566467 -2.66086483 111.78726959 155.63948059 -7.36392593
		 111.78727722 148.65858459 -14.27715111 115.94137573 143.90950012 -14.27715111 119.86988831 140.027587891 -11.8115654
		 115.94138336 136.14564514 -14.27715111 111.89024353 140.027572632 -16.23278427 122.047676086 143.90950012 -7.36392593
		 123.77506256 140.027572632 -2.77743673 122.047668457 136.14564514 -7.36392593 111.78726959 131.39656067 -14.27715111
		 111.78726959 124.41566467 -7.36392593 119.02003479 152.17819214 9.14792728 119.02003479 148.070037842 13.21626854
		 115.42654419 152.17819214 13.21626854 119.02003479 127.87695313 9.14792728 115.42655182 127.87695313 13.21626854
		 119.02003479 131.98510742 13.21626854 115.42654419 152.17819214 -10.84941006 119.02003479 148.070037842 -10.84941006
		 119.02003479 152.17819214 -6.78106737 115.42655182 127.87695313 -10.84941006 119.02003479 127.87695313 -6.78106737;
	setAttr ".vt[830:995]" 119.02003479 131.98510742 -10.84941006 111.78726959 155.63948059 9.73078442
		 111.78727722 148.65858459 16.64400482 111.78726959 131.39656067 16.64400482 111.78726959 124.41566467 9.73078442
		 115.94138336 155.63948059 5.027721405 119.86988831 153.14976501 1.18342829 115.94138336 155.63948059 -2.66086483
		 111.89024353 157.61425781 1.18342829 122.047676086 148.65859985 5.027721405 123.77507019 144.027191162 1.18342829
		 122.047676086 148.65859985 -2.66086483 122.047676086 143.90950012 9.73078442 119.86988831 140.027587891 14.17842007
		 122.047668457 136.14564514 9.73078442 123.77506256 140.027572632 5.14429283 115.94137573 143.90950012 16.64400482
		 111.89024353 140.027572632 18.59963608 115.94138336 136.14564514 16.64400482 122.047668457 131.39656067 5.027721405
		 119.86989594 126.90538025 1.18342829 122.047668457 131.39656067 -2.66086483 123.77507019 136.027938843 1.18342829
		 115.94138336 124.41566467 5.027721405 116.044342041 156.022491455 1.18342829 122.38272095 148.77630615 1.18342829
		 122.38269806 140.027572632 9.8473568 116.044342041 140.027572632 17.023300171 122.38270569 131.27883911 1.18342829
		 116.044342041 124.032653809 1.18342829 116.044342041 140.027572632 -14.65644646 122.38272858 140.027572632 -7.48049784
		 124.23918152 140.027587891 1.18342829 121.042625427 148.30545044 9.38106918 115.63247681 154.490448 9.38106918
		 115.63248444 148.30545044 15.50612068 115.63248444 125.56468201 9.38106918 121.042617798 131.74969482 9.38106918
		 115.63247681 131.74967957 15.50612068 115.63248444 148.30545044 -13.13926506 115.63247681 154.490448 -7.014211655
		 121.042625427 148.30545044 -7.014211655 115.63248444 125.56468201 -7.014211655 115.63247681 131.74967957 -13.13926506
		 121.042617798 131.74969482 -7.014211655 7.18086624 152.55409241 13.93788433 7.23234558 155.28671265 9.73078442
		 7.30957413 156.76779175 5.11515141 7.30957222 143.64500427 18.11072922 7.23234558 148.30581665 16.64400482
		 7.18086433 126.79548645 13.93788433 7.23234558 131.043777466 16.64400482 7.30957413 135.70458984 18.11072922
		 7.30957222 122.58180237 5.11515045 7.23234558 124.06288147 9.73078442 7.30957413 156.76779175 -2.74829412
		 7.33530998 157.26147461 1.18342829 7.33530998 139.67480469 18.59963608 7.33530998 122.088111877 1.18342829
		 7.30957222 122.58180237 -2.7482934 7.23234558 155.28671265 -7.36392593 7.18086433 152.55409241 -11.57102966
		 7.23234558 148.30581665 -14.27715111 7.30957222 143.64500427 -15.74387646 7.30957413 135.70458984 -15.74387646
		 7.33530998 139.67480469 -16.23278427 7.23234558 131.043777466 -14.27715111 7.18086624 126.79548645 -11.57102966
		 7.23234558 124.06288147 -7.36392593 21.52371979 152.59605408 13.93788528 21.57520103 155.32867432 9.73078442
		 21.65243149 156.80975342 5.11515141 21.67816925 157.30343628 1.18342829 21.65243149 156.80975342 -2.74829412
		 21.57520103 155.32867432 -7.36392593 21.52371979 152.59605408 -11.57102966 21.57520294 148.34777832 -14.27715111
		 21.65242958 143.68696594 -15.74387646 21.67816925 139.71676636 -16.23278427 21.65242958 135.74655151 -15.74387646
		 21.57520103 131.085739136 -14.27715111 21.52371979 126.83744812 -11.57102966 21.57520103 124.10484314 -7.36392593
		 21.65242958 122.62376404 -2.7482934 21.67816925 122.13007355 1.18342829 21.65242958 122.62376404 5.11515045
		 21.57520103 124.10484314 9.73078442 21.52371979 126.83744812 13.93788528 21.57520103 131.085739136 16.64400482
		 21.65242958 135.74655151 18.11072922 21.67816925 139.71676636 18.59963608 21.65242958 143.68696594 18.11072922
		 21.57520294 148.34777832 16.64400482 29.72481918 152.62431335 -11.57102966 29.77630234 155.35693359 -7.36392593
		 29.85353279 156.8380127 -2.74829412 29.87926865 157.33169556 1.18342829 29.85353279 156.8380127 5.11515141
		 29.77630234 155.35693359 9.73078442 29.72481918 152.62431335 13.93788528 29.77630424 148.3760376 16.64400482
		 29.85353088 143.71520996 18.11072922 29.87926865 139.74502563 18.59963608 29.85353088 135.77481079 18.11072922
		 29.77630234 131.11399841 16.64400482 29.72481918 126.8657074 13.93788528 29.77630234 124.13310242 9.73078442
		 29.85353088 122.65202332 5.11515045 29.87926865 122.15833282 1.18342829 29.85353088 122.65202332 -2.7482934
		 29.77630234 124.13310242 -7.36392593 29.72481918 126.8657074 -11.57102966 29.77630234 131.11399841 -14.27715111
		 29.85353088 135.77481079 -15.74387646 29.87926865 139.74502563 -16.23278427 29.85353088 143.71520996 -15.74387646
		 29.77630424 148.3760376 -14.27715111 37.92591858 152.65257263 -11.57102966 37.97740173 155.38519287 -7.36392593
		 38.054634094 156.86627197 -2.74829412 38.080368042 157.35995483 1.18342829 38.054634094 156.86627197 5.11515141
		 37.97740173 155.38519287 9.73078442 37.92591858 152.65257263 13.93788528 37.97740555 148.40429688 16.64400482
		 38.05463028 143.74346924 18.11072922 38.080368042 139.77328491 18.59963608 38.05463028 135.80307007 18.11072922
		 37.97740173 131.14225769 16.64400482 37.92591858 126.89396667 13.93788528 37.97740173 124.16136169 9.73078442
		 38.05463028 122.68028259 5.11515045 38.080368042 122.1865921 1.18342829 38.05463028 122.68028259 -2.7482934
		 37.97740173 124.16136169 -7.36392593 37.92591858 126.89396667 -11.57102966 37.97740173 131.14225769 -14.27715111
		 38.05463028 135.80307007 -15.74387741 38.080368042 139.77328491 -16.23278427 38.05463028 143.74346924 -15.74387741
		 37.97740555 148.40429688 -14.27715111 46.12701416 152.68083191 -11.57102966 46.17849731 155.41345215 -7.36392593
		 46.25572968 156.89453125 -2.74829412 46.28146362 157.38821411 1.18342829 46.25572968 156.89453125 5.11515141
		 46.17849731 155.41345215 9.73078537 46.12701416 152.68083191 13.93788528 46.17850113 148.43255615 16.64400482
		 46.25572586 143.77172852 18.11072922 46.28146362 139.80154419 18.59963608 46.25572586 135.83131409 18.11072922
		 46.17849731 131.17050171 16.64400482 46.12701416 126.92222595 13.93788528 46.17849731 124.18962097 9.73078537
		 46.25572586 122.70854187 5.11515045 46.28146362 122.21485138 1.18342829 46.25572586 122.70854187 -2.7482934
		 46.17849731 124.18962097 -7.36392593 46.12701416 126.92222595 -11.57102966 46.17849731 131.17050171 -14.27715111
		 46.25572586 135.83131409 -15.74387741 46.28146362 139.80154419 -16.23278427 46.25572586 143.77172852 -15.74387741
		 46.17850113 148.43255615 -14.27715111 54.32810974 152.70909119 -11.57102966;
	setAttr ".vt[996:1161]" 54.3795929 155.44169617 -7.36392593 54.45682526 156.92277527 -2.74829412
		 54.4825592 157.41647339 1.18342829 54.45682526 156.92277527 5.11515141 54.3795929 155.44169617 9.73078442
		 54.32810974 152.70909119 13.93788528 54.37960052 148.46081543 16.64400482 54.45682144 143.79998779 18.11072922
		 54.4825592 139.82980347 18.59963608 54.45682144 135.85957336 18.11072922 54.3795929 131.19876099 16.64400482
		 54.32810974 126.95048523 13.93788528 54.3795929 124.21788025 9.73078442 54.45682144 122.73680115 5.11515045
		 54.4825592 122.24310303 1.18342829 54.45682144 122.73680115 -2.7482934 54.3795929 124.21788025 -7.36392593
		 54.32810974 126.95048523 -11.57102966 54.3795929 131.19876099 -14.27715111 54.45682144 135.85957336 -15.74387646
		 54.4825592 139.82980347 -16.23278427 54.45682144 143.79998779 -15.74387646 54.37960052 148.46081543 -14.27715111
		 62.52920532 152.73735046 -11.57102966 62.58068848 155.46995544 -7.36392593 62.65792084 156.95103455 -2.74829412
		 62.6836586 157.44473267 1.18342829 62.65792084 156.95103455 5.11515141 62.58068848 155.46995544 9.73078442
		 62.52920532 152.73735046 13.93788528 62.58069611 148.48907471 16.64400482 62.65791702 143.82824707 18.11072922
		 62.6836586 139.85806274 18.59963608 62.65791321 135.88783264 18.11072922 62.58068848 131.22702026 16.64400482
		 62.52920532 126.97873688 13.93788528 62.58068848 124.2461319 9.73078442 62.65791702 122.76506042 5.11515045
		 62.6836586 122.27135468 1.18342829 62.65791702 122.76506042 -2.7482934 62.58068848 124.2461319 -7.36392593
		 62.52920532 126.97873688 -11.57102966 62.58068848 131.22702026 -14.27715111 62.65791321 135.88783264 -15.7438755
		 62.6836586 139.85806274 -16.23278427 62.65791702 143.82824707 -15.7438755 62.58069611 148.48907471 -14.27715111
		 70.7303009 152.76560974 -11.57102966 70.78178406 155.49819946 -7.36392593 70.85902405 156.97929382 -2.74829412
		 70.884758 157.47299194 1.18342829 70.85902405 156.97929382 5.11515141 70.78178406 155.49819946 9.73078442
		 70.7303009 152.76560974 13.93788528 70.78179932 148.51733398 16.64400482 70.85901642 143.85650635 18.11072922
		 70.884758 139.88630676 18.59963608 70.85900879 135.91607666 18.11072922 70.78178406 131.25526428 16.64400482
		 70.7303009 127.0069961548 13.93788528 70.78178406 124.27438354 9.73078442 70.85901642 122.7933197 5.11515045
		 70.884758 122.29960632 1.18342829 70.85901642 122.7933197 -2.7482934 70.78178406 124.27438354 -7.36392593
		 70.7303009 127.0069961548 -11.57102966 70.78178406 131.25526428 -14.27715111 70.85900879 135.91607666 -15.7438755
		 70.884758 139.88630676 -16.23278427 70.85901642 143.85650635 -15.7438755 70.78179932 148.51733398 -14.27715111
		 78.93139648 152.79386902 -11.57102966 78.98287964 155.52645874 -7.36392593 79.060119629 157.0075531006 -2.74829412
		 79.085853577 157.50125122 1.18342829 79.060119629 157.0075531006 5.11515141 78.98287964 155.52645874 9.73078442
		 78.93139648 152.79386902 13.93788528 78.9828949 148.545578 16.64400482 79.060112 143.88476563 18.11072922
		 79.085853577 139.91455078 18.59963608 79.06010437 135.94433594 18.11072922 78.98287964 131.28352356 16.64400482
		 78.93139648 127.035255432 13.93788528 78.98287964 124.30263519 9.73078442 79.060112 122.82157898 5.11515045
		 79.085853577 122.3278656 1.18342829 79.060112 122.82157898 -2.7482934 78.98287964 124.30263519 -7.36392593
		 78.93139648 127.035255432 -11.57102966 78.98287964 131.28352356 -14.27715111 79.06010437 135.94433594 -15.7438755
		 79.085853577 139.91455078 -16.23278427 79.060112 143.88476563 -15.7438755 78.9828949 148.545578 -14.27715111
		 87.13249207 152.8221283 -11.57102966 87.18397522 155.55471802 -7.36392593 87.26121521 157.035812378 -2.74829412
		 87.28695679 157.5295105 1.18342829 87.26121521 157.035812378 5.11515141 87.18397522 155.55471802 9.73078442
		 87.13249207 152.8221283 13.93788528 87.18399048 148.57383728 16.64400482 87.26120758 143.9130249 18.11072922
		 87.28695679 139.94281006 18.59963608 87.26119995 135.97259521 18.11072922 87.18397522 131.31178284 16.64400482
		 87.13249207 127.063514709 13.93788528 87.18397522 124.33088684 9.73078442 87.26120758 122.84983826 5.11515045
		 87.28695679 122.35612488 1.18342829 87.26120758 122.84983826 -2.7482934 87.18397522 124.33088684 -7.36392593
		 87.13249207 127.063514709 -11.57102966 87.18397522 131.31178284 -14.27715111 87.26119995 135.97259521 -15.74387646
		 87.28695679 139.94281006 -16.23278427 87.26120758 143.9130249 -15.74387646 87.18399048 148.57383728 -14.27715111
		 95.33358765 152.85037231 -11.57102966 95.3850708 155.58297729 -7.36392593 95.46231079 157.064056396 -2.74829412
		 95.48805237 157.55775452 1.18342829 95.46231079 157.064056396 5.11515141 95.3850708 155.58297729 9.73078442
		 95.33358765 152.85037231 13.93788433 95.38508606 148.6020813 16.64400482 95.46231079 143.94128418 18.11072922
		 95.48805237 139.97106934 18.59963608 95.46229553 136.0008392334 18.11072922 95.3850708 131.34004211 16.64400482
		 95.33358765 127.091766357 13.93788433 95.3850708 124.35914612 9.73078442 95.46231079 122.87808228 5.11515045
		 95.48805237 122.38437653 1.18342829 95.46231079 122.87808228 -2.7482934 95.3850708 124.35914612 -7.36392593
		 95.33358765 127.091766357 -11.57102966 95.3850708 131.34004211 -14.27715111 95.46229553 136.0008392334 -15.74387741
		 95.48805237 139.97106934 -16.23278427 95.46231079 143.94128418 -15.74387741 95.38508606 148.6020813 -14.27715111
		 103.53468323 152.87863159 -11.57102966 103.58616638 155.61123657 -7.36392593 103.66340637 157.092315674 -2.74829412
		 103.68914795 157.58599854 1.18342829 103.66340637 157.092315674 5.11515141 103.58616638 155.61123657 9.73078442
		 103.53468323 152.87863159 13.93788433 103.58618164 148.63034058 16.64400482 103.66340637 143.9695282 18.11072922
		 103.68914795 139.99932861 18.59963608 103.66339111 136.029098511 18.11072922 103.58616638 131.36830139 16.64400482
		 103.53468323 127.12002563 13.93788433 103.58616638 124.3874054 9.73078442 103.66340637 122.90633392 5.11515045
		 103.68914795 122.41262817 1.18342829 103.66340637 122.90633392 -2.7482934 103.58616638 124.3874054 -7.36392593
		 103.53468323 127.12002563 -11.57102966 103.58616638 131.36830139 -14.27715111 103.66339111 136.029098511 -15.74387741
		 103.68914795 139.99932861 -16.23278427 103.66340637 143.9695282 -15.74387741;
	setAttr ".vt[1162:1327]" 103.58618164 148.63034058 -14.27715111 -106.066101074 126.76802063 -8.9980278
		 -106.24420166 130.42744446 -11.39491653 -101.86968994 130.5715332 -11.49996948 -101.6915741 126.91212463 -9.10308075
		 -103.98719788 123.47994995 -18.17110443 -95.49534607 127.72377014 15.046836853 -99.18082428 127.85308838 12.68701553
		 -96.90413666 126.87425232 9.077754974 -93.21866608 126.74493408 11.43757629 -97.57162476 118.072265625 13.69924927
		 -100.53903961 146.89193726 16.32499695 -104.85261536 146.16653442 16.51223755 -104.12168884 141.85154724 16.63420105
		 -99.80812073 142.57693481 16.44695663 -101.97488403 144.6993103 25.93797684 -104.43593597 146.44203186 -10.87793255
		 -103.80899048 150.039825439 -8.46316719 -99.56897736 150.033279419 -9.55422497 -100.19590759 146.43548584 -11.96898746
		 -103.93412018 153.63438416 -17.75521469 -103.57147217 153.55007935 0.93330902 -106.66728973 153.55007935 4.029128551
		 -103.57147217 153.55007935 7.1249485 -100.47564697 153.55007935 4.029128551 -103.57147217 163.020812988 4.029128551
		 -66.069992065 124.9095459 0.7224645 -67.34074402 126.63661957 -3.094678879 -63.1568718 126.94999695 -4.34573364
		 -61.88611221 125.22292328 -0.52859217 -65.08996582 117.25351715 -5.57859039 -82.90437317 136.24237061 17.4012146
		 -86.53595734 133.81825256 17.079750061 -84.10315704 130.29490662 16.16552734 -80.47157288 132.71905518 16.48698425
		 -82.96837616 131.24185181 26.019165039 -66.57640076 153.44946289 10.68356419 -68.92172241 150.76853943 13.22921848
		 -65.33203125 149.83811951 15.55658436 -62.98670578 152.51904297 13.01094532 -67.86682129 158.85569763 18.95315552
		 -78.64927673 153.46186829 -4.22372341 -81.74510193 154.6413269 -1.36138821 -78.64927673 155.82078552 1.500947
		 -75.55345154 154.6413269 -1.36138821 -78.64927673 163.39778137 -4.96961117 -72.62890625 138.75759888 -13.092700005
		 -71.069274902 142.75834656 -13.94700146 -66.97854614 141.22406006 -13.66408062 -68.53816986 137.22332764 -12.80977917
		 -69.89207458 138.046142578 -22.64687729 -44.56890106 129.59638977 -8.26698971 -43.009552002 133.066635132 -10.43356228
		 -38.92275238 131.85083008 -9.43953037 -40.48210907 128.38058472 -7.27295589 -41.34294891 125.58295441 -16.79718018
		 -44.34165192 127.52322388 15.57671833 -47.80587006 127.34880066 12.90520191 -45.13085938 127.30000305 9.439641
		 -41.66664124 127.47442627 12.11115742 -44.50201416 117.9489975 12.82223988 -41.084972382 146.42205811 14.9166708
		 -44.28147888 144.28955078 17.01499939 -41.60073853 140.88467407 17.63840103 -38.40423203 143.017166138 15.54007053
		 -38.46967316 147.41719055 24.47954178 -48.17341232 153.55007935 0.93330902 -51.2692337 153.55007935 4.029128551
		 -48.17341232 153.55007935 7.1249485 -45.077590942 153.55007935 4.029128551 -48.17341232 163.020812988 4.029128551
		 -44.20871735 147.43011475 -10.66913319 -42.64908981 150.96618652 -8.6119566 -38.55835724 149.64015198 -9.43397808
		 -40.11798477 146.1040802 -11.49115467 -41.47189331 153.32647705 -18.22038269 12.53678513 206.43417358 10.25120544
		 11.075585365 205.53817749 14.28135681 14.89108276 203.61541748 15.2388916 16.35228157 204.51141357 11.20875549
		 17.2831707 213.65441895 16.035675049 27.66135025 76.48366547 -2.55630589 28.032669067 80.67340088 -3.77625942
		 28.24145126 81.88050842 0.42745584 27.87013245 77.69078064 1.64740944 37.81354141 78.32435608 -1.26591778
		 47.50397491 128.88632202 12.43535995 45.93348312 126.73019409 8.96363449 49.92517853 125.1232605 8.15591812
		 51.49566269 127.27938843 11.62764359 46.81864548 119.5309906 15.79492569 49.32125473 146.41465759 14.84293747
		 45.48527908 144.31771851 14.60551453 47.22813797 140.89207458 16.70208359 51.064113617 142.98901367 16.93950844
		 45.70066452 147.42253113 24.070837021 48.27469635 153.55007935 0.42828369 45.17887497 153.55007935 3.52410364
		 48.27469635 153.55007935 6.61992264 51.37051392 153.55007935 3.52410388 48.27469254 163.020797729 3.52410388
		 45.54128265 130.81170654 -10.5413847 46.89074326 133.80851746 -13.43384647 51.0081100464 132.39016724 -12.98243713
		 49.65864944 129.39335632 -10.089976311 46.91608429 125.41573334 -18.80407524 46.57751465 147.075271606 -10.65468502
		 48.076221466 150.71006775 -8.72846794 52.033992767 149.99497986 -10.45847702 50.53528214 146.36018372 -12.38469315
		 46.8793869 153.58283997 -18.19384384 73.83604431 125.13092804 -1.98328352 74.92087555 126.093307495 -6.11428356
		 79.16228485 125.80490112 -5.067646027 78.077453613 124.84252167 -0.93664622 76.40817261 116.24992371 -5.69683266
		 76.92216492 149.88423157 -9.99989033 73.43437195 151.56149292 -7.95286274 76.076156616 153.96839905 -5.42384434
		 79.56394958 152.29115295 -7.47087193 76.16061401 158.95640564 -14.048888206 75.056472778 138.01751709 -13.15656281
		 75.68000793 142.25645447 -14.057003021 80.0039596558 141.70829773 -13.64330673 79.3804245 137.46934509 -12.74286652
		 78.15278625 137.81175232 -22.62488365 103.99188995 146.42205811 14.41164589 100.79538727 144.28955078 16.50997543
		 103.47612762 140.88467407 17.13337708 106.67263031 143.017166138 15.035045624 106.60719299 147.41719055 23.97451782
		 11.2556572 76.48366547 24.27462959 12.53031158 80.67340088 24.23693848 8.61336136 81.88050842 25.77717972
		 7.33870697 77.69078064 25.81486511 13.2747612 78.32435608 34.30734253 105.14645386 129.046463013 12.40981388
		 101.54753113 126.60890961 11.88613033 103.2013092 124.96310425 8.18146324 106.80023193 127.40066528 8.70514774
		 108.20980072 119.98934174 15.21390915 100.75717926 130.76219177 -11.6237011 103.046348572 133.68109131 -13.9491806
		 106.71083069 132.43968201 -11.90012264 104.42166138 129.52076721 -9.57464314 105.26277161 125.072921753 -18.4508934
		 -25.0071220398 76.26139069 -8.68938446 -25.46421814 80.56718445 -8.03276062 -23.44080162 81.3631134 -11.83331299
		 -22.98370361 77.057312012 -12.48993874 -32.93206024 78.67913055 -14.97191238 16.83703613 197.19169617 -16.94782257
		 15.4913969 200.80673218 -14.87400055 19.15998077 200.80293274 -12.4837265 20.50562096 197.18789673 -14.55754852
		 22.47587585 204.64543152 -21.50475311 -7.96607971 202.43876648 -19.095489502 -11.61331177 202.2096405 -16.68192291
		 -10.16049194 205.49035645 -14.17232513 -6.51325989 205.71948242 -16.58586884 -13.49491882 210.52749634 -22.57752228
		 -15.10141563 203.98130798 13.17106628 -15.30711555 201.5227356 16.789505 -11.73168564 203.51327515 18.34706116
		 -11.52598572 205.97183228 14.7286377 -19.08313179 210.57077026 20.16000366;
	setAttr ".vt[1328:1493]" -12.28077316 132.58825684 24.91721535 -11.43488693 136.77798462 25.87149048
		 -15.12763596 137.98510742 23.85198212 -15.97352219 133.79537964 22.89770508 -18.68787766 134.42895508 32.89726257
		 13.11669159 132.58825684 24.48749352 14.38499451 136.77798462 24.35493088 10.59370041 137.98510742 26.18274307
		 9.32539368 133.79537964 26.31529999 15.87772751 134.42895508 34.34187317 -15.50708103 132.93453979 -20.19456673
		 -15.80624199 137.27438354 -20.70083046 -12.61323357 137.14685059 -23.6942234 -12.31407833 132.80699158 -23.18795967
		 -20.77653122 133.81355286 -29.11535454 19.41614914 131.97192383 -19.86701393 18.90870285 136.16165161 -21.036912918
		 21.79101181 137.36877441 -17.9698143 22.2984581 133.17904663 -16.7999115 27.98594284 133.81260681 -25.46082497
		 -6.86037827 184.92803955 27.1754303 -6.72531891 189.20071411 26.22376251 -10.69184113 188.91943359 24.3908844
		 -10.8269043 184.646698 25.34255219 -12.99220276 189.067993164 34.48171616 -25.84732246 184.92840576 4.32941437
		 -25.070245743 189.21200562 4.80555153 -25.41564178 189.75759888 0.4748497 -26.19271469 185.47396851 -0.0012893677
		 -35.3345108 189.070846558 3.3528862 25.97723389 186.50621033 8.64238739 25.61197662 190.83360291 9.20817566
		 23.19732285 190.16191101 12.79846954 23.56257248 185.83450317 12.23267365 32.77728271 188.37042236 16.28464508
		 12.49117088 164.94468689 25.63839149 14.24313927 168.73495483 24.31763649 10.53767776 170.81642151 25.37068367
		 8.78569984 167.026153564 26.6914444 14.98929024 169.53614807 34.62717819 -26.29610825 160.44100952 -3.14231062
		 -26.50489235 164.63972473 -1.91462731 -24.22684288 165.79449463 -5.4711566 -24.018058777 161.59577942 -6.69883966
		 -33.68616486 164.25776672 -9.3824749 28.63511467 161.41513062 -3.32178116 27.97014618 165.73318481 -3.6253624
		 27.62564278 165.98803711 0.73219681 28.29059982 161.66996765 1.035774231 37.87157822 165.32475281 -0.57904053
		 -12.080406189 101.58937836 -23.38294029 -13.71285629 105.58128357 -22.62155914 -9.74280548 107.32337952 -23.23455811
		 -8.11035919 103.33146667 -23.99593735 -12.8501091 105.58182526 -32.95310974 5.73507309 103.72648621 25.76787567
		 5.049118042 108.035888672 26.1400528 0.72468948 107.35890198 26.026411057 1.4106369 103.049507141 25.65423203
		 3.056053162 104.73577881 35.76410294 -24.45989609 102.67712402 13.16808224 -24.3246727 106.86685181 14.43610287
		 -26.16043854 108.07396698 10.64865494 -26.29566574 103.8842392 9.3806324 -34.30846024 104.51780701 15.94980335
		 23.99373245 103.64551544 9.070716858 24.88466263 107.89776611 9.62256622 24.59602356 107.39655304 13.96277237
		 23.70508957 103.14429474 13.41092682 33.97880554 103.50727081 11.96891785 16.78791809 102.67712402 -22.13257408
		 16.13858414 106.86685181 -23.23008728 19.38077545 108.07396698 -20.54624176 20.030105591 103.8842392 -19.44873047
		 24.59349442 104.51780701 -28.7511158 -15.36144924 75.25354767 20.38199425 -14.38430309 79.51831055 20.57442474
		 -17.62903595 80.38999939 17.76661682 -18.6061821 76.12525177 17.57418442 -22.77855682 78.9938736 26.63633728
		 9.51590347 76.48366547 -26.098516464 8.57184219 80.67340088 -26.95578575 12.46121979 81.88050842 -25.34719849
		 13.40528488 77.69078064 -24.4899292 15.023036957 78.32435608 -34.72427368 101.939888 147.43011475 -11.1741581
		 103.49951172 150.96618652 -9.11698151 107.59024048 149.64015198 -9.93900299 106.03061676 146.1040802 -11.99617958
		 104.67670441 153.32647705 -18.72540665 77.71870422 133.71540833 15.76281738 73.99937439 131.91389465 14.31735992
		 76.15937805 128.23057556 13.35001945 79.87870789 130.032089233 14.79547691 75.16952515 127.65270996 23.24773788
		 75.38240051 153.29525757 10.66882038 73.61331177 150.34211731 13.37390709 77.61592102 148.94035339 14.46125984
		 79.3850174 151.89347839 11.75617218 76.78613281 157.41790771 19.63051224 -14.4488821 162.1504364 -30.98517609
		 -30.72141075 163.0045623779 -27.61616135 -14.71615601 177.66270447 -31.007566452
		 -30.091697693 181.69610596 -27.84044266 -15.82676601 176.94229126 -32.32165909 -29.68465042 180.57862854 -29.38873863
		 -15.57935619 162.9487915 -32.25870514 -30.24425125 163.71699524 -29.1294899 -26.078262329 158.97964478 -28.55850029
		 -26.058227539 160.086761475 -29.9944706 -25.089668274 179.12088013 -30.35914803 -24.99361992 180.079330444 -28.88916779
		 -19.19293785 159.016235352 -29.98601341 -19.85329819 160.1207428 -31.32146072 -19.8107872 177.47306824 -31.47420692
		 -19.1367588 178.25178528 -30.094129562 -13.051032066 167.95588684 -31.3040657 -19.24528885 167.61018372 -30.018260956
		 -26.13061333 167.57359314 -28.59074783 -32.82227707 168.029495239 -27.20585251 -32.13963699 168.24966431 -28.76175308
		 -26.66299057 167.56071472 -31.15888596 -19.77766228 167.5973053 -32.58639526 -14.32208824 168.18605042 -32.57088852
		 -14.37950897 173.61195374 -32.60353088 -19.66571808 173.92512512 -31.88428497 -26.55554962 174.62797546 -30.45954514
		 -31.83478928 174.10569763 -28.87541771 -32.48096466 174.52104187 -27.30916405 -26.1736412 174.63722229 -28.61725235
		 -19.28381348 173.93437195 -30.041992188 -13.11193466 173.97067261 -31.32160759 -25.1455574 180.075668335 -29.62208939
		 -30.24363136 181.69241333 -28.57336426 -32.63290024 174.5173645 -28.04208374 -32.97420883 168.025802612 -27.93877602
		 -30.87334442 163.00090026855 -28.34908295 -26.23019791 158.97596741 -29.29142189
		 -19.34487152 159.012557983 -30.71893501 -14.60081577 162.14675903 -31.71809769 -13.20296764 167.95220947 -32.036987305
		 -13.26386833 173.96699524 -32.054527283 -14.86809158 177.6590271 -31.74048615 -19.28869247 178.24810791 -30.82705116
		 -12.31631279 173.48048401 -31.16916275 -34.13384628 179.94171143 -25.5880127 -11.87682056 179.10058594 -31.28845406
		 -33.24002838 191.37156677 -25.83062172 -12.51374817 179.085174561 -34.36095047 -33.87695694 191.3561554 -28.90311813
		 -12.95324135 173.46508789 -34.24165726 -34.77077484 179.92630005 -28.6605072 -24.94823265 176.9315033 -28.56787109
		 -25.58516121 176.91609192 -31.6403656 -24.96440125 184.85415649 -31.80886078 -24.32747269 184.86956787 -28.73636627
		 13.73394489 175.13873291 -31.1651268 35.10979843 183.1048584 -25.82517815 12.91901016 180.71524048 -31.34209251
		 33.45243073 194.44606018 -26.18508148 13.53381157 180.70744324 -34.41911697 34.067230225 194.43826294 -29.26210976
		 14.3487463 175.13093567 -34.24215317 35.72460175 183.097061157 -28.90220833 26.12506485 179.45454407 -28.70027733
		 26.73986626 179.44673157 -31.7773056;
	setAttr ".vt[1494:1539]" 25.58881569 187.32324219 -32.027259827 24.97401237 187.33103943 -28.95023346
		 15.12715912 162.71829224 -31.078517914 31.41207695 163.77032471 -27.8273983 15.20568848 178.23266602 -31.1021595
		 30.5535984 182.45278931 -28.046291351 16.50421715 177.36506653 -32.40209579 29.99169922 181.074752808 -29.6204586
		 16.42777061 163.71664429 -32.32871628 30.73703575 164.63908386 -29.35788727 26.81149673 159.68919373 -28.73626328
		 26.69277191 161.0496521 -30.17576599 25.52250671 179.59890747 -30.54056358 25.46809006 180.7741394 -29.058137894
		 19.91607475 159.64202881 -30.11387444 20.63407898 161.0094604492 -31.43593788 20.38782501 177.92970276 -31.59785271
		 19.62530136 178.87547302 -30.22073364 13.6565733 168.50631714 -31.3870182 19.86372375 168.23597717 -30.14612198
		 26.75914574 168.28314209 -28.76851082 33.45462036 168.82040405 -27.43209457 32.53414917 169.081420898 -29.013080597
		 27.27302361 168.27661133 -31.34043503 20.37760353 168.22944641 -32.7180481 15.13841629 168.80877686 -32.63026428
		 15.13045692 174.10043335 -32.66653442 20.19383812 174.55549622 -32.014862061 27.084754944 175.34210205 -30.64002609
		 32.16740799 174.78822327 -29.12670517 33.033687592 175.30732727 -27.53264427 26.71611786 175.3467865 -28.79501534
		 19.82519722 174.56018066 -30.16985321 13.64423084 174.52139282 -31.40473557 15.36819172 178.23059082 -31.91547775
		 19.78780556 178.87341309 -31.034051895 25.63059616 180.77206421 -29.87145615 30.7161026 182.45072937 -28.8596077
		 33.1961937 175.30526733 -28.34596062 33.61712646 168.81834412 -28.24541283 31.57458115 163.76826477 -28.64071655
		 26.97399902 159.68713379 -29.54958153 20.078575134 159.63996887 -30.92719269 15.28966331 162.7162323 -31.89183426
		 13.81907749 168.5042572 -32.20033646 13.80673504 174.51933289 -32.21805191;
	setAttr -s 2912 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 1 1 3 1 2 4 1 3 5 1 6 0 1
		 7 1 1 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0 6 12 0 0 13 0 12 13 0
		 2 14 0 13 14 0 4 15 0 14 15 0 15 12 0 4 16 1 5 17 1 16 17 0 7 18 1 6 19 1 19 18 0
		 10 20 1 17 20 0 8 21 1 20 21 0 18 21 0 12 22 1 19 22 0 15 23 1 23 22 0 16 23 0 16 24 1
		 17 25 1 24 25 0 18 26 1 25 26 1 19 27 1 27 26 0 24 27 1 20 28 0 25 28 0 21 29 0 28 29 0
		 26 29 0 22 30 0 27 30 0 23 31 0 31 30 0 24 31 0 34 85 1 85 33 1 35 86 1 86 34 1 42 87 1
		 87 41 1 43 88 1 88 42 1 33 89 1 89 32 1 32 90 1 90 46 1 46 91 1 91 45 1 45 92 1 92 33 1
		 32 93 1 93 37 1 37 94 1 94 47 1 47 95 1 95 46 1 37 96 1 96 36 1 36 97 1 97 40 1 40 98 1
		 98 39 1 39 99 1 99 37 1 36 100 1 100 35 1 35 101 1 101 41 1 41 102 1 102 40 1 39 103 1
		 103 38 1 38 104 1 104 54 1 54 105 1 105 53 1 53 106 1 106 39 1 38 107 1 107 43 1
		 43 60 1 60 55 1 55 61 1 61 54 1 45 62 1 62 44 1 44 63 1 63 49 1 49 64 1 64 48 1 48 65 1
		 65 52 1 52 66 1 66 51 1 51 67 1 67 49 1 48 68 1 68 47 1 47 69 1 69 53 1 53 70 1 70 52 1
		 51 71 1 71 50 1 50 72 1 72 55 1 32 73 1 73 56 1 56 74 1 74 36 1 34 75 1 75 56 1 38 76 1
		 76 57 1 57 77 1 77 42 1 40 78 1 78 57 1 44 79 1 79 58 1 58 80 1 80 48 1 46 81 1 81 58 1
		 50 82 1 82 59 1 59 83 1 83 54 1 52 84 1 84 59 1 89 108 1 108 92 1 90 108 1 91 108 1
		 93 109 1 109 90 1 94 109 1 95 109 1 96 110 1 110 99 1;
	setAttr ".ed[166:331]" 97 110 1 98 110 1 100 111 1 111 97 1 101 111 1 102 111 1
		 103 112 1 112 106 1 104 112 1 105 112 1 107 113 1 113 104 1 60 113 1 61 113 1 64 114 1
		 114 67 1 65 114 1 66 114 1 68 115 1 115 65 1 69 115 1 70 115 1 94 116 1 116 69 1
		 99 116 1 106 116 1 96 117 1 117 74 1 93 117 1 73 117 1 89 118 1 118 73 1 85 118 1
		 75 118 1 86 119 1 119 75 1 100 119 1 74 119 1 88 120 1 120 77 1 107 120 1 76 120 1
		 103 121 1 121 76 1 98 121 1 78 121 1 102 122 1 122 78 1 87 122 1 77 122 1 64 123 1
		 123 80 1 63 123 1 79 123 1 62 124 1 124 79 1 91 124 1 81 124 1 95 125 1 125 81 1
		 68 125 1 80 125 1 61 126 1 126 83 1 72 126 1 82 126 1 71 127 1 127 82 1 66 127 1
		 84 127 1 70 128 1 128 84 1 105 128 1 83 128 1 85 398 1 33 397 1 35 401 1 86 400 1
		 42 405 1 87 404 1 41 403 1 43 407 1 88 406 1 45 395 1 92 396 1 101 402 1 60 408 1
		 55 409 1 62 394 1 44 393 1 63 416 1 49 415 1 51 413 1 67 414 1 71 412 1 50 411 1
		 72 410 1 129 130 0 130 131 0 132 133 0 133 129 0 134 135 0 135 136 0 137 138 0 138 134 0
		 139 140 0 140 131 0 132 141 0 141 136 0 137 142 0 142 143 0 139 144 0 144 145 0 145 146 0
		 146 147 0 148 149 0 149 147 0 148 150 0 150 151 0 151 152 0 152 143 0 153 129 1 154 130 1
		 155 131 1 156 140 1 157 139 1 158 144 1 159 145 1 160 146 1 161 147 1 162 149 1 163 148 1
		 164 150 1 165 151 1 166 152 1 167 143 1 168 142 1 169 137 1 170 138 1 171 134 1 172 135 1
		 173 136 1 174 141 1 175 132 1 176 133 1 153 154 1 154 155 1 155 156 1 156 157 1 157 158 1
		 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1 163 164 1 164 165 1 165 166 1 166 167 1
		 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1 172 173 1 173 174 1;
	setAttr ".ed[332:497]" 174 175 1 175 176 1 176 153 1 34 399 1 177 159 1 178 158 1
		 177 178 1 179 157 1 178 179 1 180 156 1 179 180 1 181 155 1 180 181 1 182 154 1 181 182 1
		 183 153 1 182 183 1 184 176 1 183 184 1 185 175 1 184 185 1 186 174 1 185 186 1 187 173 1
		 186 187 1 188 172 1 187 188 1 189 171 1 188 189 1 190 170 1 189 190 1 191 169 1 190 191 1
		 192 168 1 191 192 1 193 167 1 192 193 1 194 166 1 193 194 1 195 165 1 194 195 1 196 164 1
		 195 196 1 197 163 1 196 197 1 198 162 1 197 198 1 199 161 1 198 199 1 200 160 1 199 200 1
		 200 177 1 201 177 1 202 178 1 201 202 1 203 179 1 202 203 1 204 180 1 203 204 1 205 181 1
		 204 205 1 206 182 1 205 206 1 207 183 1 206 207 1 208 184 1 207 208 1 209 185 1 208 209 1
		 210 186 1 209 210 1 211 187 1 210 211 1 212 188 1 211 212 1 213 189 1 212 213 1 214 190 1
		 213 214 1 215 191 1 214 215 1 216 192 1 215 216 1 217 193 1 216 217 1 218 194 1 217 218 1
		 219 195 1 218 219 1 220 196 1 219 220 1 221 197 1 220 221 1 222 198 1 221 222 1 223 199 1
		 222 223 1 224 200 1 223 224 1 224 201 1 225 201 1 226 202 1 225 226 1 227 203 1 226 227 1
		 228 204 1 227 228 1 229 205 1 228 229 1 230 206 1 229 230 1 231 207 1 230 231 1 232 208 1
		 231 232 1 233 209 1 232 233 1 234 210 1 233 234 1 235 211 1 234 235 1 236 212 1 235 236 1
		 237 213 1 236 237 1 238 214 1 237 238 1 239 215 1 238 239 1 240 216 1 239 240 1 241 217 1
		 240 241 1 242 218 1 241 242 1 243 219 1 242 243 1 244 220 1 243 244 1 245 221 1 244 245 1
		 246 222 1 245 246 1 247 223 1 246 247 1 248 224 1 247 248 1 248 225 1 249 225 1 250 226 1
		 249 250 1 251 227 1 250 251 1 252 228 1 251 252 1 253 229 1 252 253 1 254 230 1 253 254 1
		 255 231 1 254 255 1 256 232 1 255 256 1 257 233 1 256 257 1 258 234 1;
	setAttr ".ed[498:663]" 257 258 1 259 235 1 258 259 1 260 236 1 259 260 1 261 237 1
		 260 261 1 262 238 1 261 262 1 263 239 1 262 263 1 264 240 1 263 264 1 265 241 1 264 265 1
		 266 242 1 265 266 1 267 243 1 266 267 1 268 244 1 267 268 1 269 245 1 268 269 1 270 246 1
		 269 270 1 271 247 1 270 271 1 272 248 1 271 272 1 272 249 1 273 249 1 274 250 1 273 274 1
		 275 251 1 274 275 1 276 252 1 275 276 1 277 253 1 276 277 1 278 254 1 277 278 1 279 255 1
		 278 279 1 280 256 1 279 280 1 281 257 1 280 281 1 282 258 1 281 282 1 283 259 1 282 283 1
		 284 260 1 283 284 1 285 261 1 284 285 1 286 262 1 285 286 1 287 263 1 286 287 1 288 264 1
		 287 288 1 289 265 1 288 289 1 290 266 1 289 290 1 291 267 1 290 291 1 292 268 1 291 292 1
		 293 269 1 292 293 1 294 270 1 293 294 1 295 271 1 294 295 1 296 272 1 295 296 1 296 273 1
		 297 273 1 298 274 1 297 298 1 299 275 1 298 299 1 300 276 1 299 300 1 301 277 1 300 301 1
		 302 278 1 301 302 1 303 279 1 302 303 1 304 280 1 303 304 1 305 281 1 304 305 1 306 282 1
		 305 306 1 307 283 1 306 307 1 308 284 1 307 308 1 309 285 1 308 309 1 310 286 1 309 310 1
		 311 287 1 310 311 1 312 288 1 311 312 1 313 289 1 312 313 1 314 290 1 313 314 1 315 291 1
		 314 315 1 316 292 1 315 316 1 317 293 1 316 317 1 318 294 1 317 318 1 319 295 1 318 319 1
		 320 296 1 319 320 1 320 297 1 321 297 1 322 298 1 321 322 1 323 299 1 322 323 1 324 300 1
		 323 324 1 325 301 1 324 325 1 326 302 1 325 326 1 327 303 1 326 327 1 328 304 1 327 328 1
		 329 305 1 328 329 1 330 306 1 329 330 1 331 307 1 330 331 1 332 308 1 331 332 1 333 309 1
		 332 333 1 334 310 1 333 334 1 335 311 1 334 335 1 336 312 1 335 336 1 337 313 1 336 337 1
		 338 314 1 337 338 1 339 315 1 338 339 1 340 316 1 339 340 1 341 317 1;
	setAttr ".ed[664:829]" 340 341 1 342 318 1 341 342 1 343 319 1 342 343 1 344 320 1
		 343 344 1 344 321 1 345 321 1 346 322 1 345 346 1 347 323 1 346 347 1 348 324 1 347 348 1
		 349 325 1 348 349 1 350 326 1 349 350 1 351 327 1 350 351 1 352 328 1 351 352 1 353 329 1
		 352 353 1 354 330 1 353 354 1 355 331 1 354 355 1 356 332 1 355 356 1 357 333 1 356 357 1
		 358 334 1 357 358 1 359 335 1 358 359 1 360 336 1 359 360 1 361 337 1 360 361 1 362 338 1
		 361 362 1 363 339 1 362 363 1 364 340 1 363 364 1 365 341 1 364 365 1 366 342 1 365 366 1
		 367 343 1 366 367 1 368 344 1 367 368 1 368 345 1 369 345 1 370 346 1 369 370 1 371 347 1
		 370 371 1 372 348 1 371 372 1 373 349 1 372 373 1 374 350 1 373 374 1 375 351 1 374 375 1
		 376 352 1 375 376 1 377 353 1 376 377 1 378 354 1 377 378 1 379 355 1 378 379 1 380 356 1
		 379 380 1 381 357 1 380 381 1 382 358 1 381 382 1 383 359 1 382 383 1 384 360 1 383 384 1
		 385 361 1 384 385 1 386 362 1 385 386 1 387 363 1 386 387 1 388 364 1 387 388 1 389 365 1
		 388 389 1 390 366 1 389 390 1 391 367 1 390 391 1 392 368 1 391 392 1 392 369 1 393 369 1
		 394 370 1 393 394 1 395 371 1 394 395 1 396 372 1 395 396 1 397 373 1 396 397 1 398 374 1
		 397 398 1 399 375 1 398 399 1 400 376 1 399 400 1 401 377 1 400 401 1 402 378 1 401 402 1
		 403 379 1 402 403 1 404 380 1 403 404 1 405 381 1 404 405 1 406 382 1 405 406 1 407 383 1
		 406 407 1 408 384 1 407 408 1 409 385 1 408 409 1 410 386 1 409 410 1 411 387 1 410 411 1
		 412 388 1 411 412 1 413 389 1 412 413 1 414 390 1 413 414 1 415 391 1 414 415 1 416 392 1
		 415 416 1 416 393 1 419 470 1 470 418 1 420 471 1 471 419 1 427 472 1 472 426 1 428 473 1
		 473 427 1 418 474 1 474 417 1 417 475 1 475 431 1 431 476 1 476 430 1;
	setAttr ".ed[830:995]" 430 477 1 477 418 1 417 478 1 478 422 1 422 479 1 479 432 1
		 432 480 1 480 431 1 422 481 1 481 421 1 421 482 1 482 425 1 425 483 1 483 424 1 424 484 1
		 484 422 1 421 485 1 485 420 1 420 486 1 486 426 1 426 487 1 487 425 1 424 488 1 488 423 1
		 423 489 1 489 439 1 439 490 1 490 438 1 438 491 1 491 424 1 423 492 1 492 428 1 428 445 1
		 445 440 1 440 446 1 446 439 1 430 447 1 447 429 1 429 448 1 448 434 1 434 449 1 449 433 1
		 433 450 1 450 437 1 437 451 1 451 436 1 436 452 1 452 434 1 433 453 1 453 432 1 432 454 1
		 454 438 1 438 455 1 455 437 1 436 456 1 456 435 1 435 457 1 457 440 1 417 458 1 458 441 1
		 441 459 1 459 421 1 419 460 1 460 441 1 423 461 1 461 442 1 442 462 1 462 427 1 425 463 1
		 463 442 1 429 464 1 464 443 1 443 465 1 465 433 1 431 466 1 466 443 1 435 467 1 467 444 1
		 444 468 1 468 439 1 437 469 1 469 444 1 474 493 1 493 477 1 475 493 1 476 493 1 478 494 1
		 494 475 1 479 494 1 480 494 1 481 495 1 495 484 1 482 495 1 483 495 1 485 496 1 496 482 1
		 486 496 1 487 496 1 488 497 1 497 491 1 489 497 1 490 497 1 492 498 1 498 489 1 445 498 1
		 446 498 1 449 499 1 499 452 1 450 499 1 451 499 1 453 500 1 500 450 1 454 500 1 455 500 1
		 479 501 1 501 454 1 484 501 1 491 501 1 481 502 1 502 459 1 478 502 1 458 502 1 474 503 1
		 503 458 1 470 503 1 460 503 1 471 504 1 504 460 1 485 504 1 459 504 1 473 505 1 505 462 1
		 492 505 1 461 505 1 488 506 1 506 461 1 483 506 1 463 506 1 487 507 1 507 463 1 472 507 1
		 462 507 1 449 508 1 508 465 1 448 508 1 464 508 1 447 509 1 509 464 1 476 509 1 466 509 1
		 480 510 1 510 466 1 453 510 1 465 510 1 446 511 1 511 468 1 457 511 1 467 511 1 456 512 1
		 512 467 1 451 512 1 469 512 1 455 513 1 513 469 1 490 513 1 468 513 1;
	setAttr ".ed[996:1161]" 470 766 1 514 515 0 418 765 1 515 516 0 420 769 1 471 768 1
		 517 518 0 518 514 0 427 773 1 472 772 1 519 520 0 426 771 1 520 521 0 428 775 1 473 774 1
		 522 523 0 523 519 0 430 763 1 477 764 1 524 525 0 525 516 0 486 770 1 517 526 0 526 521 0
		 445 776 1 522 527 0 440 777 1 527 528 0 447 762 1 524 529 0 429 761 1 529 530 0 448 760 1
		 530 531 0 434 759 1 531 532 0 436 757 1 452 758 1 533 534 0 534 532 0 456 756 1 533 535 0
		 435 755 1 535 536 0 457 754 1 536 537 0 537 528 0 419 767 1 538 537 1 539 536 1 538 539 1
		 540 535 1 539 540 1 541 533 1 540 541 1 542 534 1 541 542 1 543 532 1 542 543 1 544 531 1
		 543 544 1 545 530 1 544 545 1 546 529 1 545 546 1 547 524 1 546 547 1 548 525 1 547 548 1
		 549 516 1 548 549 1 550 515 1 549 550 1 551 514 1 550 551 1 552 518 1 551 552 1 553 517 1
		 552 553 1 554 526 1 553 554 1 555 521 1 554 555 1 556 520 1 555 556 1 557 519 1 556 557 1
		 558 523 1 557 558 1 559 522 1 558 559 1 560 527 1 559 560 1 561 528 1 560 561 1 561 538 1
		 562 538 1 563 539 1 562 563 1 564 540 1 563 564 1 565 541 1 564 565 1 566 542 1 565 566 1
		 567 543 1 566 567 1 568 544 1 567 568 1 569 545 1 568 569 1 570 546 1 569 570 1 571 547 1
		 570 571 1 572 548 1 571 572 1 573 549 1 572 573 1 574 550 1 573 574 1 575 551 1 574 575 1
		 576 552 1 575 576 1 577 553 1 576 577 1 578 554 1 577 578 1 579 555 1 578 579 1 580 556 1
		 579 580 1 581 557 1 580 581 1 582 558 1 581 582 1 583 559 1 582 583 1 584 560 1 583 584 1
		 585 561 1 584 585 1 585 562 1 586 562 1 587 563 1 586 587 1 588 564 1 587 588 1 589 565 1
		 588 589 1 590 566 1 589 590 1 591 567 1 590 591 1 592 568 1 591 592 1 593 569 1 592 593 1
		 594 570 1 593 594 1 595 571 1 594 595 1 596 572 1 595 596 1 597 573 1;
	setAttr ".ed[1162:1327]" 596 597 1 598 574 1 597 598 1 599 575 1 598 599 1 600 576 1
		 599 600 1 601 577 1 600 601 1 602 578 1 601 602 1 603 579 1 602 603 1 604 580 1 603 604 1
		 605 581 1 604 605 1 606 582 1 605 606 1 607 583 1 606 607 1 608 584 1 607 608 1 609 585 1
		 608 609 1 609 586 1 610 586 1 611 587 1 610 611 1 612 588 1 611 612 1 613 589 1 612 613 1
		 614 590 1 613 614 1 615 591 1 614 615 1 616 592 1 615 616 1 617 593 1 616 617 1 618 594 1
		 617 618 1 619 595 1 618 619 1 620 596 1 619 620 1 621 597 1 620 621 1 622 598 1 621 622 1
		 623 599 1 622 623 1 624 600 1 623 624 1 625 601 1 624 625 1 626 602 1 625 626 1 627 603 1
		 626 627 1 628 604 1 627 628 1 629 605 1 628 629 1 630 606 1 629 630 1 631 607 1 630 631 1
		 632 608 1 631 632 1 633 609 1 632 633 1 633 610 1 634 610 1 635 611 1 634 635 1 636 612 1
		 635 636 1 637 613 1 636 637 1 638 614 1 637 638 1 639 615 1 638 639 1 640 616 1 639 640 1
		 641 617 1 640 641 1 642 618 1 641 642 1 643 619 1 642 643 1 644 620 1 643 644 1 645 621 1
		 644 645 1 646 622 1 645 646 1 647 623 1 646 647 1 648 624 1 647 648 1 649 625 1 648 649 1
		 650 626 1 649 650 1 651 627 1 650 651 1 652 628 1 651 652 1 653 629 1 652 653 1 654 630 1
		 653 654 1 655 631 1 654 655 1 656 632 1 655 656 1 657 633 1 656 657 1 657 634 1 658 634 1
		 659 635 1 658 659 1 660 636 1 659 660 1 661 637 1 660 661 1 662 638 1 661 662 1 663 639 1
		 662 663 1 664 640 1 663 664 1 665 641 1 664 665 1 666 642 1 665 666 1 667 643 1 666 667 1
		 668 644 1 667 668 1 669 645 1 668 669 1 670 646 1 669 670 1 671 647 1 670 671 1 672 648 1
		 671 672 1 673 649 1 672 673 1 674 650 1 673 674 1 675 651 1 674 675 1 676 652 1 675 676 1
		 677 653 1 676 677 1 678 654 1 677 678 1 679 655 1 678 679 1 680 656 1;
	setAttr ".ed[1328:1493]" 679 680 1 681 657 1 680 681 1 681 658 1 682 658 1 683 659 1
		 682 683 1 684 660 1 683 684 1 685 661 1 684 685 1 686 662 1 685 686 1 687 663 1 686 687 1
		 688 664 1 687 688 1 689 665 1 688 689 1 690 666 1 689 690 1 691 667 1 690 691 1 692 668 1
		 691 692 1 693 669 1 692 693 1 694 670 1 693 694 1 695 671 1 694 695 1 696 672 1 695 696 1
		 697 673 1 696 697 1 698 674 1 697 698 1 699 675 1 698 699 1 700 676 1 699 700 1 701 677 1
		 700 701 1 702 678 1 701 702 1 703 679 1 702 703 1 704 680 1 703 704 1 705 681 1 704 705 1
		 705 682 1 706 682 1 707 683 1 706 707 1 708 684 1 707 708 1 709 685 1 708 709 1 710 686 1
		 709 710 1 711 687 1 710 711 1 712 688 1 711 712 1 713 689 1 712 713 1 714 690 1 713 714 1
		 715 691 1 714 715 1 716 692 1 715 716 1 717 693 1 716 717 1 718 694 1 717 718 1 719 695 1
		 718 719 1 720 696 1 719 720 1 721 697 1 720 721 1 722 698 1 721 722 1 723 699 1 722 723 1
		 724 700 1 723 724 1 725 701 1 724 725 1 726 702 1 725 726 1 727 703 1 726 727 1 728 704 1
		 727 728 1 729 705 1 728 729 1 729 706 1 730 706 1 731 707 1 730 731 1 732 708 1 731 732 1
		 733 709 1 732 733 1 734 710 1 733 734 1 735 711 1 734 735 1 736 712 1 735 736 1 737 713 1
		 736 737 1 738 714 1 737 738 1 739 715 1 738 739 1 740 716 1 739 740 1 741 717 1 740 741 1
		 742 718 1 741 742 1 743 719 1 742 743 1 744 720 1 743 744 1 745 721 1 744 745 1 746 722 1
		 745 746 1 747 723 1 746 747 1 748 724 1 747 748 1 749 725 1 748 749 1 750 726 1 749 750 1
		 751 727 1 750 751 1 752 728 1 751 752 1 753 729 1 752 753 1 753 730 1 754 730 1 755 731 1
		 754 755 1 756 732 1 755 756 1 757 733 1 756 757 1 758 734 1 757 758 1 759 735 1 758 759 1
		 760 736 1 759 760 1 761 737 1 760 761 1 762 738 1 761 762 1 763 739 1;
	setAttr ".ed[1494:1659]" 762 763 1 764 740 1 763 764 1 765 741 1 764 765 1 766 742 1
		 765 766 1 767 743 1 766 767 1 768 744 1 767 768 1 769 745 1 768 769 1 770 746 1 769 770 1
		 771 747 1 770 771 1 772 748 1 771 772 1 773 749 1 772 773 1 774 750 1 773 774 1 775 751 1
		 774 775 1 776 752 1 775 776 1 777 753 1 776 777 1 777 754 1 780 831 1 831 779 1 781 832 1
		 832 780 1 788 833 1 833 787 1 789 834 1 834 788 1 779 835 1 835 778 1 778 836 1 836 792 1
		 792 837 1 837 791 1 791 838 1 838 779 1 778 839 1 839 783 1 783 840 1 840 793 1 793 841 1
		 841 792 1 783 842 1 842 782 1 782 843 1 843 786 1 786 844 1 844 785 1 785 845 1 845 783 1
		 782 846 1 846 781 1 781 847 1 847 787 1 787 848 1 848 786 1 785 849 1 849 784 1 784 850 1
		 850 800 1 800 851 1 851 799 1 799 852 1 852 785 1 784 853 1 853 789 1 789 806 1 806 801 1
		 801 807 1 807 800 1 791 808 1 808 790 1 790 809 1 809 795 1 795 810 1 810 794 1 794 811 1
		 811 798 1 798 812 1 812 797 1 797 813 1 813 795 1 794 814 1 814 793 1 793 815 1 815 799 1
		 799 816 1 816 798 1 797 817 1 817 796 1 796 818 1 818 801 1 778 819 1 819 802 1 802 820 1
		 820 782 1 780 821 1 821 802 1 784 822 1 822 803 1 803 823 1 823 788 1 786 824 1 824 803 1
		 790 825 1 825 804 1 804 826 1 826 794 1 792 827 1 827 804 1 796 828 1 828 805 1 805 829 1
		 829 800 1 798 830 1 830 805 1 835 854 1 854 838 1 836 854 1 837 854 1 839 855 1 855 836 1
		 840 855 1 841 855 1 842 856 1 856 845 1 843 856 1 844 856 1 846 857 1 857 843 1 847 857 1
		 848 857 1 849 858 1 858 852 1 850 858 1 851 858 1 853 859 1 859 850 1 806 859 1 807 859 1
		 810 860 1 860 813 1 811 860 1 812 860 1 814 861 1 861 811 1 815 861 1 816 861 1 840 862 1
		 862 815 1 845 862 1 852 862 1 842 863 1 863 820 1 839 863 1 819 863 1;
	setAttr ".ed[1660:1825]" 835 864 1 864 819 1 831 864 1 821 864 1 832 865 1 865 821 1
		 846 865 1 820 865 1 834 866 1 866 823 1 853 866 1 822 866 1 849 867 1 867 822 1 844 867 1
		 824 867 1 848 868 1 868 824 1 833 868 1 823 868 1 810 869 1 869 826 1 809 869 1 825 869 1
		 808 870 1 870 825 1 837 870 1 827 870 1 841 871 1 871 827 1 814 871 1 826 871 1 807 872 1
		 872 829 1 818 872 1 828 872 1 817 873 1 873 828 1 812 873 1 830 873 1 816 874 1 874 830 1
		 851 874 1 829 874 1 831 1144 1 875 876 0 779 1143 1 876 877 0 781 1147 1 832 1146 1
		 878 879 0 879 875 0 788 1151 1 833 1150 1 880 881 0 787 1149 1 881 882 0 789 1153 1
		 834 1152 1 883 884 0 884 880 0 791 1141 1 838 1142 1 885 886 0 886 877 0 847 1148 1
		 878 887 0 887 882 0 806 1154 1 883 888 0 801 1155 1 888 889 0 808 1140 1 885 890 0
		 790 1139 1 890 891 0 809 1162 1 891 892 0 795 1161 1 892 893 0 797 1159 1 813 1160 1
		 894 895 0 895 893 0 817 1158 1 894 896 0 796 1157 1 896 897 0 818 1156 1 897 898 0
		 898 889 0 899 875 1 900 876 1 901 877 1 902 886 1 903 885 1 904 890 1 905 891 1 906 892 1
		 907 893 1 908 895 1 909 894 1 910 896 1 911 897 1 912 898 1 913 889 1 914 888 1 915 883 1
		 916 884 1 917 880 1 918 881 1 919 882 1 920 887 1 921 878 1 922 879 1 899 900 1 900 901 1
		 901 902 1 902 903 1 903 904 1 904 905 1 905 906 1 906 907 1 907 908 1 908 909 1 909 910 1
		 910 911 1 911 912 1 912 913 1 913 914 1 914 915 1 915 916 1 916 917 1 917 918 1 918 919 1
		 919 920 1 920 921 1 921 922 1 922 899 1 780 1145 1 923 905 1 924 904 1 923 924 1
		 925 903 1 924 925 1 926 902 1 925 926 1 927 901 1 926 927 1 928 900 1 927 928 1 929 899 1
		 928 929 1 930 922 1 929 930 1 931 921 1 930 931 1 932 920 1 931 932 1 933 919 1 932 933 1
		 934 918 1 933 934 1 935 917 1 934 935 1 936 916 1;
	setAttr ".ed[1826:1991]" 935 936 1 937 915 1 936 937 1 938 914 1 937 938 1 939 913 1
		 938 939 1 940 912 1 939 940 1 941 911 1 940 941 1 942 910 1 941 942 1 943 909 1 942 943 1
		 944 908 1 943 944 1 945 907 1 944 945 1 946 906 1 945 946 1 946 923 1 947 923 1 948 924 1
		 947 948 1 949 925 1 948 949 1 950 926 1 949 950 1 951 927 1 950 951 1 952 928 1 951 952 1
		 953 929 1 952 953 1 954 930 1 953 954 1 955 931 1 954 955 1 956 932 1 955 956 1 957 933 1
		 956 957 1 958 934 1 957 958 1 959 935 1 958 959 1 960 936 1 959 960 1 961 937 1 960 961 1
		 962 938 1 961 962 1 963 939 1 962 963 1 964 940 1 963 964 1 965 941 1 964 965 1 966 942 1
		 965 966 1 967 943 1 966 967 1 968 944 1 967 968 1 969 945 1 968 969 1 970 946 1 969 970 1
		 970 947 1 971 947 1 972 948 1 971 972 1 973 949 1 972 973 1 974 950 1 973 974 1 975 951 1
		 974 975 1 976 952 1 975 976 1 977 953 1 976 977 1 978 954 1 977 978 1 979 955 1 978 979 1
		 980 956 1 979 980 1 981 957 1 980 981 1 982 958 1 981 982 1 983 959 1 982 983 1 984 960 1
		 983 984 1 985 961 1 984 985 1 986 962 1 985 986 1 987 963 1 986 987 1 988 964 1 987 988 1
		 989 965 1 988 989 1 990 966 1 989 990 1 991 967 1 990 991 1 992 968 1 991 992 1 993 969 1
		 992 993 1 994 970 1 993 994 1 994 971 1 995 971 1 996 972 1 995 996 1 997 973 1 996 997 1
		 998 974 1 997 998 1 999 975 1 998 999 1 1000 976 1 999 1000 1 1001 977 1 1000 1001 1
		 1002 978 1 1001 1002 1 1003 979 1 1002 1003 1 1004 980 1 1003 1004 1 1005 981 1 1004 1005 1
		 1006 982 1 1005 1006 1 1007 983 1 1006 1007 1 1008 984 1 1007 1008 1 1009 985 1 1008 1009 1
		 1010 986 1 1009 1010 1 1011 987 1 1010 1011 1 1012 988 1 1011 1012 1 1013 989 1 1012 1013 1
		 1014 990 1 1013 1014 1 1015 991 1 1014 1015 1 1016 992 1 1015 1016 1 1017 993 1 1016 1017 1
		 1018 994 1 1017 1018 1 1018 995 1;
	setAttr ".ed[1992:2157]" 1019 995 1 1020 996 1 1019 1020 1 1021 997 1 1020 1021 1
		 1022 998 1 1021 1022 1 1023 999 1 1022 1023 1 1024 1000 1 1023 1024 1 1025 1001 1
		 1024 1025 1 1026 1002 1 1025 1026 1 1027 1003 1 1026 1027 1 1028 1004 1 1027 1028 1
		 1029 1005 1 1028 1029 1 1030 1006 1 1029 1030 1 1031 1007 1 1030 1031 1 1032 1008 1
		 1031 1032 1 1033 1009 1 1032 1033 1 1034 1010 1 1033 1034 1 1035 1011 1 1034 1035 1
		 1036 1012 1 1035 1036 1 1037 1013 1 1036 1037 1 1038 1014 1 1037 1038 1 1039 1015 1
		 1038 1039 1 1040 1016 1 1039 1040 1 1041 1017 1 1040 1041 1 1042 1018 1 1041 1042 1
		 1042 1019 1 1043 1019 1 1044 1020 1 1043 1044 1 1045 1021 1 1044 1045 1 1046 1022 1
		 1045 1046 1 1047 1023 1 1046 1047 1 1048 1024 1 1047 1048 1 1049 1025 1 1048 1049 1
		 1050 1026 1 1049 1050 1 1051 1027 1 1050 1051 1 1052 1028 1 1051 1052 1 1053 1029 1
		 1052 1053 1 1054 1030 1 1053 1054 1 1055 1031 1 1054 1055 1 1056 1032 1 1055 1056 1
		 1057 1033 1 1056 1057 1 1058 1034 1 1057 1058 1 1059 1035 1 1058 1059 1 1060 1036 1
		 1059 1060 1 1061 1037 1 1060 1061 1 1062 1038 1 1061 1062 1 1063 1039 1 1062 1063 1
		 1064 1040 1 1063 1064 1 1065 1041 1 1064 1065 1 1066 1042 1 1065 1066 1 1066 1043 1
		 1067 1043 1 1068 1044 1 1067 1068 1 1069 1045 1 1068 1069 1 1070 1046 1 1069 1070 1
		 1071 1047 1 1070 1071 1 1072 1048 1 1071 1072 1 1073 1049 1 1072 1073 1 1074 1050 1
		 1073 1074 1 1075 1051 1 1074 1075 1 1076 1052 1 1075 1076 1 1077 1053 1 1076 1077 1
		 1078 1054 1 1077 1078 1 1079 1055 1 1078 1079 1 1080 1056 1 1079 1080 1 1081 1057 1
		 1080 1081 1 1082 1058 1 1081 1082 1 1083 1059 1 1082 1083 1 1084 1060 1 1083 1084 1
		 1085 1061 1 1084 1085 1 1086 1062 1 1085 1086 1 1087 1063 1 1086 1087 1 1088 1064 1
		 1087 1088 1 1089 1065 1 1088 1089 1 1090 1066 1 1089 1090 1 1090 1067 1 1091 1067 1
		 1092 1068 1 1091 1092 1 1093 1069 1 1092 1093 1 1094 1070 1 1093 1094 1 1095 1071 1
		 1094 1095 1 1096 1072 1 1095 1096 1 1097 1073 1 1096 1097 1 1098 1074 1 1097 1098 1
		 1099 1075 1 1098 1099 1 1100 1076 1 1099 1100 1 1101 1077 1 1100 1101 1 1102 1078 1;
	setAttr ".ed[2158:2323]" 1101 1102 1 1103 1079 1 1102 1103 1 1104 1080 1 1103 1104 1
		 1105 1081 1 1104 1105 1 1106 1082 1 1105 1106 1 1107 1083 1 1106 1107 1 1108 1084 1
		 1107 1108 1 1109 1085 1 1108 1109 1 1110 1086 1 1109 1110 1 1111 1087 1 1110 1111 1
		 1112 1088 1 1111 1112 1 1113 1089 1 1112 1113 1 1114 1090 1 1113 1114 1 1114 1091 1
		 1115 1091 1 1116 1092 1 1115 1116 1 1117 1093 1 1116 1117 1 1118 1094 1 1117 1118 1
		 1119 1095 1 1118 1119 1 1120 1096 1 1119 1120 1 1121 1097 1 1120 1121 1 1122 1098 1
		 1121 1122 1 1123 1099 1 1122 1123 1 1124 1100 1 1123 1124 1 1125 1101 1 1124 1125 1
		 1126 1102 1 1125 1126 1 1127 1103 1 1126 1127 1 1128 1104 1 1127 1128 1 1129 1105 1
		 1128 1129 1 1130 1106 1 1129 1130 1 1131 1107 1 1130 1131 1 1132 1108 1 1131 1132 1
		 1133 1109 1 1132 1133 1 1134 1110 1 1133 1134 1 1135 1111 1 1134 1135 1 1136 1112 1
		 1135 1136 1 1137 1113 1 1136 1137 1 1138 1114 1 1137 1138 1 1138 1115 1 1139 1115 1
		 1140 1116 1 1139 1140 1 1141 1117 1 1140 1141 1 1142 1118 1 1141 1142 1 1143 1119 1
		 1142 1143 1 1144 1120 1 1143 1144 1 1145 1121 1 1144 1145 1 1146 1122 1 1145 1146 1
		 1147 1123 1 1146 1147 1 1148 1124 1 1147 1148 1 1149 1125 1 1148 1149 1 1150 1126 1
		 1149 1150 1 1151 1127 1 1150 1151 1 1152 1128 1 1151 1152 1 1153 1129 1 1152 1153 1
		 1154 1130 1 1153 1154 1 1155 1131 1 1154 1155 1 1156 1132 1 1155 1156 1 1157 1133 1
		 1156 1157 1 1158 1134 1 1157 1158 1 1159 1135 1 1158 1159 1 1160 1136 1 1159 1160 1
		 1161 1137 1 1160 1161 1 1162 1138 1 1161 1162 1 1162 1139 1 1163 1164 0 1164 1165 0
		 1165 1166 0 1166 1163 0 1163 1167 0 1164 1167 0 1165 1167 0 1166 1167 0 1168 1169 0
		 1169 1170 0 1170 1171 0 1171 1168 0 1168 1172 0 1169 1172 0 1170 1172 0 1171 1172 0
		 1173 1174 0 1174 1175 0 1175 1176 0 1176 1173 0 1173 1177 0 1174 1177 0 1175 1177 0
		 1176 1177 0 1178 1179 0 1179 1180 0 1180 1181 0 1181 1178 0 1178 1182 0 1179 1182 0
		 1180 1182 0 1181 1182 0 1183 1184 0 1184 1185 0 1185 1186 0 1186 1183 0 1183 1187 0
		 1184 1187 0 1185 1187 0 1186 1187 0 1188 1189 0 1189 1190 0 1190 1191 0 1191 1188 0;
	setAttr ".ed[2324:2489]" 1188 1192 0 1189 1192 0 1190 1192 0 1191 1192 0 1193 1194 0
		 1194 1195 0 1195 1196 0 1196 1193 0 1193 1197 0 1194 1197 0 1195 1197 0 1196 1197 0
		 1198 1199 0 1199 1200 0 1200 1201 0 1201 1198 0 1198 1202 0 1199 1202 0 1200 1202 0
		 1201 1202 0 1203 1204 0 1204 1205 0 1205 1206 0 1206 1203 0 1203 1207 0 1204 1207 0
		 1205 1207 0 1206 1207 0 1208 1209 0 1209 1210 0 1210 1211 0 1211 1208 0 1208 1212 0
		 1209 1212 0 1210 1212 0 1211 1212 0 1213 1214 0 1214 1215 0 1215 1216 0 1216 1213 0
		 1213 1217 0 1214 1217 0 1215 1217 0 1216 1217 0 1218 1219 0 1219 1220 0 1220 1221 0
		 1221 1218 0 1218 1222 0 1219 1222 0 1220 1222 0 1221 1222 0 1223 1224 0 1224 1225 0
		 1225 1226 0 1226 1223 0 1223 1227 0 1224 1227 0 1225 1227 0 1226 1227 0 1228 1229 0
		 1229 1230 0 1230 1231 0 1231 1228 0 1228 1232 0 1229 1232 0 1230 1232 0 1231 1232 0
		 1233 1234 0 1234 1235 0 1235 1236 0 1236 1233 0 1233 1237 0 1234 1237 0 1235 1237 0
		 1236 1237 0 1238 1239 0 1239 1240 0 1240 1241 0 1241 1238 0 1238 1242 0 1239 1242 0
		 1240 1242 0 1241 1242 0 1243 1244 0 1244 1245 0 1245 1246 0 1246 1243 0 1243 1247 0
		 1244 1247 0 1245 1247 0 1246 1247 0 1248 1249 0 1249 1250 0 1250 1251 0 1251 1248 0
		 1248 1252 0 1249 1252 0 1250 1252 0 1251 1252 0 1253 1254 0 1254 1255 0 1255 1256 0
		 1256 1253 0 1253 1257 0 1254 1257 0 1255 1257 0 1256 1257 0 1258 1259 0 1259 1260 0
		 1260 1261 0 1261 1258 0 1258 1262 0 1259 1262 0 1260 1262 0 1261 1262 0 1263 1264 0
		 1264 1265 0 1265 1266 0 1266 1263 0 1263 1267 0 1264 1267 0 1265 1267 0 1266 1267 0
		 1268 1269 0 1269 1270 0 1270 1271 0 1271 1268 0 1268 1272 0 1269 1272 0 1270 1272 0
		 1271 1272 0 1273 1274 0 1274 1275 0 1275 1276 0 1276 1273 0 1273 1277 0 1274 1277 0
		 1275 1277 0 1276 1277 0 1278 1279 0 1279 1280 0 1280 1281 0 1281 1278 0 1278 1282 0
		 1279 1282 0 1280 1282 0 1281 1282 0 1283 1284 0 1284 1285 0 1285 1286 0 1286 1283 0
		 1283 1287 0 1284 1287 0 1285 1287 0 1286 1287 0 1288 1289 0 1289 1290 0 1290 1291 0
		 1291 1288 0 1288 1292 0 1289 1292 0 1290 1292 0 1291 1292 0 1293 1294 0 1294 1295 0;
	setAttr ".ed[2490:2655]" 1295 1296 0 1296 1293 0 1293 1297 0 1294 1297 0 1295 1297 0
		 1296 1297 0 1298 1299 0 1299 1300 0 1300 1301 0 1301 1298 0 1298 1302 0 1299 1302 0
		 1300 1302 0 1301 1302 0 1303 1304 0 1304 1305 0 1305 1306 0 1306 1303 0 1303 1307 0
		 1304 1307 0 1305 1307 0 1306 1307 0 1308 1309 0 1309 1310 0 1310 1311 0 1311 1308 0
		 1308 1312 0 1309 1312 0 1310 1312 0 1311 1312 0 1313 1314 0 1314 1315 0 1315 1316 0
		 1316 1313 0 1313 1317 0 1314 1317 0 1315 1317 0 1316 1317 0 1318 1319 0 1319 1320 0
		 1320 1321 0 1321 1318 0 1318 1322 0 1319 1322 0 1320 1322 0 1321 1322 0 1323 1324 0
		 1324 1325 0 1325 1326 0 1326 1323 0 1323 1327 0 1324 1327 0 1325 1327 0 1326 1327 0
		 1328 1329 0 1329 1330 0 1330 1331 0 1331 1328 0 1328 1332 0 1329 1332 0 1330 1332 0
		 1331 1332 0 1333 1334 0 1334 1335 0 1335 1336 0 1336 1333 0 1333 1337 0 1334 1337 0
		 1335 1337 0 1336 1337 0 1338 1339 0 1339 1340 0 1340 1341 0 1341 1338 0 1338 1342 0
		 1339 1342 0 1340 1342 0 1341 1342 0 1343 1344 0 1344 1345 0 1345 1346 0 1346 1343 0
		 1343 1347 0 1344 1347 0 1345 1347 0 1346 1347 0 1348 1349 0 1349 1350 0 1350 1351 0
		 1351 1348 0 1348 1352 0 1349 1352 0 1350 1352 0 1351 1352 0 1353 1354 0 1354 1355 0
		 1355 1356 0 1356 1353 0 1353 1357 0 1354 1357 0 1355 1357 0 1356 1357 0 1358 1359 0
		 1359 1360 0 1360 1361 0 1361 1358 0 1358 1362 0 1359 1362 0 1360 1362 0 1361 1362 0
		 1363 1364 0 1364 1365 0 1365 1366 0 1366 1363 0 1363 1367 0 1364 1367 0 1365 1367 0
		 1366 1367 0 1368 1369 0 1369 1370 0 1370 1371 0 1371 1368 0 1368 1372 0 1369 1372 0
		 1370 1372 0 1371 1372 0 1373 1374 0 1374 1375 0 1375 1376 0 1376 1373 0 1373 1377 0
		 1374 1377 0 1375 1377 0 1376 1377 0 1378 1379 0 1379 1380 0 1380 1381 0 1381 1378 0
		 1378 1382 0 1379 1382 0 1380 1382 0 1381 1382 0 1383 1384 0 1384 1385 0 1385 1386 0
		 1386 1383 0 1383 1387 0 1384 1387 0 1385 1387 0 1386 1387 0 1388 1389 0 1389 1390 0
		 1390 1391 0 1391 1388 0 1388 1392 0 1389 1392 0 1390 1392 0 1391 1392 0 1393 1394 0
		 1394 1395 0 1395 1396 0 1396 1393 0 1393 1397 0 1394 1397 0 1395 1397 0 1396 1397 0;
	setAttr ".ed[2656:2821]" 1398 1399 0 1399 1400 0 1400 1401 0 1401 1398 0 1398 1402 0
		 1399 1402 0 1400 1402 0 1401 1402 0 1403 1404 0 1404 1405 0 1405 1406 0 1406 1403 0
		 1403 1407 0 1404 1407 0 1405 1407 0 1406 1407 0 1408 1409 0 1409 1410 0 1410 1411 0
		 1411 1408 0 1408 1412 0 1409 1412 0 1410 1412 0 1411 1412 0 1413 1414 0 1414 1415 0
		 1415 1416 0 1416 1413 0 1413 1417 0 1414 1417 0 1415 1417 0 1416 1417 0 1418 1419 0
		 1419 1420 0 1420 1421 0 1421 1418 0 1418 1422 0 1419 1422 0 1420 1422 0 1421 1422 0
		 1423 1424 0 1424 1425 0 1425 1426 0 1426 1423 0 1423 1427 0 1424 1427 0 1425 1427 0
		 1426 1427 0 1456 1457 1 1457 1446 1 1446 1447 1 1447 1456 0 1438 1460 1 1439 1431 0
		 1431 1461 0 1438 1433 0 1448 1449 1 1449 1454 1 1454 1455 1 1455 1448 0 1436 1465 1
		 1437 1435 0 1435 1464 0 1436 1429 0 1447 1463 1 1455 1462 1 1451 1468 1 1444 1459 0
		 1459 1469 1 1452 1451 0 1440 1466 1 1441 1437 0 1440 1436 0 1449 1450 1 1450 1453 1
		 1453 1454 1 1442 1471 1 1443 1439 0 1442 1438 0 1457 1458 1 1458 1445 1 1445 1446 1
		 1434 1441 0 1428 1440 0 1434 1467 0 1452 1453 1 1450 1451 1 1430 1443 0 1432 1442 0
		 1430 1470 0 1444 1445 1 1458 1459 1 1445 1440 1 1428 1444 0 1446 1436 1 1429 1447 0
		 1448 1435 0 1437 1449 1 1441 1450 1 1451 1434 0 1453 1442 1 1432 1452 0 1454 1438 1
		 1433 1455 0 1456 1431 0 1439 1457 1 1443 1458 1 1459 1430 0 1460 1439 1 1461 1433 0
		 1462 1456 1 1463 1448 1 1464 1429 0 1465 1437 1 1466 1441 1 1467 1428 0 1468 1444 1
		 1469 1452 1 1470 1432 0 1471 1443 1 1460 1461 1 1461 1462 1 1462 1463 1 1463 1464 1
		 1464 1465 1 1465 1466 1 1466 1467 1 1467 1468 1 1468 1469 1 1469 1470 1 1470 1471 1
		 1471 1460 1 1483 1480 1 1480 1473 0 1473 1475 0 1483 1475 0 1482 1483 1 1475 1477 0
		 1482 1477 0 1481 1482 1 1477 1479 0 1481 1479 0 1480 1481 1 1479 1473 0 1478 1472 0
		 1472 1474 0 1474 1476 0 1476 1478 0 1478 1481 0 1472 1480 0 1476 1482 0 1474 1483 0
		 1484 1492 0 1486 1495 0 1488 1494 0 1490 1493 0 1484 1486 0 1485 1487 0 1486 1488 0
		 1487 1489 0 1488 1490 0 1489 1491 0 1490 1484 0 1491 1485 0 1492 1485 0 1493 1491 0;
	setAttr ".ed[2822:2911]" 1494 1489 0 1495 1487 0 1492 1493 1 1493 1494 1 1494 1495 1
		 1495 1492 1 1496 1508 0 1498 1511 0 1500 1510 0 1502 1509 0 1496 1512 0 1497 1515 0
		 1498 1528 0 1499 1531 0 1500 1520 0 1501 1523 0 1502 1537 0 1503 1534 0 1504 1497 0
		 1505 1503 0 1506 1501 0 1507 1499 0 1504 1535 1 1505 1517 1 1506 1530 1 1507 1525 1
		 1508 1504 0 1509 1505 0 1510 1506 0 1511 1507 0 1508 1536 1 1509 1518 1 1510 1529 1
		 1511 1526 1 1512 1527 0 1513 1508 1 1514 1504 1 1515 1524 0 1516 1503 0 1517 1522 1
		 1518 1521 1 1519 1502 0 1512 1513 1 1513 1514 1 1514 1515 1 1515 1533 1 1516 1517 1
		 1517 1518 1 1518 1519 1 1519 1538 1 1520 1519 0 1521 1510 1 1522 1506 1 1523 1516 0
		 1524 1499 0 1525 1514 1 1526 1513 1 1527 1498 0 1520 1521 1 1521 1522 1 1522 1523 1
		 1523 1532 1 1524 1525 1 1525 1526 1 1526 1527 1 1527 1539 1 1528 1500 0 1529 1511 1
		 1530 1507 1 1531 1501 0 1532 1524 1 1533 1516 1 1534 1497 0 1535 1505 1 1536 1509 1
		 1537 1496 0 1538 1512 1 1539 1520 1 1528 1529 1 1529 1530 1 1530 1531 1 1531 1532 1
		 1532 1533 1 1533 1534 1 1534 1535 1 1535 1536 1 1536 1537 1 1537 1538 1 1538 1539 1
		 1539 1528 1;
	setAttr -s 1491 -ch 5752 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 17 16 24 25
		f 4 1 7 -3 -7
		mu 0 4 10 5 4 11
		f 4 44 46 -49 -50
		mu 0 4 28 29 30 31
		f 4 3 9 -1 -9
		mu 0 4 14 41 16 17
		f 4 -13 -15 -17 -18
		mu 0 4 0 1 2 3
		f 4 20 22 24 25
		mu 0 4 6 7 8 9
		f 4 -10 10 12 -12
		mu 0 4 16 41 20 21
		f 4 -47 51 53 -55
		mu 0 4 30 29 32 33
		f 4 -8 15 16 -14
		mu 0 4 4 5 3 2
		f 4 -6 11 17 -16
		mu 0 4 24 16 21 26
		f 4 8 19 -21 -19
		mu 0 4 14 17 22 23
		f 4 4 21 -23 -20
		mu 0 4 17 25 27 22
		f 4 6 23 -25 -22
		mu 0 4 10 11 9 8
		f 4 49 56 -59 -60
		mu 0 4 28 31 34 35
		f 4 2 27 -29 -27
		mu 0 4 11 4 12 13
		f 4 -4 30 31 -30
		mu 0 4 15 40 18 19
		f 4 13 32 -34 -28
		mu 0 4 4 2 36 12
		f 4 14 34 -36 -33
		mu 0 4 2 1 38 36
		f 4 -11 29 36 -35
		mu 0 4 1 15 19 38
		f 4 18 37 -39 -31
		mu 0 4 40 6 39 18
		f 4 -26 39 40 -38
		mu 0 4 6 9 37 39
		f 4 -24 26 41 -40
		mu 0 4 9 11 13 37
		f 4 28 43 -45 -43
		mu 0 4 13 12 29 28
		f 4 -32 47 48 -46
		mu 0 4 19 18 31 30
		f 4 33 50 -52 -44
		mu 0 4 12 36 32 29
		f 4 35 52 -54 -51
		mu 0 4 36 38 33 32
		f 4 -37 45 54 -53
		mu 0 4 38 19 30 33
		f 4 38 55 -57 -48
		mu 0 4 18 39 34 31
		f 4 -41 57 58 -56
		mu 0 4 39 37 35 34
		f 4 -42 42 59 -58
		mu 0 4 37 13 28 35
		f 4 68 156 157 75
		mu 0 4 42 43 44 45
		f 4 69 70 158 -157
		mu 0 4 43 46 47 44
		f 4 -159 71 72 159
		mu 0 4 44 47 48 49
		f 4 -158 -160 73 74
		mu 0 4 45 44 49 50
		f 4 76 160 161 -71
		mu 0 4 46 51 52 47
		f 4 77 78 162 -161
		mu 0 4 51 53 54 52
		f 4 -163 79 80 163
		mu 0 4 52 54 55 56
		f 4 -162 -164 81 -72
		mu 0 4 47 52 56 48
		f 4 82 164 165 89
		mu 0 4 53 57 58 59
		f 4 83 84 166 -165
		mu 0 4 57 60 61 58
		f 4 -167 85 86 167
		mu 0 4 58 61 62 63
		f 4 -166 -168 87 88
		mu 0 4 59 58 63 64
		f 4 90 168 169 -85
		mu 0 4 60 65 66 61
		f 4 91 92 170 -169
		mu 0 4 65 67 68 66
		f 4 -171 93 94 171
		mu 0 4 66 68 69 70
		f 4 -170 -172 95 -86
		mu 0 4 61 66 70 62
		f 4 96 172 173 103
		mu 0 4 64 71 72 73
		f 4 97 98 174 -173
		mu 0 4 71 74 75 72
		f 4 -175 99 100 175
		mu 0 4 72 75 76 77
		f 4 -174 -176 101 102
		mu 0 4 73 72 77 78
		f 4 104 176 177 -99
		mu 0 4 74 79 80 75
		f 4 105 106 178 -177
		mu 0 4 79 81 82 80
		f 4 -179 107 108 179
		mu 0 4 80 82 83 84
		f 4 -178 -180 109 -100
		mu 0 4 75 80 84 76
		f 4 114 180 181 121
		mu 0 4 85 86 87 88
		f 4 115 116 182 -181
		mu 0 4 86 89 90 87
		f 4 -183 117 118 183
		mu 0 4 87 90 91 92
		f 4 -182 -184 119 120
		mu 0 4 88 87 92 93
		f 4 122 184 185 -117
		mu 0 4 89 94 95 90
		f 4 123 124 186 -185
		mu 0 4 94 55 96 95
		f 4 -187 125 126 187
		mu 0 4 95 96 78 97
		f 4 -186 -188 127 -118
		mu 0 4 90 95 97 91
		f 4 -80 188 189 -125
		mu 0 4 55 54 98 96
		f 4 -79 -90 190 -189
		mu 0 4 54 53 59 98
		f 4 -191 -89 -104 191
		mu 0 4 98 59 64 73
		f 4 -190 -192 -103 -126
		mu 0 4 96 98 73 78
		f 4 -84 192 193 135
		mu 0 4 60 57 99 100
		f 4 -83 -78 194 -193
		mu 0 4 57 53 51 99
		f 4 -195 -77 132 195
		mu 0 4 99 51 46 101
		f 4 -194 -196 133 134
		mu 0 4 100 99 101 102
		f 4 -70 196 197 -133
		mu 0 4 46 43 103 101
		f 4 -69 -62 198 -197
		mu 0 4 43 42 104 103
		f 4 -199 -61 136 199
		mu 0 4 103 104 105 106
		f 4 -198 -200 137 -134
		mu 0 4 101 103 106 102
		f 4 -64 200 201 -137
		mu 0 4 105 107 108 106
		f 4 -63 -92 202 -201
		mu 0 4 107 67 65 108
		f 4 -203 -91 -136 203
		mu 0 4 108 65 60 100
		f 4 -202 -204 -135 -138
		mu 0 4 106 108 100 102
		f 4 -68 204 205 141
		mu 0 4 109 110 111 112
		f 4 -67 -106 206 -205
		mu 0 4 110 81 79 111
		f 4 -207 -105 138 207
		mu 0 4 111 79 74 113
		f 4 -206 -208 139 140
		mu 0 4 112 111 113 114
		f 4 -98 208 209 -139
		mu 0 4 74 71 115 113
		f 4 -97 -88 210 -209
		mu 0 4 71 64 63 115
		f 4 -211 -87 142 211
		mu 0 4 115 63 62 116
		f 4 -210 -212 143 -140
		mu 0 4 113 115 116 114
		f 4 -96 212 213 -143
		mu 0 4 62 70 117 116
		f 4 -95 -66 214 -213
		mu 0 4 70 69 118 117
		f 4 -215 -65 -142 215
		mu 0 4 117 118 109 112
		f 4 -214 -216 -141 -144
		mu 0 4 116 117 112 114
		f 4 -116 216 217 147
		mu 0 4 89 86 119 120
		f 4 -115 -114 218 -217
		mu 0 4 86 85 121 119
		f 4 -219 -113 144 219
		mu 0 4 119 121 122 123
		f 4 -218 -220 145 146
		mu 0 4 120 119 123 124
		f 4 -112 220 221 -145
		mu 0 4 122 125 126 123
		f 4 -111 -74 222 -221
		mu 0 4 125 50 49 126
		f 4 -223 -73 148 223
		mu 0 4 126 49 48 127
		f 4 -222 -224 149 -146
		mu 0 4 123 126 127 124
		f 4 -82 224 225 -149
		mu 0 4 48 56 128 127
		f 4 -81 -124 226 -225
		mu 0 4 56 55 94 128
		f 4 -227 -123 -148 227
		mu 0 4 128 94 89 120
		f 4 -226 -228 -147 -150
		mu 0 4 127 128 120 124
		f 4 -110 228 229 153
		mu 0 4 76 84 129 130
		f 4 -109 -132 230 -229
		mu 0 4 84 83 131 129
		f 4 -231 -131 150 231
		mu 0 4 129 131 132 133
		f 4 -230 -232 151 152
		mu 0 4 130 129 133 134
		f 4 -130 232 233 -151
		mu 0 4 132 135 136 133
		f 4 -129 -120 234 -233
		mu 0 4 135 93 92 136
		f 4 -235 -119 154 235
		mu 0 4 136 92 91 137
		f 4 -234 -236 155 -152
		mu 0 4 133 136 137 134
		f 4 -128 236 237 -155
		mu 0 4 91 97 138 137
		f 4 -127 -102 238 -237
		mu 0 4 97 78 77 138
		f 4 -239 -101 -154 239
		mu 0 4 138 77 76 130
		f 4 -238 -240 -153 -156
		mu 0 4 137 138 130 134
		f 4 774 -251 -75 249
		mu 0 4 139 140 141 142
		f 4 776 -242 -76 250
		mu 0 4 143 144 145 141
		f 4 786 -252 -93 242
		mu 0 4 146 147 148 149
		f 4 788 -247 -94 251
		mu 0 4 150 151 152 148
		f 4 798 -253 -107 247
		mu 0 4 153 154 155 156
		f 4 800 -254 -108 252
		mu 0 4 157 158 159 155
		f 4 810 -260 -121 258
		mu 0 4 160 161 162 163
		f 4 812 -258 -122 259
		mu 0 4 164 165 166 162
		f 4 311 288 -264 -288
		mu 0 4 167 168 169 170
		f 4 312 289 -265 -289
		mu 0 4 171 172 173 174
		f 4 333 310 -266 -310
		mu 0 4 175 176 177 178
		f 4 334 287 -267 -311
		mu 0 4 179 180 181 182
		f 4 329 306 -268 -306
		mu 0 4 183 184 185 186
		f 4 330 307 -269 -307
		mu 0 4 187 188 189 190
		f 4 327 304 -270 -304
		mu 0 4 191 192 193 194
		f 4 328 305 -271 -305
		mu 0 4 195 196 197 198
		f 4 314 291 271 -291
		mu 0 4 199 200 201 202
		f 4 313 290 272 -290
		mu 0 4 203 204 205 206
		f 4 332 309 273 -309
		mu 0 4 207 208 209 210
		f 4 331 308 274 -308
		mu 0 4 211 212 213 214
		f 4 326 303 275 -303
		mu 0 4 215 216 217 218
		f 4 325 302 276 -302
		mu 0 4 219 220 221 222
		f 4 315 292 -278 -292
		mu 0 4 223 224 225 226
		f 4 316 293 -279 -293
		mu 0 4 227 228 229 230
		f 4 317 294 -280 -294
		mu 0 4 231 232 233 234
		f 4 318 295 -281 -295
		mu 0 4 235 236 237 238
		f 4 320 297 281 -297
		mu 0 4 239 240 241 242
		f 4 319 296 282 -296
		mu 0 4 243 244 245 246
		f 4 321 298 -284 -298
		mu 0 4 247 248 249 250
		f 4 322 299 -285 -299
		mu 0 4 251 252 253 254
		f 4 323 300 -286 -300
		mu 0 4 255 256 257 258
		f 4 324 301 -287 -301
		mu 0 4 259 260 261 262
		f 4 780 -336 60 240
		mu 0 4 263 264 265 266
		f 4 778 -241 61 241
		mu 0 4 267 268 266 145
		f 4 772 -250 110 254
		mu 0 4 269 270 142 271
		f 4 770 -255 111 255
		mu 0 4 272 273 271 274
		f 4 815 -256 112 256
		mu 0 4 275 276 274 277
		f 4 814 -257 113 257
		mu 0 4 278 279 277 280
		f 4 808 -259 128 260
		mu 0 4 281 282 163 283
		f 4 806 -261 129 261
		mu 0 4 284 285 283 286
		f 4 804 -262 130 262
		mu 0 4 287 288 286 289
		f 4 802 -263 131 253
		mu 0 4 290 291 289 159
		f 4 796 -248 66 248
		mu 0 4 292 293 156 294
		f 4 794 -249 67 244
		mu 0 4 295 296 294 297
		f 4 792 -245 64 245
		mu 0 4 298 299 297 300
		f 4 790 -246 65 246
		mu 0 4 301 302 300 152
		f 4 784 -243 62 243
		mu 0 4 303 304 149 305
		f 4 335 782 -244 63
		mu 0 4 265 306 307 305
		f 4 -317 -338 -339 336
		mu 0 4 228 227 308 309
		f 4 -316 -340 -341 337
		mu 0 4 224 223 310 311
		f 4 -315 -342 -343 339
		mu 0 4 200 199 312 313
		f 4 -314 -344 -345 341
		mu 0 4 204 203 314 315
		f 4 -313 -346 -347 343
		mu 0 4 172 171 316 317
		f 4 -312 -348 -349 345
		mu 0 4 168 167 318 319
		f 4 -351 347 -335 -350
		mu 0 4 320 321 180 179
		f 4 -334 -352 -353 349
		mu 0 4 176 175 322 323
		f 4 -333 -354 -355 351
		mu 0 4 208 207 324 325
		f 4 -332 -356 -357 353
		mu 0 4 212 211 326 327
		f 4 -331 -358 -359 355
		mu 0 4 188 187 328 329
		f 4 -330 -360 -361 357
		mu 0 4 184 183 330 331
		f 4 -329 -362 -363 359
		mu 0 4 196 195 332 333
		f 4 -328 -364 -365 361
		mu 0 4 192 191 334 335
		f 4 -327 -366 -367 363
		mu 0 4 216 215 336 337
		f 4 -326 -368 -369 365
		mu 0 4 220 219 338 339
		f 4 -325 -370 -371 367
		mu 0 4 260 259 340 341
		f 4 -324 -372 -373 369
		mu 0 4 256 255 342 343
		f 4 -323 -374 -375 371
		mu 0 4 252 251 344 345
		f 4 -322 -376 -377 373
		mu 0 4 248 247 346 347
		f 4 -321 -378 -379 375
		mu 0 4 240 239 348 349
		f 4 -320 -380 -381 377
		mu 0 4 244 243 350 351
		f 4 -319 -382 -383 379
		mu 0 4 236 235 352 353
		f 4 -318 -337 -384 381
		mu 0 4 232 231 354 355
		f 4 338 -386 -387 384
		mu 0 4 309 308 356 357
		f 4 340 -388 -389 385
		mu 0 4 311 310 358 359
		f 4 342 -390 -391 387
		mu 0 4 313 312 360 361
		f 4 344 -392 -393 389
		mu 0 4 315 314 362 363
		f 4 346 -394 -395 391
		mu 0 4 317 316 364 365
		f 4 348 -396 -397 393
		mu 0 4 319 318 366 367
		f 4 -399 395 350 -398
		mu 0 4 368 369 321 320
		f 4 352 -400 -401 397
		mu 0 4 323 322 370 371
		f 4 354 -402 -403 399
		mu 0 4 325 324 372 373
		f 4 356 -404 -405 401
		mu 0 4 327 326 374 375
		f 4 358 -406 -407 403
		mu 0 4 329 328 376 377
		f 4 360 -408 -409 405
		mu 0 4 331 330 378 379
		f 4 362 -410 -411 407
		mu 0 4 333 332 380 381
		f 4 364 -412 -413 409
		mu 0 4 335 334 382 383
		f 4 366 -414 -415 411
		mu 0 4 337 336 384 385
		f 4 368 -416 -417 413
		mu 0 4 339 338 386 387
		f 4 370 -418 -419 415
		mu 0 4 341 340 388 389
		f 4 372 -420 -421 417
		mu 0 4 343 342 390 391
		f 4 374 -422 -423 419
		mu 0 4 345 344 392 393
		f 4 376 -424 -425 421
		mu 0 4 347 346 394 395
		f 4 378 -426 -427 423
		mu 0 4 349 348 396 397
		f 4 380 -428 -429 425
		mu 0 4 351 350 398 399
		f 4 382 -430 -431 427
		mu 0 4 353 352 400 401
		f 4 383 -385 -432 429
		mu 0 4 355 354 402 403
		f 4 386 -434 -435 432
		mu 0 4 357 356 404 405
		f 4 388 -436 -437 433
		mu 0 4 359 358 406 407
		f 4 390 -438 -439 435
		mu 0 4 361 360 408 409
		f 4 392 -440 -441 437
		mu 0 4 363 362 410 411
		f 4 394 -442 -443 439
		mu 0 4 365 364 412 413
		f 4 396 -444 -445 441
		mu 0 4 367 366 414 415
		f 4 -447 443 398 -446
		mu 0 4 416 417 369 368
		f 4 400 -448 -449 445
		mu 0 4 371 370 418 419
		f 4 402 -450 -451 447
		mu 0 4 373 372 420 421
		f 4 404 -452 -453 449
		mu 0 4 375 374 422 423
		f 4 406 -454 -455 451
		mu 0 4 377 376 424 425
		f 4 408 -456 -457 453
		mu 0 4 379 378 426 427
		f 4 410 -458 -459 455
		mu 0 4 381 380 428 429
		f 4 412 -460 -461 457
		mu 0 4 383 382 430 431
		f 4 414 -462 -463 459
		mu 0 4 385 384 432 433
		f 4 416 -464 -465 461
		mu 0 4 387 386 434 435
		f 4 418 -466 -467 463
		mu 0 4 389 388 436 437
		f 4 420 -468 -469 465
		mu 0 4 391 390 438 439
		f 4 422 -470 -471 467
		mu 0 4 393 392 440 441
		f 4 424 -472 -473 469
		mu 0 4 395 394 442 443
		f 4 426 -474 -475 471
		mu 0 4 397 396 444 445
		f 4 428 -476 -477 473
		mu 0 4 399 398 446 447
		f 4 430 -478 -479 475
		mu 0 4 401 400 448 449
		f 4 431 -433 -480 477
		mu 0 4 403 402 450 451
		f 4 434 -482 -483 480
		mu 0 4 405 404 452 453
		f 4 436 -484 -485 481
		mu 0 4 407 406 454 455
		f 4 438 -486 -487 483
		mu 0 4 409 408 456 457
		f 4 440 -488 -489 485
		mu 0 4 411 410 458 459
		f 4 442 -490 -491 487
		mu 0 4 413 412 460 461
		f 4 444 -492 -493 489
		mu 0 4 415 414 462 463
		f 4 -495 491 446 -494
		mu 0 4 464 465 417 416
		f 4 448 -496 -497 493
		mu 0 4 419 418 466 467
		f 4 450 -498 -499 495
		mu 0 4 421 420 468 469
		f 4 452 -500 -501 497
		mu 0 4 423 422 470 471
		f 4 454 -502 -503 499
		mu 0 4 425 424 472 473
		f 4 456 -504 -505 501
		mu 0 4 427 426 474 475
		f 4 458 -506 -507 503
		mu 0 4 429 428 476 477
		f 4 460 -508 -509 505
		mu 0 4 431 430 478 479
		f 4 462 -510 -511 507
		mu 0 4 433 432 480 481
		f 4 464 -512 -513 509
		mu 0 4 435 434 482 483
		f 4 466 -514 -515 511
		mu 0 4 437 436 484 485
		f 4 468 -516 -517 513
		mu 0 4 439 438 486 487
		f 4 470 -518 -519 515
		mu 0 4 441 440 488 489
		f 4 472 -520 -521 517
		mu 0 4 443 442 490 491
		f 4 474 -522 -523 519
		mu 0 4 445 444 492 493
		f 4 476 -524 -525 521
		mu 0 4 447 446 494 495
		f 4 478 -526 -527 523
		mu 0 4 449 448 496 497
		f 4 479 -481 -528 525
		mu 0 4 451 450 498 499
		f 4 482 -530 -531 528
		mu 0 4 453 452 500 501
		f 4 484 -532 -533 529
		mu 0 4 455 454 502 503
		f 4 486 -534 -535 531
		mu 0 4 457 456 504 505
		f 4 488 -536 -537 533
		mu 0 4 459 458 506 507
		f 4 490 -538 -539 535
		mu 0 4 461 460 508 509
		f 4 492 -540 -541 537
		mu 0 4 463 462 510 511
		f 4 -543 539 494 -542
		mu 0 4 512 513 465 464
		f 4 496 -544 -545 541
		mu 0 4 467 466 514 515
		f 4 498 -546 -547 543
		mu 0 4 469 468 516 517
		f 4 500 -548 -549 545
		mu 0 4 471 470 518 519
		f 4 502 -550 -551 547
		mu 0 4 473 472 520 521
		f 4 504 -552 -553 549
		mu 0 4 475 474 522 523
		f 4 506 -554 -555 551
		mu 0 4 477 476 524 525
		f 4 508 -556 -557 553
		mu 0 4 479 478 526 527
		f 4 510 -558 -559 555
		mu 0 4 481 480 528 529
		f 4 512 -560 -561 557
		mu 0 4 483 482 530 531
		f 4 514 -562 -563 559
		mu 0 4 485 484 532 533
		f 4 516 -564 -565 561
		mu 0 4 487 486 534 535
		f 4 518 -566 -567 563
		mu 0 4 489 488 536 537
		f 4 520 -568 -569 565
		mu 0 4 491 490 538 539
		f 4 522 -570 -571 567
		mu 0 4 493 492 540 541
		f 4 524 -572 -573 569
		mu 0 4 495 494 542 543
		f 4 526 -574 -575 571
		mu 0 4 497 496 544 545
		f 4 527 -529 -576 573
		mu 0 4 499 498 546 547
		f 4 530 -578 -579 576
		mu 0 4 501 500 548 549
		f 4 532 -580 -581 577
		mu 0 4 503 502 550 551
		f 4 534 -582 -583 579
		mu 0 4 505 504 552 553
		f 4 536 -584 -585 581
		mu 0 4 507 506 554 555
		f 4 538 -586 -587 583
		mu 0 4 509 508 556 557
		f 4 540 -588 -589 585
		mu 0 4 511 510 558 559
		f 4 -591 587 542 -590
		mu 0 4 560 561 513 512
		f 4 544 -592 -593 589
		mu 0 4 515 514 562 563
		f 4 546 -594 -595 591
		mu 0 4 517 516 564 565
		f 4 548 -596 -597 593
		mu 0 4 519 518 566 567
		f 4 550 -598 -599 595
		mu 0 4 521 520 568 569
		f 4 552 -600 -601 597
		mu 0 4 523 522 570 571
		f 4 554 -602 -603 599
		mu 0 4 525 524 572 573
		f 4 556 -604 -605 601
		mu 0 4 527 526 574 575
		f 4 558 -606 -607 603
		mu 0 4 529 528 576 577
		f 4 560 -608 -609 605
		mu 0 4 531 530 578 579
		f 4 562 -610 -611 607
		mu 0 4 533 532 580 581
		f 4 564 -612 -613 609
		mu 0 4 535 534 582 583
		f 4 566 -614 -615 611
		mu 0 4 537 536 584 585
		f 4 568 -616 -617 613
		mu 0 4 539 538 586 587
		f 4 570 -618 -619 615
		mu 0 4 541 540 588 589
		f 4 572 -620 -621 617
		mu 0 4 543 542 590 591
		f 4 574 -622 -623 619
		mu 0 4 545 544 592 593
		f 4 575 -577 -624 621
		mu 0 4 547 546 594 595
		f 4 578 -626 -627 624
		mu 0 4 549 548 596 597
		f 4 580 -628 -629 625
		mu 0 4 551 550 598 599
		f 4 582 -630 -631 627
		mu 0 4 553 552 600 601
		f 4 584 -632 -633 629
		mu 0 4 555 554 602 603
		f 4 586 -634 -635 631
		mu 0 4 557 556 604 605
		f 4 588 -636 -637 633
		mu 0 4 559 558 606 607
		f 4 -639 635 590 -638
		mu 0 4 608 609 561 560
		f 4 592 -640 -641 637
		mu 0 4 563 562 610 611
		f 4 594 -642 -643 639
		mu 0 4 565 564 612 613
		f 4 596 -644 -645 641
		mu 0 4 567 566 614 615
		f 4 598 -646 -647 643
		mu 0 4 569 568 616 617
		f 4 600 -648 -649 645
		mu 0 4 571 570 618 619
		f 4 602 -650 -651 647
		mu 0 4 573 572 620 621
		f 4 604 -652 -653 649
		mu 0 4 575 574 622 623
		f 4 606 -654 -655 651
		mu 0 4 577 576 624 625
		f 4 608 -656 -657 653
		mu 0 4 579 578 626 627
		f 4 610 -658 -659 655
		mu 0 4 581 580 628 629
		f 4 612 -660 -661 657
		mu 0 4 583 582 630 631
		f 4 614 -662 -663 659
		mu 0 4 585 584 632 633
		f 4 616 -664 -665 661
		mu 0 4 587 586 634 635
		f 4 618 -666 -667 663
		mu 0 4 589 588 636 637
		f 4 620 -668 -669 665
		mu 0 4 591 590 638 639
		f 4 622 -670 -671 667
		mu 0 4 593 592 640 641
		f 4 623 -625 -672 669
		mu 0 4 595 594 642 643
		f 4 626 -674 -675 672
		mu 0 4 597 596 644 645
		f 4 628 -676 -677 673
		mu 0 4 599 598 646 647
		f 4 630 -678 -679 675
		mu 0 4 601 600 648 649
		f 4 632 -680 -681 677
		mu 0 4 603 602 650 651
		f 4 634 -682 -683 679
		mu 0 4 605 604 652 653
		f 4 636 -684 -685 681
		mu 0 4 607 606 654 655
		f 4 -687 683 638 -686
		mu 0 4 656 657 609 608
		f 4 640 -688 -689 685
		mu 0 4 611 610 658 659
		f 4 642 -690 -691 687
		mu 0 4 613 612 660 661
		f 4 644 -692 -693 689
		mu 0 4 615 614 662 663
		f 4 646 -694 -695 691
		mu 0 4 617 616 664 665
		f 4 648 -696 -697 693
		mu 0 4 619 618 666 667
		f 4 650 -698 -699 695
		mu 0 4 621 620 668 669
		f 4 652 -700 -701 697
		mu 0 4 623 622 670 671
		f 4 654 -702 -703 699
		mu 0 4 625 624 672 673
		f 4 656 -704 -705 701
		mu 0 4 627 626 674 675
		f 4 658 -706 -707 703
		mu 0 4 629 628 676 677
		f 4 660 -708 -709 705
		mu 0 4 631 630 678 679
		f 4 662 -710 -711 707
		mu 0 4 633 632 680 681
		f 4 664 -712 -713 709
		mu 0 4 635 634 682 683
		f 4 666 -714 -715 711
		mu 0 4 637 636 684 685
		f 4 668 -716 -717 713
		mu 0 4 639 638 686 687
		f 4 670 -718 -719 715
		mu 0 4 641 640 688 689
		f 4 671 -673 -720 717
		mu 0 4 643 642 690 691
		f 4 674 -722 -723 720
		mu 0 4 645 644 692 693
		f 4 676 -724 -725 721
		mu 0 4 647 646 694 695
		f 4 678 -726 -727 723
		mu 0 4 649 648 696 697
		f 4 680 -728 -729 725
		mu 0 4 651 650 698 699
		f 4 682 -730 -731 727
		mu 0 4 653 652 700 701
		f 4 684 -732 -733 729
		mu 0 4 655 654 702 703
		f 4 -735 731 686 -734
		mu 0 4 704 705 657 656
		f 4 688 -736 -737 733
		mu 0 4 659 658 706 707
		f 4 690 -738 -739 735
		mu 0 4 661 660 708 709
		f 4 692 -740 -741 737
		mu 0 4 663 662 710 711
		f 4 694 -742 -743 739
		mu 0 4 665 664 712 713
		f 4 696 -744 -745 741
		mu 0 4 667 666 714 715
		f 4 698 -746 -747 743
		mu 0 4 669 668 716 717
		f 4 700 -748 -749 745
		mu 0 4 671 670 718 719
		f 4 702 -750 -751 747
		mu 0 4 673 672 720 721
		f 4 704 -752 -753 749
		mu 0 4 675 674 722 723
		f 4 706 -754 -755 751
		mu 0 4 677 676 724 725
		f 4 708 -756 -757 753
		mu 0 4 679 678 726 727
		f 4 710 -758 -759 755
		mu 0 4 681 680 728 729
		f 4 712 -760 -761 757
		mu 0 4 683 682 730 731
		f 4 714 -762 -763 759
		mu 0 4 685 684 732 733
		f 4 716 -764 -765 761
		mu 0 4 687 686 734 735
		f 4 718 -766 -767 763
		mu 0 4 689 688 736 737
		f 4 719 -721 -768 765
		mu 0 4 691 690 738 739
		f 4 722 -770 -771 768
		mu 0 4 693 692 273 272
		f 4 724 -772 -773 769
		mu 0 4 695 694 270 269
		f 4 726 -774 -775 771
		mu 0 4 697 696 140 139
		f 4 728 -776 -777 773
		mu 0 4 699 698 144 143
		f 4 730 -778 -779 775
		mu 0 4 701 700 268 267
		f 4 732 -780 -781 777
		mu 0 4 703 702 264 263
		f 4 -783 779 734 -782
		mu 0 4 307 306 705 704
		f 4 736 -784 -785 781
		mu 0 4 707 706 304 303
		f 4 738 -786 -787 783
		mu 0 4 709 708 147 146
		f 4 740 -788 -789 785
		mu 0 4 711 710 151 150
		f 4 742 -790 -791 787
		mu 0 4 713 712 302 301
		f 4 744 -792 -793 789
		mu 0 4 715 714 299 298
		f 4 746 -794 -795 791
		mu 0 4 717 716 296 295
		f 4 748 -796 -797 793
		mu 0 4 719 718 293 292
		f 4 750 -798 -799 795
		mu 0 4 721 720 154 153
		f 4 752 -800 -801 797
		mu 0 4 723 722 158 157
		f 4 754 -802 -803 799
		mu 0 4 725 724 291 290
		f 4 756 -804 -805 801
		mu 0 4 727 726 288 287
		f 4 758 -806 -807 803
		mu 0 4 729 728 285 284
		f 4 760 -808 -809 805
		mu 0 4 731 730 282 281
		f 4 762 -810 -811 807
		mu 0 4 733 732 161 160
		f 4 764 -812 -813 809
		mu 0 4 735 734 165 164
		f 4 766 -814 -815 811
		mu 0 4 737 736 279 278
		f 4 767 -769 -816 813
		mu 0 4 739 738 276 275
		f 4 824 912 913 831
		mu 0 4 740 741 742 743
		f 4 825 826 914 -913
		mu 0 4 741 744 745 742
		f 4 -915 827 828 915
		mu 0 4 742 745 746 747
		f 4 -914 -916 829 830
		mu 0 4 743 742 747 748
		f 4 832 916 917 -827
		mu 0 4 744 749 750 745
		f 4 833 834 918 -917
		mu 0 4 749 751 752 750
		f 4 -919 835 836 919
		mu 0 4 750 752 753 754
		f 4 -918 -920 837 -828
		mu 0 4 745 750 754 746
		f 4 838 920 921 845
		mu 0 4 751 755 756 757
		f 4 839 840 922 -921
		mu 0 4 755 758 759 756
		f 4 -923 841 842 923
		mu 0 4 756 759 760 761
		f 4 -922 -924 843 844
		mu 0 4 757 756 761 762
		f 4 846 924 925 -841
		mu 0 4 758 763 764 759
		f 4 847 848 926 -925
		mu 0 4 763 765 766 764
		f 4 -927 849 850 927
		mu 0 4 764 766 767 768
		f 4 -926 -928 851 -842
		mu 0 4 759 764 768 760
		f 4 852 928 929 859
		mu 0 4 762 769 770 771
		f 4 853 854 930 -929
		mu 0 4 769 772 773 770
		f 4 -931 855 856 931
		mu 0 4 770 773 774 775
		f 4 -930 -932 857 858
		mu 0 4 771 770 775 776
		f 4 860 932 933 -855
		mu 0 4 772 777 778 773
		f 4 861 862 934 -933
		mu 0 4 777 779 780 778
		f 4 -935 863 864 935
		mu 0 4 778 780 781 782
		f 4 -934 -936 865 -856
		mu 0 4 773 778 782 774
		f 4 870 936 937 877
		mu 0 4 783 784 785 786
		f 4 871 872 938 -937
		mu 0 4 784 787 788 785
		f 4 -939 873 874 939
		mu 0 4 785 788 789 790
		f 4 -938 -940 875 876
		mu 0 4 786 785 790 791
		f 4 878 940 941 -873
		mu 0 4 787 792 793 788
		f 4 879 880 942 -941
		mu 0 4 792 753 794 793
		f 4 -943 881 882 943
		mu 0 4 793 794 776 795
		f 4 -942 -944 883 -874
		mu 0 4 788 793 795 789
		f 4 -836 944 945 -881
		mu 0 4 753 752 796 794
		f 4 -835 -846 946 -945
		mu 0 4 752 751 757 796
		f 4 -947 -845 -860 947
		mu 0 4 796 757 762 771
		f 4 -946 -948 -859 -882
		mu 0 4 794 796 771 776
		f 4 -840 948 949 891
		mu 0 4 758 755 797 798
		f 4 -839 -834 950 -949
		mu 0 4 755 751 749 797
		f 4 -951 -833 888 951
		mu 0 4 797 749 744 799
		f 4 -950 -952 889 890
		mu 0 4 798 797 799 800
		f 4 -826 952 953 -889
		mu 0 4 744 741 801 799
		f 4 -825 -818 954 -953
		mu 0 4 741 740 802 801
		f 4 -955 -817 892 955
		mu 0 4 801 802 803 804
		f 4 -954 -956 893 -890
		mu 0 4 799 801 804 800
		f 4 -820 956 957 -893
		mu 0 4 803 805 806 804
		f 4 -819 -848 958 -957
		mu 0 4 805 765 763 806
		f 4 -959 -847 -892 959
		mu 0 4 806 763 758 798
		f 4 -958 -960 -891 -894
		mu 0 4 804 806 798 800
		f 4 -824 960 961 897
		mu 0 4 807 808 809 810
		f 4 -823 -862 962 -961
		mu 0 4 808 779 777 809
		f 4 -963 -861 894 963
		mu 0 4 809 777 772 811
		f 4 -962 -964 895 896
		mu 0 4 810 809 811 812
		f 4 -854 964 965 -895
		mu 0 4 772 769 813 811
		f 4 -853 -844 966 -965
		mu 0 4 769 762 761 813
		f 4 -967 -843 898 967
		mu 0 4 813 761 760 814
		f 4 -966 -968 899 -896
		mu 0 4 811 813 814 812
		f 4 -852 968 969 -899
		mu 0 4 760 768 815 814
		f 4 -851 -822 970 -969
		mu 0 4 768 767 816 815
		f 4 -971 -821 -898 971
		mu 0 4 815 816 807 810
		f 4 -970 -972 -897 -900
		mu 0 4 814 815 810 812
		f 4 -872 972 973 903
		mu 0 4 787 784 817 818
		f 4 -871 -870 974 -973
		mu 0 4 784 783 819 817
		f 4 -975 -869 900 975
		mu 0 4 817 819 820 821
		f 4 -974 -976 901 902
		mu 0 4 818 817 821 822
		f 4 -868 976 977 -901
		mu 0 4 820 823 824 821
		f 4 -867 -830 978 -977
		mu 0 4 823 748 747 824
		f 4 -979 -829 904 979
		mu 0 4 824 747 746 825
		f 4 -978 -980 905 -902
		mu 0 4 821 824 825 822
		f 4 -838 980 981 -905
		mu 0 4 746 754 826 825
		f 4 -837 -880 982 -981
		mu 0 4 754 753 792 826
		f 4 -983 -879 -904 983
		mu 0 4 826 792 787 818
		f 4 -982 -984 -903 -906
		mu 0 4 825 826 818 822
		f 4 -866 984 985 909
		mu 0 4 774 782 827 828
		f 4 -865 -888 986 -985
		mu 0 4 782 781 829 827
		f 4 -987 -887 906 987
		mu 0 4 827 829 830 831
		f 4 -986 -988 907 908
		mu 0 4 828 827 831 832
		f 4 -886 988 989 -907
		mu 0 4 830 833 834 831
		f 4 -885 -876 990 -989
		mu 0 4 833 791 790 834
		f 4 -991 -875 910 991
		mu 0 4 834 790 789 835
		f 4 -990 -992 911 -908
		mu 0 4 831 834 835 832
		f 4 -884 992 993 -911
		mu 0 4 789 795 836 835
		f 4 -883 -858 994 -993
		mu 0 4 795 776 775 836
		f 4 -995 -857 -910 995
		mu 0 4 836 775 774 828
		f 4 -994 -996 -909 -912
		mu 0 4 835 836 828 832
		f 4 1502 -1044 816 996
		mu 0 4 837 838 839 840
		f 4 1500 -997 817 998
		mu 0 4 841 837 840 842
		f 4 1506 -1001 818 1001
		mu 0 4 843 844 845 846
		f 4 1504 -1002 819 1043
		mu 0 4 838 843 846 839
		f 4 1514 -1005 820 1005
		mu 0 4 847 848 849 850
		f 4 1512 -1006 821 1007
		mu 0 4 851 847 850 852
		f 4 1518 -1010 822 1010
		mu 0 4 853 854 855 856
		f 4 1516 -1011 823 1004
		mu 0 4 848 853 856 849
		f 4 1496 -1015 -831 1013
		mu 0 4 857 858 859 860
		f 4 1498 -999 -832 1014
		mu 0 4 858 841 842 859
		f 4 1508 -1018 -849 1000
		mu 0 4 844 861 862 845
		f 4 1510 -1008 -850 1017
		mu 0 4 861 851 852 862
		f 4 1520 -1021 -863 1009
		mu 0 4 854 863 864 855
		f 4 1522 -1023 -864 1020
		mu 0 4 863 865 866 864;
	setAttr ".fc[500:999]"
		f 4 1494 -1014 866 1024
		mu 0 4 867 857 860 868
		f 4 1492 -1025 867 1026
		mu 0 4 869 867 868 870
		f 4 1490 -1027 868 1028
		mu 0 4 871 869 870 872
		f 4 1488 -1029 869 1030
		mu 0 4 873 871 872 874
		f 4 1484 -1034 -877 1032
		mu 0 4 875 876 877 878
		f 4 1486 -1031 -878 1033
		mu 0 4 876 879 880 877
		f 4 1482 -1033 884 1036
		mu 0 4 881 875 878 882
		f 4 1480 -1037 885 1038
		mu 0 4 883 881 882 884
		f 4 1478 -1039 886 1040
		mu 0 4 885 883 884 886
		f 4 1523 -1041 887 1022
		mu 0 4 865 885 886 866
		f 4 -1042 -1046 -1047 1044
		mu 0 4 887 888 889 890
		f 4 -1040 -1048 -1049 1045
		mu 0 4 888 891 892 889
		f 4 -1038 -1050 -1051 1047
		mu 0 4 891 893 894 892
		f 4 1034 -1052 -1053 1049
		mu 0 4 893 895 896 894
		f 4 1035 -1054 -1055 1051
		mu 0 4 895 897 898 896
		f 4 -1032 -1056 -1057 1053
		mu 0 4 899 900 901 902
		f 4 -1030 -1058 -1059 1055
		mu 0 4 900 903 904 901
		f 4 -1028 -1060 -1061 1057
		mu 0 4 903 905 906 904
		f 4 -1026 -1062 -1063 1059
		mu 0 4 905 907 908 906
		f 4 1015 -1064 -1065 1061
		mu 0 4 907 909 910 908
		f 4 1016 -1066 -1067 1063
		mu 0 4 909 911 912 910
		f 4 -1000 -1068 -1069 1065
		mu 0 4 911 913 914 912
		f 4 -998 -1070 -1071 1067
		mu 0 4 913 915 916 914
		f 4 -1004 -1072 -1073 1069
		mu 0 4 915 917 918 916
		f 4 -1003 -1074 -1075 1071
		mu 0 4 917 919 920 918
		f 4 1018 -1076 -1077 1073
		mu 0 4 919 921 922 920
		f 4 1019 -1078 -1079 1075
		mu 0 4 921 923 924 922
		f 4 -1009 -1080 -1081 1077
		mu 0 4 923 925 926 924
		f 4 -1007 -1082 -1083 1079
		mu 0 4 925 927 928 926
		f 4 -1013 -1084 -1085 1081
		mu 0 4 927 929 930 928
		f 4 -1012 -1086 -1087 1083
		mu 0 4 929 931 932 930
		f 4 1021 -1088 -1089 1085
		mu 0 4 931 933 934 932
		f 4 1023 -1090 -1091 1087
		mu 0 4 933 935 936 934
		f 4 -1043 -1045 -1092 1089
		mu 0 4 935 887 890 936
		f 4 1046 -1094 -1095 1092
		mu 0 4 890 889 937 938
		f 4 1048 -1096 -1097 1093
		mu 0 4 889 892 939 937
		f 4 1050 -1098 -1099 1095
		mu 0 4 892 894 940 939
		f 4 1052 -1100 -1101 1097
		mu 0 4 894 896 941 940
		f 4 1054 -1102 -1103 1099
		mu 0 4 896 898 942 941
		f 4 1056 -1104 -1105 1101
		mu 0 4 902 901 943 944
		f 4 1058 -1106 -1107 1103
		mu 0 4 901 904 945 943
		f 4 1060 -1108 -1109 1105
		mu 0 4 904 906 946 945
		f 4 1062 -1110 -1111 1107
		mu 0 4 906 908 947 946
		f 4 1064 -1112 -1113 1109
		mu 0 4 908 910 948 947
		f 4 1066 -1114 -1115 1111
		mu 0 4 910 912 949 948
		f 4 1068 -1116 -1117 1113
		mu 0 4 912 914 950 949
		f 4 1070 -1118 -1119 1115
		mu 0 4 914 916 951 950
		f 4 1072 -1120 -1121 1117
		mu 0 4 916 918 952 951
		f 4 1074 -1122 -1123 1119
		mu 0 4 918 920 953 952
		f 4 1076 -1124 -1125 1121
		mu 0 4 920 922 954 953
		f 4 1078 -1126 -1127 1123
		mu 0 4 922 924 955 954
		f 4 1080 -1128 -1129 1125
		mu 0 4 924 926 956 955
		f 4 1082 -1130 -1131 1127
		mu 0 4 926 928 957 956
		f 4 1084 -1132 -1133 1129
		mu 0 4 928 930 958 957
		f 4 1086 -1134 -1135 1131
		mu 0 4 930 932 959 958
		f 4 1088 -1136 -1137 1133
		mu 0 4 932 934 960 959
		f 4 1090 -1138 -1139 1135
		mu 0 4 934 936 961 960
		f 4 1091 -1093 -1140 1137
		mu 0 4 936 890 938 961
		f 4 1094 -1142 -1143 1140
		mu 0 4 938 937 962 963
		f 4 1096 -1144 -1145 1141
		mu 0 4 937 939 964 962
		f 4 1098 -1146 -1147 1143
		mu 0 4 939 940 965 964
		f 4 1100 -1148 -1149 1145
		mu 0 4 940 941 966 965
		f 4 1102 -1150 -1151 1147
		mu 0 4 941 942 967 966
		f 4 1104 -1152 -1153 1149
		mu 0 4 944 943 968 969
		f 4 1106 -1154 -1155 1151
		mu 0 4 943 945 970 968
		f 4 1108 -1156 -1157 1153
		mu 0 4 945 946 971 970
		f 4 1110 -1158 -1159 1155
		mu 0 4 946 947 972 971
		f 4 1112 -1160 -1161 1157
		mu 0 4 947 948 973 972
		f 4 1114 -1162 -1163 1159
		mu 0 4 948 949 974 973
		f 4 1116 -1164 -1165 1161
		mu 0 4 949 950 975 974
		f 4 1118 -1166 -1167 1163
		mu 0 4 950 951 976 975
		f 4 1120 -1168 -1169 1165
		mu 0 4 951 952 977 976
		f 4 1122 -1170 -1171 1167
		mu 0 4 952 953 978 977
		f 4 1124 -1172 -1173 1169
		mu 0 4 953 954 979 978
		f 4 1126 -1174 -1175 1171
		mu 0 4 954 955 980 979
		f 4 1128 -1176 -1177 1173
		mu 0 4 955 956 981 980
		f 4 1130 -1178 -1179 1175
		mu 0 4 956 957 982 981
		f 4 1132 -1180 -1181 1177
		mu 0 4 957 958 983 982
		f 4 1134 -1182 -1183 1179
		mu 0 4 958 959 984 983
		f 4 1136 -1184 -1185 1181
		mu 0 4 959 960 985 984
		f 4 1138 -1186 -1187 1183
		mu 0 4 960 961 986 985
		f 4 1139 -1141 -1188 1185
		mu 0 4 961 938 963 986
		f 4 1142 -1190 -1191 1188
		mu 0 4 963 962 987 988
		f 4 1144 -1192 -1193 1189
		mu 0 4 962 964 989 987
		f 4 1146 -1194 -1195 1191
		mu 0 4 964 965 990 989
		f 4 1148 -1196 -1197 1193
		mu 0 4 965 966 991 990
		f 4 1150 -1198 -1199 1195
		mu 0 4 966 967 992 991
		f 4 1152 -1200 -1201 1197
		mu 0 4 969 968 993 994
		f 4 1154 -1202 -1203 1199
		mu 0 4 968 970 995 993
		f 4 1156 -1204 -1205 1201
		mu 0 4 970 971 996 995
		f 4 1158 -1206 -1207 1203
		mu 0 4 971 972 997 996
		f 4 1160 -1208 -1209 1205
		mu 0 4 972 973 998 997
		f 4 1162 -1210 -1211 1207
		mu 0 4 973 974 999 998
		f 4 1164 -1212 -1213 1209
		mu 0 4 974 975 1000 999
		f 4 1166 -1214 -1215 1211
		mu 0 4 975 976 1001 1000
		f 4 1168 -1216 -1217 1213
		mu 0 4 976 977 1002 1001
		f 4 1170 -1218 -1219 1215
		mu 0 4 977 978 1003 1002
		f 4 1172 -1220 -1221 1217
		mu 0 4 978 979 1004 1003
		f 4 1174 -1222 -1223 1219
		mu 0 4 979 980 1005 1004
		f 4 1176 -1224 -1225 1221
		mu 0 4 980 981 1006 1005
		f 4 1178 -1226 -1227 1223
		mu 0 4 981 982 1007 1006
		f 4 1180 -1228 -1229 1225
		mu 0 4 982 983 1008 1007
		f 4 1182 -1230 -1231 1227
		mu 0 4 983 984 1009 1008
		f 4 1184 -1232 -1233 1229
		mu 0 4 984 985 1010 1009
		f 4 1186 -1234 -1235 1231
		mu 0 4 985 986 1011 1010
		f 4 1187 -1189 -1236 1233
		mu 0 4 986 963 988 1011
		f 4 1190 -1238 -1239 1236
		mu 0 4 988 987 1012 1013
		f 4 1192 -1240 -1241 1237
		mu 0 4 987 989 1014 1012
		f 4 1194 -1242 -1243 1239
		mu 0 4 989 990 1015 1014
		f 4 1196 -1244 -1245 1241
		mu 0 4 990 991 1016 1015
		f 4 1198 -1246 -1247 1243
		mu 0 4 991 992 1017 1016
		f 4 1200 -1248 -1249 1245
		mu 0 4 994 993 1018 1019
		f 4 1202 -1250 -1251 1247
		mu 0 4 993 995 1020 1018
		f 4 1204 -1252 -1253 1249
		mu 0 4 995 996 1021 1020
		f 4 1206 -1254 -1255 1251
		mu 0 4 996 997 1022 1021
		f 4 1208 -1256 -1257 1253
		mu 0 4 997 998 1023 1022
		f 4 1210 -1258 -1259 1255
		mu 0 4 998 999 1024 1023
		f 4 1212 -1260 -1261 1257
		mu 0 4 999 1000 1025 1024
		f 4 1214 -1262 -1263 1259
		mu 0 4 1000 1001 1026 1025
		f 4 1216 -1264 -1265 1261
		mu 0 4 1001 1002 1027 1026
		f 4 1218 -1266 -1267 1263
		mu 0 4 1002 1003 1028 1027
		f 4 1220 -1268 -1269 1265
		mu 0 4 1003 1004 1029 1028
		f 4 1222 -1270 -1271 1267
		mu 0 4 1004 1005 1030 1029
		f 4 1224 -1272 -1273 1269
		mu 0 4 1005 1006 1031 1030
		f 4 1226 -1274 -1275 1271
		mu 0 4 1006 1007 1032 1031
		f 4 1228 -1276 -1277 1273
		mu 0 4 1007 1008 1033 1032
		f 4 1230 -1278 -1279 1275
		mu 0 4 1008 1009 1034 1033
		f 4 1232 -1280 -1281 1277
		mu 0 4 1009 1010 1035 1034
		f 4 1234 -1282 -1283 1279
		mu 0 4 1010 1011 1036 1035
		f 4 1235 -1237 -1284 1281
		mu 0 4 1011 988 1013 1036
		f 4 1238 -1286 -1287 1284
		mu 0 4 1013 1012 1037 1038
		f 4 1240 -1288 -1289 1285
		mu 0 4 1012 1014 1039 1037
		f 4 1242 -1290 -1291 1287
		mu 0 4 1014 1015 1040 1039
		f 4 1244 -1292 -1293 1289
		mu 0 4 1015 1016 1041 1040
		f 4 1246 -1294 -1295 1291
		mu 0 4 1016 1017 1042 1041
		f 4 1248 -1296 -1297 1293
		mu 0 4 1019 1018 1043 1044
		f 4 1250 -1298 -1299 1295
		mu 0 4 1018 1020 1045 1043
		f 4 1252 -1300 -1301 1297
		mu 0 4 1020 1021 1046 1045
		f 4 1254 -1302 -1303 1299
		mu 0 4 1021 1022 1047 1046
		f 4 1256 -1304 -1305 1301
		mu 0 4 1022 1023 1048 1047
		f 4 1258 -1306 -1307 1303
		mu 0 4 1023 1024 1049 1048
		f 4 1260 -1308 -1309 1305
		mu 0 4 1024 1025 1050 1049
		f 4 1262 -1310 -1311 1307
		mu 0 4 1025 1026 1051 1050
		f 4 1264 -1312 -1313 1309
		mu 0 4 1026 1027 1052 1051
		f 4 1266 -1314 -1315 1311
		mu 0 4 1027 1028 1053 1052
		f 4 1268 -1316 -1317 1313
		mu 0 4 1028 1029 1054 1053
		f 4 1270 -1318 -1319 1315
		mu 0 4 1029 1030 1055 1054
		f 4 1272 -1320 -1321 1317
		mu 0 4 1030 1031 1056 1055
		f 4 1274 -1322 -1323 1319
		mu 0 4 1031 1032 1057 1056
		f 4 1276 -1324 -1325 1321
		mu 0 4 1032 1033 1058 1057
		f 4 1278 -1326 -1327 1323
		mu 0 4 1033 1034 1059 1058
		f 4 1280 -1328 -1329 1325
		mu 0 4 1034 1035 1060 1059
		f 4 1282 -1330 -1331 1327
		mu 0 4 1035 1036 1061 1060
		f 4 1283 -1285 -1332 1329
		mu 0 4 1036 1013 1038 1061
		f 4 1286 -1334 -1335 1332
		mu 0 4 1038 1037 1062 1063
		f 4 1288 -1336 -1337 1333
		mu 0 4 1037 1039 1064 1062
		f 4 1290 -1338 -1339 1335
		mu 0 4 1039 1040 1065 1064
		f 4 1292 -1340 -1341 1337
		mu 0 4 1040 1041 1066 1065
		f 4 1294 -1342 -1343 1339
		mu 0 4 1041 1042 1067 1066
		f 4 1296 -1344 -1345 1341
		mu 0 4 1044 1043 1068 1069
		f 4 1298 -1346 -1347 1343
		mu 0 4 1043 1045 1070 1068
		f 4 1300 -1348 -1349 1345
		mu 0 4 1045 1046 1071 1070
		f 4 1302 -1350 -1351 1347
		mu 0 4 1046 1047 1072 1071
		f 4 1304 -1352 -1353 1349
		mu 0 4 1047 1048 1073 1072
		f 4 1306 -1354 -1355 1351
		mu 0 4 1048 1049 1074 1073
		f 4 1308 -1356 -1357 1353
		mu 0 4 1049 1050 1075 1074
		f 4 1310 -1358 -1359 1355
		mu 0 4 1050 1051 1076 1075
		f 4 1312 -1360 -1361 1357
		mu 0 4 1051 1052 1077 1076
		f 4 1314 -1362 -1363 1359
		mu 0 4 1052 1053 1078 1077
		f 4 1316 -1364 -1365 1361
		mu 0 4 1053 1054 1079 1078
		f 4 1318 -1366 -1367 1363
		mu 0 4 1054 1055 1080 1079
		f 4 1320 -1368 -1369 1365
		mu 0 4 1055 1056 1081 1080
		f 4 1322 -1370 -1371 1367
		mu 0 4 1056 1057 1082 1081
		f 4 1324 -1372 -1373 1369
		mu 0 4 1057 1058 1083 1082
		f 4 1326 -1374 -1375 1371
		mu 0 4 1058 1059 1084 1083
		f 4 1328 -1376 -1377 1373
		mu 0 4 1059 1060 1085 1084
		f 4 1330 -1378 -1379 1375
		mu 0 4 1060 1061 1086 1085
		f 4 1331 -1333 -1380 1377
		mu 0 4 1061 1038 1063 1086
		f 4 1334 -1382 -1383 1380
		mu 0 4 1063 1062 1087 1088
		f 4 1336 -1384 -1385 1381
		mu 0 4 1062 1064 1089 1087
		f 4 1338 -1386 -1387 1383
		mu 0 4 1064 1065 1090 1089
		f 4 1340 -1388 -1389 1385
		mu 0 4 1065 1066 1091 1090
		f 4 1342 -1390 -1391 1387
		mu 0 4 1066 1067 1092 1091
		f 4 1344 -1392 -1393 1389
		mu 0 4 1069 1068 1093 1094
		f 4 1346 -1394 -1395 1391
		mu 0 4 1068 1070 1095 1093
		f 4 1348 -1396 -1397 1393
		mu 0 4 1070 1071 1096 1095
		f 4 1350 -1398 -1399 1395
		mu 0 4 1071 1072 1097 1096
		f 4 1352 -1400 -1401 1397
		mu 0 4 1072 1073 1098 1097
		f 4 1354 -1402 -1403 1399
		mu 0 4 1073 1074 1099 1098
		f 4 1356 -1404 -1405 1401
		mu 0 4 1074 1075 1100 1099
		f 4 1358 -1406 -1407 1403
		mu 0 4 1075 1076 1101 1100
		f 4 1360 -1408 -1409 1405
		mu 0 4 1076 1077 1102 1101
		f 4 1362 -1410 -1411 1407
		mu 0 4 1077 1078 1103 1102
		f 4 1364 -1412 -1413 1409
		mu 0 4 1078 1079 1104 1103
		f 4 1366 -1414 -1415 1411
		mu 0 4 1079 1080 1105 1104
		f 4 1368 -1416 -1417 1413
		mu 0 4 1080 1081 1106 1105
		f 4 1370 -1418 -1419 1415
		mu 0 4 1081 1082 1107 1106
		f 4 1372 -1420 -1421 1417
		mu 0 4 1082 1083 1108 1107
		f 4 1374 -1422 -1423 1419
		mu 0 4 1083 1084 1109 1108
		f 4 1376 -1424 -1425 1421
		mu 0 4 1084 1085 1110 1109
		f 4 1378 -1426 -1427 1423
		mu 0 4 1085 1086 1111 1110
		f 4 1379 -1381 -1428 1425
		mu 0 4 1086 1063 1088 1111
		f 4 1382 -1430 -1431 1428
		mu 0 4 1088 1087 1112 1113
		f 4 1384 -1432 -1433 1429
		mu 0 4 1087 1089 1114 1112
		f 4 1386 -1434 -1435 1431
		mu 0 4 1089 1090 1115 1114
		f 4 1388 -1436 -1437 1433
		mu 0 4 1090 1091 1116 1115
		f 4 1390 -1438 -1439 1435
		mu 0 4 1091 1092 1117 1116
		f 4 1392 -1440 -1441 1437
		mu 0 4 1094 1093 1118 1119
		f 4 1394 -1442 -1443 1439
		mu 0 4 1093 1095 1120 1118
		f 4 1396 -1444 -1445 1441
		mu 0 4 1095 1096 1121 1120
		f 4 1398 -1446 -1447 1443
		mu 0 4 1096 1097 1122 1121
		f 4 1400 -1448 -1449 1445
		mu 0 4 1097 1098 1123 1122
		f 4 1402 -1450 -1451 1447
		mu 0 4 1098 1099 1124 1123
		f 4 1404 -1452 -1453 1449
		mu 0 4 1099 1100 1125 1124
		f 4 1406 -1454 -1455 1451
		mu 0 4 1100 1101 1126 1125
		f 4 1408 -1456 -1457 1453
		mu 0 4 1101 1102 1127 1126
		f 4 1410 -1458 -1459 1455
		mu 0 4 1102 1103 1128 1127
		f 4 1412 -1460 -1461 1457
		mu 0 4 1103 1104 1129 1128
		f 4 1414 -1462 -1463 1459
		mu 0 4 1104 1105 1130 1129
		f 4 1416 -1464 -1465 1461
		mu 0 4 1105 1106 1131 1130
		f 4 1418 -1466 -1467 1463
		mu 0 4 1106 1107 1132 1131
		f 4 1420 -1468 -1469 1465
		mu 0 4 1107 1108 1133 1132
		f 4 1422 -1470 -1471 1467
		mu 0 4 1108 1109 1134 1133
		f 4 1424 -1472 -1473 1469
		mu 0 4 1109 1110 1135 1134
		f 4 1426 -1474 -1475 1471
		mu 0 4 1110 1111 1136 1135
		f 4 1427 -1429 -1476 1473
		mu 0 4 1111 1088 1113 1136
		f 4 1430 -1478 -1479 1476
		mu 0 4 1113 1112 883 885
		f 4 1432 -1480 -1481 1477
		mu 0 4 1112 1114 881 883
		f 4 1434 -1482 -1483 1479
		mu 0 4 1114 1115 875 881
		f 4 1436 -1484 -1485 1481
		mu 0 4 1115 1116 876 875
		f 4 1438 -1486 -1487 1483
		mu 0 4 1116 1117 879 876
		f 4 1440 -1488 -1489 1485
		mu 0 4 1119 1118 871 873
		f 4 1442 -1490 -1491 1487
		mu 0 4 1118 1120 869 871
		f 4 1444 -1492 -1493 1489
		mu 0 4 1120 1121 867 869
		f 4 1446 -1494 -1495 1491
		mu 0 4 1121 1122 857 867
		f 4 1448 -1496 -1497 1493
		mu 0 4 1122 1123 858 857
		f 4 1450 -1498 -1499 1495
		mu 0 4 1123 1124 841 858
		f 4 1452 -1500 -1501 1497
		mu 0 4 1124 1125 837 841
		f 4 1454 -1502 -1503 1499
		mu 0 4 1125 1126 838 837
		f 4 1456 -1504 -1505 1501
		mu 0 4 1126 1127 843 838
		f 4 1458 -1506 -1507 1503
		mu 0 4 1127 1128 844 843
		f 4 1460 -1508 -1509 1505
		mu 0 4 1128 1129 861 844
		f 4 1462 -1510 -1511 1507
		mu 0 4 1129 1130 851 861
		f 4 1464 -1512 -1513 1509
		mu 0 4 1130 1131 847 851
		f 4 1466 -1514 -1515 1511
		mu 0 4 1131 1132 848 847
		f 4 1468 -1516 -1517 1513
		mu 0 4 1132 1133 853 848
		f 4 1470 -1518 -1519 1515
		mu 0 4 1133 1134 854 853
		f 4 1472 -1520 -1521 1517
		mu 0 4 1134 1135 863 854
		f 4 1474 -1522 -1523 1519
		mu 0 4 1135 1136 865 863
		f 4 1475 -1477 -1524 1521
		mu 0 4 1136 1113 885 865
		f 4 1532 1620 1621 1539
		mu 0 4 1137 1138 1139 1140
		f 4 1533 1534 1622 -1621
		mu 0 4 1138 1141 1142 1139
		f 4 -1623 1535 1536 1623
		mu 0 4 1139 1142 1143 1144
		f 4 -1622 -1624 1537 1538
		mu 0 4 1140 1139 1144 1145
		f 4 1540 1624 1625 -1535
		mu 0 4 1141 1146 1147 1142
		f 4 1541 1542 1626 -1625
		mu 0 4 1146 1148 1149 1147
		f 4 -1627 1543 1544 1627
		mu 0 4 1147 1149 1150 1151
		f 4 -1626 -1628 1545 -1536
		mu 0 4 1142 1147 1151 1143
		f 4 1546 1628 1629 1553
		mu 0 4 1148 1152 1153 1154
		f 4 1547 1548 1630 -1629
		mu 0 4 1152 1155 1156 1153
		f 4 -1631 1549 1550 1631
		mu 0 4 1153 1156 1157 1158
		f 4 -1630 -1632 1551 1552
		mu 0 4 1154 1153 1158 1159
		f 4 1554 1632 1633 -1549
		mu 0 4 1155 1160 1161 1156
		f 4 1555 1556 1634 -1633
		mu 0 4 1160 1162 1163 1161
		f 4 -1635 1557 1558 1635
		mu 0 4 1161 1163 1164 1165
		f 4 -1634 -1636 1559 -1550
		mu 0 4 1156 1161 1165 1157
		f 4 1560 1636 1637 1567
		mu 0 4 1159 1166 1167 1168
		f 4 1561 1562 1638 -1637
		mu 0 4 1166 1169 1170 1167
		f 4 -1639 1563 1564 1639
		mu 0 4 1167 1170 1171 1172
		f 4 -1638 -1640 1565 1566
		mu 0 4 1168 1167 1172 1173
		f 4 1568 1640 1641 -1563
		mu 0 4 1169 1174 1175 1170
		f 4 1569 1570 1642 -1641
		mu 0 4 1174 1176 1177 1175
		f 4 -1643 1571 1572 1643
		mu 0 4 1175 1177 1178 1179
		f 4 -1642 -1644 1573 -1564
		mu 0 4 1170 1175 1179 1171
		f 4 1578 1644 1645 1585
		mu 0 4 1180 1181 1182 1183
		f 4 1579 1580 1646 -1645
		mu 0 4 1181 1184 1185 1182
		f 4 -1647 1581 1582 1647
		mu 0 4 1182 1185 1186 1187
		f 4 -1646 -1648 1583 1584
		mu 0 4 1183 1182 1187 1188
		f 4 1586 1648 1649 -1581
		mu 0 4 1184 1189 1190 1185
		f 4 1587 1588 1650 -1649
		mu 0 4 1189 1150 1191 1190
		f 4 -1651 1589 1590 1651
		mu 0 4 1190 1191 1173 1192
		f 4 -1650 -1652 1591 -1582
		mu 0 4 1185 1190 1192 1186
		f 4 -1544 1652 1653 -1589
		mu 0 4 1150 1149 1193 1191
		f 4 -1543 -1554 1654 -1653
		mu 0 4 1149 1148 1154 1193
		f 4 -1655 -1553 -1568 1655
		mu 0 4 1193 1154 1159 1168
		f 4 -1654 -1656 -1567 -1590
		mu 0 4 1191 1193 1168 1173
		f 4 -1548 1656 1657 1599
		mu 0 4 1155 1152 1194 1195
		f 4 -1547 -1542 1658 -1657
		mu 0 4 1152 1148 1146 1194
		f 4 -1659 -1541 1596 1659
		mu 0 4 1194 1146 1141 1196
		f 4 -1658 -1660 1597 1598
		mu 0 4 1195 1194 1196 1197
		f 4 -1534 1660 1661 -1597
		mu 0 4 1141 1138 1198 1196
		f 4 -1533 -1526 1662 -1661
		mu 0 4 1138 1137 1199 1198
		f 4 -1663 -1525 1600 1663
		mu 0 4 1198 1199 1200 1201
		f 4 -1662 -1664 1601 -1598
		mu 0 4 1196 1198 1201 1197
		f 4 -1528 1664 1665 -1601
		mu 0 4 1200 1202 1203 1201
		f 4 -1527 -1556 1666 -1665
		mu 0 4 1202 1162 1160 1203
		f 4 -1667 -1555 -1600 1667
		mu 0 4 1203 1160 1155 1195
		f 4 -1666 -1668 -1599 -1602
		mu 0 4 1201 1203 1195 1197
		f 4 -1532 1668 1669 1605
		mu 0 4 1204 1205 1206 1207
		f 4 -1531 -1570 1670 -1669
		mu 0 4 1205 1176 1174 1206
		f 4 -1671 -1569 1602 1671
		mu 0 4 1206 1174 1169 1208
		f 4 -1670 -1672 1603 1604
		mu 0 4 1207 1206 1208 1209
		f 4 -1562 1672 1673 -1603
		mu 0 4 1169 1166 1210 1208
		f 4 -1561 -1552 1674 -1673
		mu 0 4 1166 1159 1158 1210
		f 4 -1675 -1551 1606 1675
		mu 0 4 1210 1158 1157 1211
		f 4 -1674 -1676 1607 -1604
		mu 0 4 1208 1210 1211 1209
		f 4 -1560 1676 1677 -1607
		mu 0 4 1157 1165 1212 1211
		f 4 -1559 -1530 1678 -1677
		mu 0 4 1165 1164 1213 1212
		f 4 -1679 -1529 -1606 1679
		mu 0 4 1212 1213 1204 1207
		f 4 -1678 -1680 -1605 -1608
		mu 0 4 1211 1212 1207 1209
		f 4 -1580 1680 1681 1611
		mu 0 4 1184 1181 1214 1215
		f 4 -1579 -1578 1682 -1681
		mu 0 4 1181 1180 1216 1214
		f 4 -1683 -1577 1608 1683
		mu 0 4 1214 1216 1217 1218
		f 4 -1682 -1684 1609 1610
		mu 0 4 1215 1214 1218 1219
		f 4 -1576 1684 1685 -1609
		mu 0 4 1217 1220 1221 1218
		f 4 -1575 -1538 1686 -1685
		mu 0 4 1220 1145 1144 1221
		f 4 -1687 -1537 1612 1687
		mu 0 4 1221 1144 1143 1222
		f 4 -1686 -1688 1613 -1610
		mu 0 4 1218 1221 1222 1219
		f 4 -1546 1688 1689 -1613
		mu 0 4 1143 1151 1223 1222
		f 4 -1545 -1588 1690 -1689
		mu 0 4 1151 1150 1189 1223
		f 4 -1691 -1587 -1612 1691
		mu 0 4 1223 1189 1184 1215
		f 4 -1690 -1692 -1611 -1614
		mu 0 4 1222 1223 1215 1219
		f 4 -1574 1692 1693 1617
		mu 0 4 1171 1179 1224 1225
		f 4 -1573 -1596 1694 -1693
		mu 0 4 1179 1178 1226 1224
		f 4 -1695 -1595 1614 1695
		mu 0 4 1224 1226 1227 1228
		f 4 -1694 -1696 1615 1616
		mu 0 4 1225 1224 1228 1229
		f 4 -1594 1696 1697 -1615
		mu 0 4 1227 1230 1231 1228
		f 4 -1593 -1584 1698 -1697
		mu 0 4 1230 1188 1187 1231
		f 4 -1699 -1583 1618 1699
		mu 0 4 1231 1187 1186 1232
		f 4 -1698 -1700 1619 -1616
		mu 0 4 1228 1231 1232 1229
		f 4 -1592 1700 1701 -1619
		mu 0 4 1186 1192 1233 1232
		f 4 -1591 -1566 1702 -1701
		mu 0 4 1192 1173 1172 1233
		f 4 -1703 -1565 -1618 1703
		mu 0 4 1233 1172 1171 1225
		f 4 -1702 -1704 -1617 -1620
		mu 0 4 1232 1233 1225 1229
		f 4 1775 1752 -1706 -1752
		mu 0 4 1234 1235 1236 1237
		f 4 1776 1753 -1708 -1753
		mu 0 4 1235 1238 1239 1236
		f 4 1797 1774 -1711 -1774
		mu 0 4 1240 1241 1242 1243
		f 4 1798 1751 -1712 -1775
		mu 0 4 1241 1234 1237 1242
		f 4 1793 1770 -1715 -1770
		mu 0 4 1244 1245 1246 1247
		f 4 1794 1771 -1717 -1771
		mu 0 4 1245 1248 1249 1246
		f 4 1791 1768 -1720 -1768
		mu 0 4 1250 1251 1252 1253
		f 4 1792 1769 -1721 -1769
		mu 0 4 1251 1244 1247 1252
		f 4 1778 1755 1723 -1755
		mu 0 4 1254 1255 1256 1257
		f 4 1777 1754 1724 -1754
		mu 0 4 1238 1254 1257 1239
		f 4 1796 1773 1726 -1773
		mu 0 4 1258 1240 1243 1259
		f 4 1795 1772 1727 -1772
		mu 0 4 1248 1258 1259 1249
		f 4 1790 1767 1729 -1767
		mu 0 4 1260 1250 1253 1261
		f 4 1789 1766 1731 -1766
		mu 0 4 1262 1260 1261 1263
		f 4 1779 1756 -1734 -1756
		mu 0 4 1255 1264 1265 1256
		f 4 1780 1757 -1736 -1757
		mu 0 4 1264 1266 1267 1265
		f 4 1781 1758 -1738 -1758
		mu 0 4 1266 1268 1269 1267
		f 4 1782 1759 -1740 -1759
		mu 0 4 1268 1270 1271 1269
		f 4 1784 1761 1742 -1761
		mu 0 4 1272 1273 1274 1275
		f 4 1783 1760 1743 -1760
		mu 0 4 1276 1272 1275 1277
		f 4 1785 1762 -1746 -1762
		mu 0 4 1273 1278 1279 1274
		f 4 1786 1763 -1748 -1763
		mu 0 4 1278 1280 1281 1279
		f 4 1787 1764 -1750 -1764
		mu 0 4 1280 1282 1283 1281
		f 4 1788 1765 -1751 -1765
		mu 0 4 1282 1262 1263 1283
		f 4 2244 -1800 1524 1704
		mu 0 4 1284 1285 1286 1287
		f 4 2242 -1705 1525 1706
		mu 0 4 1288 1284 1287 1289
		f 4 2240 -1707 -1540 1722
		mu 0 4 1290 1288 1289 1291
		f 4 2238 -1723 -1539 1721
		mu 0 4 1292 1290 1291 1293
		f 4 2236 -1722 1574 1732
		mu 0 4 1294 1292 1293 1295
		f 4 2234 -1733 1575 1734
		mu 0 4 1296 1294 1295 1297
		f 4 2279 -1735 1576 1736
		mu 0 4 1298 1296 1297 1299
		f 4 2278 -1737 1577 1738
		mu 0 4 1300 1298 1299 1301
		f 4 2276 -1739 -1586 1741
		mu 0 4 1302 1303 1304 1305
		f 4 2274 -1742 -1585 1740
		mu 0 4 1306 1302 1305 1307
		f 4 2272 -1741 1592 1744
		mu 0 4 1308 1306 1307 1309
		f 4 2270 -1745 1593 1746
		mu 0 4 1310 1308 1309 1311
		f 4 2268 -1747 1594 1748
		mu 0 4 1312 1310 1311 1313
		f 4 2266 -1749 1595 1730
		mu 0 4 1314 1312 1313 1315
		f 4 2264 -1731 -1572 1728
		mu 0 4 1316 1314 1315 1317
		f 4 2262 -1729 -1571 1717
		mu 0 4 1318 1316 1317 1319
		f 4 2260 -1718 1530 1718
		mu 0 4 1320 1318 1319 1321
		f 4 2258 -1719 1531 1712
		mu 0 4 1322 1320 1321 1323
		f 4 2256 -1713 1528 1713
		mu 0 4 1324 1322 1323 1325
		f 4 2254 -1714 1529 1715
		mu 0 4 1326 1324 1325 1327
		f 4 2252 -1716 -1558 1725
		mu 0 4 1328 1326 1327 1329
		f 4 2250 -1726 -1557 1708
		mu 0 4 1330 1328 1329 1331
		f 4 2248 -1709 1526 1709
		mu 0 4 1332 1330 1331 1333
		f 4 1799 2246 -1710 1527
		mu 0 4 1286 1285 1332 1333
		f 4 -1781 -1802 -1803 1800
		mu 0 4 1266 1264 1334 1335
		f 4 -1780 -1804 -1805 1801
		mu 0 4 1264 1255 1336 1334
		f 4 -1779 -1806 -1807 1803
		mu 0 4 1255 1254 1337 1336
		f 4 -1778 -1808 -1809 1805
		mu 0 4 1254 1238 1338 1337
		f 4 -1777 -1810 -1811 1807
		mu 0 4 1238 1235 1339 1338
		f 4 -1776 -1812 -1813 1809
		mu 0 4 1235 1234 1340 1339
		f 4 -1815 1811 -1799 -1814
		mu 0 4 1341 1340 1234 1241
		f 4 -1798 -1816 -1817 1813
		mu 0 4 1241 1240 1342 1341
		f 4 -1797 -1818 -1819 1815
		mu 0 4 1240 1258 1343 1342
		f 4 -1796 -1820 -1821 1817
		mu 0 4 1258 1248 1344 1343
		f 4 -1795 -1822 -1823 1819
		mu 0 4 1248 1245 1345 1344
		f 4 -1794 -1824 -1825 1821
		mu 0 4 1245 1244 1346 1345
		f 4 -1793 -1826 -1827 1823
		mu 0 4 1244 1251 1347 1346
		f 4 -1792 -1828 -1829 1825
		mu 0 4 1251 1250 1348 1347
		f 4 -1791 -1830 -1831 1827
		mu 0 4 1250 1260 1349 1348
		f 4 -1790 -1832 -1833 1829
		mu 0 4 1260 1262 1350 1349
		f 4 -1789 -1834 -1835 1831
		mu 0 4 1262 1282 1351 1350
		f 4 -1788 -1836 -1837 1833
		mu 0 4 1282 1280 1352 1351
		f 4 -1787 -1838 -1839 1835
		mu 0 4 1280 1278 1353 1352
		f 4 -1786 -1840 -1841 1837
		mu 0 4 1278 1273 1354 1353
		f 4 -1785 -1842 -1843 1839
		mu 0 4 1273 1272 1355 1354
		f 4 -1784 -1844 -1845 1841
		mu 0 4 1272 1276 1356 1355
		f 4 -1783 -1846 -1847 1843
		mu 0 4 1270 1268 1357 1358
		f 4 -1782 -1801 -1848 1845
		mu 0 4 1268 1266 1335 1357
		f 4 1802 -1850 -1851 1848
		mu 0 4 1335 1334 1359 1360
		f 4 1804 -1852 -1853 1849
		mu 0 4 1334 1336 1361 1359
		f 4 1806 -1854 -1855 1851
		mu 0 4 1336 1337 1362 1361
		f 4 1808 -1856 -1857 1853
		mu 0 4 1337 1338 1363 1362
		f 4 1810 -1858 -1859 1855
		mu 0 4 1338 1339 1364 1363
		f 4 1812 -1860 -1861 1857
		mu 0 4 1339 1340 1365 1364
		f 4 -1863 1859 1814 -1862
		mu 0 4 1366 1365 1340 1341
		f 4 1816 -1864 -1865 1861
		mu 0 4 1341 1342 1367 1366
		f 4 1818 -1866 -1867 1863
		mu 0 4 1342 1343 1368 1367
		f 4 1820 -1868 -1869 1865
		mu 0 4 1343 1344 1369 1368
		f 4 1822 -1870 -1871 1867
		mu 0 4 1344 1345 1370 1369
		f 4 1824 -1872 -1873 1869
		mu 0 4 1345 1346 1371 1370
		f 4 1826 -1874 -1875 1871
		mu 0 4 1346 1347 1372 1371
		f 4 1828 -1876 -1877 1873
		mu 0 4 1347 1348 1373 1372
		f 4 1830 -1878 -1879 1875
		mu 0 4 1348 1349 1374 1373
		f 4 1832 -1880 -1881 1877
		mu 0 4 1349 1350 1375 1374
		f 4 1834 -1882 -1883 1879
		mu 0 4 1350 1351 1376 1375
		f 4 1836 -1884 -1885 1881
		mu 0 4 1351 1352 1377 1376
		f 4 1838 -1886 -1887 1883
		mu 0 4 1352 1353 1378 1377
		f 4 1840 -1888 -1889 1885
		mu 0 4 1353 1354 1379 1378
		f 4 1842 -1890 -1891 1887
		mu 0 4 1354 1355 1380 1379
		f 4 1844 -1892 -1893 1889
		mu 0 4 1355 1356 1381 1380
		f 4 1846 -1894 -1895 1891
		mu 0 4 1358 1357 1382 1383
		f 4 1847 -1849 -1896 1893
		mu 0 4 1357 1335 1360 1382
		f 4 1850 -1898 -1899 1896
		mu 0 4 1360 1359 1384 1385
		f 4 1852 -1900 -1901 1897
		mu 0 4 1359 1361 1386 1384
		f 4 1854 -1902 -1903 1899
		mu 0 4 1361 1362 1387 1386
		f 4 1856 -1904 -1905 1901
		mu 0 4 1362 1363 1388 1387
		f 4 1858 -1906 -1907 1903
		mu 0 4 1363 1364 1389 1388
		f 4 1860 -1908 -1909 1905
		mu 0 4 1364 1365 1390 1389
		f 4 -1911 1907 1862 -1910
		mu 0 4 1391 1390 1365 1366
		f 4 1864 -1912 -1913 1909
		mu 0 4 1366 1367 1392 1391
		f 4 1866 -1914 -1915 1911
		mu 0 4 1367 1368 1393 1392
		f 4 1868 -1916 -1917 1913
		mu 0 4 1368 1369 1394 1393
		f 4 1870 -1918 -1919 1915
		mu 0 4 1369 1370 1395 1394
		f 4 1872 -1920 -1921 1917
		mu 0 4 1370 1371 1396 1395
		f 4 1874 -1922 -1923 1919
		mu 0 4 1371 1372 1397 1396
		f 4 1876 -1924 -1925 1921
		mu 0 4 1372 1373 1398 1397
		f 4 1878 -1926 -1927 1923
		mu 0 4 1373 1374 1399 1398
		f 4 1880 -1928 -1929 1925
		mu 0 4 1374 1375 1400 1399
		f 4 1882 -1930 -1931 1927
		mu 0 4 1375 1376 1401 1400
		f 4 1884 -1932 -1933 1929
		mu 0 4 1376 1377 1402 1401
		f 4 1886 -1934 -1935 1931
		mu 0 4 1377 1378 1403 1402
		f 4 1888 -1936 -1937 1933
		mu 0 4 1378 1379 1404 1403
		f 4 1890 -1938 -1939 1935
		mu 0 4 1379 1380 1405 1404
		f 4 1892 -1940 -1941 1937
		mu 0 4 1380 1381 1406 1405
		f 4 1894 -1942 -1943 1939
		mu 0 4 1383 1382 1407 1408
		f 4 1895 -1897 -1944 1941
		mu 0 4 1382 1360 1385 1407
		f 4 1898 -1946 -1947 1944
		mu 0 4 1385 1384 1409 1410
		f 4 1900 -1948 -1949 1945
		mu 0 4 1384 1386 1411 1409
		f 4 1902 -1950 -1951 1947
		mu 0 4 1386 1387 1412 1411
		f 4 1904 -1952 -1953 1949
		mu 0 4 1387 1388 1413 1412
		f 4 1906 -1954 -1955 1951
		mu 0 4 1388 1389 1414 1413
		f 4 1908 -1956 -1957 1953
		mu 0 4 1389 1390 1415 1414
		f 4 -1959 1955 1910 -1958
		mu 0 4 1416 1415 1390 1391
		f 4 1912 -1960 -1961 1957
		mu 0 4 1391 1392 1417 1416
		f 4 1914 -1962 -1963 1959
		mu 0 4 1392 1393 1418 1417
		f 4 1916 -1964 -1965 1961
		mu 0 4 1393 1394 1419 1418
		f 4 1918 -1966 -1967 1963
		mu 0 4 1394 1395 1420 1419
		f 4 1920 -1968 -1969 1965
		mu 0 4 1395 1396 1421 1420
		f 4 1922 -1970 -1971 1967
		mu 0 4 1396 1397 1422 1421
		f 4 1924 -1972 -1973 1969
		mu 0 4 1397 1398 1423 1422
		f 4 1926 -1974 -1975 1971
		mu 0 4 1398 1399 1424 1423
		f 4 1928 -1976 -1977 1973
		mu 0 4 1399 1400 1425 1424
		f 4 1930 -1978 -1979 1975
		mu 0 4 1400 1401 1426 1425
		f 4 1932 -1980 -1981 1977
		mu 0 4 1401 1402 1427 1426
		f 4 1934 -1982 -1983 1979
		mu 0 4 1402 1403 1428 1427
		f 4 1936 -1984 -1985 1981
		mu 0 4 1403 1404 1429 1428
		f 4 1938 -1986 -1987 1983
		mu 0 4 1404 1405 1430 1429
		f 4 1940 -1988 -1989 1985
		mu 0 4 1405 1406 1431 1430
		f 4 1942 -1990 -1991 1987
		mu 0 4 1408 1407 1432 1433
		f 4 1943 -1945 -1992 1989
		mu 0 4 1407 1385 1410 1432
		f 4 1946 -1994 -1995 1992
		mu 0 4 1410 1409 1434 1435
		f 4 1948 -1996 -1997 1993
		mu 0 4 1409 1411 1436 1434
		f 4 1950 -1998 -1999 1995
		mu 0 4 1411 1412 1437 1436
		f 4 1952 -2000 -2001 1997
		mu 0 4 1412 1413 1438 1437
		f 4 1954 -2002 -2003 1999
		mu 0 4 1413 1414 1439 1438
		f 4 1956 -2004 -2005 2001
		mu 0 4 1414 1415 1440 1439
		f 4 -2007 2003 1958 -2006
		mu 0 4 1441 1440 1415 1416
		f 4 1960 -2008 -2009 2005
		mu 0 4 1416 1417 1442 1441
		f 4 1962 -2010 -2011 2007
		mu 0 4 1417 1418 1443 1442
		f 4 1964 -2012 -2013 2009
		mu 0 4 1418 1419 1444 1443
		f 4 1966 -2014 -2015 2011
		mu 0 4 1419 1420 1445 1444
		f 4 1968 -2016 -2017 2013
		mu 0 4 1420 1421 1446 1445
		f 4 1970 -2018 -2019 2015
		mu 0 4 1421 1422 1447 1446
		f 4 1972 -2020 -2021 2017
		mu 0 4 1422 1423 1448 1447
		f 4 1974 -2022 -2023 2019
		mu 0 4 1423 1424 1449 1448
		f 4 1976 -2024 -2025 2021
		mu 0 4 1424 1425 1450 1449
		f 4 1978 -2026 -2027 2023
		mu 0 4 1425 1426 1451 1450
		f 4 1980 -2028 -2029 2025
		mu 0 4 1426 1427 1452 1451
		f 4 1982 -2030 -2031 2027
		mu 0 4 1427 1428 1453 1452
		f 4 1984 -2032 -2033 2029
		mu 0 4 1428 1429 1454 1453
		f 4 1986 -2034 -2035 2031
		mu 0 4 1429 1430 1455 1454
		f 4 1988 -2036 -2037 2033
		mu 0 4 1430 1431 1456 1455;
	setAttr ".fc[1000:1490]"
		f 4 1990 -2038 -2039 2035
		mu 0 4 1433 1432 1457 1458
		f 4 1991 -1993 -2040 2037
		mu 0 4 1432 1410 1435 1457
		f 4 1994 -2042 -2043 2040
		mu 0 4 1435 1434 1459 1460
		f 4 1996 -2044 -2045 2041
		mu 0 4 1434 1436 1461 1459
		f 4 1998 -2046 -2047 2043
		mu 0 4 1436 1437 1462 1461
		f 4 2000 -2048 -2049 2045
		mu 0 4 1437 1438 1463 1462
		f 4 2002 -2050 -2051 2047
		mu 0 4 1438 1439 1464 1463
		f 4 2004 -2052 -2053 2049
		mu 0 4 1439 1440 1465 1464
		f 4 -2055 2051 2006 -2054
		mu 0 4 1466 1465 1440 1441
		f 4 2008 -2056 -2057 2053
		mu 0 4 1441 1442 1467 1466
		f 4 2010 -2058 -2059 2055
		mu 0 4 1442 1443 1468 1467
		f 4 2012 -2060 -2061 2057
		mu 0 4 1443 1444 1469 1468
		f 4 2014 -2062 -2063 2059
		mu 0 4 1444 1445 1470 1469
		f 4 2016 -2064 -2065 2061
		mu 0 4 1445 1446 1471 1470
		f 4 2018 -2066 -2067 2063
		mu 0 4 1446 1447 1472 1471
		f 4 2020 -2068 -2069 2065
		mu 0 4 1447 1448 1473 1472
		f 4 2022 -2070 -2071 2067
		mu 0 4 1448 1449 1474 1473
		f 4 2024 -2072 -2073 2069
		mu 0 4 1449 1450 1475 1474
		f 4 2026 -2074 -2075 2071
		mu 0 4 1450 1451 1476 1475
		f 4 2028 -2076 -2077 2073
		mu 0 4 1451 1452 1477 1476
		f 4 2030 -2078 -2079 2075
		mu 0 4 1452 1453 1478 1477
		f 4 2032 -2080 -2081 2077
		mu 0 4 1453 1454 1479 1478
		f 4 2034 -2082 -2083 2079
		mu 0 4 1454 1455 1480 1479
		f 4 2036 -2084 -2085 2081
		mu 0 4 1455 1456 1481 1480
		f 4 2038 -2086 -2087 2083
		mu 0 4 1458 1457 1482 1483
		f 4 2039 -2041 -2088 2085
		mu 0 4 1457 1435 1460 1482
		f 4 2042 -2090 -2091 2088
		mu 0 4 1460 1459 1484 1485
		f 4 2044 -2092 -2093 2089
		mu 0 4 1459 1461 1486 1484
		f 4 2046 -2094 -2095 2091
		mu 0 4 1461 1462 1487 1486
		f 4 2048 -2096 -2097 2093
		mu 0 4 1462 1463 1488 1487
		f 4 2050 -2098 -2099 2095
		mu 0 4 1463 1464 1489 1488
		f 4 2052 -2100 -2101 2097
		mu 0 4 1464 1465 1490 1489
		f 4 -2103 2099 2054 -2102
		mu 0 4 1491 1490 1465 1466
		f 4 2056 -2104 -2105 2101
		mu 0 4 1466 1467 1492 1491
		f 4 2058 -2106 -2107 2103
		mu 0 4 1467 1468 1493 1492
		f 4 2060 -2108 -2109 2105
		mu 0 4 1468 1469 1494 1493
		f 4 2062 -2110 -2111 2107
		mu 0 4 1469 1470 1495 1494
		f 4 2064 -2112 -2113 2109
		mu 0 4 1470 1471 1496 1495
		f 4 2066 -2114 -2115 2111
		mu 0 4 1471 1472 1497 1496
		f 4 2068 -2116 -2117 2113
		mu 0 4 1472 1473 1498 1497
		f 4 2070 -2118 -2119 2115
		mu 0 4 1473 1474 1499 1498
		f 4 2072 -2120 -2121 2117
		mu 0 4 1474 1475 1500 1499
		f 4 2074 -2122 -2123 2119
		mu 0 4 1475 1476 1501 1500
		f 4 2076 -2124 -2125 2121
		mu 0 4 1476 1477 1502 1501
		f 4 2078 -2126 -2127 2123
		mu 0 4 1477 1478 1503 1502
		f 4 2080 -2128 -2129 2125
		mu 0 4 1478 1479 1504 1503
		f 4 2082 -2130 -2131 2127
		mu 0 4 1479 1480 1505 1504
		f 4 2084 -2132 -2133 2129
		mu 0 4 1480 1481 1506 1505
		f 4 2086 -2134 -2135 2131
		mu 0 4 1483 1482 1507 1508
		f 4 2087 -2089 -2136 2133
		mu 0 4 1482 1460 1485 1507
		f 4 2090 -2138 -2139 2136
		mu 0 4 1485 1484 1509 1510
		f 4 2092 -2140 -2141 2137
		mu 0 4 1484 1486 1511 1509
		f 4 2094 -2142 -2143 2139
		mu 0 4 1486 1487 1512 1511
		f 4 2096 -2144 -2145 2141
		mu 0 4 1487 1488 1513 1512
		f 4 2098 -2146 -2147 2143
		mu 0 4 1488 1489 1514 1513
		f 4 2100 -2148 -2149 2145
		mu 0 4 1489 1490 1515 1514
		f 4 -2151 2147 2102 -2150
		mu 0 4 1516 1515 1490 1491
		f 4 2104 -2152 -2153 2149
		mu 0 4 1491 1492 1517 1516
		f 4 2106 -2154 -2155 2151
		mu 0 4 1492 1493 1518 1517
		f 4 2108 -2156 -2157 2153
		mu 0 4 1493 1494 1519 1518
		f 4 2110 -2158 -2159 2155
		mu 0 4 1494 1495 1520 1519
		f 4 2112 -2160 -2161 2157
		mu 0 4 1495 1496 1521 1520
		f 4 2114 -2162 -2163 2159
		mu 0 4 1496 1497 1522 1521
		f 4 2116 -2164 -2165 2161
		mu 0 4 1497 1498 1523 1522
		f 4 2118 -2166 -2167 2163
		mu 0 4 1498 1499 1524 1523
		f 4 2120 -2168 -2169 2165
		mu 0 4 1499 1500 1525 1524
		f 4 2122 -2170 -2171 2167
		mu 0 4 1500 1501 1526 1525
		f 4 2124 -2172 -2173 2169
		mu 0 4 1501 1502 1527 1526
		f 4 2126 -2174 -2175 2171
		mu 0 4 1502 1503 1528 1527
		f 4 2128 -2176 -2177 2173
		mu 0 4 1503 1504 1529 1528
		f 4 2130 -2178 -2179 2175
		mu 0 4 1504 1505 1530 1529
		f 4 2132 -2180 -2181 2177
		mu 0 4 1505 1506 1531 1530
		f 4 2134 -2182 -2183 2179
		mu 0 4 1508 1507 1532 1533
		f 4 2135 -2137 -2184 2181
		mu 0 4 1507 1485 1510 1532
		f 4 2138 -2186 -2187 2184
		mu 0 4 1510 1509 1534 1535
		f 4 2140 -2188 -2189 2185
		mu 0 4 1509 1511 1536 1534
		f 4 2142 -2190 -2191 2187
		mu 0 4 1511 1512 1537 1536
		f 4 2144 -2192 -2193 2189
		mu 0 4 1512 1513 1538 1537
		f 4 2146 -2194 -2195 2191
		mu 0 4 1513 1514 1539 1538
		f 4 2148 -2196 -2197 2193
		mu 0 4 1514 1515 1540 1539
		f 4 -2199 2195 2150 -2198
		mu 0 4 1541 1540 1515 1516
		f 4 2152 -2200 -2201 2197
		mu 0 4 1516 1517 1542 1541
		f 4 2154 -2202 -2203 2199
		mu 0 4 1517 1518 1543 1542
		f 4 2156 -2204 -2205 2201
		mu 0 4 1518 1519 1544 1543
		f 4 2158 -2206 -2207 2203
		mu 0 4 1519 1520 1545 1544
		f 4 2160 -2208 -2209 2205
		mu 0 4 1520 1521 1546 1545
		f 4 2162 -2210 -2211 2207
		mu 0 4 1521 1522 1547 1546
		f 4 2164 -2212 -2213 2209
		mu 0 4 1522 1523 1548 1547
		f 4 2166 -2214 -2215 2211
		mu 0 4 1523 1524 1549 1548
		f 4 2168 -2216 -2217 2213
		mu 0 4 1524 1525 1550 1549
		f 4 2170 -2218 -2219 2215
		mu 0 4 1525 1526 1551 1550
		f 4 2172 -2220 -2221 2217
		mu 0 4 1526 1527 1552 1551
		f 4 2174 -2222 -2223 2219
		mu 0 4 1527 1528 1553 1552
		f 4 2176 -2224 -2225 2221
		mu 0 4 1528 1529 1554 1553
		f 4 2178 -2226 -2227 2223
		mu 0 4 1529 1530 1555 1554
		f 4 2180 -2228 -2229 2225
		mu 0 4 1530 1531 1556 1555
		f 4 2182 -2230 -2231 2227
		mu 0 4 1533 1532 1557 1558
		f 4 2183 -2185 -2232 2229
		mu 0 4 1532 1510 1535 1557
		f 4 2186 -2234 -2235 2232
		mu 0 4 1535 1534 1294 1296
		f 4 2188 -2236 -2237 2233
		mu 0 4 1534 1536 1292 1294
		f 4 2190 -2238 -2239 2235
		mu 0 4 1536 1537 1290 1292
		f 4 2192 -2240 -2241 2237
		mu 0 4 1537 1538 1288 1290
		f 4 2194 -2242 -2243 2239
		mu 0 4 1538 1539 1284 1288
		f 4 2196 -2244 -2245 2241
		mu 0 4 1539 1540 1285 1284
		f 4 -2247 2243 2198 -2246
		mu 0 4 1332 1285 1540 1541
		f 4 2200 -2248 -2249 2245
		mu 0 4 1541 1542 1330 1332
		f 4 2202 -2250 -2251 2247
		mu 0 4 1542 1543 1328 1330
		f 4 2204 -2252 -2253 2249
		mu 0 4 1543 1544 1326 1328
		f 4 2206 -2254 -2255 2251
		mu 0 4 1544 1545 1324 1326
		f 4 2208 -2256 -2257 2253
		mu 0 4 1545 1546 1322 1324
		f 4 2210 -2258 -2259 2255
		mu 0 4 1546 1547 1320 1322
		f 4 2212 -2260 -2261 2257
		mu 0 4 1547 1548 1318 1320
		f 4 2214 -2262 -2263 2259
		mu 0 4 1548 1549 1316 1318
		f 4 2216 -2264 -2265 2261
		mu 0 4 1549 1550 1314 1316
		f 4 2218 -2266 -2267 2263
		mu 0 4 1550 1551 1312 1314
		f 4 2220 -2268 -2269 2265
		mu 0 4 1551 1552 1310 1312
		f 4 2222 -2270 -2271 2267
		mu 0 4 1552 1553 1308 1310
		f 4 2224 -2272 -2273 2269
		mu 0 4 1553 1554 1306 1308
		f 4 2226 -2274 -2275 2271
		mu 0 4 1554 1555 1302 1306
		f 4 2228 -2276 -2277 2273
		mu 0 4 1555 1556 1303 1302
		f 4 2230 -2278 -2279 2275
		mu 0 4 1558 1557 1298 1300
		f 4 2231 -2233 -2280 2277
		mu 0 4 1557 1535 1296 1298
		f 4 -2284 -2283 -2282 -2281
		mu 0 4 1559 1560 1561 1562
		f 3 2280 2285 -2285
		mu 0 3 1563 1564 1565
		f 3 2281 2286 -2286
		mu 0 3 1564 1566 1565
		f 3 2282 2287 -2287
		mu 0 3 1566 1567 1565
		f 3 2283 2284 -2288
		mu 0 3 1567 1568 1565
		f 4 -2292 -2291 -2290 -2289
		mu 0 4 1569 1570 1571 1572
		f 3 2288 2293 -2293
		mu 0 3 1573 1574 1575
		f 3 2289 2294 -2294
		mu 0 3 1574 1576 1575
		f 3 2290 2295 -2295
		mu 0 3 1576 1577 1575
		f 3 2291 2292 -2296
		mu 0 3 1577 1578 1575
		f 4 -2300 -2299 -2298 -2297
		mu 0 4 1579 1580 1581 1582
		f 3 2296 2301 -2301
		mu 0 3 1583 1584 1585
		f 3 2297 2302 -2302
		mu 0 3 1584 1586 1585
		f 3 2298 2303 -2303
		mu 0 3 1586 1587 1585
		f 3 2299 2300 -2304
		mu 0 3 1587 1588 1585
		f 4 -2308 -2307 -2306 -2305
		mu 0 4 1589 1590 1591 1592
		f 3 2304 2309 -2309
		mu 0 3 1593 1594 1595
		f 3 2305 2310 -2310
		mu 0 3 1594 1596 1595
		f 3 2306 2311 -2311
		mu 0 3 1596 1597 1595
		f 3 2307 2308 -2312
		mu 0 3 1597 1598 1595
		f 4 -2316 -2315 -2314 -2313
		mu 0 4 1599 1600 1601 1602
		f 3 2312 2317 -2317
		mu 0 3 1603 1604 1605
		f 3 2313 2318 -2318
		mu 0 3 1604 1606 1605
		f 3 2314 2319 -2319
		mu 0 3 1606 1607 1605
		f 3 2315 2316 -2320
		mu 0 3 1607 1608 1605
		f 4 -2324 -2323 -2322 -2321
		mu 0 4 1609 1610 1611 1612
		f 3 2320 2325 -2325
		mu 0 3 1613 1614 1615
		f 3 2321 2326 -2326
		mu 0 3 1614 1616 1615
		f 3 2322 2327 -2327
		mu 0 3 1616 1617 1615
		f 3 2323 2324 -2328
		mu 0 3 1617 1618 1615
		f 4 -2332 -2331 -2330 -2329
		mu 0 4 1619 1620 1621 1622
		f 3 2328 2333 -2333
		mu 0 3 1623 1624 1625
		f 3 2329 2334 -2334
		mu 0 3 1624 1626 1625
		f 3 2330 2335 -2335
		mu 0 3 1626 1627 1625
		f 3 2331 2332 -2336
		mu 0 3 1627 1628 1625
		f 4 -2340 -2339 -2338 -2337
		mu 0 4 1629 1630 1631 1632
		f 3 2336 2341 -2341
		mu 0 3 1633 1634 1635
		f 3 2337 2342 -2342
		mu 0 3 1634 1636 1635
		f 3 2338 2343 -2343
		mu 0 3 1636 1637 1635
		f 3 2339 2340 -2344
		mu 0 3 1637 1638 1635
		f 4 -2348 -2347 -2346 -2345
		mu 0 4 1639 1640 1641 1642
		f 3 2344 2349 -2349
		mu 0 3 1643 1644 1645
		f 3 2345 2350 -2350
		mu 0 3 1644 1646 1645
		f 3 2346 2351 -2351
		mu 0 3 1646 1647 1645
		f 3 2347 2348 -2352
		mu 0 3 1647 1648 1645
		f 4 -2356 -2355 -2354 -2353
		mu 0 4 1649 1650 1651 1652
		f 3 2352 2357 -2357
		mu 0 3 1653 1654 1655
		f 3 2353 2358 -2358
		mu 0 3 1654 1656 1655
		f 3 2354 2359 -2359
		mu 0 3 1656 1657 1655
		f 3 2355 2356 -2360
		mu 0 3 1657 1658 1655
		f 4 -2364 -2363 -2362 -2361
		mu 0 4 1659 1660 1661 1662
		f 3 2360 2365 -2365
		mu 0 3 1663 1664 1665
		f 3 2361 2366 -2366
		mu 0 3 1664 1666 1665
		f 3 2362 2367 -2367
		mu 0 3 1666 1667 1665
		f 3 2363 2364 -2368
		mu 0 3 1667 1668 1665
		f 4 -2372 -2371 -2370 -2369
		mu 0 4 1669 1670 1671 1672
		f 3 2368 2373 -2373
		mu 0 3 1673 1674 1675
		f 3 2369 2374 -2374
		mu 0 3 1674 1676 1675
		f 3 2370 2375 -2375
		mu 0 3 1676 1677 1675
		f 3 2371 2372 -2376
		mu 0 3 1677 1678 1675
		f 4 -2380 -2379 -2378 -2377
		mu 0 4 1679 1680 1681 1682
		f 3 2376 2381 -2381
		mu 0 3 1683 1684 1685
		f 3 2377 2382 -2382
		mu 0 3 1684 1686 1685
		f 3 2378 2383 -2383
		mu 0 3 1686 1687 1685
		f 3 2379 2380 -2384
		mu 0 3 1687 1688 1685
		f 4 -2388 -2387 -2386 -2385
		mu 0 4 1689 1690 1691 1692
		f 3 2384 2389 -2389
		mu 0 3 1693 1694 1695
		f 3 2385 2390 -2390
		mu 0 3 1694 1696 1695
		f 3 2386 2391 -2391
		mu 0 3 1696 1697 1695
		f 3 2387 2388 -2392
		mu 0 3 1697 1698 1695
		f 4 -2396 -2395 -2394 -2393
		mu 0 4 1699 1700 1701 1702
		f 3 2392 2397 -2397
		mu 0 3 1703 1704 1705
		f 3 2393 2398 -2398
		mu 0 3 1704 1706 1705
		f 3 2394 2399 -2399
		mu 0 3 1706 1707 1705
		f 3 2395 2396 -2400
		mu 0 3 1707 1708 1705
		f 4 -2404 -2403 -2402 -2401
		mu 0 4 1709 1710 1711 1712
		f 3 2400 2405 -2405
		mu 0 3 1713 1714 1715
		f 3 2401 2406 -2406
		mu 0 3 1714 1716 1715
		f 3 2402 2407 -2407
		mu 0 3 1716 1717 1715
		f 3 2403 2404 -2408
		mu 0 3 1717 1718 1715
		f 4 -2412 -2411 -2410 -2409
		mu 0 4 1719 1720 1721 1722
		f 3 2408 2413 -2413
		mu 0 3 1723 1724 1725
		f 3 2409 2414 -2414
		mu 0 3 1724 1726 1725
		f 3 2410 2415 -2415
		mu 0 3 1726 1727 1725
		f 3 2411 2412 -2416
		mu 0 3 1727 1728 1725
		f 4 -2420 -2419 -2418 -2417
		mu 0 4 1729 1730 1731 1732
		f 3 2416 2421 -2421
		mu 0 3 1733 1734 1735
		f 3 2417 2422 -2422
		mu 0 3 1734 1736 1735
		f 3 2418 2423 -2423
		mu 0 3 1736 1737 1735
		f 3 2419 2420 -2424
		mu 0 3 1737 1738 1735
		f 4 -2428 -2427 -2426 -2425
		mu 0 4 1739 1740 1741 1742
		f 3 2424 2429 -2429
		mu 0 3 1743 1744 1745
		f 3 2425 2430 -2430
		mu 0 3 1744 1746 1745
		f 3 2426 2431 -2431
		mu 0 3 1746 1747 1745
		f 3 2427 2428 -2432
		mu 0 3 1747 1748 1745
		f 4 -2436 -2435 -2434 -2433
		mu 0 4 1749 1750 1751 1752
		f 3 2432 2437 -2437
		mu 0 3 1753 1754 1755
		f 3 2433 2438 -2438
		mu 0 3 1754 1756 1755
		f 3 2434 2439 -2439
		mu 0 3 1756 1757 1755
		f 3 2435 2436 -2440
		mu 0 3 1757 1758 1755
		f 4 -2444 -2443 -2442 -2441
		mu 0 4 1759 1760 1761 1762
		f 3 2440 2445 -2445
		mu 0 3 1763 1764 1765
		f 3 2441 2446 -2446
		mu 0 3 1764 1766 1765
		f 3 2442 2447 -2447
		mu 0 3 1766 1767 1765
		f 3 2443 2444 -2448
		mu 0 3 1767 1768 1765
		f 4 -2452 -2451 -2450 -2449
		mu 0 4 1769 1770 1771 1772
		f 3 2448 2453 -2453
		mu 0 3 1773 1774 1775
		f 3 2449 2454 -2454
		mu 0 3 1774 1776 1775
		f 3 2450 2455 -2455
		mu 0 3 1776 1777 1775
		f 3 2451 2452 -2456
		mu 0 3 1777 1778 1775
		f 4 -2460 -2459 -2458 -2457
		mu 0 4 1779 1780 1781 1782
		f 3 2456 2461 -2461
		mu 0 3 1783 1784 1785
		f 3 2457 2462 -2462
		mu 0 3 1784 1786 1785
		f 3 2458 2463 -2463
		mu 0 3 1786 1787 1785
		f 3 2459 2460 -2464
		mu 0 3 1787 1788 1785
		f 4 -2468 -2467 -2466 -2465
		mu 0 4 1789 1790 1791 1792
		f 3 2464 2469 -2469
		mu 0 3 1793 1794 1795
		f 3 2465 2470 -2470
		mu 0 3 1794 1796 1795
		f 3 2466 2471 -2471
		mu 0 3 1796 1797 1795
		f 3 2467 2468 -2472
		mu 0 3 1797 1798 1795
		f 4 -2476 -2475 -2474 -2473
		mu 0 4 1799 1800 1801 1802
		f 3 2472 2477 -2477
		mu 0 3 1803 1804 1805
		f 3 2473 2478 -2478
		mu 0 3 1804 1806 1805
		f 3 2474 2479 -2479
		mu 0 3 1806 1807 1805
		f 3 2475 2476 -2480
		mu 0 3 1807 1808 1805
		f 4 -2484 -2483 -2482 -2481
		mu 0 4 1809 1810 1811 1812
		f 3 2480 2485 -2485
		mu 0 3 1813 1814 1815
		f 3 2481 2486 -2486
		mu 0 3 1814 1816 1815
		f 3 2482 2487 -2487
		mu 0 3 1816 1817 1815
		f 3 2483 2484 -2488
		mu 0 3 1817 1818 1815
		f 4 -2492 -2491 -2490 -2489
		mu 0 4 1819 1820 1821 1822
		f 3 2488 2493 -2493
		mu 0 3 1823 1824 1825
		f 3 2489 2494 -2494
		mu 0 3 1824 1826 1825
		f 3 2490 2495 -2495
		mu 0 3 1826 1827 1825
		f 3 2491 2492 -2496
		mu 0 3 1827 1828 1825
		f 4 -2500 -2499 -2498 -2497
		mu 0 4 1829 1830 1831 1832
		f 3 2496 2501 -2501
		mu 0 3 1833 1834 1835
		f 3 2497 2502 -2502
		mu 0 3 1834 1836 1835
		f 3 2498 2503 -2503
		mu 0 3 1836 1837 1835
		f 3 2499 2500 -2504
		mu 0 3 1837 1838 1835
		f 4 -2508 -2507 -2506 -2505
		mu 0 4 1839 1840 1841 1842
		f 3 2504 2509 -2509
		mu 0 3 1843 1844 1845
		f 3 2505 2510 -2510
		mu 0 3 1844 1846 1845
		f 3 2506 2511 -2511
		mu 0 3 1846 1847 1845
		f 3 2507 2508 -2512
		mu 0 3 1847 1848 1845
		f 4 -2516 -2515 -2514 -2513
		mu 0 4 1849 1850 1851 1852
		f 3 2512 2517 -2517
		mu 0 3 1853 1854 1855
		f 3 2513 2518 -2518
		mu 0 3 1854 1856 1855
		f 3 2514 2519 -2519
		mu 0 3 1856 1857 1855
		f 3 2515 2516 -2520
		mu 0 3 1857 1858 1855
		f 4 -2524 -2523 -2522 -2521
		mu 0 4 1859 1860 1861 1862
		f 3 2520 2525 -2525
		mu 0 3 1863 1864 1865
		f 3 2521 2526 -2526
		mu 0 3 1864 1866 1865
		f 3 2522 2527 -2527
		mu 0 3 1866 1867 1865
		f 3 2523 2524 -2528
		mu 0 3 1867 1868 1865
		f 4 -2532 -2531 -2530 -2529
		mu 0 4 1869 1870 1871 1872
		f 3 2528 2533 -2533
		mu 0 3 1873 1874 1875
		f 3 2529 2534 -2534
		mu 0 3 1874 1876 1875
		f 3 2530 2535 -2535
		mu 0 3 1876 1877 1875
		f 3 2531 2532 -2536
		mu 0 3 1877 1878 1875
		f 4 -2540 -2539 -2538 -2537
		mu 0 4 1879 1880 1881 1882
		f 3 2536 2541 -2541
		mu 0 3 1883 1884 1885
		f 3 2537 2542 -2542
		mu 0 3 1884 1886 1885
		f 3 2538 2543 -2543
		mu 0 3 1886 1887 1885
		f 3 2539 2540 -2544
		mu 0 3 1887 1888 1885
		f 4 -2548 -2547 -2546 -2545
		mu 0 4 1889 1890 1891 1892
		f 3 2544 2549 -2549
		mu 0 3 1893 1894 1895
		f 3 2545 2550 -2550
		mu 0 3 1894 1896 1895
		f 3 2546 2551 -2551
		mu 0 3 1896 1897 1895
		f 3 2547 2548 -2552
		mu 0 3 1897 1898 1895
		f 4 -2556 -2555 -2554 -2553
		mu 0 4 1899 1900 1901 1902
		f 3 2552 2557 -2557
		mu 0 3 1903 1904 1905
		f 3 2553 2558 -2558
		mu 0 3 1904 1906 1905
		f 3 2554 2559 -2559
		mu 0 3 1906 1907 1905
		f 3 2555 2556 -2560
		mu 0 3 1907 1908 1905
		f 4 -2564 -2563 -2562 -2561
		mu 0 4 1909 1910 1911 1912
		f 3 2560 2565 -2565
		mu 0 3 1913 1914 1915
		f 3 2561 2566 -2566
		mu 0 3 1914 1916 1915
		f 3 2562 2567 -2567
		mu 0 3 1916 1917 1915
		f 3 2563 2564 -2568
		mu 0 3 1917 1918 1915
		f 4 -2572 -2571 -2570 -2569
		mu 0 4 1919 1920 1921 1922
		f 3 2568 2573 -2573
		mu 0 3 1923 1924 1925
		f 3 2569 2574 -2574
		mu 0 3 1924 1926 1925
		f 3 2570 2575 -2575
		mu 0 3 1926 1927 1925
		f 3 2571 2572 -2576
		mu 0 3 1927 1928 1925
		f 4 -2580 -2579 -2578 -2577
		mu 0 4 1929 1930 1931 1932
		f 3 2576 2581 -2581
		mu 0 3 1933 1934 1935
		f 3 2577 2582 -2582
		mu 0 3 1934 1936 1935
		f 3 2578 2583 -2583
		mu 0 3 1936 1937 1935
		f 3 2579 2580 -2584
		mu 0 3 1937 1938 1935
		f 4 -2588 -2587 -2586 -2585
		mu 0 4 1939 1940 1941 1942
		f 3 2584 2589 -2589
		mu 0 3 1943 1944 1945
		f 3 2585 2590 -2590
		mu 0 3 1944 1946 1945
		f 3 2586 2591 -2591
		mu 0 3 1946 1947 1945
		f 3 2587 2588 -2592
		mu 0 3 1947 1948 1945
		f 4 -2596 -2595 -2594 -2593
		mu 0 4 1949 1950 1951 1952
		f 3 2592 2597 -2597
		mu 0 3 1953 1954 1955
		f 3 2593 2598 -2598
		mu 0 3 1954 1956 1955
		f 3 2594 2599 -2599
		mu 0 3 1956 1957 1955
		f 3 2595 2596 -2600
		mu 0 3 1957 1958 1955
		f 4 -2604 -2603 -2602 -2601
		mu 0 4 1959 1960 1961 1962
		f 3 2600 2605 -2605
		mu 0 3 1963 1964 1965
		f 3 2601 2606 -2606
		mu 0 3 1964 1966 1965
		f 3 2602 2607 -2607
		mu 0 3 1966 1967 1965
		f 3 2603 2604 -2608
		mu 0 3 1967 1968 1965
		f 4 -2612 -2611 -2610 -2609
		mu 0 4 1969 1970 1971 1972
		f 3 2608 2613 -2613
		mu 0 3 1973 1974 1975
		f 3 2609 2614 -2614
		mu 0 3 1974 1976 1975
		f 3 2610 2615 -2615
		mu 0 3 1976 1977 1975
		f 3 2611 2612 -2616
		mu 0 3 1977 1978 1975
		f 4 -2620 -2619 -2618 -2617
		mu 0 4 1979 1980 1981 1982
		f 3 2616 2621 -2621
		mu 0 3 1983 1984 1985
		f 3 2617 2622 -2622
		mu 0 3 1984 1986 1985
		f 3 2618 2623 -2623
		mu 0 3 1986 1987 1985
		f 3 2619 2620 -2624
		mu 0 3 1987 1988 1985
		f 4 -2628 -2627 -2626 -2625
		mu 0 4 1989 1990 1991 1992
		f 3 2624 2629 -2629
		mu 0 3 1993 1994 1995
		f 3 2625 2630 -2630
		mu 0 3 1994 1996 1995
		f 3 2626 2631 -2631
		mu 0 3 1996 1997 1995
		f 3 2627 2628 -2632
		mu 0 3 1997 1998 1995
		f 4 -2636 -2635 -2634 -2633
		mu 0 4 1999 2000 2001 2002
		f 3 2632 2637 -2637
		mu 0 3 2003 2004 2005
		f 3 2633 2638 -2638
		mu 0 3 2004 2006 2005
		f 3 2634 2639 -2639
		mu 0 3 2006 2007 2005
		f 3 2635 2636 -2640
		mu 0 3 2007 2008 2005
		f 4 -2644 -2643 -2642 -2641
		mu 0 4 2009 2010 2011 2012
		f 3 2640 2645 -2645
		mu 0 3 2013 2014 2015
		f 3 2641 2646 -2646
		mu 0 3 2014 2016 2015
		f 3 2642 2647 -2647
		mu 0 3 2016 2017 2015
		f 3 2643 2644 -2648
		mu 0 3 2017 2018 2015
		f 4 -2652 -2651 -2650 -2649
		mu 0 4 2019 2020 2021 2022
		f 3 2648 2653 -2653
		mu 0 3 2023 2024 2025
		f 3 2649 2654 -2654
		mu 0 3 2024 2026 2025
		f 3 2650 2655 -2655
		mu 0 3 2026 2027 2025
		f 3 2651 2652 -2656
		mu 0 3 2027 2028 2025
		f 4 -2660 -2659 -2658 -2657
		mu 0 4 2029 2030 2031 2032
		f 3 2656 2661 -2661
		mu 0 3 2033 2034 2035
		f 3 2657 2662 -2662
		mu 0 3 2034 2036 2035
		f 3 2658 2663 -2663
		mu 0 3 2036 2037 2035
		f 3 2659 2660 -2664
		mu 0 3 2037 2038 2035
		f 4 -2668 -2667 -2666 -2665
		mu 0 4 2039 2040 2041 2042
		f 3 2664 2669 -2669
		mu 0 3 2043 2044 2045
		f 3 2665 2670 -2670
		mu 0 3 2044 2046 2045
		f 3 2666 2671 -2671
		mu 0 3 2046 2047 2045
		f 3 2667 2668 -2672
		mu 0 3 2047 2048 2045
		f 4 -2676 -2675 -2674 -2673
		mu 0 4 2049 2050 2051 2052
		f 3 2672 2677 -2677
		mu 0 3 2053 2054 2055
		f 3 2673 2678 -2678
		mu 0 3 2054 2056 2055
		f 3 2674 2679 -2679
		mu 0 3 2056 2057 2055
		f 3 2675 2676 -2680
		mu 0 3 2057 2058 2055
		f 4 -2684 -2683 -2682 -2681
		mu 0 4 2059 2060 2061 2062
		f 3 2680 2685 -2685
		mu 0 3 2063 2064 2065
		f 3 2681 2686 -2686
		mu 0 3 2064 2066 2065
		f 3 2682 2687 -2687
		mu 0 3 2066 2067 2065
		f 3 2683 2684 -2688
		mu 0 3 2067 2068 2065
		f 4 -2692 -2691 -2690 -2689
		mu 0 4 2069 2070 2071 2072
		f 3 2688 2693 -2693
		mu 0 3 2073 2074 2075
		f 3 2689 2694 -2694
		mu 0 3 2074 2076 2075
		f 3 2690 2695 -2695
		mu 0 3 2076 2077 2075
		f 3 2691 2692 -2696
		mu 0 3 2077 2078 2075
		f 4 -2700 -2699 -2698 -2697
		mu 0 4 2079 2080 2081 2082
		f 3 2696 2701 -2701
		mu 0 3 2083 2084 2085
		f 3 2697 2702 -2702
		mu 0 3 2084 2086 2085
		f 3 2698 2703 -2703
		mu 0 3 2086 2087 2085
		f 3 2699 2700 -2704
		mu 0 3 2087 2088 2085
		f 4 -2708 -2707 -2706 -2705
		mu 0 4 2089 2090 2091 2092
		f 4 2776 -2711 -2710 -2765
		mu 0 4 2093 2094 2095 2096
		f 4 -2716 -2715 -2714 -2713
		mu 0 4 2097 2098 2099 2100
		f 4 2719 -2769 2780 -2717
		mu 0 4 2101 2102 2103 2104
		f 4 2707 -2767 2778 -2721
		mu 0 4 2105 2106 2107 2108
		f 4 2784 -2725 -2724 -2773
		mu 0 4 2109 2110 2111 2112
		f 4 2728 2716 2781 -2727
		mu 0 4 2113 2101 2104 2114
		f 4 2713 -2732 -2731 -2730
		mu 0 4 2100 2099 2115 2116
		f 4 2787 2764 -2734 -2776
		mu 0 4 2117 2093 2096 2118
		f 4 2705 -2738 -2737 -2736
		mu 0 4 2092 2091 2119 2120
		f 4 2782 2771 2739 2726
		mu 0 4 2114 2121 2122 2113
		f 4 2725 -2743 2730 -2742
		mu 0 4 2123 2124 2116 2115
		f 4 2745 2786 2775 -2744
		mu 0 4 2125 2126 2117 2118
		f 4 2723 -2748 2736 -2747
		mu 0 4 2127 2128 2120 2119
		f 4 2749 2746 2748 -2740
		mu 0 4 2129 2127 2119 2130
		f 4 2750 -2729 -2749 2737
		mu 0 4 2091 2131 2130 2119
		f 4 -2752 -2720 -2751 2706
		mu 0 4 2090 2132 2131 2091
		f 4 2751 2720 2779 2768
		mu 0 4 2102 2105 2108 2103
		f 4 2717 -2753 2712 -2754
		mu 0 4 2133 2134 2097 2100
		f 4 2727 2753 2729 -2755
		mu 0 4 2135 2133 2100 2116
		f 4 2755 2738 2754 2742
		mu 0 4 2124 2136 2135 2116
		f 4 2783 2772 -2750 -2772
		mu 0 4 2121 2109 2112 2122
		f 4 2757 2741 2756 -2745
		mu 0 4 2137 2123 2115 2138
		f 4 2758 -2735 -2757 2731
		mu 0 4 2099 2139 2138 2115
		f 4 -2760 -2712 -2759 2714
		mu 0 4 2098 2140 2139 2099
		f 4 2760 2710 2777 2766
		mu 0 4 2106 2095 2094 2107
		f 4 2709 -2761 2704 -2762
		mu 0 4 2141 2142 2089 2092
		f 4 2733 2761 2735 -2763
		mu 0 4 2143 2141 2092 2120
		f 4 2763 2743 2762 2747
		mu 0 4 2128 2144 2143 2120
		f 4 2785 -2746 -2764 2724
		mu 0 4 2110 2126 2125 2111
		f 4 2711 -2766 -2777 -2709
		mu 0 4 2139 2140 2094 2093
		f 4 -2778 2765 2759 2721
		mu 0 4 2107 2094 2140 2098
		f 4 -2779 -2722 2715 -2768
		mu 0 4 2108 2107 2098 2097
		f 4 -2780 2767 2752 2718
		mu 0 4 2103 2108 2097 2134
		f 4 -2781 -2719 -2718 -2770
		mu 0 4 2104 2103 2134 2133
		f 4 -2782 2769 -2728 -2771
		mu 0 4 2114 2104 2133 2135
		f 4 2740 -2783 2770 -2739
		mu 0 4 2136 2121 2114 2135
		f 4 -2756 2722 -2784 -2741
		mu 0 4 2136 2124 2109 2121
		f 4 -2726 -2774 -2785 -2723
		mu 0 4 2124 2123 2110 2109
		f 4 -2758 -2775 -2786 2773
		mu 0 4 2123 2137 2126 2110
		f 4 -2787 2774 2744 2732
		mu 0 4 2117 2126 2137 2138
		f 4 2734 2708 -2788 -2733
		mu 0 4 2138 2139 2093 2117
		f 4 2791 -2791 -2790 -2789
		mu 0 4 2145 2146 2147 2148
		f 4 2794 -2794 -2792 -2793
		mu 0 4 2149 2150 2151 2152
		f 4 2797 -2797 -2795 -2796
		mu 0 4 2153 2154 2150 2149
		f 4 2789 -2800 -2798 -2799
		mu 0 4 2148 2147 2154 2153
		f 4 2790 2793 2796 2799
		mu 0 4 2155 2156 2150 2154
		f 4 -2804 -2803 -2802 -2801
		mu 0 4 2157 2158 2159 2160
		f 4 2800 2805 2798 -2805
		mu 0 4 2157 2161 2148 2153
		f 4 2803 2804 2795 -2807
		mu 0 4 2158 2157 2153 2149
		f 4 2802 2806 2792 -2808
		mu 0 4 2162 2158 2149 2152
		f 4 2801 2807 2788 -2806
		mu 0 4 2161 2163 2145 2148
		f 4 2827 2820 2813 -2824
		mu 0 4 2164 2165 2166 2167
		f 4 2826 2823 2815 -2823
		mu 0 4 2168 2169 2170 2171
		f 4 2825 2822 2817 -2822
		mu 0 4 2172 2168 2171 2173
		f 4 2824 2821 2819 -2821
		mu 0 4 2165 2172 2173 2166
		f 4 -2820 -2818 -2816 -2814
		mu 0 4 2174 2173 2171 2175
		f 4 2818 2812 2814 2816
		mu 0 4 2176 2177 2178 2179
		f 4 2811 -2825 -2809 -2819
		mu 0 4 2176 2172 2165 2180
		f 4 2810 -2826 -2812 -2817
		mu 0 4 2179 2168 2172 2176
		f 4 2809 -2827 -2811 -2815
		mu 0 4 2181 2169 2168 2179
		f 4 2808 -2828 -2810 -2813
		mu 0 4 2180 2165 2164 2182
		f 4 2884 2877 2866 2859
		mu 0 4 2183 2184 2185 2186
		f 4 2846 2902 2891 -2843
		mu 0 4 2187 2188 2189 2190
		f 4 2868 2861 2882 2875
		mu 0 4 2191 2192 2193 2194
		f 4 2906 2895 2841 2839
		mu 0 4 2195 2196 2197 2198
		f 4 2904 2893 -2876 2883
		mu 0 4 2199 2200 2191 2194
		f 4 2871 2910 2899 2872
		mu 0 4 2201 2202 2203 2204
		f 4 2907 2896 2849 -2896
		mu 0 4 2196 2205 2206 2197
		f 4 2869 2862 2881 -2862
		mu 0 4 2192 2207 2208 2193
		f 4 2854 2901 -2847 -2851
		mu 0 4 2209 2210 2188 2187
		f 4 2885 2878 2865 -2878
		mu 0 4 2184 2211 2212 2185
		f 4 2831 -2897 2908 -2839
		mu 0 4 2213 2206 2205 2214
		f 4 2880 -2863 2870 -2873
		mu 0 4 2204 2208 2207 2201
		f 4 2900 -2855 -2831 -2889
		mu 0 4 2215 2210 2209 2216
		f 4 2864 -2879 2886 -2857
		mu 0 4 2217 2212 2211 2218
		f 4 2828 -2858 -2865 -2833
		mu 0 4 2219 2220 2212 2217
		f 4 -2866 2857 2848 -2859
		mu 0 4 2185 2212 2220 2221
		f 4 -2867 2858 2840 2833
		mu 0 4 2186 2185 2221 2222
		f 4 2905 -2840 -2861 -2894
		mu 0 4 2200 2195 2198 2191
		f 4 2845 -2869 2860 -2842
		mu 0 4 2197 2192 2191 2198
		f 4 2853 -2870 -2846 -2850
		mu 0 4 2206 2207 2192 2197
		f 4 -2871 -2854 -2832 -2864
		mu 0 4 2201 2207 2206 2213
		f 4 2838 2909 -2872 2863
		mu 0 4 2213 2214 2202 2201
		f 4 2830 -2874 -2881 -2837
		mu 0 4 2216 2209 2208 2204
		f 4 -2882 2873 2850 -2875
		mu 0 4 2193 2208 2209 2187
		f 4 -2883 2874 2842 2837
		mu 0 4 2194 2193 2187 2190
		f 4 2903 -2884 -2838 -2892
		mu 0 4 2189 2199 2194 2190
		f 4 2847 -2885 2876 -2844
		mu 0 4 2223 2184 2183 2224
		f 4 2855 -2886 -2848 -2852
		mu 0 4 2225 2211 2184 2223
		f 4 -2887 -2856 -2830 -2880
		mu 0 4 2218 2211 2225 2226
		f 4 -2900 2911 2888 2836
		mu 0 4 2204 2203 2215 2216
		f 4 2829 -2890 -2901 -2835
		mu 0 4 2227 2228 2210 2215
		f 4 -2902 2889 2851 -2891
		mu 0 4 2188 2210 2228 2229
		f 4 -2903 2890 2843 2835
		mu 0 4 2189 2188 2229 2230
		f 4 -2893 -2904 -2836 -2877
		mu 0 4 2231 2199 2189 2230
		f 4 2867 -2905 2892 -2860
		mu 0 4 2232 2200 2199 2231
		f 4 -2895 -2906 -2868 -2834
		mu 0 4 2233 2195 2200 2232
		f 4 2844 -2907 2894 -2841
		mu 0 4 2234 2196 2195 2233
		f 4 2852 -2908 -2845 -2849
		mu 0 4 2235 2205 2196 2234
		f 4 -2909 -2853 -2829 -2898
		mu 0 4 2214 2205 2235 2236
		f 4 -2910 2897 2832 -2899
		mu 0 4 2202 2214 2236 2237
		f 4 -2911 2898 2856 2887
		mu 0 4 2203 2202 2237 2238
		f 4 -2912 -2888 2879 2834
		mu 0 4 2215 2203 2238 2227;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pot_low_body";
	rename -uid "FF473ABA-45B8-8278-4BF3-A29A597CFB7B";
	setAttr ".t" -type "double3" 0 0 -3.4694469519536142e-018 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -5.3290705182007514e-015 0 0 ;
	setAttr ".sp" -type "double3" -5.3290705182007514e-015 0 0 ;
createNode mesh -n "pot_low_bodyShape" -p "pot_low_body";
	rename -uid "2E7B6B0B-4D54-CF32-E5DC-D28948DDAEBB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.46730396151542664 0.32285907864570618 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "pot_low_bodyShapeOrig" -p "pot_low_body";
	rename -uid "6D9B140C-4EB0-D9E3-3FDB-2A8DDBE36BB5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 252 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.6119557 0.06041193 0.61531961
		 0.22845852 0.57971674 0.22845852 0.65092272 0.22845852 0.68652552 0.22845852 0.57682681
		 0.060384393 0.012384772 0.22845829 0.64708567 0.060410976 0.045672655 0.22845852
		 0.081275702 0.22845829 0.11687851 0.22845829 0.15248156 0.22845852 0.01406467 0.060610414
		 0.1880846 0.22845852 0.22368741 0.22845852 0.050005674 0.060525894 0.25929034 0.22845829
		 0.29489338 0.22845852 0.085131645 0.060427547 0.33049631 0.22845829 0.366099 0.22845829
		 0.12025607 0.060320973 0.40170211 0.22845829 0.43730503 0.22845829 0.15537906 0.060210943
		 0.47290796 0.22845829 0.50851071 0.22845829 0.19050014 0.060109854 0.5441137 0.22845829
		 0.57971656 0.2408911 0.61531967 0.2408911 0.22561836 0.060026526 0.65092266 0.2408911
		 0.26073515 0.059967875 0.012221932 0.2408911 0.045672655 0.2408911 0.29585254 0.059938788
		 0.081275702 0.2408911 0.11687863 0.2408911 0.33096945 0.059940219 0.3660872 0.059973001
		 0.40120578 0.060027957 0.43632609 0.060102105 0.4714486 0.060183764 0.50657326 0.060263515
		 0.54169953 0.060332417 0.61123008 0.26230097 0.57621121 0.26230073 0.64624876 0.26230097
		 0.68126744 0.26230073 0.020077229 0.26230097 0.050930738 0.26230073 0.085949302 0.26230073
		 0.12096834 0.26230073 0.15248168 0.2408911 0.15598679 0.26230097 0.1880846 0.24089122
		 0.19100571 0.26230073 0.22368753 0.2408911 0.22602415 0.26230097 0.25929058 0.2408911
		 0.26104319 0.26230097 0.29489338 0.2408911 0.29606199 0.26230097 0.33049631 0.2408911
		 0.33108056 0.26230073 0.36609912 0.2408911 0.36609912 0.26230097 0.40170211 0.24089122
		 0.40111786 0.26230097 0.43730503 0.2408911 0.43613666 0.26230097 0.47290796 0.2408911
		 0.47115535 0.26230073 0.50851077 0.2408911 0.50617379 0.26230073 0.5441137 0.2408911
		 0.54119271 0.26230073 0.57745576 0.044565558 0.61268342 0.044617653 0.61269307 0.051942706
		 0.57746881 0.051889777 0.64790952 0.04461658 0.64791548 0.05194211 0.68313771 0.051886201
		 0.013673544 0.045014381 0.013671517 0.052340269 0.049036026 0.04486084 0.049031377
		 0.052185178 0.084266543 0.044672608 0.084260702 0.051997781 0.11949825 0.044470191
		 0.11949205 0.051795363 0.15472949 0.044264555 0.15472329 0.051590443 0.18995857 0.044077873
		 0.18995428 0.051402926 0.22518468 0.043923974 0.22518337 0.05124855 0.26040804 0.043814659
		 0.26041019 0.051140428 0.29563046 0.043759108 0.29563642 0.051084399 0.33085227 0.043758631
		 0.33086193 0.051083446 0.36607575 0.043811679 0.36608911 0.051137209 0.40130234 0.043909431
		 0.54224223 0.05179143 0.68221486 0.060381532 0.50701249 0.051661611 0.71849549 0.051776767
		 0.71815693 0.060321927 0.47178137 0.051513195 0.43654919 0.051364899 0.40131819 0.051234245
		 0.91829783 0.58867615 0.92133862 0.59464461 0.90097648 0.60126144 0.92238599 0.60126084
		 0.9135614 0.58393949 0.92133766 0.60787731 0.90759331 0.58089858 0.9182964 0.61384588
		 0.90097719 0.57985109 0.9135595 0.6185829 0.89436108 0.58089942 0.90759116 0.62162453
		 0.88839251 0.58394092 0.90097481 0.62267286 0.88388664 0.58849221 0.89435869 0.62162524
		 0.88080007 0.59454948 0.8883906 0.61858433 0.87956601 0.60126323 0.88365418 0.61384779
		 0.88061339 0.60787946 0.8823278 0.11915398 0.87189913 0.13962126 0.81250107 0.096465826
		 0.8556565 0.15586424 0.8859216 0.096465826 0.83518922 0.16629291 0.88232803 0.073777676
		 0.81250107 0.16988635 0.87189913 0.053310394 0.78981245 0.16629291 0.8556565 0.037067413
		 0.76934552 0.15586424 0.8351891 0.026638746 0.75310242 0.13962126 0.81250107 0.023045301
		 0.74267399 0.11915398 0.78981245 0.026638746 0.73908079 0.096465826 0.76934564 0.037067413
		 0.74267399 0.073777676 0.75310242 0.053310394 0.57745993 0.037019014 0.61268264 0.037023544
		 0.54222625 0.044465661 0.5422343 0.037011623 0.64790374 0.037026405 0.50699472 0.044336796
		 0.50700551 0.037005305 0.68313587 0.044561625 0.68312418 0.037028432 0.47176301 0.044186711
		 0.47177452 0.036996245 0.71849626 0.044451475 0.7179054 0.037029982 0.43653154 0.044041276
		 0.43654251 0.036989808 0.04901576 0.036978841 0.40131116 0.036981583 0.084243894
		 0.036975622 0.366081 0.036975265 0.11947417 0.036974072 0.33085203 0.036970139 0.15470552
		 0.036969781 0.29562426 0.036966562 0.18993747 0.036967039 0.26039624 0.03696537 0.22516775
		 0.036965847 0.68652564 0.2408911 0.61123043 0.26912391 0.57621199 0.26912391 0.54119343
		 0.26912427 0.64624894 0.26912391 0.50617474 0.26912427 0.68126702 0.26912391 0.4711563
		 0.26912463 0.02067852 0.26912355 0.71212101 0.26230073 0.43613762 0.2691251 0.050929904
		 0.26912391 0.40111858 0.2691251 0.085948467 0.26912427 0.36609948 0.26912546 0.12096739
		 0.26912427 0.33108068 0.26912522 0.15598595 0.26912463 0.29606199 0.26912546 0.19100487
		 0.2691251 0.26104283 0.26912522 0.22602367 0.26912522 0.57746291 0.024341464 0.61268955
		 0.024300098 0.64791459 0.024307489 0.014232159 0.036978126 0.013809919 0.023814917
		 0.04903841 0.023963809 0.084269047 0.024143338 0.18996215 0.024712682 0.22518897
		 0.024861932 0.26041329 0.024969459 0.29563606 0.02502811 0.33085907 0.025035262 0.36608326
		 0.024993539 0.40131015 0.024910092 0.43654001 0.024796605 0.47177136 0.024667263
		 0.50700331 0.024538279 0.54223412 0.024424911 0.61123067 0.28024948 0.57621247 0.2802496
		 0.64624876 0.28024924 0.68126673 0.28024948 0.015910745 0.280249 0.05092907 0.28024924
		 0.085947394 0.28024971 0.12096584 0.28025055 0.15598476 0.28025138 0.19100392 0.28025186
		 0.22602296 0.28025246 0.26104236 0.28025281 0.29606175 0.28025281 0.71981347 0.22845829
		 0.33108115 0.28025281 0.71997631 0.2408911 0.36610031 0.2802527 0.40111953 0.2802521
		 0.43613857 0.28025186 0.47115725 0.28025115 0.50617594 0.28025091 0.54119438 0.28025019
		 0.11799669 0.024701357 0.15393996 0.024899483 0.68313992 0.024365783 0.71836638 0.024473429;
	setAttr ".uvst[0].uvsp[250:251]" 0.71151859 0.26912427 0.71628481 0.28024971;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 202 ".vt";
	setAttr ".vt[0:165]"  29.66178131 0.13144255 -9.63833332 25.23219681 0.14734411 -18.33290291
		 18.33259583 0.15282583 -25.23276711 9.63835716 0.14734983 -29.66251755 0.00053772685 0.13145113 -31.18854141
		 -9.63744831 0.10668325 -29.66146469 -18.33216667 0.075478077 -25.2307663 -25.23251724 0.040879726 -18.3301487
		 -29.66304779 0.0062870979 -9.63509941 -31.19006729 -0.02492094 0.0032534413 -29.66410065 -0.049691677 9.64143944
		 -25.23451805 -0.065596104 18.33600426 -18.33492088 -0.071074963 25.23586655 -9.64068699 -0.065598965 29.66561508
		 -0.0028707192 -0.049694538 31.19164658 9.63511181 -0.024932384 29.66456413 18.32982635 0.0062727928 25.23386574
		 25.23017502 0.040871143 18.33325386 29.66070747 0.075463772 9.63820457 31.18772507 0.10667181 -0.0001447056
		 31.92889023 2.22132826 -10.37455177 27.16037941 2.22318792 -19.7332859 19.73324966 2.22353983 -27.16041946
		 10.37451172 2.2223525 -31.92892456 0.000275956 2.21974039 -33.57203674 -10.37396431 2.21595812 -31.92891121
		 -19.73270035 2.21137762 -27.16039085 -27.15983582 2.20644522 -19.73325157 -31.92835426 2.20164728 -10.37451363
		 -33.57146835 2.19745016 -0.00027538466 -31.9283638 2.1942687 10.37395954 -27.15985298 2.19240904 19.73269081
		 -19.73272514 2.19205713 27.15981483 -10.37399578 2.19324446 31.92832565 0.00023722996 2.19585943 33.57143402
		 10.37447071 2.1996417 31.92831039 19.7332077 2.2042222 27.15979195 27.16034126 2.20915174 19.73265839
		 31.9288559 2.21395254 10.37392235 33.57197952 2.2181468 -0.00031219432 39.90801239 59.43210983 -12.96689892
		 33.94778061 59.43210983 -24.66450882 24.664505 59.43210983 -33.94778061 12.96689606 59.43210983 -39.90800476
		 5.4213058e-007 59.43210983 -41.96175766 -12.9668951 59.43210983 -39.90800095 -24.66450119 59.43210983 -33.94777298
		 -33.94777298 59.43210983 -24.66449928 -39.90799713 59.43210983 -12.96689415 -41.96175003 59.43210983 -9.0025402e-008
		 -39.90799713 59.43210983 12.9668932 -33.94776535 59.43210983 24.66449356 -24.66449738 59.43210983 33.94776535
		 -12.9668932 59.43210983 39.90799332 -7.08416e-007 59.43210983 41.96174622 12.96688938 59.43210983 39.9079895
		 24.66449165 59.43210983 33.94776154 33.94776154 59.43210983 24.66449165 39.90798187 59.43210983 12.96688938
		 41.96173859 59.43210983 -9.0014524e-008 37.23395157 69.17619324 -12.098322868 31.67313004 69.1762619 -23.012052536
		 23.011943817 69.17628479 -31.67323875 12.098214149 69.1762619 -37.23405457 0.00025535165 69.17620087 -39.15018463
		 -12.097704887 69.17610168 -37.23405075 -23.011428833 69.17597198 -31.67323112 -31.67261124 69.17583466 -23.012044907
		 -37.23343277 69.1756897 -12.0983181 -39.14955521 69.17556 -0.00036193529 -37.23343277 69.17546082 12.097595215
		 -31.67260933 69.17539978 23.011320114 -23.011425018 69.17537689 31.6724968 -12.097702026 69.17539978 37.23331833
		 0.00025410802 69.17544556 39.14944458 12.098209381 69.17555237 37.23331451 23.011932373 69.17567444 31.67249489
		 31.67311287 69.17582703 23.011318207 37.23392868 69.17596436 12.097593307 39.15005875 69.17608643 -0.00036201224
		 29.66386604 1.57134962 -9.64117336 25.23428345 1.58725405 -18.33574486 18.33468246 1.59273005 -25.23560715
		 9.64044189 1.58725405 -29.66535568 0.0026229722 1.57135534 -31.19138908 -9.63536453 1.54658747 -29.66431046
		 -18.33008385 1.51538229 -25.23360634 -25.23043442 1.48078394 -18.33299065 -29.66096497 1.44619131 -9.63793945
		 -31.18798447 1.41498327 0.0004127748 -29.66201973 1.3902154 9.63859844 -25.23243332 1.37431097 18.33316422
		 -18.33283424 1.36882925 25.23302269 -9.63860226 1.37430525 29.66277885 -0.00078547484 1.39020681 31.18880081
		 9.63719559 1.41497469 29.66172409 18.3319149 1.44617701 25.23102188 25.23226357 1.48077536 18.33041382
		 29.66279793 1.51536798 9.63536453 31.18981171 1.54657888 -0.0029853727 27.74822617 0.12559748 -9.016465187
		 23.60439301 0.14047766 -17.15014839 17.14988518 0.14560747 -23.60490227 9.016511917 0.14047766 -27.74888992
		 0.00042762392 0.12560606 -29.17647362 -9.015812874 0.1024375 -27.74790573 -17.14963531 0.073243618 -23.60303116
		 -23.60484505 0.040879726 -17.14757156 -27.74956131 0.0085186958 -9.013438225 -29.17807388 -0.020678043 0.0031435196
		 -27.75054741 -0.043849468 9.019570351 -23.60671616 -0.058729649 17.15324974 -17.15221024 -0.063856602 23.60800362
		 -9.018841743 -0.058726788 27.75198936 -0.002760944 -0.043852329 29.17957497 9.013475418 -0.020683765 27.75100708
		 17.14729309 0.0085072517 23.60612869 23.60250473 0.040871143 17.15067673 27.74722099 0.073232174 9.016545296
		 29.17572975 0.10242891 -3.5413759e-005 27.34439659 2.56592989 -8.88574314 23.26096535 2.58059549 -16.90087318
		 2.4001165e-005 2.48236799 5.57643e-005 16.90053749 2.58564806 -23.26154327 8.88571358 2.58059835 -27.34512901
		 0.0010460938 2.56593847 -28.75190353 -8.88377571 2.54311037 -27.34415817 -16.89904213 2.51434278 -23.25969505
		 -28.75214958 2.42179155 0.0023975361 -27.345438 2.39895773 8.88740253 -23.26200485 2.38429213 16.90252495
		 -16.901577 2.37923956 23.26319313 -8.88676071 2.38429213 27.34677887 -0.0020959976 2.39894915 28.7535553
		 8.8827219 2.42178297 27.34581375 16.89798164 2.4505477 23.26135063 23.25910187 2.48243952 16.89999199
		 27.34340668 2.51433134 8.88442135 28.75109482 2.54310465 -0.00073506666 42.85258102 60.25673676 -13.92364311
		 36.45257568 60.25673676 -26.4843483 36.45257568 69.17582703 -26.4843483 42.85258102 69.17582703 -13.92364311
		 26.48434448 60.25673676 -36.45257187 26.48434448 69.17582703 -36.45257187 13.92364311 60.25673676 -42.85256577
		 13.92364311 69.17582703 -42.85256577 8.9669527e-007 60.25673676 -45.057857513 3.0286998e-007 69.17582703 -45.057857513
		 -13.92364216 60.25673676 -42.85256195 -13.92364216 69.17582703 -42.85256195 -26.48434067 60.25673676 -36.45256805
		 -26.48434067 69.17582703 -36.45256424 -36.45256424 60.25673676 -26.48434067 -36.45256424 69.17582703 -26.48434067
		 -42.85255814 60.25673676 -13.92363834 -42.85255814 69.17582703 -13.92363834 -45.057842255 60.25673676 4.5314988e-007
		 -45.057842255 69.17582703 1.2754506e-006 -42.85255814 60.2567482 13.9236393 -42.85255814 69.17583466 13.92364216
		 -36.45256042 60.2567482 26.48433876 -36.45256424 69.17583466 26.48433876 -26.48433685 60.2567482 36.45255661
		 -26.48433685 69.17583466 36.45256042 -13.92363834 60.2567482 42.85255432;
	setAttr ".vt[166:201]" -13.92363834 69.17583466 42.85255432 -4.4612125e-007 60.2567482 45.057842255
		 -1.0399466e-006 69.17583466 45.057842255 13.92363644 60.25673676 42.85254669 13.92363644 69.17582703 42.85255432
		 26.48433113 60.25673676 36.45255661 26.48433113 69.17582703 36.45255661 36.45255661 60.25673676 26.48433304
		 36.45255661 69.17582703 26.48433304 42.85254288 60.25673676 13.92363739 42.85254288 69.17582703 13.92363739
		 45.057834625 60.25673676 4.531615e-007 45.057834625 69.17582703 1.2754624e-006 35.63233185 65.39189148 -11.57810402
		 30.3107338 65.39199829 -22.022327423 0.00040177151 64.1700058 -0.00057053071 22.022151947 65.39204407 -30.31091118
		 11.57792854 65.39200592 -35.63250351 0.00041964703 65.39190674 -37.46619797 -11.57708931 65.39174652 -35.63250351
		 -22.021306992 65.39154816 -30.31090355 -30.30988503 65.39131927 -22.022321701 -35.63148499 65.39109039 -11.57810116
		 -37.46517181 65.3908844 -0.0005950979 -35.63148499 65.39072418 11.57691097 -30.30988312 65.390625 22.021129608
		 -22.021303177 65.39057922 30.30970383 -11.5770874 65.39060974 35.63129807 0.00041840752 65.39071655 37.46500015
		 11.57792091 65.39087677 35.63129807 22.022138596 65.3910675 30.30970192 30.31071854 65.39129639 22.021125793
		 35.63230896 65.39152527 11.57690811 37.46600723 65.39173126 -0.00059522089 -22.94992447 2.41082525 -17.14779663
		 -27.094638824 2.37846422 -9.013664246;
	setAttr -s 420 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 1 21 22 1 22 23 1 23 24 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 29 30 1 30 31 1
		 31 32 1 32 33 1 33 34 1 34 35 1 35 36 1 36 37 1 37 38 1 38 39 1 39 20 1 40 41 0 41 42 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 40 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 60 0 0 80 1 1 81 1 2 82 1 3 83 1 4 84 1 5 85 1
		 6 86 1 7 87 1 8 88 1 9 89 1 10 90 1 11 91 1 12 92 1 13 93 1 14 94 1 15 95 1 16 96 1
		 17 97 1 18 98 1 19 99 1 20 40 1 21 41 1 22 42 1 23 43 1 24 44 1 25 45 1 26 46 1 27 47 1
		 28 48 1 29 49 1 30 50 1 31 51 1 32 52 1 33 53 1 34 54 1 35 55 1 36 56 1 37 57 1 38 58 1
		 39 59 1 80 20 1 81 21 1 82 22 1 83 23 1 84 24 1 85 25 1 86 26 1 87 27 1 88 28 1 89 29 1
		 90 30 1 91 31 1 92 32 1 93 33 1 94 34 1 95 35 1 96 36 1 97 37 1 98 38 1 99 39 1 80 81 1
		 81 82 1 82 83 1 83 84 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1 90 91 1 91 92 1
		 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 80 1 0 100 0 1 101 0 100 101 0
		 2 102 0 101 102 0 3 103 0;
	setAttr ".ed[166:331]" 102 103 0 4 104 0 103 104 0 5 105 0 104 105 0 6 106 0
		 105 106 0 7 107 0 106 107 0 8 108 0 107 108 0 9 109 0 108 109 0 10 110 0 109 110 0
		 11 111 0 110 111 0 12 112 0 111 112 0 13 113 0 112 113 0 14 114 0 113 114 0 15 115 0
		 114 115 0 16 116 0 115 116 0 17 117 0 116 117 0 18 118 0 117 118 0 19 119 0 118 119 0
		 119 100 0 100 120 0 101 121 0 120 121 0 122 120 1 122 121 1 102 123 0 121 123 0 122 123 1
		 103 124 0 123 124 0 122 124 1 104 125 0 124 125 0 122 125 1 105 126 0 125 126 0 122 126 1
		 106 127 0 126 127 0 122 127 0 109 128 0 122 128 0 110 129 0 128 129 0 122 129 1 111 130 0
		 129 130 0 122 130 1 112 131 0 130 131 0 122 131 1 113 132 0 131 132 0 122 132 1 114 133 0
		 132 133 0 122 133 1 115 134 0 133 134 0 122 134 1 116 135 0 134 135 0 122 135 1 117 136 0
		 135 136 0 122 136 1 118 137 0 136 137 0 122 137 1 119 138 0 137 138 0 122 138 1 138 120 0
		 40 139 0 41 140 0 139 140 0 61 141 0 140 141 1 60 142 0 142 141 0 139 142 1 42 143 0
		 140 143 0 62 144 0 143 144 1 141 144 0 43 145 0 143 145 0 63 146 0 145 146 1 144 146 0
		 44 147 0 145 147 0 64 148 0 147 148 1 146 148 0 45 149 0 147 149 0 65 150 0 149 150 1
		 148 150 0 46 151 0 149 151 0 66 152 0 151 152 1 150 152 0 47 153 0 151 153 0 67 154 0
		 153 154 1 152 154 0 48 155 0 153 155 0 68 156 0 155 156 1 154 156 0 49 157 0 155 157 0
		 69 158 0 157 158 1 156 158 0 50 159 0 157 159 0 70 160 0 159 160 1 158 160 0 51 161 0
		 159 161 0 71 162 0 161 162 1 160 162 0 52 163 0 161 163 0 72 164 0 163 164 1 162 164 0
		 53 165 0 163 165 0 73 166 0 165 166 1 164 166 0 54 167 0 165 167 0 74 168 0 167 168 1
		 166 168 0 55 169 0 167 169 0 75 170 0 169 170 1 168 170 0 56 171 0;
	setAttr ".ed[332:419]" 169 171 0 76 172 0 171 172 1 170 172 0 57 173 0 171 173 0
		 77 174 0 173 174 1 172 174 0 58 175 0 173 175 0 78 176 0 175 176 1 174 176 0 59 177 0
		 175 177 0 79 178 0 177 178 1 176 178 0 177 139 0 178 142 0 60 179 0 61 180 0 179 180 0
		 180 181 1 179 181 1 62 182 0 180 182 0 182 181 1 63 183 0 182 183 0 183 181 1 64 184 0
		 183 184 0 184 181 1 65 185 0 184 185 0 185 181 1 66 186 0 185 186 0 186 181 1 67 187 0
		 186 187 0 187 181 1 68 188 0 187 188 0 188 181 1 69 189 0 188 189 0 189 181 1 70 190 0
		 189 190 0 190 181 1 71 191 0 190 191 0 191 181 1 72 192 0 191 192 0 192 181 1 73 193 0
		 192 193 0 193 181 1 74 194 0 193 194 0 194 181 1 75 195 0 194 195 0 195 181 1 76 196 0
		 195 196 0 196 181 1 77 197 0 196 197 0 197 181 1 78 198 0 197 198 0 198 181 1 79 199 0
		 198 199 0 199 181 1 199 179 0 107 200 0 127 200 0 108 201 0 200 201 0 201 128 0 201 122 0
		 122 200 0;
	setAttr -s 220 -ch 840 ".fc[0:219]" -type "polyFaces" 
		f 4 140 121 -21 -121
		mu 0 4 81 80 0 5
		f 4 141 122 -22 -122
		mu 0 4 80 83 7 0
		f 4 142 123 -23 -123
		mu 0 4 83 84 109 7
		f 4 143 124 -24 -124
		mu 0 4 84 111 112 109
		f 4 144 125 -25 -125
		mu 0 4 86 88 15 12
		f 4 145 126 -26 -126
		mu 0 4 88 90 18 15
		f 4 146 127 -27 -127
		mu 0 4 90 92 21 18
		f 4 147 128 -28 -128
		mu 0 4 92 94 24 21
		f 4 148 129 -29 -129
		mu 0 4 94 96 27 24
		f 4 149 130 -30 -130
		mu 0 4 96 98 31 27
		f 4 150 131 -31 -131
		mu 0 4 98 100 33 31
		f 4 151 132 -32 -132
		mu 0 4 100 102 36 33
		f 4 152 133 -33 -133
		mu 0 4 102 104 39 36
		f 4 153 134 -34 -134
		mu 0 4 104 106 40 39
		f 4 154 135 -35 -135
		mu 0 4 106 115 41 40
		f 4 155 136 -36 -136
		mu 0 4 115 114 42 41
		f 4 156 137 -37 -137
		mu 0 4 114 113 43 42
		f 4 157 138 -38 -138
		mu 0 4 113 110 44 43
		f 4 158 139 -39 -139
		mu 0 4 110 108 45 44
		f 4 159 120 -40 -140
		mu 0 4 108 81 5 45
		f 4 101 -41 -101 20
		mu 0 4 0 1 2 5
		f 4 102 -42 -102 21
		mu 0 4 7 3 1 0
		f 4 103 -43 -103 22
		mu 0 4 109 4 3 7
		f 4 104 -44 -104 23
		mu 0 4 112 237 4 109
		f 4 105 -45 -105 24
		mu 0 4 15 8 6 12
		f 4 106 -46 -106 25
		mu 0 4 18 9 8 15
		f 4 107 -47 -107 26
		mu 0 4 21 10 9 18
		f 4 108 -48 -108 27
		mu 0 4 24 11 10 21
		f 4 109 -49 -109 28
		mu 0 4 27 13 11 24
		f 4 110 -50 -110 29
		mu 0 4 31 14 13 27
		f 4 111 -51 -111 30
		mu 0 4 33 16 14 31
		f 4 112 -52 -112 31
		mu 0 4 36 17 16 33
		f 4 113 -53 -113 32
		mu 0 4 39 19 17 36
		f 4 114 -54 -114 33
		mu 0 4 40 20 19 39
		f 4 115 -55 -115 34
		mu 0 4 41 22 20 40
		f 4 116 -56 -116 35
		mu 0 4 42 23 22 41
		f 4 117 -57 -117 36
		mu 0 4 43 25 23 42
		f 4 118 -58 -118 37
		mu 0 4 44 26 25 43
		f 4 119 -59 -119 38
		mu 0 4 45 28 26 44
		f 4 100 -60 -120 39
		mu 0 4 5 2 28 45
		f 4 255 257 -260 -261
		mu 0 4 29 30 46 47
		f 4 262 264 -266 -258
		mu 0 4 30 32 48 46
		f 4 267 269 -271 -265
		mu 0 4 32 184 49 48
		f 4 272 274 -276 -270
		mu 0 4 184 239 193 49
		f 4 277 279 -281 -275
		mu 0 4 34 35 51 50
		f 4 282 284 -286 -280
		mu 0 4 35 37 52 51
		f 4 287 289 -291 -285
		mu 0 4 37 38 53 52
		f 4 292 294 -296 -290
		mu 0 4 38 54 55 53
		f 4 297 299 -301 -295
		mu 0 4 54 56 57 55
		f 4 302 304 -306 -300
		mu 0 4 56 58 59 57
		f 4 307 309 -311 -305
		mu 0 4 58 60 61 59
		f 4 312 314 -316 -310
		mu 0 4 60 62 63 61
		f 4 317 319 -321 -315
		mu 0 4 62 64 65 63
		f 4 322 324 -326 -320
		mu 0 4 64 66 67 65
		f 4 327 329 -331 -325
		mu 0 4 66 68 69 67
		f 4 332 334 -336 -330
		mu 0 4 68 70 71 69
		f 4 337 339 -341 -335
		mu 0 4 70 72 73 71
		f 4 342 344 -346 -340
		mu 0 4 72 74 75 73
		f 4 347 349 -351 -345
		mu 0 4 74 76 77 75
		f 4 351 260 -353 -350
		mu 0 4 76 29 47 77
		f 3 -203 -204 204
		mu 0 3 116 117 118
		f 3 -207 -205 207
		mu 0 3 120 116 118
		f 3 -210 -208 210
		mu 0 3 122 120 118
		f 3 -213 -211 213
		mu 0 3 124 122 118
		f 3 -216 -214 216
		mu 0 3 126 124 118
		f 3 -219 -217 219
		mu 0 3 128 126 118
		f 3 -224 -222 224
		mu 0 3 136 134 118
		f 3 -227 -225 227
		mu 0 3 135 136 118
		f 3 -230 -228 230
		mu 0 3 133 135 118
		f 3 -233 -231 233
		mu 0 3 131 133 118
		f 3 -236 -234 236
		mu 0 3 129 131 118
		f 3 -239 -237 239
		mu 0 3 127 129 118
		f 3 -242 -240 242
		mu 0 3 125 127 118
		f 3 -245 -243 245
		mu 0 3 123 125 118
		f 3 -248 -246 248
		mu 0 3 121 123 118
		f 3 -251 -249 251
		mu 0 3 119 121 118
		f 3 -253 -252 203
		mu 0 3 117 119 118
		f 3 355 356 -358
		mu 0 3 137 138 139
		f 3 359 360 -357
		mu 0 3 138 140 139
		f 3 362 363 -361
		mu 0 3 140 142 139
		f 3 365 366 -364
		mu 0 3 142 144 139
		f 3 368 369 -367
		mu 0 3 144 146 139
		f 3 371 372 -370
		mu 0 3 146 148 139
		f 3 374 375 -373
		mu 0 3 148 150 139
		f 3 377 378 -376
		mu 0 3 150 152 139
		f 3 380 381 -379
		mu 0 3 152 154 139
		f 3 383 384 -382
		mu 0 3 154 156 139
		f 3 386 387 -385
		mu 0 3 156 157 139
		f 3 389 390 -388
		mu 0 3 157 155 139
		f 3 392 393 -391
		mu 0 3 155 153 139
		f 3 395 396 -394
		mu 0 3 153 151 139
		f 3 398 399 -397
		mu 0 3 151 149 139
		f 3 401 402 -400
		mu 0 3 149 147 139
		f 3 404 405 -403
		mu 0 3 147 145 139
		f 3 407 408 -406
		mu 0 3 145 143 139
		f 3 410 411 -409
		mu 0 3 143 141 139
		f 3 412 357 -412
		mu 0 3 141 137 139
		f 4 0 81 -141 -81
		mu 0 4 78 79 80 81
		f 4 1 82 -142 -82
		mu 0 4 79 82 83 80
		f 4 2 83 -143 -83
		mu 0 4 82 165 84 83
		f 4 3 84 -144 -84
		mu 0 4 165 169 111 84
		f 4 4 85 -145 -85
		mu 0 4 85 87 88 86
		f 4 5 86 -146 -86
		mu 0 4 87 89 90 88
		f 4 6 87 -147 -87
		mu 0 4 89 91 92 90
		f 4 7 88 -148 -88
		mu 0 4 91 93 94 92
		f 4 8 89 -149 -89
		mu 0 4 93 95 96 94
		f 4 9 90 -150 -90
		mu 0 4 95 97 98 96
		f 4 10 91 -151 -91
		mu 0 4 97 99 100 98
		f 4 11 92 -152 -92
		mu 0 4 99 101 102 100
		f 4 12 93 -153 -93
		mu 0 4 101 103 104 102
		f 4 13 94 -154 -94
		mu 0 4 103 105 106 104
		f 4 14 95 -155 -95
		mu 0 4 105 107 115 106
		f 4 15 96 -156 -96
		mu 0 4 107 171 114 115
		f 4 16 97 -157 -97
		mu 0 4 171 167 113 114
		f 4 17 98 -158 -98
		mu 0 4 167 163 110 113
		f 4 18 99 -159 -99
		mu 0 4 163 160 108 110
		f 4 19 80 -160 -100
		mu 0 4 160 78 81 108
		f 4 -1 160 162 -162
		mu 0 4 79 78 158 159
		f 4 -2 161 164 -164
		mu 0 4 82 79 159 162
		f 4 -3 163 166 -166
		mu 0 4 165 82 162 166
		f 4 -4 165 168 -168
		mu 0 4 169 165 166 170
		f 4 -5 167 170 -170
		mu 0 4 87 85 209 173
		f 4 -6 169 172 -172
		mu 0 4 89 87 173 175
		f 4 -7 171 174 -174
		mu 0 4 91 89 175 177
		f 4 -8 173 176 -176
		mu 0 4 93 91 177 179
		f 4 -9 175 178 -178
		mu 0 4 95 93 179 181
		f 4 -10 177 180 -180
		mu 0 4 97 95 181 183
		f 4 -11 179 182 -182
		mu 0 4 99 97 183 182
		f 4 -12 181 184 -184
		mu 0 4 101 99 182 180
		f 4 -13 183 186 -186
		mu 0 4 103 101 180 178
		f 4 -14 185 188 -188
		mu 0 4 105 103 178 176
		f 4 -15 187 190 -190
		mu 0 4 107 105 176 174
		f 4 -16 189 192 -192
		mu 0 4 171 107 174 172
		f 4 -17 191 194 -194
		mu 0 4 167 171 172 168
		f 4 -18 193 196 -196
		mu 0 4 163 167 168 164
		f 4 -19 195 198 -198
		mu 0 4 160 163 164 161
		f 4 -20 197 199 -161
		mu 0 4 78 160 161 158
		f 4 -163 200 202 -202
		mu 0 4 159 158 206 207
		f 4 -165 201 206 -206
		mu 0 4 162 159 207 208
		f 4 -167 205 209 -209
		mu 0 4 166 162 208 248
		f 4 -169 208 212 -212
		mu 0 4 170 166 248 249
		f 4 -171 211 215 -215
		mu 0 4 173 209 210 211
		f 4 -173 214 218 -218
		mu 0 4 175 173 211 212
		f 4 -181 220 223 -223
		mu 0 4 183 181 213 214
		f 4 -183 222 226 -226
		mu 0 4 182 183 214 215
		f 4 -185 225 229 -229
		mu 0 4 180 182 215 216
		f 4 -187 228 232 -232
		mu 0 4 178 180 216 217
		f 4 -189 231 235 -235
		mu 0 4 176 178 217 218
		f 4 -191 234 238 -238
		mu 0 4 174 176 218 219
		f 4 -193 237 241 -241
		mu 0 4 172 174 219 220
		f 4 -195 240 244 -244
		mu 0 4 168 172 220 221
		f 4 -197 243 247 -247
		mu 0 4 164 168 221 222
		f 4 -199 246 250 -250
		mu 0 4 161 164 222 223
		f 4 -200 249 252 -201
		mu 0 4 158 161 223 206
		f 4 40 254 -256 -254
		mu 0 4 2 1 30 29
		f 4 -61 258 259 -257
		mu 0 4 185 186 47 46
		f 4 41 261 -263 -255
		mu 0 4 1 3 32 30
		f 4 -62 256 265 -264
		mu 0 4 188 185 46 48
		f 4 42 266 -268 -262
		mu 0 4 3 4 184 32
		f 4 -63 263 270 -269
		mu 0 4 190 188 48 49
		f 4 43 271 -273 -267
		mu 0 4 4 237 239 184
		f 4 -64 268 275 -274
		mu 0 4 250 190 49 193
		f 4 44 276 -278 -272
		mu 0 4 6 8 35 34
		f 4 -65 273 280 -279
		mu 0 4 195 192 50 51
		f 4 45 281 -283 -277
		mu 0 4 8 9 37 35
		f 4 -66 278 285 -284
		mu 0 4 197 195 51 52
		f 4 46 286 -288 -282
		mu 0 4 9 10 38 37
		f 4 -67 283 290 -289
		mu 0 4 199 197 52 53
		f 4 47 291 -293 -287
		mu 0 4 10 11 54 38
		f 4 -68 288 295 -294
		mu 0 4 201 199 53 55
		f 4 48 296 -298 -292
		mu 0 4 11 13 56 54
		f 4 -69 293 300 -299
		mu 0 4 203 201 55 57
		f 4 49 301 -303 -297
		mu 0 4 13 14 58 56
		f 4 -70 298 305 -304
		mu 0 4 205 203 57 59
		f 4 50 306 -308 -302
		mu 0 4 14 16 60 58
		f 4 -71 303 310 -309
		mu 0 4 204 205 59 61
		f 4 51 311 -313 -307
		mu 0 4 16 17 62 60
		f 4 -72 308 315 -314
		mu 0 4 202 204 61 63
		f 4 52 316 -318 -312
		mu 0 4 17 19 64 62
		f 4 -73 313 320 -319
		mu 0 4 200 202 63 65
		f 4 53 321 -323 -317
		mu 0 4 19 20 66 64
		f 4 -74 318 325 -324
		mu 0 4 198 200 65 67
		f 4 54 326 -328 -322
		mu 0 4 20 22 68 66
		f 4 -75 323 330 -329
		mu 0 4 196 198 67 69
		f 4 55 331 -333 -327
		mu 0 4 22 23 70 68
		f 4 -76 328 335 -334
		mu 0 4 194 196 69 71
		f 4 56 336 -338 -332
		mu 0 4 23 25 72 70
		f 4 -77 333 340 -339
		mu 0 4 191 194 71 73
		f 4 57 341 -343 -337
		mu 0 4 25 26 74 72
		f 4 -78 338 345 -344
		mu 0 4 189 191 73 75
		f 4 58 346 -348 -342
		mu 0 4 26 28 76 74
		f 4 -79 343 350 -349
		mu 0 4 187 189 75 77
		f 4 59 253 -352 -347
		mu 0 4 28 2 29 76
		f 4 -80 348 352 -259
		mu 0 4 186 187 77 47
		f 4 60 354 -356 -354
		mu 0 4 186 185 224 225
		f 4 61 358 -360 -355
		mu 0 4 185 188 226 224
		f 4 62 361 -363 -359
		mu 0 4 188 190 227 226
		f 4 63 364 -366 -362
		mu 0 4 190 250 251 227
		f 4 64 367 -369 -365
		mu 0 4 192 195 229 228
		f 4 65 370 -372 -368
		mu 0 4 195 197 230 229
		f 4 66 373 -375 -371
		mu 0 4 197 199 231 230
		f 4 67 376 -378 -374
		mu 0 4 199 201 232 231
		f 4 68 379 -381 -377
		mu 0 4 201 203 233 232
		f 4 69 382 -384 -380
		mu 0 4 203 205 234 233
		f 4 70 385 -387 -383
		mu 0 4 205 204 235 234
		f 4 71 388 -390 -386
		mu 0 4 204 202 236 235
		f 4 72 391 -393 -389
		mu 0 4 202 200 238 236
		f 4 73 394 -396 -392
		mu 0 4 200 198 240 238
		f 4 74 397 -399 -395
		mu 0 4 198 196 241 240
		f 4 75 400 -402 -398
		mu 0 4 196 194 242 241
		f 4 76 403 -405 -401
		mu 0 4 194 191 243 242
		f 4 77 406 -408 -404
		mu 0 4 191 189 244 243
		f 4 78 409 -411 -407
		mu 0 4 189 187 245 244
		f 4 79 353 -413 -410
		mu 0 4 187 186 225 245
		f 4 -175 217 414 -414
		mu 0 4 177 175 212 246
		f 4 -177 413 416 -416
		mu 0 4 179 177 246 247
		f 4 -179 415 417 -221
		mu 0 4 181 179 247 213
		f 3 221 -418 418
		mu 0 3 118 134 132
		f 3 -220 419 -415
		mu 0 3 128 118 130
		f 3 -420 -419 -417
		mu 0 3 130 118 132;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "3605D5DB-41C6-7888-B22E-5BA28D04EF39";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "0209DC83-4C53-5AD0-7F4D-15B29BD7CFA9";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "BDABBB16-44A0-08AC-2BDE-AF8AA683F00F";
createNode displayLayerManager -n "layerManager";
	rename -uid "C1A50BC9-48DE-CD23-4125-63896A2BC5D1";
	setAttr ".cdl" 4;
	setAttr -s 8 ".dli[1:7]"  8 1 3 2 5 6 4;
	setAttr -s 5 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "588CB6A9-4BB2-B28C-AC98-26A82E17C6A1";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "66C0AC27-4A7B-2C7B-EE03-80A17EA64880";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "AAF15044-434C-FDF3-6E4A-01812EEFBA80";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3FC1DA49-4184-12E9-EC7C-01832A53CC46";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 0\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 409\n                -height 396\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 0\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 409\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 0\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 409\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 0\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 409\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 0\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 409\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 0\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 409\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 0\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1517\n                -height 836\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 0\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 0\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1517\n            -height 836\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n"
		+ "                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n"
		+ "            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n"
		+ "                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n"
		+ "            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n"
		+ "                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n"
		+ "            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n"
		+ "            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n"
		+ "                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n"
		+ "            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n"
		+ "                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n"
		+ "                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n"
		+ "            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n"
		+ "            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n"
		+ "                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n"
		+ "            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n"
		+ "            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n"
		+ "                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 0\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1517\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 0\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1517\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 300 -size 1000 -divisions 3 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "14807440-4322-3003-B3AE-8DADFDA15029";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 7 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode lambert -n "Cage1";
	rename -uid "E1133A86-4C84-34D6-4EB8-A7BFF1152CA4";
	setAttr ".c" -type "float3" 0.6631 0 0 ;
	setAttr ".it" -type "float3" 0.51298702 0.51298702 0.51298702 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "F2291D9D-481A-C5E8-BCDA-DAB272FD41EE";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "9E639591-4E37-3709-3D6C-5CB0608F34BE";
createNode phong -n "Mat_Leg_Arm_Face";
	rename -uid "84C8C167-4771-A725-54D7-2285CA55E52B";
createNode shadingEngine -n "phong1SG";
	rename -uid "92BC4321-4C20-4229-06C4-A4AE73EB020A";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "C56CF78A-41FD-7BB9-DA3E-F7BE3E51E160";
createNode file -n "file1";
	rename -uid "C44F2792-43CC-3348-64CC-D8AAE54C53B7";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "C:/Users/andrew/Documents/capstone-2016-17-t18/Maya Files/Characters//data/MainChar_Normals_normals.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "907FBB15-41C5-92EA-E83B-C7A585219696";
createNode bump2d -n "bump2d1";
	rename -uid "7538C22A-4167-BDA3-E1AD-67AF4F0E31C3";
	setAttr ".bi" 1;
	setAttr ".vc2" -type "float3" 9.9999997e-006 9.9999997e-006 0 ;
createNode lambert -n "Mat_Body";
	rename -uid "C976DECB-4D6E-F352-0DD8-4982A7318AC3";
createNode shadingEngine -n "lambert3SG";
	rename -uid "414C83CB-4502-9718-75CC-0F8C1A3DE566";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "D28E62F1-4DBA-A301-B5AE-BB80784989D1";
createNode phongE -n "Main_Character_Mat";
	rename -uid "9B1862B7-4A31-0325-C849-698AA878383F";
createNode shadingEngine -n "phongE1SG";
	rename -uid "EAE7DC71-40D8-AD7E-E9D3-5FA6DE155A72";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "6835A51A-4DD0-9527-26F7-838A9F379320";
createNode file -n "file2";
	rename -uid "92A1772B-4C51-55CC-DFB1-A3AB1DDCADF9";
	setAttr ".ftn" -type "string" "C:/Users/andrew/Documents/capstone-2016-17-t18/Maya Files/Characters//sourceimages/MCharacter_Color.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture2";
	rename -uid "01A20A78-48EF-4E0F-ECD8-E39080C5577D";
createNode file -n "file3";
	rename -uid "D6F2A973-4151-69EA-3E23-C9813F74784C";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "C:/Users/andrew/Documents/capstone-2016-17-t18/Maya Files/Characters//sourceimages/MCharacter_Normals.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture3";
	rename -uid "09B231C5-41B4-D7E2-5AA7-58880E7B871C";
createNode bump2d -n "bump2d2";
	rename -uid "A4DE29EA-42D3-9F88-A28C-2F80CD866050";
	setAttr ".bi" 1;
	setAttr ".vc1" -type "float3" 0 9.9999997e-006 0 ;
	setAttr ".vc2" -type "float3" 9.9999997e-006 9.9999997e-006 0 ;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "4C1424EB-4B8D-D03D-AF2A-079E7847EBA8";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -1604.2340634300601 127.2926349535866 ;
	setAttr ".tgi[0].vh" -type "double2" 1382.5041007681655 366.85663766172672 ;
	setAttr -s 13 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -197.40757751464844;
	setAttr ".tgi[0].ni[0].y" 30.403230667114258;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" -202.22856140136719;
	setAttr ".tgi[0].ni[1].y" 229.89703369140625;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" 215.81268310546875;
	setAttr ".tgi[0].ni[2].y" 128.57649230957031;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 358.41925048828125;
	setAttr ".tgi[0].ni[3].y" 271.02578735351562;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" -851.55133056640625;
	setAttr ".tgi[0].ni[4].y" 134.61952209472656;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" 354.28570556640625;
	setAttr ".tgi[0].ni[5].y" 22.857143402099609;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" -195.93038940429687;
	setAttr ".tgi[0].ni[6].y" 126.35247802734375;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" 661.4285888671875;
	setAttr ".tgi[0].ni[7].y" 22.857143402099609;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" 82.277809143066406;
	setAttr ".tgi[0].ni[8].y" 39.209617614746094;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" -544.40850830078125;
	setAttr ".tgi[0].ni[9].y" 134.61952209472656;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" 90.544853210449219;
	setAttr ".tgi[0].ni[10].y" 215.04161071777344;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" 661.4285888671875;
	setAttr ".tgi[0].ni[11].y" 136.84353637695312;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" 628.36041259765625;
	setAttr ".tgi[0].ni[12].y" 244.15789794921875;
	setAttr ".tgi[0].ni[12].nvs" 18304;
createNode displayLayer -n "___MESH___";
	rename -uid "B16C06A2-44E7-4223-8C80-76966D0F7002";
	setAttr ".c" 16;
	setAttr ".do" 4;
createNode displayLayer -n "___CTRLS___";
	rename -uid "333E4F49-4E94-A6F4-7D4E-57BC0EF4CB4A";
	setAttr ".c" 17;
	setAttr ".do" 1;
createNode displayLayer -n "___JNTS___";
	rename -uid "8EE83C8F-41CF-4DAE-FCB0-77A4818D60B3";
	setAttr ".c" 9;
	setAttr ".do" 2;
createNode displayLayer -n "Spikes";
	rename -uid "54201F59-4EF0-FAE7-9E8B-B6AEB59BEC09";
	setAttr ".c" 6;
	setAttr ".do" 3;
createNode ikSplineSolver -n "ikSplineSolver";
	rename -uid "78CF5DED-41B5-3224-FD5D-D5A689215071";
createNode cluster -n "upper_torso_cstrCluster";
	rename -uid "A987574D-427E-0E1C-932D-43A149CEAE42";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode tweak -n "tweak1";
	rename -uid "0497BC65-4290-6ED7-82BA-F68B051F8776";
createNode cluster -n "lower_back_cstrCluster";
	rename -uid "E143EB9D-4F23-7C94-CED7-73B9BF7AFD9A";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode cluster -n "mid_back_clstrCluster";
	rename -uid "886175BC-4FA8-3F60-A3FD-2C8C08BA600D";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -1.0148019021405438e-014 0 0 1;
createNode animCurveTA -n "ctrl_pot_rotateX";
	rename -uid "AEAD7CE0-4F64-3B71-6E56-ED9ECCA8AD8F";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "ctrl_pot_rotateY";
	rename -uid "4BC9701A-4DFA-3365-61BD-74B256C9E364";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "ctrl_pot_rotateZ";
	rename -uid "3DB6094C-40E6-18C2-00AA-A9B33A00AB0E";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "ctrl_pot_visibility";
	rename -uid "EF15AAB6-4622-0667-4032-B3BEAFE7F504";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "ctrl_pot_translateX";
	rename -uid "A34D0C5B-4374-92DE-01F9-9E9B2F53F4E5";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0.57230964874220991;
createNode animCurveTL -n "ctrl_pot_translateY";
	rename -uid "B6E5F528-455D-2871-0FC2-6BA2D8713825";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 -1.3926860721763035;
createNode animCurveTL -n "ctrl_pot_translateZ";
	rename -uid "F0141FC8-4FD0-7545-B3F9-47B3147DEB41";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 -0.26257625752009517;
createNode animCurveTU -n "ctrl_pot_scaleX";
	rename -uid "49F6FD63-436B-6BB4-8506-3298593D220F";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "ctrl_pot_scaleY";
	rename -uid "817CB8E8-451C-BA54-B5D1-D1A4E37E8CE4";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "ctrl_pot_scaleZ";
	rename -uid "ACD73C63-4B3E-FE97-87C4-389BB735431E";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 1;
createNode objectSet -n "tweakSet1";
	rename -uid "CFDF0334-404A-4589-3086-83A176403877";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId3";
	rename -uid "1D9F0118-4F47-6BA9-75CC-E292C831D6C4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "B50445CC-45ED-B5CF-06A2-3989E3125C99";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode objectSet -n "cluster3Set";
	rename -uid "10D22FEE-4B66-362C-D0E5-169FB34B4C3C";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster3GroupId";
	rename -uid "634F21C0-45F0-59E6-FB8D-67B4C72BE4A2";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster3GroupParts";
	rename -uid "A5559A65-462C-1FA9-C406-BB9FB49955BD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode objectSet -n "cluster2Set";
	rename -uid "856C236E-42B2-D985-105E-2ABEF3B0DC38";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster2GroupId";
	rename -uid "3751F5CB-486F-DB30-B1A9-5697188FFF3C";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster2GroupParts";
	rename -uid "1BCE9B8C-4543-ECAC-537A-47850B88614B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0:1]";
createNode objectSet -n "cluster1Set";
	rename -uid "EAB9F756-44A3-EA5C-B84F-4AAEE67B79DF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster1GroupId";
	rename -uid "6A30B937-4DD5-8BF8-D89B-A69A1675CBB8";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster1GroupParts";
	rename -uid "86BFF347-4E2E-C050-7C53-159454F4657A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3:4]";
createNode dagPose -n "bindPose1";
	rename -uid "54703A88-40E3-8FCC-EA15-E5AC1C2947C6";
	setAttr -s 15 ".wm";
	setAttr -s 15 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.57230964874220991 -1.3926860721763035
		 -0.26257625752009517 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.57230964874220991 41.745146629677954
		 0.26257625752009517 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0.0035067595773902644 0 -1.0148019021405439e-014
		 13.813601289431418 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 -0.0072251349044211875 0 0
		 43.262503942680354 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 40.800410222365215 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 5.5511151231257839e-017 -5.5511151231257839e-017
		 -1.1102230246251568e-016 0 0 38.561419684426539 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 -0.68931787870306671 -0.58514850941020724 0.32562878679270191 0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.5600364406079656e-014
		 38.050047542593475 -7.6882152712063823e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 -4.9238195782919192 8.1308569417116949
		 37.109166324320405 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 -8.772693246739399 26.885707230811629
		 20.641875523607148 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 -6.401129090982782e-022 -3.9595711295476982e-019
		 0.0032332408259299222 0 29.998887665505102 1.5171130899355205 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0.70710678118654768 0.70710678118654746 4.3297802811774658e-017 4.3297802811774677e-017 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 -1.2200931131985419e-016 1.2246383329808589e-016
		 -7.4708639811444435e-033 0 -2.8421709430404007e-014 38 4.6536578367599419e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 -5.2180482157382334e-015
		 47 -1.0436096431476475e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.70710678118654768 0.70710678118654746 4.3297802811774658e-017 4.3297802811774677e-017 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 -1.1102230246251565e-016 0 -30.000000000000021
		 1.7490665905603748 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.70926137704704417 0.70494560004962847 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 2.931682674400804e-016 0 0
		 38.000707798564918 -4.6537445170846357e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.003051715180956851 0.99999534350638575 1 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.5990577929911875e-013
		 49.041367281439577 1.5879248213175413e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.70710678118654746 0.70710678118654768 1 1 1 yes;
	setAttr -s 15 ".m";
	setAttr -s 15 ".p";
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	rename -uid "E364BCFD-456D-1355-05E9-F8A81283FC7A";
	setAttr -s 202 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr ".wl[15].w[0]"  1;
	setAttr ".wl[16].w[0]"  1;
	setAttr ".wl[17].w[0]"  1;
	setAttr ".wl[18].w[0]"  1;
	setAttr ".wl[19].w[0]"  1;
	setAttr ".wl[20].w[0]"  1;
	setAttr ".wl[21].w[0]"  1;
	setAttr ".wl[22].w[0]"  1;
	setAttr ".wl[23].w[0]"  1;
	setAttr ".wl[24].w[0]"  1;
	setAttr ".wl[25].w[0]"  1;
	setAttr ".wl[26].w[0]"  1;
	setAttr ".wl[27].w[0]"  1;
	setAttr ".wl[28].w[0]"  1;
	setAttr ".wl[29].w[0]"  1;
	setAttr ".wl[30].w[0]"  1;
	setAttr ".wl[31].w[0]"  1;
	setAttr ".wl[32].w[0]"  1;
	setAttr ".wl[33].w[0]"  1;
	setAttr ".wl[34].w[0]"  1;
	setAttr ".wl[35].w[0]"  1;
	setAttr ".wl[36].w[0]"  1;
	setAttr ".wl[37].w[0]"  1;
	setAttr ".wl[38].w[0]"  1;
	setAttr ".wl[39].w[0]"  1;
	setAttr ".wl[40].w[0]"  1;
	setAttr ".wl[41].w[0]"  1;
	setAttr ".wl[42].w[0]"  1;
	setAttr ".wl[43].w[0]"  1;
	setAttr ".wl[44].w[0]"  1;
	setAttr ".wl[45].w[0]"  1;
	setAttr ".wl[46].w[0]"  1;
	setAttr ".wl[47].w[0]"  1;
	setAttr ".wl[48].w[0]"  1;
	setAttr ".wl[49].w[0]"  1;
	setAttr ".wl[50].w[0]"  1;
	setAttr ".wl[51].w[0]"  1;
	setAttr ".wl[52].w[0]"  1;
	setAttr ".wl[53].w[0]"  1;
	setAttr ".wl[54].w[0]"  1;
	setAttr ".wl[55].w[0]"  1;
	setAttr ".wl[56].w[0]"  1;
	setAttr ".wl[57].w[0]"  1;
	setAttr ".wl[58].w[0]"  1;
	setAttr ".wl[59].w[0]"  1;
	setAttr ".wl[60].w[0]"  1;
	setAttr ".wl[61].w[0]"  1;
	setAttr ".wl[62].w[0]"  1;
	setAttr ".wl[63].w[0]"  1;
	setAttr ".wl[64].w[0]"  1;
	setAttr ".wl[65].w[0]"  1;
	setAttr ".wl[66].w[0]"  1;
	setAttr ".wl[67].w[0]"  1;
	setAttr ".wl[68].w[0]"  1;
	setAttr ".wl[69].w[0]"  1;
	setAttr ".wl[70].w[0]"  1;
	setAttr ".wl[71].w[0]"  1;
	setAttr ".wl[72].w[0]"  1;
	setAttr ".wl[73].w[0]"  1;
	setAttr ".wl[74].w[0]"  1;
	setAttr ".wl[75].w[0]"  1;
	setAttr ".wl[76].w[0]"  1;
	setAttr ".wl[77].w[0]"  1;
	setAttr ".wl[78].w[0]"  1;
	setAttr ".wl[79].w[0]"  1;
	setAttr ".wl[80].w[0]"  1;
	setAttr ".wl[81].w[0]"  1;
	setAttr ".wl[82].w[0]"  1;
	setAttr ".wl[83].w[0]"  1;
	setAttr ".wl[84].w[0]"  1;
	setAttr ".wl[85].w[0]"  1;
	setAttr ".wl[86].w[0]"  1;
	setAttr ".wl[87].w[0]"  1;
	setAttr ".wl[88].w[0]"  1;
	setAttr ".wl[89].w[0]"  1;
	setAttr ".wl[90].w[0]"  1;
	setAttr ".wl[91].w[0]"  1;
	setAttr ".wl[92].w[0]"  1;
	setAttr ".wl[93].w[0]"  1;
	setAttr ".wl[94].w[0]"  1;
	setAttr ".wl[95].w[0]"  1;
	setAttr ".wl[96].w[0]"  1;
	setAttr ".wl[97].w[0]"  1;
	setAttr ".wl[98].w[0]"  1;
	setAttr ".wl[99].w[0]"  1;
	setAttr ".wl[100].w[0]"  1;
	setAttr ".wl[101].w[0]"  1;
	setAttr ".wl[102].w[0]"  1;
	setAttr ".wl[103].w[0]"  1;
	setAttr ".wl[104].w[0]"  1;
	setAttr ".wl[105].w[0]"  1;
	setAttr ".wl[106].w[0]"  1;
	setAttr ".wl[107].w[0]"  1;
	setAttr ".wl[108].w[0]"  1;
	setAttr ".wl[109].w[0]"  1;
	setAttr ".wl[110].w[0]"  1;
	setAttr ".wl[111].w[0]"  1;
	setAttr ".wl[112].w[0]"  1;
	setAttr ".wl[113].w[0]"  1;
	setAttr ".wl[114].w[0]"  1;
	setAttr ".wl[115].w[0]"  1;
	setAttr ".wl[116].w[0]"  1;
	setAttr ".wl[117].w[0]"  1;
	setAttr ".wl[118].w[0]"  1;
	setAttr ".wl[119].w[0]"  1;
	setAttr ".wl[120].w[0]"  1;
	setAttr ".wl[121].w[0]"  1;
	setAttr ".wl[122].w[0]"  1;
	setAttr ".wl[123].w[0]"  1;
	setAttr ".wl[124].w[0]"  1;
	setAttr ".wl[125].w[0]"  1;
	setAttr ".wl[126].w[0]"  1;
	setAttr ".wl[127].w[0]"  1;
	setAttr ".wl[128].w[0]"  1;
	setAttr ".wl[129].w[0]"  1;
	setAttr ".wl[130].w[0]"  1;
	setAttr ".wl[131].w[0]"  1;
	setAttr ".wl[132].w[0]"  1;
	setAttr ".wl[133].w[0]"  1;
	setAttr ".wl[134].w[0]"  1;
	setAttr ".wl[135].w[0]"  1;
	setAttr ".wl[136].w[0]"  1;
	setAttr ".wl[137].w[0]"  1;
	setAttr ".wl[138].w[0]"  1;
	setAttr ".wl[139].w[0]"  1;
	setAttr ".wl[140].w[0]"  1;
	setAttr ".wl[141].w[0]"  1;
	setAttr ".wl[142].w[0]"  1;
	setAttr ".wl[143].w[0]"  1;
	setAttr ".wl[144].w[0]"  1;
	setAttr ".wl[145].w[0]"  1;
	setAttr ".wl[146].w[0]"  1;
	setAttr ".wl[147].w[0]"  1;
	setAttr ".wl[148].w[0]"  1;
	setAttr ".wl[149].w[0]"  1;
	setAttr ".wl[150].w[0]"  1;
	setAttr ".wl[151].w[0]"  1;
	setAttr ".wl[152].w[0]"  1;
	setAttr ".wl[153].w[0]"  1;
	setAttr ".wl[154].w[0]"  1;
	setAttr ".wl[155].w[0]"  1;
	setAttr ".wl[156].w[0]"  1;
	setAttr ".wl[157].w[0]"  1;
	setAttr ".wl[158].w[0]"  1;
	setAttr ".wl[159].w[0]"  1;
	setAttr ".wl[160].w[0]"  1;
	setAttr ".wl[161].w[0]"  1;
	setAttr ".wl[162].w[0]"  1;
	setAttr ".wl[163].w[0]"  1;
	setAttr ".wl[164].w[0]"  1;
	setAttr ".wl[165].w[0]"  1;
	setAttr ".wl[166].w[0]"  1;
	setAttr ".wl[167].w[0]"  1;
	setAttr ".wl[168].w[0]"  1;
	setAttr ".wl[169].w[0]"  1;
	setAttr ".wl[170].w[0]"  1;
	setAttr ".wl[171].w[0]"  1;
	setAttr ".wl[172].w[0]"  1;
	setAttr ".wl[173].w[0]"  1;
	setAttr ".wl[174].w[0]"  1;
	setAttr ".wl[175].w[0]"  1;
	setAttr ".wl[176].w[0]"  1;
	setAttr ".wl[177].w[0]"  1;
	setAttr ".wl[178].w[0]"  1;
	setAttr ".wl[179].w[0]"  1;
	setAttr ".wl[180].w[0]"  1;
	setAttr ".wl[181].w[0]"  1;
	setAttr ".wl[182].w[0]"  1;
	setAttr ".wl[183].w[0]"  1;
	setAttr ".wl[184].w[0]"  1;
	setAttr ".wl[185].w[0]"  1;
	setAttr ".wl[186].w[0]"  1;
	setAttr ".wl[187].w[0]"  1;
	setAttr ".wl[188].w[0]"  1;
	setAttr ".wl[189].w[0]"  1;
	setAttr ".wl[190].w[0]"  1;
	setAttr ".wl[191].w[0]"  1;
	setAttr ".wl[192].w[0]"  1;
	setAttr ".wl[193].w[0]"  1;
	setAttr ".wl[194].w[0]"  1;
	setAttr ".wl[195].w[0]"  1;
	setAttr ".wl[196].w[0]"  1;
	setAttr ".wl[197].w[0]"  1;
	setAttr ".wl[198].w[0]"  1;
	setAttr ".wl[199].w[0]"  1;
	setAttr ".wl[200].w[0]"  1;
	setAttr ".wl[201].w[0]"  1;
	setAttr -s 15 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.57230964874220991 1.3926860721763035 0.26257625752009517 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -40.352460557501651 0 1;
	setAttr ".pm[2]" -type "matrix" 0.99999385132493424 -0.0035067523900789263 0 0 0.0035067523900789263 0.99999385132493424 0 0
		 0 0 1 0 -0.18994696684288531 -54.165728797419177 0 1;
	setAttr ".pm[3]" -type "matrix" 0.99999308685042898 0.0037183667584653763 0 0 -0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 0.51398399154690999 -97.427062129398365 0 1;
	setAttr ".pm[4]" -type "matrix" 0.99999308685042898 0.0037183667584653763 0 0 -0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 0.51398399154690999 -138.22747235176357 0 1;
	setAttr ".pm[5]" -type "matrix" 0.10680253104629513 0.62607794119939131 -0.7724115683401156 0
		 0.9863369185469486 -0.16471561548401623 0.0028721293550296877 0 -0.12543007005266102 -0.76216479685121485 -0.63511583192936649 0
		 -174.38940453763485 29.030234154332419 -0.39700992560143977 1;
	setAttr ".pm[6]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 -23.331451378769671 -170.61007839517504 29.000406755479887 1;
	setAttr ".pm[7]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 24.590050937571 -170.610083864199 29.148076936850188 1;
	setAttr ".pm[8]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 0.51398399154698793 -163.76676111029278 32.500962007041984 1;
	setAttr ".pm[9]" -type "matrix" 0.0069515601636596118 0.99997583761373499 1.2246383329808592e-016 0
		 0.99997583761373499 -0.0069515601636593897 -4.553685948810993e-019 0 3.9595711295480444e-019 1.2246403980182624e-016 -1 0
		 -139.839186636341 -29.032922447137789 -3.6108592907480876e-015 1;
	setAttr ".pm[10]" -type "matrix" 0.0069515601636596118 0.99997583761373499 1.2246383329808592e-016 0
		 0.99997583761373499 -0.0069515601636593897 -4.553685948810993e-019 0 3.9595711295480444e-019 1.2246403980182624e-016 -1 0
		 -139.83918663634097 -67.032922447137807 -8.2645171275080327e-015 1;
	setAttr ".pm[11]" -type "matrix" 0.99997583761373476 0.0069515601636593888 -1.2161251270773539e-016 0
		 -0.0069515601636589448 0.99997583761373476 1.2291708947071648e-016 0 1.2246403980182619e-016 -1.2206872280178055e-016 1 0
		 -114.03292244713785 -139.83918663634088 -1.9296940534924869e-014 1;
	setAttr ".pm[12]" -type "matrix" -0.0023850622471857531 -0.99999715573499348 0 0
		 0.99999715573499348 -0.0023850622471857531 0 0 0 0 1 0 -140.16017085808713 -29.659082560661336 0 1;
	setAttr ".pm[13]" -type "matrix" 0.00079060731477846211 -0.99999933940555474 -0.00083434317146115092 0
		 0.99999966395231621 0.00079078797037350588 -0.00021621665140966916 0 0.00021687629712130503 -0.00083417194861587962 0.99999962856084701 0
		 -139.94461211514198 -68.105291420843017 -0.02651989992194264 1;
	setAttr ".pm[14]" -type "matrix" 0.99999933940555452 0.00079060731477823996 -0.00083434317146115103 0
		 -0.00079078797037328372 0.99999966395231599 -0.00021621665140966918 0 0.00083417194861587951 0.00021687629712130482 0.99999962856084701 0
		 117.14665870228254 -139.94461211514246 -0.026519899921958506 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -3.4694469519536142e-018 1;
	setAttr -s 7 ".ma";
	setAttr -s 15 ".dpf[0:14]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 7 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 7 ".ifcl";
createNode tweak -n "tweak3";
	rename -uid "33DB75FF-490A-3152-DD5D-34BDADA925EC";
createNode objectSet -n "skinCluster2Set";
	rename -uid "EB172BF9-411D-E328-85DF-63B7FD26B69A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster2GroupId";
	rename -uid "042B0116-4F3D-657B-FA8C-6DA0EBA8EE90";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster2GroupParts";
	rename -uid "666F2730-45CF-A2CD-AB84-90B10584639B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose2";
	rename -uid "E756EE3B-4E22-DC28-1FA0-B6954DB97AAB";
	setAttr -s 15 ".wm";
	setAttr ".wm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.57230964874220991 -1.3926860721763035 -0.26257625752009517 1;
	setAttr ".wm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 40.352460557501651 0 1;
	setAttr ".wm[2]" -type "matrix" 0.99999385132493424 0.0035067523900789263 0 0 -0.0035067523900789263 0.99999385132493424 0 0
		 0 0 1 0 -1.0148019021405439e-014 54.166061846933069 0 1;
	setAttr ".wm[3]" -type "matrix" 0.99999308685042898 -0.0037183667584653763 0 0 0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 -0.15171088910180347 97.42829978253414 0 1;
	setAttr ".wm[4]" -type "matrix" 0.99999308685042898 -0.0037183667584653763 0 0 0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 7.9028450450380205e-013 138.22842794556092 0 1;
	setAttr ".wm[5]" -type "matrix" 0.10680253104629521 0.98633691854694849 -0.12543007005266105 0
		 0.6260779411993912 -0.16471561548401611 -0.76216479685121463 0 -0.77241156834011537 0.0028721293550296443 -0.63511583192936638 0
		 0.14338550111459436 176.7895810491255 0 1;
	setAttr ".wm[6]" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 23.965680929120474 170.52214404895116 -29.000406755479819 1;
	setAttr ".wm[7]" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 -23.95549007837117 170.70033923916614 -29.148076936850142 1;
	setAttr ".wm[8]" -type "matrix" 0.99999308685042876 -0.0037183667584656387 4.4408920985006262e-016 0
		 0.003718366758465918 0.99999308685042887 2.9179468924898982e-016 0 -4.9960036108132044e-016 -3.3718687564299188e-016 0.99999999999999978 0
		 0.094964442355380596 163.76754014716695 -32.500962007041927 1;
	setAttr ".wm[9]" -type "matrix" 0.0069515601636593915 0.99997583761373521 3.9595711295478152e-019 0
		 0.99997583761373521 -0.0069515601636596135 1.2246403980182624e-016 0 1.2246383329808592e-016 -4.5536859488110364e-019 -1 0
		 30.004321461590944 139.63398368078037 0 1;
	setAttr ".wm[10]" -type "matrix" 0.0069515601636593915 0.99997583761373521 3.9595711295478152e-019 0
		 0.99997583761373521 -0.0069515601636596135 1.2246403980182624e-016 0 1.2246383329808592e-016 -4.5536859488110364e-019 -1 0
		 68.003403290912885 139.36982439456128 -2.4324290544609439e-020 1;
	setAttr ".wm[11]" -type "matrix" 0.99997583761373543 -0.0069515601636593932 1.2246403980182629e-016 0
		 0.0069515601636589491 0.99997583761373543 -1.2206872280178063e-016 0 -1.2161251270773539e-016 1.2291708947071655e-016 1 0
		 115.00226765875844 139.04310106686927 1.6191881977871764e-014 1;
	setAttr ".wm[12]" -type "matrix" -0.0023850622471857535 0.9999971557349937 0 0 -0.9999971557349937 -0.0023850622471857535 0 0
		 0 0 1 0 -29.99328893444342 140.0890334473163 0 1;
	setAttr ".wm[13]" -type "matrix" 0.00079060731477846244 0.99999966395231643 0.00021687629712130506 0
		 -0.99999933940555497 0.00079078797037350577 -0.00083417194861587951 0 -0.00083434317146115124 -0.00021621665140966918 0.99999962856084701 0
		 -67.994627323561161 139.99841619820967 5.9035693445832352e-005 1;
	setAttr ".wm[14]" -type "matrix" 0.99999933940555519 -0.00079078797037328394 0.00083417194861587973 0
		 0.00079060731477824061 0.99999966395231665 0.00021687629712130492 0 -0.00083434317146115124 -0.00021621665140966918 0.99999962856084701 0
		 -117.03596220854593 140.03719752150695 -0.040849897214483684 1;
	setAttr -s 15 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.57230964874220991 -1.3926860721763035
		 -0.26257625752009517 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.57230964874220991 41.745146629677954
		 0.26257625752009517 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0.0035067595773902644 0 -1.0148019021405439e-014
		 13.813601289431418 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 -0.0072251349044211875 0 0
		 43.262503942680354 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 40.800410222365215 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 5.5511151231257839e-017 -5.5511151231257839e-017
		 -1.1102230246251568e-016 0 0 38.561419684426539 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 -0.68931787870306671 -0.58514850941020724 0.32562878679270191 0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.5600364406079656e-014
		 38.050047542593475 -7.6882152712063823e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 -4.9238195782919192 8.1308569417116949
		 37.109166324320405 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 -8.772693246739399 26.885707230811629
		 20.641875523607148 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.6893178787030666 -0.58514850941020735 0.32562878679270224 -0.27641992917883085 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 -6.401129090982782e-022 -3.9595711295476982e-019
		 0.0032332408259299222 0 29.998887665505102 1.5171130899355205 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0.70710678118654768 0.70710678118654746 4.3297802811774658e-017 4.3297802811774677e-017 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 -1.2200931131985419e-016 1.2246383329808589e-016
		 -7.4708639811444435e-033 0 -2.8421709430404007e-014 38 4.6536578367599419e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 -5.2180482157382334e-015
		 47 -1.0436096431476475e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.70710678118654768 0.70710678118654746 4.3297802811774658e-017 4.3297802811774677e-017 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 -1.1102230246251565e-016 0 -30.000000000000021
		 1.7490665905603748 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.70926137704704417 0.70494560004962847 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 -0.00083417206497578939 -0.00021687629882144639
		 0.00292776791128694 0 1.8566167256039989e-005 38.001446431020554 5.9035693445832352e-005 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.003051715180956851 0.99999534350638575 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.5990577929911875e-013
		 49.041367281439577 1.5879248213175413e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.70710678118654746 0.70710678118654768 1 1 1 yes;
	setAttr -s 15 ".m";
	setAttr -s 15 ".p";
	setAttr ".bp" yes;
createNode objectSet -n "tweakSet2";
	rename -uid "8F75DAFD-487C-2FA3-ADFA-AAA407E3D061";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode objectSet -n "skinCluster1Set";
	rename -uid "FB2F47CB-4765-1BD6-C284-0D88B42405A9";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "groupParts6";
	rename -uid "A1560767-4681-7223-7711-AEABF0CBB599";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode groupId -n "groupId7";
	rename -uid "B47AF8E3-4BED-D503-4C0F-0DA283036A2D";
	setAttr ".ihi" 0;
createNode tweak -n "tweak2";
	rename -uid "9DE8FC24-4086-613A-A75D-ADBCED0DDFE6";
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "EEC43A97-47C0-F062-FE8B-99839577B2CB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode groupId -n "skinCluster1GroupId";
	rename -uid "C60DC525-4CA3-312A-8160-8E97650BEC5A";
	setAttr ".ihi" 0;
createNode skinCluster -n "skinCluster1";
	rename -uid "C331824A-4E41-C8B0-E0A8-BBB0434CF266";
	setAttr -s 1540 ".wl";
	setAttr -s 5 ".wl[0].w[4:8]"  0.00030921443534955983 0.50979619665801068 
		0.00039054142322704521 0.0010381356730188621 0.48846591181039378;
	setAttr -s 5 ".wl[1].w[4:8]"  0.00016225254415177615 0.51453086355337885 
		0.00047633318702578175 0.00022613806897380472 0.48460441264646975;
	setAttr -s 5 ".wl[2].w[4:8]"  0.0002430773640778066 0.6852532353883708 
		0.00037232747209984629 0.0011564652071153179 0.31297489456833633;
	setAttr -s 5 ".wl[3].w[4:8]"  0.0001076993629471113 0.75611877330427002 
		0.0004270801190685915 0.00018672542564319628 0.24315972178807105;
	setAttr -s 5 ".wl[4].w[4:8]"  0.00019611923555661401 0.49905195735921865 
		0.00044606130849939089 0.0012539047375067468 0.49905195735921865;
	setAttr -s 5 ".wl[5].w[4:8]"  0.00010718970883979898 0.4995207655730276 
		0.00057408850548293117 0.00027719063962204041 0.4995207655730276;
	setAttr -s 5 ".wl[6].w[4:8]"  0.0001612787347894225 0.49938760089370332 
		0.00030670935207089881 0.00075681012573306687 0.49938760089370332;
	setAttr -s 5 ".wl[7].w[4:8]"  8.1522870074788215e-005 0.49970605432626031 
		0.0003342841252911993 0.0001720843521133519 0.49970605432626031;
	setAttr -s 5 ".wl[8].w[4:8]"  0.0029150132839388457 0.48158453383477151 
		0.031380853791762225 0.0025350652547559446 0.48158453383477151;
	setAttr -s 5 ".wl[9].w[4:8]"  0.0071220329257416738 0.4927396905709221 
		0.0584687458434111 0.0038481280720565059 0.43782140258786878;
	setAttr -s 5 ".wl[10].w[4:8]"  0.0022955258558763642 0.48179797179229034 
		0.032565866567451307 0.0021606544858865174 0.48117998129849543;
	setAttr -s 5 ".wl[11].w[4:8]"  0.0053926376918796938 0.52578057297168201 
		0.061163603935163381 0.003163286056665104 0.40449989934460984;
	setAttr -s 5 ".wl[12].w[4:8]"  0.0042312558740334452 0.4694850742184653 
		0.0034392073479422034 0.053359388341093883 0.46948507421846519;
	setAttr -s 5 ".wl[13].w[4:8]"  0.0094555859013808578 0.46955549965082999 
		0.0048371883578287051 0.090411489329837905 0.42574023676012251;
	setAttr -s 5 ".wl[14].w[4:8]"  0.0074328428173868104 0.48987489720412675 
		0.0041051390284286681 0.10262712376718333 0.39595999718287445;
	setAttr -s 5 ".wl[15].w[4:8]"  0.0034704395070866579 0.46696266421791816 
		0.0030344572454958671 0.059997364003246234 0.46653507502625319;
	setAttr -s 5 ".wl[16].w[4:8]"  0.00017656609425910705 0.49914327557878635 
		0.00040573061889479981 0.001131152129273469 0.49914327557878635;
	setAttr -s 5 ".wl[17].w[4:8]"  9.3552138210405768e-005 0.4995795604216493 
		0.00050314567148223828 0.00024418134700872346 0.4995795604216493;
	setAttr -s 5 ".wl[18].w[4:8]"  7.5009960706014198e-005 0.49972320554322558 
		0.00031599263999134054 0.00016258631285159594 0.49972320554322558;
	setAttr -s 5 ".wl[19].w[4:8]"  0.00015110695037552827 0.49941226506182779 
		0.00029475394427811157 0.00072960898169083558 0.49941226506182779;
	setAttr -s 5 ".wl[20].w[4:8]"  0.001965652806185921 0.48500404974442035 
		0.026129195718872532 0.0019355304884859533 0.48496557124203526;
	setAttr -s 5 ".wl[21].w[4:8]"  0.0024568668568987913 0.48482574525820049 
		0.025642274792973044 0.002249367833727178 0.48482574525820049;
	setAttr -s 5 ".wl[22].w[4:8]"  0.0036501359880908016 0.4742562579268515 
		0.0031160960486338639 0.044721252109572299 0.4742562579268515;
	setAttr -s 5 ".wl[23].w[4:8]"  0.0030426290891224948 0.47251687710339069 
		0.0027775749229654498 0.049164931724087632 0.47249798716043379;
	setAttr -s 5 ".wl[24].w[4:8]"  8.3213013218635406e-005 0.49963052655305634 
		0.00019188093330141075 0.00046385294736730624 0.49963052655305634;
	setAttr -s 5 ".wl[25].w[4:8]"  3.5606835635486301e-005 0.49985155577635737 
		0.00016797081048561602 9.331080116419999e-005 0.49985155577635737;
	setAttr -s 5 ".wl[26].w[4:8]"  2.2646940278474629e-005 0.49991731276811341 
		9.07272973204579e-005 5.2000226174273934e-005 0.49991731276811341;
	setAttr -s 5 ".wl[27].w[4:8]"  6.2786036390775313e-005 0.49975934238633513 
		0.00012889950149387 0.00028962968944510947 0.49975934238633513;
	setAttr -s 5 ".wl[28].w[4:8]"  0.00089106844730099373 0.49860330300022671 
		0.0079426133819731053 0.00098809781193125884 0.49157491735856806;
	setAttr -s 5 ".wl[29].w[4:8]"  0.0010481016469077872 0.49484959405921064 
		0.0081498146140035407 0.0011028956206673574 0.49484959405921064;
	setAttr -s 5 ".wl[30].w[4:8]"  0.0017849311659751288 0.49003958876824316 
		0.0017332352457476499 0.016402656051790917 0.49003958876824316;
	setAttr -s 5 ".wl[31].w[4:8]"  0.0015791395534118498 0.492406141787883 
		0.001608356141979606 0.016983033278744623 0.48742332923798098;
	setAttr -s 5 ".wl[32].w";
	setAttr ".wl[32].w[4]" 0.40147635948064092;
	setAttr ".wl[32].w[5]" 0.41243595706018288;
	setAttr ".wl[32].w[7]" 0.10556031219934278;
	setAttr ".wl[32].w[8]" 0.047137555649784432;
	setAttr ".wl[32].w[12]" 0.033389815610049035;
	setAttr -s 5 ".wl[33].w";
	setAttr ".wl[33].w[4]" 0.35005524614768779;
	setAttr ".wl[33].w[5]" 0.43415513256276417;
	setAttr ".wl[33].w[7]" 0.12543665356815978;
	setAttr ".wl[33].w[8]" 0.03946067724628486;
	setAttr ".wl[33].w[12]" 0.050892290475103345;
	setAttr -s 5 ".wl[34].w";
	setAttr ".wl[34].w[3]" 0.036756588443574739;
	setAttr ".wl[34].w[4]" 0.42766461058734467;
	setAttr ".wl[34].w[5]" 0.42766461058734445;
	setAttr ".wl[34].w[7]" 0.057561328751199278;
	setAttr ".wl[34].w[12]" 0.050352861630536969;
	setAttr -s 5 ".wl[35].w";
	setAttr ".wl[35].w[3]" 0.035572312127343027;
	setAttr ".wl[35].w[4]" 0.45249998177908834;
	setAttr ".wl[35].w[5]" 0.45249998177908857;
	setAttr ".wl[35].w[7]" 0.029667972719784979;
	setAttr ".wl[35].w[12]" 0.029759751594695027;
	setAttr -s 5 ".wl[36].w[4:8]"  0.44613998587938947 0.44613998587938947 
		0.032626100125886306 0.042557311949152875 0.032536616166181892;
	setAttr -s 5 ".wl[37].w[4:8]"  0.42544222507694807 0.42544222507694807 
		0.043545155209778606 0.061439016637542247 0.044131377998783063;
	setAttr -s 5 ".wl[38].w";
	setAttr ".wl[38].w[4]" 0.39944899105348386;
	setAttr ".wl[38].w[5]" 0.41052081865454215;
	setAttr ".wl[38].w[6]" 0.10867821203787009;
	setAttr ".wl[38].w[8]" 0.047779152720800404;
	setAttr ".wl[38].w[9]" 0.033572825533303596;
	setAttr -s 5 ".wl[39].w[4:8]"  0.4249059735175138 0.4249059735175138 
		0.062872029351625489 0.042968626620331692 0.044347396993015216;
	setAttr -s 5 ".wl[40].w[4:8]"  0.44574680582404824 0.44574680582404824 
		0.043565365882847776 0.032224432027042241 0.032716590442013464;
	setAttr -s 5 ".wl[41].w";
	setAttr ".wl[41].w[3]" 0.035756070336906566;
	setAttr ".wl[41].w[4]" 0.45191574770040388;
	setAttr ".wl[41].w[5]" 0.45191574770040388;
	setAttr ".wl[41].w[6]" 0.030520596403877751;
	setAttr ".wl[41].w[9]" 0.029891837858407943;
	setAttr -s 5 ".wl[42].w";
	setAttr ".wl[42].w[3]" 0.03730599929695564;
	setAttr ".wl[42].w[4]" 0.42594175681818236;
	setAttr ".wl[42].w[5]" 0.42594175681818236;
	setAttr ".wl[42].w[6]" 0.059912717158448293;
	setAttr ".wl[42].w[9]" 0.050897769908231381;
	setAttr -s 5 ".wl[43].w";
	setAttr ".wl[43].w[4]" 0.3473283221148204;
	setAttr ".wl[43].w[5]" 0.43051180703644387;
	setAttr ".wl[43].w[6]" 0.13043120865575389;
	setAttr ".wl[43].w[8]" 0.040331452484713731;
	setAttr ".wl[43].w[9]" 0.051397209708268277;
	setAttr -s 5 ".wl[44].w[4:8]"  0.065784182863915408 0.57383611957324498 
		0.012292051506386197 0.30442144885468347 0.043666197201769792;
	setAttr -s 5 ".wl[45].w";
	setAttr ".wl[45].w[4]" 0.19252322596783888;
	setAttr ".wl[45].w[5]" 0.51919096136951204;
	setAttr ".wl[45].w[7]" 0.21907546291027158;
	setAttr ".wl[45].w[8]" 0.041223022961142906;
	setAttr ".wl[45].w[12]" 0.027987326791234564;
	setAttr -s 5 ".wl[46].w[4:8]"  0.30969519386748501 0.42710287963533317 
		0.03467665614722535 0.16781858093874907 0.060706689411207368;
	setAttr -s 5 ".wl[47].w[4:8]"  0.38420225437775946 0.39229704679599536 
		0.061405820274269309 0.095677972784714696 0.066416905767261072;
	setAttr -s 5 ".wl[48].w[4:8]"  0.25142659152897739 0.38516237880133614 
		0.085663300249376323 0.16275339570360206 0.11499433371670804;
	setAttr -s 5 ".wl[49].w[4:8]"  0.11094936315113615 0.45206019370828976 
		0.063736029163918351 0.20469208651664159 0.16856232746001415;
	setAttr -s 5 ".wl[50].w[4:8]"  0.065860553936039581 0.56301562440557729 
		0.31523621911735861 0.01214666742217761 0.043740935118846795;
	setAttr -s 5 ".wl[51].w[4:8]"  0.10850674584607675 0.45475863759767676 
		0.21319349161196929 0.059636244695285147 0.16390488024899191;
	setAttr -s 5 ".wl[52].w[4:8]"  0.24993933674252766 0.38540176612056409 
		0.16701005499701116 0.083136404828697413 0.11451243731119974;
	setAttr -s 5 ".wl[53].w[4:8]"  0.38333183863987735 0.39178046438608677 
		0.097936850404447232 0.060351882649301573 0.06659896392028708;
	setAttr -s 5 ".wl[54].w[4:8]"  0.30801577107341921 0.42415347584179341 
		0.17212624265152748 0.034430927682489738 0.061273582750770106;
	setAttr -s 5 ".wl[55].w";
	setAttr ".wl[55].w[4]" 0.19219929819134696;
	setAttr ".wl[55].w[5]" 0.51047551965619153;
	setAttr ".wl[55].w[6]" 0.2268191411949213;
	setAttr ".wl[55].w[8]" 0.042066986674213644;
	setAttr ".wl[55].w[9]" 0.028439054283326617;
	setAttr -s 5 ".wl[56].w";
	setAttr ".wl[56].w[4]" 0.43154808821722829;
	setAttr ".wl[56].w[5]" 0.43154808821722829;
	setAttr ".wl[56].w[7]" 0.064392604038534487;
	setAttr ".wl[56].w[8]" 0.036099091963981415;
	setAttr ".wl[56].w[12]" 0.036412127563027537;
	setAttr -s 5 ".wl[57].w";
	setAttr ".wl[57].w[4]" 0.43016481365951048;
	setAttr ".wl[57].w[5]" 0.43016481365951048;
	setAttr ".wl[57].w[6]" 0.066431898328274314;
	setAttr ".wl[57].w[8]" 0.036616399727391592;
	setAttr ".wl[57].w[9]" 0.03662207462531309;
	setAttr -s 5 ".wl[58].w[4:8]"  0.19400135063557414 0.45555686001955048 
		0.037303476588460366 0.23568974907286705 0.077448563683548077;
	setAttr -s 5 ".wl[59].w[4:8]"  0.19281391659651489 0.45181613492691053 
		0.24126180437985129 0.036645474223137214 0.077462669873586021;
	setAttr -s 5 ".wl[60].w";
	setAttr ".wl[60].w[4]" 0.27254711693448946;
	setAttr ".wl[60].w[5]" 0.46518312258681604;
	setAttr ".wl[60].w[6]" 0.17764467718224158;
	setAttr ".wl[60].w[8]" 0.042634721491162181;
	setAttr ".wl[60].w[9]" 0.041990361805290831;
	setAttr -s 5 ".wl[61].w";
	setAttr ".wl[61].w[4]" 0.25176716470269822;
	setAttr ".wl[61].w[5]" 0.46255040128142028;
	setAttr ".wl[61].w[6]" 0.20534622536146324;
	setAttr ".wl[61].w[8]" 0.052470881081916929;
	setAttr ".wl[61].w[9]" 0.027865327572501222;
	setAttr -s 5 ".wl[62].w";
	setAttr ".wl[62].w[4]" 0.10805425987812935;
	setAttr ".wl[62].w[5]" 0.56410525358129882;
	setAttr ".wl[62].w[7]" 0.27420982329322979;
	setAttr ".wl[62].w[8]" 0.039022041805207394;
	setAttr ".wl[62].w[12]" 0.014608621442134664;
	setAttr -s 5 ".wl[63].w[4:8]"  0.074300541776427476 0.54946000330643063 
		0.023101779742204665 0.2727425547254303 0.080395120449507085;
	setAttr -s 5 ".wl[64].w[4:8]"  0.17704462388729592 0.41087226453170328 
		0.080899128389016819 0.19049439357535997 0.14068958961662401;
	setAttr -s 5 ".wl[65].w[4:8]"  0.27238299259602783 0.3467724754065889 
		0.12886939894635918 0.12519832506626472 0.12677680798475946;
	setAttr -s 5 ".wl[66].w[4:8]"  0.17505128515558568 0.41194503652373227 
		0.19658567887849335 0.077424164273829074 0.13899383516835981;
	setAttr -s 5 ".wl[67].w[4:8]"  0.12741999083995464 0.39096639630096536 
		0.13522274573498599 0.12784859551999325 0.21854227160410072;
	setAttr -s 5 ".wl[68].w[4:8]"  0.3280912980189209 0.38321332700971561 
		0.074903980397008296 0.12627628126631765 0.087515113308037629;
	setAttr -s 5 ".wl[69].w[4:8]"  0.38944611105594118 0.38944611105594118 
		0.077756216207109444 0.076171765955681883 0.067179795725326258;
	setAttr -s 5 ".wl[70].w[4:8]"  0.32691428543947626 0.38297181035086103 
		0.12933168274576054 0.073262197106929494 0.087520024356972662;
	setAttr -s 5 ".wl[71].w[4:8]"  0.072845169272187649 0.54397500476311245 
		0.28283858686463187 0.022022594784401196 0.078318644315666847;
	setAttr -s 5 ".wl[72].w";
	setAttr ".wl[72].w[4]" 0.10866381752392744;
	setAttr ".wl[72].w[5]" 0.55277024308551104;
	setAttr ".wl[72].w[6]" 0.28390222747381544;
	setAttr ".wl[72].w[8]" 0.039723559119482194;
	setAttr ".wl[72].w[9]" 0.014940152797263911;
	setAttr -s 5 ".wl[73].w";
	setAttr ".wl[73].w[4]" 0.42113684327355033;
	setAttr ".wl[73].w[5]" 0.42113684327355033;
	setAttr ".wl[73].w[7]" 0.080669783350549265;
	setAttr ".wl[73].w[8]" 0.040841127035114944;
	setAttr ".wl[73].w[12]" 0.036215403067235023;
	setAttr -s 5 ".wl[74].w";
	setAttr ".wl[74].w[4]" 0.43995472152143894;
	setAttr ".wl[74].w[5]" 0.43995472152143916;
	setAttr ".wl[74].w[7]" 0.053892367229364933;
	setAttr ".wl[74].w[8]" 0.034698890443196913;
	setAttr ".wl[74].w[12]" 0.031499299284559919;
	setAttr -s 5 ".wl[75].w";
	setAttr ".wl[75].w[4]" 0.42997496515934241;
	setAttr ".wl[75].w[5]" 0.42997496515934264;
	setAttr ".wl[75].w[7]" 0.063004923640460328;
	setAttr ".wl[75].w[8]" 0.034076013767607494;
	setAttr ".wl[75].w[12]" 0.042969132273247071;
	setAttr -s 5 ".wl[76].w";
	setAttr ".wl[76].w[4]" 0.4194965362730379;
	setAttr ".wl[76].w[5]" 0.41949653627303779;
	setAttr ".wl[76].w[6]" 0.083157064904968464;
	setAttr ".wl[76].w[8]" 0.041432354059115307;
	setAttr ".wl[76].w[9]" 0.036417508489840648;
	setAttr -s 5 ".wl[77].w";
	setAttr ".wl[77].w[4]" 0.42842836727042505;
	setAttr ".wl[77].w[5]" 0.42842836727042483;
	setAttr ".wl[77].w[6]" 0.065202078229758237;
	setAttr ".wl[77].w[8]" 0.034648296392022541;
	setAttr ".wl[77].w[9]" 0.043292890837369431;
	setAttr -s 5 ".wl[78].w";
	setAttr ".wl[78].w[4]" 0.43895638532009917;
	setAttr ".wl[78].w[5]" 0.43895638532009917;
	setAttr ".wl[78].w[6]" 0.055415728595100924;
	setAttr ".wl[78].w[8]" 0.035068606479027931;
	setAttr ".wl[78].w[9]" 0.031602894285672839;
	setAttr -s 5 ".wl[79].w[4:8]"  0.13093144589416553 0.49711391489740531 
		0.025618348498841079 0.27975227846827644 0.066584012241311732;
	setAttr -s 5 ".wl[80].w[4:8]"  0.21623749400929887 0.4253016926025609 
		0.053726434667923928 0.2095928236231516 0.095141555097064776;
	setAttr -s 5 ".wl[81].w[4:8]"  0.24060820174009034 0.44165160842843254 
		0.036388448611443414 0.21130861149774052 0.070043129722293207;
	setAttr -s 5 ".wl[82].w[4:8]"  0.13032569439559175 0.4911911703770897 
		0.28673303084107171 0.025193430008686988 0.066556674377559827;
	setAttr -s 5 ".wl[83].w[4:8]"  0.23934424432992729 0.43808127515385958 
		0.21622609500692058 0.0359605255306489 0.070387859978643705;
	setAttr -s 5 ".wl[84].w[4:8]"  0.21468288733615856 0.42342124462169534 
		0.21463975609700894 0.052433557521325051 0.094822554423812211;
	setAttr -s 5 ".wl[85].w";
	setAttr ".wl[85].w[4]" 0.40856918291691435;
	setAttr ".wl[85].w[5]" 0.41556883352025276;
	setAttr ".wl[85].w[7]" 0.085345652236540559;
	setAttr ".wl[85].w[8]" 0.03526869601332016;
	setAttr ".wl[85].w[12]" 0.055247635312972124;
	setAttr -s 5 ".wl[86].w";
	setAttr ".wl[86].w[3]" 0.036175854846387426;
	setAttr ".wl[86].w[4]" 0.44290028671996279;
	setAttr ".wl[86].w[5]" 0.44290028671996279;
	setAttr ".wl[86].w[7]" 0.039388944192168221;
	setAttr ".wl[86].w[12]" 0.03863462752151875;
	setAttr -s 5 ".wl[87].w";
	setAttr ".wl[87].w[3]" 0.036573370597336466;
	setAttr ".wl[87].w[4]" 0.44179892532926229;
	setAttr ".wl[87].w[5]" 0.44179892532926207;
	setAttr ".wl[87].w[6]" 0.04083022641441663;
	setAttr ".wl[87].w[9]" 0.038998552329722588;
	setAttr -s 5 ".wl[88].w";
	setAttr ".wl[88].w[4]" 0.4059266742683843;
	setAttr ".wl[88].w[5]" 0.41326133006572208;
	setAttr ".wl[88].w[6]" 0.088925302845406451;
	setAttr ".wl[88].w[8]" 0.036065869576522559;
	setAttr ".wl[88].w[9]" 0.055820823243964618;
	setAttr -s 5 ".wl[89].w";
	setAttr ".wl[89].w[4]" 0.3761861042290055;
	setAttr ".wl[89].w[5]" 0.41944792727781438;
	setAttr ".wl[89].w[7]" 0.11960684580105031;
	setAttr ".wl[89].w[8]" 0.043723460102854016;
	setAttr ".wl[89].w[12]" 0.041035662589275781;
	setAttr -s 5 ".wl[90].w[4:8]"  0.36293497032141753 0.41577068389762706 
		0.033194535616132559 0.1343777911186898 0.05372201904613285;
	setAttr -s 5 ".wl[91].w";
	setAttr ".wl[91].w[4]" 0.2530334623926293;
	setAttr ".wl[91].w[5]" 0.46795024447545108;
	setAttr ".wl[91].w[7]" 0.19965907276962264;
	setAttr ".wl[91].w[8]" 0.05175772343849578;
	setAttr ".wl[91].w[12]" 0.027599496923801189;
	setAttr -s 5 ".wl[92].w";
	setAttr ".wl[92].w[4]" 0.27426083142806407;
	setAttr ".wl[92].w[5]" 0.47108546756542929;
	setAttr ".wl[92].w[7]" 0.17136840645614138;
	setAttr ".wl[92].w[8]" 0.041761942833293257;
	setAttr ".wl[92].w[12]" 0.041523351717071971;
	setAttr -s 5 ".wl[93].w[4:8]"  0.41846708762576063 0.41846708762576051 
		0.036816342705866915 0.080529662631220161 0.045719819411391797;
	setAttr -s 5 ".wl[94].w[4:8]"  0.40912765098951637 0.40912765098951637 
		0.051726197739374882 0.076319136261334825 0.053699364020257573;
	setAttr -s 5 ".wl[95].w[4:8]"  0.35698091812372457 0.40428648382410409 
		0.046580331844462759 0.12748312523387792 0.064669140973830641;
	setAttr -s 5 ".wl[96].w[4:8]"  0.43789694481025293 0.43789694481025282 
		0.03715276590359029 0.050011929838803516 0.037041414637100402;
	setAttr -s 5 ".wl[97].w[4:8]"  0.4472795159260648 0.4472795159260648 
		0.037001752442815269 0.036341147534331222 0.032098068170723949;
	setAttr -s 5 ".wl[98].w[4:8]"  0.43744841124117506 0.43744841124117495 
		0.051175712494940288 0.036690076726144952 0.037237388296564783;
	setAttr -s 5 ".wl[99].w[4:8]"  0.42635094430934106 0.42635094430934106 
		0.052042812345356224 0.051098224696436254 0.044157074339525419;
	setAttr -s 5 ".wl[100].w";
	setAttr ".wl[100].w[3]" 0.03181890950502185;
	setAttr ".wl[100].w[4]" 0.45248591321707865;
	setAttr ".wl[100].w[5]" 0.45248591321707865;
	setAttr ".wl[100].w[7]" 0.035396278816681094;
	setAttr ".wl[100].w[8]" 0.027812985244139728;
	setAttr -s 5 ".wl[101].w[3:7]"  0.036120793624416135 0.45625282912478454 
		0.45625282912478443 0.02595493050938966 0.025418617616625182;
	setAttr -s 5 ".wl[102].w";
	setAttr ".wl[102].w[3]" 0.031965595286604441;
	setAttr ".wl[102].w[4]" 0.45187799287665237;
	setAttr ".wl[102].w[5]" 0.45187799287665226;
	setAttr ".wl[102].w[6]" 0.036299519402448688;
	setAttr ".wl[102].w[8]" 0.027978899557642278;
	setAttr -s 5 ".wl[103].w[4:8]"  0.41726180559720299 0.41726180559720299 
		0.082751129377662339 0.036544057578366096 0.046181201849565569;
	setAttr -s 5 ".wl[104].w[4:8]"  0.3609956944076288 0.41347814320874432 
		0.13810771672584676 0.033050263762189537 0.054368181895590506;
	setAttr -s 5 ".wl[105].w[4:8]"  0.35538192921557921 0.40284746150719458 
		0.13071274057327473 0.04600234914311431 0.065055519560837188;
	setAttr -s 5 ".wl[106].w[4:8]"  0.40850291627407881 0.4085029162740787 
		0.078099328422562078 0.050972836627798383 0.053922002401482066;
	setAttr -s 5 ".wl[107].w";
	setAttr ".wl[107].w[4]" 0.37372597210495601;
	setAttr ".wl[107].w[5]" 0.41674629768224142;
	setAttr ".wl[107].w[6]" 0.12365553955094673;
	setAttr ".wl[107].w[8]" 0.044505290041284888;
	setAttr ".wl[107].w[9]" 0.041366900620571009;
	setAttr -s 5 ".wl[108].w";
	setAttr ".wl[108].w[4]" 0.31970293970504071;
	setAttr ".wl[108].w[5]" 0.4392702353625037;
	setAttr ".wl[108].w[7]" 0.15747435077712194;
	setAttr ".wl[108].w[8]" 0.048167038760717255;
	setAttr ".wl[108].w[12]" 0.03538543539461652;
	setAttr -s 5 ".wl[109].w[4:8]"  0.39628886585549289 0.40588572596867462 
		0.041708980800835668 0.1017005073293735 0.054415920045623317;
	setAttr -s 5 ".wl[110].w[4:8]"  0.43895349190464017 0.43895349190464017 
		0.043048240584794159 0.042285067327748137 0.036759708278177372;
	setAttr -s 5 ".wl[111].w[3:7]"  0.031745461800000145 0.45364169099034396 
		0.45364169099034396 0.030778734338372776 0.030192421880939169;
	setAttr -s 5 ".wl[112].w[4:8]"  0.39482051245383148 0.40461076309134014 
		0.10438157123009718 0.04131644015944079 0.054870713065290525;
	setAttr -s 5 ".wl[113].w";
	setAttr ".wl[113].w[4]" 0.31766125607808454;
	setAttr ".wl[113].w[5]" 0.4353517914483806;
	setAttr ".wl[113].w[6]" 0.16235770179952172;
	setAttr ".wl[113].w[8]" 0.048945956086335479;
	setAttr ".wl[113].w[9]" 0.035683294587677661;
	setAttr -s 5 ".wl[114].w[4:8]"  0.20152886998676681 0.34561997494872054 
		0.14446963530718648 0.13887207191275883 0.1695094478445674;
	setAttr -s 5 ".wl[115].w[4:8]"  0.34388628703147039 0.36307971316899412 
		0.10184217090759871 0.099469536904370118 0.091722291987566554;
	setAttr -s 5 ".wl[116].w[4:8]"  0.41028398849812525 0.41028398849812525 
		0.063359105282863537 0.062165626291433136 0.053907291429452664;
	setAttr -s 5 ".wl[117].w[4:8]"  0.43196929400797168 0.43196929400797168 
		0.032960117943708657 0.064047084378272098 0.039054209662075914;
	setAttr -s 5 ".wl[118].w";
	setAttr ".wl[118].w[4]" 0.41444371197731067;
	setAttr ".wl[118].w[5]" 0.41552167700215159;
	setAttr ".wl[118].w[7]" 0.086723676621673504;
	setAttr ".wl[118].w[8]" 0.038627594792939736;
	setAttr ".wl[118].w[12]" 0.044683339605924535;
	setAttr -s 5 ".wl[119].w";
	setAttr ".wl[119].w[3]" 0.032931779762621002;
	setAttr ".wl[119].w[4]" 0.44336296549396254;
	setAttr ".wl[119].w[5]" 0.44336296549396254;
	setAttr ".wl[119].w[7]" 0.045691665769322945;
	setAttr ".wl[119].w[12]" 0.034650623480130982;
	setAttr -s 5 ".wl[120].w";
	setAttr ".wl[120].w[4]" 0.41229438776013699;
	setAttr ".wl[120].w[5]" 0.41349576254836173;
	setAttr ".wl[120].w[6]" 0.089818711642810495;
	setAttr ".wl[120].w[8]" 0.039345570353435867;
	setAttr ".wl[120].w[9]" 0.04504556769525471;
	setAttr -s 5 ".wl[121].w[4:8]"  0.43097683454851288 0.43097683454851288 
		0.065835471886690339 0.032740898958857151 0.039469960057426708;
	setAttr -s 5 ".wl[122].w";
	setAttr ".wl[122].w[3]" 0.033257034495669711;
	setAttr ".wl[122].w[4]" 0.4423727263362553;
	setAttr ".wl[122].w[5]" 0.44237272633625518;
	setAttr ".wl[122].w[6]" 0.047144230257771202;
	setAttr ".wl[122].w[9]" 0.034853282574048731;
	setAttr -s 5 ".wl[123].w[4:8]"  0.14143582932312013 0.47201914635003456 
		0.041239093773718045 0.24894160809685439 0.096364322456272883;
	setAttr -s 5 ".wl[124].w[4:8]"  0.17595748882932169 0.4941692626042033 
		0.022979636490528713 0.25085530259354333 0.056038309482403084;
	setAttr -s 5 ".wl[125].w[4:8]"  0.29233815876440555 0.40999486264749629 
		0.052503682983162166 0.16527994145778699 0.079883354147148783;
	setAttr -s 5 ".wl[126].w[4:8]"  0.17550500458244442 0.48767844176149278 
		0.25740214723492144 0.022852963501653889 0.05656144291948751;
	setAttr -s 5 ".wl[127].w[4:8]"  0.13982777463581497 0.46912607967695041 
		0.25590543363171669 0.039866969901298767 0.095273742154219174;
	setAttr -s 5 ".wl[128].w[4:8]"  0.29075130260003806 0.40834781793270103 
		0.16926543330134097 0.051591864887315653 0.080043581278604295;
	setAttr -s 4 ".wl[129].w[1:4]"  0.60730131983529712 0.36569649209185134 
		0.023172315005686232 0.0038298730671651971;
	setAttr -s 4 ".wl[130].w[1:4]"  0.61242944238328312 0.36223374681123061 
		0.021698901019851145 0.0036379097856351307;
	setAttr -s 4 ".wl[131].w[1:4]"  0.61560693056916327 0.36023714853548128 
		0.020664441364122443 0.0034914795312330016;
	setAttr -s 4 ".wl[132].w[1:4]"  0.61146527102566706 0.36350528449132985 
		0.02162793765312606 0.0034015068298769149;
	setAttr -s 4 ".wl[133].w[1:4]"  0.61050662267803546 0.36378621960411101 
		0.022169267783139976 0.0035378899347136019;
	setAttr -s 4 ".wl[134].w[1:4]"  0.59602496066664246 0.37330415244559179 
		0.026139471218159037 0.0045314156696067004;
	setAttr -s 4 ".wl[135].w[1:4]"  0.60250903709058967 0.3693692002697796 
		0.024150473994933849 0.0039712886446967664;
	setAttr -s 4 ".wl[136].w[1:4]"  0.60763656233144847 0.36624452525875451 
		0.02253773355302072 0.0035811788567762797;
	setAttr -s 4 ".wl[137].w[1:4]"  0.59937864942741226 0.37159319020914239 
		0.02458602005786863 0.0044421403055767538;
	setAttr -s 4 ".wl[138].w[1:4]"  0.598105232169997 0.37210891161906118 
		0.0252846462347797 0.0045012099761621563;
	setAttr -s 4 ".wl[139].w[1:4]"  0.61561872502516002 0.36022856778125883 
		0.020661751091445999 0.0034909561021351848;
	setAttr -s 4 ".wl[140].w[1:4]"  0.61459350950768754 0.36106930686189287 
		0.020799413680685334 0.0035377699497342418;
	setAttr -s 4 ".wl[141].w[1:4]"  0.60860012528364238 0.36567696416490503 
		0.022213702789940455 0.003509207761512098;
	setAttr -s 4 ".wl[142].w[1:4]"  0.59805066970259213 0.3725785178819252 
		0.024842414699420771 0.0045283977160620204;
	setAttr -s 4 ".wl[143].w[1:4]"  0.59938853737520148 0.37158662567302048 
		0.024583275602541373 0.0044415613492366666;
	setAttr -s 4 ".wl[144].w[1:4]"  0.61245398040717691 0.36221622407543558 
		0.021693025577595733 0.0036367699397918127;
	setAttr -s 4 ".wl[145].w[1:4]"  0.60733565956857416 0.36567266745912536 
		0.023163498113375321 0.0038281748589251946;
	setAttr -s 4 ".wl[146].w[1:4]"  0.61054991938764436 0.36375553665512517 
		0.022158627463130298 0.0035359164941002304;
	setAttr -s 4 ".wl[147].w[1:4]"  0.6115135508663192 0.36347075477933866 
		0.021616309356845987 0.0033993849974961383;
	setAttr -s 4 ".wl[148].w[1:4]"  0.60768286208984945 0.36621207571842451 
		0.022526045860656013 0.0035790163310700542;
	setAttr -s 4 ".wl[149].w[1:4]"  0.60864842222944548 0.36564289582241238 
		0.022201682728867262 0.0035069992192748958;
	setAttr -s 4 ".wl[150].w[1:4]"  0.60254871658323905 0.36934233059858784 
		0.024139732199059828 0.00396922061911312;
	setAttr -s 4 ".wl[151].w[1:4]"  0.59605533320768822 0.37328452466285589 
		0.026130548087698949 0.004529594041756904;
	setAttr -s 4 ".wl[152].w[1:4]"  0.59812623298332979 0.37209512855649141 
		0.025278674288601949 0.0044999641715768998;
	setAttr -s 4 ".wl[153].w[1:4]"  0.47028738014286392 0.47334638210867341 
		0.050328027085782587 0.0060382106626800745;
	setAttr -s 4 ".wl[154].w[1:4]"  0.47149114684581606 0.47511426261547529 
		0.047578669666716408 0.0058159208719922792;
	setAttr -s 4 ".wl[155].w[1:4]"  0.47212016060688744 0.47648955206369309 
		0.045743898715759977 0.0056463886136593061;
	setAttr -s 4 ".wl[156].w[1:4]"  0.47179268736804603 0.47635318357119671 
		0.046102511032292633 0.0057516180284645987;
	setAttr -s 4 ".wl[157].w[1:4]"  0.47212312329202405 0.47649294427594097 
		0.045738427504901549 0.0056455049271334724;
	setAttr -s 4 ".wl[158].w[1:4]"  0.47149761746902841 0.475121481315044 
		0.047566881636103675 0.0058140195798240552;
	setAttr -s 4 ".wl[159].w[1:4]"  0.47029701700032772 0.4733569177733386 
		0.050310642071948443 0.006035423154385126;
	setAttr -s 4 ".wl[160].w[1:4]"  0.47131889636940327 0.47472142705845355 
		0.048465296755103034 0.0054943798170401055;
	setAttr -s 4 ".wl[161].w[1:4]"  0.47162620069030931 0.47551361284471066 
		0.04760278340537006 0.0052574030596100304;
	setAttr -s 4 ".wl[162].w[1:4]"  0.47091758002947909 0.47480827963251704 
		0.048834346513721302 0.0054397938242826058;
	setAttr -s 4 ".wl[163].w[1:4]"  0.47074884097625458 0.47432390575848149 
		0.049367730721703577 0.0055595225435604136;
	setAttr -s 4 ".wl[164].w[1:4]"  0.4693519142422517 0.47217068422646169 
		0.052248400782604128 0.0062290007486825155;
	setAttr -s 4 ".wl[165].w[1:4]"  0.4672967598302592 0.46959520795923859 
		0.055865055421322668 0.0072429767891794231;
	setAttr -s 4 ".wl[166].w[1:4]"  0.46785727961196599 0.47043752312736831 
		0.054378784963885402 0.0073264122967803599;
	setAttr -s 4 ".wl[167].w[1:4]"  0.46816677190009282 0.47121832758562743 
		0.053281387553600325 0.0073335129606794044;
	setAttr -s 4 ".wl[168].w[1:4]"  0.46772233386741924 0.47089273618610894 
		0.053865634134229248 0.0075192958122426035;
	setAttr -s 4 ".wl[169].w[1:4]"  0.46816375009306621 0.471215039860022 
		0.053286714015117725 0.0073344960317939306;
	setAttr -s 4 ".wl[170].w[1:4]"  0.4678507301283501 0.47043049479155036 
		0.054390274319109637 0.0073285007609899836;
	setAttr -s 4 ".wl[171].w[1:4]"  0.46728706768437256 0.46958489508950535 
		0.055882059231301184 0.0072459779948208765;
	setAttr -s 4 ".wl[172].w[1:4]"  0.46934024348526054 0.4721580389998386 
		0.052269349419982764 0.0062323680949180884;
	setAttr -s 4 ".wl[173].w[1:4]"  0.47073616705266758 0.47430981150690649 
		0.049390996961931437 0.0055630244784945033;
	setAttr -s 4 ".wl[174].w[1:4]"  0.47090458494029347 0.47479367767567909 
		0.048858372363345559 0.0054433650206818672;
	setAttr -s 4 ".wl[175].w[1:4]"  0.47161358044653018 0.47549940358679454 
		0.04762618123333133 0.0052608347333439392;
	setAttr -s 4 ".wl[176].w[1:4]"  0.47130730108466085 0.47470858331553123 
		0.048486531962883321 0.0054975836369245881;
	setAttr -s 4 ".wl[177].w[1:4]"  0.34186063968220681 0.51941742813096459 
		0.12673181842991005 0.011990113756918545;
	setAttr -s 4 ".wl[178].w[1:4]"  0.33828691029071772 0.52667367280076383 
		0.12311250406370253 0.011926912844815904;
	setAttr -s 4 ".wl[179].w[1:4]"  0.33445329858652095 0.53270794600073279 
		0.12097379525234055 0.011864960160405801;
	setAttr -s 4 ".wl[180].w[1:4]"  0.33383580885455311 0.53227887708096144 
		0.12175119741795443 0.01213411664653107;
	setAttr -s 4 ".wl[181].w[1:4]"  0.33445727365550132 0.53269398056803763 
		0.12098224423965209 0.011866501536809031;
	setAttr -s 4 ".wl[182].w[1:4]"  0.33829440943180439 0.52664514233165138 
		0.12313028530771164 0.011930162928832616;
	setAttr -s 4 ".wl[183].w[1:4]"  0.34186994554118294 0.51937808951265629 
		0.1267572108666099 0.011994754079550885;
	setAttr -s 4 ".wl[184].w[1:4]"  0.34000009405333276 0.52477030313873896 
		0.12439692602802561 0.010832676779902595;
	setAttr -s 4 ".wl[185].w[1:4]"  0.33776974122493586 0.52826924444566492 
		0.1236258890867654 0.01033512524263383;
	setAttr -s 4 ".wl[186].w[1:4]"  0.33820778955569542 0.52559124121458767 
		0.12556302129319655 0.010637947936520412;
	setAttr -s 4 ".wl[187].w[1:4]"  0.33964151644873253 0.52350618014350714 
		0.1259973811766868 0.010854922231073497;
	setAttr -s 4 ".wl[188].w[1:4]"  0.34361920670345325 0.51493775352825699 
		0.12932984443313936 0.012113195335150349;
	setAttr -s 4 ".wl[189].w[1:4]"  0.34655304804200465 0.5056135754871619 
		0.13370826395122051 0.014125112519613035;
	setAttr -s 4 ".wl[190].w[1:4]"  0.34454048088877626 0.50889834471568618 
		0.13188683423201444 0.014674340163523219;
	setAttr -s 4 ".wl[191].w[1:4]"  0.34191127622120243 0.51222174965703848 
		0.13087330401969827 0.014993670102060869;
	setAttr -s 4 ".wl[192].w[1:4]"  0.34143315731656676 0.51128663188131607 
		0.131858289465521 0.01542192133659611;
	setAttr -s 4 ".wl[193].w[1:4]"  0.34190889055096652 0.5122332874882346 
		0.13086581774200398 0.014992004218794907;
	setAttr -s 4 ".wl[194].w[1:4]"  0.34453584742511051 0.50892242367772456 
		0.13187087169188497 0.014670857205280099;
	setAttr -s 4 ".wl[195].w[1:4]"  0.3465468719947013 0.50564793370566841 
		0.13368498362353154 0.014120210676098775;
	setAttr -s 4 ".wl[196].w[1:4]"  0.34360919686542818 0.51498320883706217 
		0.1292999028263094 0.012107691471200211;
	setAttr -s 4 ".wl[197].w[1:4]"  0.33962770311731599 0.52356019655222685 
		0.12596292675194451 0.010849173578512682;
	setAttr -s 4 ".wl[198].w[1:4]"  0.3381927926944277 0.52564795160805422 
		0.12552717914668701 0.010632076550831098;
	setAttr -s 4 ".wl[199].w[1:4]"  0.3377542899196927 0.52832578062429614 
		0.12359047276025159 0.010329456695759532;
	setAttr -s 4 ".wl[200].w[1:4]"  0.3399871609307874 0.52482030660922652 
		0.12436516121666301 0.010827371243323084;
	setAttr -s 4 ".wl[201].w[1:4]"  0.15828281308974662 0.52630717176647268 
		0.29127421366999984 0.024135801473780742;
	setAttr -s 4 ".wl[202].w[1:4]"  0.15232334666787631 0.53327218861760983 
		0.28954087085757257 0.024863593856941354;
	setAttr -s 4 ".wl[203].w[1:4]"  0.14711023762134526 0.53843607165270646 
		0.28911294535888871 0.025340745367059613;
	setAttr -s 4 ".wl[204].w[1:4]"  0.14683941150026855 0.5372190056683015 
		0.28987523696293371 0.026066345868496259;
	setAttr -s 4 ".wl[205].w[1:4]"  0.1471186002005116 0.5384195505713919 
		0.28911824229378758 0.025343606934308895;
	setAttr -s 4 ".wl[206].w[1:4]"  0.15234084232805889 0.53323805484763021 
		0.28955160968541954 0.024869493138891391;
	setAttr -s 4 ".wl[207].w[1:4]"  0.15830754929115537 0.52625980559246155 
		0.2912886716018932 0.024143973514489891;
	setAttr -s 4 ".wl[208].w[1:4]"  0.15490300193762663 0.53279210505905461 
		0.29100375251842547 0.021301140484893264;
	setAttr -s 4 ".wl[209].w[1:4]"  0.15203678482935623 0.53621722298516861 
		0.29169733447686036 0.020048657708614831;
	setAttr -s 4 ".wl[210].w[1:4]"  0.15351999818452944 0.53287846039657427 
		0.29305385483968949 0.020547686579206827;
	setAttr -s 4 ".wl[211].w[1:4]"  0.15532764938929972 0.5310164536136518 
		0.29261545876658968 0.021040438230458702;
	setAttr -s 4 ".wl[212].w[1:4]"  0.16174181268055113 0.52183226789341564 
		0.29264876695319708 0.023777152472836155;
	setAttr -s 4 ".wl[213].w[1:4]"  0.16790766070167001 0.51064870526178585 
		0.29308724029082012 0.028356393745724007;
	setAttr -s 4 ".wl[214].w[1:4]"  0.16433294577093624 0.51314363727768775 
		0.29202527324005134 0.030498143711324713;
	setAttr -s 4 ".wl[215].w[1:4]"  0.16057299039534392 0.51542579410889633 
		0.29207106283443612 0.031930152661323714;
	setAttr -s 4 ".wl[216].w[1:4]"  0.16056349291138344 0.51368096076606529 
		0.29271929937073077 0.033036246951820329;
	setAttr -s 4 ".wl[217].w[1:4]"  0.16056587456228047 0.51543969161654157 
		0.2920672889749481 0.03192714484622991;
	setAttr -s 4 ".wl[218].w[1:4]"  0.16431783884934692 0.51317288212276646 
		0.29201730847541546 0.030491970552471184;
	setAttr -s 4 ".wl[219].w[1:4]"  0.16788570850356993 0.5106905470980978 
		0.29307583189668968 0.028347912501642657;
	setAttr -s 4 ".wl[220].w[1:4]"  0.16171283492511765 0.52188711373560526 
		0.29263239103565286 0.023767660303624302;
	setAttr -s 4 ".wl[221].w[1:4]"  0.15529369680125729 0.5310806893934833 
		0.29259509048801546 0.021030523317243976;
	setAttr -s 4 ".wl[222].w[1:4]"  0.15348457265086723 0.53294544695501611 
		0.29303241450436612 0.020537565889750579;
	setAttr -s 4 ".wl[223].w[1:4]"  0.15200159152568418 0.5362841716202611 
		0.29167539953814031 0.020038837315914475;
	setAttr -s 4 ".wl[224].w[1:4]"  0.1548716320236514 0.53285193149880483 
		0.29098454458308559 0.021291891894458268;
	setAttr -s 5 ".wl[225].w";
	setAttr ".wl[225].w[1]" 0.057099458196785927;
	setAttr ".wl[225].w[2]" 0.43980674671231107;
	setAttr ".wl[225].w[3]" 0.41775444893047303;
	setAttr ".wl[225].w[4]" 0.045204234189531976;
	setAttr ".wl[225].w[12]" 0.040135111970897849;
	setAttr -s 5 ".wl[226].w";
	setAttr ".wl[226].w[1]" 0.052959560340126229;
	setAttr ".wl[226].w[2]" 0.43718364328009313;
	setAttr ".wl[226].w[3]" 0.41515032658158968;
	setAttr ".wl[226].w[4]" 0.048663902838900069;
	setAttr ".wl[226].w[12]" 0.046042566959290952;
	setAttr -s 5 ".wl[227].w";
	setAttr ".wl[227].w[1]" 0.04977957737985534;
	setAttr ".wl[227].w[2]" 0.43529612108642823;
	setAttr ".wl[227].w[3]" 0.41383523456405519;
	setAttr ".wl[227].w[4]" 0.051290685943155896;
	setAttr ".wl[227].w[12]" 0.049798381026505412;
	setAttr -s 5 ".wl[228].w";
	setAttr ".wl[228].w[1]" 0.04960605106276024;
	setAttr ".wl[228].w[2]" 0.43290166897397675;
	setAttr ".wl[228].w[3]" 0.41204995274868284;
	setAttr ".wl[228].w[4]" 0.053308678402863727;
	setAttr ".wl[228].w[12]" 0.052133648811716587;
	setAttr -s 5 ".wl[229].w";
	setAttr ".wl[229].w[1]" 0.049784612650489814;
	setAttr ".wl[229].w[2]" 0.43528735233104243;
	setAttr ".wl[229].w[3]" 0.41382879122994365;
	setAttr ".wl[229].w[4]" 0.051295826172285973;
	setAttr ".wl[229].w[12]" 0.049803417616238126;
	setAttr -s 5 ".wl[230].w";
	setAttr ".wl[230].w[1]" 0.052970609683241909;
	setAttr ".wl[230].w[2]" 0.4371654022879678;
	setAttr ".wl[230].w[3]" 0.41513707348958556;
	setAttr ".wl[230].w[4]" 0.048674325364409049;
	setAttr ".wl[230].w[12]" 0.04605258917479569;
	setAttr -s 5 ".wl[231].w";
	setAttr ".wl[231].w[1]" 0.057116075639568976;
	setAttr ".wl[231].w[2]" 0.43978136902619053;
	setAttr ".wl[231].w[3]" 0.41773612520428355;
	setAttr ".wl[231].w[4]" 0.045218366648623498;
	setAttr ".wl[231].w[12]" 0.040148063481333533;
	setAttr -s 5 ".wl[232].w";
	setAttr ".wl[232].w[1]" 0.055713436663276161;
	setAttr ".wl[232].w[2]" 0.44935921232589948;
	setAttr ".wl[232].w[3]" 0.426677116206716;
	setAttr ".wl[232].w[4]" 0.038521893843187799;
	setAttr ".wl[232].w[12]" 0.02972834096092045;
	setAttr -s 4 ".wl[233].w[1:4]"  0.055861480640313009 0.46532776404635412 
		0.44237416500478155 0.036436590308551377;
	setAttr -s 4 ".wl[234].w[1:4]"  0.056987979513537221 0.46403604598165837 
		0.44173443503366255 0.037241539471141859;
	setAttr -s 5 ".wl[235].w";
	setAttr ".wl[235].w[1]" 0.056508973261195854;
	setAttr ".wl[235].w[2]" 0.45140710120861316;
	setAttr ".wl[235].w[3]" 0.42935004598937698;
	setAttr ".wl[235].w[4]" 0.037462068450854806;
	setAttr ".wl[235].w[9]" 0.025271811089959156;
	setAttr -s 5 ".wl[236].w";
	setAttr ".wl[236].w[1]" 0.059750319034998958;
	setAttr ".wl[236].w[2]" 0.44187684186004605;
	setAttr ".wl[236].w[3]" 0.42000221889517203;
	setAttr ".wl[236].w[4]" 0.043192393870369446;
	setAttr ".wl[236].w[9]" 0.035178226339413414;
	setAttr -s 5 ".wl[237].w";
	setAttr ".wl[237].w[1]" 0.062755620660494116;
	setAttr ".wl[237].w[2]" 0.42782503010692496;
	setAttr ".wl[237].w[3]" 0.40692988687731962;
	setAttr ".wl[237].w[4]" 0.053315538816773977;
	setAttr ".wl[237].w[9]" 0.049173923538487187;
	setAttr -s 5 ".wl[238].w";
	setAttr ".wl[238].w[1]" 0.059617010418202915;
	setAttr ".wl[238].w[2]" 0.4213695041502144;
	setAttr ".wl[238].w[3]" 0.40085026385220235;
	setAttr ".wl[238].w[4]" 0.059939949557568172;
	setAttr ".wl[238].w[9]" 0.058223272021812086;
	setAttr -s 5 ".wl[239].w";
	setAttr ".wl[239].w[1]" 0.056898464337659459;
	setAttr ".wl[239].w[2]" 0.41689257817302539;
	setAttr ".wl[239].w[3]" 0.39712312919768478;
	setAttr ".wl[239].w[4]" 0.064892116101708155;
	setAttr ".wl[239].w[9]" 0.064193712189922272;
	setAttr -s 5 ".wl[240].w";
	setAttr ".wl[240].w[1]" 0.056788578652046333;
	setAttr ".wl[240].w[2]" 0.41362506481296279;
	setAttr ".wl[240].w[3]" 0.3944546955373795;
	setAttr ".wl[240].w[4]" 0.067785259563269529;
	setAttr ".wl[240].w[9]" 0.067346401434341951;
	setAttr -s 5 ".wl[241].w";
	setAttr ".wl[241].w[1]" 0.056893809254518618;
	setAttr ".wl[241].w[2]" 0.41690090237716998;
	setAttr ".wl[241].w[3]" 0.39712952023025766;
	setAttr ".wl[241].w[4]" 0.064887069898307836;
	setAttr ".wl[241].w[9]" 0.064188698239745878;
	setAttr -s 5 ".wl[242].w";
	setAttr ".wl[242].w[1]" 0.059606720765380844;
	setAttr ".wl[242].w[2]" 0.42138700208468521;
	setAttr ".wl[242].w[3]" 0.40086351750221055;
	setAttr ".wl[242].w[4]" 0.059929625093959493;
	setAttr ".wl[242].w[9]" 0.058213134553763785;
	setAttr -s 5 ".wl[243].w";
	setAttr ".wl[243].w[1]" 0.062739932944684343;
	setAttr ".wl[243].w[2]" 0.42784973462928255;
	setAttr ".wl[243].w[3]" 0.40694836552889113;
	setAttr ".wl[243].w[4]" 0.053301413959531578;
	setAttr ".wl[243].w[9]" 0.04916055293761043;
	setAttr -s 5 ".wl[244].w";
	setAttr ".wl[244].w[1]" 0.059730044251766097;
	setAttr ".wl[244].w[2]" 0.44190610513864259;
	setAttr ".wl[244].w[3]" 0.42002327889755453;
	setAttr ".wl[244].w[4]" 0.043176212287443658;
	setAttr ".wl[244].w[9]" 0.035164359424593158;
	setAttr -s 5 ".wl[245].w";
	setAttr ".wl[245].w[1]" 0.05648585993362077;
	setAttr ".wl[245].w[2]" 0.45143818044748368;
	setAttr ".wl[245].w[3]" 0.42937174940983974;
	setAttr ".wl[245].w[4]" 0.037444925301104028;
	setAttr ".wl[245].w[9]" 0.025259284907951787;
	setAttr -s 4 ".wl[246].w[1:4]"  0.056962764773518172 0.46406262763515027 
		0.4417514690125669 0.037223138578764733;
	setAttr -s 4 ".wl[247].w[1:4]"  0.055836805680313698 0.46535392979202367 
		0.44239061855540873 0.036418645972253888;
	setAttr -s 5 ".wl[248].w";
	setAttr ".wl[248].w[1]" 0.055692452892285561;
	setAttr ".wl[248].w[2]" 0.4493887714673554;
	setAttr ".wl[248].w[3]" 0.42669773223270191;
	setAttr ".wl[248].w[4]" 0.038505825103134023;
	setAttr ".wl[248].w[12]" 0.029715218304523009;
	setAttr -s 5 ".wl[249].w";
	setAttr ".wl[249].w[1]" 0.024682011411240507;
	setAttr ".wl[249].w[2]" 0.36053821405921843;
	setAttr ".wl[249].w[3]" 0.41025948817239455;
	setAttr ".wl[249].w[4]" 0.11073973059738203;
	setAttr ".wl[249].w[12]" 0.093780555759764503;
	setAttr -s 5 ".wl[250].w";
	setAttr ".wl[250].w[2]" 0.33912815699677468;
	setAttr ".wl[250].w[3]" 0.38917268066751265;
	setAttr ".wl[250].w[4]" 0.12899764053852877;
	setAttr ".wl[250].w[12]" 0.11946316301259693;
	setAttr ".wl[250].w[13]" 0.023238358784586976;
	setAttr -s 5 ".wl[251].w";
	setAttr ".wl[251].w[2]" 0.32030271801402166;
	setAttr ".wl[251].w[3]" 0.37096670930303255;
	setAttr ".wl[251].w[4]" 0.14409368776455622;
	setAttr ".wl[251].w[12]" 0.13854369076298076;
	setAttr ".wl[251].w[13]" 0.026093194155408748;
	setAttr -s 5 ".wl[252].w";
	setAttr ".wl[252].w[2]" 0.31205772549781385;
	setAttr ".wl[252].w[3]" 0.36173456987937674;
	setAttr ".wl[252].w[4]" 0.15139183264805775;
	setAttr ".wl[252].w[12]" 0.14713209271501651;
	setAttr ".wl[252].w[13]" 0.027683779259735045;
	setAttr -s 5 ".wl[253].w";
	setAttr ".wl[253].w[2]" 0.32029808878538329;
	setAttr ".wl[253].w[3]" 0.37095655723436372;
	setAttr ".wl[253].w[4]" 0.14409964815348419;
	setAttr ".wl[253].w[12]" 0.13854972792596959;
	setAttr ".wl[253].w[13]" 0.026095977900799264;
	setAttr -s 5 ".wl[254].w";
	setAttr ".wl[254].w[2]" 0.33911757066765469;
	setAttr ".wl[254].w[3]" 0.38915063373423858;
	setAttr ".wl[254].w[4]" 0.12901126508984845;
	setAttr ".wl[254].w[12]" 0.11947677148426851;
	setAttr ".wl[254].w[13]" 0.02324375902398983;
	setAttr -s 5 ".wl[255].w";
	setAttr ".wl[255].w[1]" 0.024690211618963412;
	setAttr ".wl[255].w[2]" 0.36052240632563082;
	setAttr ".wl[255].w[3]" 0.41022756114552844;
	setAttr ".wl[255].w[4]" 0.11075999826556238;
	setAttr ".wl[255].w[12]" 0.093799822644314967;
	setAttr -s 5 ".wl[256].w";
	setAttr ".wl[256].w[1]" 0.024974524810196103;
	setAttr ".wl[256].w[2]" 0.38233456740480504;
	setAttr ".wl[256].w[3]" 0.43859528459502622;
	setAttr ".wl[256].w[4]" 0.090754526581742587;
	setAttr ".wl[256].w[12]" 0.063341096608230077;
	setAttr -s 5 ".wl[257].w";
	setAttr ".wl[257].w[2]" 0.39258384177866607;
	setAttr ".wl[257].w[3]" 0.45412272887639588;
	setAttr ".wl[257].w[4]" 0.081993007851975691;
	setAttr ".wl[257].w[9]" 0.027656562811072379;
	setAttr ".wl[257].w[12]" 0.043643858681889816;
	setAttr -s 5 ".wl[258].w";
	setAttr ".wl[258].w[2]" 0.39252401626030609;
	setAttr ".wl[258].w[3]" 0.4540285836688267;
	setAttr ".wl[258].w[4]" 0.082813874374175131;
	setAttr ".wl[258].w[9]" 0.036772321099753813;
	setAttr ".wl[258].w[12]" 0.033861204596938345;
	setAttr -s 5 ".wl[259].w";
	setAttr ".wl[259].w[2]" 0.38913723553470347;
	setAttr ".wl[259].w[3]" 0.44917097754804763;
	setAttr ".wl[259].w[4]" 0.08567410425333305;
	setAttr ".wl[259].w[9]" 0.049438799588970468;
	setAttr ".wl[259].w[12]" 0.026578883074945398;
	setAttr -s 5 ".wl[260].w";
	setAttr ".wl[260].w[1]" 0.026604858011397434;
	setAttr ".wl[260].w[2]" 0.37293490876860341;
	setAttr ".wl[260].w[3]" 0.42608747689360066;
	setAttr ".wl[260].w[4]" 0.099863359482190245;
	setAttr ".wl[260].w[9]" 0.074509396844208209;
	setAttr -s 5 ".wl[261].w";
	setAttr ".wl[261].w[1]" 0.026721126917523919;
	setAttr ".wl[261].w[2]" 0.34435997309514105;
	setAttr ".wl[261].w[3]" 0.38983644842541004;
	setAttr ".wl[261].w[4]" 0.12652175218746259;
	setAttr ".wl[261].w[9]" 0.11256069937446245;
	setAttr -s 5 ".wl[262].w";
	setAttr ".wl[262].w[2]" 0.3154326823950816;
	setAttr ".wl[262].w[3]" 0.35940574746792048;
	setAttr ".wl[262].w[4]" 0.15094677821590716;
	setAttr ".wl[262].w[9]" 0.14481455177673586;
	setAttr ".wl[262].w[10]" 0.029400240144354849;
	setAttr -s 5 ".wl[263].w";
	setAttr ".wl[263].w[2]" 0.29210895511960105;
	setAttr ".wl[263].w[3]" 0.33533176358075556;
	setAttr ".wl[263].w[4]" 0.1707519474823469;
	setAttr ".wl[263].w[9]" 0.16835221791427221;
	setAttr ".wl[263].w[10]" 0.033455115903024323;
	setAttr -s 5 ".wl[264].w";
	setAttr ".wl[264].w[2]" 0.28247253028419306;
	setAttr ".wl[264].w[3]" 0.32447635234159894;
	setAttr ".wl[264].w[4]" 0.17946226887217701;
	setAttr ".wl[264].w[9]" 0.17808101794785156;
	setAttr ".wl[264].w[10]" 0.035507830554179311;
	setAttr -s 5 ".wl[265].w";
	setAttr ".wl[265].w[2]" 0.29211244350402932;
	setAttr ".wl[265].w[3]" 0.33533939268413976;
	setAttr ".wl[265].w[4]" 0.17074790362966463;
	setAttr ".wl[265].w[9]" 0.16834809355729613;
	setAttr ".wl[265].w[10]" 0.033452166624870293;
	setAttr -s 5 ".wl[266].w";
	setAttr ".wl[266].w[2]" 0.31544178561831743;
	setAttr ".wl[266].w[3]" 0.35942394316851078;
	setAttr ".wl[266].w[4]" 0.15093611585778971;
	setAttr ".wl[266].w[9]" 0.14480365898069175;
	setAttr ".wl[266].w[10]" 0.029394496374690066;
	setAttr -s 5 ".wl[267].w";
	setAttr ".wl[267].w[1]" 0.026713223852670456;
	setAttr ".wl[267].w[2]" 0.34437492932250091;
	setAttr ".wl[267].w[3]" 0.38986511032579979;
	setAttr ".wl[267].w[4]" 0.12650381580588296;
	setAttr ".wl[267].w[9]" 0.11254292069314591;
	setAttr -s 5 ".wl[268].w";
	setAttr ".wl[268].w[1]" 0.026594404330086075;
	setAttr ".wl[268].w[2]" 0.37295274676438345;
	setAttr ".wl[268].w[3]" 0.42612551595431009;
	setAttr ".wl[268].w[4]" 0.099839154167817468;
	setAttr ".wl[268].w[9]" 0.074488178783402886;
	setAttr -s 5 ".wl[269].w";
	setAttr ".wl[269].w[2]" 0.38915426608234521;
	setAttr ".wl[269].w[3]" 0.44921366461973838;
	setAttr ".wl[269].w[4]" 0.085646514332096912;
	setAttr ".wl[269].w[9]" 0.049419037337634128;
	setAttr ".wl[269].w[12]" 0.026566517628185429;
	setAttr -s 5 ".wl[270].w";
	setAttr ".wl[270].w[2]" 0.39254084923736743;
	setAttr ".wl[270].w[3]" 0.45407259550602075;
	setAttr ".wl[270].w[4]" 0.082785378409295798;
	setAttr ".wl[270].w[9]" 0.036755635997817018;
	setAttr ".wl[270].w[12]" 0.033845540849498951;
	setAttr -s 5 ".wl[271].w";
	setAttr ".wl[271].w[2]" 0.39260040809423585;
	setAttr ".wl[271].w[3]" 0.45416617652231983;
	setAttr ".wl[271].w[4]" 0.081964954377860375;
	setAttr ".wl[271].w[9]" 0.027643351238821372;
	setAttr ".wl[271].w[12]" 0.043625109766762614;
	setAttr -s 5 ".wl[272].w";
	setAttr ".wl[272].w[1]" 0.024963833504402334;
	setAttr ".wl[272].w[2]" 0.3823519643113224;
	setAttr ".wl[272].w[3]" 0.43863507447145078;
	setAttr ".wl[272].w[4]" 0.090729045680977755;
	setAttr ".wl[272].w[12]" 0.063320082031846789;
	setAttr -s 5 ".wl[273].w";
	setAttr ".wl[273].w[2]" 0.15282475334042644;
	setAttr ".wl[273].w[3]" 0.32756448479161082;
	setAttr ".wl[273].w[4]" 0.27391633963954576;
	setAttr ".wl[273].w[5]" 0.031806005566242863;
	setAttr ".wl[273].w[12]" 0.21388841666217401;
	setAttr -s 5 ".wl[274].w";
	setAttr ".wl[274].w[2]" 0.10989092529174002;
	setAttr ".wl[274].w[3]" 0.24374763413038511;
	setAttr ".wl[274].w[4]" 0.32992489046018958;
	setAttr ".wl[274].w[12]" 0.29240101127271867;
	setAttr ".wl[274].w[13]" 0.024035538844966661;
	setAttr -s 5 ".wl[275].w";
	setAttr ".wl[275].w[2]" 0.078270426194739423;
	setAttr ".wl[275].w[3]" 0.17916814936452599;
	setAttr ".wl[275].w[4]" 0.37139268412563914;
	setAttr ".wl[275].w[12]" 0.34886090381947377;
	setAttr ".wl[275].w[13]" 0.02230783649562175;
	setAttr -s 5 ".wl[276].w";
	setAttr ".wl[276].w[2]" 0.06867972019099941;
	setAttr ".wl[276].w[3]" 0.15718748152326506;
	setAttr ".wl[276].w[4]" 0.38444900180580588;
	setAttr ".wl[276].w[12]" 0.36786224480430019;
	setAttr ".wl[276].w[13]" 0.021821551675629509;
	setAttr -s 5 ".wl[277].w";
	setAttr ".wl[277].w[2]" 0.078278925477749992;
	setAttr ".wl[277].w[3]" 0.17917659809148198;
	setAttr ".wl[277].w[4]" 0.37138060820860974;
	setAttr ".wl[277].w[12]" 0.34885236392144875;
	setAttr ".wl[277].w[13]" 0.022311504300709571;
	setAttr -s 5 ".wl[278].w";
	setAttr ".wl[278].w[2]" 0.10990587053868338;
	setAttr ".wl[278].w[3]" 0.24375033701597568;
	setAttr ".wl[278].w[4]" 0.32990805332226669;
	setAttr ".wl[278].w[12]" 0.29239366463935368;
	setAttr ".wl[278].w[13]" 0.024042074483720605;
	setAttr -s 5 ".wl[279].w";
	setAttr ".wl[279].w[2]" 0.15426152092426357;
	setAttr ".wl[279].w[3]" 0.33058739513821456;
	setAttr ".wl[279].w[4]" 0.27645686118897228;
	setAttr ".wl[279].w[12]" 0.21588458125456012;
	setAttr ".wl[279].w[13]" 0.022809641493989563;
	setAttr -s 5 ".wl[280].w";
	setAttr ".wl[280].w[2]" 0.18893668997460228;
	setAttr ".wl[280].w[3]" 0.41720260261628067;
	setAttr ".wl[280].w[4]" 0.22716049660121684;
	setAttr ".wl[280].w[9]" 0.030506151654095856;
	setAttr ".wl[280].w[12]" 0.13619405915380442;
	setAttr -s 5 ".wl[281].w";
	setAttr ".wl[281].w[2]" 0.2043192349688992;
	setAttr ".wl[281].w[3]" 0.46269988210406765;
	setAttr ".wl[281].w[4]" 0.20205339322442939;
	setAttr ".wl[281].w[9]" 0.045796656574186079;
	setAttr ".wl[281].w[12]" 0.085130833128417766;
	setAttr -s 5 ".wl[282].w";
	setAttr ".wl[282].w[2]" 0.20730559433082804;
	setAttr ".wl[282].w[3]" 0.46658024691548078;
	setAttr ".wl[282].w[4]" 0.2001345025317138;
	setAttr ".wl[282].w[9]" 0.065910805844540885;
	setAttr ".wl[282].w[12]" 0.060068850377436513;
	setAttr -s 5 ".wl[283].w";
	setAttr ".wl[283].w[2]" 0.20216465267669348;
	setAttr ".wl[283].w[3]" 0.45180672772097574;
	setAttr ".wl[283].w[4]" 0.20734978832328199;
	setAttr ".wl[283].w[9]" 0.09585806482357101;
	setAttr ".wl[283].w[12]" 0.042820766455477922;
	setAttr -s 5 ".wl[284].w";
	setAttr ".wl[284].w[2]" 0.18271049698293648;
	setAttr ".wl[284].w[3]" 0.39287657640952478;
	setAttr ".wl[284].w[4]" 0.2394695999074597;
	setAttr ".wl[284].w[9]" 0.15587108326643564;
	setAttr ".wl[284].w[12]" 0.029072243433643417;
	setAttr -s 5 ".wl[285].w";
	setAttr ".wl[285].w[2]" 0.14275246842598607;
	setAttr ".wl[285].w[3]" 0.29511016415452085;
	setAttr ".wl[285].w[4]" 0.29221672481577426;
	setAttr ".wl[285].w[9]" 0.24384833012241225;
	setAttr ".wl[285].w[10]" 0.026072312481306547;
	setAttr -s 5 ".wl[286].w";
	setAttr ".wl[286].w[2]" 0.09688669965497522;
	setAttr ".wl[286].w[3]" 0.20497720164036659;
	setAttr ".wl[286].w[4]" 0.34676431164089122;
	setAttr ".wl[286].w[9]" 0.32405643225717906;
	setAttr ".wl[286].w[10]" 0.027315354806587783;
	setAttr -s 5 ".wl[287].w";
	setAttr ".wl[287].w[2]" 0.066207981013812936;
	setAttr ".wl[287].w[3]" 0.14341063708153656;
	setAttr ".wl[287].w[4]" 0.38690836407143747;
	setAttr ".wl[287].w[9]" 0.37844628615172249;
	setAttr ".wl[287].w[10]" 0.02502673168149051;
	setAttr -s 5 ".wl[288].w";
	setAttr ".wl[288].w[2]" 0.057309601930883511;
	setAttr ".wl[288].w[3]" 0.1239747229561709;
	setAttr ".wl[288].w[4]" 0.39930524595282529;
	setAttr ".wl[288].w[9]" 0.39506227632331903;
	setAttr ".wl[288].w[10]" 0.024348152836801265;
	setAttr -s 5 ".wl[289].w";
	setAttr ".wl[289].w[2]" 0.06619966593189594;
	setAttr ".wl[289].w[3]" 0.14340012641531946;
	setAttr ".wl[289].w[4]" 0.3869205740609899;
	setAttr ".wl[289].w[9]" 0.37845711363941625;
	setAttr ".wl[289].w[10]" 0.02502251995237853;
	setAttr -s 5 ".wl[290].w";
	setAttr ".wl[290].w[2]" 0.096871504822423307;
	setAttr ".wl[290].w[3]" 0.20496725219048861;
	setAttr ".wl[290].w[4]" 0.34678361605791502;
	setAttr ".wl[290].w[9]" 0.32406960793442319;
	setAttr ".wl[290].w[10]" 0.027308018994749984;
	setAttr -s 5 ".wl[291].w";
	setAttr ".wl[291].w[2]" 0.1418927571716383;
	setAttr ".wl[291].w[3]" 0.29337779902178823;
	setAttr ".wl[291].w[4]" 0.29050062714730951;
	setAttr ".wl[291].w[5]" 0.031823021357030128;
	setAttr ".wl[291].w[9]" 0.24240579530223377;
	setAttr -s 5 ".wl[292].w";
	setAttr ".wl[292].w[2]" 0.17902670426998574;
	setAttr ".wl[292].w[3]" 0.38503443230638584;
	setAttr ".wl[292].w[4]" 0.23465638741718198;
	setAttr ".wl[292].w[5]" 0.048559182530335802;
	setAttr ".wl[292].w[9]" 0.15272329347611072;
	setAttr -s 5 ".wl[293].w";
	setAttr ".wl[293].w[2]" 0.19840370008942623;
	setAttr ".wl[293].w[3]" 0.44351049951713128;
	setAttr ".wl[293].w[4]" 0.2034936612553703;
	setAttr ".wl[293].w[5]" 0.060531896927860124;
	setAttr ".wl[293].w[9]" 0.094060242210212128;
	setAttr -s 5 ".wl[294].w";
	setAttr ".wl[294].w[2]" 0.20597359449663721;
	setAttr ".wl[294].w[3]" 0.46370138113635462;
	setAttr ".wl[294].w[4]" 0.19884679826305507;
	setAttr ".wl[294].w[5]" 0.06573911305197655;
	setAttr ".wl[294].w[8]" 0.06573911305197655;
	setAttr -s 5 ".wl[295].w";
	setAttr ".wl[295].w[2]" 0.2012437867850306;
	setAttr ".wl[295].w[3]" 0.45585151450936395;
	setAttr ".wl[295].w[4]" 0.19901149178695926;
	setAttr ".wl[295].w[5]" 0.060058794491554233;
	setAttr ".wl[295].w[12]" 0.083834412427092039;
	setAttr -s 5 ".wl[296].w";
	setAttr ".wl[296].w[2]" 0.18552499708589013;
	setAttr ".wl[296].w[3]" 0.40976007248635865;
	setAttr ".wl[296].w[4]" 0.22306842471183927;
	setAttr ".wl[296].w[5]" 0.047920978310410318;
	setAttr ".wl[296].w[12]" 0.13372552740550162;
	setAttr -s 5 ".wl[297].w";
	setAttr ".wl[297].w[3]" 0.1716275816599325;
	setAttr ".wl[297].w[4]" 0.43856284068469215;
	setAttr ".wl[297].w[5]" 0.041730217847508987;
	setAttr ".wl[297].w[8]" 0.041730217847508987;
	setAttr ".wl[297].w[12]" 0.30634914196035745;
	setAttr -s 5 ".wl[298].w";
	setAttr ".wl[298].w[2]" 0.012874891440314092;
	setAttr ".wl[298].w[3]" 0.066682964454062604;
	setAttr ".wl[298].w[4]" 0.5020886021011185;
	setAttr ".wl[298].w[5]" 0.012278972613485617;
	setAttr ".wl[298].w[12]" 0.4060745693910191;
	setAttr -s 5 ".wl[299].w";
	setAttr ".wl[299].w[2]" 0.0031955272574378975;
	setAttr ".wl[299].w[3]" 0.017353916789604639;
	setAttr ".wl[299].w[4]" 0.52460335592673224;
	setAttr ".wl[299].w[12]" 0.45155739318027543;
	setAttr ".wl[299].w[13]" 0.0032898068459499175;
	setAttr -s 5 ".wl[300].w";
	setAttr ".wl[300].w[2]" 0.0016076958160297083;
	setAttr ".wl[300].w[3]" 0.0087118825109062121;
	setAttr ".wl[300].w[4]" 0.5239232667328716;
	setAttr ".wl[300].w[12]" 0.46387398968307636;
	setAttr ".wl[300].w[13]" 0.0018831652571161413;
	setAttr -s 5 ".wl[301].w";
	setAttr ".wl[301].w[2]" 0.0031982757990530759;
	setAttr ".wl[301].w[3]" 0.017367059077597405;
	setAttr ".wl[301].w[4]" 0.52457596300670339;
	setAttr ".wl[301].w[12]" 0.45156606936064003;
	setAttr ".wl[301].w[13]" 0.0032926327560061394;
	setAttr -s 5 ".wl[302].w";
	setAttr ".wl[302].w[2]" 0.012921711123425404;
	setAttr ".wl[302].w[3]" 0.066911281653539698;
	setAttr ".wl[302].w[4]" 0.5034758697197298;
	setAttr ".wl[302].w[12]" 0.40723904940682776;
	setAttr ".wl[302].w[13]" 0.009452088096477242;
	setAttr -s 5 ".wl[303].w";
	setAttr ".wl[303].w[2]" 0.036196164681690665;
	setAttr ".wl[303].w[3]" 0.17747191909399843;
	setAttr ".wl[303].w[4]" 0.45335069345555912;
	setAttr ".wl[303].w[12]" 0.31672357798208095;
	setAttr ".wl[303].w[13]" 0.016257644786670827;
	setAttr -s 5 ".wl[304].w";
	setAttr ".wl[304].w[2]" 0.06053046257293606;
	setAttr ".wl[304].w[3]" 0.31152031619246207;
	setAttr ".wl[304].w[4]" 0.39669773456752733;
	setAttr ".wl[304].w[9]" 0.030270612717115827;
	setAttr ".wl[304].w[12]" 0.20098087394995859;
	setAttr -s 5 ".wl[305].w";
	setAttr ".wl[305].w[2]" 0.073940892292021637;
	setAttr ".wl[305].w[3]" 0.39539631100705286;
	setAttr ".wl[305].w[4]" 0.35638755181707082;
	setAttr ".wl[305].w[9]" 0.054769132112923703;
	setAttr ".wl[305].w[12]" 0.11950611277093104;
	setAttr -s 5 ".wl[306].w";
	setAttr ".wl[306].w[2]" 0.077569745873539625;
	setAttr ".wl[306].w[3]" 0.41007616102034777;
	setAttr ".wl[306].w[4]" 0.34769226111782303;
	setAttr ".wl[306].w[9]" 0.086192160828129458;
	setAttr ".wl[306].w[12]" 0.078469671160160126;
	setAttr -s 5 ".wl[307].w";
	setAttr ".wl[307].w[2]" 0.073104809594328093;
	setAttr ".wl[307].w[3]" 0.38282887222732581;
	setAttr ".wl[307].w[4]" 0.36054031496024308;
	setAttr ".wl[307].w[9]" 0.13304907485790826;
	setAttr ".wl[307].w[12]" 0.050476928360194719;
	setAttr -s 5 ".wl[308].w";
	setAttr ".wl[308].w[2]" 0.058222025083246164;
	setAttr ".wl[308].w[3]" 0.28709020899440985;
	setAttr ".wl[308].w[4]" 0.40289755688235268;
	setAttr ".wl[308].w[9]" 0.22388601596483096;
	setAttr ".wl[308].w[12]" 0.027904193075160424;
	setAttr -s 5 ".wl[309].w";
	setAttr ".wl[309].w[2]" 0.033094708582427169;
	setAttr ".wl[309].w[3]" 0.15295978678482061;
	setAttr ".wl[309].w[4]" 0.45227969877513474;
	setAttr ".wl[309].w[9]" 0.34412752376737332;
	setAttr ".wl[309].w[10]" 0.017538282090244169;
	setAttr -s 5 ".wl[310].w";
	setAttr ".wl[310].w[2]" 0.011111920620988548;
	setAttr ".wl[310].w[3]" 0.053319894800898958;
	setAttr ".wl[310].w[4]" 0.4916397445746577;
	setAttr ".wl[310].w[9]" 0.43400356848241245;
	setAttr ".wl[310].w[10]" 0.0099248715210423517;
	setAttr -s 5 ".wl[311].w";
	setAttr ".wl[311].w[2]" 0.0025325137565749009;
	setAttr ".wl[311].w[3]" 0.012596647193675647;
	setAttr ".wl[311].w[4]" 0.50361093926493461;
	setAttr ".wl[311].w[9]" 0.47799558043386003;
	setAttr ".wl[311].w[10]" 0.0032643193509548627;
	setAttr -s 5 ".wl[312].w";
	setAttr ".wl[312].w[2]" 0.0012092734038412114;
	setAttr ".wl[312].w[3]" 0.0059912146436442446;
	setAttr ".wl[312].w[4]" 0.50167260473764452;
	setAttr ".wl[312].w[9]" 0.48933873958211038;
	setAttr ".wl[312].w[10]" 0.0017881676327596841;
	setAttr -s 5 ".wl[313].w";
	setAttr ".wl[313].w[2]" 0.0025301451586396653;
	setAttr ".wl[313].w[3]" 0.012586006102709307;
	setAttr ".wl[313].w[4]" 0.50362572805278361;
	setAttr ".wl[313].w[9]" 0.47799682187146902;
	setAttr ".wl[313].w[10]" 0.0032612988143983757;
	setAttr -s 5 ".wl[314].w";
	setAttr ".wl[314].w[3]" 0.05317662245969916;
	setAttr ".wl[314].w[4]" 0.49066699082471976;
	setAttr ".wl[314].w[5]" 0.011588924693448107;
	setAttr ".wl[314].w[6]" 0.011450331904795059;
	setAttr ".wl[314].w[9]" 0.43311713011733805;
	setAttr -s 5 ".wl[315].w";
	setAttr ".wl[315].w[3]" 0.14820545271674965;
	setAttr ".wl[315].w[4]" 0.43838106139182298;
	setAttr ".wl[315].w[5]" 0.03996823833958011;
	setAttr ".wl[315].w[6]" 0.039929866362001208;
	setAttr ".wl[315].w[9]" 0.33351538118984614;
	setAttr -s 5 ".wl[316].w";
	setAttr ".wl[316].w[3]" 0.25620381553861893;
	setAttr ".wl[316].w[4]" 0.3595950969562367;
	setAttr ".wl[316].w[5]" 0.092208404398397184;
	setAttr ".wl[316].w[8]" 0.092208404398397184;
	setAttr ".wl[316].w[9]" 0.19978427870835003;
	setAttr -s 5 ".wl[317].w";
	setAttr ".wl[317].w[3]" 0.31473567562870647;
	setAttr ".wl[317].w[4]" 0.29640504174348309;
	setAttr ".wl[317].w[5]" 0.13975440407665729;
	setAttr ".wl[317].w[8]" 0.13975440407665729;
	setAttr ".wl[317].w[9]" 0.10935047447449583;
	setAttr -s 5 ".wl[318].w";
	setAttr ".wl[318].w[3]" 0.33067685728378476;
	setAttr ".wl[318].w[4]" 0.28035467912444861;
	setAttr ".wl[318].w[5]" 0.15974694349183513;
	setAttr ".wl[318].w[8]" 0.15974694349183513;
	setAttr ".wl[318].w[9]" 0.069474576608096347;
	setAttr -s 5 ".wl[319].w";
	setAttr ".wl[319].w[3]" 0.32530307233866279;
	setAttr ".wl[319].w[4]" 0.29319825984467696;
	setAttr ".wl[319].w[5]" 0.14160570630083497;
	setAttr ".wl[319].w[8]" 0.14160570630083497;
	setAttr ".wl[319].w[12]" 0.098287255214990363;
	setAttr -s 5 ".wl[320].w";
	setAttr ".wl[320].w[3]" 0.27735530678406284;
	setAttr ".wl[320].w[4]" 0.35322189005238164;
	setAttr ".wl[320].w[5]" 0.095253859182297035;
	setAttr ".wl[320].w[8]" 0.095253859182297035;
	setAttr ".wl[320].w[12]" 0.17891508479896157;
	setAttr -s 5 ".wl[321].w";
	setAttr ".wl[321].w[3]" 0.10622439831902859;
	setAttr ".wl[321].w[4]" 0.4117498370360923;
	setAttr ".wl[321].w[5]" 0.099956742961211056;
	setAttr ".wl[321].w[7]" 0.096551631494844081;
	setAttr ".wl[321].w[12]" 0.28551739018882388;
	setAttr -s 5 ".wl[322].w";
	setAttr ".wl[322].w[3]" 0.030086414618136201;
	setAttr ".wl[322].w[4]" 0.51890315033291123;
	setAttr ".wl[322].w[5]" 0.020205050735268314;
	setAttr ".wl[322].w[7]" 0.018671835540747268;
	setAttr ".wl[322].w[12]" 0.41213354877293706;
	setAttr -s 5 ".wl[323].w";
	setAttr ".wl[323].w[3]" 0.00185048360553386;
	setAttr ".wl[323].w[4]" 0.56215348882484228;
	setAttr ".wl[323].w[5]" 0.00079030608417160096;
	setAttr ".wl[323].w[7]" 0.00066312313483487954;
	setAttr ".wl[323].w[12]" 0.43454259835061737;
	setAttr -s 5 ".wl[324].w";
	setAttr ".wl[324].w[3]" 2.614704484737464e-005;
	setAttr ".wl[324].w[4]" 0.71912379693117767;
	setAttr ".wl[324].w[5]" 8.0635907186456625e-006;
	setAttr ".wl[324].w[12]" 0.28083525875192966;
	setAttr ".wl[324].w[13]" 6.7336813265702749e-006;
	setAttr -s 5 ".wl[325].w";
	setAttr ".wl[325].w[3]" 0.0018566477504615301;
	setAttr ".wl[325].w[4]" 0.56240340692016888;
	setAttr ".wl[325].w[5]" 0.00042736059302430886;
	setAttr ".wl[325].w[12]" 0.43489622296680441;
	setAttr ".wl[325].w[13]" 0.00041636176954080057;
	setAttr -s 5 ".wl[326].w";
	setAttr ".wl[326].w[3]" 0.030995640701847758;
	setAttr ".wl[326].w[4]" 0.53396252123952881;
	setAttr ".wl[326].w[5]" 0.0058091306214853918;
	setAttr ".wl[326].w[12]" 0.42416505865870191;
	setAttr ".wl[326].w[13]" 0.0050676487784361299;
	setAttr -s 5 ".wl[327].w";
	setAttr ".wl[327].w[3]" 0.12754686070049934;
	setAttr ".wl[327].w[4]" 0.4941476469018744;
	setAttr ".wl[327].w[5]" 0.022428474772919531;
	setAttr ".wl[327].w[12]" 0.34271350453711175;
	setAttr ".wl[327].w[13]" 0.013163513087594801;
	setAttr -s 5 ".wl[328].w";
	setAttr ".wl[328].w[3]" 0.26451476706124732;
	setAttr ".wl[328].w[4]" 0.44100712641692863;
	setAttr ".wl[328].w[5]" 0.044616839844441182;
	setAttr ".wl[328].w[9]" 0.028652851037883263;
	setAttr ".wl[328].w[12]" 0.22120841563949961;
	setAttr -s 5 ".wl[329].w";
	setAttr ".wl[329].w[3]" 0.35890177779946081;
	setAttr ".wl[329].w[4]" 0.39445900605091577;
	setAttr ".wl[329].w[5]" 0.059520142390890554;
	setAttr ".wl[329].w[9]" 0.056177352451448823;
	setAttr ".wl[329].w[12]" 0.13094172130728396;
	setAttr -s 5 ".wl[330].w";
	setAttr ".wl[330].w[3]" 0.37419508248280586;
	setAttr ".wl[330].w[4]" 0.38881984851917301;
	setAttr ".wl[330].w[5]" 0.06333917285777535;
	setAttr ".wl[330].w[9]" 0.090234711057340461;
	setAttr ".wl[330].w[12]" 0.083411185082905182;
	setAttr -s 5 ".wl[331].w";
	setAttr ".wl[331].w[3]" 0.34847408229899279;
	setAttr ".wl[331].w[4]" 0.39712384647936416;
	setAttr ".wl[331].w[5]" 0.059282726224809144;
	setAttr ".wl[331].w[9]" 0.14279798308081806;
	setAttr ".wl[331].w[12]" 0.05232136191601585;
	setAttr -s 5 ".wl[332].w";
	setAttr ".wl[332].w[3]" 0.24633102743226992;
	setAttr ".wl[332].w[4]" 0.44252731537209017;
	setAttr ".wl[332].w[5]" 0.043818090367095823;
	setAttr ".wl[332].w[9]" 0.24064037180302053;
	setAttr ".wl[332].w[12]" 0.026683195025523541;
	setAttr -s 5 ".wl[333].w";
	setAttr ".wl[333].w[3]" 0.11307191525529824;
	setAttr ".wl[333].w[4]" 0.48636074090757059;
	setAttr ".wl[333].w[5]" 0.021510270983951717;
	setAttr ".wl[333].w[9]" 0.36493321973922421;
	setAttr ".wl[333].w[10]" 0.014123853113955314;
	setAttr -s 5 ".wl[334].w";
	setAttr ".wl[334].w[3]" 0.02639984163177124;
	setAttr ".wl[334].w[4]" 0.51446192784502043;
	setAttr ".wl[334].w[5]" 0.0055078611913083773;
	setAttr ".wl[334].w[9]" 0.44817469992525633;
	setAttr ".wl[334].w[10]" 0.0054556694066437462;
	setAttr -s 5 ".wl[335].w";
	setAttr ".wl[335].w[3]" 0.0015743807350941581;
	setAttr ".wl[335].w[4]" 0.51796730216269393;
	setAttr ".wl[335].w[5]" 0.00041122901014742103;
	setAttr ".wl[335].w[9]" 0.47958684217802494;
	setAttr ".wl[335].w[10]" 0.00046024591403946106;
	setAttr -s 5 ".wl[336].w";
	setAttr ".wl[336].w[3]" 3.0331454605077085e-005;
	setAttr ".wl[336].w[4]" 0.52326776522338803;
	setAttr ".wl[336].w[5]" 1.0691281658954935e-005;
	setAttr ".wl[336].w[9]" 0.47668094229325209;
	setAttr ".wl[336].w[10]" 1.0269747095891825e-005;
	setAttr -s 5 ".wl[337].w";
	setAttr ".wl[337].w[3]" 0.0015692117780446509;
	setAttr ".wl[337].w[4]" 0.51770544816472852;
	setAttr ".wl[337].w[5]" 0.00076929476844058862;
	setAttr ".wl[337].w[6]" 0.00066499950537262493;
	setAttr ".wl[337].w[9]" 0.47929104578341358;
	setAttr -s 5 ".wl[338].w";
	setAttr ".wl[338].w[3]" 0.025638053103599425;
	setAttr ".wl[338].w[4]" 0.50020528458482261;
	setAttr ".wl[338].w[5]" 0.019774752660404113;
	setAttr ".wl[338].w[6]" 0.018671131665057149;
	setAttr ".wl[338].w[9]" 0.43571077798611668;
	setAttr -s 5 ".wl[339].w";
	setAttr ".wl[339].w[3]" 0.093686383554556468;
	setAttr ".wl[339].w[4]" 0.40319318232810747;
	setAttr ".wl[339].w[5]" 0.10132038320721876;
	setAttr ".wl[339].w[6]" 0.099312290115155244;
	setAttr ".wl[339].w[9]" 0.30248776079496204;
	setAttr -s 5 ".wl[340].w";
	setAttr ".wl[340].w[3]" 0.15296174558278447;
	setAttr ".wl[340].w[4]" 0.27485154089153468;
	setAttr ".wl[340].w[5]" 0.21137987335635811;
	setAttr ".wl[340].w[8]" 0.21137987335635811;
	setAttr ".wl[340].w[9]" 0.14942696681296472;
	setAttr -s 5 ".wl[341].w";
	setAttr ".wl[341].w[3]" 0.13425985800623741;
	setAttr ".wl[341].w[4]" 0.15301115389451861;
	setAttr ".wl[341].w[5]" 0.31760590648235421;
	setAttr ".wl[341].w[6]" 0.077517175134535585;
	setAttr ".wl[341].w[8]" 0.31760590648235421;
	setAttr -s 5 ".wl[342].w";
	setAttr ".wl[342].w[3]" 0.12088596706487853;
	setAttr ".wl[342].w[4]" 0.12561242282872107;
	setAttr ".wl[342].w[5]" 0.35413954914166934;
	setAttr ".wl[342].w[6]" 0.045222511823061654;
	setAttr ".wl[342].w[8]" 0.35413954914166934;
	setAttr -s 5 ".wl[343].w";
	setAttr ".wl[343].w[3]" 0.13532884867809941;
	setAttr ".wl[343].w[4]" 0.14874154376204168;
	setAttr ".wl[343].w[5]" 0.32360394994004393;
	setAttr ".wl[343].w[7]" 0.068721707679771107;
	setAttr ".wl[343].w[8]" 0.32360394994004382;
	setAttr -s 5 ".wl[344].w";
	setAttr ".wl[344].w[3]" 0.15963500617658755;
	setAttr ".wl[344].w[4]" 0.266199827956838;
	setAttr ".wl[344].w[5]" 0.2203365883227221;
	setAttr ".wl[344].w[8]" 0.2203365883227221;
	setAttr ".wl[344].w[12]" 0.13349198922113018;
	setAttr -s 5 ".wl[345].w";
	setAttr ".wl[345].w[4]" 0.095781555504891855;
	setAttr ".wl[345].w[5]" 0.40330652704566983;
	setAttr ".wl[345].w[7]" 0.32800743077806443;
	setAttr ".wl[345].w[8]" 0.095629763669052251;
	setAttr ".wl[345].w[12]" 0.077274723002321535;
	setAttr -s 5 ".wl[346].w";
	setAttr ".wl[346].w[4]" 0.27649832760324988;
	setAttr ".wl[346].w[5]" 0.24581446117383737;
	setAttr ".wl[346].w[7]" 0.18254625794734955;
	setAttr ".wl[346].w[8]" 0.040455516148987083;
	setAttr ".wl[346].w[12]" 0.25468543712657588;
	setAttr -s 5 ".wl[347].w";
	setAttr ".wl[347].w[3]" 0.024793103287448548;
	setAttr ".wl[347].w[4]" 0.43630867119727612;
	setAttr ".wl[347].w[5]" 0.068501000332464895;
	setAttr ".wl[347].w[7]" 0.043981508101052232;
	setAttr ".wl[347].w[12]" 0.42641571708175818;
	setAttr -s 5 ".wl[348].w";
	setAttr ".wl[348].w[3]" 0.018448272246629451;
	setAttr ".wl[348].w[4]" 0.46987686018038594;
	setAttr ".wl[348].w[5]" 0.029137169462073596;
	setAttr ".wl[348].w[7]" 0.016228045937375982;
	setAttr ".wl[348].w[12]" 0.46630965217353509;
	setAttr -s 5 ".wl[349].w";
	setAttr ".wl[349].w[3]" 0.026827983827981836;
	setAttr ".wl[349].w[4]" 0.47191676784566439;
	setAttr ".wl[349].w[5]" 0.027461289215920334;
	setAttr ".wl[349].w[7]" 0.012574580571815935;
	setAttr ".wl[349].w[12]" 0.46121937853861755;
	setAttr -s 5 ".wl[350].w";
	setAttr ".wl[350].w[3]" 0.064357494572199664;
	setAttr ".wl[350].w[4]" 0.45366230171928323;
	setAttr ".wl[350].w[5]" 0.047573917352998242;
	setAttr ".wl[350].w[7]" 0.016520589544290368;
	setAttr ".wl[350].w[12]" 0.41788569681122861;
	setAttr -s 5 ".wl[351].w";
	setAttr ".wl[351].w[3]" 0.14108088718504502;
	setAttr ".wl[351].w[4]" 0.41138788263668286;
	setAttr ".wl[351].w[5]" 0.093074359915685703;
	setAttr ".wl[351].w[7]" 0.022533332032274804;
	setAttr ".wl[351].w[12]" 0.33192353823031173;
	setAttr -s 5 ".wl[352].w";
	setAttr ".wl[352].w[3]" 0.22289232147832741;
	setAttr ".wl[352].w[4]" 0.38407557377762097;
	setAttr ".wl[352].w[5]" 0.14727460746625023;
	setAttr ".wl[352].w[9]" 0.034411176238977652;
	setAttr ".wl[352].w[12]" 0.21134632103882375;
	setAttr -s 5 ".wl[353].w";
	setAttr ".wl[353].w[3]" 0.24263764858349765;
	setAttr ".wl[353].w[4]" 0.42777199675280003;
	setAttr ".wl[353].w[5]" 0.16225503420764237;
	setAttr ".wl[353].w[9]" 0.052933080888046852;
	setAttr ".wl[353].w[12]" 0.11440223956801304;
	setAttr -s 5 ".wl[354].w";
	setAttr ".wl[354].w[3]" 0.24622544441423361;
	setAttr ".wl[354].w[4]" 0.43348430416634021;
	setAttr ".wl[354].w[5]" 0.16656597401241788;
	setAttr ".wl[354].w[9]" 0.078856800554995665;
	setAttr ".wl[354].w[12]" 0.074867476852012546;
	setAttr -s 5 ".wl[355].w";
	setAttr ".wl[355].w[3]" 0.2409719940476609;
	setAttr ".wl[355].w[4]" 0.42290863488899866;
	setAttr ".wl[355].w[5]" 0.16281741798483435;
	setAttr ".wl[355].w[9]" 0.12225098708568541;
	setAttr ".wl[355].w[12]" 0.051050965992820704;
	setAttr -s 5 ".wl[356].w";
	setAttr ".wl[356].w[3]" 0.21849620656867247;
	setAttr ".wl[356].w[4]" 0.37316084725123139;
	setAttr ".wl[356].w[5]" 0.14759116305218353;
	setAttr ".wl[356].w[9]" 0.2270991676280415;
	setAttr ".wl[356].w[12]" 0.033652615499871148;
	setAttr -s 5 ".wl[357].w";
	setAttr ".wl[357].w[3]" 0.1328985529114006;
	setAttr ".wl[357].w[4]" 0.40915118312247839;
	setAttr ".wl[357].w[5]" 0.090640457300958696;
	setAttr ".wl[357].w[6]" 0.023421680312375948;
	setAttr ".wl[357].w[9]" 0.34388812635278643;
	setAttr -s 5 ".wl[358].w";
	setAttr ".wl[358].w[3]" 0.060556507488894046;
	setAttr ".wl[358].w[4]" 0.44858190260972503;
	setAttr ".wl[358].w[5]" 0.047088776372736924;
	setAttr ".wl[358].w[6]" 0.017400444717129721;
	setAttr ".wl[358].w[9]" 0.42637236881151425;
	setAttr -s 5 ".wl[359].w";
	setAttr ".wl[359].w[3]" 0.025952991471601451;
	setAttr ".wl[359].w[4]" 0.467340161043519;
	setAttr ".wl[359].w[5]" 0.028199128264747166;
	setAttr ".wl[359].w[6]" 0.013715169943766403;
	setAttr ".wl[359].w[9]" 0.46479254927636593;
	setAttr -s 5 ".wl[360].w";
	setAttr ".wl[360].w[3]" 0.018207364676396009;
	setAttr ".wl[360].w[4]" 0.4665848470309914;
	setAttr ".wl[360].w[5]" 0.030574115936446402;
	setAttr ".wl[360].w[6]" 0.018055620550934532;
	setAttr ".wl[360].w[9]" 0.46657805180523165;
	setAttr -s 5 ".wl[361].w";
	setAttr ".wl[361].w[3]" 0.023882925441886159;
	setAttr ".wl[361].w[4]" 0.43024075157084024;
	setAttr ".wl[361].w[5]" 0.070209959010478495;
	setAttr ".wl[361].w[6]" 0.047771611646797642;
	setAttr ".wl[361].w[9]" 0.42789475232999735;
	setAttr -s 5 ".wl[362].w";
	setAttr ".wl[362].w[4]" 0.26929469121979838;
	setAttr ".wl[362].w[5]" 0.24463065550642366;
	setAttr ".wl[362].w[6]" 0.19202106630720267;
	setAttr ".wl[362].w[8]" 0.038096555988795473;
	setAttr ".wl[362].w[9]" 0.25595703097777983;
	setAttr -s 5 ".wl[363].w";
	setAttr ".wl[363].w[4]" 0.090595104733610654;
	setAttr ".wl[363].w[5]" 0.40438199312682394;
	setAttr ".wl[363].w[6]" 0.34582467071890016;
	setAttr ".wl[363].w[8]" 0.083058213177880938;
	setAttr ".wl[363].w[9]" 0.076140018242784366;
	setAttr -s 5 ".wl[364].w";
	setAttr ".wl[364].w[4]" 0.050854195731088049;
	setAttr ".wl[364].w[5]" 0.34415553346482791;
	setAttr ".wl[364].w[6]" 0.27876673203614943;
	setAttr ".wl[364].w[8]" 0.29527899929961998;
	setAttr ".wl[364].w[9]" 0.030944539468314561;
	setAttr -s 5 ".wl[365].w";
	setAttr ".wl[365].w[3]" 0.0099862369093330575;
	setAttr ".wl[365].w[4]" 0.017529165384644673;
	setAttr ".wl[365].w[5]" 0.47011159507330291;
	setAttr ".wl[365].w[6]" 0.038734643789392093;
	setAttr ".wl[365].w[8]" 0.46363835884332721;
	setAttr -s 5 ".wl[366].w[4:8]"  0.0078181038529477279 0.48916042329521009 
		0.0078899442810399303 0.0068902311979944066 0.4882412973728078;
	setAttr -s 5 ".wl[367].w";
	setAttr ".wl[367].w[3]" 0.0089433981425967538;
	setAttr ".wl[367].w[4]" 0.01577022532532037;
	setAttr ".wl[367].w[5]" 0.47653127077753826;
	setAttr ".wl[367].w[7]" 0.029051794212972052;
	setAttr ".wl[367].w[8]" 0.46970331154157263;
	setAttr -s 5 ".wl[368].w";
	setAttr ".wl[368].w[3]" 0.030495348069928419;
	setAttr ".wl[368].w[4]" 0.052556299942009749;
	setAttr ".wl[368].w[5]" 0.35261044142309189;
	setAttr ".wl[368].w[7]" 0.23331092193878389;
	setAttr ".wl[368].w[8]" 0.33102698862618612;
	setAttr -s 5 ".wl[369].w";
	setAttr ".wl[369].w[4]" 0.0015961485042904665;
	setAttr ".wl[369].w[5]" 0.86999522151105713;
	setAttr ".wl[369].w[7]" 0.12398069608039253;
	setAttr ".wl[369].w[8]" 0.0036371972959193394;
	setAttr ".wl[369].w[12]" 0.00079073660834060359;
	setAttr -s 5 ".wl[370].w";
	setAttr ".wl[370].w[4]" 0.021363674045045408;
	setAttr ".wl[370].w[5]" 0.70947831458015531;
	setAttr ".wl[370].w[7]" 0.23698134243847951;
	setAttr ".wl[370].w[8]" 0.015105268581656305;
	setAttr ".wl[370].w[12]" 0.017071400354663608;
	setAttr -s 5 ".wl[371].w";
	setAttr ".wl[371].w[4]" 0.10689378859928737;
	setAttr ".wl[371].w[5]" 0.54922885422506607;
	setAttr ".wl[371].w[7]" 0.20784401886303766;
	setAttr ".wl[371].w[8]" 0.029361658686548654;
	setAttr ".wl[371].w[12]" 0.10667167962606026;
	setAttr -s 5 ".wl[372].w";
	setAttr ".wl[372].w[3]" 0.041911001190325009;
	setAttr ".wl[372].w[4]" 0.21265020919982724;
	setAttr ".wl[372].w[5]" 0.39265350433175084;
	setAttr ".wl[372].w[7]" 0.1401389529751125;
	setAttr ".wl[372].w[12]" 0.21264633230298444;
	setAttr -s 5 ".wl[373].w";
	setAttr ".wl[373].w[3]" 0.059736239785725578;
	setAttr ".wl[373].w[4]" 0.26865743304785689;
	setAttr ".wl[373].w[5]" 0.31100260972849619;
	setAttr ".wl[373].w[7]" 0.09250446168263568;
	setAttr ".wl[373].w[12]" 0.26809925575528554;
	setAttr -s 5 ".wl[374].w";
	setAttr ".wl[374].w[3]" 0.082485596080408036;
	setAttr ".wl[374].w[4]" 0.32016669098433781;
	setAttr ".wl[374].w[5]" 0.27934961421041016;
	setAttr ".wl[374].w[7]" 0.062147492084883564;
	setAttr ".wl[374].w[12]" 0.2558506066399604;
	setAttr -s 5 ".wl[375].w";
	setAttr ".wl[375].w[3]" 0.10077243152644076;
	setAttr ".wl[375].w[4]" 0.37402782072203977;
	setAttr ".wl[375].w[5]" 0.29642037414241057;
	setAttr ".wl[375].w[7]" 0.043455856422324052;
	setAttr ".wl[375].w[12]" 0.18532351718678469;
	setAttr -s 5 ".wl[376].w";
	setAttr ".wl[376].w[3]" 0.10787539681064155;
	setAttr ".wl[376].w[4]" 0.42000381215200089;
	setAttr ".wl[376].w[5]" 0.33139468122171439;
	setAttr ".wl[376].w[7]" 0.030182350461834019;
	setAttr ".wl[376].w[12]" 0.1105437593538091;
	setAttr -s 5 ".wl[377].w";
	setAttr ".wl[377].w[3]" 0.10861244709226771;
	setAttr ".wl[377].w[4]" 0.44034696772732074;
	setAttr ".wl[377].w[5]" 0.34750516932126924;
	setAttr ".wl[377].w[9]" 0.036214002566929672;
	setAttr ".wl[377].w[12]" 0.067321413292212695;
	setAttr -s 5 ".wl[378].w";
	setAttr ".wl[378].w[3]" 0.10956450591270148;
	setAttr ".wl[378].w[4]" 0.44207046630546748;
	setAttr ".wl[378].w[5]" 0.35061763191363127;
	setAttr ".wl[378].w[9]" 0.0495247229802111;
	setAttr ".wl[378].w[12]" 0.048222672887988656;
	setAttr -s 5 ".wl[379].w";
	setAttr ".wl[379].w[3]" 0.10897685889060177;
	setAttr ".wl[379].w[4]" 0.43800225370759716;
	setAttr ".wl[379].w[5]" 0.34731462027176269;
	setAttr ".wl[379].w[9]" 0.069962095670269189;
	setAttr ".wl[379].w[12]" 0.035744171459769096;
	setAttr -s 5 ".wl[380].w";
	setAttr ".wl[380].w[3]" 0.1082528689631241;
	setAttr ".wl[380].w[4]" 0.41414355554888055;
	setAttr ".wl[380].w[5]" 0.33007306085217142;
	setAttr ".wl[380].w[6]" 0.032050774626901646;
	setAttr ".wl[380].w[9]" 0.11547974000892235;
	setAttr -s 5 ".wl[381].w";
	setAttr ".wl[381].w[3]" 0.10096129745711775;
	setAttr ".wl[381].w[4]" 0.3659035204999786;
	setAttr ".wl[381].w[5]" 0.29412665174749192;
	setAttr ".wl[381].w[6]" 0.046480218033188532;
	setAttr ".wl[381].w[9]" 0.19252831226222322;
	setAttr -s 5 ".wl[382].w";
	setAttr ".wl[382].w[3]" 0.082492056391583649;
	setAttr ".wl[382].w[4]" 0.31038081195312939;
	setAttr ".wl[382].w[5]" 0.27795190949864579;
	setAttr ".wl[382].w[6]" 0.066742274499485124;
	setAttr ".wl[382].w[9]" 0.26243294765715613;
	setAttr -s 5 ".wl[383].w";
	setAttr ".wl[383].w[3]" 0.059118939821912436;
	setAttr ".wl[383].w[4]" 0.26865975428833633;
	setAttr ".wl[383].w[5]" 0.30522261758533425;
	setAttr ".wl[383].w[6]" 0.098411263533924986;
	setAttr ".wl[383].w[9]" 0.26858742477049197;
	setAttr -s 5 ".wl[384].w";
	setAttr ".wl[384].w[3]" 0.041686944500580324;
	setAttr ".wl[384].w[4]" 0.21287755767896038;
	setAttr ".wl[384].w[5]" 0.38318348358578153;
	setAttr ".wl[384].w[6]" 0.14937445655571741;
	setAttr ".wl[384].w[9]" 0.21287755767896038;
	setAttr -s 5 ".wl[385].w";
	setAttr ".wl[385].w[4]" 0.10850197378639083;
	setAttr ".wl[385].w[5]" 0.52995174412277735;
	setAttr ".wl[385].w[6]" 0.22337120574662678;
	setAttr ".wl[385].w[8]" 0.029702316479960009;
	setAttr ".wl[385].w[9]" 0.10847275986424508;
	setAttr -s 5 ".wl[386].w";
	setAttr ".wl[386].w[4]" 0.021479945777158518;
	setAttr ".wl[386].w[5]" 0.67788715027724067;
	setAttr ".wl[386].w[6]" 0.26710114166252902;
	setAttr ".wl[386].w[8]" 0.015370573468211676;
	setAttr ".wl[386].w[9]" 0.018161188814860219;
	setAttr -s 5 ".wl[387].w";
	setAttr ".wl[387].w[4]" 0.0015147315240580733;
	setAttr ".wl[387].w[5]" 0.83950640592547099;
	setAttr ".wl[387].w[6]" 0.15486102726343684;
	setAttr ".wl[387].w[8]" 0.0033209377551984783;
	setAttr ".wl[387].w[9]" 0.00079689753183561913;
	setAttr -s 5 ".wl[388].w[4:8]"  0.0030073981295536813 0.80896997885633892 
		0.15758374265151029 0.00093906133586186831 0.029499819026735277;
	setAttr -s 5 ".wl[389].w[4:8]"  0.0025158901248784975 0.71700318957224662 
		0.017845533166443672 0.0017483598378215919 0.26088702729860958;
	setAttr -s 5 ".wl[390].w[4:8]"  5.4024309469615446e-006 0.99465072860492343 
		1.1072269204555696e-005 9.4357092976811469e-006 0.005323360985627283;
	setAttr -s 5 ".wl[391].w[4:8]"  0.0017285282769001215 0.75853580223970529 
		0.0013307181032632006 0.0096301141145825192 0.22877483726554881;
	setAttr -s 5 ".wl[392].w[4:8]"  0.0042797313023447492 0.79446246580400215 
		0.0014059603070721916 0.15266138518136693 0.047190457405214041;
	setAttr -s 5 ".wl[393].w";
	setAttr ".wl[393].w[4]" 0.0032035870476004947;
	setAttr ".wl[393].w[5]" 0.88030175928089416;
	setAttr ".wl[393].w[7]" 0.1120550173093208;
	setAttr ".wl[393].w[8]" 0.0038497907311787222;
	setAttr ".wl[393].w[12]" 0.00058984563100586071;
	setAttr -s 5 ".wl[394].w";
	setAttr ".wl[394].w[4]" 0.025221117616363246;
	setAttr ".wl[394].w[5]" 0.76103276914879525;
	setAttr ".wl[394].w[7]" 0.19578322455994807;
	setAttr ".wl[394].w[8]" 0.012073126172162565;
	setAttr ".wl[394].w[12]" 0.0058897625027308628;
	setAttr -s 5 ".wl[395].w";
	setAttr ".wl[395].w[4]" 0.10699467808111765;
	setAttr ".wl[395].w[5]" 0.64362222702800442;
	setAttr ".wl[395].w[7]" 0.19575497556260557;
	setAttr ".wl[395].w[8]" 0.02502633935415232;
	setAttr ".wl[395].w[12]" 0.028601779974120053;
	setAttr -s 5 ".wl[396].w";
	setAttr ".wl[396].w[4]" 0.20994316498907981;
	setAttr ".wl[396].w[5]" 0.54071607999471449;
	setAttr ".wl[396].w[7]" 0.15788141544085982;
	setAttr ".wl[396].w[8]" 0.031641716813828144;
	setAttr ".wl[396].w[12]" 0.05981762276151785;
	setAttr -s 5 ".wl[397].w";
	setAttr ".wl[397].w[3]" 0.033445199682531249;
	setAttr ".wl[397].w[4]" 0.30958383451141097;
	setAttr ".wl[397].w[5]" 0.46228790164008166;
	setAttr ".wl[397].w[7]" 0.11191826227765292;
	setAttr ".wl[397].w[12]" 0.082764801888323344;
	setAttr -s 5 ".wl[398].w";
	setAttr ".wl[398].w[3]" 0.043696212064991156;
	setAttr ".wl[398].w[4]" 0.38538765184975787;
	setAttr ".wl[398].w[5]" 0.40846740354569222;
	setAttr ".wl[398].w[7]" 0.072433993493172769;
	setAttr ".wl[398].w[12]" 0.090014739046386016;
	setAttr -s 5 ".wl[399].w";
	setAttr ".wl[399].w[3]" 0.049597111419282698;
	setAttr ".wl[399].w[4]" 0.41356619172157305;
	setAttr ".wl[399].w[5]" 0.41356619172157305;
	setAttr ".wl[399].w[7]" 0.047101707821799392;
	setAttr ".wl[399].w[12]" 0.07616879731577185;
	setAttr -s 5 ".wl[400].w";
	setAttr ".wl[400].w[3]" 0.04894140912987556;
	setAttr ".wl[400].w[4]" 0.4342776257734105;
	setAttr ".wl[400].w[5]" 0.4342776257734105;
	setAttr ".wl[400].w[7]" 0.030735205993128716;
	setAttr ".wl[400].w[12]" 0.051768133330174736;
	setAttr -s 5 ".wl[401].w";
	setAttr ".wl[401].w[3]" 0.047896755674611546;
	setAttr ".wl[401].w[4]" 0.44698631967167179;
	setAttr ".wl[401].w[5]" 0.44698631967167179;
	setAttr ".wl[401].w[7]" 0.022342049314369217;
	setAttr ".wl[401].w[12]" 0.035788555667675748;
	setAttr -s 5 ".wl[402].w";
	setAttr ".wl[402].w[3]" 0.048302689681530321;
	setAttr ".wl[402].w[4]" 0.44769219495134349;
	setAttr ".wl[402].w[5]" 0.44769219495134349;
	setAttr ".wl[402].w[9]" 0.028286022799815343;
	setAttr ".wl[402].w[12]" 0.028026897615967421;
	setAttr -s 5 ".wl[403].w";
	setAttr ".wl[403].w[3]" 0.048235351165214421;
	setAttr ".wl[403].w[4]" 0.44601969524951257;
	setAttr ".wl[403].w[5]" 0.44601969524951257;
	setAttr ".wl[403].w[6]" 0.023256010435323011;
	setAttr ".wl[403].w[9]" 0.036469247900437429;
	setAttr -s 5 ".wl[404].w";
	setAttr ".wl[404].w[3]" 0.049653234998959697;
	setAttr ".wl[404].w[4]" 0.4323903368769722;
	setAttr ".wl[404].w[5]" 0.43239033687697209;
	setAttr ".wl[404].w[6]" 0.03240350679877968;
	setAttr ".wl[404].w[9]" 0.05316258444831639;
	setAttr -s 5 ".wl[405].w";
	setAttr ".wl[405].w[3]" 0.050530485468387093;
	setAttr ".wl[405].w[4]" 0.41059185702509454;
	setAttr ".wl[405].w[5]" 0.41059185702509443;
	setAttr ".wl[405].w[6]" 0.050022355186558252;
	setAttr ".wl[405].w[9]" 0.078263445294865661;
	setAttr -s 5 ".wl[406].w";
	setAttr ".wl[406].w[3]" 0.044639488172341903;
	setAttr ".wl[406].w[4]" 0.38076697202607729;
	setAttr ".wl[406].w[5]" 0.40518824129880748;
	setAttr ".wl[406].w[6]" 0.077192481144235686;
	setAttr ".wl[406].w[9]" 0.092212817358537577;
	setAttr -s 5 ".wl[407].w";
	setAttr ".wl[407].w[3]" 0.03427973103631532;
	setAttr ".wl[407].w[4]" 0.30531255462203571;
	setAttr ".wl[407].w[5]" 0.45643410369942;
	setAttr ".wl[407].w[6]" 0.1193345556576938;
	setAttr ".wl[407].w[9]" 0.084639054984535153;
	setAttr -s 5 ".wl[408].w";
	setAttr ".wl[408].w[4]" 0.20808317870438001;
	setAttr ".wl[408].w[5]" 0.52938718656181316;
	setAttr ".wl[408].w[6]" 0.16839351222718271;
	setAttr ".wl[408].w[8]" 0.032725454961550909;
	setAttr ".wl[408].w[9]" 0.061410667545073343;
	setAttr -s 5 ".wl[409].w";
	setAttr ".wl[409].w[4]" 0.10808581033351761;
	setAttr ".wl[409].w[5]" 0.62440527921800204;
	setAttr ".wl[409].w[6]" 0.21141690128636415;
	setAttr ".wl[409].w[8]" 0.026130727675906847;
	setAttr ".wl[409].w[9]" 0.029961281486209395;
	setAttr -s 5 ".wl[410].w";
	setAttr ".wl[410].w[4]" 0.026983984842432012;
	setAttr ".wl[410].w[5]" 0.73080243330112404;
	setAttr ".wl[410].w[6]" 0.22259611665418597;
	setAttr ".wl[410].w[8]" 0.013083755206359168;
	setAttr ".wl[410].w[9]" 0.0065337099958987866;
	setAttr -s 5 ".wl[411].w";
	setAttr ".wl[411].w[4]" 0.0036593802912230165;
	setAttr ".wl[411].w[5]" 0.84764779549695091;
	setAttr ".wl[411].w[6]" 0.1436639121086847;
	setAttr ".wl[411].w[8]" 0.0043315943558895429;
	setAttr ".wl[411].w[9]" 0.00069731774725186697;
	setAttr -s 5 ".wl[412].w[4:8]"  0.0058611826238342086 0.83539872827142447 
		0.13949738857951724 0.0017288531013383811 0.017513847423885707;
	setAttr -s 5 ".wl[413].w[4:8]"  0.031759755339801077 0.56218221442229954 
		0.15798979063347618 0.020105091060862213 0.22796314854356098;
	setAttr -s 5 ".wl[414].w[4:8]"  0.025169970465220612 0.63207928152133963 
		0.042363257866349993 0.037705184001583285 0.26268230614550642;
	setAttr -s 5 ".wl[415].w[4:8]"  0.032159903666068919 0.56941548690423316 
		0.022043975496390054 0.13627210734391729 0.24010852658939061;
	setAttr -s 5 ".wl[416].w[4:8]"  0.0065793301070293016 0.84293428143894611 
		0.0020268477101307641 0.12800183281556904 0.020457707928324881;
	setAttr -s 5 ".wl[417].w";
	setAttr ".wl[417].w[4]" 0.00044774911977698976;
	setAttr ".wl[417].w[5]" 0.00020998951714020782;
	setAttr ".wl[417].w[12]" 0.0032706390986058163;
	setAttr ".wl[417].w[13]" 0.49803581113223849;
	setAttr ".wl[417].w[14]" 0.49803581113223849;
	setAttr -s 5 ".wl[418].w";
	setAttr ".wl[418].w[4]" 0.0010433054051929554;
	setAttr ".wl[418].w[5]" 0.00042997427378341441;
	setAttr ".wl[418].w[12]" 0.0088651682675609245;
	setAttr ".wl[418].w[13]" 0.49618379190169443;
	setAttr ".wl[418].w[14]" 0.4934777601517682;
	setAttr -s 5 ".wl[419].w";
	setAttr ".wl[419].w[4]" 0.0014557792788867212;
	setAttr ".wl[419].w[5]" 0.00057802819547114032;
	setAttr ".wl[419].w[12]" 0.011999635759569133;
	setAttr ".wl[419].w[13]" 0.49438950389601133;
	setAttr ".wl[419].w[14]" 0.49157705287006176;
	setAttr -s 5 ".wl[420].w";
	setAttr ".wl[420].w[4]" 0.0013768752575190081;
	setAttr ".wl[420].w[5]" 0.00057485493372888552;
	setAttr ".wl[420].w[12]" 0.011348218329458624;
	setAttr ".wl[420].w[13]" 0.49440151787888847;
	setAttr ".wl[420].w[14]" 0.49229853360040499;
	setAttr -s 5 ".wl[421].w";
	setAttr ".wl[421].w[4]" 0.00058338226089661119;
	setAttr ".wl[421].w[5]" 0.00027570673860108044;
	setAttr ".wl[421].w[12]" 0.004191616363528528;
	setAttr ".wl[421].w[13]" 0.4974746473184869;
	setAttr ".wl[421].w[14]" 0.4974746473184869;
	setAttr -s 5 ".wl[422].w";
	setAttr ".wl[422].w[4]" 0.00017225677331034832;
	setAttr ".wl[422].w[5]" 9.0128839218632251e-005;
	setAttr ".wl[422].w[12]" 0.0012112630664183823;
	setAttr ".wl[422].w[13]" 0.49926317566052619;
	setAttr ".wl[422].w[14]" 0.49926317566052641;
	setAttr -s 5 ".wl[423].w";
	setAttr ".wl[423].w[4]" 0.00036775124178030702;
	setAttr ".wl[423].w[5]" 0.00022220633705019645;
	setAttr ".wl[423].w[12]" 0.0027004682875127191;
	setAttr ".wl[423].w[13]" 0.49835478706682845;
	setAttr ".wl[423].w[14]" 0.49835478706682845;
	setAttr -s 5 ".wl[424].w";
	setAttr ".wl[424].w[4]" 0.00015730338058161681;
	setAttr ".wl[424].w[5]" 8.8658420337889689e-005;
	setAttr ".wl[424].w[12]" 0.0011077440527187141;
	setAttr ".wl[424].w[13]" 0.49932314707318076;
	setAttr ".wl[424].w[14]" 0.49932314707318098;
	setAttr -s 5 ".wl[425].w";
	setAttr ".wl[425].w[4]" 0.00055561357245466662;
	setAttr ".wl[425].w[5]" 0.00028211556684872572;
	setAttr ".wl[425].w[12]" 0.0039981999906169869;
	setAttr ".wl[425].w[13]" 0.49758203543503982;
	setAttr ".wl[425].w[14]" 0.49758203543503982;
	setAttr -s 5 ".wl[426].w";
	setAttr ".wl[426].w[4]" 0.0013268583660948085;
	setAttr ".wl[426].w[5]" 0.00060092143936806713;
	setAttr ".wl[426].w[12]" 0.010958871170946086;
	setAttr ".wl[426].w[13]" 0.49456295296758124;
	setAttr ".wl[426].w[14]" 0.49255039605600981;
	setAttr -s 5 ".wl[427].w";
	setAttr ".wl[427].w[4]" 0.0012920372481471942;
	setAttr ".wl[427].w[5]" 0.00067231599410760563;
	setAttr ".wl[427].w[12]" 0.010722797604934933;
	setAttr ".wl[427].w[13]" 0.49490661173679829;
	setAttr ".wl[427].w[14]" 0.49240623741601197;
	setAttr -s 5 ".wl[428].w";
	setAttr ".wl[428].w[4]" 0.00085903385912771552;
	setAttr ".wl[428].w[5]" 0.00051670841701809426;
	setAttr ".wl[428].w[12]" 0.0073680745362917121;
	setAttr ".wl[428].w[13]" 0.49676695729043235;
	setAttr ".wl[428].w[14]" 0.49448922589713007;
	setAttr -s 5 ".wl[429].w";
	setAttr ".wl[429].w[4]" 0.0006469381145725717;
	setAttr ".wl[429].w[5]" 0.00032801124959846745;
	setAttr ".wl[429].w[12]" 0.0057395519016679468;
	setAttr ".wl[429].w[13]" 0.49881688944038199;
	setAttr ".wl[429].w[14]" 0.49446860929377906;
	setAttr -s 5 ".wl[430].w";
	setAttr ".wl[430].w[4]" 0.00080472971628946276;
	setAttr ".wl[430].w[5]" 0.00035687285646072737;
	setAttr ".wl[430].w[12]" 0.0069952789689715399;
	setAttr ".wl[430].w[13]" 0.49747635996516881;
	setAttr ".wl[430].w[14]" 0.49436675849310946;
	setAttr -s 5 ".wl[431].w";
	setAttr ".wl[431].w[4]" 0.00031782196995751381;
	setAttr ".wl[431].w[5]" 0.00015909283763625376;
	setAttr ".wl[431].w[12]" 0.0023606565659668563;
	setAttr ".wl[431].w[13]" 0.49858121431321967;
	setAttr ".wl[431].w[14]" 0.49858121431321967;
	setAttr -s 5 ".wl[432].w";
	setAttr ".wl[432].w[4]" 9.6310569163702718e-005;
	setAttr ".wl[432].w[5]" 5.3936277741175375e-005;
	setAttr ".wl[432].w[12]" 0.00068804948766477145;
	setAttr ".wl[432].w[13]" 0.49958085183271522;
	setAttr ".wl[432].w[14]" 0.49958085183271522;
	setAttr -s 5 ".wl[433].w";
	setAttr ".wl[433].w[4]" 0.00016625371924580177;
	setAttr ".wl[433].w[5]" 9.9025777179773087e-005;
	setAttr ".wl[433].w[12]" 0.0012639175185781876;
	setAttr ".wl[433].w[13]" 0.49923540149249795;
	setAttr ".wl[433].w[14]" 0.49923540149249818;
	setAttr -s 5 ".wl[434].w";
	setAttr ".wl[434].w[4]" 0.00040313357254547134;
	setAttr ".wl[434].w[5]" 0.00023738936715437969;
	setAttr ".wl[434].w[12]" 0.0036672210502114431;
	setAttr ".wl[434].w[13]" 0.49986667697182918;
	setAttr ".wl[434].w[14]" 0.49582557903825958;
	setAttr -s 5 ".wl[435].w";
	setAttr ".wl[435].w[4]" 0.00053477065876432982;
	setAttr ".wl[435].w[5]" 0.00037181032257991952;
	setAttr ".wl[435].w[12]" 0.0047797496805281831;
	setAttr ".wl[435].w[13]" 0.49915878482940107;
	setAttr ".wl[435].w[14]" 0.49515488450872647;
	setAttr -s 5 ".wl[436].w";
	setAttr ".wl[436].w[4]" 0.00037470341075482696;
	setAttr ".wl[436].w[5]" 0.00024382637982419769;
	setAttr ".wl[436].w[12]" 0.0034165864173985204;
	setAttr ".wl[436].w[13]" 0.49995097274573819;
	setAttr ".wl[436].w[14]" 0.49601391104628428;
	setAttr -s 5 ".wl[437].w";
	setAttr ".wl[437].w[4]" 0.00015108325727945806;
	setAttr ".wl[437].w[5]" 9.7706813549009697e-005;
	setAttr ".wl[437].w[12]" 0.0011504692936375408;
	setAttr ".wl[437].w[13]" 0.49930037031776697;
	setAttr ".wl[437].w[14]" 0.49930037031776697;
	setAttr -s 5 ".wl[438].w";
	setAttr ".wl[438].w[4]" 8.5112211232742351e-005;
	setAttr ".wl[438].w[5]" 5.1501056782223502e-005;
	setAttr ".wl[438].w[12]" 0.00060896113459107818;
	setAttr ".wl[438].w[13]" 0.49962721279869704;
	setAttr ".wl[438].w[14]" 0.49962721279869693;
	setAttr -s 5 ".wl[439].w";
	setAttr ".wl[439].w[4]" 0.00025039249639690947;
	setAttr ".wl[439].w[5]" 0.00016319091648188408;
	setAttr ".wl[439].w[12]" 0.0018698428753547843;
	setAttr ".wl[439].w[13]" 0.49885828685588324;
	setAttr ".wl[439].w[14]" 0.49885828685588324;
	setAttr -s 5 ".wl[440].w";
	setAttr ".wl[440].w[4]" 0.00064191321780635003;
	setAttr ".wl[440].w[5]" 0.00042323721447342006;
	setAttr ".wl[440].w[12]" 0.0056338769426092323;
	setAttr ".wl[440].w[13]" 0.49797986324370463;
	setAttr ".wl[440].w[14]" 0.49532110938140639;
	setAttr -s 5 ".wl[441].w";
	setAttr ".wl[441].w[4]" 0.0007633350271795136;
	setAttr ".wl[441].w[5]" 0.00033815773403314298;
	setAttr ".wl[441].w[12]" 0.0056096740054510904;
	setAttr ".wl[441].w[13]" 0.49664441661666825;
	setAttr ".wl[441].w[14]" 0.49664441661666803;
	setAttr -s 5 ".wl[442].w";
	setAttr ".wl[442].w[4]" 0.00067203125315952103;
	setAttr ".wl[442].w[5]" 0.00036796194437266062;
	setAttr ".wl[442].w[12]" 0.0049614852008928057;
	setAttr ".wl[442].w[13]" 0.49699926080078766;
	setAttr ".wl[442].w[14]" 0.49699926080078743;
	setAttr -s 5 ".wl[443].w";
	setAttr ".wl[443].w[4]" 0.00031976617854231312;
	setAttr ".wl[443].w[5]" 0.00017173249628867692;
	setAttr ".wl[443].w[12]" 0.0024705262587294621;
	setAttr ".wl[443].w[13]" 0.49851898753321983;
	setAttr ".wl[443].w[14]" 0.49851898753321983;
	setAttr -s 5 ".wl[444].w";
	setAttr ".wl[444].w[4]" 0.00025996241612418673;
	setAttr ".wl[444].w[5]" 0.00017724580637501467;
	setAttr ".wl[444].w[12]" 0.0020183375649740808;
	setAttr ".wl[444].w[13]" 0.49877222710626351;
	setAttr ".wl[444].w[14]" 0.49877222710626329;
	setAttr -s 5 ".wl[445].w";
	setAttr ".wl[445].w[4]" 0.00075148239522716408;
	setAttr ".wl[445].w[5]" 0.00047600101137528915;
	setAttr ".wl[445].w[12]" 0.0065103025817459586;
	setAttr ".wl[445].w[13]" 0.49727620449461635;
	setAttr ".wl[445].w[14]" 0.4949860095170352;
	setAttr -s 5 ".wl[446].w";
	setAttr ".wl[446].w[4]" 0.00040645472175513693;
	setAttr ".wl[446].w[5]" 0.00026853541875849623;
	setAttr ".wl[446].w[12]" 0.0032499898973813715;
	setAttr ".wl[446].w[13]" 0.49803750998105251;
	setAttr ".wl[446].w[14]" 0.49803750998105251;
	setAttr -s 5 ".wl[447].w";
	setAttr ".wl[447].w[4]" 0.00071577586777086589;
	setAttr ".wl[447].w[5]" 0.00033724473090379587;
	setAttr ".wl[447].w[12]" 0.0062946852364345034;
	setAttr ".wl[447].w[13]" 0.49823308660762022;
	setAttr ".wl[447].w[14]" 0.49441920755727065;
	setAttr -s 5 ".wl[448].w";
	setAttr ".wl[448].w[4]" 0.00049742594129516249;
	setAttr ".wl[448].w[5]" 0.00027247548294902365;
	setAttr ".wl[448].w[12]" 0.0044827391043656016;
	setAttr ".wl[448].w[13]" 0.49957142127654752;
	setAttr ".wl[448].w[14]" 0.49517593819484279;
	setAttr -s 5 ".wl[449].w";
	setAttr ".wl[449].w[4]" 0.00025235619474456143;
	setAttr ".wl[449].w[5]" 0.00015072482856967018;
	setAttr ".wl[449].w[12]" 0.0020731784549876515;
	setAttr ".wl[449].w[13]" 0.49876187026084906;
	setAttr ".wl[449].w[14]" 0.49876187026084906;
	setAttr -s 5 ".wl[450].w";
	setAttr ".wl[450].w[4]" 0.00014426487311562498;
	setAttr ".wl[450].w[5]" 8.9806843952588167e-005;
	setAttr ".wl[450].w[12]" 0.0010955824984499995;
	setAttr ".wl[450].w[13]" 0.49933517289224089;
	setAttr ".wl[450].w[14]" 0.49933517289224089;
	setAttr -s 5 ".wl[451].w";
	setAttr ".wl[451].w[4]" 0.00023199437146643781;
	setAttr ".wl[451].w[5]" 0.00015162909978492646;
	setAttr ".wl[451].w[12]" 0.0019095627019500919;
	setAttr ".wl[451].w[13]" 0.4988534069133993;
	setAttr ".wl[451].w[14]" 0.4988534069133993;
	setAttr -s 5 ".wl[452].w";
	setAttr ".wl[452].w[4]" 0.00038191032524818381;
	setAttr ".wl[452].w[5]" 0.00023767041402493537;
	setAttr ".wl[452].w[12]" 0.0034787741223676616;
	setAttr ".wl[452].w[13]" 0.49985223826494141;
	setAttr ".wl[452].w[14]" 0.49604940687341786;
	setAttr -s 5 ".wl[453].w";
	setAttr ".wl[453].w[4]" 0.00010767950004279684;
	setAttr ".wl[453].w[5]" 6.244706486174745e-005;
	setAttr ".wl[453].w[12]" 0.00078718717177725785;
	setAttr ".wl[453].w[13]" 0.49952134313165913;
	setAttr ".wl[453].w[14]" 0.49952134313165913;
	setAttr -s 5 ".wl[454].w";
	setAttr ".wl[454].w[4]" 8.0669724094247259e-005;
	setAttr ".wl[454].w[5]" 4.707502527539316e-005;
	setAttr ".wl[454].w[12]" 0.00057347181514541697;
	setAttr ".wl[454].w[13]" 0.49964939171774242;
	setAttr ".wl[454].w[14]" 0.49964939171774242;
	setAttr -s 5 ".wl[455].w";
	setAttr ".wl[455].w[4]" 9.5817444664834624e-005;
	setAttr ".wl[455].w[5]" 6.012973933382005e-005;
	setAttr ".wl[455].w[12]" 0.00070154680046536982;
	setAttr ".wl[455].w[13]" 0.49957125300776806;
	setAttr ".wl[455].w[14]" 0.49957125300776795;
	setAttr -s 5 ".wl[456].w";
	setAttr ".wl[456].w[4]" 0.0004302543454693849;
	setAttr ".wl[456].w[5]" 0.00029229120871152384;
	setAttr ".wl[456].w[12]" 0.0038970238000495764;
	setAttr ".wl[456].w[13]" 0.4997746599989219;
	setAttr ".wl[456].w[14]" 0.49560577064684758;
	setAttr -s 5 ".wl[457].w";
	setAttr ".wl[457].w[4]" 0.00057441078083346063;
	setAttr ".wl[457].w[5]" 0.00039295527264763446;
	setAttr ".wl[457].w[12]" 0.0050966776160780844;
	setAttr ".wl[457].w[13]" 0.49866464929170767;
	setAttr ".wl[457].w[14]" 0.49527130703873312;
	setAttr -s 5 ".wl[458].w";
	setAttr ".wl[458].w[4]" 0.00062059561455212104;
	setAttr ".wl[458].w[5]" 0.00028147589451425406;
	setAttr ".wl[458].w[12]" 0.0045121565414445635;
	setAttr ".wl[458].w[13]" 0.49729288597474469;
	setAttr ".wl[458].w[14]" 0.49729288597474447;
	setAttr -s 5 ".wl[459].w";
	setAttr ".wl[459].w[4]" 0.00068915585309110004;
	setAttr ".wl[459].w[5]" 0.00031366825081444931;
	setAttr ".wl[459].w[12]" 0.0049734582720136929;
	setAttr ".wl[459].w[13]" 0.49701185881204024;
	setAttr ".wl[459].w[14]" 0.49701185881204046;
	setAttr -s 5 ".wl[460].w";
	setAttr ".wl[460].w[4]" 0.0010623611640206139;
	setAttr ".wl[460].w[5]" 0.00044814995790990597;
	setAttr ".wl[460].w[12]" 0.0081060295195414609;
	setAttr ".wl[460].w[13]" 0.49519172967926406;
	setAttr ".wl[460].w[14]" 0.49519172967926395;
	setAttr -s 5 ".wl[461].w";
	setAttr ".wl[461].w[4]" 0.00053082326307697398;
	setAttr ".wl[461].w[5]" 0.00030473268576493778;
	setAttr ".wl[461].w[12]" 0.0038787207810482387;
	setAttr ".wl[461].w[13]" 0.49764286163505494;
	setAttr ".wl[461].w[14]" 0.49764286163505494;
	setAttr -s 5 ".wl[462].w";
	setAttr ".wl[462].w[4]" 0.00093806525123853255;
	setAttr ".wl[462].w[5]" 0.00050426973186263864;
	setAttr ".wl[462].w[12]" 0.0071975046247081753;
	setAttr ".wl[462].w[13]" 0.49568008019609516;
	setAttr ".wl[462].w[14]" 0.49568008019609539;
	setAttr -s 5 ".wl[463].w";
	setAttr ".wl[463].w[4]" 0.00062596269324135113;
	setAttr ".wl[463].w[5]" 0.00033189444411490084;
	setAttr ".wl[463].w[12]" 0.0045321725657619947;
	setAttr ".wl[463].w[13]" 0.49725498514844091;
	setAttr ".wl[463].w[14]" 0.49725498514844091;
	setAttr -s 5 ".wl[464].w";
	setAttr ".wl[464].w[4]" 0.00045539160576135598;
	setAttr ".wl[464].w[5]" 0.0002393344063451255;
	setAttr ".wl[464].w[12]" 0.0036906994363356161;
	setAttr ".wl[464].w[13]" 0.49780728727577911;
	setAttr ".wl[464].w[14]" 0.49780728727577889;
	setAttr -s 5 ".wl[465].w";
	setAttr ".wl[465].w[4]" 0.00024273680444081665;
	setAttr ".wl[465].w[5]" 0.00013693054013350959;
	setAttr ".wl[465].w[12]" 0.0018487712715885612;
	setAttr ".wl[465].w[13]" 0.49888578069191858;
	setAttr ".wl[465].w[14]" 0.49888578069191858;
	setAttr -s 5 ".wl[466].w";
	setAttr ".wl[466].w[4]" 0.0003189151237172937;
	setAttr ".wl[466].w[5]" 0.00016616669577298139;
	setAttr ".wl[466].w[12]" 0.0024027830979513934;
	setAttr ".wl[466].w[13]" 0.49855606754127918;
	setAttr ".wl[466].w[14]" 0.49855606754127918;
	setAttr -s 5 ".wl[467].w";
	setAttr ".wl[467].w[4]" 0.00037246400857820301;
	setAttr ".wl[467].w[5]" 0.0002585310208859851;
	setAttr ".wl[467].w[12]" 0.0030366999279764178;
	setAttr ".wl[467].w[13]" 0.49816615252127955;
	setAttr ".wl[467].w[14]" 0.49816615252127977;
	setAttr -s 5 ".wl[468].w";
	setAttr ".wl[468].w[4]" 0.0002542150360142323;
	setAttr ".wl[468].w[5]" 0.00017124890332553952;
	setAttr ".wl[468].w[12]" 0.0019253124731062471;
	setAttr ".wl[468].w[13]" 0.49882461179377696;
	setAttr ".wl[468].w[14]" 0.49882461179377696;
	setAttr -s 5 ".wl[469].w";
	setAttr ".wl[469].w[4]" 0.00020464984854230987;
	setAttr ".wl[469].w[5]" 0.00013737226464635564;
	setAttr ".wl[469].w[12]" 0.0015641323444273213;
	setAttr ".wl[469].w[13]" 0.49904692277119217;
	setAttr ".wl[469].w[14]" 0.49904692277119195;
	setAttr -s 5 ".wl[470].w";
	setAttr ".wl[470].w[4]" 0.0012457194921829816;
	setAttr ".wl[470].w[5]" 0.00049967194707151229;
	setAttr ".wl[470].w[12]" 0.010426874505092231;
	setAttr ".wl[470].w[13]" 0.49533057305951383;
	setAttr ".wl[470].w[14]" 0.49249716099613949;
	setAttr -s 5 ".wl[471].w";
	setAttr ".wl[471].w[4]" 0.0014289554212596454;
	setAttr ".wl[471].w[5]" 0.0005771213697254846;
	setAttr ".wl[471].w[12]" 0.011771863091537278;
	setAttr ".wl[471].w[13]" 0.49436172848225751;
	setAttr ".wl[471].w[14]" 0.49186033163522008;
	setAttr -s 5 ".wl[472].w";
	setAttr ".wl[472].w[4]" 0.001319302595385427;
	setAttr ".wl[472].w[5]" 0.00063691914873610948;
	setAttr ".wl[472].w[12]" 0.010918152031695227;
	setAttr ".wl[472].w[13]" 0.4947114032851328;
	setAttr ".wl[472].w[14]" 0.49241422293905052;
	setAttr -s 5 ".wl[473].w";
	setAttr ".wl[473].w[4]" 0.0010620340870907423;
	setAttr ".wl[473].w[5]" 0.00059616905429719653;
	setAttr ".wl[473].w[12]" 0.0089645014495143716;
	setAttr ".wl[473].w[13]" 0.49591203635194808;
	setAttr ".wl[473].w[14]" 0.49346525905714955;
	setAttr -s 5 ".wl[474].w";
	setAttr ".wl[474].w[4]" 0.00069341542412907416;
	setAttr ".wl[474].w[5]" 0.00030490876906049476;
	setAttr ".wl[474].w[12]" 0.0053991945805624622;
	setAttr ".wl[474].w[13]" 0.49680124061312403;
	setAttr ".wl[474].w[14]" 0.49680124061312403;
	setAttr -s 5 ".wl[475].w";
	setAttr ".wl[475].w[4]" 0.00036312561409297743;
	setAttr ".wl[475].w[5]" 0.0001757903606443791;
	setAttr ".wl[475].w[12]" 0.0026680627154073342;
	setAttr ".wl[475].w[13]" 0.49839651065492774;
	setAttr ".wl[475].w[14]" 0.49839651065492763;
	setAttr -s 5 ".wl[476].w";
	setAttr ".wl[476].w[4]" 0.00051731217780819549;
	setAttr ".wl[476].w[5]" 0.00024357624221948062;
	setAttr ".wl[476].w[12]" 0.0041057466351704756;
	setAttr ".wl[476].w[13]" 0.49756668247240099;
	setAttr ".wl[476].w[14]" 0.49756668247240088;
	setAttr -s 5 ".wl[477].w";
	setAttr ".wl[477].w[4]" 0.0009307748580342477;
	setAttr ".wl[477].w[5]" 0.00039592940793983669;
	setAttr ".wl[477].w[12]" 0.0079853821957927371;
	setAttr ".wl[477].w[13]" 0.49671289838136901;
	setAttr ".wl[477].w[14]" 0.49397501515686409;
	setAttr -s 5 ".wl[478].w";
	setAttr ".wl[478].w[4]" 0.00026027926522688178;
	setAttr ".wl[478].w[5]" 0.00012938379615648859;
	setAttr ".wl[478].w[12]" 0.001851675761236004;
	setAttr ".wl[478].w[13]" 0.49887933058869038;
	setAttr ".wl[478].w[14]" 0.49887933058869027;
	setAttr -s 5 ".wl[479].w";
	setAttr ".wl[479].w[4]" 0.00012011693746855037;
	setAttr ".wl[479].w[5]" 6.5174470032003794e-005;
	setAttr ".wl[479].w[12]" 0.00084643513227451294;
	setAttr ".wl[479].w[13]" 0.49948413673011249;
	setAttr ".wl[479].w[14]" 0.49948413673011249;
	setAttr -s 5 ".wl[480].w";
	setAttr ".wl[480].w[4]" 0.00016507814676982842;
	setAttr ".wl[480].w[5]" 8.7641327164218863e-005;
	setAttr ".wl[480].w[12]" 0.0011932891804117586;
	setAttr ".wl[480].w[13]" 0.49927699567282707;
	setAttr ".wl[480].w[14]" 0.49927699567282707;
	setAttr -s 5 ".wl[481].w";
	setAttr ".wl[481].w[4]" 0.00031294006291999652;
	setAttr ".wl[481].w[5]" 0.00015616521748839008;
	setAttr ".wl[481].w[12]" 0.0022078545780343591;
	setAttr ".wl[481].w[13]" 0.49866152007077863;
	setAttr ".wl[481].w[14]" 0.49866152007077863;
	setAttr -s 5 ".wl[482].w";
	setAttr ".wl[482].w[4]" 0.00055055543840090617;
	setAttr ".wl[482].w[5]" 0.00026950441556287876;
	setAttr ".wl[482].w[12]" 0.0039483131641624416;
	setAttr ".wl[482].w[13]" 0.49761581349093692;
	setAttr ".wl[482].w[14]" 0.49761581349093692;
	setAttr -s 5 ".wl[483].w";
	setAttr ".wl[483].w[4]" 0.00029290409129404706;
	setAttr ".wl[483].w[5]" 0.00015710989692601366;
	setAttr ".wl[483].w[12]" 0.0020695346776863255;
	setAttr ".wl[483].w[13]" 0.49874022566704684;
	setAttr ".wl[483].w[14]" 0.49874022566704684;
	setAttr -s 5 ".wl[484].w";
	setAttr ".wl[484].w[4]" 0.0001512657541323732;
	setAttr ".wl[484].w[5]" 8.2337687632937361e-005;
	setAttr ".wl[484].w[12]" 0.0010584784534141438;
	setAttr ".wl[484].w[13]" 0.49935395905241026;
	setAttr ".wl[484].w[14]" 0.49935395905241026;
	setAttr -s 5 ".wl[485].w";
	setAttr ".wl[485].w[4]" 0.00092295355895925118;
	setAttr ".wl[485].w[5]" 0.00041018729247955556;
	setAttr ".wl[485].w[12]" 0.0070180307486267038;
	setAttr ".wl[485].w[13]" 0.4958244141999672;
	setAttr ".wl[485].w[14]" 0.4958244141999672;
	setAttr -s 5 ".wl[486].w";
	setAttr ".wl[486].w[4]" 0.0013740662202851581;
	setAttr ".wl[486].w[5]" 0.00059478268348349872;
	setAttr ".wl[486].w[12]" 0.011308249183498814;
	setAttr ".wl[486].w[13]" 0.49432456045280498;
	setAttr ".wl[486].w[14]" 0.49239834145992756;
	setAttr -s 5 ".wl[487].w";
	setAttr ".wl[487].w[4]" 0.00088539719961440878;
	setAttr ".wl[487].w[5]" 0.00042439820876452919;
	setAttr ".wl[487].w[12]" 0.0067442510188127254;
	setAttr ".wl[487].w[13]" 0.49597297678640412;
	setAttr ".wl[487].w[14]" 0.49597297678640412;
	setAttr -s 5 ".wl[488].w";
	setAttr ".wl[488].w[4]" 0.0002203981861710684;
	setAttr ".wl[488].w[5]" 0.00012913969804457048;
	setAttr ".wl[488].w[12]" 0.0015731442315365287;
	setAttr ".wl[488].w[13]" 0.49903865894212396;
	setAttr ".wl[488].w[14]" 0.49903865894212396;
	setAttr -s 5 ".wl[489].w";
	setAttr ".wl[489].w[4]" 0.00029003486369361386;
	setAttr ".wl[489].w[5]" 0.00018258580875315128;
	setAttr ".wl[489].w[12]" 0.0021425444520317932;
	setAttr ".wl[489].w[13]" 0.49869241743776077;
	setAttr ".wl[489].w[14]" 0.49869241743776077;
	setAttr -s 5 ".wl[490].w";
	setAttr ".wl[490].w[4]" 0.00013336424392740713;
	setAttr ".wl[490].w[5]" 8.4019529435824645e-005;
	setAttr ".wl[490].w[12]" 0.00096729684412774533;
	setAttr ".wl[490].w[13]" 0.4994076596912545;
	setAttr ".wl[490].w[14]" 0.4994076596912545;
	setAttr -s 5 ".wl[491].w";
	setAttr ".wl[491].w[4]" 0.00010761656327911993;
	setAttr ".wl[491].w[5]" 6.3001018079554091e-005;
	setAttr ".wl[491].w[12]" 0.00075947018948242296;
	setAttr ".wl[491].w[13]" 0.49953495611457943;
	setAttr ".wl[491].w[14]" 0.49953495611457943;
	setAttr -s 5 ".wl[492].w";
	setAttr ".wl[492].w[4]" 0.0005654357608693187;
	setAttr ".wl[492].w[5]" 0.00034385269124038314;
	setAttr ".wl[492].w[12]" 0.0044348185242248468;
	setAttr ".wl[492].w[13]" 0.49732794651183276;
	setAttr ".wl[492].w[14]" 0.49732794651183276;
	setAttr -s 5 ".wl[493].w";
	setAttr ".wl[493].w[4]" 0.00059352286411588266;
	setAttr ".wl[493].w[5]" 0.00026923409290950564;
	setAttr ".wl[493].w[12]" 0.0046576733432696317;
	setAttr ".wl[493].w[13]" 0.49723978484985254;
	setAttr ".wl[493].w[14]" 0.49723978484985254;
	setAttr -s 5 ".wl[494].w";
	setAttr ".wl[494].w[4]" 0.00019445103800201503;
	setAttr ".wl[494].w[5]" 0.00010000082601468283;
	setAttr ".wl[494].w[12]" 0.0013887882937472075;
	setAttr ".wl[494].w[13]" 0.49915837992111806;
	setAttr ".wl[494].w[14]" 0.49915837992111806;
	setAttr -s 5 ".wl[495].w";
	setAttr ".wl[495].w[4]" 0.00028410302357629593;
	setAttr ".wl[495].w[5]" 0.00014715005734933893;
	setAttr ".wl[495].w[12]" 0.0019978814893131793;
	setAttr ".wl[495].w[13]" 0.49878543271488063;
	setAttr ".wl[495].w[14]" 0.49878543271488063;
	setAttr -s 5 ".wl[496].w";
	setAttr ".wl[496].w[4]" 0.0008987389826347257;
	setAttr ".wl[496].w[5]" 0.00041366400499462857;
	setAttr ".wl[496].w[12]" 0.0068248830477301162;
	setAttr ".wl[496].w[13]" 0.49593135698232022;
	setAttr ".wl[496].w[14]" 0.49593135698232022;
	setAttr -s 5 ".wl[497].w";
	setAttr ".wl[497].w[4]" 0.00015975326912801984;
	setAttr ".wl[497].w[5]" 9.7308032674042939e-005;
	setAttr ".wl[497].w[12]" 0.0011448050208448151;
	setAttr ".wl[497].w[13]" 0.49929906683867664;
	setAttr ".wl[497].w[14]" 0.49929906683867664;
	setAttr -s 5 ".wl[498].w";
	setAttr ".wl[498].w[4]" 0.00047238591328375458;
	setAttr ".wl[498].w[5]" 0.00030075290927452557;
	setAttr ".wl[498].w[12]" 0.0037349896787111161;
	setAttr ".wl[498].w[13]" 0.49774593574936526;
	setAttr ".wl[498].w[14]" 0.49774593574936526;
	setAttr -s 5 ".wl[499].w";
	setAttr ".wl[499].w[4]" 0.00022900611073654349;
	setAttr ".wl[499].w[5]" 0.0001436710819057912;
	setAttr ".wl[499].w[12]" 0.0018821668783673183;
	setAttr ".wl[499].w[13]" 0.49887257796449508;
	setAttr ".wl[499].w[14]" 0.4988725779644953;
	setAttr -s 5 ".wl[500].w";
	setAttr ".wl[500].w[4]" 8.9661490960906608e-005;
	setAttr ".wl[500].w[5]" 5.4224552088790565e-005;
	setAttr ".wl[500].w[12]" 0.00065347061242431478;
	setAttr ".wl[500].w[13]" 0.49960132167226301;
	setAttr ".wl[500].w[14]" 0.49960132167226301;
	setAttr -s 5 ".wl[501].w";
	setAttr ".wl[501].w[4]" 0.00010360115364562916;
	setAttr ".wl[501].w[5]" 5.8543118597941795e-005;
	setAttr ".wl[501].w[12]" 0.00072597169114340179;
	setAttr ".wl[501].w[13]" 0.4995559420183065;
	setAttr ".wl[501].w[14]" 0.4995559420183065;
	setAttr -s 5 ".wl[502].w";
	setAttr ".wl[502].w[4]" 0.00041699311203294249;
	setAttr ".wl[502].w[5]" 0.00019901536152828965;
	setAttr ".wl[502].w[12]" 0.0029653438588594116;
	setAttr ".wl[502].w[13]" 0.49820932383378963;
	setAttr ".wl[502].w[14]" 0.49820932383378963;
	setAttr -s 5 ".wl[503].w";
	setAttr ".wl[503].w[4]" 0.00088241177088535917;
	setAttr ".wl[503].w[5]" 0.00037686542490425327;
	setAttr ".wl[503].w[12]" 0.0068027972080173804;
	setAttr ".wl[503].w[13]" 0.49596896279809649;
	setAttr ".wl[503].w[14]" 0.49596896279809649;
	setAttr -s 5 ".wl[504].w";
	setAttr ".wl[504].w[4]" 0.0010114794657086238;
	setAttr ".wl[504].w[5]" 0.00043442220243407884;
	setAttr ".wl[504].w[12]" 0.0077021804430246598;
	setAttr ".wl[504].w[13]" 0.49542595894441632;
	setAttr ".wl[504].w[14]" 0.49542595894441632;
	setAttr -s 5 ".wl[505].w";
	setAttr ".wl[505].w[4]" 0.00074808186975107698;
	setAttr ".wl[505].w[5]" 0.0004287702239023644;
	setAttr ".wl[505].w[12]" 0.005805867817596135;
	setAttr ".wl[505].w[13]" 0.49650864004437517;
	setAttr ".wl[505].w[14]" 0.49650864004437517;
	setAttr -s 5 ".wl[506].w";
	setAttr ".wl[506].w[4]" 0.0003678062625482578;
	setAttr ".wl[506].w[5]" 0.00020520778634318423;
	setAttr ".wl[506].w[12]" 0.0026239709279671846;
	setAttr ".wl[506].w[13]" 0.4984015075115707;
	setAttr ".wl[506].w[14]" 0.4984015075115707;
	setAttr -s 5 ".wl[507].w";
	setAttr ".wl[507].w[4]" 0.00092830607911315651;
	setAttr ".wl[507].w[5]" 0.0004691754827282029;
	setAttr ".wl[507].w[12]" 0.0070954660195916754;
	setAttr ".wl[507].w[13]" 0.49575352620928353;
	setAttr ".wl[507].w[14]" 0.49575352620928353;
	setAttr -s 5 ".wl[508].w";
	setAttr ".wl[508].w[4]" 0.00033778915827715545;
	setAttr ".wl[508].w[5]" 0.00018956232901901066;
	setAttr ".wl[508].w[12]" 0.0027638028982003107;
	setAttr ".wl[508].w[13]" 0.49835442280725178;
	setAttr ".wl[508].w[14]" 0.49835442280725178;
	setAttr -s 5 ".wl[509].w";
	setAttr ".wl[509].w[4]" 0.00048578949133532093;
	setAttr ".wl[509].w[5]" 0.00024041695369718773;
	setAttr ".wl[509].w[12]" 0.0039018156308572075;
	setAttr ".wl[509].w[13]" 0.49768598896205513;
	setAttr ".wl[509].w[14]" 0.49768598896205513;
	setAttr -s 5 ".wl[510].w";
	setAttr ".wl[510].w[4]" 0.00017785542860175745;
	setAttr ".wl[510].w[5]" 9.7866009982176382e-005;
	setAttr ".wl[510].w[12]" 0.0013095882937303866;
	setAttr ".wl[510].w[13]" 0.49920734513384291;
	setAttr ".wl[510].w[14]" 0.49920734513384291;
	setAttr -s 5 ".wl[511].w";
	setAttr ".wl[511].w[4]" 0.00038519560756474853;
	setAttr ".wl[511].w[5]" 0.0002633250510595275;
	setAttr ".wl[511].w[12]" 0.0031156451575237581;
	setAttr ".wl[511].w[13]" 0.49811791709192599;
	setAttr ".wl[511].w[14]" 0.49811791709192599;
	setAttr -s 5 ".wl[512].w";
	setAttr ".wl[512].w[4]" 0.00028844089167263427;
	setAttr ".wl[512].w[5]" 0.00019606229999217942;
	setAttr ".wl[512].w[12]" 0.0023697248527242538;
	setAttr ".wl[512].w[13]" 0.49857288597780547;
	setAttr ".wl[512].w[14]" 0.49857288597780547;
	setAttr -s 5 ".wl[513].w";
	setAttr ".wl[513].w[4]" 0.00014561812165070405;
	setAttr ".wl[513].w[5]" 9.4996944517354624e-005;
	setAttr ".wl[513].w[12]" 0.0010758181541620634;
	setAttr ".wl[513].w[13]" 0.49934178338983493;
	setAttr ".wl[513].w[14]" 0.49934178338983493;
	setAttr -s 5 ".wl[514].w";
	setAttr ".wl[514].w[2]" 0.033757315379939964;
	setAttr ".wl[514].w[3]" 0.58080646158624993;
	setAttr ".wl[514].w[4]" 0.32303334421649516;
	setAttr ".wl[514].w[9]" 0.015873097546579798;
	setAttr ".wl[514].w[12]" 0.046529781270735204;
	setAttr -s 5 ".wl[515].w";
	setAttr ".wl[515].w[2]" 0.026173896789523633;
	setAttr ".wl[515].w[3]" 0.75291732703384906;
	setAttr ".wl[515].w[4]" 0.19023213619142054;
	setAttr ".wl[515].w[9]" 0.0075859813165230564;
	setAttr ".wl[515].w[12]" 0.023090658668683654;
	setAttr -s 5 ".wl[516].w";
	setAttr ".wl[516].w[2]" 0.011651336156082829;
	setAttr ".wl[516].w[3]" 0.90481408854473755;
	setAttr ".wl[516].w[4]" 0.073661814032221398;
	setAttr ".wl[516].w[9]" 0.0023572732886740764;
	setAttr ".wl[516].w[12]" 0.0075154879782841928;
	setAttr -s 5 ".wl[517].w";
	setAttr ".wl[517].w[2]" 0.020352828108569171;
	setAttr ".wl[517].w[3]" 0.40570875781416765;
	setAttr ".wl[517].w[4]" 0.4701475520469578;
	setAttr ".wl[517].w[9]" 0.025705347695333336;
	setAttr ".wl[517].w[12]" 0.078085514334972198;
	setAttr -s 5 ".wl[518].w";
	setAttr ".wl[518].w[2]" 0.027549724937664555;
	setAttr ".wl[518].w[3]" 0.46066088580270009;
	setAttr ".wl[518].w[4]" 0.42394702782584642;
	setAttr ".wl[518].w[9]" 0.022115043794026162;
	setAttr ".wl[518].w[12]" 0.065727317639762736;
	setAttr -s 5 ".wl[519].w";
	setAttr ".wl[519].w[3]" 0.20898382040785674;
	setAttr ".wl[519].w[4]" 0.64996283537390775;
	setAttr ".wl[519].w[5]" 0.070732246555881548;
	setAttr ".wl[519].w[9]" 0.017487511133524369;
	setAttr ".wl[519].w[12]" 0.052833586528829635;
	setAttr -s 5 ".wl[520].w";
	setAttr ".wl[520].w[3]" 0.31719166899287188;
	setAttr ".wl[520].w[4]" 0.52765802820257546;
	setAttr ".wl[520].w[5]" 0.0541901651082492;
	setAttr ".wl[520].w[9]" 0.025035287288472097;
	setAttr ".wl[520].w[12]" 0.075924850407831271;
	setAttr -s 5 ".wl[521].w";
	setAttr ".wl[521].w[3]" 0.38495504368917155;
	setAttr ".wl[521].w[4]" 0.46431051353542807;
	setAttr ".wl[521].w[5]" 0.036409141665618386;
	setAttr ".wl[521].w[9]" 0.028116869088373943;
	setAttr ".wl[521].w[12]" 0.086208432021408141;
	setAttr -s 5 ".wl[522].w";
	setAttr ".wl[522].w[3]" 0.03415259010774159;
	setAttr ".wl[522].w[4]" 0.92514551031965908;
	setAttr ".wl[522].w[5]" 0.030156009983489416;
	setAttr ".wl[522].w[8]" 0.0024981196762717903;
	setAttr ".wl[522].w[12]" 0.0080477699128379959;
	setAttr -s 5 ".wl[523].w";
	setAttr ".wl[523].w[3]" 0.10263737038824604;
	setAttr ".wl[523].w[4]" 0.80428711356922689;
	setAttr ".wl[523].w[5]" 0.059788565758426011;
	setAttr ".wl[523].w[9]" 0.0080014860104503094;
	setAttr ".wl[523].w[12]" 0.025285464273650592;
	setAttr -s 5 ".wl[524].w";
	setAttr ".wl[524].w[2]" 0.002898586783694387;
	setAttr ".wl[524].w[3]" 0.97208146074227264;
	setAttr ".wl[524].w[4]" 0.02265239065094694;
	setAttr ".wl[524].w[9]" 0.00054582178475286376;
	setAttr ".wl[524].w[12]" 0.0018217400383332965;
	setAttr -s 5 ".wl[525].w";
	setAttr ".wl[525].w[2]" 0.0043874620575593565;
	setAttr ".wl[525].w[3]" 0.96513423315452651;
	setAttr ".wl[525].w[4]" 0.027233261061086134;
	setAttr ".wl[525].w[9]" 0.00076002132687249277;
	setAttr ".wl[525].w[12]" 0.0024850223999554324;
	setAttr -s 5 ".wl[526].w";
	setAttr ".wl[526].w[3]" 0.39353866059242942;
	setAttr ".wl[526].w[4]" 0.47036499499833107;
	setAttr ".wl[526].w[5]" 0.024880835020954912;
	setAttr ".wl[526].w[9]" 0.027420949693879915;
	setAttr ".wl[526].w[12]" 0.083794559694404705;
	setAttr -s 5 ".wl[527].w";
	setAttr ".wl[527].w[3]" 0.011929433600858791;
	setAttr ".wl[527].w[4]" 0.97039501367053249;
	setAttr ".wl[527].w[5]" 0.013770652846163261;
	setAttr ".wl[527].w[8]" 0.0011995930277562423;
	setAttr ".wl[527].w[12]" 0.0027053068546891781;
	setAttr -s 5 ".wl[528].w";
	setAttr ".wl[528].w[3]" 0.0095352644308760448;
	setAttr ".wl[528].w[4]" 0.97557167846516002;
	setAttr ".wl[528].w[5]" 0.011597759848099373;
	setAttr ".wl[528].w[8]" 0.0012933298314187825;
	setAttr ".wl[528].w[12]" 0.0020019674244456304;
	setAttr -s 5 ".wl[529].w";
	setAttr ".wl[529].w[2]" 0.0065569006101506115;
	setAttr ".wl[529].w[3]" 0.9093655574748073;
	setAttr ".wl[529].w[4]" 0.076680777520632584;
	setAttr ".wl[529].w[9]" 0.0016989739682293265;
	setAttr ".wl[529].w[12]" 0.005697790426180023;
	setAttr -s 5 ".wl[530].w";
	setAttr ".wl[530].w[2]" 0.012258557650616838;
	setAttr ".wl[530].w[3]" 0.72150368294804068;
	setAttr ".wl[530].w[4]" 0.24317931591261849;
	setAttr ".wl[530].w[9]" 0.0052697211719652734;
	setAttr ".wl[530].w[12]" 0.017788722316758671;
	setAttr -s 5 ".wl[531].w";
	setAttr ".wl[531].w[2]" 0.010610243156834332;
	setAttr ".wl[531].w[3]" 0.51259660050453815;
	setAttr ".wl[531].w[4]" 0.43810671928098238;
	setAttr ".wl[531].w[5]" 0.009255836561382725;
	setAttr ".wl[531].w[12]" 0.029430600496262359;
	setAttr -s 5 ".wl[532].w";
	setAttr ".wl[532].w[3]" 0.40016786668440235;
	setAttr ".wl[532].w[4]" 0.52960076259236688;
	setAttr ".wl[532].w[5]" 0.018051318160975854;
	setAttr ".wl[532].w[8]" 0.016611639486275857;
	setAttr ".wl[532].w[12]" 0.035568413075979077;
	setAttr -s 5 ".wl[533].w";
	setAttr ".wl[533].w[3]" 0.3578228601115695;
	setAttr ".wl[533].w[4]" 0.50139093704876814;
	setAttr ".wl[533].w[5]" 0.056116832263665511;
	setAttr ".wl[533].w[8]" 0.043029379130291118;
	setAttr ".wl[533].w[12]" 0.041639991445705724;
	setAttr -s 5 ".wl[534].w";
	setAttr ".wl[534].w[3]" 0.37609349171258544;
	setAttr ".wl[534].w[4]" 0.52532221165835247;
	setAttr ".wl[534].w[5]" 0.031843699681280778;
	setAttr ".wl[534].w[8]" 0.027738011396212181;
	setAttr ".wl[534].w[12]" 0.039002585551569269;
	setAttr -s 5 ".wl[535].w";
	setAttr ".wl[535].w[3]" 0.24127417610160956;
	setAttr ".wl[535].w[4]" 0.58501195381799298;
	setAttr ".wl[535].w[5]" 0.089412638140958609;
	setAttr ".wl[535].w[8]" 0.050265664108819851;
	setAttr ".wl[535].w[12]" 0.034035567830619159;
	setAttr -s 5 ".wl[536].w";
	setAttr ".wl[536].w[3]" 0.10985091527340334;
	setAttr ".wl[536].w[4]" 0.7488674113811451;
	setAttr ".wl[536].w[5]" 0.092479717454833604;
	setAttr ".wl[536].w[8]" 0.029875538472285287;
	setAttr ".wl[536].w[12]" 0.018926417418332699;
	setAttr -s 5 ".wl[537].w";
	setAttr ".wl[537].w[3]" 0.031640036112670439;
	setAttr ".wl[537].w[4]" 0.9192822214905314;
	setAttr ".wl[537].w[5]" 0.036608887296565654;
	setAttr ".wl[537].w[8]" 0.0064132340279709358;
	setAttr ".wl[537].w[12]" 0.006055621072261705;
	setAttr -s 5 ".wl[538].w";
	setAttr ".wl[538].w[3]" 0.10923732868348461;
	setAttr ".wl[538].w[4]" 0.45120831812758494;
	setAttr ".wl[538].w[5]" 0.21205060642764711;
	setAttr ".wl[538].w[7]" 0.053587617857255923;
	setAttr ".wl[538].w[12]" 0.17391612890402725;
	setAttr -s 5 ".wl[539].w";
	setAttr ".wl[539].w[3]" 0.11668568732257341;
	setAttr ".wl[539].w[4]" 0.46811599006583549;
	setAttr ".wl[539].w[5]" 0.17018451857545616;
	setAttr ".wl[539].w[7]" 0.065912367491060317;
	setAttr ".wl[539].w[12]" 0.17910143654507465;
	setAttr -s 5 ".wl[540].w";
	setAttr ".wl[540].w[3]" 0.12319823127475486;
	setAttr ".wl[540].w[4]" 0.55636893985857272;
	setAttr ".wl[540].w[5]" 0.085700327064560558;
	setAttr ".wl[540].w[7]" 0.049360931706104214;
	setAttr ".wl[540].w[12]" 0.18537157009600774;
	setAttr -s 5 ".wl[541].w";
	setAttr ".wl[541].w[3]" 0.12501981856672451;
	setAttr ".wl[541].w[4]" 0.62198358745073579;
	setAttr ".wl[541].w[5]" 0.040725774768687217;
	setAttr ".wl[541].w[8]" 0.030130515437405012;
	setAttr ".wl[541].w[12]" 0.18214030377644749;
	setAttr -s 5 ".wl[542].w";
	setAttr ".wl[542].w[3]" 0.12876702096418649;
	setAttr ".wl[542].w[4]" 0.64731476495561757;
	setAttr ".wl[542].w[5]" 0.024312585614749572;
	setAttr ".wl[542].w[8]" 0.021791867737047253;
	setAttr ".wl[542].w[12]" 0.17781376072839913;
	setAttr -s 5 ".wl[543].w";
	setAttr ".wl[543].w[3]" 0.13643004425371302;
	setAttr ".wl[543].w[4]" 0.66103704175683775;
	setAttr ".wl[543].w[5]" 0.016085653704652751;
	setAttr ".wl[543].w[8]" 0.015017841792074701;
	setAttr ".wl[543].w[12]" 0.17142941849272189;
	setAttr -s 5 ".wl[544].w";
	setAttr ".wl[544].w[2]" 0.012490321105497181;
	setAttr ".wl[544].w[3]" 0.17801943464093567;
	setAttr ".wl[544].w[4]" 0.63323430611350628;
	setAttr ".wl[544].w[5]" 0.010970406864230562;
	setAttr ".wl[544].w[12]" 0.16528553127583034;
	setAttr -s 5 ".wl[545].w";
	setAttr ".wl[545].w[2]" 0.021619349704538101;
	setAttr ".wl[545].w[3]" 0.26529273119541569;
	setAttr ".wl[545].w[4]" 0.55259071019903594;
	setAttr ".wl[545].w[5]" 0.0076907285673179816;
	setAttr ".wl[545].w[12]" 0.15280648033369235;
	setAttr -s 5 ".wl[546].w";
	setAttr ".wl[546].w[2]" 0.029154130706676051;
	setAttr ".wl[546].w[3]" 0.36165030182767588;
	setAttr ".wl[546].w[4]" 0.47143390580271494;
	setAttr ".wl[546].w[9]" 0.0052414413302574901;
	setAttr ".wl[546].w[12]" 0.13252022033267558;
	setAttr -s 5 ".wl[547].w";
	setAttr ".wl[547].w[2]" 0.035315941105736381;
	setAttr ".wl[547].w[3]" 0.43276199403979582;
	setAttr ".wl[547].w[4]" 0.40646834405515453;
	setAttr ".wl[547].w[9]" 0.0049869879714224026;
	setAttr ".wl[547].w[12]" 0.12046673282789087;
	setAttr -s 5 ".wl[548].w";
	setAttr ".wl[548].w[2]" 0.04070449872348815;
	setAttr ".wl[548].w[3]" 0.45166317010125717;
	setAttr ".wl[548].w[4]" 0.38059761432391509;
	setAttr ".wl[548].w[9]" 0.0054171602175232614;
	setAttr ".wl[548].w[12]" 0.12161755663381639;
	setAttr -s 5 ".wl[549].w";
	setAttr ".wl[549].w[2]" 0.042870238078796828;
	setAttr ".wl[549].w[3]" 0.41872049596905653;
	setAttr ".wl[549].w[4]" 0.39694816641438441;
	setAttr ".wl[549].w[9]" 0.0064337117961816605;
	setAttr ".wl[549].w[12]" 0.13502738774158066;
	setAttr -s 5 ".wl[550].w";
	setAttr ".wl[550].w[2]" 0.043041805644258448;
	setAttr ".wl[550].w[3]" 0.35554119973077264;
	setAttr ".wl[550].w[4]" 0.43147425929585148;
	setAttr ".wl[550].w[9]" 0.0086011341744486724;
	setAttr ".wl[550].w[12]" 0.16134160115466881;
	setAttr -s 5 ".wl[551].w";
	setAttr ".wl[551].w[2]" 0.03787172732687847;
	setAttr ".wl[551].w[3]" 0.28855155644304847;
	setAttr ".wl[551].w[4]" 0.46943345552112964;
	setAttr ".wl[551].w[9]" 0.011344628713260311;
	setAttr ".wl[551].w[12]" 0.19279863199568301;
	setAttr -s 5 ".wl[552].w";
	setAttr ".wl[552].w[2]" 0.026782041049842466;
	setAttr ".wl[552].w[3]" 0.23080455234864003;
	setAttr ".wl[552].w[4]" 0.51057563611822643;
	setAttr ".wl[552].w[9]" 0.012739993946159002;
	setAttr ".wl[552].w[12]" 0.21909777653713214;
	setAttr -s 5 ".wl[553].w";
	setAttr ".wl[553].w[2]" 0.018783935982604385;
	setAttr ".wl[553].w[3]" 0.20183493850181786;
	setAttr ".wl[553].w[4]" 0.52729256328983187;
	setAttr ".wl[553].w[5]" 0.015993925537440598;
	setAttr ".wl[553].w[12]" 0.23609463668830513;
	setAttr -s 5 ".wl[554].w";
	setAttr ".wl[554].w[2]" 0.014548996305257191;
	setAttr ".wl[554].w[3]" 0.19589350981265008;
	setAttr ".wl[554].w[4]" 0.52360360730275046;
	setAttr ".wl[554].w[5]" 0.021694483490552273;
	setAttr ".wl[554].w[12]" 0.24425940308879002;
	setAttr -s 5 ".wl[555].w";
	setAttr ".wl[555].w[3]" 0.19061858204854545;
	setAttr ".wl[555].w[4]" 0.51825804044050749;
	setAttr ".wl[555].w[5]" 0.029754419632493714;
	setAttr ".wl[555].w[9]" 0.013900650052305686;
	setAttr ".wl[555].w[12]" 0.2474683078261477;
	setAttr -s 5 ".wl[556].w";
	setAttr ".wl[556].w[3]" 0.18519646555428521;
	setAttr ".wl[556].w[4]" 0.5044973939660643;
	setAttr ".wl[556].w[5]" 0.047145110725894175;
	setAttr ".wl[556].w[9]" 0.014183549419374192;
	setAttr ".wl[556].w[12]" 0.24897748033438205;
	setAttr -s 5 ".wl[557].w";
	setAttr ".wl[557].w[3]" 0.1761346698437864;
	setAttr ".wl[557].w[4]" 0.48445753481683607;
	setAttr ".wl[557].w[5]" 0.078190432239845328;
	setAttr ".wl[557].w[7]" 0.015758516551646024;
	setAttr ".wl[557].w[12]" 0.24545884654788613;
	setAttr -s 5 ".wl[558].w";
	setAttr ".wl[558].w[3]" 0.15507799818752033;
	setAttr ".wl[558].w[4]" 0.48566269550329938;
	setAttr ".wl[558].w[5]" 0.11032580056982814;
	setAttr ".wl[558].w[7]" 0.019623080528502247;
	setAttr ".wl[558].w[12]" 0.22931042521084996;
	setAttr -s 5 ".wl[559].w";
	setAttr ".wl[559].w[3]" 0.1182674449001994;
	setAttr ".wl[559].w[4]" 0.5454121234097935;
	setAttr ".wl[559].w[5]" 0.12949984082297095;
	setAttr ".wl[559].w[7]" 0.021772121835605011;
	setAttr ".wl[559].w[12]" 0.1850484690314311;
	setAttr -s 5 ".wl[560].w";
	setAttr ".wl[560].w[3]" 0.099551790650162258;
	setAttr ".wl[560].w[4]" 0.56173458175665703;
	setAttr ".wl[560].w[5]" 0.15265755050502822;
	setAttr ".wl[560].w[7]" 0.025892918623217055;
	setAttr ".wl[560].w[12]" 0.16016315846493542;
	setAttr -s 5 ".wl[561].w";
	setAttr ".wl[561].w[3]" 0.096790122003125229;
	setAttr ".wl[561].w[4]" 0.53314692687229148;
	setAttr ".wl[561].w[5]" 0.17873494053408109;
	setAttr ".wl[561].w[7]" 0.034095017754168233;
	setAttr ".wl[561].w[12]" 0.15723299283633402;
	setAttr -s 5 ".wl[562].w";
	setAttr ".wl[562].w[3]" 0.032121984684143748;
	setAttr ".wl[562].w[4]" 0.42248084741318293;
	setAttr ".wl[562].w[5]" 0.095951988408435648;
	setAttr ".wl[562].w[7]" 0.05311338929677796;
	setAttr ".wl[562].w[12]" 0.39633179019745968;
	setAttr -s 5 ".wl[563].w";
	setAttr ".wl[563].w[3]" 0.030899423468751925;
	setAttr ".wl[563].w[4]" 0.425226513382779;
	setAttr ".wl[563].w[5]" 0.08810278553588155;
	setAttr ".wl[563].w[7]" 0.062034610262465359;
	setAttr ".wl[563].w[12]" 0.39373666735012219;
	setAttr -s 5 ".wl[564].w";
	setAttr ".wl[564].w[3]" 0.028430293573140718;
	setAttr ".wl[564].w[4]" 0.46077826628381852;
	setAttr ".wl[564].w[5]" 0.050482678454931575;
	setAttr ".wl[564].w[7]" 0.042784957797361862;
	setAttr ".wl[564].w[12]" 0.41752380389074739;
	setAttr -s 5 ".wl[565].w";
	setAttr ".wl[565].w[3]" 0.026854828368461505;
	setAttr ".wl[565].w[4]" 0.48882323725479249;
	setAttr ".wl[565].w[5]" 0.026593013751431426;
	setAttr ".wl[565].w[7]" 0.02467699981279986;
	setAttr ".wl[565].w[12]" 0.43305192081251487;
	setAttr -s 5 ".wl[566].w";
	setAttr ".wl[566].w[3]" 0.027728982033188312;
	setAttr ".wl[566].w[4]" 0.50152232268974406;
	setAttr ".wl[566].w[5]" 0.017143302805240367;
	setAttr ".wl[566].w[7]" 0.016447544325642195;
	setAttr ".wl[566].w[12]" 0.43715784814618514;
	setAttr -s 5 ".wl[567].w";
	setAttr ".wl[567].w[3]" 0.029694906175047334;
	setAttr ".wl[567].w[4]" 0.51080498703832034;
	setAttr ".wl[567].w[5]" 0.011396210042255111;
	setAttr ".wl[567].w[7]" 0.011057278203537324;
	setAttr ".wl[567].w[12]" 0.43704661854083993;
	setAttr -s 5 ".wl[568].w";
	setAttr ".wl[568].w[3]" 0.038971250122419858;
	setAttr ".wl[568].w[4]" 0.5122117979044668;
	setAttr ".wl[568].w[5]" 0.008322151722279356;
	setAttr ".wl[568].w[7]" 0.0080903508766014629;
	setAttr ".wl[568].w[12]" 0.4324044493742325;
	setAttr -s 5 ".wl[569].w";
	setAttr ".wl[569].w[2]" 0.012459971587392496;
	setAttr ".wl[569].w[3]" 0.05683656676206271;
	setAttr ".wl[569].w[4]" 0.50080303015780192;
	setAttr ".wl[569].w[12]" 0.42173734259788498;
	setAttr ".wl[569].w[13]" 0.0081630888948579765;
	setAttr -s 5 ".wl[570].w";
	setAttr ".wl[570].w[2]" 0.017675768936420113;
	setAttr ".wl[570].w[3]" 0.07205393556325837;
	setAttr ".wl[570].w[4]" 0.48959004798891936;
	setAttr ".wl[570].w[12]" 0.41203144957749899;
	setAttr ".wl[570].w[13]" 0.0086487979339031906;
	setAttr -s 5 ".wl[571].w";
	setAttr ".wl[571].w[2]" 0.022679944157249363;
	setAttr ".wl[571].w[3]" 0.085875424744132761;
	setAttr ".wl[571].w[4]" 0.47697775254283936;
	setAttr ".wl[571].w[12]" 0.40503900673928017;
	setAttr ".wl[571].w[13]" 0.0094278718164983889;
	setAttr -s 5 ".wl[572].w";
	setAttr ".wl[572].w[2]" 0.026710170959521561;
	setAttr ".wl[572].w[3]" 0.096221470106248541;
	setAttr ".wl[572].w[4]" 0.46599005422707807;
	setAttr ".wl[572].w[12]" 0.40047791096789614;
	setAttr ".wl[572].w[13]" 0.010600393739255604;
	setAttr -s 5 ".wl[573].w";
	setAttr ".wl[573].w[2]" 0.02753559920338684;
	setAttr ".wl[573].w[3]" 0.097978810091887247;
	setAttr ".wl[573].w[4]" 0.46194030812621717;
	setAttr ".wl[573].w[12]" 0.40082268774330532;
	setAttr ".wl[573].w[13]" 0.011722594835203508;
	setAttr -s 5 ".wl[574].w";
	setAttr ".wl[574].w[2]" 0.027013193621659975;
	setAttr ".wl[574].w[3]" 0.096856201834009517;
	setAttr ".wl[574].w[4]" 0.45868351149923781;
	setAttr ".wl[574].w[12]" 0.40369737908048109;
	setAttr ".wl[574].w[13]" 0.013749713964611551;
	setAttr -s 5 ".wl[575].w";
	setAttr ".wl[575].w[2]" 0.023720904231760091;
	setAttr ".wl[575].w[3]" 0.090434453017302227;
	setAttr ".wl[575].w[4]" 0.45902909584874801;
	setAttr ".wl[575].w[12]" 0.41077366803748794;
	setAttr ".wl[575].w[13]" 0.016041878864701712;
	setAttr -s 5 ".wl[576].w";
	setAttr ".wl[576].w[2]" 0.016823428163631901;
	setAttr ".wl[576].w[3]" 0.076216141433601323;
	setAttr ".wl[576].w[4]" 0.46676245187248144;
	setAttr ".wl[576].w[12]" 0.42365519110428329;
	setAttr ".wl[576].w[13]" 0.016542787426001963;
	setAttr -s 5 ".wl[577].w";
	setAttr ".wl[577].w[2]" 0.012002655021624845;
	setAttr ".wl[577].w[3]" 0.067880307062259088;
	setAttr ".wl[577].w[4]" 0.47030209820442259;
	setAttr ".wl[577].w[12]" 0.43310922212227165;
	setAttr ".wl[577].w[13]" 0.016705717589421847;
	setAttr -s 5 ".wl[578].w";
	setAttr ".wl[578].w[3]" 0.066077301605330083;
	setAttr ".wl[578].w[4]" 0.46770424800621885;
	setAttr ".wl[578].w[5]" 0.013709631770744029;
	setAttr ".wl[578].w[12]" 0.43552106891132975;
	setAttr ".wl[578].w[13]" 0.016987749706377321;
	setAttr -s 5 ".wl[579].w";
	setAttr ".wl[579].w[3]" 0.063806276040941759;
	setAttr ".wl[579].w[4]" 0.46545951453890622;
	setAttr ".wl[579].w[5]" 0.017438037991814058;
	setAttr ".wl[579].w[12]" 0.43660473971907293;
	setAttr ".wl[579].w[13]" 0.016691431709264983;
	setAttr -s 5 ".wl[580].w";
	setAttr ".wl[580].w[3]" 0.0625551616220274;
	setAttr ".wl[580].w[4]" 0.46042446791576935;
	setAttr ".wl[580].w[5]" 0.025175741923718804;
	setAttr ".wl[580].w[12]" 0.43517482357293569;
	setAttr ".wl[580].w[13]" 0.016669804965548728;
	setAttr -s 5 ".wl[581].w";
	setAttr ".wl[581].w[3]" 0.060428188228659301;
	setAttr ".wl[581].w[4]" 0.45366468373071306;
	setAttr ".wl[581].w[5]" 0.037876581900691264;
	setAttr ".wl[581].w[12]" 0.43164935792931225;
	setAttr ".wl[581].w[13]" 0.016381188210624096;
	setAttr -s 5 ".wl[582].w";
	setAttr ".wl[582].w[3]" 0.052240693551164202;
	setAttr ".wl[582].w[4]" 0.45057523505132097;
	setAttr ".wl[582].w[5]" 0.049270467402387615;
	setAttr ".wl[582].w[7]" 0.018158670444504841;
	setAttr ".wl[582].w[12]" 0.4297549335506225;
	setAttr -s 5 ".wl[583].w";
	setAttr ".wl[583].w[3]" 0.044377896761850645;
	setAttr ".wl[583].w[4]" 0.44518708630264581;
	setAttr ".wl[583].w[5]" 0.06195231003668436;
	setAttr ".wl[583].w[7]" 0.023560103877513165;
	setAttr ".wl[583].w[12]" 0.42492260302130608;
	setAttr -s 5 ".wl[584].w";
	setAttr ".wl[584].w[3]" 0.039624202203710192;
	setAttr ".wl[584].w[4]" 0.43672338692932638;
	setAttr ".wl[584].w[5]" 0.076354988188861286;
	setAttr ".wl[584].w[7]" 0.030916336826200243;
	setAttr ".wl[584].w[12]" 0.41638108585190187;
	setAttr -s 5 ".wl[585].w";
	setAttr ".wl[585].w[3]" 0.03515343120645624;
	setAttr ".wl[585].w[4]" 0.43092732347134555;
	setAttr ".wl[585].w[5]" 0.086280808424669134;
	setAttr ".wl[585].w[7]" 0.03922926645167004;
	setAttr ".wl[585].w[12]" 0.40840917044585906;
	setAttr -s 5 ".wl[586].w";
	setAttr ".wl[586].w[4]" 0.3885120752692241;
	setAttr ".wl[586].w[5]" 0.048899461146782668;
	setAttr ".wl[586].w[7]" 0.04256917242694646;
	setAttr ".wl[586].w[12]" 0.49820038000908951;
	setAttr ".wl[586].w[13]" 0.021818911147957323;
	setAttr -s 5 ".wl[587].w";
	setAttr ".wl[587].w[4]" 0.38498544555061243;
	setAttr ".wl[587].w[5]" 0.049913094695310876;
	setAttr ".wl[587].w[7]" 0.047752621889853124;
	setAttr ".wl[587].w[12]" 0.4971598626509191;
	setAttr ".wl[587].w[13]" 0.02018897521330458;
	setAttr -s 5 ".wl[588].w";
	setAttr ".wl[588].w[4]" 0.3914255720075655;
	setAttr ".wl[588].w[5]" 0.033523932856808825;
	setAttr ".wl[588].w[7]" 0.033355868843129632;
	setAttr ".wl[588].w[12]" 0.52407637454911382;
	setAttr ".wl[588].w[13]" 0.017618251743382239;
	setAttr -s 5 ".wl[589].w";
	setAttr ".wl[589].w[4]" 0.39577755650808938;
	setAttr ".wl[589].w[5]" 0.02042020857471916;
	setAttr ".wl[589].w[7]" 0.02042020857471916;
	setAttr ".wl[589].w[12]" 0.54719934933299885;
	setAttr ".wl[589].w[13]" 0.016182677009473512;
	setAttr -s 5 ".wl[590].w";
	setAttr ".wl[590].w[4]" 0.39972647645051124;
	setAttr ".wl[590].w[5]" 0.01438685621642094;
	setAttr ".wl[590].w[7]" 0.01438685621642094;
	setAttr ".wl[590].w[12]" 0.55510963847573369;
	setAttr ".wl[590].w[13]" 0.016390172640913261;
	setAttr -s 5 ".wl[591].w";
	setAttr ".wl[591].w[3]" 0.011327873602608949;
	setAttr ".wl[591].w[4]" 0.40394478536211381;
	setAttr ".wl[591].w[5]" 0.010162090705005643;
	setAttr ".wl[591].w[12]" 0.55774332016985939;
	setAttr ".wl[591].w[13]" 0.01682193016041229;
	setAttr -s 5 ".wl[592].w";
	setAttr ".wl[592].w[3]" 0.014303256290943596;
	setAttr ".wl[592].w[4]" 0.41041035355409505;
	setAttr ".wl[592].w[5]" 0.0077612729610409071;
	setAttr ".wl[592].w[12]" 0.54835911846504448;
	setAttr ".wl[592].w[13]" 0.019165998728875987;
	setAttr -s 5 ".wl[593].w";
	setAttr ".wl[593].w[2]" 0.0074645034178908052;
	setAttr ".wl[593].w[3]" 0.019749441641720419;
	setAttr ".wl[593].w[4]" 0.41498939797756684;
	setAttr ".wl[593].w[12]" 0.53494078247660048;
	setAttr ".wl[593].w[13]" 0.022855874486221516;
	setAttr -s 5 ".wl[594].w";
	setAttr ".wl[594].w[2]" 0.009952985033020445;
	setAttr ".wl[594].w[3]" 0.023606246832436412;
	setAttr ".wl[594].w[4]" 0.41310658820062962;
	setAttr ".wl[594].w[12]" 0.52878123903349483;
	setAttr ".wl[594].w[13]" 0.024552940900418749;
	setAttr -s 5 ".wl[595].w";
	setAttr ".wl[595].w[2]" 0.012343427429991602;
	setAttr ".wl[595].w[3]" 0.02745610293576296;
	setAttr ".wl[595].w[4]" 0.41121779197843894;
	setAttr ".wl[595].w[12]" 0.52205650379452662;
	setAttr ".wl[595].w[13]" 0.026926173861279806;
	setAttr -s 5 ".wl[596].w";
	setAttr ".wl[596].w[2]" 0.014462001651434327;
	setAttr ".wl[596].w[3]" 0.031214304450889355;
	setAttr ".wl[596].w[4]" 0.41078941996074458;
	setAttr ".wl[596].w[12]" 0.51354868189077585;
	setAttr ".wl[596].w[13]" 0.029985592046155706;
	setAttr -s 5 ".wl[597].w";
	setAttr ".wl[597].w[2]" 0.015222513657379731;
	setAttr ".wl[597].w[3]" 0.033108000252675365;
	setAttr ".wl[597].w[4]" 0.41183316616289611;
	setAttr ".wl[597].w[12]" 0.50734597772860868;
	setAttr ".wl[597].w[13]" 0.032490342198440089;
	setAttr -s 5 ".wl[598].w";
	setAttr ".wl[598].w[2]" 0.015641148129745472;
	setAttr ".wl[598].w[3]" 0.035308578890668268;
	setAttr ".wl[598].w[4]" 0.41408748563471304;
	setAttr ".wl[598].w[12]" 0.49833949516398657;
	setAttr ".wl[598].w[13]" 0.036623292180886689;
	setAttr -s 5 ".wl[599].w";
	setAttr ".wl[599].w[2]" 0.014622784818130841;
	setAttr ".wl[599].w[3]" 0.035870194065118148;
	setAttr ".wl[599].w[4]" 0.4164635301440564;
	setAttr ".wl[599].w[12]" 0.49213075494688147;
	setAttr ".wl[599].w[13]" 0.040912736025813169;
	setAttr -s 5 ".wl[600].w";
	setAttr ".wl[600].w[2]" 0.011088051462435147;
	setAttr ".wl[600].w[3]" 0.031935846720945814;
	setAttr ".wl[600].w[4]" 0.41913549870589462;
	setAttr ".wl[600].w[12]" 0.49649133462314415;
	setAttr ".wl[600].w[13]" 0.041349268487580297;
	setAttr -s 5 ".wl[601].w";
	setAttr ".wl[601].w[2]" 0.0083758798875593898;
	setAttr ".wl[601].w[3]" 0.029275611277738121;
	setAttr ".wl[601].w[4]" 0.42040334494267656;
	setAttr ".wl[601].w[12]" 0.50059142940399259;
	setAttr ".wl[601].w[13]" 0.041353734488033395;
	setAttr -s 5 ".wl[602].w";
	setAttr ".wl[602].w[3]" 0.028759526622334941;
	setAttr ".wl[602].w[4]" 0.41975570815961116;
	setAttr ".wl[602].w[5]" 0.010041355177226615;
	setAttr ".wl[602].w[12]" 0.49971030785383208;
	setAttr ".wl[602].w[13]" 0.041733102186995205;
	setAttr -s 5 ".wl[603].w";
	setAttr ".wl[603].w[3]" 0.027808193901729138;
	setAttr ".wl[603].w[4]" 0.41961764340455127;
	setAttr ".wl[603].w[5]" 0.01209553602743616;
	setAttr ".wl[603].w[12]" 0.49960188690621021;
	setAttr ".wl[603].w[13]" 0.040876739760073263;
	setAttr -s 5 ".wl[604].w";
	setAttr ".wl[604].w[3]" 0.02741255747539097;
	setAttr ".wl[604].w[4]" 0.41931597324035202;
	setAttr ".wl[604].w[5]" 0.01619380332662769;
	setAttr ".wl[604].w[12]" 0.49657318474978679;
	setAttr ".wl[604].w[13]" 0.040504481207842627;
	setAttr -s 5 ".wl[605].w";
	setAttr ".wl[605].w[3]" 0.026677012318892072;
	setAttr ".wl[605].w[4]" 0.4177233411084828;
	setAttr ".wl[605].w[5]" 0.022422236122684729;
	setAttr ".wl[605].w[12]" 0.4934157641783497;
	setAttr ".wl[605].w[13]" 0.039761646271590734;
	setAttr -s 5 ".wl[606].w";
	setAttr ".wl[606].w[3]" 0.022931462625442205;
	setAttr ".wl[606].w[4]" 0.41513352993183084;
	setAttr ".wl[606].w[5]" 0.027200797917966979;
	setAttr ".wl[606].w[12]" 0.49952514976231704;
	setAttr ".wl[606].w[13]" 0.035209059762442971;
	setAttr -s 5 ".wl[607].w";
	setAttr ".wl[607].w[4]" 0.41015933652519748;
	setAttr ".wl[607].w[5]" 0.03233109026089568;
	setAttr ".wl[607].w[7]" 0.021245511114356797;
	setAttr ".wl[607].w[12]" 0.50550118993231352;
	setAttr ".wl[607].w[13]" 0.030762872167236468;
	setAttr -s 5 ".wl[608].w";
	setAttr ".wl[608].w[4]" 0.40280459076678815;
	setAttr ".wl[608].w[5]" 0.038268461782164177;
	setAttr ".wl[608].w[7]" 0.02708148287095034;
	setAttr ".wl[608].w[12]" 0.5040262810122117;
	setAttr ".wl[608].w[13]" 0.027819183567885583;
	setAttr -s 5 ".wl[609].w";
	setAttr ".wl[609].w[4]" 0.3961323045355285;
	setAttr ".wl[609].w[5]" 0.042648754401087598;
	setAttr ".wl[609].w[7]" 0.033086764092301706;
	setAttr ".wl[609].w[12]" 0.50366125504288262;
	setAttr ".wl[609].w[13]" 0.024470921928199538;
	setAttr -s 5 ".wl[610].w";
	setAttr ".wl[610].w[4]" 0.17822177168388401;
	setAttr ".wl[610].w[5]" 0.030773988189788326;
	setAttr ".wl[610].w[7]" 0.030652387918331411;
	setAttr ".wl[610].w[12]" 0.67366777520916665;
	setAttr ".wl[610].w[13]" 0.086684076998829532;
	setAttr -s 5 ".wl[611].w";
	setAttr ".wl[611].w[4]" 0.17296342670484566;
	setAttr ".wl[611].w[5]" 0.032848606252587793;
	setAttr ".wl[611].w[7]" 0.032848606252587793;
	setAttr ".wl[611].w[12]" 0.67926958420931183;
	setAttr ".wl[611].w[13]" 0.082069776580667025;
	setAttr -s 5 ".wl[612].w";
	setAttr ".wl[612].w[4]" 0.16018301949299438;
	setAttr ".wl[612].w[5]" 0.024070272361569566;
	setAttr ".wl[612].w[7]" 0.024070272361569566;
	setAttr ".wl[612].w[12]" 0.71730818308462985;
	setAttr ".wl[612].w[13]" 0.074368252699236595;
	setAttr -s 5 ".wl[613].w";
	setAttr ".wl[613].w[4]" 0.15074024215680099;
	setAttr ".wl[613].w[5]" 0.016115189458094831;
	setAttr ".wl[613].w[7]" 0.016115189458094831;
	setAttr ".wl[613].w[12]" 0.74715208864650096;
	setAttr ".wl[613].w[13]" 0.069877290280508417;
	setAttr -s 5 ".wl[614].w";
	setAttr ".wl[614].w[4]" 0.15092159701559155;
	setAttr ".wl[614].w[5]" 0.012234002557258606;
	setAttr ".wl[614].w[7]" 0.012234002557258606;
	setAttr ".wl[614].w[12]" 0.75405383572366602;
	setAttr ".wl[614].w[13]" 0.070556562146225313;
	setAttr -s 5 ".wl[615].w";
	setAttr ".wl[615].w[4]" 0.15394100609717026;
	setAttr ".wl[615].w[5]" 0.0092841664940666018;
	setAttr ".wl[615].w[7]" 0.0092841664940666018;
	setAttr ".wl[615].w[12]" 0.75554067650593726;
	setAttr ".wl[615].w[13]" 0.071949984408759354;
	setAttr -s 5 ".wl[616].w";
	setAttr ".wl[616].w[3]" 0.0078725998237360768;
	setAttr ".wl[616].w[4]" 0.16774438886278942;
	setAttr ".wl[616].w[5]" 0.0075795246633103006;
	setAttr ".wl[616].w[12]" 0.73757330504947638;
	setAttr ".wl[616].w[13]" 0.079230181600687899;
	setAttr -s 5 ".wl[617].w";
	setAttr ".wl[617].w[3]" 0.010370759513429381;
	setAttr ".wl[617].w[4]" 0.18446566640200532;
	setAttr ".wl[617].w[5]" 0.0066055174751596911;
	setAttr ".wl[617].w[12]" 0.70889862221489763;
	setAttr ".wl[617].w[13]" 0.08965943439450795;
	setAttr -s 5 ".wl[618].w";
	setAttr ".wl[618].w[2]" 0.0067163961198566813;
	setAttr ".wl[618].w[3]" 0.011916716032875302;
	setAttr ".wl[618].w[4]" 0.18908861807685595;
	setAttr ".wl[618].w[12]" 0.69768101924754278;
	setAttr ".wl[618].w[13]" 0.094597250522869242;
	setAttr -s 5 ".wl[619].w";
	setAttr ".wl[619].w[2]" 0.0080052495438111548;
	setAttr ".wl[619].w[3]" 0.013519831170092792;
	setAttr ".wl[619].w[4]" 0.19449945141703592;
	setAttr ".wl[619].w[12]" 0.68295271002903324;
	setAttr ".wl[619].w[13]" 0.10102275784002684;
	setAttr -s 5 ".wl[620].w";
	setAttr ".wl[620].w[2]" 0.0092244579950882238;
	setAttr ".wl[620].w[3]" 0.01527086612849575;
	setAttr ".wl[620].w[4]" 0.20276783087467243;
	setAttr ".wl[620].w[12]" 0.66425405930836268;
	setAttr ".wl[620].w[13]" 0.10848278569338099;
	setAttr -s 5 ".wl[621].w";
	setAttr ".wl[621].w[2]" 0.0097943599467805557;
	setAttr ".wl[621].w[3]" 0.01638123642737406;
	setAttr ".wl[621].w[4]" 0.21021481645426224;
	setAttr ".wl[621].w[12]" 0.64967453754658178;
	setAttr ".wl[621].w[13]" 0.11393504962500138;
	setAttr -s 5 ".wl[622].w";
	setAttr ".wl[622].w[2]" 0.010338815640189672;
	setAttr ".wl[622].w[3]" 0.017942038271928609;
	setAttr ".wl[622].w[4]" 0.2222411697007203;
	setAttr ".wl[622].w[12]" 0.62743842281534479;
	setAttr ".wl[622].w[13]" 0.12203955357181667;
	setAttr -s 5 ".wl[623].w";
	setAttr ".wl[623].w[2]" 0.010103010720053844;
	setAttr ".wl[623].w[3]" 0.018896283179192692;
	setAttr ".wl[623].w[4]" 0.23255309940719027;
	setAttr ".wl[623].w[12]" 0.60866367765448148;
	setAttr ".wl[623].w[13]" 0.12978392903908156;
	setAttr -s 5 ".wl[624].w";
	setAttr ".wl[624].w[2]" 0.0081563770261315893;
	setAttr ".wl[624].w[3]" 0.01743743442083933;
	setAttr ".wl[624].w[4]" 0.23239011667917103;
	setAttr ".wl[624].w[12]" 0.61134241987697191;
	setAttr ".wl[624].w[13]" 0.13067365199688613;
	setAttr -s 5 ".wl[625].w";
	setAttr ".wl[625].w[3]" 0.016321517702578267;
	setAttr ".wl[625].w[4]" 0.2305223477068615;
	setAttr ".wl[625].w[5]" 0.0072364504082885958;
	setAttr ".wl[625].w[12]" 0.61502292240908896;
	setAttr ".wl[625].w[13]" 0.13089676177318274;
	setAttr -s 5 ".wl[626].w";
	setAttr ".wl[626].w[3]" 0.016155035113650351;
	setAttr ".wl[626].w[4]" 0.2306364965932437;
	setAttr ".wl[626].w[5]" 0.0083673979028366929;
	setAttr ".wl[626].w[12]" 0.61320855672838126;
	setAttr ".wl[626].w[13]" 0.13163251366188805;
	setAttr -s 5 ".wl[627].w";
	setAttr ".wl[627].w[3]" 0.015717876221070681;
	setAttr ".wl[627].w[4]" 0.2298532171465747;
	setAttr ".wl[627].w[5]" 0.0097061773455392429;
	setAttr ".wl[627].w[12]" 0.61491831302185518;
	setAttr ".wl[627].w[13]" 0.12980441626496028;
	setAttr -s 5 ".wl[628].w";
	setAttr ".wl[628].w[3]" 0.015610877129216711;
	setAttr ".wl[628].w[4]" 0.2313558113451693;
	setAttr ".wl[628].w[5]" 0.012311172873490876;
	setAttr ".wl[628].w[12]" 0.61217416861445739;
	setAttr ".wl[628].w[13]" 0.12854797003766569;
	setAttr -s 5 ".wl[629].w";
	setAttr ".wl[629].w[3]" 0.015316188422114005;
	setAttr ".wl[629].w[4]" 0.23127304192318796;
	setAttr ".wl[629].w[5]" 0.016050054852241327;
	setAttr ".wl[629].w[12]" 0.61058645636455788;
	setAttr ".wl[629].w[13]" 0.1267742584378988;
	setAttr -s 5 ".wl[630].w";
	setAttr ".wl[630].w[4]" 0.21972097287455694;
	setAttr ".wl[630].w[5]" 0.018570658281968886;
	setAttr ".wl[630].w[7]" 0.015378141171883472;
	setAttr ".wl[630].w[12]" 0.62842677549044501;
	setAttr ".wl[630].w[13]" 0.1179034521811457;
	setAttr -s 5 ".wl[631].w";
	setAttr ".wl[631].w[4]" 0.20550031811307529;
	setAttr ".wl[631].w[5]" 0.021124843137342111;
	setAttr ".wl[631].w[7]" 0.018493777535643123;
	setAttr ".wl[631].w[12]" 0.64644668522069493;
	setAttr ".wl[631].w[13]" 0.10843437599324454;
	setAttr -s 5 ".wl[632].w";
	setAttr ".wl[632].w[4]" 0.19598829912706106;
	setAttr ".wl[632].w[5]" 0.024265133959643644;
	setAttr ".wl[632].w[7]" 0.02231367237538014;
	setAttr ".wl[632].w[12]" 0.65554036895919321;
	setAttr ".wl[632].w[13]" 0.10189252557872198;
	setAttr -s 5 ".wl[633].w";
	setAttr ".wl[633].w[4]" 0.18581810189203052;
	setAttr ".wl[633].w[5]" 0.02670169856520347;
	setAttr ".wl[633].w[7]" 0.025696361099856235;
	setAttr ".wl[633].w[12]" 0.66806656938489906;
	setAttr ".wl[633].w[13]" 0.093717269058010699;
	setAttr -s 5 ".wl[634].w";
	setAttr ".wl[634].w[4]" 0.052690823388965337;
	setAttr ".wl[634].w[5]" 0.014366430088156749;
	setAttr ".wl[634].w[7]" 0.014366430088156749;
	setAttr ".wl[634].w[12]" 0.64426266067710658;
	setAttr ".wl[634].w[13]" 0.27431365575761452;
	setAttr -s 5 ".wl[635].w";
	setAttr ".wl[635].w[4]" 0.050302661458109937;
	setAttr ".wl[635].w[5]" 0.014782769956473776;
	setAttr ".wl[635].w[7]" 0.014782769956473776;
	setAttr ".wl[635].w[12]" 0.65237696620085084;
	setAttr ".wl[635].w[13]" 0.26775483242809167;
	setAttr -s 5 ".wl[636].w";
	setAttr ".wl[636].w[4]" 0.043497807401529733;
	setAttr ".wl[636].w[5]" 0.0110652931565643;
	setAttr ".wl[636].w[7]" 0.0110652931565643;
	setAttr ".wl[636].w[12]" 0.6780033629612614;
	setAttr ".wl[636].w[13]" 0.2563682433240802;
	setAttr -s 5 ".wl[637].w";
	setAttr ".wl[637].w[4]" 0.038990798098176419;
	setAttr ".wl[637].w[5]" 0.0078925339442860853;
	setAttr ".wl[637].w[7]" 0.0078925339442860853;
	setAttr ".wl[637].w[12]" 0.69582523860229628;
	setAttr ".wl[637].w[13]" 0.24939889541095511;
	setAttr -s 5 ".wl[638].w";
	setAttr ".wl[638].w[4]" 0.038781678345033654;
	setAttr ".wl[638].w[5]" 0.0064017679346193594;
	setAttr ".wl[638].w[7]" 0.0064017679346193594;
	setAttr ".wl[638].w[12]" 0.69767711870941385;
	setAttr ".wl[638].w[13]" 0.25073766707631384;
	setAttr -s 5 ".wl[639].w";
	setAttr ".wl[639].w[4]" 0.039686946953879303;
	setAttr ".wl[639].w[5]" 0.0052083863590518661;
	setAttr ".wl[639].w[7]" 0.0052083863590518661;
	setAttr ".wl[639].w[12]" 0.69703121991781058;
	setAttr ".wl[639].w[13]" 0.2528650604102064;
	setAttr -s 5 ".wl[640].w";
	setAttr ".wl[640].w[4]" 0.045102905032535758;
	setAttr ".wl[640].w[5]" 0.0046009404312528542;
	setAttr ".wl[640].w[7]" 0.0046009404312528542;
	setAttr ".wl[640].w[12]" 0.68179844399957623;
	setAttr ".wl[640].w[13]" 0.26389677010538232;
	setAttr -s 5 ".wl[641].w";
	setAttr ".wl[641].w[3]" 0.0045234231770100207;
	setAttr ".wl[641].w[4]" 0.052839005695068636;
	setAttr ".wl[641].w[5]" 0.0042999108223271635;
	setAttr ".wl[641].w[12]" 0.65954734678182569;
	setAttr ".wl[641].w[13]" 0.27879031352376843;
	setAttr -s 5 ".wl[642].w";
	setAttr ".wl[642].w[3]" 0.0051084786659559671;
	setAttr ".wl[642].w[4]" 0.055491078606270124;
	setAttr ".wl[642].w[5]" 0.0036621756286424113;
	setAttr ".wl[642].w[12]" 0.64973633795463204;
	setAttr ".wl[642].w[13]" 0.28600192914449946;
	setAttr -s 5 ".wl[643].w";
	setAttr ".wl[643].w[2]" 0.0039892136358533094;
	setAttr ".wl[643].w[3]" 0.005755104680111766;
	setAttr ".wl[643].w[4]" 0.058852832783711055;
	setAttr ".wl[643].w[12]" 0.63676915604154305;
	setAttr ".wl[643].w[13]" 0.29463369285878077;
	setAttr -s 5 ".wl[644].w";
	setAttr ".wl[644].w[2]" 0.00459994248267702;
	setAttr ".wl[644].w[3]" 0.0065432645254290718;
	setAttr ".wl[644].w[4]" 0.063908864030682827;
	setAttr ".wl[644].w[12]" 0.62184319618625261;
	setAttr ".wl[644].w[13]" 0.30310473277495847;
	setAttr -s 5 ".wl[645].w";
	setAttr ".wl[645].w[2]" 0.0049659084456580849;
	setAttr ".wl[645].w[3]" 0.0071298583193678527;
	setAttr ".wl[645].w[4]" 0.068532517680862479;
	setAttr ".wl[645].w[12]" 0.61106266942772258;
	setAttr ".wl[645].w[13]" 0.30830904612638899;
	setAttr -s 5 ".wl[646].w";
	setAttr ".wl[646].w[2]" 0.0054397027131771964;
	setAttr ".wl[646].w[3]" 0.0080578175272380103;
	setAttr ".wl[646].w[4]" 0.076485875088858693;
	setAttr ".wl[646].w[12]" 0.59513938981069814;
	setAttr ".wl[646].w[13]" 0.3148772148600279;
	setAttr -s 5 ".wl[647].w";
	setAttr ".wl[647].w[3]" 0.0087954802287656172;
	setAttr ".wl[647].w[4]" 0.083982494342681804;
	setAttr ".wl[647].w[12]" 0.58089686474890012;
	setAttr ".wl[647].w[13]" 0.32050662572184507;
	setAttr ".wl[647].w[14]" 0.0058185349578072729;
	setAttr -s 5 ".wl[648].w";
	setAttr ".wl[648].w[3]" 0.0082720851018187221;
	setAttr ".wl[648].w[4]" 0.083493049760710203;
	setAttr ".wl[648].w[12]" 0.58098937626056446;
	setAttr ".wl[648].w[13]" 0.32142213185795815;
	setAttr ".wl[648].w[14]" 0.0058233570189484335;
	setAttr -s 5 ".wl[649].w";
	setAttr ".wl[649].w[3]" 0.0078181437261666924;
	setAttr ".wl[649].w[4]" 0.082081355416957366;
	setAttr ".wl[649].w[12]" 0.58213754911667093;
	setAttr ".wl[649].w[13]" 0.32220418202818596;
	setAttr ".wl[649].w[14]" 0.0057587697120189772;
	setAttr -s 5 ".wl[650].w";
	setAttr ".wl[650].w[3]" 0.0077853318969015517;
	setAttr ".wl[650].w[4]" 0.082431009790398038;
	setAttr ".wl[650].w[12]" 0.58093248108258244;
	setAttr ".wl[650].w[13]" 0.32302633562099031;
	setAttr ".wl[650].w[14]" 0.0058248416091275165;
	setAttr -s 5 ".wl[651].w";
	setAttr ".wl[651].w[3]" 0.0075969332096140095;
	setAttr ".wl[651].w[4]" 0.081755660233695723;
	setAttr ".wl[651].w[5]" 0.0060872017491556615;
	setAttr ".wl[651].w[12]" 0.58327758509734862;
	setAttr ".wl[651].w[13]" 0.32128261971018601;
	setAttr -s 5 ".wl[652].w";
	setAttr ".wl[652].w[3]" 0.0075999704441306491;
	setAttr ".wl[652].w[4]" 0.082728502120696892;
	setAttr ".wl[652].w[5]" 0.007423980718863939;
	setAttr ".wl[652].w[12]" 0.58304620475961177;
	setAttr ".wl[652].w[13]" 0.31920134195669686;
	setAttr -s 5 ".wl[653].w";
	setAttr ".wl[653].w[4]" 0.082684926014671881;
	setAttr ".wl[653].w[5]" 0.009239016231936955;
	setAttr ".wl[653].w[7]" 0.0085735178253959921;
	setAttr ".wl[653].w[12]" 0.58290904740197591;
	setAttr ".wl[653].w[13]" 0.31659349252601932;
	setAttr -s 5 ".wl[654].w";
	setAttr ".wl[654].w[4]" 0.07464475119739207;
	setAttr ".wl[654].w[5]" 0.010121914043355712;
	setAttr ".wl[654].w[7]" 0.0096486034397601714;
	setAttr ".wl[654].w[12]" 0.596652602651352;
	setAttr ".wl[654].w[13]" 0.30893212866814013;
	setAttr -s 5 ".wl[655].w";
	setAttr ".wl[655].w[4]" 0.066263451643729251;
	setAttr ".wl[655].w[5]" 0.010948832665855932;
	setAttr ".wl[655].w[7]" 0.010734981535916699;
	setAttr ".wl[655].w[12]" 0.6116686244947821;
	setAttr ".wl[655].w[13]" 0.3003841096597159;
	setAttr -s 5 ".wl[656].w";
	setAttr ".wl[656].w[4]" 0.061341576117948236;
	setAttr ".wl[656].w[5]" 0.012124664915487606;
	setAttr ".wl[656].w[7]" 0.012079583618840262;
	setAttr ".wl[656].w[12]" 0.62087493608313027;
	setAttr ".wl[656].w[13]" 0.29357923926459362;
	setAttr -s 5 ".wl[657].w";
	setAttr ".wl[657].w[4]" 0.056123908442795994;
	setAttr ".wl[657].w[5]" 0.012964602909447205;
	setAttr ".wl[657].w[7]" 0.012964602909447205;
	setAttr ".wl[657].w[12]" 0.63410611064178835;
	setAttr ".wl[657].w[13]" 0.28384077509652128;
	setAttr -s 5 ".wl[658].w";
	setAttr ".wl[658].w[4]" 0.014733200949956858;
	setAttr ".wl[658].w[5]" 0.0055361112166496902;
	setAttr ".wl[658].w[7]" 0.0055361112166496902;
	setAttr ".wl[658].w[12]" 0.49427579521092579;
	setAttr ".wl[658].w[13]" 0.47991878140581801;
	setAttr -s 5 ".wl[659].w";
	setAttr ".wl[659].w[4]" 0.013778019024988883;
	setAttr ".wl[659].w[5]" 0.0054721102811284457;
	setAttr ".wl[659].w[7]" 0.0054721102811284457;
	setAttr ".wl[659].w[12]" 0.49548679723011257;
	setAttr ".wl[659].w[13]" 0.47979096318264169;
	setAttr -s 5 ".wl[660].w";
	setAttr ".wl[660].w[4]" 0.0112614008653416;
	setAttr ".wl[660].w[5]" 0.0040681453813759478;
	setAttr ".wl[660].w[7]" 0.0040681453813759478;
	setAttr ".wl[660].w[12]" 0.49866819971148479;
	setAttr ".wl[660].w[13]" 0.48193410866042175;
	setAttr -s 5 ".wl[661].w";
	setAttr ".wl[661].w[4]" 0.0097322487389068183;
	setAttr ".wl[661].w[5]" 0.0029987061792408846;
	setAttr ".wl[661].w[7]" 0.0029987061792408846;
	setAttr ".wl[661].w[12]" 0.50046369170724747;
	setAttr ".wl[661].w[13]" 0.48380664719536393;
	setAttr -s 5 ".wl[662].w";
	setAttr ".wl[662].w[4]" 0.0096631462489262833;
	setAttr ".wl[662].w[5]" 0.0025619538444776988;
	setAttr ".wl[662].w[12]" 0.5005205787077327;
	setAttr ".wl[662].w[13]" 0.4844235797418317;
	setAttr ".wl[662].w[14]" 0.002830741457031613;
	setAttr -s 5 ".wl[663].w";
	setAttr ".wl[663].w[4]" 0.0099281217626820146;
	setAttr ".wl[663].w[5]" 0.0022100101000385951;
	setAttr ".wl[663].w[12]" 0.50054981326834092;
	setAttr ".wl[663].w[13]" 0.48441102531541796;
	setAttr ".wl[663].w[14]" 0.0029010295535205461;
	setAttr -s 5 ".wl[664].w";
	setAttr ".wl[664].w[4]" 0.011716002775213574;
	setAttr ".wl[664].w[5]" 0.0021243439003291583;
	setAttr ".wl[664].w[12]" 0.49921604006666082;
	setAttr ".wl[664].w[13]" 0.48352076013578232;
	setAttr ".wl[664].w[14]" 0.0034228531220141293;
	setAttr -s 5 ".wl[665].w";
	setAttr ".wl[665].w[4]" 0.014506189508032497;
	setAttr ".wl[665].w[5]" 0.0021624981135822765;
	setAttr ".wl[665].w[12]" 0.49670232545069526;
	setAttr ".wl[665].w[13]" 0.48234651177485993;
	setAttr ".wl[665].w[14]" 0.0042824751528300542;
	setAttr -s 5 ".wl[666].w";
	setAttr ".wl[666].w[3]" 0.0020326532670826947;
	setAttr ".wl[666].w[4]" 0.015612508010868702;
	setAttr ".wl[666].w[12]" 0.49526981534193398;
	setAttr ".wl[666].w[13]" 0.48240105909291892;
	setAttr ".wl[666].w[14]" 0.0046839642871955344;
	setAttr -s 5 ".wl[667].w";
	setAttr ".wl[667].w[3]" 0.0023161074972816078;
	setAttr ".wl[667].w[4]" 0.017095509162102299;
	setAttr ".wl[667].w[12]" 0.49321224204930569;
	setAttr ".wl[667].w[13]" 0.48213004731640868;
	setAttr ".wl[667].w[14]" 0.0052460939749016918;
	setAttr -s 5 ".wl[668].w";
	setAttr ".wl[668].w[3]" 0.0026881027658563982;
	setAttr ".wl[668].w[4]" 0.019292988409604674;
	setAttr ".wl[668].w[12]" 0.49095387432999477;
	setAttr ".wl[668].w[13]" 0.48103958709066713;
	setAttr ".wl[668].w[14]" 0.006025447403877093;
	setAttr -s 5 ".wl[669].w";
	setAttr ".wl[669].w[3]" 0.0029894154852177571;
	setAttr ".wl[669].w[4]" 0.021301758355085122;
	setAttr ".wl[669].w[12]" 0.48928394631639011;
	setAttr ".wl[669].w[13]" 0.47972392327877988;
	setAttr ".wl[669].w[14]" 0.0067009565645272792;
	setAttr -s 5 ".wl[670].w";
	setAttr ".wl[670].w[3]" 0.0034960751600901078;
	setAttr ".wl[670].w[4]" 0.024866120793309812;
	setAttr ".wl[670].w[12]" 0.48657746909833866;
	setAttr ".wl[670].w[13]" 0.47718560182234543;
	setAttr ".wl[670].w[14]" 0.0078747331259160225;
	setAttr -s 5 ".wl[671].w";
	setAttr ".wl[671].w[3]" 0.0039567711296968925;
	setAttr ".wl[671].w[4]" 0.028460514117347725;
	setAttr ".wl[671].w[12]" 0.48377323191338323;
	setAttr ".wl[671].w[13]" 0.47470281862794783;
	setAttr ".wl[671].w[14]" 0.0091066642116243365;
	setAttr -s 5 ".wl[672].w";
	setAttr ".wl[672].w[3]" 0.0037745578637357296;
	setAttr ".wl[672].w[4]" 0.028282989092012979;
	setAttr ".wl[672].w[12]" 0.48377076184391121;
	setAttr ".wl[672].w[13]" 0.47505309666924422;
	setAttr ".wl[672].w[14]" 0.0091185945310958399;
	setAttr -s 5 ".wl[673].w";
	setAttr ".wl[673].w[3]" 0.0035927328059892835;
	setAttr ".wl[673].w[4]" 0.027696473591806166;
	setAttr ".wl[673].w[12]" 0.48396191588266491;
	setAttr ".wl[673].w[13]" 0.47573200129388049;
	setAttr ".wl[673].w[14]" 0.0090168764256592752;
	setAttr -s 5 ".wl[674].w";
	setAttr ".wl[674].w[3]" 0.0035946459595437735;
	setAttr ".wl[674].w[4]" 0.027903637085019695;
	setAttr ".wl[674].w[12]" 0.48370273045680373;
	setAttr ".wl[674].w[13]" 0.47566621736523212;
	setAttr ".wl[674].w[14]" 0.0091327691334007702;
	setAttr -s 5 ".wl[675].w";
	setAttr ".wl[675].w[3]" 0.0035007544007654018;
	setAttr ".wl[675].w[4]" 0.027484138152700545;
	setAttr ".wl[675].w[12]" 0.48426531065007034;
	setAttr ".wl[675].w[13]" 0.47581145822382914;
	setAttr ".wl[675].w[14]" 0.0089383385726343779;
	setAttr -s 5 ".wl[676].w";
	setAttr ".wl[676].w[4]" 0.027806611485897286;
	setAttr ".wl[676].w[5]" 0.0040244017453063561;
	setAttr ".wl[676].w[12]" 0.48422276763106425;
	setAttr ".wl[676].w[13]" 0.47500207532959304;
	setAttr ".wl[676].w[14]" 0.0089441438081390209;
	setAttr -s 5 ".wl[677].w";
	setAttr ".wl[677].w[4]" 0.027733590152211652;
	setAttr ".wl[677].w[5]" 0.0048257574004954163;
	setAttr ".wl[677].w[12]" 0.48422023651901303;
	setAttr ".wl[677].w[13]" 0.47437770402831642;
	setAttr ".wl[677].w[14]" 0.0088427118999635339;
	setAttr -s 5 ".wl[678].w";
	setAttr ".wl[678].w[4]" 0.023980095150318719;
	setAttr ".wl[678].w[5]" 0.0050010245894712892;
	setAttr ".wl[678].w[12]" 0.48693399732049342;
	setAttr ".wl[678].w[13]" 0.47652445675602312;
	setAttr ".wl[678].w[14]" 0.0075604261836934505;
	setAttr -s 5 ".wl[679].w";
	setAttr ".wl[679].w[4]" 0.020340476516463372;
	setAttr ".wl[679].w[5]" 0.0051091536515279496;
	setAttr ".wl[679].w[12]" 0.48947431494328925;
	setAttr ".wl[679].w[13]" 0.47870951779420151;
	setAttr ".wl[679].w[14]" 0.0063665370945179189;
	setAttr -s 5 ".wl[680].w";
	setAttr ".wl[680].w[4]" 0.018309818333535972;
	setAttr ".wl[680].w[5]" 0.0053680513856264678;
	setAttr ".wl[680].w[12]" 0.49093460243714448;
	setAttr ".wl[680].w[13]" 0.47969928371096804;
	setAttr ".wl[680].w[14]" 0.0056882441327250309;
	setAttr -s 5 ".wl[681].w";
	setAttr ".wl[681].w[4]" 0.016143934128134165;
	setAttr ".wl[681].w[5]" 0.0053610986454023842;
	setAttr ".wl[681].w[7]" 0.0053610986454023842;
	setAttr ".wl[681].w[12]" 0.49282957696301083;
	setAttr ".wl[681].w[13]" 0.48030429161805033;
	setAttr -s 5 ".wl[682].w";
	setAttr ".wl[682].w[4]" 0.0075266926011519804;
	setAttr ".wl[682].w[5]" 0.0035028532397161673;
	setAttr ".wl[682].w[12]" 0.38385243314214057;
	setAttr ".wl[682].w[13]" 0.59379650251327054;
	setAttr ".wl[682].w[14]" 0.011321518503720835;
	setAttr -s 5 ".wl[683].w";
	setAttr ".wl[683].w[4]" 0.0070341089086169706;
	setAttr ".wl[683].w[5]" 0.0034120497176131455;
	setAttr ".wl[683].w[12]" 0.38180367116353903;
	setAttr ".wl[683].w[13]" 0.59724088428226041;
	setAttr ".wl[683].w[14]" 0.010509285927970522;
	setAttr -s 5 ".wl[684].w";
	setAttr ".wl[684].w[4]" 0.0057984601888118638;
	setAttr ".wl[684].w[5]" 0.0026327536868095188;
	setAttr ".wl[684].w[12]" 0.37074668583369941;
	setAttr ".wl[684].w[13]" 0.61204526815203786;
	setAttr ".wl[684].w[14]" 0.0087768321386412235;
	setAttr -s 5 ".wl[685].w";
	setAttr ".wl[685].w[4]" 0.0050732096932115302;
	setAttr ".wl[685].w[5]" 0.0020528316026895525;
	setAttr ".wl[685].w[12]" 0.36153164489961265;
	setAttr ".wl[685].w[13]" 0.62354506564024903;
	setAttr ".wl[685].w[14]" 0.0077972481642371409;
	setAttr -s 5 ".wl[686].w";
	setAttr ".wl[686].w[4]" 0.005063680670839516;
	setAttr ".wl[686].w[5]" 0.0018305418881849373;
	setAttr ".wl[686].w[12]" 0.36130807813899246;
	setAttr ".wl[686].w[13]" 0.62398859078222824;
	setAttr ".wl[686].w[14]" 0.0078091085197549287;
	setAttr -s 5 ".wl[687].w";
	setAttr ".wl[687].w[4]" 0.0052053027592068165;
	setAttr ".wl[687].w[5]" 0.0016455167112428293;
	setAttr ".wl[687].w[12]" 0.36417985503675415;
	setAttr ".wl[687].w[13]" 0.62098880268603907;
	setAttr ".wl[687].w[14]" 0.0079805228067569966;
	setAttr -s 5 ".wl[688].w";
	setAttr ".wl[688].w[4]" 0.0060998478209032094;
	setAttr ".wl[688].w[5]" 0.0016367400054615726;
	setAttr ".wl[688].w[12]" 0.37583576054612766;
	setAttr ".wl[688].w[13]" 0.60724281423030824;
	setAttr ".wl[688].w[14]" 0.0091848373971994031;
	setAttr -s 5 ".wl[689].w";
	setAttr ".wl[689].w[4]" 0.0075134302076832506;
	setAttr ".wl[689].w[5]" 0.0017129260229361537;
	setAttr ".wl[689].w[12]" 0.38824017862212501;
	setAttr ".wl[689].w[13]" 0.59139275290092663;
	setAttr ".wl[689].w[14]" 0.011140712246329051;
	setAttr -s 5 ".wl[690].w";
	setAttr ".wl[690].w[4]" 0.0081183435523576077;
	setAttr ".wl[690].w[5]" 0.001592833926478364;
	setAttr ".wl[690].w[12]" 0.39116442454220224;
	setAttr ".wl[690].w[13]" 0.58702461062965405;
	setAttr ".wl[690].w[14]" 0.012099787349307702;
	setAttr -s 5 ".wl[691].w";
	setAttr ".wl[691].w[3]" 0.0015757519063604589;
	setAttr ".wl[691].w[4]" 0.0089324340638409685;
	setAttr ".wl[691].w[12]" 0.39410581840442693;
	setAttr ".wl[691].w[13]" 0.58195779609745446;
	setAttr ".wl[691].w[14]" 0.013428199527917141;
	setAttr -s 5 ".wl[692].w";
	setAttr ".wl[692].w[3]" 0.001813483698600377;
	setAttr ".wl[692].w[4]" 0.010092492732999808;
	setAttr ".wl[692].w[12]" 0.39872554786988595;
	setAttr ".wl[692].w[13]" 0.574205596767144;
	setAttr ".wl[692].w[14]" 0.015162878931369897;
	setAttr -s 5 ".wl[693].w";
	setAttr ".wl[693].w[3]" 0.0020057086094302052;
	setAttr ".wl[693].w[4]" 0.011127943505940886;
	setAttr ".wl[693].w[12]" 0.40283348424703863;
	setAttr ".wl[693].w[13]" 0.56744269088212218;
	setAttr ".wl[693].w[14]" 0.016590172755467997;
	setAttr -s 5 ".wl[694].w";
	setAttr ".wl[694].w[3]" 0.0023293263199995707;
	setAttr ".wl[694].w[4]" 0.012956683214025324;
	setAttr ".wl[694].w[12]" 0.40904737594234974;
	setAttr ".wl[694].w[13]" 0.55668916971623783;
	setAttr ".wl[694].w[14]" 0.018977444807387583;
	setAttr -s 5 ".wl[695].w";
	setAttr ".wl[695].w[3]" 0.0026332448326530358;
	setAttr ".wl[695].w[4]" 0.014830690542229521;
	setAttr ".wl[695].w[12]" 0.41338773496917036;
	setAttr ".wl[695].w[13]" 0.54770665460531798;
	setAttr ".wl[695].w[14]" 0.02144167505062905;
	setAttr -s 5 ".wl[696].w";
	setAttr ".wl[696].w[3]" 0.0025368280506448963;
	setAttr ".wl[696].w[4]" 0.014743299092897686;
	setAttr ".wl[696].w[12]" 0.41220211950069713;
	setAttr ".wl[696].w[13]" 0.54901443592463783;
	setAttr ".wl[696].w[14]" 0.021503317431122389;
	setAttr -s 5 ".wl[697].w";
	setAttr ".wl[697].w[3]" 0.0024335358037821451;
	setAttr ".wl[697].w[4]" 0.014450828277296615;
	setAttr ".wl[697].w[12]" 0.41018970036075353;
	setAttr ".wl[697].w[13]" 0.551565462317957;
	setAttr ".wl[697].w[14]" 0.021360473240210724;
	setAttr -s 5 ".wl[698].w";
	setAttr ".wl[698].w[4]" 0.014553678410489188;
	setAttr ".wl[698].w[5]" 0.0025069830871271275;
	setAttr ".wl[698].w[12]" 0.40968927254917159;
	setAttr ".wl[698].w[13]" 0.55164451894200706;
	setAttr ".wl[698].w[14]" 0.02160554701120506;
	setAttr -s 5 ".wl[699].w";
	setAttr ".wl[699].w[4]" 0.014279818501665782;
	setAttr ".wl[699].w[5]" 0.0026835022853938517;
	setAttr ".wl[699].w[12]" 0.40919882103708238;
	setAttr ".wl[699].w[13]" 0.552684242637978;
	setAttr ".wl[699].w[14]" 0.021153615537879995;
	setAttr -s 5 ".wl[700].w";
	setAttr ".wl[700].w[4]" 0.01437019642509004;
	setAttr ".wl[700].w[5]" 0.0030706788573430272;
	setAttr ".wl[700].w[12]" 0.41009984745379757;
	setAttr ".wl[700].w[13]" 0.55140223568943192;
	setAttr ".wl[700].w[14]" 0.021057041574337517;
	setAttr -s 5 ".wl[701].w";
	setAttr ".wl[701].w[4]" 0.014271512624566884;
	setAttr ".wl[701].w[5]" 0.0035652768320726383;
	setAttr ".wl[701].w[12]" 0.41021431868808034;
	setAttr ".wl[701].w[13]" 0.55117297842395552;
	setAttr ".wl[701].w[14]" 0.020775913431324514;
	setAttr -s 5 ".wl[702].w";
	setAttr ".wl[702].w[4]" 0.012296513185255986;
	setAttr ".wl[702].w[5]" 0.0035996066185164399;
	setAttr ".wl[702].w[12]" 0.40453581537442485;
	setAttr ".wl[702].w[13]" 0.56140166688681059;
	setAttr ".wl[702].w[14]" 0.018166397934992209;
	setAttr -s 5 ".wl[703].w";
	setAttr ".wl[703].w[4]" 0.010429757231688902;
	setAttr ".wl[703].w[5]" 0.0035569035464722175;
	setAttr ".wl[703].w[12]" 0.39697270764201226;
	setAttr ".wl[703].w[13]" 0.57333854158809905;
	setAttr ".wl[703].w[14]" 0.015702089991727518;
	setAttr -s 5 ".wl[704].w";
	setAttr ".wl[704].w[4]" 0.009390053356684077;
	setAttr ".wl[704].w[5]" 0.0036153531208186953;
	setAttr ".wl[704].w[12]" 0.39200139206544476;
	setAttr ".wl[704].w[13]" 0.58074038431550523;
	setAttr ".wl[704].w[14]" 0.014252817141547287;
	setAttr -s 5 ".wl[705].w";
	setAttr ".wl[705].w[4]" 0.0082696834174695501;
	setAttr ".wl[705].w[5]" 0.003506742930545423;
	setAttr ".wl[705].w[12]" 0.38674093031176482;
	setAttr ".wl[705].w[13]" 0.58892487968846408;
	setAttr ".wl[705].w[14]" 0.01255776365175622;
	setAttr -s 5 ".wl[706].w";
	setAttr ".wl[706].w[4]" 0.0048662674376495621;
	setAttr ".wl[706].w[5]" 0.0026217569330061869;
	setAttr ".wl[706].w[12]" 0.16005424970925272;
	setAttr ".wl[706].w[13]" 0.79479465446482267;
	setAttr ".wl[706].w[14]" 0.037663071455268829;
	setAttr -s 5 ".wl[707].w";
	setAttr ".wl[707].w[4]" 0.0045582960461816495;
	setAttr ".wl[707].w[5]" 0.0025348527430238744;
	setAttr ".wl[707].w[12]" 0.15531614653306472;
	setAttr ".wl[707].w[13]" 0.80224070924460789;
	setAttr ".wl[707].w[14]" 0.035349995433121881;
	setAttr -s 5 ".wl[708].w";
	setAttr ".wl[708].w[4]" 0.0037581968590166444;
	setAttr ".wl[708].w[5]" 0.0019912226216155767;
	setAttr ".wl[708].w[12]" 0.13897522348697186;
	setAttr ".wl[708].w[13]" 0.82501695773740258;
	setAttr ".wl[708].w[14]" 0.030258399294993292;
	setAttr -s 5 ".wl[709].w";
	setAttr ".wl[709].w[4]" 0.0032911131715772429;
	setAttr ".wl[709].w[5]" 0.0015993498007471122;
	setAttr ".wl[709].w[12]" 0.12787329552784429;
	setAttr ".wl[709].w[13]" 0.83992135691370273;
	setAttr ".wl[709].w[14]" 0.027314884586128668;
	setAttr -s 5 ".wl[710].w";
	setAttr ".wl[710].w[4]" 0.0032999522002589415;
	setAttr ".wl[710].w[5]" 0.0014706933792677755;
	setAttr ".wl[710].w[12]" 0.12804943930367016;
	setAttr ".wl[710].w[13]" 0.83972594583919147;
	setAttr ".wl[710].w[14]" 0.02745396927761164;
	setAttr -s 5 ".wl[711].w";
	setAttr ".wl[711].w[4]" 0.0034125458244522256;
	setAttr ".wl[711].w[5]" 0.0013698340182071297;
	setAttr ".wl[711].w[12]" 0.13122573234990614;
	setAttr ".wl[711].w[13]" 0.83589843042309653;
	setAttr ".wl[711].w[14]" 0.028093457384337949;
	setAttr -s 5 ".wl[712].w";
	setAttr ".wl[712].w[4]" 0.0040339643168873984;
	setAttr ".wl[712].w[5]" 0.0014200202137356541;
	setAttr ".wl[712].w[12]" 0.14597869484888071;
	setAttr ".wl[712].w[13]" 0.81664226115662686;
	setAttr ".wl[712].w[14]" 0.031925059463869351;
	setAttr -s 5 ".wl[713].w";
	setAttr ".wl[713].w[4]" 0.0049929877054493456;
	setAttr ".wl[713].w[5]" 0.0015369385403427071;
	setAttr ".wl[713].w[12]" 0.16504916060907196;
	setAttr ".wl[713].w[13]" 0.79064628590812935;
	setAttr ".wl[713].w[14]" 0.037774627237006628;
	setAttr -s 5 ".wl[714].w";
	setAttr ".wl[714].w[4]" 0.0054019030290735209;
	setAttr ".wl[714].w[5]" 0.0014679714244779575;
	setAttr ".wl[714].w[12]" 0.17145258039238656;
	setAttr ".wl[714].w[13]" 0.78109400876959034;
	setAttr ".wl[714].w[14]" 0.040583536384471673;
	setAttr -s 5 ".wl[715].w";
	setAttr ".wl[715].w[4]" 0.0059394157411877145;
	setAttr ".wl[715].w[5]" 0.0014620937004781989;
	setAttr ".wl[715].w[12]" 0.17890076384793888;
	setAttr ".wl[715].w[13]" 0.76936616554213;
	setAttr ".wl[715].w[14]" 0.044331561168265157;
	setAttr -s 5 ".wl[716].w";
	setAttr ".wl[716].w[4]" 0.0066977774177339059;
	setAttr ".wl[716].w[5]" 0.0015450631518043457;
	setAttr ".wl[716].w[12]" 0.18925718151640722;
	setAttr ".wl[716].w[13]" 0.75352454824262138;
	setAttr ".wl[716].w[14]" 0.048975429671433042;
	setAttr -s 5 ".wl[717].w";
	setAttr ".wl[717].w[3]" 0.0016243308139703681;
	setAttr ".wl[717].w[4]" 0.0073675219372702355;
	setAttr ".wl[717].w[12]" 0.19805738160722322;
	setAttr ".wl[717].w[13]" 0.74036195667148663;
	setAttr ".wl[717].w[14]" 0.052588808970049632;
	setAttr -s 5 ".wl[718].w";
	setAttr ".wl[718].w[3]" 0.0018743943852654011;
	setAttr ".wl[718].w[4]" 0.0085379176002189534;
	setAttr ".wl[718].w[12]" 0.21208604717059307;
	setAttr ".wl[718].w[13]" 0.71920799688476222;
	setAttr ".wl[718].w[14]" 0.058293643959160417;
	setAttr -s 5 ".wl[719].w";
	setAttr ".wl[719].w[3]" 0.0021081711506263962;
	setAttr ".wl[719].w[4]" 0.0097189619061080566;
	setAttr ".wl[719].w[12]" 0.22393333088736686;
	setAttr ".wl[719].w[13]" 0.70036275942865844;
	setAttr ".wl[719].w[14]" 0.0638767766272402;
	setAttr -s 5 ".wl[720].w";
	setAttr ".wl[720].w[4]" 0.0096378145724957693;
	setAttr ".wl[720].w[5]" 0.0020702889092561755;
	setAttr ".wl[720].w[12]" 0.22217503155148263;
	setAttr ".wl[720].w[13]" 0.70208646978624445;
	setAttr ".wl[720].w[14]" 0.064030395180521102;
	setAttr -s 5 ".wl[721].w";
	setAttr ".wl[721].w[4]" 0.0094223881327690894;
	setAttr ".wl[721].w[5]" 0.0021350126160053309;
	setAttr ".wl[721].w[12]" 0.2187797194479798;
	setAttr ".wl[721].w[13]" 0.70591739370718454;
	setAttr ".wl[721].w[14]" 0.063745486096061343;
	setAttr -s 5 ".wl[722].w";
	setAttr ".wl[722].w[4]" 0.0094641133174949953;
	setAttr ".wl[722].w[5]" 0.0022755669266591515;
	setAttr ".wl[722].w[12]" 0.21849761963448572;
	setAttr ".wl[722].w[13]" 0.70549060432016086;
	setAttr ".wl[722].w[14]" 0.064272095801199344;
	setAttr -s 5 ".wl[723].w";
	setAttr ".wl[723].w[4]" 0.0092689163127550288;
	setAttr ".wl[723].w[5]" 0.0023990281362743647;
	setAttr ".wl[723].w[12]" 0.21671937692841495;
	setAttr ".wl[723].w[13]" 0.70848439904171556;
	setAttr ".wl[723].w[14]" 0.063128279580840149;
	setAttr -s 5 ".wl[724].w";
	setAttr ".wl[724].w[4]" 0.0093027432644318449;
	setAttr ".wl[724].w[5]" 0.0026797428171987984;
	setAttr ".wl[724].w[12]" 0.21771818630611067;
	setAttr ".wl[724].w[13]" 0.70759704630064069;
	setAttr ".wl[724].w[14]" 0.062702281311618063;
	setAttr -s 5 ".wl[725].w";
	setAttr ".wl[725].w[4]" 0.0092186165106547379;
	setAttr ".wl[725].w[5]" 0.0030200752989980738;
	setAttr ".wl[725].w[12]" 0.21727253040001879;
	setAttr ".wl[725].w[13]" 0.7085868720275873;
	setAttr ".wl[725].w[14]" 0.061901905762741107;
	setAttr -s 5 ".wl[726].w";
	setAttr ".wl[726].w[4]" 0.0079463747547460356;
	setAttr ".wl[726].w[5]" 0.0029673052849843715;
	setAttr ".wl[726].w[12]" 0.20325367459687083;
	setAttr ".wl[726].w[13]" 0.73009303620241894;
	setAttr ".wl[726].w[14]" 0.055739609160979829;
	setAttr -s 5 ".wl[727].w";
	setAttr ".wl[727].w[4]" 0.0067394311627822692;
	setAttr ".wl[727].w[5]" 0.0028475735123226617;
	setAttr ".wl[727].w[12]" 0.18746670894435621;
	setAttr ".wl[727].w[13]" 0.75333693045560624;
	setAttr ".wl[727].w[14]" 0.049609355924932645;
	setAttr -s 5 ".wl[728].w";
	setAttr ".wl[728].w[4]" 0.0060649916630091463;
	setAttr ".wl[728].w[5]" 0.0028205629217947863;
	setAttr ".wl[728].w[12]" 0.17775958936831021;
	setAttr ".wl[728].w[13]" 0.76754612959997515;
	setAttr ".wl[728].w[14]" 0.045808726446910725;
	setAttr -s 5 ".wl[729].w";
	setAttr ".wl[729].w[4]" 0.0053408386834128243;
	setAttr ".wl[729].w[5]" 0.0026788804316777966;
	setAttr ".wl[729].w[12]" 0.16700287725338295;
	setAttr ".wl[729].w[13]" 0.78380928286583662;
	setAttr ".wl[729].w[14]" 0.041168120765689833;
	setAttr -s 5 ".wl[730].w";
	setAttr ".wl[730].w[4]" 0.0027006159263266;
	setAttr ".wl[730].w[5]" 0.0016144685093294679;
	setAttr ".wl[730].w[12]" 0.052048460302175525;
	setAttr ".wl[730].w[13]" 0.82674540284699305;
	setAttr ".wl[730].w[14]" 0.11689105241517533;
	setAttr -s 5 ".wl[731].w";
	setAttr ".wl[731].w[4]" 0.0025311718311503174;
	setAttr ".wl[731].w[5]" 0.001551162795998212;
	setAttr ".wl[731].w[12]" 0.049736848887901031;
	setAttr ".wl[731].w[13]" 0.83491530407403591;
	setAttr ".wl[731].w[14]" 0.11126551241091458;
	setAttr -s 5 ".wl[732].w";
	setAttr ".wl[732].w[4]" 0.002078235755738728;
	setAttr ".wl[732].w[5]" 0.0012275985868376133;
	setAttr ".wl[732].w[12]" 0.042538451020353356;
	setAttr ".wl[732].w[13]" 0.85553341614698042;
	setAttr ".wl[732].w[14]" 0.098622298490089896;
	setAttr -s 5 ".wl[733].w";
	setAttr ".wl[733].w[4]" 0.001819351953495723;
	setAttr ".wl[733].w[5]" 0.0010051649609912783;
	setAttr ".wl[733].w[12]" 0.038107303205254017;
	setAttr ".wl[733].w[13]" 0.86787590801423542;
	setAttr ".wl[733].w[14]" 0.091192271866023494;
	setAttr -s 5 ".wl[734].w";
	setAttr ".wl[734].w[4]" 0.0018328644729897589;
	setAttr ".wl[734].w[5]" 0.00094605966129984796;
	setAttr ".wl[734].w[12]" 0.038333369562983013;
	setAttr ".wl[734].w[13]" 0.86701967530035706;
	setAttr ".wl[734].w[14]" 0.091868031002370204;
	setAttr -s 5 ".wl[735].w";
	setAttr ".wl[735].w[4]" 0.0019071267672801176;
	setAttr ".wl[735].w[5]" 0.00090591117445145699;
	setAttr ".wl[735].w[12]" 0.039715197859985368;
	setAttr ".wl[735].w[13]" 0.86369280324875553;
	setAttr ".wl[735].w[14]" 0.093778960949527576;
	setAttr -s 5 ".wl[736].w";
	setAttr ".wl[736].w[4]" 0.002279616854975948;
	setAttr ".wl[736].w[5]" 0.00097369020840049004;
	setAttr ".wl[736].w[12]" 0.046088771710817392;
	setAttr ".wl[736].w[13]" 0.84672450599119453;
	setAttr ".wl[736].w[14]" 0.10393341523461155;
	setAttr -s 5 ".wl[737].w";
	setAttr ".wl[737].w[4]" 0.0028527779290727063;
	setAttr ".wl[737].w[5]" 0.0010907847658821691;
	setAttr ".wl[737].w[12]" 0.055070682079989886;
	setAttr ".wl[737].w[13]" 0.82248728740870392;
	setAttr ".wl[737].w[14]" 0.11849846781635125;
	setAttr -s 5 ".wl[738].w";
	setAttr ".wl[738].w[4]" 0.0030984533772852809;
	setAttr ".wl[738].w[5]" 0.0010680232553064374;
	setAttr ".wl[738].w[12]" 0.058469700153526281;
	setAttr ".wl[738].w[13]" 0.81201284981602684;
	setAttr ".wl[738].w[14]" 0.12535097339785517;
	setAttr -s 5 ".wl[739].w";
	setAttr ".wl[739].w[4]" 0.0034157154455244755;
	setAttr ".wl[739].w[5]" 0.001082858855229534;
	setAttr ".wl[739].w[12]" 0.062573663346443043;
	setAttr ".wl[739].w[13]" 0.79883925494287045;
	setAttr ".wl[739].w[14]" 0.1340885074099325;
	setAttr -s 5 ".wl[740].w";
	setAttr ".wl[740].w[4]" 0.0038643474473709877;
	setAttr ".wl[740].w[5]" 0.001158215006875256;
	setAttr ".wl[740].w[12]" 0.068335877879386803;
	setAttr ".wl[740].w[13]" 0.78245833579321744;
	setAttr ".wl[740].w[14]" 0.1441832238731495;
	setAttr -s 5 ".wl[741].w";
	setAttr ".wl[741].w[4]" 0.0042623194170673681;
	setAttr ".wl[741].w[5]" 0.0012246072910925068;
	setAttr ".wl[741].w[12]" 0.073360217673631156;
	setAttr ".wl[741].w[13]" 0.76963569516237251;
	setAttr ".wl[741].w[14]" 0.15151716045583641;
	setAttr -s 5 ".wl[742].w";
	setAttr ".wl[742].w[4]" 0.0049626718970091537;
	setAttr ".wl[742].w[5]" 0.0013768629798474967;
	setAttr ".wl[742].w[12]" 0.081817560477263851;
	setAttr ".wl[742].w[13]" 0.74947578726138508;
	setAttr ".wl[742].w[14]" 0.1623671173844945;
	setAttr -s 5 ".wl[743].w";
	setAttr ".wl[743].w[4]" 0.0056678603480087384;
	setAttr ".wl[743].w[5]" 0.0015543979399586011;
	setAttr ".wl[743].w[12]" 0.089577854104132854;
	setAttr ".wl[743].w[13]" 0.73085127039912667;
	setAttr ".wl[743].w[14]" 0.17234861720877326;
	setAttr -s 5 ".wl[744].w";
	setAttr ".wl[744].w[4]" 0.0055989999797268286;
	setAttr ".wl[744].w[5]" 0.001571746732073901;
	setAttr ".wl[744].w[12]" 0.088457253064292807;
	setAttr ".wl[744].w[13]" 0.73170963041478576;
	setAttr ".wl[744].w[14]" 0.1726623698091207;
	setAttr -s 5 ".wl[745].w";
	setAttr ".wl[745].w[4]" 0.0054467076768176283;
	setAttr ".wl[745].w[5]" 0.0016000249539442359;
	setAttr ".wl[745].w[12]" 0.086318361913301;
	setAttr ".wl[745].w[13]" 0.73436172765931884;
	setAttr ".wl[745].w[14]" 0.17227317779661841;
	setAttr -s 5 ".wl[746].w";
	setAttr ".wl[746].w[4]" 0.0054559528044329534;
	setAttr ".wl[746].w[5]" 0.0016848970860004991;
	setAttr ".wl[746].w[12]" 0.086160113537833677;
	setAttr ".wl[746].w[13]" 0.73351573066795117;
	setAttr ".wl[746].w[14]" 0.17318330590378184;
	setAttr -s 5 ".wl[747].w";
	setAttr ".wl[747].w[4]" 0.0053276168544079926;
	setAttr ".wl[747].w[5]" 0.001751096183174747;
	setAttr ".wl[747].w[12]" 0.084815565558642753;
	setAttr ".wl[747].w[13]" 0.73718178066969864;
	setAttr ".wl[747].w[14]" 0.17092394073407588;
	setAttr -s 5 ".wl[748].w";
	setAttr ".wl[748].w[4]" 0.0053384987410290555;
	setAttr ".wl[748].w[5]" 0.0019189138050527887;
	setAttr ".wl[748].w[12]" 0.0851800083382203;
	setAttr ".wl[748].w[13]" 0.73780006057332348;
	setAttr ".wl[748].w[14]" 0.1697625185423744;
	setAttr -s 5 ".wl[749].w";
	setAttr ".wl[749].w[4]" 0.0052783032708704335;
	setAttr ".wl[749].w[5]" 0.0021105396773463763;
	setAttr ".wl[749].w[12]" 0.084666243377832323;
	setAttr ".wl[749].w[13]" 0.73993507687554272;
	setAttr ".wl[749].w[14]" 0.1680098367984082;
	setAttr -s 5 ".wl[750].w";
	setAttr ".wl[750].w[4]" 0.0045076824528708561;
	setAttr ".wl[750].w[5]" 0.0020084950775726817;
	setAttr ".wl[750].w[12]" 0.075717622783610536;
	setAttr ".wl[750].w[13]" 0.76141759603697701;
	setAttr ".wl[750].w[14]" 0.15634860364896899;
	setAttr -s 5 ".wl[751].w";
	setAttr ".wl[751].w[4]" 0.0037859550216585705;
	setAttr ".wl[751].w[5]" 0.0018655589796068683;
	setAttr ".wl[751].w[12]" 0.06656097652978317;
	setAttr ".wl[751].w[13]" 0.78379057675567354;
	setAttr ".wl[751].w[14]" 0.1439969327132779;
	setAttr -s 5 ".wl[752].w";
	setAttr ".wl[752].w[4]" 0.0033882550666386939;
	setAttr ".wl[752].w[5]" 0.0018030167486315588;
	setAttr ".wl[752].w[12]" 0.061274075580527172;
	setAttr ".wl[752].w[13]" 0.79769357785955919;
	setAttr ".wl[752].w[14]" 0.13584107474464344;
	setAttr -s 5 ".wl[753].w";
	setAttr ".wl[753].w[4]" 0.0029689483370284142;
	setAttr ".wl[753].w[5]" 0.0016778955041544202;
	setAttr ".wl[753].w[12]" 0.055625418581897269;
	setAttr ".wl[753].w[13]" 0.81443128751956617;
	setAttr ".wl[753].w[14]" 0.12529645005735374;
	setAttr -s 5 ".wl[754].w";
	setAttr ".wl[754].w[4]" 0.0012564875071219973;
	setAttr ".wl[754].w[5]" 0.00081094761254603257;
	setAttr ".wl[754].w[12]" 0.015640742968346749;
	setAttr ".wl[754].w[13]" 0.66765853738363634;
	setAttr ".wl[754].w[14]" 0.31463328452834893;
	setAttr -s 5 ".wl[755].w";
	setAttr ".wl[755].w[4]" 0.0011803006483090195;
	setAttr ".wl[755].w[5]" 0.00077704533359698323;
	setAttr ".wl[755].w[12]" 0.014861586142770219;
	setAttr ".wl[755].w[13]" 0.67521227579500198;
	setAttr ".wl[755].w[14]" 0.30796879208032185;
	setAttr -s 5 ".wl[756].w";
	setAttr ".wl[756].w[4]" 0.00097042668191477696;
	setAttr ".wl[756].w[5]" 0.00062071754115514523;
	setAttr ".wl[756].w[12]" 0.012494878288742579;
	setAttr ".wl[756].w[13]" 0.69216140426122896;
	setAttr ".wl[756].w[14]" 0.29375257322695852;
	setAttr -s 5 ".wl[757].w";
	setAttr ".wl[757].w[4]" 0.0008531513865415503;
	setAttr ".wl[757].w[5]" 0.00051749490166763852;
	setAttr ".wl[757].w[12]" 0.011116115321563962;
	setAttr ".wl[757].w[13]" 0.70222391286869423;
	setAttr ".wl[757].w[14]" 0.28528932552153263;
	setAttr -s 5 ".wl[758].w";
	setAttr ".wl[758].w[4]" 0.00086316943529363469;
	setAttr ".wl[758].w[5]" 0.0004957643362194828;
	setAttr ".wl[758].w[12]" 0.011232547722874795;
	setAttr ".wl[758].w[13]" 0.70071237037317957;
	setAttr ".wl[758].w[14]" 0.28669614813243249;
	setAttr -s 5 ".wl[759].w";
	setAttr ".wl[759].w[4]" 0.00090266929162406543;
	setAttr ".wl[759].w[5]" 0.00048475626126683041;
	setAttr ".wl[759].w[12]" 0.01171976369447977;
	setAttr ".wl[759].w[13]" 0.69783872806286706;
	setAttr ".wl[759].w[14]" 0.28905408268976229;
	setAttr -s 5 ".wl[760].w";
	setAttr ".wl[760].w[4]" 0.0010853995548166984;
	setAttr ".wl[760].w[5]" 0.00053426862681950934;
	setAttr ".wl[760].w[12]" 0.013869449483016445;
	setAttr ".wl[760].w[13]" 0.68369208035283913;
	setAttr ".wl[760].w[14]" 0.30081880198250832;
	setAttr -s 5 ".wl[761].w";
	setAttr ".wl[761].w[4]" 0.0013674653570118372;
	setAttr ".wl[761].w[5]" 0.00061389957011685672;
	setAttr ".wl[761].w[12]" 0.017028446939021457;
	setAttr ".wl[761].w[13]" 0.66460336038270151;
	setAttr ".wl[761].w[14]" 0.31638682775114829;
	setAttr -s 5 ".wl[762].w";
	setAttr ".wl[762].w[4]" 0.0014897662522075957;
	setAttr ".wl[762].w[5]" 0.00061316862096164558;
	setAttr ".wl[762].w[12]" 0.018300369255292291;
	setAttr ".wl[762].w[13]" 0.6557140233323322;
	setAttr ".wl[762].w[14]" 0.32388267253920622;
	setAttr -s 5 ".wl[763].w";
	setAttr ".wl[763].w[4]" 0.0016464366244430884;
	setAttr ".wl[763].w[5]" 0.00063120491600838461;
	setAttr ".wl[763].w[12]" 0.019865854782711297;
	setAttr ".wl[763].w[13]" 0.64492254117837078;
	setAttr ".wl[763].w[14]" 0.33293396249846646;
	setAttr -s 5 ".wl[764].w";
	setAttr ".wl[764].w[4]" 0.0018710716821188579;
	setAttr ".wl[764].w[5]" 0.00068349080668885821;
	setAttr ".wl[764].w[12]" 0.022111699322914739;
	setAttr ".wl[764].w[13]" 0.63336691754801322;
	setAttr ".wl[764].w[14]" 0.34196682064026429;
	setAttr -s 5 ".wl[765].w";
	setAttr ".wl[765].w[4]" 0.002073138708342288;
	setAttr ".wl[765].w[5]" 0.00073011115062100469;
	setAttr ".wl[765].w[12]" 0.024120587286737192;
	setAttr ".wl[765].w[13]" 0.62542995578432048;
	setAttr ".wl[765].w[14]" 0.34764620706997917;
	setAttr -s 5 ".wl[766].w";
	setAttr ".wl[766].w[4]" 0.00243494085729352;
	setAttr ".wl[766].w[5]" 0.00083146571119167509;
	setAttr ".wl[766].w[12]" 0.027640154867445029;
	setAttr ".wl[766].w[13]" 0.61398571715399808;
	setAttr ".wl[766].w[14]" 0.35510772141007174;
	setAttr -s 5 ".wl[767].w";
	setAttr ".wl[767].w[4]" 0.0028042701026555952;
	setAttr ".wl[767].w[5]" 0.00094707166209704913;
	setAttr ".wl[767].w[12]" 0.031055279960482424;
	setAttr ".wl[767].w[13]" 0.60355672791124693;
	setAttr ".wl[767].w[14]" 0.36163665036351794;
	setAttr -s 5 ".wl[768].w";
	setAttr ".wl[768].w[4]" 0.0027591112397456403;
	setAttr ".wl[768].w[5]" 0.00095052428568975658;
	setAttr ".wl[768].w[12]" 0.030537838707327344;
	setAttr ".wl[768].w[13]" 0.60339414850961759;
	setAttr ".wl[768].w[14]" 0.36235837725761971;
	setAttr -s 5 ".wl[769].w";
	setAttr ".wl[769].w[4]" 0.0026687175565629166;
	setAttr ".wl[769].w[5]" 0.00095568479486425389;
	setAttr ".wl[769].w[12]" 0.029575360253701409;
	setAttr ".wl[769].w[13]" 0.60394883426031598;
	setAttr ".wl[769].w[14]" 0.36285140313455538;
	setAttr -s 5 ".wl[770].w";
	setAttr ".wl[770].w[4]" 0.0026664711999794871;
	setAttr ".wl[770].w[5]" 0.00099618543181393174;
	setAttr ".wl[770].w[12]" 0.029484488302479555;
	setAttr ".wl[770].w[13]" 0.60312277702818062;
	setAttr ".wl[770].w[14]" 0.36373007803754648;
	setAttr -s 5 ".wl[771].w";
	setAttr ".wl[771].w[4]" 0.0025925493742337304;
	setAttr ".wl[771].w[5]" 0.0010212309682955806;
	setAttr ".wl[771].w[12]" 0.028818033791427888;
	setAttr ".wl[771].w[13]" 0.60559747833250988;
	setAttr ".wl[771].w[14]" 0.36197070753353283;
	setAttr -s 5 ".wl[772].w";
	setAttr ".wl[772].w[4]" 0.0025921364493286798;
	setAttr ".wl[772].w[5]" 0.001100933993039877;
	setAttr ".wl[772].w[12]" 0.028878827857492712;
	setAttr ".wl[772].w[13]" 0.60697573880385658;
	setAttr ".wl[772].w[14]" 0.36045236289628219;
	setAttr -s 5 ".wl[773].w";
	setAttr ".wl[773].w[4]" 0.0025546467268146664;
	setAttr ".wl[773].w[5]" 0.0011859401286599515;
	setAttr ".wl[773].w[12]" 0.028570493782088615;
	setAttr ".wl[773].w[13]" 0.60894549488900807;
	setAttr ".wl[773].w[14]" 0.35874342447342866;
	setAttr -s 5 ".wl[774].w";
	setAttr ".wl[774].w[4]" 0.0021500764382934349;
	setAttr ".wl[774].w[5]" 0.0010923364088337348;
	setAttr ".wl[774].w[12]" 0.024706936015239725;
	setAttr ".wl[774].w[13]" 0.62146506086083775;
	setAttr ".wl[774].w[14]" 0.35058559027679542;
	setAttr -s 5 ".wl[775].w";
	setAttr ".wl[775].w[4]" 0.0017820485958237088;
	setAttr ".wl[775].w[5]" 0.00098361538369520925;
	setAttr ".wl[775].w[12]" 0.02102038809668123;
	setAttr ".wl[775].w[13]" 0.63490639840844476;
	setAttr ".wl[775].w[14]" 0.34130754951535514;
	setAttr -s 5 ".wl[776].w";
	setAttr ".wl[776].w[4]" 0.001584328176462999;
	setAttr ".wl[776].w[5]" 0.0009309563830749082;
	setAttr ".wl[776].w[12]" 0.018993607927985059;
	setAttr ".wl[776].w[13]" 0.64409782938887328;
	setAttr ".wl[776].w[14]" 0.33439327812360375;
	setAttr -s 5 ".wl[777].w";
	setAttr ".wl[777].w[4]" 0.0013816465469543709;
	setAttr ".wl[777].w[5]" 0.00085240636434133412;
	setAttr ".wl[777].w[12]" 0.016910075681196183;
	setAttr ".wl[777].w[13]" 0.65674509532719738;
	setAttr ".wl[777].w[14]" 0.32411077608031069;
	setAttr -s 5 ".wl[778].w";
	setAttr ".wl[778].w[4]" 0.00041755821721814278;
	setAttr ".wl[778].w[5]" 0.00025123150486483406;
	setAttr ".wl[778].w[9]" 0.0034262164167024982;
	setAttr ".wl[778].w[10]" 0.49795249693060728;
	setAttr ".wl[778].w[11]" 0.49795249693060728;
	setAttr -s 5 ".wl[779].w";
	setAttr ".wl[779].w[4]" 0.0012724523918101061;
	setAttr ".wl[779].w[5]" 0.00076514441839644384;
	setAttr ".wl[779].w[9]" 0.012267940763223214;
	setAttr ".wl[779].w[10]" 0.50752611778256018;
	setAttr ".wl[779].w[11]" 0.47816834464401009;
	setAttr -s 5 ".wl[780].w";
	setAttr ".wl[780].w[4]" 0.0015149570757624982;
	setAttr ".wl[780].w[5]" 0.00077498781231600037;
	setAttr ".wl[780].w[9]" 0.014359882425399071;
	setAttr ".wl[780].w[10]" 0.5058745201196162;
	setAttr ".wl[780].w[11]" 0.47747565256690633;
	setAttr -s 5 ".wl[781].w";
	setAttr ".wl[781].w[4]" 0.0012666526193368585;
	setAttr ".wl[781].w[5]" 0.00055484422318836143;
	setAttr ".wl[781].w[9]" 0.012176580578869523;
	setAttr ".wl[781].w[10]" 0.50687990523881366;
	setAttr ".wl[781].w[11]" 0.47912201733979159;
	setAttr -s 5 ".wl[782].w";
	setAttr ".wl[782].w[4]" 0.00041811630368234973;
	setAttr ".wl[782].w[5]" 0.00020745100511638154;
	setAttr ".wl[782].w[9]" 0.0034248645601705993;
	setAttr ".wl[782].w[10]" 0.49797478406551549;
	setAttr ".wl[782].w[11]" 0.49797478406551526;
	setAttr -s 5 ".wl[783].w";
	setAttr ".wl[783].w[4]" 9.4245358637186182e-005;
	setAttr ".wl[783].w[5]" 5.2411337477675664e-005;
	setAttr ".wl[783].w[9]" 0.00074724581690060294;
	setAttr ".wl[783].w[10]" 0.49955304874349227;
	setAttr ".wl[783].w[11]" 0.49955304874349227;
	setAttr -s 5 ".wl[784].w";
	setAttr ".wl[784].w[4]" 0.00026057493915313375;
	setAttr ".wl[784].w[5]" 0.00011839472631666144;
	setAttr ".wl[784].w[9]" 0.0021786326559633112;
	setAttr ".wl[784].w[10]" 0.49872119883928345;
	setAttr ".wl[784].w[11]" 0.49872119883928345;
	setAttr -s 5 ".wl[785].w";
	setAttr ".wl[785].w[4]" 7.1454983780895666e-005;
	setAttr ".wl[785].w[5]" 3.6599638481467153e-005;
	setAttr ".wl[785].w[9]" 0.00056955862401116832;
	setAttr ".wl[785].w[10]" 0.4996611933768631;
	setAttr ".wl[785].w[11]" 0.49966119337686332;
	setAttr -s 5 ".wl[786].w";
	setAttr ".wl[786].w[4]" 0.00036782514781119521;
	setAttr ".wl[786].w[5]" 0.00016854363045620412;
	setAttr ".wl[786].w[9]" 0.0030296331166977009;
	setAttr ".wl[786].w[10]" 0.49821699905251748;
	setAttr ".wl[786].w[11]" 0.49821699905251748;
	setAttr -s 5 ".wl[787].w";
	setAttr ".wl[787].w[4]" 0.001162778019674731;
	setAttr ".wl[787].w[5]" 0.00046517017855993284;
	setAttr ".wl[787].w[9]" 0.011261454917314978;
	setAttr ".wl[787].w[10]" 0.50757713968996432;
	setAttr ".wl[787].w[11]" 0.47953345719448598;
	setAttr -s 5 ".wl[788].w";
	setAttr ".wl[788].w[4]" 0.0011628433516624648;
	setAttr ".wl[788].w[5]" 0.00044001823949080628;
	setAttr ".wl[788].w[9]" 0.011290642282409377;
	setAttr ".wl[788].w[10]" 0.50820550357801597;
	setAttr ".wl[788].w[11]" 0.47890099254842144;
	setAttr -s 5 ".wl[789].w";
	setAttr ".wl[789].w[4]" 0.00085310689183477656;
	setAttr ".wl[789].w[5]" 0.00033654813897730882;
	setAttr ".wl[789].w[9]" 0.0084961597970187059;
	setAttr ".wl[789].w[10]" 0.51073542413460982;
	setAttr ".wl[789].w[11]" 0.47957876103755931;
	setAttr -s 5 ".wl[790].w";
	setAttr ".wl[790].w[4]" 0.0011065901441142439;
	setAttr ".wl[790].w[5]" 0.00078153566316315284;
	setAttr ".wl[790].w[9]" 0.010876133698428866;
	setAttr ".wl[790].w[10]" 0.51047143314486076;
	setAttr ".wl[790].w[11]" 0.47676430734943304;
	setAttr -s 5 ".wl[791].w";
	setAttr ".wl[791].w[4]" 0.0011507571242659199;
	setAttr ".wl[791].w[5]" 0.00076643958518197823;
	setAttr ".wl[791].w[9]" 0.011219636087494309;
	setAttr ".wl[791].w[10]" 0.50893464987551496;
	setAttr ".wl[791].w[11]" 0.47792851732754288;
	setAttr -s 5 ".wl[792].w";
	setAttr ".wl[792].w[4]" 0.00035892953894948288;
	setAttr ".wl[792].w[5]" 0.00023479270823789744;
	setAttr ".wl[792].w[9]" 0.0029692366485732451;
	setAttr ".wl[792].w[10]" 0.49821852055211985;
	setAttr ".wl[792].w[11]" 0.49821852055211963;
	setAttr -s 5 ".wl[793].w";
	setAttr ".wl[793].w[4]" 6.7639746750728074e-005;
	setAttr ".wl[793].w[5]" 4.0677976563876406e-005;
	setAttr ".wl[793].w[9]" 0.00054046973653703982;
	setAttr ".wl[793].w[10]" 0.49967560627007424;
	setAttr ".wl[793].w[11]" 0.49967560627007424;
	setAttr -s 5 ".wl[794].w";
	setAttr ".wl[794].w[4]" 0.00023743407741910858;
	setAttr ".wl[794].w[5]" 0.00015389470417986458;
	setAttr ".wl[794].w[9]" 0.0019993817565948178;
	setAttr ".wl[794].w[10]" 0.49880464473090297;
	setAttr ".wl[794].w[11]" 0.49880464473090319;
	setAttr -s 5 ".wl[795].w";
	setAttr ".wl[795].w[4]" 0.00078342429641452803;
	setAttr ".wl[795].w[5]" 0.00051364520396051012;
	setAttr ".wl[795].w[9]" 0.0079086645780241712;
	setAttr ".wl[795].w[10]" 0.51337211937696858;
	setAttr ".wl[795].w[11]" 0.47742214654463233;
	setAttr -s 5 ".wl[796].w";
	setAttr ".wl[796].w[4]" 0.00080424062568930615;
	setAttr ".wl[796].w[5]" 0.00039953317209918883;
	setAttr ".wl[796].w[9]" 0.0081031207446433003;
	setAttr ".wl[796].w[10]" 0.51321006091333077;
	setAttr ".wl[796].w[11]" 0.47748304454423746;
	setAttr -s 5 ".wl[797].w";
	setAttr ".wl[797].w[4]" 0.00070038978370253716;
	setAttr ".wl[797].w[5]" 0.0004108392581686325;
	setAttr ".wl[797].w[9]" 0.0071254688232398559;
	setAttr ".wl[797].w[10]" 0.51427929906866987;
	setAttr ".wl[797].w[11]" 0.47748400306621902;
	setAttr -s 5 ".wl[798].w";
	setAttr ".wl[798].w[4]" 0.00019947722221114463;
	setAttr ".wl[798].w[5]" 0.00011803237863870967;
	setAttr ".wl[798].w[9]" 0.0016893074862601831;
	setAttr ".wl[798].w[10]" 0.49899659145644504;
	setAttr ".wl[798].w[11]" 0.49899659145644504;
	setAttr -s 5 ".wl[799].w";
	setAttr ".wl[799].w[4]" 4.8515516596002228e-005;
	setAttr ".wl[799].w[5]" 2.6783536809760117e-005;
	setAttr ".wl[799].w[9]" 0.00038973413661737554;
	setAttr ".wl[799].w[10]" 0.49976748340498844;
	setAttr ".wl[799].w[11]" 0.49976748340498844;
	setAttr -s 5 ".wl[800].w";
	setAttr ".wl[800].w[4]" 0.00021425285953325136;
	setAttr ".wl[800].w[5]" 0.0001046215546300857;
	setAttr ".wl[800].w[9]" 0.0018062427044679035;
	setAttr ".wl[800].w[10]" 0.49893744144068436;
	setAttr ".wl[800].w[11]" 0.49893744144068436;
	setAttr -s 5 ".wl[801].w";
	setAttr ".wl[801].w[4]" 0.0007521351295761582;
	setAttr ".wl[801].w[5]" 0.00032196038101022455;
	setAttr ".wl[801].w[9]" 0.007577343864082705;
	setAttr ".wl[801].w[10]" 0.51233416665909193;
	setAttr ".wl[801].w[11]" 0.47901439396623902;
	setAttr -s 5 ".wl[802].w";
	setAttr ".wl[802].w[4]" 0.00067307504801879128;
	setAttr ".wl[802].w[5]" 0.00036335932682768747;
	setAttr ".wl[802].w[9]" 0.0056111601537498839;
	setAttr ".wl[802].w[10]" 0.4966762027357019;
	setAttr ".wl[802].w[11]" 0.49667620273570179;
	setAttr -s 5 ".wl[803].w";
	setAttr ".wl[803].w[4]" 0.00049387018174130374;
	setAttr ".wl[803].w[5]" 0.00021075647478076024;
	setAttr ".wl[803].w[9]" 0.0041858756612614895;
	setAttr ".wl[803].w[10]" 0.49755474884110829;
	setAttr ".wl[803].w[11]" 0.49755474884110829;
	setAttr -s 5 ".wl[804].w";
	setAttr ".wl[804].w[4]" 0.00046625271869810673;
	setAttr ".wl[804].w[5]" 0.00032081779438803841;
	setAttr ".wl[804].w[9]" 0.0039831942953628382;
	setAttr ".wl[804].w[10]" 0.49761486759577556;
	setAttr ".wl[804].w[11]" 0.49761486759577556;
	setAttr -s 5 ".wl[805].w";
	setAttr ".wl[805].w[4]" 0.00031788054992161252;
	setAttr ".wl[805].w[5]" 0.00016787324898250407;
	setAttr ".wl[805].w[9]" 0.0027619617819663565;
	setAttr ".wl[805].w[10]" 0.49837614220956478;
	setAttr ".wl[805].w[11]" 0.49837614220956478;
	setAttr -s 5 ".wl[806].w";
	setAttr ".wl[806].w[4]" 0.00080418811720025965;
	setAttr ".wl[806].w[5]" 0.00032864440312841125;
	setAttr ".wl[806].w[9]" 0.0080439141606791168;
	setAttr ".wl[806].w[10]" 0.51116937038294552;
	setAttr ".wl[806].w[11]" 0.47965388293604672;
	setAttr -s 5 ".wl[807].w";
	setAttr ".wl[807].w[4]" 0.00042178694939998978;
	setAttr ".wl[807].w[5]" 0.0001927049828498653;
	setAttr ".wl[807].w[9]" 0.0038337898815549109;
	setAttr ".wl[807].w[10]" 0.49777585909309763;
	setAttr ".wl[807].w[11]" 0.49777585909309763;
	setAttr -s 5 ".wl[808].w";
	setAttr ".wl[808].w[4]" 0.0011261190555839967;
	setAttr ".wl[808].w[5]" 0.00078135012611536619;
	setAttr ".wl[808].w[9]" 0.011033457748055427;
	setAttr ".wl[808].w[10]" 0.50989078986565572;
	setAttr ".wl[808].w[11]" 0.47716828320458948;
	setAttr -s 5 ".wl[809].w";
	setAttr ".wl[809].w[4]" 0.00092207437101139082;
	setAttr ".wl[809].w[5]" 0.00063441110521623338;
	setAttr ".wl[809].w[9]" 0.0092052265413641483;
	setAttr ".wl[809].w[10]" 0.51229675685550558;
	setAttr ".wl[809].w[11]" 0.47694153112690268;
	setAttr -s 5 ".wl[810].w";
	setAttr ".wl[810].w[4]" 0.00044854421597350145;
	setAttr ".wl[810].w[5]" 0.00029474072140259216;
	setAttr ".wl[810].w[9]" 0.0040794828508184848;
	setAttr ".wl[810].w[10]" 0.49758861610590277;
	setAttr ".wl[810].w[11]" 0.49758861610590266;
	setAttr -s 5 ".wl[811].w";
	setAttr ".wl[811].w[4]" 0.00019927473333227296;
	setAttr ".wl[811].w[5]" 0.00012380387429356332;
	setAttr ".wl[811].w[9]" 0.001679276340168448;
	setAttr ".wl[811].w[10]" 0.49899882252610289;
	setAttr ".wl[811].w[11]" 0.49899882252610289;
	setAttr -s 5 ".wl[812].w";
	setAttr ".wl[812].w[4]" 0.00039138259886927265;
	setAttr ".wl[812].w[5]" 0.00023269239249397222;
	setAttr ".wl[812].w[9]" 0.0035828377507276317;
	setAttr ".wl[812].w[10]" 0.49789654362895458;
	setAttr ".wl[812].w[11]" 0.49789654362895458;
	setAttr -s 5 ".wl[813].w";
	setAttr ".wl[813].w[4]" 0.0007393867857549499;
	setAttr ".wl[813].w[5]" 0.00046122243318665003;
	setAttr ".wl[813].w[9]" 0.0074872684421422812;
	setAttr ".wl[813].w[10]" 0.51355327915092641;
	setAttr ".wl[813].w[11]" 0.47775884318798967;
	setAttr -s 5 ".wl[814].w";
	setAttr ".wl[814].w[4]" 0.00011129268745070928;
	setAttr ".wl[814].w[5]" 6.9707063394345344e-005;
	setAttr ".wl[814].w[9]" 0.00090545168322660012;
	setAttr ".wl[814].w[10]" 0.49945677428296414;
	setAttr ".wl[814].w[11]" 0.49945677428296414;
	setAttr -s 5 ".wl[815].w";
	setAttr ".wl[815].w[4]" 4.732975146924958e-005;
	setAttr ".wl[815].w[5]" 2.7340270280325653e-005;
	setAttr ".wl[815].w[9]" 0.00037692448369484611;
	setAttr ".wl[815].w[10]" 0.4997742027472778;
	setAttr ".wl[815].w[11]" 0.4997742027472778;
	setAttr -s 5 ".wl[816].w";
	setAttr ".wl[816].w[4]" 8.6358748107148958e-005;
	setAttr ".wl[816].w[5]" 4.9569222975121086e-005;
	setAttr ".wl[816].w[9]" 0.00070639894185058625;
	setAttr ".wl[816].w[10]" 0.49957883654353358;
	setAttr ".wl[816].w[11]" 0.49957883654353358;
	setAttr -s 5 ".wl[817].w";
	setAttr ".wl[817].w[4]" 0.00073225289864860916;
	setAttr ".wl[817].w[5]" 0.00039636491408236206;
	setAttr ".wl[817].w[9]" 0.0074339284905488093;
	setAttr ".wl[817].w[10]" 0.51422780558809711;
	setAttr ".wl[817].w[11]" 0.47720964810862315;
	setAttr -s 5 ".wl[818].w";
	setAttr ".wl[818].w[4]" 0.00076243807204468906;
	setAttr ".wl[818].w[5]" 0.00034911933803113759;
	setAttr ".wl[818].w[9]" 0.0076980492447736122;
	setAttr ".wl[818].w[10]" 0.51314099670990698;
	setAttr ".wl[818].w[11]" 0.47804939663524354;
	setAttr -s 5 ".wl[819].w";
	setAttr ".wl[819].w[4]" 0.00055334854522961011;
	setAttr ".wl[819].w[5]" 0.00031467890134840578;
	setAttr ".wl[819].w[9]" 0.0045401642869506561;
	setAttr ".wl[819].w[10]" 0.49729590413323566;
	setAttr ".wl[819].w[11]" 0.49729590413323566;
	setAttr -s 5 ".wl[820].w";
	setAttr ".wl[820].w[4]" 0.00055247743196855067;
	setAttr ".wl[820].w[5]" 0.00028767252079827792;
	setAttr ".wl[820].w[9]" 0.004529996679529605;
	setAttr ".wl[820].w[10]" 0.4973149266838518;
	setAttr ".wl[820].w[11]" 0.4973149266838518;
	setAttr -s 5 ".wl[821].w";
	setAttr ".wl[821].w[4]" 0.0010258161791389957;
	setAttr ".wl[821].w[5]" 0.00054320312863321695;
	setAttr ".wl[821].w[9]" 0.0089162506127944442;
	setAttr ".wl[821].w[10]" 0.49475736503971668;
	setAttr ".wl[821].w[11]" 0.49475736503971668;
	setAttr -s 5 ".wl[822].w";
	setAttr ".wl[822].w[4]" 0.00037841736953991444;
	setAttr ".wl[822].w[5]" 0.00016571681386306751;
	setAttr ".wl[822].w[9]" 0.0031608427263275967;
	setAttr ".wl[822].w[10]" 0.4981475115451347;
	setAttr ".wl[822].w[11]" 0.4981475115451347;
	setAttr -s 5 ".wl[823].w";
	setAttr ".wl[823].w[4]" 0.00076853551725339136;
	setAttr ".wl[823].w[5]" 0.00031082952868354756;
	setAttr ".wl[823].w[9]" 0.0068138609203093354;
	setAttr ".wl[823].w[10]" 0.49605338701687685;
	setAttr ".wl[823].w[11]" 0.49605338701687685;
	setAttr -s 5 ".wl[824].w";
	setAttr ".wl[824].w[4]" 0.00043327789869411254;
	setAttr ".wl[824].w[5]" 0.00019046929721019647;
	setAttr ".wl[824].w[9]" 0.0035947486782444953;
	setAttr ".wl[824].w[10]" 0.4978907520629256;
	setAttr ".wl[824].w[11]" 0.4978907520629256;
	setAttr -s 5 ".wl[825].w";
	setAttr ".wl[825].w[4]" 0.00072980989124031765;
	setAttr ".wl[825].w[5]" 0.00051304707951344019;
	setAttr ".wl[825].w[9]" 0.0065335014435447406;
	setAttr ".wl[825].w[10]" 0.49611182079285077;
	setAttr ".wl[825].w[11]" 0.49611182079285077;
	setAttr -s 5 ".wl[826].w";
	setAttr ".wl[826].w[4]" 0.00035102714476270552;
	setAttr ".wl[826].w[5]" 0.00023720972149189986;
	setAttr ".wl[826].w[9]" 0.0029550396487531415;
	setAttr ".wl[826].w[10]" 0.49822836174249618;
	setAttr ".wl[826].w[11]" 0.49822836174249618;
	setAttr -s 5 ".wl[827].w";
	setAttr ".wl[827].w[4]" 0.00041509405235637625;
	setAttr ".wl[827].w[5]" 0.00028169612588812941;
	setAttr ".wl[827].w[9]" 0.0034654881721757733;
	setAttr ".wl[827].w[10]" 0.49791886082478981;
	setAttr ".wl[827].w[11]" 0.49791886082478981;
	setAttr -s 5 ".wl[828].w";
	setAttr ".wl[828].w[4]" 0.00051291695976044623;
	setAttr ".wl[828].w[5]" 0.00026473248627392862;
	setAttr ".wl[828].w[9]" 0.0046862780058625911;
	setAttr ".wl[828].w[10]" 0.49726803627405153;
	setAttr ".wl[828].w[11]" 0.49726803627405153;
	setAttr -s 5 ".wl[829].w";
	setAttr ".wl[829].w[4]" 0.0002647164476166644;
	setAttr ".wl[829].w[5]" 0.0001351443928602481;
	setAttr ".wl[829].w[9]" 0.0022505075118032917;
	setAttr ".wl[829].w[10]" 0.49867481582385992;
	setAttr ".wl[829].w[11]" 0.49867481582385992;
	setAttr -s 5 ".wl[830].w";
	setAttr ".wl[830].w[4]" 0.00025641252130884903;
	setAttr ".wl[830].w[5]" 0.00014290905340145807;
	setAttr ".wl[830].w[9]" 0.0021847575458083975;
	setAttr ".wl[830].w[10]" 0.49870796043974064;
	setAttr ".wl[830].w[11]" 0.49870796043974064;
	setAttr -s 5 ".wl[831].w";
	setAttr ".wl[831].w[4]" 0.0013954278381870442;
	setAttr ".wl[831].w[5]" 0.00077688225238503757;
	setAttr ".wl[831].w[9]" 0.013343340578885874;
	setAttr ".wl[831].w[10]" 0.50680048694663515;
	setAttr ".wl[831].w[11]" 0.47768386238390698;
	setAttr -s 5 ".wl[832].w";
	setAttr ".wl[832].w[4]" 0.0013891434304433895;
	setAttr ".wl[832].w[5]" 0.00065352367368224371;
	setAttr ".wl[832].w[9]" 0.013266291726015267;
	setAttr ".wl[832].w[10]" 0.50650411236515369;
	setAttr ".wl[832].w[11]" 0.47818692880470537;
	setAttr -s 5 ".wl[833].w";
	setAttr ".wl[833].w[4]" 0.0011583207315144757;
	setAttr ".wl[833].w[5]" 0.00044661362439332643;
	setAttr ".wl[833].w[9]" 0.011242111645970472;
	setAttr ".wl[833].w[10]" 0.5080502589207726;
	setAttr ".wl[833].w[11]" 0.47910269507734915;
	setAttr -s 5 ".wl[834].w";
	setAttr ".wl[834].w[4]" 0.00099088716275737418;
	setAttr ".wl[834].w[5]" 0.00037925584394608001;
	setAttr ".wl[834].w[9]" 0.0097579571236422417;
	setAttr ".wl[834].w[10]" 0.50968727519796819;
	setAttr ".wl[834].w[11]" 0.47918462467168615;
	setAttr -s 5 ".wl[835].w";
	setAttr ".wl[835].w[4]" 0.0007676220570744472;
	setAttr ".wl[835].w[5]" 0.00046607990494742358;
	setAttr ".wl[835].w[9]" 0.0067362508537922808;
	setAttr ".wl[835].w[10]" 0.49601502359209293;
	setAttr ".wl[835].w[11]" 0.49601502359209293;
	setAttr -s 5 ".wl[836].w";
	setAttr ".wl[836].w[4]" 0.00036669352045084396;
	setAttr ".wl[836].w[5]" 0.00023083993803311336;
	setAttr ".wl[836].w[9]" 0.0030132800921032783;
	setAttr ".wl[836].w[10]" 0.49819459322470638;
	setAttr ".wl[836].w[11]" 0.49819459322470638;
	setAttr -s 5 ".wl[837].w";
	setAttr ".wl[837].w[4]" 0.00068156041569596257;
	setAttr ".wl[837].w[5]" 0.00045365524110375964;
	setAttr ".wl[837].w[9]" 0.0060372627332778454;
	setAttr ".wl[837].w[10]" 0.49641376080496125;
	setAttr ".wl[837].w[11]" 0.49641376080496125;
	setAttr -s 5 ".wl[838].w";
	setAttr ".wl[838].w[4]" 0.001226379501303136;
	setAttr ".wl[838].w[5]" 0.0007811604607268353;
	setAttr ".wl[838].w[9]" 0.011863071115781261;
	setAttr ".wl[838].w[10]" 0.50782909080006988;
	setAttr ".wl[838].w[11]" 0.47830029812211894;
	setAttr -s 5 ".wl[839].w";
	setAttr ".wl[839].w[4]" 0.00018998060031812832;
	setAttr ".wl[839].w[5]" 0.00011035288183974474;
	setAttr ".wl[839].w[9]" 0.0015205325466787181;
	setAttr ".wl[839].w[10]" 0.49908956698558171;
	setAttr ".wl[839].w[11]" 0.49908956698558171;
	setAttr -s 5 ".wl[840].w";
	setAttr ".wl[840].w[4]" 6.8511933848748813e-005;
	setAttr ".wl[840].w[5]" 3.9722738957247881e-005;
	setAttr ".wl[840].w[9]" 0.00054200003993163379;
	setAttr ".wl[840].w[10]" 0.49967488264363119;
	setAttr ".wl[840].w[11]" 0.49967488264363119;
	setAttr -s 5 ".wl[841].w";
	setAttr ".wl[841].w[4]" 0.00015161051499510374;
	setAttr ".wl[841].w[5]" 9.5397058041618232e-005;
	setAttr ".wl[841].w[9]" 0.0012229244262754776;
	setAttr ".wl[841].w[10]" 0.49926503400034389;
	setAttr ".wl[841].w[11]" 0.49926503400034389;
	setAttr -s 5 ".wl[842].w";
	setAttr ".wl[842].w[4]" 0.00019114473833992061;
	setAttr ".wl[842].w[5]" 0.00010068956297679636;
	setAttr ".wl[842].w[9]" 0.0015283088465635173;
	setAttr ".wl[842].w[10]" 0.49908992842605987;
	setAttr ".wl[842].w[11]" 0.49908992842605987;
	setAttr -s 5 ".wl[843].w";
	setAttr ".wl[843].w[4]" 0.00037041804143863092;
	setAttr ".wl[843].w[5]" 0.000176465164790655;
	setAttr ".wl[843].w[9]" 0.0030350379981572075;
	setAttr ".wl[843].w[10]" 0.49820903939780675;
	setAttr ".wl[843].w[11]" 0.49820903939780675;
	setAttr -s 5 ".wl[844].w";
	setAttr ".wl[844].w[4]" 0.00015821822418355598;
	setAttr ".wl[844].w[5]" 7.6942610324643793e-005;
	setAttr ".wl[844].w[9]" 0.0012717849009520725;
	setAttr ".wl[844].w[10]" 0.49924652713226991;
	setAttr ".wl[844].w[11]" 0.4992465271322698;
	setAttr -s 5 ".wl[845].w";
	setAttr ".wl[845].w[4]" 7.0007788098171855e-005;
	setAttr ".wl[845].w[5]" 3.7455263515809637e-005;
	setAttr ".wl[845].w[9]" 0.0005532335986002581;
	setAttr ".wl[845].w[10]" 0.49966965167489286;
	setAttr ".wl[845].w[11]" 0.49966965167489286;
	setAttr -s 5 ".wl[846].w";
	setAttr ".wl[846].w[4]" 0.00076490708198482084;
	setAttr ".wl[846].w[5]" 0.00035644274972698498;
	setAttr ".wl[846].w[9]" 0.0066970397944405404;
	setAttr ".wl[846].w[10]" 0.49609080518692383;
	setAttr ".wl[846].w[11]" 0.49609080518692383;
	setAttr -s 5 ".wl[847].w";
	setAttr ".wl[847].w[4]" 0.0012265397328589582;
	setAttr ".wl[847].w[5]" 0.00051083176619651303;
	setAttr ".wl[847].w[9]" 0.011810016881418752;
	setAttr ".wl[847].w[10]" 0.5068632870941765;
	setAttr ".wl[847].w[11]" 0.4795893245253493;
	setAttr -s 5 ".wl[848].w";
	setAttr ".wl[848].w[4]" 0.0006909459794667462;
	setAttr ".wl[848].w[5]" 0.00029602414438114098;
	setAttr ".wl[848].w[9]" 0.0060876710145708682;
	setAttr ".wl[848].w[10]" 0.4964626794307907;
	setAttr ".wl[848].w[11]" 0.49646267943079059;
	setAttr -s 5 ".wl[849].w";
	setAttr ".wl[849].w[4]" 0.00012111980205051043;
	setAttr ".wl[849].w[5]" 5.864751609822104e-005;
	setAttr ".wl[849].w[9]" 0.00098096587373621932;
	setAttr ".wl[849].w[10]" 0.49941963340405754;
	setAttr ".wl[849].w[11]" 0.49941963340405754;
	setAttr -s 5 ".wl[850].w";
	setAttr ".wl[850].w[4]" 0.00021848848240582294;
	setAttr ".wl[850].w[5]" 0.00010281036415844615;
	setAttr ".wl[850].w[9]" 0.0018300358643928223;
	setAttr ".wl[850].w[10]" 0.49892433264452152;
	setAttr ".wl[850].w[11]" 0.49892433264452152;
	setAttr -s 5 ".wl[851].w";
	setAttr ".wl[851].w[4]" 9.0736081193792785e-005;
	setAttr ".wl[851].w[5]" 4.7244740274099599e-005;
	setAttr ".wl[851].w[9]" 0.00074069470073741999;
	setAttr ".wl[851].w[10]" 0.49956066223889734;
	setAttr ".wl[851].w[11]" 0.49956066223889734;
	setAttr -s 5 ".wl[852].w";
	setAttr ".wl[852].w[4]" 4.9275698089866009e-005;
	setAttr ".wl[852].w[5]" 2.6267922932712888e-005;
	setAttr ".wl[852].w[9]" 0.00039189239551447959;
	setAttr ".wl[852].w[10]" 0.49976628199173134;
	setAttr ".wl[852].w[11]" 0.49976628199173156;
	setAttr -s 5 ".wl[853].w";
	setAttr ".wl[853].w[4]" 0.00049111538128464749;
	setAttr ".wl[853].w[5]" 0.00020798262207547945;
	setAttr ".wl[853].w[9]" 0.0044214146089513896;
	setAttr ".wl[853].w[10]" 0.49743974369384419;
	setAttr ".wl[853].w[11]" 0.49743974369384419;
	setAttr -s 5 ".wl[854].w";
	setAttr ".wl[854].w[4]" 0.00071278498121478168;
	setAttr ".wl[854].w[5]" 0.00045535613973486975;
	setAttr ".wl[854].w[9]" 0.006271925549835476;
	setAttr ".wl[854].w[10]" 0.49627996666460744;
	setAttr ".wl[854].w[11]" 0.49627996666460744;
	setAttr -s 5 ".wl[855].w";
	setAttr ".wl[855].w[4]" 0.00015245242952966308;
	setAttr ".wl[855].w[5]" 9.2414231152293866e-005;
	setAttr ".wl[855].w[9]" 0.0012197764577830093;
	setAttr ".wl[855].w[10]" 0.49926767844076758;
	setAttr ".wl[855].w[11]" 0.49926767844076747;
	setAttr -s 5 ".wl[856].w";
	setAttr ".wl[856].w[4]" 0.00015568462835720883;
	setAttr ".wl[856].w[5]" 7.8885110852022726e-005;
	setAttr ".wl[856].w[9]" 0.0012430069515722494;
	setAttr ".wl[856].w[10]" 0.49926121165460924;
	setAttr ".wl[856].w[11]" 0.49926121165460924;
	setAttr -s 5 ".wl[857].w";
	setAttr ".wl[857].w[4]" 0.00071438189343754274;
	setAttr ".wl[857].w[5]" 0.00031820138373037484;
	setAttr ".wl[857].w[9]" 0.0062628525139165692;
	setAttr ".wl[857].w[10]" 0.49635228210445775;
	setAttr ".wl[857].w[11]" 0.49635228210445775;
	setAttr -s 5 ".wl[858].w";
	setAttr ".wl[858].w[4]" 9.1026516624969034e-005;
	setAttr ".wl[858].w[5]" 4.5755530361705181e-005;
	setAttr ".wl[858].w[9]" 0.00073707433794036362;
	setAttr ".wl[858].w[10]" 0.49956307180753656;
	setAttr ".wl[858].w[11]" 0.49956307180753656;
	setAttr -s 5 ".wl[859].w";
	setAttr ".wl[859].w[4]" 0.00044157474281019142;
	setAttr ".wl[859].w[5]" 0.00019357046189607708;
	setAttr ".wl[859].w[9]" 0.0039887671381734031;
	setAttr ".wl[859].w[10]" 0.49768804382856024;
	setAttr ".wl[859].w[11]" 0.49768804382856024;
	setAttr -s 5 ".wl[860].w";
	setAttr ".wl[860].w[4]" 0.00040346282954763739;
	setAttr ".wl[860].w[5]" 0.0002533068492739074;
	setAttr ".wl[860].w[9]" 0.0036776762323744599;
	setAttr ".wl[860].w[10]" 0.49783277704440215;
	setAttr ".wl[860].w[11]" 0.49783277704440193;
	setAttr -s 5 ".wl[861].w";
	setAttr ".wl[861].w[4]" 8.4022786712224773e-005;
	setAttr ".wl[861].w[5]" 5.0509735449240794e-005;
	setAttr ".wl[861].w[9]" 0.00068269817288880194;
	setAttr ".wl[861].w[10]" 0.49959138465247471;
	setAttr ".wl[861].w[11]" 0.49959138465247493;
	setAttr -s 5 ".wl[862].w";
	setAttr ".wl[862].w[4]" 4.87163666727185e-005;
	setAttr ".wl[862].w[5]" 2.715746474966692e-005;
	setAttr ".wl[862].w[9]" 0.00038381257255925254;
	setAttr ".wl[862].w[10]" 0.49977015679800918;
	setAttr ".wl[862].w[11]" 0.49977015679800918;
	setAttr -s 5 ".wl[863].w";
	setAttr ".wl[863].w[4]" 0.00030863930976066355;
	setAttr ".wl[863].w[5]" 0.0001698978509025415;
	setAttr ".wl[863].w[9]" 0.002480610146344704;
	setAttr ".wl[863].w[10]" 0.49852042634649607;
	setAttr ".wl[863].w[11]" 0.49852042634649607;
	setAttr -s 5 ".wl[864].w";
	setAttr ".wl[864].w[4]" 0.00090731019254481845;
	setAttr ".wl[864].w[5]" 0.0005159084475818554;
	setAttr ".wl[864].w[9]" 0.0079262511064726524;
	setAttr ".wl[864].w[10]" 0.49532526512670033;
	setAttr ".wl[864].w[11]" 0.49532526512670033;
	setAttr -s 5 ".wl[865].w";
	setAttr ".wl[865].w[4]" 0.00090359180405978829;
	setAttr ".wl[865].w[5]" 0.00044669566615687445;
	setAttr ".wl[865].w[9]" 0.007885791769812061;
	setAttr ".wl[865].w[10]" 0.49538196037998566;
	setAttr ".wl[865].w[11]" 0.49538196037998566;
	setAttr -s 5 ".wl[866].w";
	setAttr ".wl[866].w[4]" 0.00062431956671665106;
	setAttr ".wl[866].w[5]" 0.00025598863864018903;
	setAttr ".wl[866].w[9]" 0.0055847862871884679;
	setAttr ".wl[866].w[10]" 0.49676745275372736;
	setAttr ".wl[866].w[11]" 0.49676745275372736;
	setAttr -s 5 ".wl[867].w";
	setAttr ".wl[867].w[4]" 0.00022123353522405704;
	setAttr ".wl[867].w[5]" 0.00010244122250708687;
	setAttr ".wl[867].w[9]" 0.0017988171208536941;
	setAttr ".wl[867].w[10]" 0.49893875406070759;
	setAttr ".wl[867].w[11]" 0.49893875406070759;
	setAttr -s 5 ".wl[868].w";
	setAttr ".wl[868].w[4]" 0.00073598903824599322;
	setAttr ".wl[868].w[5]" 0.00030361614495989922;
	setAttr ".wl[868].w[9]" 0.0065103022757798806;
	setAttr ".wl[868].w[10]" 0.49622504627050706;
	setAttr ".wl[868].w[11]" 0.49622504627050706;
	setAttr -s 5 ".wl[869].w";
	setAttr ".wl[869].w[4]" 0.00057935749881842457;
	setAttr ".wl[869].w[5]" 0.00039777588763285855;
	setAttr ".wl[869].w[9]" 0.0052376454763736783;
	setAttr ".wl[869].w[10]" 0.49689261056858758;
	setAttr ".wl[869].w[11]" 0.49689261056858747;
	setAttr -s 5 ".wl[870].w";
	setAttr ".wl[870].w[4]" 0.00071325510184852049;
	setAttr ".wl[870].w[5]" 0.00049307625866799095;
	setAttr ".wl[870].w[9]" 0.0063572818280674083;
	setAttr ".wl[870].w[10]" 0.49621819340570805;
	setAttr ".wl[870].w[11]" 0.49621819340570805;
	setAttr -s 5 ".wl[871].w";
	setAttr ".wl[871].w[4]" 0.00020732679607878145;
	setAttr ".wl[871].w[5]" 0.0001356279898973156;
	setAttr ".wl[871].w[9]" 0.0016949087493202482;
	setAttr ".wl[871].w[10]" 0.49898106823235189;
	setAttr ".wl[871].w[11]" 0.49898106823235178;
	setAttr -s 5 ".wl[872].w";
	setAttr ".wl[872].w[4]" 0.00046321739338448079;
	setAttr ".wl[872].w[5]" 0.00022366915653544321;
	setAttr ".wl[872].w[9]" 0.0042294463903306666;
	setAttr ".wl[872].w[10]" 0.49754183352987458;
	setAttr ".wl[872].w[11]" 0.4975418335298748;
	setAttr -s 5 ".wl[873].w";
	setAttr ".wl[873].w[4]" 0.00044478921621853891;
	setAttr ".wl[873].w[5]" 0.00024683722900404432;
	setAttr ".wl[873].w[9]" 0.0040774200202982926;
	setAttr ".wl[873].w[10]" 0.49761547676723961;
	setAttr ".wl[873].w[11]" 0.4976154767672395;
	setAttr -s 5 ".wl[874].w";
	setAttr ".wl[874].w[4]" 0.00013651172899539527;
	setAttr ".wl[874].w[5]" 7.3967422337434764e-005;
	setAttr ".wl[874].w[9]" 0.0011291919151168485;
	setAttr ".wl[874].w[10]" 0.49933016446677514;
	setAttr ".wl[874].w[11]" 0.49933016446677514;
	setAttr -s 5 ".wl[875].w";
	setAttr ".wl[875].w[3]" 0.19954468775994527;
	setAttr ".wl[875].w[4]" 0.67597299482377138;
	setAttr ".wl[875].w[5]" 0.058767743371685319;
	setAttr ".wl[875].w[9]" 0.052174459799464931;
	setAttr ".wl[875].w[12]" 0.013540114245133138;
	setAttr -s 5 ".wl[876].w";
	setAttr ".wl[876].w[3]" 0.092286877553666991;
	setAttr ".wl[876].w[4]" 0.82958765149876856;
	setAttr ".wl[876].w[5]" 0.048047997277197024;
	setAttr ".wl[876].w[9]" 0.02403321835442827;
	setAttr ".wl[876].w[12]" 0.0060442553159392679;
	setAttr -s 5 ".wl[877].w";
	setAttr ".wl[877].w[3]" 0.032071241409859612;
	setAttr ".wl[877].w[4]" 0.93040803913158598;
	setAttr ".wl[877].w[5]" 0.026773822316789;
	setAttr ".wl[877].w[8]" 0.0025003939516724184;
	setAttr ".wl[877].w[9]" 0.0082465031900929419;
	setAttr -s 5 ".wl[878].w";
	setAttr ".wl[878].w[3]" 0.36888324550694923;
	setAttr ".wl[878].w[4]" 0.49755118469224113;
	setAttr ".wl[878].w[5]" 0.02849588043464266;
	setAttr ".wl[878].w[9]" 0.084324759245974365;
	setAttr ".wl[878].w[12]" 0.020744930120192507;
	setAttr -s 5 ".wl[879].w";
	setAttr ".wl[879].w[3]" 0.31570315336926474;
	setAttr ".wl[879].w[4]" 0.54220767581907714;
	setAttr ".wl[879].w[5]" 0.045212177532974143;
	setAttr ".wl[879].w[9]" 0.077299247744524749;
	setAttr ".wl[879].w[12]" 0.019577745534159186;
	setAttr -s 5 ".wl[880].w";
	setAttr ".wl[880].w[2]" 0.029025051802820199;
	setAttr ".wl[880].w[3]" 0.58932515259552032;
	setAttr ".wl[880].w[4]" 0.32378308687003898;
	setAttr ".wl[880].w[9]" 0.046218302306444886;
	setAttr ".wl[880].w[12]" 0.011648406425175574;
	setAttr -s 5 ".wl[881].w";
	setAttr ".wl[881].w[2]" 0.023050478890903153;
	setAttr ".wl[881].w[3]" 0.45292411068761002;
	setAttr ".wl[881].w[4]" 0.44270137204397064;
	setAttr ".wl[881].w[9]" 0.065146204729680648;
	setAttr ".wl[881].w[12]" 0.016177833647835649;
	setAttr -s 5 ".wl[882].w";
	setAttr ".wl[882].w[2]" 0.016533652938154644;
	setAttr ".wl[882].w[3]" 0.38813724118715948;
	setAttr ".wl[882].w[4]" 0.49996231421064541;
	setAttr ".wl[882].w[9]" 0.076674601381691757;
	setAttr ".wl[882].w[12]" 0.018692190282348715;
	setAttr -s 5 ".wl[883].w";
	setAttr ".wl[883].w[2]" 0.011460945545977453;
	setAttr ".wl[883].w[3]" 0.90414534097473043;
	setAttr ".wl[883].w[4]" 0.07397203758394956;
	setAttr ".wl[883].w[9]" 0.0084533108613615883;
	setAttr ".wl[883].w[12]" 0.0019683650339809705;
	setAttr -s 5 ".wl[884].w";
	setAttr ".wl[884].w[2]" 0.022746257672047437;
	setAttr ".wl[884].w[3]" 0.76806471003840648;
	setAttr ".wl[884].w[4]" 0.18073208824938664;
	setAttr ".wl[884].w[9]" 0.022900859482431171;
	setAttr ".wl[884].w[12]" 0.0055560845577283368;
	setAttr -s 5 ".wl[885].w";
	setAttr ".wl[885].w[3]" 0.020745908529203941;
	setAttr ".wl[885].w[4]" 0.94444752277353672;
	setAttr ".wl[885].w[5]" 0.026213103066108887;
	setAttr ".wl[885].w[8]" 0.0034929260952327196;
	setAttr ".wl[885].w[9]" 0.0051005395359175077;
	setAttr -s 5 ".wl[886].w";
	setAttr ".wl[886].w[3]" 0.016151900409721559;
	setAttr ".wl[886].w[4]" 0.95916145269477249;
	setAttr ".wl[886].w[5]" 0.018698594145482795;
	setAttr ".wl[886].w[8]" 0.0018624425320814823;
	setAttr ".wl[886].w[9]" 0.0041256102179417012;
	setAttr -s 5 ".wl[887].w";
	setAttr ".wl[887].w[3]" 0.37553278000481205;
	setAttr ".wl[887].w[4]" 0.50316044179233299;
	setAttr ".wl[887].w[5]" 0.019243419683009445;
	setAttr ".wl[887].w[9]" 0.082023460836892409;
	setAttr ".wl[887].w[12]" 0.020039897682953105;
	setAttr -s 5 ".wl[888].w";
	setAttr ".wl[888].w[2]" 0.006730999937048769;
	setAttr ".wl[888].w[3]" 0.9477132889538068;
	setAttr ".wl[888].w[4]" 0.040145958112672082;
	setAttr ".wl[888].w[9]" 0.0044005534470648528;
	setAttr ".wl[888].w[12]" 0.0010091995494075549;
	setAttr -s 5 ".wl[889].w";
	setAttr ".wl[889].w[2]" 0.0073757909751242905;
	setAttr ".wl[889].w[3]" 0.93421241917892228;
	setAttr ".wl[889].w[4]" 0.051784442363440132;
	setAttr ".wl[889].w[9]" 0.0053993525638425192;
	setAttr ".wl[889].w[12]" 0.0012279949186707443;
	setAttr -s 5 ".wl[890].w";
	setAttr ".wl[890].w[3]" 0.058856929590757591;
	setAttr ".wl[890].w[4]" 0.83372203824466706;
	setAttr ".wl[890].w[5]" 0.077186801665233354;
	setAttr ".wl[890].w[8]" 0.016327605804003616;
	setAttr ".wl[890].w[9]" 0.013906624695338387;
	setAttr -s 5 ".wl[891].w";
	setAttr ".wl[891].w[3]" 0.13971621942607007;
	setAttr ".wl[891].w[4]" 0.6251660360160276;
	setAttr ".wl[891].w[5]" 0.14697392054871172;
	setAttr ".wl[891].w[8]" 0.056562605979335802;
	setAttr ".wl[891].w[9]" 0.031581218029854793;
	setAttr -s 5 ".wl[892].w";
	setAttr ".wl[892].w[3]" 0.24952263148208964;
	setAttr ".wl[892].w[4]" 0.48866556997593369;
	setAttr ".wl[892].w[5]" 0.12913707309578931;
	setAttr ".wl[892].w[8]" 0.082490051817136409;
	setAttr ".wl[892].w[9]" 0.050184673629050894;
	setAttr -s 5 ".wl[893].w";
	setAttr ".wl[893].w[3]" 0.32429843440166628;
	setAttr ".wl[893].w[4]" 0.47374664344604317;
	setAttr ".wl[893].w[5]" 0.078399256090918135;
	setAttr ".wl[893].w[8]" 0.065134950920682763;
	setAttr ".wl[893].w[9]" 0.058420715140689694;
	setAttr -s 5 ".wl[894].w";
	setAttr ".wl[894].w[3]" 0.37257593112604992;
	setAttr ".wl[894].w[4]" 0.51629475599108643;
	setAttr ".wl[894].w[5]" 0.028281459693433663;
	setAttr ".wl[894].w[8]" 0.026969615679852156;
	setAttr ".wl[894].w[9]" 0.055878237509577836;
	setAttr -s 5 ".wl[895].w";
	setAttr ".wl[895].w[3]" 0.34610247541968953;
	setAttr ".wl[895].w[4]" 0.50386883967561458;
	setAttr ".wl[895].w[5]" 0.047772580904593992;
	setAttr ".wl[895].w[8]" 0.043830485129039386;
	setAttr ".wl[895].w[9]" 0.058425618871062415;
	setAttr -s 5 ".wl[896].w";
	setAttr ".wl[896].w[2]" 0.015944601148314676;
	setAttr ".wl[896].w[3]" 0.46702660253928757;
	setAttr ".wl[896].w[4]" 0.45354369253591942;
	setAttr ".wl[896].w[5]" 0.015002745105683748;
	setAttr ".wl[896].w[9]" 0.048482358670794569;
	setAttr -s 5 ".wl[897].w";
	setAttr ".wl[897].w[2]" 0.020092322474276732;
	setAttr ".wl[897].w[3]" 0.63995648430789953;
	setAttr ".wl[897].w[4]" 0.29923716828031754;
	setAttr ".wl[897].w[9]" 0.032979910214061374;
	setAttr ".wl[897].w[12]" 0.0077341147234448095;
	setAttr -s 5 ".wl[898].w";
	setAttr ".wl[898].w[2]" 0.014290891685231989;
	setAttr ".wl[898].w[3]" 0.83088971376236465;
	setAttr ".wl[898].w[4]" 0.13710407848876938;
	setAttr ".wl[898].w[9]" 0.014392853285996781;
	setAttr ".wl[898].w[12]" 0.0033224627776371469;
	setAttr -s 5 ".wl[899].w";
	setAttr ".wl[899].w[3]" 0.090681799521092413;
	setAttr ".wl[899].w[4]" 0.48822271547385004;
	setAttr ".wl[899].w[5]" 0.047704447409256417;
	setAttr ".wl[899].w[6]" 0.014361165771966999;
	setAttr ".wl[899].w[9]" 0.35902987182383417;
	setAttr -s 5 ".wl[900].w";
	setAttr ".wl[900].w[3]" 0.081913194670493655;
	setAttr ".wl[900].w[4]" 0.47786264499458792;
	setAttr ".wl[900].w[5]" 0.069797134965477792;
	setAttr ".wl[900].w[6]" 0.019742865573124074;
	setAttr ".wl[900].w[9]" 0.35068415979631651;
	setAttr -s 5 ".wl[901].w";
	setAttr ".wl[901].w[3]" 0.073095247549857648;
	setAttr ".wl[901].w[4]" 0.46237055986647196;
	setAttr ".wl[901].w[5]" 0.098823106168355282;
	setAttr ".wl[901].w[6]" 0.028015194573122914;
	setAttr ".wl[901].w[9]" 0.33769589184219218;
	setAttr -s 5 ".wl[902].w";
	setAttr ".wl[902].w[3]" 0.067384799677218429;
	setAttr ".wl[902].w[4]" 0.44004212166573603;
	setAttr ".wl[902].w[5]" 0.13253718823552135;
	setAttr ".wl[902].w[6]" 0.03948828779579111;
	setAttr ".wl[902].w[9]" 0.32054760262573312;
	setAttr -s 5 ".wl[903].w";
	setAttr ".wl[903].w[3]" 0.062206110571109134;
	setAttr ".wl[903].w[4]" 0.42195546479912344;
	setAttr ".wl[903].w[5]" 0.15896312076553215;
	setAttr ".wl[903].w[6]" 0.053916335507964701;
	setAttr ".wl[903].w[9]" 0.30295896835627062;
	setAttr -s 5 ".wl[904].w";
	setAttr ".wl[904].w[3]" 0.059142017389377834;
	setAttr ".wl[904].w[4]" 0.39935337509296692;
	setAttr ".wl[904].w[5]" 0.18046580475129262;
	setAttr ".wl[904].w[6]" 0.078551149418656707;
	setAttr ".wl[904].w[9]" 0.28248765334770592;
	setAttr -s 5 ".wl[905].w";
	setAttr ".wl[905].w[3]" 0.060378580895149296;
	setAttr ".wl[905].w[4]" 0.40061198284058797;
	setAttr ".wl[905].w[5]" 0.16210702086146556;
	setAttr ".wl[905].w[6]" 0.097896157068155062;
	setAttr ".wl[905].w[9]" 0.27900625833464215;
	setAttr -s 5 ".wl[906].w";
	setAttr ".wl[906].w[3]" 0.063503214758396737;
	setAttr ".wl[906].w[4]" 0.46036440736389306;
	setAttr ".wl[906].w[5]" 0.094685639259873403;
	setAttr ".wl[906].w[6]" 0.074147233361107817;
	setAttr ".wl[906].w[9]" 0.30729950525672911;
	setAttr -s 5 ".wl[907].w";
	setAttr ".wl[907].w[3]" 0.065644333189266185;
	setAttr ".wl[907].w[4]" 0.5109158108605858;
	setAttr ".wl[907].w[5]" 0.05058849786930756;
	setAttr ".wl[907].w[6]" 0.045039475045160503;
	setAttr ".wl[907].w[9]" 0.32781188303568004;
	setAttr -s 5 ".wl[908].w";
	setAttr ".wl[908].w[3]" 0.068662066229002644;
	setAttr ".wl[908].w[4]" 0.53427617285545825;
	setAttr ".wl[908].w[5]" 0.03185027682635725;
	setAttr ".wl[908].w[6]" 0.029771343674099306;
	setAttr ".wl[908].w[9]" 0.33544014041508269;
	setAttr -s 5 ".wl[909].w";
	setAttr ".wl[909].w[3]" 0.072500802729908859;
	setAttr ".wl[909].w[4]" 0.55121754138636836;
	setAttr ".wl[909].w[5]" 0.020260214355047781;
	setAttr ".wl[909].w[6]" 0.019283839313190897;
	setAttr ".wl[909].w[9]" 0.33673760221548421;
	setAttr -s 5 ".wl[910].w";
	setAttr ".wl[910].w[3]" 0.089587584584382068;
	setAttr ".wl[910].w[4]" 0.55173532979401352;
	setAttr ".wl[910].w[5]" 0.013316886329823132;
	setAttr ".wl[910].w[6]" 0.012743129734948644;
	setAttr ".wl[910].w[9]" 0.33261706955683268;
	setAttr -s 5 ".wl[911].w";
	setAttr ".wl[911].w[2]" 0.020243994058140209;
	setAttr ".wl[911].w[3]" 0.1216993150145583;
	setAttr ".wl[911].w[4]" 0.52900210471227882;
	setAttr ".wl[911].w[5]" 0.0093577663590306455;
	setAttr ".wl[911].w[9]" 0.31969681985599202;
	setAttr -s 5 ".wl[912].w";
	setAttr ".wl[912].w[2]" 0.026695437399426342;
	setAttr ".wl[912].w[3]" 0.15014773487957039;
	setAttr ".wl[912].w[4]" 0.51162476195209639;
	setAttr ".wl[912].w[9]" 0.3046765467885994;
	setAttr ".wl[912].w[10]" 0.0068555189803074894;
	setAttr -s 5 ".wl[913].w";
	setAttr ".wl[913].w[2]" 0.031707811011734276;
	setAttr ".wl[913].w[3]" 0.17274703537210559;
	setAttr ".wl[913].w[4]" 0.4941792701449933;
	setAttr ".wl[913].w[9]" 0.29469187165669325;
	setAttr ".wl[913].w[10]" 0.0066740118144734606;
	setAttr -s 5 ".wl[914].w";
	setAttr ".wl[914].w[2]" 0.035055342442603027;
	setAttr ".wl[914].w[3]" 0.18478641819869801;
	setAttr ".wl[914].w[4]" 0.48135745527807905;
	setAttr ".wl[914].w[9]" 0.29185565157162136;
	setAttr ".wl[914].w[10]" 0.0069451325089985921;
	setAttr -s 5 ".wl[915].w";
	setAttr ".wl[915].w[2]" 0.034112102267557169;
	setAttr ".wl[915].w[3]" 0.17798306304156375;
	setAttr ".wl[915].w[4]" 0.48380792951468488;
	setAttr ".wl[915].w[9]" 0.29678782736602416;
	setAttr ".wl[915].w[10]" 0.0073090778101699873;
	setAttr -s 5 ".wl[916].w";
	setAttr ".wl[916].w[2]" 0.031304527380920266;
	setAttr ".wl[916].w[3]" 0.16172536716582303;
	setAttr ".wl[916].w[4]" 0.48966495226273182;
	setAttr ".wl[916].w[9]" 0.3090089961233059;
	setAttr ".wl[916].w[10]" 0.00829615706721905;
	setAttr -s 5 ".wl[917].w";
	setAttr ".wl[917].w[2]" 0.025815739960278174;
	setAttr ".wl[917].w[3]" 0.13882096072449257;
	setAttr ".wl[917].w[4]" 0.49929649037744489;
	setAttr ".wl[917].w[9]" 0.3265001252161423;
	setAttr ".wl[917].w[10]" 0.0095666837216421587;
	setAttr -s 5 ".wl[918].w";
	setAttr ".wl[918].w[2]" 0.017247744497134867;
	setAttr ".wl[918].w[3]" 0.11045554969786145;
	setAttr ".wl[918].w[4]" 0.51813265308825862;
	setAttr ".wl[918].w[9]" 0.3444001953090769;
	setAttr ".wl[918].w[10]" 0.0097638574076682524;
	setAttr -s 5 ".wl[919].w";
	setAttr ".wl[919].w[2]" 0.011784456477312433;
	setAttr ".wl[919].w[3]" 0.095688244437276457;
	setAttr ".wl[919].w[4]" 0.52548259553176302;
	setAttr ".wl[919].w[5]" 0.010266116472015098;
	setAttr ".wl[919].w[9]" 0.3567785870816329;
	setAttr -s 5 ".wl[920].w";
	setAttr ".wl[920].w[3]" 0.093300150583719807;
	setAttr ".wl[920].w[4]" 0.52110155109331424;
	setAttr ".wl[920].w[5]" 0.013449041272923824;
	setAttr ".wl[920].w[9]" 0.36209450966923368;
	setAttr ".wl[920].w[10]" 0.010054747380808486;
	setAttr -s 5 ".wl[921].w";
	setAttr ".wl[921].w[3]" 0.091388596716565726;
	setAttr ".wl[921].w[4]" 0.51651192237900689;
	setAttr ".wl[921].w[5]" 0.018045215204826878;
	setAttr ".wl[921].w[9]" 0.36402318733524935;
	setAttr ".wl[921].w[10]" 0.010031078364351201;
	setAttr -s 5 ".wl[922].w";
	setAttr ".wl[922].w[3]" 0.091669761511302517;
	setAttr ".wl[922].w[4]" 0.50548340507988765;
	setAttr ".wl[922].w[5]" 0.028476708163232195;
	setAttr ".wl[922].w[9]" 0.36400928376449876;
	setAttr ".wl[922].w[10]" 0.010360841481078671;
	setAttr -s 5 ".wl[923].w";
	setAttr ".wl[923].w[3]" 0.024264882068302532;
	setAttr ".wl[923].w[4]" 0.39715935938649338;
	setAttr ".wl[923].w[5]" 0.097174317161705034;
	setAttr ".wl[923].w[6]" 0.084242081997005766;
	setAttr ".wl[923].w[9]" 0.39715935938649338;
	setAttr -s 5 ".wl[924].w";
	setAttr ".wl[924].w[3]" 0.02423288429969726;
	setAttr ".wl[924].w[4]" 0.40612323677539292;
	setAttr ".wl[924].w[5]" 0.094604577243044732;
	setAttr ".wl[924].w[6]" 0.068916064906471813;
	setAttr ".wl[924].w[9]" 0.40612323677539314;
	setAttr -s 5 ".wl[925].w";
	setAttr ".wl[925].w[3]" 0.02527537847030592;
	setAttr ".wl[925].w[4]" 0.4252409548869181;
	setAttr ".wl[925].w[5]" 0.077038538931231859;
	setAttr ".wl[925].w[6]" 0.047204172824626078;
	setAttr ".wl[925].w[9]" 0.4252409548869181;
	setAttr -s 5 ".wl[926].w";
	setAttr ".wl[926].w[3]" 0.027311281497889937;
	setAttr ".wl[926].w[4]" 0.43740105433854559;
	setAttr ".wl[926].w[5]" 0.06337117815378851;
	setAttr ".wl[926].w[6]" 0.034515431671230383;
	setAttr ".wl[926].w[9]" 0.43740105433854559;
	setAttr -s 5 ".wl[927].w";
	setAttr ".wl[927].w[3]" 0.029191927637414808;
	setAttr ".wl[927].w[4]" 0.4491601267323368;
	setAttr ".wl[927].w[5]" 0.048212560196501941;
	setAttr ".wl[927].w[6]" 0.024275258701409688;
	setAttr ".wl[927].w[9]" 0.4491601267323368;
	setAttr -s 5 ".wl[928].w";
	setAttr ".wl[928].w[3]" 0.032882642897660644;
	setAttr ".wl[928].w[4]" 0.45693616995044256;
	setAttr ".wl[928].w[5]" 0.036071480065442779;
	setAttr ".wl[928].w[6]" 0.0171735371360115;
	setAttr ".wl[928].w[9]" 0.45693616995044256;
	setAttr -s 5 ".wl[929].w";
	setAttr ".wl[929].w[3]" 0.036676980368843461;
	setAttr ".wl[929].w[4]" 0.45942628782818568;
	setAttr ".wl[929].w[5]" 0.026486585436550664;
	setAttr ".wl[929].w[9]" 0.45942628782818556;
	setAttr ".wl[929].w[10]" 0.017983858538234688;
	setAttr -s 5 ".wl[930].w";
	setAttr ".wl[930].w[3]" 0.036192281326911699;
	setAttr ".wl[930].w[4]" 0.46466214840996845;
	setAttr ".wl[930].w[5]" 0.016943247429639288;
	setAttr ".wl[930].w[9]" 0.46466214840996845;
	setAttr ".wl[930].w[10]" 0.017540174423512207;
	setAttr -s 5 ".wl[931].w";
	setAttr ".wl[931].w[3]" 0.03561340338215175;
	setAttr ".wl[931].w[4]" 0.46789255412856373;
	setAttr ".wl[931].w[5]" 0.011488679662434571;
	setAttr ".wl[931].w[9]" 0.46789255412856373;
	setAttr ".wl[931].w[10]" 0.017112808698286148;
	setAttr -s 5 ".wl[932].w";
	setAttr ".wl[932].w[3]" 0.036418961423469502;
	setAttr ".wl[932].w[4]" 0.46863115038605047;
	setAttr ".wl[932].w[5]" 0.0090370155781066986;
	setAttr ".wl[932].w[9]" 0.46859232285423336;
	setAttr ".wl[932].w[10]" 0.017320549758140034;
	setAttr -s 5 ".wl[933].w";
	setAttr ".wl[933].w[2]" 0.0075718150111534106;
	setAttr ".wl[933].w[3]" 0.037066300300491378;
	setAttr ".wl[933].w[4]" 0.46928576719109122;
	setAttr ".wl[933].w[9]" 0.4689855601172348;
	setAttr ".wl[933].w[10]" 0.017090557380029105;
	setAttr -s 5 ".wl[934].w";
	setAttr ".wl[934].w[2]" 0.010667334442856323;
	setAttr ".wl[934].w[3]" 0.041831443799428845;
	setAttr ".wl[934].w[4]" 0.46557097765540612;
	setAttr ".wl[934].w[9]" 0.46452650949871549;
	setAttr ".wl[934].w[10]" 0.017403734603593207;
	setAttr -s 5 ".wl[935].w";
	setAttr ".wl[935].w[2]" 0.015311026303656987;
	setAttr ".wl[935].w[3]" 0.050059628716184107;
	setAttr ".wl[935].w[4]" 0.45944230461281094;
	setAttr ".wl[935].w[9]" 0.45746830024661195;
	setAttr ".wl[935].w[10]" 0.017718740120736008;
	setAttr -s 5 ".wl[936].w";
	setAttr ".wl[936].w[2]" 0.017619682542390297;
	setAttr ".wl[936].w[3]" 0.05276244387429678;
	setAttr ".wl[936].w[4]" 0.45805369392050438;
	setAttr ".wl[936].w[9]" 0.45557296239211725;
	setAttr ".wl[936].w[10]" 0.015991217270691339;
	setAttr -s 5 ".wl[937].w";
	setAttr ".wl[937].w[2]" 0.018462275908912223;
	setAttr ".wl[937].w[3]" 0.053170397952490497;
	setAttr ".wl[937].w[4]" 0.45821452086699865;
	setAttr ".wl[937].w[9]" 0.45561510866025884;
	setAttr ".wl[937].w[10]" 0.014537696611339872;
	setAttr -s 5 ".wl[938].w";
	setAttr ".wl[938].w[2]" 0.018694616263797547;
	setAttr ".wl[938].w[3]" 0.053229510292992104;
	setAttr ".wl[938].w[4]" 0.45836834462555387;
	setAttr ".wl[938].w[9]" 0.45571705776413934;
	setAttr ".wl[938].w[10]" 0.013990471053517084;
	setAttr -s 5 ".wl[939].w";
	setAttr ".wl[939].w[2]" 0.016858240284731554;
	setAttr ".wl[939].w[3]" 0.049377074737356696;
	setAttr ".wl[939].w[4]" 0.46165469451407737;
	setAttr ".wl[939].w[9]" 0.45887132395007141;
	setAttr ".wl[939].w[10]" 0.013238666513762952;
	setAttr -s 5 ".wl[940].w";
	setAttr ".wl[940].w[2]" 0.014500960373659742;
	setAttr ".wl[940].w[3]" 0.045008924067932145;
	setAttr ".wl[940].w[4]" 0.46511203413065233;
	setAttr ".wl[940].w[9]" 0.46224760231803103;
	setAttr ".wl[940].w[10]" 0.013130479109724601;
	setAttr -s 5 ".wl[941].w";
	setAttr ".wl[941].w[2]" 0.011484348380105095;
	setAttr ".wl[941].w[3]" 0.039561399367875887;
	setAttr ".wl[941].w[4]" 0.46901186683450741;
	setAttr ".wl[941].w[9]" 0.46658762577512264;
	setAttr ".wl[941].w[10]" 0.013354759642388987;
	setAttr -s 5 ".wl[942].w";
	setAttr ".wl[942].w[3]" 0.030294051387977243;
	setAttr ".wl[942].w[4]" 0.47395230800203647;
	setAttr ".wl[942].w[5]" 0.011121143460038729;
	setAttr ".wl[942].w[9]" 0.47260778213197246;
	setAttr ".wl[942].w[10]" 0.012024715017975167;
	setAttr -s 5 ".wl[943].w";
	setAttr ".wl[943].w[3]" 0.025048094722773816;
	setAttr ".wl[943].w[4]" 0.47148310960670797;
	setAttr ".wl[943].w[5]" 0.01619512424872023;
	setAttr ".wl[943].w[6]" 0.016184123432392023;
	setAttr ".wl[943].w[9]" 0.47108954798940594;
	setAttr -s 5 ".wl[944].w";
	setAttr ".wl[944].w[3]" 0.023977154115575355;
	setAttr ".wl[944].w[4]" 0.46347702735666113;
	setAttr ".wl[944].w[5]" 0.02457934112513991;
	setAttr ".wl[944].w[6]" 0.024539863436276878;
	setAttr ".wl[944].w[9]" 0.46342661396634666;
	setAttr -s 5 ".wl[945].w";
	setAttr ".wl[945].w[3]" 0.023122515243249302;
	setAttr ".wl[945].w[4]" 0.4516550445150041;
	setAttr ".wl[945].w[5]" 0.03694271647336362;
	setAttr ".wl[945].w[6]" 0.03662467925337911;
	setAttr ".wl[945].w[9]" 0.45165504451500388;
	setAttr -s 5 ".wl[946].w";
	setAttr ".wl[946].w[3]" 0.023499993459950436;
	setAttr ".wl[946].w[4]" 0.42597426825047657;
	setAttr ".wl[946].w[5]" 0.063591252406820745;
	setAttr ".wl[946].w[6]" 0.060960217632275722;
	setAttr ".wl[946].w[9]" 0.42597426825047657;
	setAttr -s 5 ".wl[947].w";
	setAttr ".wl[947].w[4]" 0.3406461570596489;
	setAttr ".wl[947].w[5]" 0.068672855449109171;
	setAttr ".wl[947].w[6]" 0.068243441185306591;
	setAttr ".wl[947].w[9]" 0.49150431711511172;
	setAttr ".wl[947].w[10]" 0.030933229190823482;
	setAttr -s 5 ".wl[948].w";
	setAttr ".wl[948].w[4]" 0.34659368699890813;
	setAttr ".wl[948].w[5]" 0.061831557228001412;
	setAttr ".wl[948].w[6]" 0.058111394823890439;
	setAttr ".wl[948].w[9]" 0.50188899006296384;
	setAttr ".wl[948].w[10]" 0.0315743708862362;
	setAttr -s 5 ".wl[949].w";
	setAttr ".wl[949].w[4]" 0.356907345586086;
	setAttr ".wl[949].w[5]" 0.049242738544142559;
	setAttr ".wl[949].w[6]" 0.042189997683157607;
	setAttr ".wl[949].w[9]" 0.51844474476340985;
	setAttr ".wl[949].w[10]" 0.033215173423204125;
	setAttr -s 5 ".wl[950].w";
	setAttr ".wl[950].w[4]" 0.36562613012891793;
	setAttr ".wl[950].w[5]" 0.040837253082892669;
	setAttr ".wl[950].w[6]" 0.032187962931957394;
	setAttr ".wl[950].w[9]" 0.52574632206512795;
	setAttr ".wl[950].w[10]" 0.035602331791104107;
	setAttr -s 5 ".wl[951].w";
	setAttr ".wl[951].w[4]" 0.37420437708031057;
	setAttr ".wl[951].w[5]" 0.032031695421416456;
	setAttr ".wl[951].w[6]" 0.023473726951519374;
	setAttr ".wl[951].w[9]" 0.53302155394175232;
	setAttr ".wl[951].w[10]" 0.037268646605001267;
	setAttr -s 5 ".wl[952].w";
	setAttr ".wl[952].w[3]" 0.018859284465312976;
	setAttr ".wl[952].w[4]" 0.38303386280340385;
	setAttr ".wl[952].w[5]" 0.024944495665650327;
	setAttr ".wl[952].w[9]" 0.53285968155746199;
	setAttr ".wl[952].w[10]" 0.040302675508170877;
	setAttr -s 5 ".wl[953].w";
	setAttr ".wl[953].w[3]" 0.020815825321027107;
	setAttr ".wl[953].w[4]" 0.38772128281572865;
	setAttr ".wl[953].w[5]" 0.019229368043398191;
	setAttr ".wl[953].w[9]" 0.52887263479602975;
	setAttr ".wl[953].w[10]" 0.043360889023816317;
	setAttr -s 5 ".wl[954].w";
	setAttr ".wl[954].w[3]" 0.020333824024178465;
	setAttr ".wl[954].w[4]" 0.38760243329929878;
	setAttr ".wl[954].w[5]" 0.013130695178391426;
	setAttr ".wl[954].w[9]" 0.53638882400115684;
	setAttr ".wl[954].w[10]" 0.042544223496974384;
	setAttr -s 5 ".wl[955].w";
	setAttr ".wl[955].w[3]" 0.019844031849723696;
	setAttr ".wl[955].w[4]" 0.38585146867044312;
	setAttr ".wl[955].w[5]" 0.0094283433447596762;
	setAttr ".wl[955].w[9]" 0.54302966960897592;
	setAttr ".wl[955].w[10]" 0.041846486526097536;
	setAttr -s 5 ".wl[956].w";
	setAttr ".wl[956].w[3]" 0.020149010386097049;
	setAttr ".wl[956].w[4]" 0.38544954635452577;
	setAttr ".wl[956].w[5]" 0.0076973735631096045;
	setAttr ".wl[956].w[9]" 0.54442185839026513;
	setAttr ".wl[956].w[10]" 0.042282211306002439;
	setAttr -s 5 ".wl[957].w";
	setAttr ".wl[957].w[3]" 0.020266473152741323;
	setAttr ".wl[957].w[4]" 0.38534378047150986;
	setAttr ".wl[957].w[5]" 0.0063799334293742236;
	setAttr ".wl[957].w[9]" 0.54623930122487085;
	setAttr ".wl[957].w[10]" 0.041770511721503797;
	setAttr -s 5 ".wl[958].w";
	setAttr ".wl[958].w[2]" 0.0079893752510518355;
	setAttr ".wl[958].w[3]" 0.022172132332809603;
	setAttr ".wl[958].w[4]" 0.38569209446711877;
	setAttr ".wl[958].w[9]" 0.54185171992664494;
	setAttr ".wl[958].w[10]" 0.042294678022374903;
	setAttr -s 5 ".wl[959].w";
	setAttr ".wl[959].w[2]" 0.010717233814272019;
	setAttr ".wl[959].w[3]" 0.025280671163495024;
	setAttr ".wl[959].w[4]" 0.38473232528791496;
	setAttr ".wl[959].w[9]" 0.5362892806709344;
	setAttr ".wl[959].w[10]" 0.042980489063383504;
	setAttr -s 5 ".wl[960].w";
	setAttr ".wl[960].w[2]" 0.011721626559642855;
	setAttr ".wl[960].w[3]" 0.025284809876156281;
	setAttr ".wl[960].w[4]" 0.37966407415244419;
	setAttr ".wl[960].w[9]" 0.54348685694604137;
	setAttr ".wl[960].w[10]" 0.039842632465715282;
	setAttr -s 5 ".wl[961].w";
	setAttr ".wl[961].w[2]" 0.011890885500429068;
	setAttr ".wl[961].w[3]" 0.02452324818961139;
	setAttr ".wl[961].w[4]" 0.37448739634809153;
	setAttr ".wl[961].w[9]" 0.55196246960334183;
	setAttr ".wl[961].w[10]" 0.037136000358526128;
	setAttr -s 5 ".wl[962].w";
	setAttr ".wl[962].w[2]" 0.011896244931959869;
	setAttr ".wl[962].w[3]" 0.024184913516208655;
	setAttr ".wl[962].w[4]" 0.37244604117768576;
	setAttr ".wl[962].w[9]" 0.55538989100123659;
	setAttr ".wl[962].w[10]" 0.036082909372909221;
	setAttr -s 5 ".wl[963].w";
	setAttr ".wl[963].w[2]" 0.010862708630123952;
	setAttr ".wl[963].w[3]" 0.022576549311011232;
	setAttr ".wl[963].w[4]" 0.37181161832433068;
	setAttr ".wl[963].w[9]" 0.56035918861597378;
	setAttr ".wl[963].w[10]" 0.034389935118560436;
	setAttr -s 5 ".wl[964].w";
	setAttr ".wl[964].w[2]" 0.0096710278089179973;
	setAttr ".wl[964].w[3]" 0.021222038618134466;
	setAttr ".wl[964].w[4]" 0.37447079348606577;
	setAttr ".wl[964].w[9]" 0.56073022320016541;
	setAttr ".wl[964].w[10]" 0.03390591688671641;
	setAttr -s 5 ".wl[965].w";
	setAttr ".wl[965].w[3]" 0.019609385773963591;
	setAttr ".wl[965].w[4]" 0.37775048798453564;
	setAttr ".wl[965].w[5]" 0.0084351136037455598;
	setAttr ".wl[965].w[9]" 0.56007615468988159;
	setAttr ".wl[965].w[10]" 0.034128857947873618;
	setAttr -s 5 ".wl[966].w";
	setAttr ".wl[966].w[3]" 0.015833322324606224;
	setAttr ".wl[966].w[4]" 0.37309431580119229;
	setAttr ".wl[966].w[5]" 0.01115093506075214;
	setAttr ".wl[966].w[9]" 0.56866947477767604;
	setAttr ".wl[966].w[10]" 0.031251952035773349;
	setAttr -s 5 ".wl[967].w";
	setAttr ".wl[967].w[4]" 0.36689883399319956;
	setAttr ".wl[967].w[5]" 0.015756449944962243;
	setAttr ".wl[967].w[6]" 0.015756449944962243;
	setAttr ".wl[967].w[9]" 0.57224409903273887;
	setAttr ".wl[967].w[10]" 0.029344167084137129;
	setAttr -s 5 ".wl[968].w";
	setAttr ".wl[968].w[4]" 0.36194141114854006;
	setAttr ".wl[968].w[5]" 0.022872925330984423;
	setAttr ".wl[968].w[6]" 0.022872925330984423;
	setAttr ".wl[968].w[9]" 0.56319552900340719;
	setAttr ".wl[968].w[10]" 0.029117209186083973;
	setAttr -s 5 ".wl[969].w";
	setAttr ".wl[969].w[4]" 0.35629736934338496;
	setAttr ".wl[969].w[5]" 0.032492375314031069;
	setAttr ".wl[969].w[6]" 0.032492375314031069;
	setAttr ".wl[969].w[9]" 0.55015230228178835;
	setAttr ".wl[969].w[10]" 0.028565577746764562;
	setAttr -s 5 ".wl[970].w";
	setAttr ".wl[970].w[4]" 0.34853459610001214;
	setAttr ".wl[970].w[5]" 0.050755205667156918;
	setAttr ".wl[970].w[6]" 0.050755205667156918;
	setAttr ".wl[970].w[9]" 0.52062791816119713;
	setAttr ".wl[970].w[10]" 0.02932707440447695;
	setAttr -s 5 ".wl[971].w";
	setAttr ".wl[971].w[4]" 0.18523599039228431;
	setAttr ".wl[971].w[5]" 0.047555269788354809;
	setAttr ".wl[971].w[6]" 0.047555269788354809;
	setAttr ".wl[971].w[9]" 0.62513999176371304;
	setAttr ".wl[971].w[10]" 0.094513478267293061;
	setAttr -s 5 ".wl[972].w";
	setAttr ".wl[972].w[4]" 0.18675578780808491;
	setAttr ".wl[972].w[5]" 0.042086569857411299;
	setAttr ".wl[972].w[6]" 0.042085931764814274;
	setAttr ".wl[972].w[9]" 0.63310969197406486;
	setAttr ".wl[972].w[10]" 0.095962018595624754;
	setAttr -s 5 ".wl[973].w";
	setAttr ".wl[973].w[4]" 0.19007539421665282;
	setAttr ".wl[973].w[5]" 0.033476670428627051;
	setAttr ".wl[973].w[6]" 0.032772468886338023;
	setAttr ".wl[973].w[9]" 0.64427163094922713;
	setAttr ".wl[973].w[10]" 0.099403835519154965;
	setAttr -s 5 ".wl[974].w";
	setAttr ".wl[974].w[4]" 0.19607832265496014;
	setAttr ".wl[974].w[5]" 0.028168476532282691;
	setAttr ".wl[974].w[6]" 0.026512223013051506;
	setAttr ".wl[974].w[9]" 0.64507796549475471;
	setAttr ".wl[974].w[10]" 0.10416301230495097;
	setAttr -s 5 ".wl[975].w";
	setAttr ".wl[975].w[4]" 0.20150071469568923;
	setAttr ".wl[975].w[5]" 0.02275480054012179;
	setAttr ".wl[975].w[6]" 0.020448456904722193;
	setAttr ".wl[975].w[9]" 0.64804641457229883;
	setAttr ".wl[975].w[10]" 0.10724961328716796;
	setAttr -s 5 ".wl[976].w";
	setAttr ".wl[976].w[4]" 0.21123849554271978;
	setAttr ".wl[976].w[5]" 0.018423605983728948;
	setAttr ".wl[976].w[6]" 0.015677841730643603;
	setAttr ".wl[976].w[9]" 0.64188900361599877;
	setAttr ".wl[976].w[10]" 0.11277105312690891;
	setAttr -s 5 ".wl[977].w";
	setAttr ".wl[977].w[3]" 0.013359669078372327;
	setAttr ".wl[977].w[4]" 0.2194634992868337;
	setAttr ".wl[977].w[5]" 0.014856948385353555;
	setAttr ".wl[977].w[9]" 0.63388978996102519;
	setAttr ".wl[977].w[10]" 0.11843009328841529;
	setAttr -s 5 ".wl[978].w";
	setAttr ".wl[978].w[3]" 0.01294345935856772;
	setAttr ".wl[978].w[4]" 0.21548777092111354;
	setAttr ".wl[978].w[5]" 0.01067786685513821;
	setAttr ".wl[978].w[9]" 0.64373383038801146;
	setAttr ".wl[978].w[10]" 0.11715707247716918;
	setAttr -s 5 ".wl[979].w";
	setAttr ".wl[979].w[3]" 0.012523835433856151;
	setAttr ".wl[979].w[4]" 0.21060756280015691;
	setAttr ".wl[979].w[5]" 0.0080092690356643754;
	setAttr ".wl[979].w[9]" 0.65270143145228343;
	setAttr ".wl[979].w[10]" 0.11615790127803911;
	setAttr -s 5 ".wl[980].w";
	setAttr ".wl[980].w[3]" 0.012623174553350424;
	setAttr ".wl[980].w[4]" 0.20976669343794657;
	setAttr ".wl[980].w[5]" 0.0067326615908772343;
	setAttr ".wl[980].w[9]" 0.65386706853169385;
	setAttr ".wl[980].w[10]" 0.11701040188613185;
	setAttr -s 5 ".wl[981].w";
	setAttr ".wl[981].w[3]" 0.012589030749394122;
	setAttr ".wl[981].w[4]" 0.2084783058251822;
	setAttr ".wl[981].w[5]" 0.0057382299592469354;
	setAttr ".wl[981].w[9]" 0.65728374405843393;
	setAttr ".wl[981].w[10]" 0.1159106894077428;
	setAttr -s 5 ".wl[982].w";
	setAttr ".wl[982].w[2]" 0.0061874831889180224;
	setAttr ".wl[982].w[3]" 0.013495999563149591;
	setAttr ".wl[982].w[4]" 0.21080810259715693;
	setAttr ".wl[982].w[9]" 0.6529151870617862;
	setAttr ".wl[982].w[10]" 0.11659322758898931;
	setAttr -s 5 ".wl[983].w";
	setAttr ".wl[983].w[2]" 0.0078445417032093432;
	setAttr ".wl[983].w[3]" 0.014904252445904567;
	setAttr ".wl[983].w[4]" 0.21261279063810487;
	setAttr ".wl[983].w[9]" 0.64697880277752584;
	setAttr ".wl[983].w[10]" 0.11765961243525544;
	setAttr -s 5 ".wl[984].w";
	setAttr ".wl[984].w[2]" 0.0082286757658335437;
	setAttr ".wl[984].w[3]" 0.014446754743913019;
	setAttr ".wl[984].w[4]" 0.20340517788491252;
	setAttr ".wl[984].w[9]" 0.6618782771944115;
	setAttr ".wl[984].w[10]" 0.11204111441092941;
	setAttr -s 5 ".wl[985].w";
	setAttr ".wl[985].w[2]" 0.0081253606094672578;
	setAttr ".wl[985].w[3]" 0.013697973130396951;
	setAttr ".wl[985].w[4]" 0.19410570697916626;
	setAttr ".wl[985].w[9]" 0.67704980867146158;
	setAttr ".wl[985].w[10]" 0.10702115060950797;
	setAttr -s 5 ".wl[986].w";
	setAttr ".wl[986].w[2]" 0.0080491118862782898;
	setAttr ".wl[986].w[3]" 0.013392440767329827;
	setAttr ".wl[986].w[4]" 0.19049065426571793;
	setAttr ".wl[986].w[9]" 0.68309467043376759;
	setAttr ".wl[986].w[10]" 0.10497312264690636;
	setAttr -s 5 ".wl[987].w";
	setAttr ".wl[987].w[2]" 0.0074281064736539991;
	setAttr ".wl[987].w[3]" 0.012572339687694015;
	setAttr ".wl[987].w[4]" 0.18698714973903144;
	setAttr ".wl[987].w[9]" 0.69166384945228976;
	setAttr ".wl[987].w[10]" 0.10134855464733068;
	setAttr -s 5 ".wl[988].w";
	setAttr ".wl[988].w[2]" 0.0068111354110834566;
	setAttr ".wl[988].w[3]" 0.01206659138115339;
	setAttr ".wl[988].w[4]" 0.18857081905338932;
	setAttr ".wl[988].w[9]" 0.69248303281643009;
	setAttr ".wl[988].w[10]" 0.10006842133794361;
	setAttr -s 5 ".wl[989].w";
	setAttr ".wl[989].w[3]" 0.011495636964941163;
	setAttr ".wl[989].w[4]" 0.19124824971705143;
	setAttr ".wl[989].w[5]" 0.0081178983306804814;
	setAttr ".wl[989].w[9]" 0.6889570551147719;
	setAttr ".wl[989].w[10]" 0.10018115987255502;
	setAttr -s 5 ".wl[990].w";
	setAttr ".wl[990].w[4]" 0.18236566589170181;
	setAttr ".wl[990].w[5]" 0.010310428120029195;
	setAttr ".wl[990].w[6]" 0.010310428120029195;
	setAttr ".wl[990].w[9]" 0.70284405111460202;
	setAttr ".wl[990].w[10]" 0.094169426753637764;
	setAttr -s 5 ".wl[991].w";
	setAttr ".wl[991].w[4]" 0.17463917438941495;
	setAttr ".wl[991].w[5]" 0.013751633375949826;
	setAttr ".wl[991].w[6]" 0.013751633375949826;
	setAttr ".wl[991].w[9]" 0.70786406363485643;
	setAttr ".wl[991].w[10]" 0.089993495223829101;
	setAttr -s 5 ".wl[992].w";
	setAttr ".wl[992].w[4]" 0.17381993784845215;
	setAttr ".wl[992].w[5]" 0.01885082077094738;
	setAttr ".wl[992].w[6]" 0.01885082077094738;
	setAttr ".wl[992].w[9]" 0.69874633203597492;
	setAttr ".wl[992].w[10]" 0.089732088573678231;
	setAttr -s 5 ".wl[993].w";
	setAttr ".wl[993].w[4]" 0.17361597987321509;
	setAttr ".wl[993].w[5]" 0.025222034341111921;
	setAttr ".wl[993].w[6]" 0.025222034341111921;
	setAttr ".wl[993].w[9]" 0.68723339320019072;
	setAttr ".wl[993].w[10]" 0.088706558244370348;
	setAttr -s 5 ".wl[994].w";
	setAttr ".wl[994].w[4]" 0.17885718020574209;
	setAttr ".wl[994].w[5]" 0.036716834627999496;
	setAttr ".wl[994].w[6]" 0.036716834627999496;
	setAttr ".wl[994].w[9]" 0.65700486190758678;
	setAttr ".wl[994].w[10]" 0.09070428863067212;
	setAttr -s 5 ".wl[995].w";
	setAttr ".wl[995].w[4]" 0.075150510580620722;
	setAttr ".wl[995].w[5]" 0.025369641904418958;
	setAttr ".wl[995].w[6]" 0.025369641904418958;
	setAttr ".wl[995].w[9]" 0.63138953367113393;
	setAttr ".wl[995].w[10]" 0.24272067193940744;
	setAttr -s 5 ".wl[996].w";
	setAttr ".wl[996].w[4]" 0.075242447834473786;
	setAttr ".wl[996].w[5]" 0.023117450404522219;
	setAttr ".wl[996].w[6]" 0.023117450404522219;
	setAttr ".wl[996].w[9]" 0.63385987811632649;
	setAttr ".wl[996].w[10]" 0.24466277324015526;
	setAttr -s 5 ".wl[997].w";
	setAttr ".wl[997].w[4]" 0.075937171339715101;
	setAttr ".wl[997].w[5]" 0.019088303358880156;
	setAttr ".wl[997].w[6]" 0.019088303358880156;
	setAttr ".wl[997].w[9]" 0.63662111059643989;
	setAttr ".wl[997].w[10]" 0.2492651113460847;
	setAttr -s 5 ".wl[998].w";
	setAttr ".wl[998].w[4]" 0.078923924687420377;
	setAttr ".wl[998].w[5]" 0.016402254321117027;
	setAttr ".wl[998].w[6]" 0.01635295678197228;
	setAttr ".wl[998].w[9]" 0.63316978817133662;
	setAttr ".wl[998].w[10]" 0.25515107603815362;
	setAttr -s 5 ".wl[999].w";
	setAttr ".wl[999].w[4]" 0.081389178914648794;
	setAttr ".wl[999].w[5]" 0.013614936538310952;
	setAttr ".wl[999].w[6]" 0.013355352215303725;
	setAttr ".wl[999].w[9]" 0.63301525928432445;
	setAttr ".wl[999].w[10]" 0.25862527304741212;
	setAttr -s 5 ".wl[1000].w";
	setAttr ".wl[1000].w[4]" 0.087030771212263908;
	setAttr ".wl[1000].w[5]" 0.011444895106584595;
	setAttr ".wl[1000].w[6]" 0.010900071887487912;
	setAttr ".wl[1000].w[9]" 0.62625234402778629;
	setAttr ".wl[1000].w[10]" 0.26437191776587732;
	setAttr -s 5 ".wl[1001].w";
	setAttr ".wl[1001].w[4]" 0.092349375407457246;
	setAttr ".wl[1001].w[5]" 0.0096076722003045952;
	setAttr ".wl[1001].w[6]" 0.0088966540216888958;
	setAttr ".wl[1001].w[9]" 0.61876103100095536;
	setAttr ".wl[1001].w[10]" 0.2703852673695939;
	setAttr -s 5 ".wl[1002].w";
	setAttr ".wl[1002].w[3]" 0.0072009625384802682;
	setAttr ".wl[1002].w[4]" 0.089119667049842921;
	setAttr ".wl[1002].w[5]" 0.0071600524215505697;
	setAttr ".wl[1002].w[9]" 0.62652536148276161;
	setAttr ".wl[1002].w[10]" 0.26999395650736457;
	setAttr -s 5 ".wl[1003].w";
	setAttr ".wl[1003].w[3]" 0.0068819912353637082;
	setAttr ".wl[1003].w[4]" 0.085488777705150204;
	setAttr ".wl[1003].w[5]" 0.0055290459713390576;
	setAttr ".wl[1003].w[9]" 0.63246293332622605;
	setAttr ".wl[1003].w[10]" 0.26963725176192099;
	setAttr -s 5 ".wl[1004].w";
	setAttr ".wl[1004].w[3]" 0.0068881119694712062;
	setAttr ".wl[1004].w[4]" 0.084827298850474728;
	setAttr ".wl[1004].w[5]" 0.0047547105580131805;
	setAttr ".wl[1004].w[9]" 0.63270388434618619;
	setAttr ".wl[1004].w[10]" 0.2708259942758548;
	setAttr -s 5 ".wl[1005].w";
	setAttr ".wl[1005].w[3]" 0.0068095012380714253;
	setAttr ".wl[1005].w[4]" 0.083612677615947026;
	setAttr ".wl[1005].w[9]" 0.63562612787196215;
	setAttr ".wl[1005].w[10]" 0.26934147668254393;
	setAttr ".wl[1005].w[11]" 0.00461021659147533;
	setAttr -s 5 ".wl[1006].w";
	setAttr ".wl[1006].w[3]" 0.0072101526604900204;
	setAttr ".wl[1006].w[4]" 0.085077525056530684;
	setAttr ".wl[1006].w[9]" 0.63346169517950168;
	setAttr ".wl[1006].w[10]" 0.26956099969244535;
	setAttr ".wl[1006].w[11]" 0.0046896274110322213;
	setAttr -s 5 ".wl[1007].w";
	setAttr ".wl[1007].w[3]" 0.0078180773786945493;
	setAttr ".wl[1007].w[4]" 0.086558645927093794;
	setAttr ".wl[1007].w[9]" 0.63026722839479132;
	setAttr ".wl[1007].w[10]" 0.27055916404515079;
	setAttr ".wl[1007].w[11]" 0.0047968842542694748;
	setAttr -s 5 ".wl[1008].w";
	setAttr ".wl[1008].w[2]" 0.0048005939595991993;
	setAttr ".wl[1008].w[3]" 0.0073611097919360595;
	setAttr ".wl[1008].w[4]" 0.080163101816797214;
	setAttr ".wl[1008].w[9]" 0.64298883939677154;
	setAttr ".wl[1008].w[10]" 0.26468635503489596;
	setAttr -s 5 ".wl[1009].w";
	setAttr ".wl[1009].w[2]" 0.0046063260926863091;
	setAttr ".wl[1009].w[3]" 0.0068234119670721191;
	setAttr ".wl[1009].w[4]" 0.074121512137083012;
	setAttr ".wl[1009].w[9]" 0.65516016673229871;
	setAttr ".wl[1009].w[10]" 0.25928858307085989;
	setAttr -s 5 ".wl[1010].w";
	setAttr ".wl[1010].w[2]" 0.0045158808810882248;
	setAttr ".wl[1010].w[3]" 0.0066149686133691182;
	setAttr ".wl[1010].w[4]" 0.071853345579794101;
	setAttr ".wl[1010].w[9]" 0.66007542184735213;
	setAttr ".wl[1010].w[10]" 0.25694038307839651;
	setAttr -s 5 ".wl[1011].w";
	setAttr ".wl[1011].w[2]" 0.0041852304997605583;
	setAttr ".wl[1011].w[3]" 0.0062134611791426287;
	setAttr ".wl[1011].w[4]" 0.069474256584335828;
	setAttr ".wl[1011].w[9]" 0.66801162727212104;
	setAttr ".wl[1011].w[10]" 0.25211542446464008;
	setAttr -s 5 ".wl[1012].w";
	setAttr ".wl[1012].w[3]" 0.0060465007836167277;
	setAttr ".wl[1012].w[4]" 0.070105026820262983;
	setAttr ".wl[1012].w[5]" 0.0043987291222005783;
	setAttr ".wl[1012].w[9]" 0.66968237509989936;
	setAttr ".wl[1012].w[10]" 0.24976736817402026;
	setAttr -s 5 ".wl[1013].w";
	setAttr ".wl[1013].w[3]" 0.0058973815668769041;
	setAttr ".wl[1013].w[4]" 0.071663192283981172;
	setAttr ".wl[1013].w[5]" 0.005768971224375973;
	setAttr ".wl[1013].w[9]" 0.66726006714350461;
	setAttr ".wl[1013].w[10]" 0.24941038778126137;
	setAttr -s 5 ".wl[1014].w";
	setAttr ".wl[1014].w[4]" 0.066501186189200936;
	setAttr ".wl[1014].w[5]" 0.0069521491021917086;
	setAttr ".wl[1014].w[6]" 0.0069521491021917086;
	setAttr ".wl[1014].w[9]" 0.67822621517147563;
	setAttr ".wl[1014].w[10]" 0.24136830043494009;
	setAttr -s 5 ".wl[1015].w";
	setAttr ".wl[1015].w[4]" 0.062873866552795185;
	setAttr ".wl[1015].w[5]" 0.0087384782272172008;
	setAttr ".wl[1015].w[6]" 0.0087384782272172008;
	setAttr ".wl[1015].w[9]" 0.68342384090385744;
	setAttr ".wl[1015].w[10]" 0.23622533608891289;
	setAttr -s 5 ".wl[1016].w";
	setAttr ".wl[1016].w[4]" 0.063289168675347826;
	setAttr ".wl[1016].w[5]" 0.011360209862506531;
	setAttr ".wl[1016].w[6]" 0.011360209862506531;
	setAttr ".wl[1016].w[9]" 0.67773740415647488;
	setAttr ".wl[1016].w[10]" 0.2362530074431643;
	setAttr -s 5 ".wl[1017].w";
	setAttr ".wl[1017].w[4]" 0.064145472441899581;
	setAttr ".wl[1017].w[5]" 0.014435286786048836;
	setAttr ".wl[1017].w[6]" 0.014435286786048836;
	setAttr ".wl[1017].w[9]" 0.67203465486053293;
	setAttr ".wl[1017].w[10]" 0.23494929912546977;
	setAttr -s 5 ".wl[1018].w";
	setAttr ".wl[1018].w[4]" 0.069051317268152224;
	setAttr ".wl[1018].w[5]" 0.019976470945212012;
	setAttr ".wl[1018].w[6]" 0.019976470945212012;
	setAttr ".wl[1018].w[9]" 0.65329339153487676;
	setAttr ".wl[1018].w[10]" 0.23770234930654699;
	setAttr -s 5 ".wl[1019].w";
	setAttr ".wl[1019].w[4]" 0.02714997964153866;
	setAttr ".wl[1019].w[5]" 0.011366540260501875;
	setAttr ".wl[1019].w[6]" 0.011366540260501875;
	setAttr ".wl[1019].w[9]" 0.51997428112399879;
	setAttr ".wl[1019].w[10]" 0.43014265871345886;
	setAttr -s 5 ".wl[1020].w";
	setAttr ".wl[1020].w[4]" 0.02716693874662348;
	setAttr ".wl[1020].w[5]" 0.010613402731779213;
	setAttr ".wl[1020].w[6]" 0.010613402731779213;
	setAttr ".wl[1020].w[9]" 0.52020307303082902;
	setAttr ".wl[1020].w[10]" 0.43140318275898903;
	setAttr -s 5 ".wl[1021].w";
	setAttr ".wl[1021].w[4]" 0.027410354735471109;
	setAttr ".wl[1021].w[5]" 0.0092052494832022894;
	setAttr ".wl[1021].w[6]" 0.0092052494832022894;
	setAttr ".wl[1021].w[9]" 0.52018378982001889;
	setAttr ".wl[1021].w[10]" 0.43399535647810539;
	setAttr -s 5 ".wl[1022].w";
	setAttr ".wl[1022].w[4]" 0.028802059368837996;
	setAttr ".wl[1022].w[5]" 0.0083113395647493003;
	setAttr ".wl[1022].w[6]" 0.0083113395647493003;
	setAttr ".wl[1022].w[9]" 0.51864112211731495;
	setAttr ".wl[1022].w[10]" 0.43593413938434844;
	setAttr -s 5 ".wl[1023].w";
	setAttr ".wl[1023].w[4]" 0.029845826590126773;
	setAttr ".wl[1023].w[5]" 0.0071473471977096317;
	setAttr ".wl[1023].w[6]" 0.0071473471977096317;
	setAttr ".wl[1023].w[9]" 0.51880560330477876;
	setAttr ".wl[1023].w[10]" 0.43705387570967524;
	setAttr -s 5 ".wl[1024].w";
	setAttr ".wl[1024].w[4]" 0.032495991487935333;
	setAttr ".wl[1024].w[5]" 0.006242021895473137;
	setAttr ".wl[1024].w[9]" 0.51697385644961269;
	setAttr ".wl[1024].w[10]" 0.43765083865222687;
	setAttr ".wl[1024].w[11]" 0.0066372915147520405;
	setAttr -s 5 ".wl[1025].w";
	setAttr ".wl[1025].w[4]" 0.035082734132921775;
	setAttr ".wl[1025].w[5]" 0.0054418788166332053;
	setAttr ".wl[1025].w[9]" 0.51414529315751401;
	setAttr ".wl[1025].w[10]" 0.43809449620119073;
	setAttr ".wl[1025].w[11]" 0.0072355976917402309;
	setAttr -s 5 ".wl[1026].w";
	setAttr ".wl[1026].w[4]" 0.033241043253210895;
	setAttr ".wl[1026].w[5]" 0.0041461996999673789;
	setAttr ".wl[1026].w[9]" 0.51620374979228312;
	setAttr ".wl[1026].w[10]" 0.43953055660540541;
	setAttr ".wl[1026].w[11]" 0.006878450649133159;
	setAttr -s 5 ".wl[1027].w";
	setAttr ".wl[1027].w[3]" 0.0034041255553646948;
	setAttr ".wl[1027].w[4]" 0.031362217021789429;
	setAttr ".wl[1027].w[9]" 0.51755918724538563;
	setAttr ".wl[1027].w[10]" 0.4411306254129283;
	setAttr ".wl[1027].w[11]" 0.0065438447645317635;
	setAttr -s 5 ".wl[1028].w";
	setAttr ".wl[1028].w[3]" 0.0033864094041552396;
	setAttr ".wl[1028].w[4]" 0.030997726389935962;
	setAttr ".wl[1028].w[9]" 0.51720213045847219;
	setAttr ".wl[1028].w[10]" 0.44189739864438132;
	setAttr ".wl[1028].w[11]" 0.0065163351030553601;
	setAttr -s 5 ".wl[1029].w";
	setAttr ".wl[1029].w[3]" 0.0033167551011489198;
	setAttr ".wl[1029].w[4]" 0.030278812437048476;
	setAttr ".wl[1029].w[9]" 0.51830868870061897;
	setAttr ".wl[1029].w[10]" 0.44175679012223912;
	setAttr ".wl[1029].w[11]" 0.0063389536389445991;
	setAttr -s 5 ".wl[1030].w";
	setAttr ".wl[1030].w[3]" 0.0034799678821920482;
	setAttr ".wl[1030].w[4]" 0.030873923873029636;
	setAttr ".wl[1030].w[9]" 0.51808165931179073;
	setAttr ".wl[1030].w[10]" 0.44112966236976886;
	setAttr ".wl[1030].w[11]" 0.0064347865632187976;
	setAttr -s 5 ".wl[1031].w";
	setAttr ".wl[1031].w[3]" 0.0037256746971461078;
	setAttr ".wl[1031].w[4]" 0.031543045675810802;
	setAttr ".wl[1031].w[9]" 0.51730266549603765;
	setAttr ".wl[1031].w[10]" 0.44085318502819315;
	setAttr ".wl[1031].w[11]" 0.0065754291028123314;
	setAttr -s 5 ".wl[1032].w";
	setAttr ".wl[1032].w[3]" 0.0034020812652497904;
	setAttr ".wl[1032].w[4]" 0.028310154487663261;
	setAttr ".wl[1032].w[9]" 0.52131983217762712;
	setAttr ".wl[1032].w[10]" 0.44111006510115269;
	setAttr ".wl[1032].w[11]" 0.0058578669683072571;
	setAttr -s 5 ".wl[1033].w";
	setAttr ".wl[1033].w[3]" 0.0030754225763462988;
	setAttr ".wl[1033].w[4]" 0.025426822867312401;
	setAttr ".wl[1033].w[9]" 0.5247624497816038;
	setAttr ".wl[1033].w[10]" 0.44148775005608637;
	setAttr ".wl[1033].w[11]" 0.0052475547186512745;
	setAttr -s 5 ".wl[1034].w";
	setAttr ".wl[1034].w[3]" 0.0029528080683600909;
	setAttr ".wl[1034].w[4]" 0.024369387518377895;
	setAttr ".wl[1034].w[9]" 0.52616217890695383;
	setAttr ".wl[1034].w[10]" 0.44149451612221213;
	setAttr ".wl[1034].w[11]" 0.0050211093840960572;
	setAttr -s 5 ".wl[1035].w";
	setAttr ".wl[1035].w[3]" 0.0027572088363951482;
	setAttr ".wl[1035].w[4]" 0.023205155832075847;
	setAttr ".wl[1035].w[9]" 0.52902593089790217;
	setAttr ".wl[1035].w[10]" 0.44029347491148318;
	setAttr ".wl[1035].w[11]" 0.0047182295221436243;
	setAttr -s 5 ".wl[1036].w";
	setAttr ".wl[1036].w[3]" 0.0027065268046050574;
	setAttr ".wl[1036].w[4]" 0.023411327356823625;
	setAttr ".wl[1036].w[9]" 0.53043905030470029;
	setAttr ".wl[1036].w[10]" 0.43875312309174663;
	setAttr ".wl[1036].w[11]" 0.0046899724421244834;
	setAttr -s 5 ".wl[1037].w";
	setAttr ".wl[1037].w[4]" 0.024122605012027145;
	setAttr ".wl[1037].w[5]" 0.0032432839494016762;
	setAttr ".wl[1037].w[9]" 0.5301960108306335;
	setAttr ".wl[1037].w[10]" 0.43764500116187582;
	setAttr ".wl[1037].w[11]" 0.0047930990460619783;
	setAttr -s 5 ".wl[1038].w";
	setAttr ".wl[1038].w[4]" 0.0219357920690504;
	setAttr ".wl[1038].w[5]" 0.0037007700819745794;
	setAttr ".wl[1038].w[9]" 0.53387802287793684;
	setAttr ".wl[1038].w[10]" 0.43617410504526677;
	setAttr ".wl[1038].w[11]" 0.0043113099257714761;
	setAttr -s 5 ".wl[1039].w";
	setAttr ".wl[1039].w[4]" 0.02055268130470685;
	setAttr ".wl[1039].w[5]" 0.0043943207366834635;
	setAttr ".wl[1039].w[6]" 0.0043943207366834635;
	setAttr ".wl[1039].w[9]" 0.53527443341863112;
	setAttr ".wl[1039].w[10]" 0.43538424380329505;
	setAttr -s 5 ".wl[1040].w";
	setAttr ".wl[1040].w[4]" 0.020918659436926147;
	setAttr ".wl[1040].w[5]" 0.0054760823046921892;
	setAttr ".wl[1040].w[6]" 0.0054760823046921892;
	setAttr ".wl[1040].w[9]" 0.53329600699987678;
	setAttr ".wl[1040].w[10]" 0.4348331689538128;
	setAttr -s 5 ".wl[1041].w";
	setAttr ".wl[1041].w[4]" 0.021449652887153444;
	setAttr ".wl[1041].w[5]" 0.0066853926902368487;
	setAttr ".wl[1041].w[6]" 0.0066853926902368487;
	setAttr ".wl[1041].w[9]" 0.53194873157091405;
	setAttr ".wl[1041].w[10]" 0.43323083016145875;
	setAttr -s 5 ".wl[1042].w";
	setAttr ".wl[1042].w[4]" 0.023924568246763787;
	setAttr ".wl[1042].w[5]" 0.0089760274081461342;
	setAttr ".wl[1042].w[6]" 0.0089760274081461342;
	setAttr ".wl[1042].w[9]" 0.5267857480857;
	setAttr ".wl[1042].w[10]" 0.43133762885124399;
	setAttr -s 5 ".wl[1043].w";
	setAttr ".wl[1043].w[4]" 0.012665327057670263;
	setAttr ".wl[1043].w[5]" 0.0062106150072895388;
	setAttr ".wl[1043].w[9]" 0.4752476336441625;
	setAttr ".wl[1043].w[10]" 0.49648776288086599;
	setAttr ".wl[1043].w[11]" 0.0093886614100117637;
	setAttr -s 5 ".wl[1044].w";
	setAttr ".wl[1044].w[4]" 0.012715393391992871;
	setAttr ".wl[1044].w[5]" 0.0059185657379431621;
	setAttr ".wl[1044].w[9]" 0.4751008831868262;
	setAttr ".wl[1044].w[10]" 0.49678863101583859;
	setAttr ".wl[1044].w[11]" 0.0094765266673991679;
	setAttr -s 5 ".wl[1045].w";
	setAttr ".wl[1045].w[4]" 0.012886535706518468;
	setAttr ".wl[1045].w[5]" 0.0053414302755595156;
	setAttr ".wl[1045].w[9]" 0.47475842867673373;
	setAttr ".wl[1045].w[10]" 0.4973014910881377;
	setAttr ".wl[1045].w[11]" 0.0097121142530505553;
	setAttr -s 5 ".wl[1046].w";
	setAttr ".wl[1046].w[4]" 0.013629977943338123;
	setAttr ".wl[1046].w[5]" 0.0050129592417612256;
	setAttr ".wl[1046].w[9]" 0.47446870833318572;
	setAttr ".wl[1046].w[10]" 0.49656364362156163;
	setAttr ".wl[1046].w[11]" 0.010324710860153324;
	setAttr -s 5 ".wl[1047].w";
	setAttr ".wl[1047].w[4]" 0.014132319482481021;
	setAttr ".wl[1047].w[5]" 0.004477639215083983;
	setAttr ".wl[1047].w[9]" 0.4747152552699816;
	setAttr ".wl[1047].w[10]" 0.49599753133332297;
	setAttr ".wl[1047].w[11]" 0.010677254699130456;
	setAttr -s 5 ".wl[1048].w";
	setAttr ".wl[1048].w[4]" 0.015460381077526921;
	setAttr ".wl[1048].w[5]" 0.0040664612533566373;
	setAttr ".wl[1048].w[9]" 0.47485139131390386;
	setAttr ".wl[1048].w[10]" 0.4940357517143571;
	setAttr ".wl[1048].w[11]" 0.01158601464085552;
	setAttr -s 5 ".wl[1049].w";
	setAttr ".wl[1049].w[4]" 0.016808914493807878;
	setAttr ".wl[1049].w[5]" 0.0036510141823958148;
	setAttr ".wl[1049].w[9]" 0.47460899708685228;
	setAttr ".wl[1049].w[10]" 0.49236689664661842;
	setAttr ".wl[1049].w[11]" 0.012564177590325553;
	setAttr -s 5 ".wl[1050].w";
	setAttr ".wl[1050].w[4]" 0.01581700026274559;
	setAttr ".wl[1050].w[5]" 0.0028497870660943404;
	setAttr ".wl[1050].w[9]" 0.4748975307439528;
	setAttr ".wl[1050].w[10]" 0.49448279527163069;
	setAttr ".wl[1050].w[11]" 0.011952886655576589;
	setAttr -s 5 ".wl[1051].w";
	setAttr ".wl[1051].w[4]" 0.014843033932289112;
	setAttr ".wl[1051].w[5]" 0.002294464384806964;
	setAttr ".wl[1051].w[9]" 0.47473734897111741;
	setAttr ".wl[1051].w[10]" 0.49673140532599491;
	setAttr ".wl[1051].w[11]" 0.011393747385791506;
	setAttr -s 5 ".wl[1052].w";
	setAttr ".wl[1052].w[3]" 0.0020653950790759782;
	setAttr ".wl[1052].w[4]" 0.014654998975952221;
	setAttr ".wl[1052].w[9]" 0.47448463684664888;
	setAttr ".wl[1052].w[10]" 0.49745322827559574;
	setAttr ".wl[1052].w[11]" 0.011341740822727186;
	setAttr -s 5 ".wl[1053].w";
	setAttr ".wl[1053].w[3]" 0.0020091507305727133;
	setAttr ".wl[1053].w[4]" 0.01422169573508175;
	setAttr ".wl[1053].w[9]" 0.47465969950439291;
	setAttr ".wl[1053].w[10]" 0.49810830781512483;
	setAttr ".wl[1053].w[11]" 0.011001146214827794;
	setAttr -s 5 ".wl[1054].w";
	setAttr ".wl[1054].w[3]" 0.0020853743996107599;
	setAttr ".wl[1054].w[4]" 0.01444787816429853;
	setAttr ".wl[1054].w[9]" 0.47491177604851087;
	setAttr ".wl[1054].w[10]" 0.49745421116258248;
	setAttr ".wl[1054].w[11]" 0.011100760224997414;
	setAttr -s 5 ".wl[1055].w";
	setAttr ".wl[1055].w[3]" 0.0022052661523726562;
	setAttr ".wl[1055].w[4]" 0.014742043110519914;
	setAttr ".wl[1055].w[9]" 0.47491621580756349;
	setAttr ".wl[1055].w[10]" 0.4968432709920389;
	setAttr ".wl[1055].w[11]" 0.011293203937505158;
	setAttr -s 5 ".wl[1056].w";
	setAttr ".wl[1056].w[3]" 0.0019828871670538664;
	setAttr ".wl[1056].w[4]" 0.013040446664886957;
	setAttr ".wl[1056].w[9]" 0.47502898556157885;
	setAttr ".wl[1056].w[10]" 0.49987602441615048;
	setAttr ".wl[1056].w[11]" 0.010071656190329759;
	setAttr -s 5 ".wl[1057].w";
	setAttr ".wl[1057].w[3]" 0.001774526984791849;
	setAttr ".wl[1057].w[4]" 0.011580477506667517;
	setAttr ".wl[1057].w[9]" 0.47464337977627191;
	setAttr ".wl[1057].w[10]" 0.50295437550203226;
	setAttr ".wl[1057].w[11]" 0.0090472402302363211;
	setAttr -s 5 ".wl[1058].w";
	setAttr ".wl[1058].w[3]" 0.0016970988407762433;
	setAttr ".wl[1058].w[4]" 0.011048538684262107;
	setAttr ".wl[1058].w[9]" 0.47445162167128169;
	setAttr ".wl[1058].w[10]" 0.50413851218221517;
	setAttr ".wl[1058].w[11]" 0.0086642286214648288;
	setAttr -s 5 ".wl[1059].w";
	setAttr ".wl[1059].w[3]" 0.0015794611486545339;
	setAttr ".wl[1059].w[4]" 0.010426941162907473;
	setAttr ".wl[1059].w[9]" 0.47482176342450944;
	setAttr ".wl[1059].w[10]" 0.50504372532150277;
	setAttr ".wl[1059].w[11]" 0.0081281089424258702;
	setAttr -s 5 ".wl[1060].w";
	setAttr ".wl[1060].w[4]" 0.010474461671806476;
	setAttr ".wl[1060].w[5]" 0.0017348334623072848;
	setAttr ".wl[1060].w[9]" 0.47561783977698152;
	setAttr ".wl[1060].w[10]" 0.50412348639623861;
	setAttr ".wl[1060].w[11]" 0.0080493786926661443;
	setAttr -s 5 ".wl[1061].w";
	setAttr ".wl[1061].w[4]" 0.010812402808588318;
	setAttr ".wl[1061].w[5]" 0.0021431676171058418;
	setAttr ".wl[1061].w[9]" 0.47607261391078975;
	setAttr ".wl[1061].w[10]" 0.50275248553703433;
	setAttr ".wl[1061].w[11]" 0.0082193301264817716;
	setAttr -s 5 ".wl[1062].w";
	setAttr ".wl[1062].w[4]" 0.0097606806969579296;
	setAttr ".wl[1062].w[5]" 0.0023463023888686016;
	setAttr ".wl[1062].w[9]" 0.47582516753359833;
	setAttr ".wl[1062].w[10]" 0.50463789220341437;
	setAttr ".wl[1062].w[11]" 0.0074299571771607948;
	setAttr -s 5 ".wl[1063].w";
	setAttr ".wl[1063].w[4]" 0.00915141954431049;
	setAttr ".wl[1063].w[5]" 0.0026734147141124864;
	setAttr ".wl[1063].w[9]" 0.47516733380422005;
	setAttr ".wl[1063].w[10]" 0.50599904680867946;
	setAttr ".wl[1063].w[11]" 0.0070087851286775122;
	setAttr -s 5 ".wl[1064].w";
	setAttr ".wl[1064].w[4]" 0.0093919492465589071;
	setAttr ".wl[1064].w[5]" 0.0032226211263499693;
	setAttr ".wl[1064].w[9]" 0.47495133278609092;
	setAttr ".wl[1064].w[10]" 0.5052394503240305;
	setAttr ".wl[1064].w[11]" 0.0071946465169696796;
	setAttr -s 5 ".wl[1065].w";
	setAttr ".wl[1065].w[4]" 0.0096813389238995691;
	setAttr ".wl[1065].w[5]" 0.0038061070742041233;
	setAttr ".wl[1065].w[9]" 0.47523224200595743;
	setAttr ".wl[1065].w[10]" 0.50392442416327332;
	setAttr ".wl[1065].w[11]" 0.007355887832665658;
	setAttr -s 5 ".wl[1066].w";
	setAttr ".wl[1066].w[4]" 0.01095220554118175;
	setAttr ".wl[1066].w[5]" 0.0049541682479591178;
	setAttr ".wl[1066].w[9]" 0.47561644098979089;
	setAttr ".wl[1066].w[10]" 0.5002813997825043;
	setAttr ".wl[1066].w[11]" 0.0081957854385639028;
	setAttr -s 5 ".wl[1067].w";
	setAttr ".wl[1067].w[4]" 0.0086111509881908691;
	setAttr ".wl[1067].w[5]" 0.0047429767178708742;
	setAttr ".wl[1067].w[9]" 0.33425670253916223;
	setAttr ".wl[1067].w[10]" 0.62857294234474215;
	setAttr ".wl[1067].w[11]" 0.023816227410033908;
	setAttr -s 5 ".wl[1068].w";
	setAttr ".wl[1068].w[4]" 0.0086676534891098854;
	setAttr ".wl[1068].w[5]" 0.0045856133054316232;
	setAttr ".wl[1068].w[9]" 0.33408680840433796;
	setAttr ".wl[1068].w[10]" 0.62858033298648786;
	setAttr ".wl[1068].w[11]" 0.024079591814632571;
	setAttr -s 5 ".wl[1069].w";
	setAttr ".wl[1069].w[4]" 0.0087981863799470612;
	setAttr ".wl[1069].w[5]" 0.0042508430091520704;
	setAttr ".wl[1069].w[9]" 0.33384286655078416;
	setAttr ".wl[1069].w[10]" 0.62844500749582888;
	setAttr ".wl[1069].w[11]" 0.024663096564287876;
	setAttr -s 5 ".wl[1070].w";
	setAttr ".wl[1070].w[4]" 0.00927903747763981;
	setAttr ".wl[1070].w[5]" 0.0040753276117272211;
	setAttr ".wl[1070].w[9]" 0.33695662040428354;
	setAttr ".wl[1070].w[10]" 0.62373389812799407;
	setAttr ".wl[1070].w[11]" 0.025955116378355361;
	setAttr -s 5 ".wl[1071].w";
	setAttr ".wl[1071].w[4]" 0.0095895519668761156;
	setAttr ".wl[1071].w[5]" 0.0037343880199331714;
	setAttr ".wl[1071].w[9]" 0.33959865960720875;
	setAttr ".wl[1071].w[10]" 0.62044099324992041;
	setAttr ".wl[1071].w[11]" 0.026636407156061557;
	setAttr -s 5 ".wl[1072].w";
	setAttr ".wl[1072].w[4]" 0.010407426326028153;
	setAttr ".wl[1072].w[5]" 0.0034758873824128106;
	setAttr ".wl[1072].w[9]" 0.34600378569729229;
	setAttr ".wl[1072].w[10]" 0.61175985513384501;
	setAttr ".wl[1072].w[11]" 0.028353045460421642;
	setAttr -s 5 ".wl[1073].w";
	setAttr ".wl[1073].w[4]" 0.01123369764453302;
	setAttr ".wl[1073].w[5]" 0.0031931792435597977;
	setAttr ".wl[1073].w[9]" 0.35114796286467315;
	setAttr ".wl[1073].w[10]" 0.60423729359234746;
	setAttr ".wl[1073].w[11]" 0.030187866654886562;
	setAttr -s 5 ".wl[1074].w";
	setAttr ".wl[1074].w[4]" 0.01059007766661939;
	setAttr ".wl[1074].w[5]" 0.0025699053141579995;
	setAttr ".wl[1074].w[9]" 0.34639866384122647;
	setAttr ".wl[1074].w[10]" 0.6114298867637491;
	setAttr ".wl[1074].w[11]" 0.029011466414247081;
	setAttr -s 5 ".wl[1075].w";
	setAttr ".wl[1075].w[4]" 0.0099602688935849261;
	setAttr ".wl[1075].w[5]" 0.0021213067703159056;
	setAttr ".wl[1075].w[9]" 0.34069923842848115;
	setAttr ".wl[1075].w[10]" 0.61927346665421967;
	setAttr ".wl[1075].w[11]" 0.027945719253398393;
	setAttr -s 5 ".wl[1076].w";
	setAttr ".wl[1076].w[4]" 0.009822691002102972;
	setAttr ".wl[1076].w[5]" 0.0019142426436895435;
	setAttr ".wl[1076].w[9]" 0.33885711491512682;
	setAttr ".wl[1076].w[10]" 0.62157389659576501;
	setAttr ".wl[1076].w[11]" 0.027832054843315732;
	setAttr -s 5 ".wl[1077].w";
	setAttr ".wl[1077].w[4]" 0.0095199707628314846;
	setAttr ".wl[1077].w[5]" 0.0017278695510254314;
	setAttr ".wl[1077].w[9]" 0.33685478589321705;
	setAttr ".wl[1077].w[10]" 0.62481927197548504;
	setAttr ".wl[1077].w[11]" 0.027078101817441077;
	setAttr -s 5 ".wl[1078].w";
	setAttr ".wl[1078].w[3]" 0.0017065928650273343;
	setAttr ".wl[1078].w[4]" 0.0096214909158506903;
	setAttr ".wl[1078].w[9]" 0.33841249105332316;
	setAttr ".wl[1078].w[10]" 0.62311414248112595;
	setAttr ".wl[1078].w[11]" 0.027145282684672898;
	setAttr -s 5 ".wl[1079].w";
	setAttr ".wl[1079].w[3]" 0.0017829650768299409;
	setAttr ".wl[1079].w[4]" 0.0097727817414385177;
	setAttr ".wl[1079].w[9]" 0.33981196455971513;
	setAttr ".wl[1079].w[10]" 0.62120180508999112;
	setAttr ".wl[1079].w[11]" 0.027430483532025172;
	setAttr -s 5 ".wl[1080].w";
	setAttr ".wl[1080].w[3]" 0.0016086472910308701;
	setAttr ".wl[1080].w[4]" 0.0086875139345599789;
	setAttr ".wl[1080].w[9]" 0.33070235597500303;
	setAttr ".wl[1080].w[10]" 0.63402808235185093;
	setAttr ".wl[1080].w[11]" 0.02497340044755527;
	setAttr -s 5 ".wl[1081].w";
	setAttr ".wl[1081].w[3]" 0.0014487004536565271;
	setAttr ".wl[1081].w[4]" 0.0077632685337668695;
	setAttr ".wl[1081].w[9]" 0.32100951305987085;
	setAttr ".wl[1081].w[10]" 0.64688373494932783;
	setAttr ".wl[1081].w[11]" 0.022894783003377866;
	setAttr -s 5 ".wl[1082].w";
	setAttr ".wl[1082].w[3]" 0.0013890720673743523;
	setAttr ".wl[1082].w[4]" 0.0074249611931830528;
	setAttr ".wl[1082].w[9]" 0.31713047125207339;
	setAttr ".wl[1082].w[10]" 0.65195688162728649;
	setAttr ".wl[1082].w[11]" 0.022098613860082765;
	setAttr -s 5 ".wl[1083].w";
	setAttr ".wl[1083].w[4]" 0.0070255093262421072;
	setAttr ".wl[1083].w[5]" 0.0014192106004165238;
	setAttr ".wl[1083].w[9]" 0.31356710013044226;
	setAttr ".wl[1083].w[10]" 0.65706245891571302;
	setAttr ".wl[1083].w[11]" 0.020925721027186081;
	setAttr -s 5 ".wl[1084].w";
	setAttr ".wl[1084].w[4]" 0.0070523190742431234;
	setAttr ".wl[1084].w[5]" 0.0016070690395782647;
	setAttr ".wl[1084].w[9]" 0.31567146340049634;
	setAttr ".wl[1084].w[10]" 0.65496514694445429;
	setAttr ".wl[1084].w[11]" 0.020704001541228086;
	setAttr -s 5 ".wl[1085].w";
	setAttr ".wl[1085].w[4]" 0.0072756451111006572;
	setAttr ".wl[1085].w[5]" 0.0019299304310144191;
	setAttr ".wl[1085].w[9]" 0.31941824332817065;
	setAttr ".wl[1085].w[10]" 0.65032040280064052;
	setAttr ".wl[1085].w[11]" 0.021055778329073672;
	setAttr -s 5 ".wl[1086].w";
	setAttr ".wl[1086].w[4]" 0.0066358903961965254;
	setAttr ".wl[1086].w[5]" 0.0020713904840953803;
	setAttr ".wl[1086].w[9]" 0.31195899849077013;
	setAttr ".wl[1086].w[10]" 0.65989486657819674;
	setAttr ".wl[1086].w[11]" 0.019438854050741177;
	setAttr -s 5 ".wl[1087].w";
	setAttr ".wl[1087].w[4]" 0.0062817837330150115;
	setAttr ".wl[1087].w[5]" 0.0023025089100316577;
	setAttr ".wl[1087].w[9]" 0.30665080722747079;
	setAttr ".wl[1087].w[10]" 0.66613707851317006;
	setAttr ".wl[1087].w[11]" 0.018627821616312438;
	setAttr -s 5 ".wl[1088].w";
	setAttr ".wl[1088].w[4]" 0.006462967082269443;
	setAttr ".wl[1088].w[5]" 0.0026958593815208129;
	setAttr ".wl[1088].w[9]" 0.30883231649821546;
	setAttr ".wl[1088].w[10]" 0.66289931033487604;
	setAttr ".wl[1088].w[11]" 0.019109546703118194;
	setAttr -s 5 ".wl[1089].w";
	setAttr ".wl[1089].w[4]" 0.0066688352600907039;
	setAttr ".wl[1089].w[5]" 0.0030983661662416897;
	setAttr ".wl[1089].w[9]" 0.31232194042081729;
	setAttr ".wl[1089].w[10]" 0.65841893422546305;
	setAttr ".wl[1089].w[11]" 0.019491923927387262;
	setAttr -s 5 ".wl[1090].w";
	setAttr ".wl[1090].w[4]" 0.0075003552326902614;
	setAttr ".wl[1090].w[5]" 0.0038852145963801676;
	setAttr ".wl[1090].w[9]" 0.32321652242057364;
	setAttr ".wl[1090].w[10]" 0.64409592488854939;
	setAttr ".wl[1090].w[11]" 0.02130198286180664;
	setAttr -s 5 ".wl[1091].w";
	setAttr ".wl[1091].w[4]" 0.0060444300413429082;
	setAttr ".wl[1091].w[5]" 0.0036323189918937952;
	setAttr ".wl[1091].w[9]" 0.16635485376386727;
	setAttr ".wl[1091].w[10]" 0.76030601964893296;
	setAttr ".wl[1091].w[11]" 0.063662377553963173;
	setAttr -s 5 ".wl[1092].w";
	setAttr ".wl[1092].w[4]" 0.0060951299286805304;
	setAttr ".wl[1092].w[5]" 0.0035478428547625657;
	setAttr ".wl[1092].w[9]" 0.16667387814108764;
	setAttr ".wl[1092].w[10]" 0.7593078189684499;
	setAttr ".wl[1092].w[11]" 0.064375330107019388;
	setAttr -s 5 ".wl[1093].w";
	setAttr ".wl[1093].w[4]" 0.0061887646083419578;
	setAttr ".wl[1093].w[5]" 0.0033505195739857579;
	setAttr ".wl[1093].w[9]" 0.16716807744465112;
	setAttr ".wl[1093].w[10]" 0.75752328528419577;
	setAttr ".wl[1093].w[11]" 0.065769353088825352;
	setAttr -s 5 ".wl[1094].w";
	setAttr ".wl[1094].w[4]" 0.0065261226086217776;
	setAttr ".wl[1094].w[5]" 0.0032706123292227372;
	setAttr ".wl[1094].w[9]" 0.17140519513979963;
	setAttr ".wl[1094].w[10]" 0.75019872776703933;
	setAttr ".wl[1094].w[11]" 0.068599342155316531;
	setAttr -s 5 ".wl[1095].w";
	setAttr ".wl[1095].w[4]" 0.0067415274016229777;
	setAttr ".wl[1095].w[5]" 0.0030625825725446015;
	setAttr ".wl[1095].w[9]" 0.1744612221589153;
	setAttr ".wl[1095].w[10]" 0.74573292817805925;
	setAttr ".wl[1095].w[11]" 0.070001739688857909;
	setAttr -s 5 ".wl[1096].w";
	setAttr ".wl[1096].w[4]" 0.0073084275735812608;
	setAttr ".wl[1096].w[5]" 0.0029227148726025287;
	setAttr ".wl[1096].w[9]" 0.18220711826988953;
	setAttr ".wl[1096].w[10]" 0.73411467502667815;
	setAttr ".wl[1096].w[11]" 0.073447064257248521;
	setAttr -s 5 ".wl[1097].w";
	setAttr ".wl[1097].w[4]" 0.0078699373621733855;
	setAttr ".wl[1097].w[5]" 0.0027471898296821916;
	setAttr ".wl[1097].w[9]" 0.18897582131804233;
	setAttr ".wl[1097].w[10]" 0.72337576533950676;
	setAttr ".wl[1097].w[11]" 0.077031286150595277;
	setAttr -s 5 ".wl[1098].w";
	setAttr ".wl[1098].w[4]" 0.0073940590127412149;
	setAttr ".wl[1098].w[5]" 0.0022584798539221776;
	setAttr ".wl[1098].w[9]" 0.18265895155165116;
	setAttr ".wl[1098].w[10]" 0.73308944514714069;
	setAttr ".wl[1098].w[11]" 0.07459906443454481;
	setAttr -s 5 ".wl[1099].w";
	setAttr ".wl[1099].w[4]" 0.0069237590101890864;
	setAttr ".wl[1099].w[5]" 0.0018920398987802717;
	setAttr ".wl[1099].w[9]" 0.1757163450140603;
	setAttr ".wl[1099].w[10]" 0.74311637380386919;
	setAttr ".wl[1099].w[11]" 0.07235148227310112;
	setAttr -s 5 ".wl[1100].w";
	setAttr ".wl[1100].w[4]" 0.006803835303811484;
	setAttr ".wl[1100].w[5]" 0.0017218957470374479;
	setAttr ".wl[1100].w[9]" 0.17358989574324016;
	setAttr ".wl[1100].w[10]" 0.74586267259814698;
	setAttr ".wl[1100].w[11]" 0.072021700607763917;
	setAttr -s 5 ".wl[1101].w";
	setAttr ".wl[1101].w[4]" 0.0065726956018025877;
	setAttr ".wl[1101].w[5]" 0.0015644364787650794;
	setAttr ".wl[1101].w[9]" 0.17064472913696219;
	setAttr ".wl[1101].w[10]" 0.7509413833110623;
	setAttr ".wl[1101].w[11]" 0.07027675547140784;
	setAttr -s 5 ".wl[1102].w";
	setAttr ".wl[1102].w[4]" 0.0066231214516934943;
	setAttr ".wl[1102].w[5]" 0.0014922951870509245;
	setAttr ".wl[1102].w[9]" 0.17184542457612126;
	setAttr ".wl[1102].w[10]" 0.74984305334718659;
	setAttr ".wl[1102].w[11]" 0.070196105437947839;
	setAttr -s 5 ".wl[1103].w";
	setAttr ".wl[1103].w[4]" 0.0067088643354984365;
	setAttr ".wl[1103].w[5]" 0.0014708892522484369;
	setAttr ".wl[1103].w[9]" 0.17321008670660276;
	setAttr ".wl[1103].w[10]" 0.74799071987233856;
	setAttr ".wl[1103].w[11]" 0.070619439833311759;
	setAttr -s 5 ".wl[1104].w";
	setAttr ".wl[1104].w[4]" 0.0059350987811891912;
	setAttr ".wl[1104].w[5]" 0.0013199615517934045;
	setAttr ".wl[1104].w[9]" 0.16200090998527347;
	setAttr ".wl[1104].w[10]" 0.76546283900585954;
	setAttr ".wl[1104].w[11]" 0.065281190675884365;
	setAttr -s 5 ".wl[1105].w";
	setAttr ".wl[1105].w[4]" 0.0052768212133946155;
	setAttr ".wl[1105].w[5]" 0.0012250935204926238;
	setAttr ".wl[1105].w[9]" 0.15120918075994091;
	setAttr ".wl[1105].w[10]" 0.78165516698512894;
	setAttr ".wl[1105].w[11]" 0.060633737521042945;
	setAttr -s 5 ".wl[1106].w";
	setAttr ".wl[1106].w[4]" 0.0050357689439468438;
	setAttr ".wl[1106].w[5]" 0.0012319082349238021;
	setAttr ".wl[1106].w[9]" 0.14705046640075109;
	setAttr ".wl[1106].w[10]" 0.78787724833773531;
	setAttr ".wl[1106].w[11]" 0.058804608082643053;
	setAttr -s 5 ".wl[1107].w";
	setAttr ".wl[1107].w[4]" 0.0047601404618012834;
	setAttr ".wl[1107].w[5]" 0.0012477869763417124;
	setAttr ".wl[1107].w[9]" 0.14281636500941183;
	setAttr ".wl[1107].w[10]" 0.79508917969276705;
	setAttr ".wl[1107].w[11]" 0.056086527859678173;
	setAttr -s 5 ".wl[1108].w";
	setAttr ".wl[1108].w[4]" 0.0047911342632904266;
	setAttr ".wl[1108].w[5]" 0.0013930441077854875;
	setAttr ".wl[1108].w[9]" 0.14430833337617657;
	setAttr ".wl[1108].w[10]" 0.79391876311361653;
	setAttr ".wl[1108].w[11]" 0.055588725139130886;
	setAttr -s 5 ".wl[1109].w";
	setAttr ".wl[1109].w[4]" 0.0049651980246656381;
	setAttr ".wl[1109].w[5]" 0.0016421786339488463;
	setAttr ".wl[1109].w[9]" 0.14799347504337176;
	setAttr ".wl[1109].w[10]" 0.78891026448145696;
	setAttr ".wl[1109].w[11]" 0.056488883816556791;
	setAttr -s 5 ".wl[1110].w";
	setAttr ".wl[1110].w[4]" 0.0045345560369780113;
	setAttr ".wl[1110].w[5]" 0.0017207895755508504;
	setAttr ".wl[1110].w[9]" 0.14039815285065793;
	setAttr ".wl[1110].w[10]" 0.80047373924796894;
	setAttr ".wl[1110].w[11]" 0.052872762288844369;
	setAttr -s 5 ".wl[1111].w";
	setAttr ".wl[1111].w[4]" 0.0043043186212647962;
	setAttr ".wl[1111].w[5]" 0.0018661399685625576;
	setAttr ".wl[1111].w[9]" 0.13575658278638977;
	setAttr ".wl[1111].w[10]" 0.80692918346002007;
	setAttr ".wl[1111].w[11]" 0.051143775163762797;
	setAttr -s 5 ".wl[1112].w";
	setAttr ".wl[1112].w[4]" 0.0044510709975142324;
	setAttr ".wl[1112].w[5]" 0.0021439905824464328;
	setAttr ".wl[1112].w[9]" 0.13841575958042326;
	setAttr ".wl[1112].w[10]" 0.80251650877545033;
	setAttr ".wl[1112].w[11]" 0.052472670064165637;
	setAttr -s 5 ".wl[1113].w";
	setAttr ".wl[1113].w[4]" 0.0046185516425879579;
	setAttr ".wl[1113].w[5]" 0.0024265376980458142;
	setAttr ".wl[1113].w[9]" 0.14194309608808622;
	setAttr ".wl[1113].w[10]" 0.7974821074578079;
	setAttr ".wl[1113].w[11]" 0.053529707113472103;
	setAttr -s 5 ".wl[1114].w";
	setAttr ".wl[1114].w[4]" 0.0052353628145998393;
	setAttr ".wl[1114].w[5]" 0.0029986244447416334;
	setAttr ".wl[1114].w[9]" 0.15339318142121902;
	setAttr ".wl[1114].w[10]" 0.78047909301853458;
	setAttr ".wl[1114].w[11]" 0.057893738300905009;
	setAttr -s 5 ".wl[1115].w";
	setAttr ".wl[1115].w[4]" 0.0037716054788959961;
	setAttr ".wl[1115].w[5]" 0.0024232566283703704;
	setAttr ".wl[1115].w[9]" 0.069966038112644652;
	setAttr ".wl[1115].w[10]" 0.76774833105635654;
	setAttr ".wl[1115].w[11]" 0.15609076872373237;
	setAttr -s 5 ".wl[1116].w";
	setAttr ".wl[1116].w[4]" 0.0038105361391494634;
	setAttr ".wl[1116].w[5]" 0.0023859834464745188;
	setAttr ".wl[1116].w[9]" 0.070327478366169593;
	setAttr ".wl[1116].w[10]" 0.76585868100484644;
	setAttr ".wl[1116].w[11]" 0.15761732104335999;
	setAttr -s 5 ".wl[1117].w";
	setAttr ".wl[1117].w[4]" 0.0038705935038333729;
	setAttr ".wl[1117].w[5]" 0.002284843314286562;
	setAttr ".wl[1117].w[9]" 0.070801067933783574;
	setAttr ".wl[1117].w[10]" 0.7627121666936274;
	setAttr ".wl[1117].w[11]" 0.16033132855446916;
	setAttr -s 5 ".wl[1118].w";
	setAttr ".wl[1118].w[4]" 0.0040853717648115942;
	setAttr ".wl[1118].w[5]" 0.0022633702639941072;
	setAttr ".wl[1118].w[9]" 0.073423723982584871;
	setAttr ".wl[1118].w[10]" 0.75499042903229541;
	setAttr ".wl[1118].w[11]" 0.16523710495631405;
	setAttr -s 5 ".wl[1119].w";
	setAttr ".wl[1119].w[4]" 0.0042220712885906297;
	setAttr ".wl[1119].w[5]" 0.0021566871885866034;
	setAttr ".wl[1119].w[9]" 0.075228705545460781;
	setAttr ".wl[1119].w[10]" 0.75092562703716181;
	setAttr ".wl[1119].w[11]" 0.16746690894020014;
	setAttr -s 5 ".wl[1120].w";
	setAttr ".wl[1120].w[4]" 0.0045836930545417563;
	setAttr ".wl[1120].w[5]" 0.0021043370163430172;
	setAttr ".wl[1120].w[9]" 0.07992016911425788;
	setAttr ".wl[1120].w[10]" 0.74060874789144349;
	setAttr ".wl[1120].w[11]" 0.17278305292341387;
	setAttr -s 5 ".wl[1121].w";
	setAttr ".wl[1121].w[4]" 0.0049367284894378657;
	setAttr ".wl[1121].w[5]" 0.0020196928760865961;
	setAttr ".wl[1121].w[9]" 0.084174593181105956;
	setAttr ".wl[1121].w[10]" 0.73066464130676589;
	setAttr ".wl[1121].w[11]" 0.17820434414660374;
	setAttr -s 5 ".wl[1122].w";
	setAttr ".wl[1122].w[4]" 0.0046108360990610997;
	setAttr ".wl[1122].w[5]" 0.0016840372961927806;
	setAttr ".wl[1122].w[9]" 0.080004405137359846;
	setAttr ".wl[1122].w[10]" 0.73932811220842676;
	setAttr ".wl[1122].w[11]" 0.17437260925895959;
	setAttr -s 5 ".wl[1123].w";
	setAttr ".wl[1123].w[4]" 0.0042878544023984789;
	setAttr ".wl[1123].w[5]" 0.0014235565652317994;
	setAttr ".wl[1123].w[9]" 0.075610602736540578;
	setAttr ".wl[1123].w[10]" 0.74787728459947511;
	setAttr ".wl[1123].w[11]" 0.17080070169635411;
	setAttr -s 5 ".wl[1124].w";
	setAttr ".wl[1124].w[4]" 0.0041952911918662428;
	setAttr ".wl[1124].w[5]" 0.0013032475352300166;
	setAttr ".wl[1124].w[9]" 0.074228397935773655;
	setAttr ".wl[1124].w[10]" 0.75009334070494471;
	setAttr ".wl[1124].w[11]" 0.17017972263218542;
	setAttr -s 5 ".wl[1125].w";
	setAttr ".wl[1125].w[4]" 0.0040357219168330113;
	setAttr ".wl[1125].w[5]" 0.0011888927191660711;
	setAttr ".wl[1125].w[9]" 0.072245723484300334;
	setAttr ".wl[1125].w[10]" 0.75550271612171505;
	setAttr ".wl[1125].w[11]" 0.1670269457579856;
	setAttr -s 5 ".wl[1126].w";
	setAttr ".wl[1126].w[4]" 0.0040552134963952229;
	setAttr ".wl[1126].w[5]" 0.0011387072563653153;
	setAttr ".wl[1126].w[9]" 0.072699238920122353;
	setAttr ".wl[1126].w[10]" 0.75565889343217352;
	setAttr ".wl[1126].w[11]" 0.16644794689494358;
	setAttr -s 5 ".wl[1127].w";
	setAttr ".wl[1127].w[4]" 0.0040971009526949145;
	setAttr ".wl[1127].w[5]" 0.001123063623140356;
	setAttr ".wl[1127].w[9]" 0.073325465741796866;
	setAttr ".wl[1127].w[10]" 0.75463770891281523;
	setAttr ".wl[1127].w[11]" 0.16681666076955259;
	setAttr -s 5 ".wl[1128].w";
	setAttr ".wl[1128].w[4]" 0.0035964362971772626;
	setAttr ".wl[1128].w[5]" 0.0009992526154307508;
	setAttr ".wl[1128].w[9]" 0.066575167319813749;
	setAttr ".wl[1128].w[10]" 0.77116150786012105;
	setAttr ".wl[1128].w[11]" 0.15766763590745717;
	setAttr -s 5 ".wl[1129].w";
	setAttr ".wl[1129].w[4]" 0.0031756551780724148;
	setAttr ".wl[1129].w[5]" 0.0009168337719960214;
	setAttr ".wl[1129].w[9]" 0.060482991681602073;
	setAttr ".wl[1129].w[10]" 0.78595371956960614;
	setAttr ".wl[1129].w[11]" 0.14947079979872338;
	setAttr -s 5 ".wl[1130].w";
	setAttr ".wl[1130].w[4]" 0.0030228367581135026;
	setAttr ".wl[1130].w[5]" 0.00091361184514935766;
	setAttr ".wl[1130].w[9]" 0.058209433034790939;
	setAttr ".wl[1130].w[10]" 0.79171442135861514;
	setAttr ".wl[1130].w[11]" 0.14613969700333115;
	setAttr -s 5 ".wl[1131].w";
	setAttr ".wl[1131].w[4]" 0.0028541574597798938;
	setAttr ".wl[1131].w[5]" 0.00091632243334189276;
	setAttr ".wl[1131].w[9]" 0.055879481873312521;
	setAttr ".wl[1131].w[10]" 0.79936619866959735;
	setAttr ".wl[1131].w[11]" 0.14098383956396832;
	setAttr -s 5 ".wl[1132].w";
	setAttr ".wl[1132].w[4]" 0.0028812077587151192;
	setAttr ".wl[1132].w[5]" 0.0010114687674058298;
	setAttr ".wl[1132].w[9]" 0.056602345625707484;
	setAttr ".wl[1132].w[10]" 0.7995904583327953;
	setAttr ".wl[1132].w[11]" 0.13991451951537631;
	setAttr -s 5 ".wl[1133].w";
	setAttr ".wl[1133].w[4]" 0.0030008152732764908;
	setAttr ".wl[1133].w[5]" 0.0011758062688381431;
	setAttr ".wl[1133].w[9]" 0.058613103075438336;
	setAttr ".wl[1133].w[10]" 0.79556160719058466;
	setAttr ".wl[1133].w[11]" 0.14164866819186231;
	setAttr -s 5 ".wl[1134].w";
	setAttr ".wl[1134].w[4]" 0.0027439627602480927;
	setAttr ".wl[1134].w[5]" 0.0012082335693458163;
	setAttr ".wl[1134].w[9]" 0.054742429329337594;
	setAttr ".wl[1134].w[10]" 0.80618588922668888;
	setAttr ".wl[1134].w[11]" 0.13511948511437966;
	setAttr -s 5 ".wl[1135].w";
	setAttr ".wl[1135].w[4]" 0.0026126168886262064;
	setAttr ".wl[1135].w[5]" 0.0012860376380317059;
	setAttr ".wl[1135].w[9]" 0.052589619770370952;
	setAttr ".wl[1135].w[10]" 0.81126488749363657;
	setAttr ".wl[1135].w[11]" 0.13224683820933469;
	setAttr -s 5 ".wl[1136].w";
	setAttr ".wl[1136].w[4]" 0.002715726292095582;
	setAttr ".wl[1136].w[5]" 0.0014581459432751456;
	setAttr ".wl[1136].w[9]" 0.054157800647598196;
	setAttr ".wl[1136].w[10]" 0.80649434190347535;
	setAttr ".wl[1136].w[11]" 0.13517398521355581;
	setAttr -s 5 ".wl[1137].w";
	setAttr ".wl[1137].w[4]" 0.002834192432226568;
	setAttr ".wl[1137].w[5]" 0.0016339377602892381;
	setAttr ".wl[1137].w[9]" 0.056121407638462746;
	setAttr ".wl[1137].w[10]" 0.80202014221243245;
	setAttr ".wl[1137].w[11]" 0.13739031995658901;
	setAttr -s 5 ".wl[1138].w";
	setAttr ".wl[1138].w[4]" 0.0032410071918153693;
	setAttr ".wl[1138].w[5]" 0.0020037615564505507;
	setAttr ".wl[1138].w[9]" 0.06241863385094356;
	setAttr ".wl[1138].w[10]" 0.78668124939188711;
	setAttr ".wl[1138].w[11]" 0.14565534800890351;
	setAttr -s 5 ".wl[1139].w";
	setAttr ".wl[1139].w[4]" 0.0020647184920080329;
	setAttr ".wl[1139].w[5]" 0.0013981103003786982;
	setAttr ".wl[1139].w[9]" 0.027062503557862716;
	setAttr ".wl[1139].w[10]" 0.64620421478177625;
	setAttr ".wl[1139].w[11]" 0.32327045286797434;
	setAttr -s 5 ".wl[1140].w";
	setAttr ".wl[1140].w[4]" 0.0020908444604708638;
	setAttr ".wl[1140].w[5]" 0.0013860893276851723;
	setAttr ".wl[1140].w[9]" 0.027296032884401307;
	setAttr ".wl[1140].w[10]" 0.64417425260700123;
	setAttr ".wl[1140].w[11]" 0.32505278072044147;
	setAttr -s 5 ".wl[1141].w";
	setAttr ".wl[1141].w[4]" 0.0021258498716731104;
	setAttr ".wl[1141].w[5]" 0.0013422353634864894;
	setAttr ".wl[1141].w[9]" 0.027572297010384737;
	setAttr ".wl[1141].w[10]" 0.64089120785893472;
	setAttr ".wl[1141].w[11]" 0.328068409895521;
	setAttr -s 5 ".wl[1142].w";
	setAttr ".wl[1142].w[4]" 0.002248795079623112;
	setAttr ".wl[1142].w[5]" 0.0013467440748869697;
	setAttr ".wl[1142].w[9]" 0.028832845509974947;
	setAttr ".wl[1142].w[10]" 0.63520262744013212;
	setAttr ".wl[1142].w[11]" 0.33236898789538288;
	setAttr -s 5 ".wl[1143].w";
	setAttr ".wl[1143].w[4]" 0.0023267620511236474;
	setAttr ".wl[1143].w[5]" 0.0013018514712057083;
	setAttr ".wl[1143].w[9]" 0.02967683563111595;
	setAttr ".wl[1143].w[10]" 0.632742259263679;
	setAttr ".wl[1143].w[11]" 0.33395229158287565;
	setAttr -s 5 ".wl[1144].w";
	setAttr ".wl[1144].w[4]" 0.0025345108304514834;
	setAttr ".wl[1144].w[5]" 0.0012957130867595455;
	setAttr ".wl[1144].w[9]" 0.031906868107870502;
	setAttr ".wl[1144].w[10]" 0.62676105857441133;
	setAttr ".wl[1144].w[11]" 0.33750184940050709;
	setAttr -s 5 ".wl[1145].w";
	setAttr ".wl[1145].w[4]" 0.0027358127302491149;
	setAttr ".wl[1145].w[5]" 0.0012675718410899623;
	setAttr ".wl[1145].w[9]" 0.033972226143059267;
	setAttr ".wl[1145].w[10]" 0.62075512875335181;
	setAttr ".wl[1145].w[11]" 0.34126926053224993;
	setAttr -s 5 ".wl[1146].w";
	setAttr ".wl[1146].w[4]" 0.0025351184852217762;
	setAttr ".wl[1146].w[5]" 0.0010658704701576554;
	setAttr ".wl[1146].w[9]" 0.031834039034664974;
	setAttr ".wl[1146].w[10]" 0.62570070999895289;
	setAttr ".wl[1146].w[11]" 0.33886426201100273;
	setAttr -s 5 ".wl[1147].w";
	setAttr ".wl[1147].w[4]" 0.0023370482973778866;
	setAttr ".wl[1147].w[5]" 0.00090499495879337499;
	setAttr ".wl[1147].w[9]" 0.029643851498266375;
	setAttr ".wl[1147].w[10]" 0.63029906738071617;
	setAttr ".wl[1147].w[11]" 0.33681503786484623;
	setAttr -s 5 ".wl[1148].w";
	setAttr ".wl[1148].w[4]" 0.0022753259424696175;
	setAttr ".wl[1148].w[5]" 0.00083162338790653478;
	setAttr ".wl[1148].w[9]" 0.028926571057088207;
	setAttr ".wl[1148].w[10]" 0.63136229136946476;
	setAttr ".wl[1148].w[11]" 0.33660418824307092;
	setAttr -s 5 ".wl[1149].w";
	setAttr ".wl[1149].w[4]" 0.0021769249844667622;
	setAttr ".wl[1149].w[5]" 0.00075981248885562363;
	setAttr ".wl[1149].w[9]" 0.027900446151031918;
	setAttr ".wl[1149].w[10]" 0.63530121895057612;
	setAttr ".wl[1149].w[11]" 0.33386159742506971;
	setAttr -s 5 ".wl[1150].w";
	setAttr ".wl[1150].w[4]" 0.0021804601511015227;
	setAttr ".wl[1150].w[5]" 0.00072976047241458562;
	setAttr ".wl[1150].w[9]" 0.028003252276627144;
	setAttr ".wl[1150].w[10]" 0.636354398782914;
	setAttr ".wl[1150].w[11]" 0.33273212831694288;
	setAttr -s 5 ".wl[1151].w";
	setAttr ".wl[1151].w[4]" 0.0021969629090021715;
	setAttr ".wl[1151].w[5]" 0.00071981603441149261;
	setAttr ".wl[1151].w[9]" 0.028208626412677934;
	setAttr ".wl[1151].w[10]" 0.63626032065211158;
	setAttr ".wl[1151].w[11]" 0.33261427399179683;
	setAttr -s 5 ".wl[1152].w";
	setAttr ".wl[1152].w[4]" 0.0019086204715137566;
	setAttr ".wl[1152].w[5]" 0.00063321245402209831;
	setAttr ".wl[1152].w[9]" 0.025034602030603519;
	setAttr ".wl[1152].w[10]" 0.6471564637841819;
	setAttr ".wl[1152].w[11]" 0.32526710125967867;
	setAttr -s 5 ".wl[1153].w";
	setAttr ".wl[1153].w[4]" 0.0016711524516540544;
	setAttr ".wl[1153].w[5]" 0.00057364315487599884;
	setAttr ".wl[1153].w[9]" 0.022305533397653125;
	setAttr ".wl[1153].w[10]" 0.65687881198866316;
	setAttr ".wl[1153].w[11]" 0.31857085900715365;
	setAttr -s 5 ".wl[1154].w";
	setAttr ".wl[1154].w[4]" 0.0015859942720639149;
	setAttr ".wl[1154].w[5]" 0.00056671051231430567;
	setAttr ".wl[1154].w[9]" 0.021312080419128337;
	setAttr ".wl[1154].w[10]" 0.660849448189055;
	setAttr ".wl[1154].w[11]" 0.31568576660743836;
	setAttr -s 5 ".wl[1155].w";
	setAttr ".wl[1155].w[4]" 0.0014955289196423209;
	setAttr ".wl[1155].w[5]" 0.00056337721966808819;
	setAttr ".wl[1155].w[9]" 0.020311042574820196;
	setAttr ".wl[1155].w[10]" 0.66717308179142676;
	setAttr ".wl[1155].w[11]" 0.31045696949444268;
	setAttr -s 5 ".wl[1156].w";
	setAttr ".wl[1156].w[4]" 0.0015145370071381558;
	setAttr ".wl[1156].w[5]" 0.00061644483822798535;
	setAttr ".wl[1156].w[9]" 0.020625983935579521;
	setAttr ".wl[1156].w[10]" 0.66858101799193503;
	setAttr ".wl[1156].w[11]" 0.30866201622711936;
	setAttr -s 5 ".wl[1157].w";
	setAttr ".wl[1157].w[4]" 0.0015858609087331338;
	setAttr ".wl[1157].w[5]" 0.00070931027997852007;
	setAttr ".wl[1157].w[9]" 0.02152456825914114;
	setAttr ".wl[1157].w[10]" 0.66628272867847715;
	setAttr ".wl[1157].w[11]" 0.30989753187367008;
	setAttr -s 5 ".wl[1158].w";
	setAttr ".wl[1158].w[4]" 0.0014521892875229549;
	setAttr ".wl[1158].w[5]" 0.00071761566645985291;
	setAttr ".wl[1158].w[9]" 0.019955267926137301;
	setAttr ".wl[1158].w[10]" 0.67394598990489307;
	setAttr ".wl[1158].w[11]" 0.30392893721498682;
	setAttr -s 5 ".wl[1159].w";
	setAttr ".wl[1159].w[4]" 0.0013872657202192082;
	setAttr ".wl[1159].w[5]" 0.00075323474345627742;
	setAttr ".wl[1159].w[9]" 0.019146663117129044;
	setAttr ".wl[1159].w[10]" 0.67689113693065039;
	setAttr ".wl[1159].w[11]" 0.30182169948854515;
	setAttr -s 5 ".wl[1160].w";
	setAttr ".wl[1160].w[4]" 0.0014495656311884807;
	setAttr ".wl[1160].w[5]" 0.00084637900526365493;
	setAttr ".wl[1160].w[9]" 0.019878158536634202;
	setAttr ".wl[1160].w[10]" 0.6727088618329593;
	setAttr ".wl[1160].w[11]" 0.30511703499395437;
	setAttr -s 5 ".wl[1161].w";
	setAttr ".wl[1161].w[4]" 0.0015218821509765076;
	setAttr ".wl[1161].w[5]" 0.00094272137012472469;
	setAttr ".wl[1161].w[9]" 0.020775218064427128;
	setAttr ".wl[1161].w[10]" 0.66964429350007271;
	setAttr ".wl[1161].w[11]" 0.30711588491439895;
	setAttr -s 5 ".wl[1162].w";
	setAttr ".wl[1162].w[4]" 0.0017570057361809307;
	setAttr ".wl[1162].w[5]" 0.0011529120867907596;
	setAttr ".wl[1162].w[9]" 0.023578293622024619;
	setAttr ".wl[1162].w[10]" 0.65917213692055043;
	setAttr ".wl[1162].w[11]" 0.31433965163445327;
	setAttr -s 5 ".wl[1163].w";
	setAttr ".wl[1163].w[4]" 0.0012389304703085394;
	setAttr ".wl[1163].w[5]" 0.00054942387638531149;
	setAttr ".wl[1163].w[12]" 0.015567024796259789;
	setAttr ".wl[1163].w[13]" 0.67100878978428935;
	setAttr ".wl[1163].w[14]" 0.31163583107275711;
	setAttr -s 5 ".wl[1164].w";
	setAttr ".wl[1164].w[4]" 0.0009503355580552207;
	setAttr ".wl[1164].w[5]" 0.00045675967537722869;
	setAttr ".wl[1164].w[12]" 0.012207207150467731;
	setAttr ".wl[1164].w[13]" 0.68920437103355547;
	setAttr ".wl[1164].w[14]" 0.29718132658254437;
	setAttr -s 5 ".wl[1165].w";
	setAttr ".wl[1165].w[4]" 0.001353412368492778;
	setAttr ".wl[1165].w[5]" 0.00061531865157177744;
	setAttr ".wl[1165].w[12]" 0.020982025021701309;
	setAttr ".wl[1165].w[13]" 0.78683111687225937;
	setAttr ".wl[1165].w[14]" 0.19021812708597469;
	setAttr -s 5 ".wl[1166].w";
	setAttr ".wl[1166].w[4]" 0.0017381587556713413;
	setAttr ".wl[1166].w[5]" 0.0007240780825698152;
	setAttr ".wl[1166].w[12]" 0.026218370846828459;
	setAttr ".wl[1166].w[13]" 0.7641316020087443;
	setAttr ".wl[1166].w[14]" 0.20718779030618603;
	setAttr -s 5 ".wl[1167].w";
	setAttr ".wl[1167].w[4]" 0.005785890160589109;
	setAttr ".wl[1167].w[5]" 0.0027912333049944335;
	setAttr ".wl[1167].w[12]" 0.059359450895304512;
	setAttr ".wl[1167].w[13]" 0.57979174854793003;
	setAttr ".wl[1167].w[14]" 0.35227167709118201;
	setAttr -s 5 ".wl[1168].w";
	setAttr ".wl[1168].w[4]" 0.0049975783029755942;
	setAttr ".wl[1168].w[5]" 0.0013702941634485559;
	setAttr ".wl[1168].w[12]" 0.084838236786476215;
	setAttr ".wl[1168].w[13]" 0.75635490996535393;
	setAttr ".wl[1168].w[14]" 0.15243898078174575;
	setAttr -s 5 ".wl[1169].w";
	setAttr ".wl[1169].w[4]" 0.0028538184613235225;
	setAttr ".wl[1169].w[5]" 0.0008676631487351896;
	setAttr ".wl[1169].w[12]" 0.045185060072902614;
	setAttr ".wl[1169].w[13]" 0.76525541937879549;
	setAttr ".wl[1169].w[14]" 0.18583803893824327;
	setAttr -s 5 ".wl[1170].w";
	setAttr ".wl[1170].w[4]" 0.0024454534994047066;
	setAttr ".wl[1170].w[5]" 0.00072352148639207394;
	setAttr ".wl[1170].w[12]" 0.046091545266056237;
	setAttr ".wl[1170].w[13]" 0.82669987666144429;
	setAttr ".wl[1170].w[14]" 0.1240396030867026;
	setAttr -s 5 ".wl[1171].w";
	setAttr ".wl[1171].w[4]" 0.0041593792405021732;
	setAttr ".wl[1171].w[5]" 0.001099262988551949;
	setAttr ".wl[1171].w[12]" 0.086783945476247917;
	setAttr ".wl[1171].w[13]" 0.80802478436234837;
	setAttr ".wl[1171].w[14]" 0.099932627932349652;
	setAttr -s 5 ".wl[1172].w";
	setAttr ".wl[1172].w[3]" 0.0029803425214004081;
	setAttr ".wl[1172].w[4]" 0.010201375268923635;
	setAttr ".wl[1172].w[12]" 0.11757442443794933;
	setAttr ".wl[1172].w[13]" 0.61778650516208433;
	setAttr ".wl[1172].w[14]" 0.25145735260964225;
	setAttr -s 5 ".wl[1173].w";
	setAttr ".wl[1173].w[4]" 0.0025967799755731661;
	setAttr ".wl[1173].w[5]" 0.0010056739869557089;
	setAttr ".wl[1173].w[12]" 0.038528886278031471;
	setAttr ".wl[1173].w[13]" 0.74459200236159284;
	setAttr ".wl[1173].w[14]" 0.21327665739784682;
	setAttr -s 5 ".wl[1174].w";
	setAttr ".wl[1174].w[4]" 0.0018259486200515426;
	setAttr ".wl[1174].w[5]" 0.00074932556853599705;
	setAttr ".wl[1174].w[12]" 0.022915437212614544;
	setAttr ".wl[1174].w[13]" 0.66862873737990924;
	setAttr ".wl[1174].w[14]" 0.30588055121888869;
	setAttr -s 5 ".wl[1175].w";
	setAttr ".wl[1175].w[4]" 0.0016432546227088598;
	setAttr ".wl[1175].w[5]" 0.00062618821123086349;
	setAttr ".wl[1175].w[12]" 0.021803818355751004;
	setAttr ".wl[1175].w[13]" 0.70072836781078518;
	setAttr ".wl[1175].w[14]" 0.27519837099952399;
	setAttr -s 5 ".wl[1176].w";
	setAttr ".wl[1176].w[4]" 0.0022471575289421219;
	setAttr ".wl[1176].w[5]" 0.00080171646626981817;
	setAttr ".wl[1176].w[12]" 0.035865566240540499;
	setAttr ".wl[1176].w[13]" 0.77950717485921861;
	setAttr ".wl[1176].w[14]" 0.18157838490502889;
	setAttr -s 5 ".wl[1177].w";
	setAttr ".wl[1177].w[4]" 0.0080665581717600399;
	setAttr ".wl[1177].w[5]" 0.0029008513225254277;
	setAttr ".wl[1177].w[12]" 0.081397670300440622;
	setAttr ".wl[1177].w[13]" 0.5786435767855761;
	setAttr ".wl[1177].w[14]" 0.32899134341969788;
	setAttr -s 5 ".wl[1178].w";
	setAttr ".wl[1178].w[4]" 0.00060946070305908451;
	setAttr ".wl[1178].w[5]" 0.00036171541941831651;
	setAttr ".wl[1178].w[12]" 0.0089459097409288445;
	setAttr ".wl[1178].w[13]" 0.79209106642180505;
	setAttr ".wl[1178].w[14]" 0.19799184771478875;
	setAttr -s 5 ".wl[1179].w";
	setAttr ".wl[1179].w[4]" 0.00072515631403348034;
	setAttr ".wl[1179].w[5]" 0.00043790358476186459;
	setAttr ".wl[1179].w[12]" 0.010813844100969503;
	setAttr ".wl[1179].w[13]" 0.79423719450724672;
	setAttr ".wl[1179].w[14]" 0.19378590149298841;
	setAttr -s 5 ".wl[1180].w";
	setAttr ".wl[1180].w[4]" 0.0012088110595163397;
	setAttr ".wl[1180].w[5]" 0.0007178577778560411;
	setAttr ".wl[1180].w[12]" 0.021643002124144311;
	setAttr ".wl[1180].w[13]" 0.8507595952720276;
	setAttr ".wl[1180].w[14]" 0.12567073376645571;
	setAttr -s 5 ".wl[1181].w";
	setAttr ".wl[1181].w[4]" 0.0010888804184423341;
	setAttr ".wl[1181].w[5]" 0.00063453492171606032;
	setAttr ".wl[1181].w[12]" 0.019065548800869792;
	setAttr ".wl[1181].w[13]" 0.84813914514734701;
	setAttr ".wl[1181].w[14]" 0.13107189071162481;
	setAttr -s 5 ".wl[1182].w";
	setAttr ".wl[1182].w[4]" 0.0042300496684878662;
	setAttr ".wl[1182].w[5]" 0.0032392273337880297;
	setAttr ".wl[1182].w[12]" 0.046838328900804908;
	setAttr ".wl[1182].w[13]" 0.60816230040943431;
	setAttr ".wl[1182].w[14]" 0.33753009368748499;
	setAttr -s 5 ".wl[1183].w";
	setAttr ".wl[1183].w[4]" 0.00082424317797300071;
	setAttr ".wl[1183].w[5]" 0.00045425753866773272;
	setAttr ".wl[1183].w[12]" 0.012270915767805247;
	setAttr ".wl[1183].w[13]" 0.78826551814776635;
	setAttr ".wl[1183].w[14]" 0.19818506536778763;
	setAttr -s 5 ".wl[1184].w";
	setAttr ".wl[1184].w[4]" 0.00072512925258192708;
	setAttr ".wl[1184].w[5]" 0.00039468966140249098;
	setAttr ".wl[1184].w[12]" 0.009313143702341169;
	setAttr ".wl[1184].w[13]" 0.69690809548772892;
	setAttr ".wl[1184].w[14]" 0.2926589418959456;
	setAttr -s 5 ".wl[1185].w";
	setAttr ".wl[1185].w[4]" 0.0012457676736111951;
	setAttr ".wl[1185].w[5]" 0.00062848790942971291;
	setAttr ".wl[1185].w[12]" 0.017643694874758097;
	setAttr ".wl[1185].w[13]" 0.74514987739992689;
	setAttr ".wl[1185].w[14]" 0.23533217214227409;
	setAttr -s 5 ".wl[1186].w";
	setAttr ".wl[1186].w[4]" 0.0012051803316120106;
	setAttr ".wl[1186].w[5]" 0.00061168327994082011;
	setAttr ".wl[1186].w[12]" 0.020416217100840121;
	setAttr ".wl[1186].w[13]" 0.83170308302818097;
	setAttr ".wl[1186].w[14]" 0.14606383625942618;
	setAttr -s 5 ".wl[1187].w";
	setAttr ".wl[1187].w[4]" 0.0049362566819347005;
	setAttr ".wl[1187].w[5]" 0.0031142048175871206;
	setAttr ".wl[1187].w[12]" 0.053327897103745366;
	setAttr ".wl[1187].w[13]" 0.60006901012159874;
	setAttr ".wl[1187].w[14]" 0.33855263127513396;
	setAttr -s 5 ".wl[1188].w";
	setAttr ".wl[1188].w[3]" 0.0013711801803807154;
	setAttr ".wl[1188].w[4]" 0.011094774122055057;
	setAttr ".wl[1188].w[12]" 0.49976386428078778;
	setAttr ".wl[1188].w[13]" 0.48451950666400517;
	setAttr ".wl[1188].w[14]" 0.0032506747527711781;
	setAttr -s 5 ".wl[1189].w";
	setAttr ".wl[1189].w[4]" 0.0069842014401521055;
	setAttr ".wl[1189].w[5]" 0.00086584734071871723;
	setAttr ".wl[1189].w[12]" 0.49585092581767598;
	setAttr ".wl[1189].w[13]" 0.49382278778577432;
	setAttr ".wl[1189].w[14]" 0.0024762376156788127;
	setAttr -s 5 ".wl[1190].w";
	setAttr ".wl[1190].w[3]" 0.0012297158588629859;
	setAttr ".wl[1190].w[4]" 0.01181262357049595;
	setAttr ".wl[1190].w[12]" 0.54885375053480356;
	setAttr ".wl[1190].w[13]" 0.4360498731164873;
	setAttr ".wl[1190].w[14]" 0.0020540369193501199;
	setAttr -s 5 ".wl[1191].w";
	setAttr ".wl[1191].w[3]" 0.001851591050256415;
	setAttr ".wl[1191].w[4]" 0.017654946904779981;
	setAttr ".wl[1191].w[12]" 0.56445545471331893;
	setAttr ".wl[1191].w[13]" 0.41349857438745125;
	setAttr ".wl[1191].w[14]" 0.0025394329441933939;
	setAttr -s 5 ".wl[1192].w";
	setAttr ".wl[1192].w[3]" 0.0078707526606581289;
	setAttr ".wl[1192].w[4]" 0.044811114042674183;
	setAttr ".wl[1192].w[12]" 0.4737846235134695;
	setAttr ".wl[1192].w[13]" 0.46006769330294539;
	setAttr ".wl[1192].w[14]" 0.013465816480252829;
	setAttr -s 5 ".wl[1193].w";
	setAttr ".wl[1193].w[4]" 0.0073780378612818536;
	setAttr ".wl[1193].w[5]" 0.0015372012635204604;
	setAttr ".wl[1193].w[12]" 0.24655450594081321;
	setAttr ".wl[1193].w[13]" 0.71194196583609159;
	setAttr ".wl[1193].w[14]" 0.032588289098292963;
	setAttr -s 5 ".wl[1194].w";
	setAttr ".wl[1194].w[4]" 0.0066563066297779007;
	setAttr ".wl[1194].w[5]" 0.0015075038715352128;
	setAttr ".wl[1194].w[12]" 0.18254045775715541;
	setAttr ".wl[1194].w[13]" 0.7573039971590545;
	setAttr ".wl[1194].w[14]" 0.05199173458247703;
	setAttr -s 5 ".wl[1195].w";
	setAttr ".wl[1195].w[4]" 0.0083499776241426366;
	setAttr ".wl[1195].w[5]" 0.001689305227131501;
	setAttr ".wl[1195].w[12]" 0.23826087099946738;
	setAttr ".wl[1195].w[13]" 0.70845613601331303;
	setAttr ".wl[1195].w[14]" 0.04324371013594544;
	setAttr -s 5 ".wl[1196].w";
	setAttr ".wl[1196].w[4]" 0.0084801636699989129;
	setAttr ".wl[1196].w[5]" 0.0015516751509097974;
	setAttr ".wl[1196].w[12]" 0.30342119570342169;
	setAttr ".wl[1196].w[13]" 0.66125784262894893;
	setAttr ".wl[1196].w[14]" 0.025289122846720653;
	setAttr -s 5 ".wl[1197].w";
	setAttr ".wl[1197].w[3]" 0.0055248784016269509;
	setAttr ".wl[1197].w[4]" 0.024868443174508426;
	setAttr ".wl[1197].w[12]" 0.32987459103985839;
	setAttr ".wl[1197].w[13]" 0.55392819703417573;
	setAttr ".wl[1197].w[14]" 0.085803890349830478;
	setAttr -s 5 ".wl[1198].w";
	setAttr ".wl[1198].w[4]" 0.016067839334301014;
	setAttr ".wl[1198].w[5]" 0.0031218555302578916;
	setAttr ".wl[1198].w[12]" 0.49121061120570847;
	setAttr ".wl[1198].w[13]" 0.48428651703935083;
	setAttr ".wl[1198].w[14]" 0.0053131768903817694;
	setAttr -s 5 ".wl[1199].w";
	setAttr ".wl[1199].w[4]" 0.012761936642949282;
	setAttr ".wl[1199].w[5]" 0.0023396015654703357;
	setAttr ".wl[1199].w[12]" 0.48780710347008283;
	setAttr ".wl[1199].w[13]" 0.49095357039261311;
	setAttr ".wl[1199].w[14]" 0.0061377879288844325;
	setAttr -s 5 ".wl[1200].w";
	setAttr ".wl[1200].w[4]" 0.022538911735301009;
	setAttr ".wl[1200].w[5]" 0.0033330843919938341;
	setAttr ".wl[1200].w[12]" 0.49412571752599971;
	setAttr ".wl[1200].w[13]" 0.4737392595275548;
	setAttr ".wl[1200].w[14]" 0.0062630268191505873;
	setAttr -s 5 ".wl[1201].w";
	setAttr ".wl[1201].w[4]" 0.02753259500030503;
	setAttr ".wl[1201].w[5]" 0.0042144883332140576;
	setAttr ".wl[1201].w[12]" 0.51756216168107883;
	setAttr ".wl[1201].w[13]" 0.44546782389416084;
	setAttr ".wl[1201].w[14]" 0.0052229310912412499;
	setAttr -s 5 ".wl[1202].w";
	setAttr ".wl[1202].w[4]" 0.050660065564767223;
	setAttr ".wl[1202].w[5]" 0.01243053822847409;
	setAttr ".wl[1202].w[12]" 0.45653406905334087;
	setAttr ".wl[1202].w[13]" 0.45649626303319901;
	setAttr ".wl[1202].w[14]" 0.023879064120218774;
	setAttr -s 5 ".wl[1203].w";
	setAttr ".wl[1203].w[4]" 0.0041894908159632021;
	setAttr ".wl[1203].w[5]" 0.0018018342469230885;
	setAttr ".wl[1203].w[12]" 0.2817919653648488;
	setAttr ".wl[1203].w[13]" 0.70231305597215987;
	setAttr ".wl[1203].w[14]" 0.0099036536001050612;
	setAttr -s 5 ".wl[1204].w";
	setAttr ".wl[1204].w[4]" 0.0042091239983687821;
	setAttr ".wl[1204].w[5]" 0.0018435737661319602;
	setAttr ".wl[1204].w[12]" 0.21421925665984107;
	setAttr ".wl[1204].w[13]" 0.76313107075704323;
	setAttr ".wl[1204].w[14]" 0.01659697481861503;
	setAttr -s 5 ".wl[1205].w";
	setAttr ".wl[1205].w[4]" 0.0061264495103855636;
	setAttr ".wl[1205].w[5]" 0.0024339612391932131;
	setAttr ".wl[1205].w[12]" 0.31302214310834975;
	setAttr ".wl[1205].w[13]" 0.66423843734177579;
	setAttr ".wl[1205].w[14]" 0.014179008800295755;
	setAttr -s 5 ".wl[1206].w";
	setAttr ".wl[1206].w[4]" 0.0053816811681526699;
	setAttr ".wl[1206].w[5]" 0.0020688976459587657;
	setAttr ".wl[1206].w[12]" 0.37730787024970674;
	setAttr ".wl[1206].w[13]" 0.60768236052855551;
	setAttr ".wl[1206].w[14]" 0.0075591904076262751;
	setAttr -s 5 ".wl[1207].w";
	setAttr ".wl[1207].w[4]" 0.02060185979099784;
	setAttr ".wl[1207].w[5]" 0.013476796250187549;
	setAttr ".wl[1207].w[12]" 0.37747642539639903;
	setAttr ".wl[1207].w[13]" 0.54567089291960824;
	setAttr ".wl[1207].w[14]" 0.042774025642807453;
	setAttr -s 5 ".wl[1208].w";
	setAttr ".wl[1208].w[4]" 0.0041761958376623576;
	setAttr ".wl[1208].w[5]" 0.0012445005207304479;
	setAttr ".wl[1208].w[12]" 0.43786350402565027;
	setAttr ".wl[1208].w[13]" 0.55311891554397385;
	setAttr ".wl[1208].w[14]" 0.0035968840719830747;
	setAttr -s 5 ".wl[1209].w";
	setAttr ".wl[1209].w[4]" 0.0059145839871028736;
	setAttr ".wl[1209].w[5]" 0.0020207620038215249;
	setAttr ".wl[1209].w[12]" 0.47133090327931881;
	setAttr ".wl[1209].w[13]" 0.51679276013887843;
	setAttr ".wl[1209].w[14]" 0.003940990590878447;
	setAttr -s 5 ".wl[1210].w";
	setAttr ".wl[1210].w[4]" 0.007269844469150512;
	setAttr ".wl[1210].w[5]" 0.0020092241895658404;
	setAttr ".wl[1210].w[12]" 0.49686238433540919;
	setAttr ".wl[1210].w[13]" 0.49143261961034251;
	setAttr ".wl[1210].w[14]" 0.0024259273955319878;
	setAttr -s 5 ".wl[1211].w";
	setAttr ".wl[1211].w[4]" 0.0053294918232337802;
	setAttr ".wl[1211].w[5]" 0.0012860103770246437;
	setAttr ".wl[1211].w[12]" 0.49472138631673429;
	setAttr ".wl[1211].w[13]" 0.4963647607671216;
	setAttr ".wl[1211].w[14]" 0.0022983507158855195;
	setAttr -s 5 ".wl[1212].w";
	setAttr ".wl[1212].w[4]" 0.028481814115861506;
	setAttr ".wl[1212].w[5]" 0.01222750418620653;
	setAttr ".wl[1212].w[12]" 0.46795851879623646;
	setAttr ".wl[1212].w[13]" 0.4744572044780821;
	setAttr ".wl[1212].w[14]" 0.01687495842361339;
	setAttr -s 5 ".wl[1213].w";
	setAttr ".wl[1213].w[3]" 0.0058645633160357459;
	setAttr ".wl[1213].w[4]" 0.16159240674722383;
	setAttr ".wl[1213].w[5]" 0.0037956335686168069;
	setAttr ".wl[1213].w[12]" 0.78181006635181438;
	setAttr ".wl[1213].w[13]" 0.04693733001630912;
	setAttr -s 5 ".wl[1214].w";
	setAttr ".wl[1214].w[3]" 0.0050573073376967408;
	setAttr ".wl[1214].w[4]" 0.1804782128015269;
	setAttr ".wl[1214].w[5]" 0.0042708336889381963;
	setAttr ".wl[1214].w[12]" 0.77844967691203015;
	setAttr ".wl[1214].w[13]" 0.031743969259808026;
	setAttr -s 5 ".wl[1215].w";
	setAttr ".wl[1215].w[3]" 0.0064682998273512711;
	setAttr ".wl[1215].w[4]" 0.29635198462641749;
	setAttr ".wl[1215].w[5]" 0.003688532383848731;
	setAttr ".wl[1215].w[12]" 0.67690689819512606;
	setAttr ".wl[1215].w[13]" 0.016584284967256526;
	setAttr -s 5 ".wl[1216].w";
	setAttr ".wl[1216].w[3]" 0.008633995568403623;
	setAttr ".wl[1216].w[4]" 0.27357906116957914;
	setAttr ".wl[1216].w[5]" 0.0038085741395757142;
	setAttr ".wl[1216].w[12]" 0.6863933950795702;
	setAttr ".wl[1216].w[13]" 0.027584974042871312;
	setAttr -s 5 ".wl[1217].w";
	setAttr ".wl[1217].w[3]" 0.032319045784223975;
	setAttr ".wl[1217].w[4]" 0.33062720544553453;
	setAttr ".wl[1217].w[5]" 0.020582944426863401;
	setAttr ".wl[1217].w[12]" 0.52784948429366396;
	setAttr ".wl[1217].w[13]" 0.088621320049714167;
	setAttr -s 5 ".wl[1218].w";
	setAttr ".wl[1218].w[2]" 0.010018332385411204;
	setAttr ".wl[1218].w[3]" 0.019810900546269869;
	setAttr ".wl[1218].w[4]" 0.2619946424693948;
	setAttr ".wl[1218].w[12]" 0.60313196759928556;
	setAttr ".wl[1218].w[13]" 0.1050441569996385;
	setAttr -s 5 ".wl[1219].w";
	setAttr ".wl[1219].w[2]" 0.0065169461119977869;
	setAttr ".wl[1219].w[3]" 0.012078047344345842;
	setAttr ".wl[1219].w[4]" 0.17337056752358698;
	setAttr ".wl[1219].w[12]" 0.67372362194720592;
	setAttr ".wl[1219].w[13]" 0.13431081707286349;
	setAttr -s 5 ".wl[1220].w";
	setAttr ".wl[1220].w[2]" 0.0050140393879018093;
	setAttr ".wl[1220].w[3]" 0.01005011295071608;
	setAttr ".wl[1220].w[4]" 0.19498037055197415;
	setAttr ".wl[1220].w[12]" 0.71421918400807138;
	setAttr ".wl[1220].w[13]" 0.07573629310133663;
	setAttr -s 5 ".wl[1221].w";
	setAttr ".wl[1221].w[2]" 0.0075265797566158813;
	setAttr ".wl[1221].w[3]" 0.016384153808281298;
	setAttr ".wl[1221].w[4]" 0.2961319709847412;
	setAttr ".wl[1221].w[12]" 0.62169473463212455;
	setAttr ".wl[1221].w[13]" 0.058262560818237115;
	setAttr -s 5 ".wl[1222].w";
	setAttr ".wl[1222].w[2]" 0.032349213465044373;
	setAttr ".wl[1222].w[3]" 0.046102297658569984;
	setAttr ".wl[1222].w[4]" 0.28161403267760776;
	setAttr ".wl[1222].w[12]" 0.49417256551494532;
	setAttr ".wl[1222].w[13]" 0.14576189068383261;
	setAttr -s 5 ".wl[1223].w";
	setAttr ".wl[1223].w[3]" 0.011297936760246383;
	setAttr ".wl[1223].w[4]" 0.2971120588901045;
	setAttr ".wl[1223].w[5]" 0.0072567077204229464;
	setAttr ".wl[1223].w[12]" 0.63905471965737748;
	setAttr ".wl[1223].w[13]" 0.045278576971848679;
	setAttr -s 5 ".wl[1224].w";
	setAttr ".wl[1224].w[3]" 0.011922817473064833;
	setAttr ".wl[1224].w[4]" 0.23855286575659795;
	setAttr ".wl[1224].w[5]" 0.0073515227616235152;
	setAttr ".wl[1224].w[12]" 0.65981451401446289;
	setAttr ".wl[1224].w[13]" 0.082358279994250883;
	setAttr -s 5 ".wl[1225].w";
	setAttr ".wl[1225].w[3]" 0.014320658316417195;
	setAttr ".wl[1225].w[4]" 0.3015474665817735;
	setAttr ".wl[1225].w[5]" 0.0066842018291535677;
	setAttr ".wl[1225].w[12]" 0.61834478141031768;
	setAttr ".wl[1225].w[13]" 0.0591028918623379;
	setAttr -s 5 ".wl[1226].w";
	setAttr ".wl[1226].w[3]" 0.012258619131861642;
	setAttr ".wl[1226].w[4]" 0.36032593128583723;
	setAttr ".wl[1226].w[5]" 0.0056970737436679653;
	setAttr ".wl[1226].w[12]" 0.59247685278730011;
	setAttr ".wl[1226].w[13]" 0.029241523051332964;
	setAttr -s 5 ".wl[1227].w";
	setAttr ".wl[1227].w[3]" 0.042609748471851575;
	setAttr ".wl[1227].w[4]" 0.37968779880761511;
	setAttr ".wl[1227].w[5]" 0.024622977309096728;
	setAttr ".wl[1227].w[12]" 0.46742063662839395;
	setAttr ".wl[1227].w[13]" 0.085658838783042687;
	setAttr -s 5 ".wl[1228].w";
	setAttr ".wl[1228].w[4]" 0.10224332571346885;
	setAttr ".wl[1228].w[5]" 0.0086602379553194723;
	setAttr ".wl[1228].w[7]" 0.0084263122798618369;
	setAttr ".wl[1228].w[12]" 0.80008361403386796;
	setAttr ".wl[1228].w[13]" 0.080586510017481891;
	setAttr -s 5 ".wl[1229].w";
	setAttr ".wl[1229].w[4]" 0.072897628074127155;
	setAttr ".wl[1229].w[5]" 0.0069041126867105266;
	setAttr ".wl[1229].w[7]" 0.0067217237712791028;
	setAttr ".wl[1229].w[12]" 0.77953294945887863;
	setAttr ".wl[1229].w[13]" 0.13394358600900449;
	setAttr -s 5 ".wl[1230].w";
	setAttr ".wl[1230].w[4]" 0.12882676485874972;
	setAttr ".wl[1230].w[5]" 0.0092119432493574776;
	setAttr ".wl[1230].w[7]" 0.008449111122581416;
	setAttr ".wl[1230].w[12]" 0.74992127435474065;
	setAttr ".wl[1230].w[13]" 0.10359090641457068;
	setAttr -s 5 ".wl[1231].w";
	setAttr ".wl[1231].w[4]" 0.16546960699028651;
	setAttr ".wl[1231].w[5]" 0.009693619196451864;
	setAttr ".wl[1231].w[7]" 0.0088171063251212666;
	setAttr ".wl[1231].w[12]" 0.7591720708606724;
	setAttr ".wl[1231].w[13]" 0.056847596627467994;
	setAttr -s 5 ".wl[1232].w";
	setAttr ".wl[1232].w[4]" 0.20166532334397996;
	setAttr ".wl[1232].w[5]" 0.055129111975037469;
	setAttr ".wl[1232].w[7]" 0.050360029520401708;
	setAttr ".wl[1232].w[12]" 0.51872911618432271;
	setAttr ".wl[1232].w[13]" 0.17411641897625824;
	setAttr -s 5 ".wl[1233].w";
	setAttr ".wl[1233].w[4]" 0.16001575844637225;
	setAttr ".wl[1233].w[5]" 0.013085411468032482;
	setAttr ".wl[1233].w[7]" 0.013085411468032482;
	setAttr ".wl[1233].w[12]" 0.77328453456100232;
	setAttr ".wl[1233].w[13]" 0.040528884056560445;
	setAttr -s 5 ".wl[1234].w";
	setAttr ".wl[1234].w[4]" 0.21293501251576813;
	setAttr ".wl[1234].w[5]" 0.019676453010570791;
	setAttr ".wl[1234].w[7]" 0.019658435513044741;
	setAttr ".wl[1234].w[12]" 0.70991477233343103;
	setAttr ".wl[1234].w[13]" 0.037815326627185335;
	setAttr -s 5 ".wl[1235].w";
	setAttr ".wl[1235].w[4]" 0.31764971675472065;
	setAttr ".wl[1235].w[5]" 0.018909444414417657;
	setAttr ".wl[1235].w[7]" 0.018690051475679838;
	setAttr ".wl[1235].w[12]" 0.62616729351546385;
	setAttr ".wl[1235].w[13]" 0.018583493839718038;
	setAttr -s 5 ".wl[1236].w";
	setAttr ".wl[1236].w[4]" 0.26535333785164961;
	setAttr ".wl[1236].w[5]" 0.014016888808651328;
	setAttr ".wl[1236].w[7]" 0.014016888808651328;
	setAttr ".wl[1236].w[12]" 0.68489701065976172;
	setAttr ".wl[1236].w[13]" 0.02171587387128595;
	setAttr -s 5 ".wl[1237].w";
	setAttr ".wl[1237].w[4]" 0.22725609549671549;
	setAttr ".wl[1237].w[5]" 0.17502787415295853;
	setAttr ".wl[1237].w[7]" 0.17502787415295853;
	setAttr ".wl[1237].w[12]" 0.35963166986040213;
	setAttr ".wl[1237].w[13]" 0.063056486336965395;
	setAttr -s 5 ".wl[1238].w[4:8]"  0.43256823212360535 0.43256823212360535 
		0.063837313698737991 0.032327526731994578 0.038698695322056727;
	setAttr -s 5 ".wl[1239].w[4:8]"  0.43954129405735232 0.43954129405735232 
		0.054504984994173916 0.031045017739967855 0.035367409151153634;
	setAttr -s 5 ".wl[1240].w";
	setAttr ".wl[1240].w[4]" 0.43725594492143355;
	setAttr ".wl[1240].w[5]" 0.43725594492143344;
	setAttr ".wl[1240].w[6]" 0.060848243091731521;
	setAttr ".wl[1240].w[8]" 0.035813824168412098;
	setAttr ".wl[1240].w[9]" 0.028826042896989399;
	setAttr -s 5 ".wl[1241].w[4:8]"  0.42967427552312309 0.42967427552312309 
		0.071800603708017607 0.029749150048337024 0.0391016951973992;
	setAttr -s 5 ".wl[1242].w[4:8]"  0.40149077244405168 0.4014907724440519 
		0.094666746644557764 0.046915316357947774 0.055436392109390789;
	setAttr -s 4 ".wl[1243].w[1:4]"  0.20555108506522071 0.55033506214085193 
		0.22310719948320684 0.021006653310720533;
	setAttr -s 4 ".wl[1244].w[1:4]"  0.15215129902858521 0.52981074416741514 
		0.28953807154186151 0.028499885262138158;
	setAttr -s 4 ".wl[1245].w[1:4]"  0.13783978239926717 0.52380972074996102 
		0.30766692415800223 0.030683572692769577;
	setAttr -s 4 ".wl[1246].w[1:4]"  0.18892817230492626 0.54598538540454855 
		0.24207251873518718 0.02301392355533809;
	setAttr -s 4 ".wl[1247].w";
	setAttr ".wl[1247].w[1]" 0.22249143848206615;
	setAttr ".wl[1247].w[2]" 0.43698554625261693;
	setAttr ".wl[1247].w[3]" 0.27640245731641494;
	setAttr ".wl[1247].w[9]" 0.064120557948901924;
	setAttr -s 5 ".wl[1248].w";
	setAttr ".wl[1248].w[2]" 0.0044259334485855254;
	setAttr ".wl[1248].w[3]" 0.0088551853701966474;
	setAttr ".wl[1248].w[4]" 0.15539419395308807;
	setAttr ".wl[1248].w[9]" 0.721227191198622;
	setAttr ".wl[1248].w[10]" 0.11009749602950783;
	setAttr -s 5 ".wl[1249].w";
	setAttr ".wl[1249].w[2]" 0.0046364094426937239;
	setAttr ".wl[1249].w[3]" 0.0090342382033033221;
	setAttr ".wl[1249].w[4]" 0.17355933375777732;
	setAttr ".wl[1249].w[9]" 0.73052242876135087;
	setAttr ".wl[1249].w[10]" 0.082247589834874818;
	setAttr -s 5 ".wl[1250].w";
	setAttr ".wl[1250].w[2]" 0.0047987187165558949;
	setAttr ".wl[1250].w[3]" 0.0081497187337405685;
	setAttr ".wl[1250].w[4]" 0.11795576019222015;
	setAttr ".wl[1250].w[9]" 0.71843558032977561;
	setAttr ".wl[1250].w[10]" 0.15066022202770776;
	setAttr -s 5 ".wl[1251].w";
	setAttr ".wl[1251].w[2]" 0.0041586140706193391;
	setAttr ".wl[1251].w[3]" 0.0072830104520592333;
	setAttr ".wl[1251].w[4]" 0.10094575109482223;
	setAttr ".wl[1251].w[9]" 0.70266860125922082;
	setAttr ".wl[1251].w[10]" 0.18494402312327846;
	setAttr -s 5 ".wl[1252].w";
	setAttr ".wl[1252].w[2]" 0.024880799501513732;
	setAttr ".wl[1252].w[3]" 0.035991565156728363;
	setAttr ".wl[1252].w[4]" 0.2460064329395622;
	setAttr ".wl[1252].w[9]" 0.51220501998829804;
	setAttr ".wl[1252].w[10]" 0.18091618241389773;
	setAttr -s 5 ".wl[1253].w";
	setAttr ".wl[1253].w[3]" 0.0070348536895204986;
	setAttr ".wl[1253].w[4]" 0.12728388344818969;
	setAttr ".wl[1253].w[5]" 0.0059312810294431893;
	setAttr ".wl[1253].w[9]" 0.72390837434657407;
	setAttr ".wl[1253].w[10]" 0.13584160748627247;
	setAttr -s 5 ".wl[1254].w";
	setAttr ".wl[1254].w[3]" 0.0075999445953319859;
	setAttr ".wl[1254].w[4]" 0.18125513522056697;
	setAttr ".wl[1254].w[5]" 0.005167614447823818;
	setAttr ".wl[1254].w[9]" 0.73229400332124184;
	setAttr ".wl[1254].w[10]" 0.073683302415035415;
	setAttr -s 5 ".wl[1255].w";
	setAttr ".wl[1255].w[3]" 0.0088223937011288837;
	setAttr ".wl[1255].w[4]" 0.16770349013414826;
	setAttr ".wl[1255].w[5]" 0.0052219880418621235;
	setAttr ".wl[1255].w[9]" 0.7083123349009256;
	setAttr ".wl[1255].w[10]" 0.10993979322193521;
	setAttr -s 5 ".wl[1256].w";
	setAttr ".wl[1256].w[3]" 0.0073019733157381594;
	setAttr ".wl[1256].w[4]" 0.11294870179055159;
	setAttr ".wl[1256].w[5]" 0.0053117400046509902;
	setAttr ".wl[1256].w[9]" 0.6940922644639701;
	setAttr ".wl[1256].w[10]" 0.18034532042508911;
	setAttr -s 5 ".wl[1257].w";
	setAttr ".wl[1257].w[3]" 0.028131154564043137;
	setAttr ".wl[1257].w[4]" 0.27123001774938627;
	setAttr ".wl[1257].w[5]" 0.019667501584397933;
	setAttr ".wl[1257].w[9]" 0.5174546420184527;
	setAttr ".wl[1257].w[10]" 0.16351668408372;
	setAttr -s 5 ".wl[1258].w";
	setAttr ".wl[1258].w[4]" 0.10953455092993987;
	setAttr ".wl[1258].w[5]" 0.01024212611172967;
	setAttr ".wl[1258].w[6]" 0.010014631769717484;
	setAttr ".wl[1258].w[9]" 0.78274623698802914;
	setAttr ".wl[1258].w[10]" 0.087462454200583795;
	setAttr -s 5 ".wl[1259].w";
	setAttr ".wl[1259].w[4]" 0.17139350797981484;
	setAttr ".wl[1259].w[5]" 0.01103065169009447;
	setAttr ".wl[1259].w[6]" 0.010117914613194201;
	setAttr ".wl[1259].w[9]" 0.74696304630872024;
	setAttr ".wl[1259].w[10]" 0.060494879408176272;
	setAttr -s 5 ".wl[1260].w";
	setAttr ".wl[1260].w[4]" 0.13197617220951227;
	setAttr ".wl[1260].w[5]" 0.010107249391001025;
	setAttr ".wl[1260].w[6]" 0.0093373322938625283;
	setAttr ".wl[1260].w[9]" 0.74146291892473781;
	setAttr ".wl[1260].w[10]" 0.1071163271808864;
	setAttr -s 5 ".wl[1261].w";
	setAttr ".wl[1261].w[4]" 0.077139980706964914;
	setAttr ".wl[1261].w[5]" 0.0078767798315019458;
	setAttr ".wl[1261].w[6]" 0.0077020668868405763;
	setAttr ".wl[1261].w[9]" 0.766316788940266;
	setAttr ".wl[1261].w[10]" 0.14096438363442657;
	setAttr -s 5 ".wl[1262].w";
	setAttr ".wl[1262].w[4]" 0.20272252946898148;
	setAttr ".wl[1262].w[5]" 0.060453731473624817;
	setAttr ".wl[1262].w[6]" 0.055628706544843275;
	setAttr ".wl[1262].w[9]" 0.50608402752677661;
	setAttr ".wl[1262].w[10]" 0.17511100498577373;
	setAttr -s 5 ".wl[1263].w";
	setAttr ".wl[1263].w[3]" 0.0057293697913953067;
	setAttr ".wl[1263].w[4]" 0.14804823377873899;
	setAttr ".wl[1263].w[5]" 0.0048277099540780747;
	setAttr ".wl[1263].w[9]" 0.78362178900188384;
	setAttr ".wl[1263].w[10]" 0.057772897473903753;
	setAttr -s 5 ".wl[1264].w";
	setAttr ".wl[1264].w[4]" 0.13913134664713137;
	setAttr ".wl[1264].w[5]" 0.0077563427142409826;
	setAttr ".wl[1264].w[6]" 0.0077563427142409826;
	setAttr ".wl[1264].w[9]" 0.76495233151850839;
	setAttr ".wl[1264].w[10]" 0.080403636405878331;
	setAttr -s 5 ".wl[1265].w";
	setAttr ".wl[1265].w[4]" 0.083277884852170844;
	setAttr ".wl[1265].w[5]" 0.0061237113625635881;
	setAttr ".wl[1265].w[6]" 0.0061237113625635881;
	setAttr ".wl[1265].w[9]" 0.76191238824053664;
	setAttr ".wl[1265].w[10]" 0.14256230418216537;
	setAttr -s 5 ".wl[1266].w";
	setAttr ".wl[1266].w[3]" 0.0049370260652781524;
	setAttr ".wl[1266].w[4]" 0.092813146149228254;
	setAttr ".wl[1266].w[5]" 0.0044458875278934832;
	setAttr ".wl[1266].w[9]" 0.78499973859597072;
	setAttr ".wl[1266].w[10]" 0.11280420166162949;
	setAttr -s 5 ".wl[1267].w";
	setAttr ".wl[1267].w[3]" 0.025465426321015418;
	setAttr ".wl[1267].w[4]" 0.23537902550133324;
	setAttr ".wl[1267].w[5]" 0.023477943122612897;
	setAttr ".wl[1267].w[9]" 0.54653281850951196;
	setAttr ".wl[1267].w[10]" 0.16914478654552639;
	setAttr -s 5 ".wl[1268].w";
	setAttr ".wl[1268].w[4]" 0.11830589037129587;
	setAttr ".wl[1268].w[5]" 0.011916440473524029;
	setAttr ".wl[1268].w[6]" 0.011916440473524029;
	setAttr ".wl[1268].w[9]" 0.79957894830839193;
	setAttr ".wl[1268].w[10]" 0.058282280373264127;
	setAttr -s 5 ".wl[1269].w";
	setAttr ".wl[1269].w[4]" 0.11312519802088684;
	setAttr ".wl[1269].w[5]" 0.016288121989718261;
	setAttr ".wl[1269].w[6]" 0.016288121989718261;
	setAttr ".wl[1269].w[9]" 0.76807657355345982;
	setAttr ".wl[1269].w[10]" 0.086221984446216884;
	setAttr -s 5 ".wl[1270].w";
	setAttr ".wl[1270].w[4]" 0.0726619441662502;
	setAttr ".wl[1270].w[5]" 0.01484856960503708;
	setAttr ".wl[1270].w[6]" 0.01484856960503708;
	setAttr ".wl[1270].w[9]" 0.7404414185279673;
	setAttr ".wl[1270].w[10]" 0.1571994980957083;
	setAttr -s 5 ".wl[1271].w";
	setAttr ".wl[1271].w[4]" 0.080851531673421892;
	setAttr ".wl[1271].w[5]" 0.012681907751495925;
	setAttr ".wl[1271].w[6]" 0.012681907751495925;
	setAttr ".wl[1271].w[9]" 0.77300238830405832;
	setAttr ".wl[1271].w[10]" 0.12078226451952791;
	setAttr -s 5 ".wl[1272].w";
	setAttr ".wl[1272].w[4]" 0.18001268830228076;
	setAttr ".wl[1272].w[5]" 0.13703967370936221;
	setAttr ".wl[1272].w[6]" 0.13703967370936221;
	setAttr ".wl[1272].w[9]" 0.42263550184980075;
	setAttr ".wl[1272].w[10]" 0.12327246242919404;
	setAttr -s 5 ".wl[1273].w";
	setAttr ".wl[1273].w[4]" 0.0052876477064664256;
	setAttr ".wl[1273].w[5]" 0.00086582106801171563;
	setAttr ".wl[1273].w[9]" 0.41620644197853907;
	setAttr ".wl[1273].w[10]" 0.57090486748040004;
	setAttr ".wl[1273].w[11]" 0.0067352217665827474;
	setAttr -s 5 ".wl[1274].w";
	setAttr ".wl[1274].w[4]" 0.0053459330184651441;
	setAttr ".wl[1274].w[5]" 0.0010320332690057906;
	setAttr ".wl[1274].w[9]" 0.39163733293582698;
	setAttr ".wl[1274].w[10]" 0.59382434734564082;
	setAttr ".wl[1274].w[11]" 0.0081603534310612253;
	setAttr -s 5 ".wl[1275].w";
	setAttr ".wl[1275].w[4]" 0.0043976837849614078;
	setAttr ".wl[1275].w[5]" 0.0009659130595145311;
	setAttr ".wl[1275].w[9]" 0.27304342564421058;
	setAttr ".wl[1275].w[10]" 0.70777132322238878;
	setAttr ".wl[1275].w[11]" 0.013821654288924724;
	setAttr -s 5 ".wl[1276].w";
	setAttr ".wl[1276].w[4]" 0.0046642931128110087;
	setAttr ".wl[1276].w[5]" 0.00088729008968980083;
	setAttr ".wl[1276].w[9]" 0.3041224409202094;
	setAttr ".wl[1276].w[10]" 0.67814466694581332;
	setAttr ".wl[1276].w[11]" 0.012181308931476354;
	setAttr -s 5 ".wl[1277].w";
	setAttr ".wl[1277].w[3]" 0.0048129095284036071;
	setAttr ".wl[1277].w[4]" 0.022308444169424183;
	setAttr ".wl[1277].w[9]" 0.40972051099395956;
	setAttr ".wl[1277].w[10]" 0.52318910785569783;
	setAttr ".wl[1277].w[11]" 0.039969027452514752;
	setAttr -s 5 ".wl[1278].w";
	setAttr ".wl[1278].w[4]" 0.004966316278442749;
	setAttr ".wl[1278].w[5]" 0.002229669967996761;
	setAttr ".wl[1278].w[9]" 0.34172098303153275;
	setAttr ".wl[1278].w[10]" 0.64074217568254388;
	setAttr ".wl[1278].w[11]" 0.010340855039483973;
	setAttr -s 5 ".wl[1279].w";
	setAttr ".wl[1279].w[4]" 0.0057380664916554707;
	setAttr ".wl[1279].w[5]" 0.0023772903633529846;
	setAttr ".wl[1279].w[9]" 0.43087751990674189;
	setAttr ".wl[1279].w[10]" 0.55439033793319314;
	setAttr ".wl[1279].w[11]" 0.0066167853050565157;
	setAttr -s 5 ".wl[1280].w";
	setAttr ".wl[1280].w[4]" 0.0064455711846177766;
	setAttr ".wl[1280].w[5]" 0.0028499773734751333;
	setAttr ".wl[1280].w[9]" 0.37835213006653873;
	setAttr ".wl[1280].w[10]" 0.60088437276330331;
	setAttr ".wl[1280].w[11]" 0.011467948612065064;
	setAttr -s 5 ".wl[1281].w";
	setAttr ".wl[1281].w[4]" 0.0049469333344408731;
	setAttr ".wl[1281].w[5]" 0.0023378886205671491;
	setAttr ".wl[1281].w[9]" 0.27806057811322366;
	setAttr ".wl[1281].w[10]" 0.69868377171417562;
	setAttr ".wl[1281].w[11]" 0.015970828217592704;
	setAttr -s 5 ".wl[1282].w";
	setAttr ".wl[1282].w[4]" 0.023657819787743749;
	setAttr ".wl[1282].w[5]" 0.018176331477219136;
	setAttr ".wl[1282].w[9]" 0.41164731573955438;
	setAttr ".wl[1282].w[10]" 0.50747413311270684;
	setAttr ".wl[1282].w[11]" 0.039044399882775985;
	setAttr -s 5 ".wl[1283].w";
	setAttr ".wl[1283].w[4]" 0.0038696514644468898;
	setAttr ".wl[1283].w[5]" 0.0012261775472281516;
	setAttr ".wl[1283].w[9]" 0.37287172430331472;
	setAttr ".wl[1283].w[10]" 0.6160235693662397;
	setAttr ".wl[1283].w[11]" 0.0060088773187704399;
	setAttr -s 5 ".wl[1284].w";
	setAttr ".wl[1284].w[4]" 0.0049687493264632112;
	setAttr ".wl[1284].w[5]" 0.001908049026305916;
	setAttr ".wl[1284].w[9]" 0.37167259565952382;
	setAttr ".wl[1284].w[10]" 0.61297716780409262;
	setAttr ".wl[1284].w[11]" 0.0084734381836144984;
	setAttr -s 5 ".wl[1285].w";
	setAttr ".wl[1285].w[4]" 0.0037649440263907126;
	setAttr ".wl[1285].w[5]" 0.0015402101909483944;
	setAttr ".wl[1285].w[9]" 0.24193261614698838;
	setAttr ".wl[1285].w[10]" 0.73921141236072918;
	setAttr ".wl[1285].w[11]" 0.013550817274943302;
	setAttr -s 5 ".wl[1286].w";
	setAttr ".wl[1286].w[4]" 0.0030341914034500306;
	setAttr ".wl[1286].w[5]" 0.0010503784434804993;
	setAttr ".wl[1286].w[9]" 0.23592496711134192;
	setAttr ".wl[1286].w[10]" 0.74997388098849671;
	setAttr ".wl[1286].w[11]" 0.010016582053230867;
	setAttr -s 5 ".wl[1287].w";
	setAttr ".wl[1287].w[4]" 0.018106790063681894;
	setAttr ".wl[1287].w[5]" 0.0088765806769951928;
	setAttr ".wl[1287].w[9]" 0.38146713658321191;
	setAttr ".wl[1287].w[10]" 0.55003481541986809;
	setAttr ".wl[1287].w[11]" 0.041514677256242909;
	setAttr -s 5 ".wl[1288].w";
	setAttr ".wl[1288].w[4]" 0.0013892320637645362;
	setAttr ".wl[1288].w[5]" 0.00057937703669426932;
	setAttr ".wl[1288].w[9]" 0.018908244743068483;
	setAttr ".wl[1288].w[10]" 0.66900454487517125;
	setAttr ".wl[1288].w[11]" 0.31011860128130142;
	setAttr -s 5 ".wl[1289].w";
	setAttr ".wl[1289].w[4]" 0.0022585295220290914;
	setAttr ".wl[1289].w[5]" 0.00084732164558859566;
	setAttr ".wl[1289].w[9]" 0.033769894147351306;
	setAttr ".wl[1289].w[10]" 0.71077856310930709;
	setAttr ".wl[1289].w[11]" 0.25234569157572395;
	setAttr -s 5 ".wl[1290].w";
	setAttr ".wl[1290].w[4]" 0.0017902975229627952;
	setAttr ".wl[1290].w[5]" 0.00066681769774488953;
	setAttr ".wl[1290].w[9]" 0.024024220255080494;
	setAttr ".wl[1290].w[10]" 0.65938312339559624;
	setAttr ".wl[1290].w[11]" 0.31413554112861569;
	setAttr -s 5 ".wl[1291].w";
	setAttr ".wl[1291].w[4]" 0.00095987537935359889;
	setAttr ".wl[1291].w[5]" 0.00039476861377119282;
	setAttr ".wl[1291].w[9]" 0.011930307687910929;
	setAttr ".wl[1291].w[10]" 0.61609948644638868;
	setAttr ".wl[1291].w[11]" 0.37061556187257561;
	setAttr -s 5 ".wl[1292].w";
	setAttr ".wl[1292].w[4]" 0.0051183402831132116;
	setAttr ".wl[1292].w[5]" 0.0020930483747912164;
	setAttr ".wl[1292].w[9]" 0.047723307459401017;
	setAttr ".wl[1292].w[10]" 0.52207501469092776;
	setAttr ".wl[1292].w[11]" 0.42299028919176673;
	setAttr -s 4 ".wl[1293].w[1:4]"  0.19863580223456478 0.5683406004739151 
		0.21829641931260665 0.014727177978913347;
	setAttr -s 4 ".wl[1294].w[1:4]"  0.14564184669779059 0.54687859292366059 
		0.28803648854541158 0.019443071833137326;
	setAttr -s 4 ".wl[1295].w[1:4]"  0.13084853058007967 0.54198853205006681 
		0.30741135085253934 0.019751586517314214;
	setAttr -s 4 ".wl[1296].w[1:4]"  0.18137672865480631 0.56526238219922298 
		0.23791677193058958 0.01544411721538117;
	setAttr -s 4 ".wl[1297].w[1:4]"  0.22385517823443574 0.45706176567885481 
		0.28312184412347674 0.03596121196323273;
	setAttr -s 5 ".wl[1298].w";
	setAttr ".wl[1298].w[4]" 0.001198656470183333;
	setAttr ".wl[1298].w[5]" 0.00041689670251796691;
	setAttr ".wl[1298].w[9]" 0.015575582391024613;
	setAttr ".wl[1298].w[10]" 0.64284523992346709;
	setAttr ".wl[1298].w[11]" 0.33996362451280698;
	setAttr -s 5 ".wl[1299].w";
	setAttr ".wl[1299].w[4]" 0.0020972659049023523;
	setAttr ".wl[1299].w[5]" 0.00066817353106917799;
	setAttr ".wl[1299].w[9]" 0.030261230787465922;
	setAttr ".wl[1299].w[10]" 0.69516559083882667;
	setAttr ".wl[1299].w[11]" 0.27180773893773597;
	setAttr -s 5 ".wl[1300].w";
	setAttr ".wl[1300].w[4]" 0.0015284570388668728;
	setAttr ".wl[1300].w[5]" 0.00051287296636383069;
	setAttr ".wl[1300].w[9]" 0.021228016367910201;
	setAttr ".wl[1300].w[10]" 0.67950430367641734;
	setAttr ".wl[1300].w[11]" 0.29722634995044173;
	setAttr -s 5 ".wl[1301].w";
	setAttr ".wl[1301].w[4]" 0.00075409130682859869;
	setAttr ".wl[1301].w[5]" 0.00027482034143361521;
	setAttr ".wl[1301].w[9]" 0.0095096900730402608;
	setAttr ".wl[1301].w[10]" 0.62521197135638984;
	setAttr ".wl[1301].w[11]" 0.36424942692230772;
	setAttr -s 5 ".wl[1302].w";
	setAttr ".wl[1302].w[4]" 0.0040243079537415375;
	setAttr ".wl[1302].w[5]" 0.0013546855505394025;
	setAttr ".wl[1302].w[9]" 0.036901835546934263;
	setAttr ".wl[1302].w[10]" 0.51315693992650813;
	setAttr ".wl[1302].w[11]" 0.44456223102227671;
	setAttr -s 5 ".wl[1303].w";
	setAttr ".wl[1303].w[4]" 0.0012061630824373333;
	setAttr ".wl[1303].w[5]" 0.00054297707684439536;
	setAttr ".wl[1303].w[9]" 0.019995281140679094;
	setAttr ".wl[1303].w[10]" 0.77948003686213496;
	setAttr ".wl[1303].w[11]" 0.19877554183790422;
	setAttr -s 5 ".wl[1304].w";
	setAttr ".wl[1304].w[4]" 0.0011587699584559728;
	setAttr ".wl[1304].w[5]" 0.00058423892822991258;
	setAttr ".wl[1304].w[9]" 0.016953364678873688;
	setAttr ".wl[1304].w[10]" 0.71379512294031744;
	setAttr ".wl[1304].w[11]" 0.26750850349412308;
	setAttr -s 5 ".wl[1305].w";
	setAttr ".wl[1305].w[4]" 0.00060428111638660166;
	setAttr ".wl[1305].w[5]" 0.00030249870427665626;
	setAttr ".wl[1305].w[9]" 0.0078335517917870676;
	setAttr ".wl[1305].w[10]" 0.64523787093079821;
	setAttr ".wl[1305].w[11]" 0.34602179745675143;
	setAttr -s 5 ".wl[1306].w";
	setAttr ".wl[1306].w[4]" 0.00073090886860727319;
	setAttr ".wl[1306].w[5]" 0.00032990910593466596;
	setAttr ".wl[1306].w[9]" 0.010462914043554259;
	setAttr ".wl[1306].w[10]" 0.71121773213336703;
	setAttr ".wl[1306].w[11]" 0.27725853584853682;
	setAttr -s 5 ".wl[1307].w";
	setAttr ".wl[1307].w[4]" 0.0041182823229829685;
	setAttr ".wl[1307].w[5]" 0.0020614979881260023;
	setAttr ".wl[1307].w[9]" 0.042586482402089156;
	setAttr ".wl[1307].w[10]" 0.55086022997287798;
	setAttr ".wl[1307].w[11]" 0.4003735073139239;
	setAttr -s 4 ".wl[1308].w[1:4]"  0.19684263661389972 0.57290686253772871 
		0.21365066407269334 0.016599836775678223;
	setAttr -s 4 ".wl[1309].w[1:4]"  0.13985203481413944 0.55428308303999874 
		0.28393412172439653 0.021930760421465318;
	setAttr -s 4 ".wl[1310].w[1:4]"  0.12778064600464925 0.55618480198184428 
		0.29506715239739967 0.020967399616106851;
	setAttr -s 4 ".wl[1311].w[1:4]"  0.18300767516537786 0.57684249411751332 
		0.22393634211256078 0.016213488604548087;
	setAttr -s 4 ".wl[1312].w";
	setAttr ".wl[1312].w[1]" 0.21207880238333238;
	setAttr ".wl[1312].w[2]" 0.45548270399942103;
	setAttr ".wl[1312].w[3]" 0.28397065237829661;
	setAttr ".wl[1312].w[12]" 0.048467841238949996;
	setAttr -s 5 ".wl[1313].w[4:8]"  0.18074636596299948 0.51502946742559641 
		0.21177791318095066 0.027571880648560303 0.064874372781893244;
	setAttr -s 5 ".wl[1314].w[4:8]"  0.24262711849929955 0.46706239932884158 
		0.18350384562959068 0.036464991096975388 0.070341645445292672;
	setAttr -s 5 ".wl[1315].w[4:8]"  0.24224160476994494 0.4657241298725972 
		0.19714288377561531 0.031320245646719522 0.063571135935123035;
	setAttr -s 5 ".wl[1316].w[4:8]"  0.18186268916581458 0.50928080943413856 
		0.22778234397908959 0.023650035894466813 0.057424121526490471;
	setAttr -s 5 ".wl[1317].w[4:8]"  0.15425670721768525 0.40480817997905333 
		0.31131647990209427 0.041071643261819855 0.088546989639347373;
	setAttr -s 5 ".wl[1318].w[4:8]"  0.25376988891019508 0.41959161538459744 
		0.065849339646894434 0.1615349175122183 0.09925423854609472;
	setAttr -s 5 ".wl[1319].w[4:8]"  0.25464920083180637 0.44363345012371941 
		0.049035306429392127 0.17001389176232592 0.082668150852756209;
	setAttr -s 5 ".wl[1320].w[4:8]"  0.30288285367885953 0.41212602875336263 
		0.056970724313947674 0.14667319694253456 0.081347196311295708;
	setAttr -s 5 ".wl[1321].w[4:8]"  0.29841948538833279 0.39328074482668873 
		0.074011202237226847 0.13997752584621342 0.094311041701538256;
	setAttr -s 5 ".wl[1322].w[4:8]"  0.21278615489169778 0.35595280508721744 
		0.077191803332686562 0.23672340811336712 0.11734582857503101;
	setAttr -s 5 ".wl[1323].w[4:8]"  0.43482630587106008 0.43482630587105997 
		0.029138379359558222 0.064357004696617912 0.03685200420170387;
	setAttr -s 5 ".wl[1324].w";
	setAttr ".wl[1324].w[4]" 0.43911938646988197;
	setAttr ".wl[1324].w[5]" 0.43911938646988197;
	setAttr ".wl[1324].w[7]" 0.057284335715406941;
	setAttr ".wl[1324].w[8]" 0.033621118270593144;
	setAttr ".wl[1324].w[12]" 0.030855773074236044;
	setAttr -s 5 ".wl[1325].w[4:8]"  0.44232421841079256 0.44232421841079245 
		0.029632871530963018 0.051821075356358492 0.033897616291093446;
	setAttr -s 5 ".wl[1326].w[4:8]"  0.43672468488959471 0.43672468488959493 
		0.032318163455778821 0.057374651308006869 0.036857815457024511;
	setAttr -s 5 ".wl[1327].w";
	setAttr ".wl[1327].w[4]" 0.40390472840375935;
	setAttr ".wl[1327].w[5]" 0.40390472840375935;
	setAttr ".wl[1327].w[7]" 0.090754903857882638;
	setAttr ".wl[1327].w[8]" 0.052231929600113622;
	setAttr ".wl[1327].w[12]" 0.049203709734485047;
	setAttr -s 5 ".wl[1328].w";
	setAttr ".wl[1328].w[2]" 0.046116165203280171;
	setAttr ".wl[1328].w[3]" 0.31148626234309146;
	setAttr ".wl[1328].w[4]" 0.42307763990553193;
	setAttr ".wl[1328].w[9]" 0.030646645425153905;
	setAttr ".wl[1328].w[12]" 0.18867328712294257;
	setAttr -s 5 ".wl[1329].w";
	setAttr ".wl[1329].w[2]" 0.035847989112826155;
	setAttr ".wl[1329].w[3]" 0.30805155920984029;
	setAttr ".wl[1329].w[4]" 0.43398113033119101;
	setAttr ".wl[1329].w[9]" 0.034376700878284823;
	setAttr ".wl[1329].w[12]" 0.18774262046785767;
	setAttr -s 5 ".wl[1330].w";
	setAttr ".wl[1330].w[2]" 0.025541140625539142;
	setAttr ".wl[1330].w[3]" 0.23856768354057192;
	setAttr ".wl[1330].w[4]" 0.46677355036475798;
	setAttr ".wl[1330].w[5]" 0.028502857951594195;
	setAttr ".wl[1330].w[12]" 0.24061476751753674;
	setAttr -s 5 ".wl[1331].w";
	setAttr ".wl[1331].w[2]" 0.032425828389229618;
	setAttr ".wl[1331].w[3]" 0.23505812867933235;
	setAttr ".wl[1331].w[4]" 0.46532932161774454;
	setAttr ".wl[1331].w[5]" 0.020578491383751566;
	setAttr ".wl[1331].w[12]" 0.24660822992994197;
	setAttr -s 5 ".wl[1332].w";
	setAttr ".wl[1332].w[2]" 0.058811023733522108;
	setAttr ".wl[1332].w[3]" 0.22435827678313766;
	setAttr ".wl[1332].w[4]" 0.37506102261497504;
	setAttr ".wl[1332].w[5]" 0.043998216402898811;
	setAttr ".wl[1332].w[12]" 0.29777146046546643;
	setAttr -s 5 ".wl[1333].w";
	setAttr ".wl[1333].w[2]" 0.043503362534218937;
	setAttr ".wl[1333].w[3]" 0.29528623827209832;
	setAttr ".wl[1333].w[4]" 0.43170229252474152;
	setAttr ".wl[1333].w[9]" 0.20165446789171507;
	setAttr ".wl[1333].w[12]" 0.027853638777226259;
	setAttr -s 5 ".wl[1334].w";
	setAttr ".wl[1334].w[2]" 0.029538014348143037;
	setAttr ".wl[1334].w[3]" 0.25534984939285604;
	setAttr ".wl[1334].w[4]" 0.45778663062548719;
	setAttr ".wl[1334].w[5]" 0.02846259905850778;
	setAttr ".wl[1334].w[9]" 0.22886290657500596;
	setAttr -s 5 ".wl[1335].w";
	setAttr ".wl[1335].w[3]" 0.31746155380482244;
	setAttr ".wl[1335].w[4]" 0.42924550572284326;
	setAttr ".wl[1335].w[5]" 0.038176922303072999;
	setAttr ".wl[1335].w[9]" 0.17812969096853243;
	setAttr ".wl[1335].w[12]" 0.036986327200728787;
	setAttr -s 5 ".wl[1336].w";
	setAttr ".wl[1336].w[2]" 0.047383269107226123;
	setAttr ".wl[1336].w[3]" 0.34526356534867886;
	setAttr ".wl[1336].w[4]" 0.40931603662840982;
	setAttr ".wl[1336].w[9]" 0.15758458408106693;
	setAttr ".wl[1336].w[12]" 0.040452544834618347;
	setAttr -s 5 ".wl[1337].w";
	setAttr ".wl[1337].w[2]" 0.066474273379261387;
	setAttr ".wl[1337].w[3]" 0.25515077708104594;
	setAttr ".wl[1337].w[4]" 0.36332532730857586;
	setAttr ".wl[1337].w[5]" 0.050414085774992207;
	setAttr ".wl[1337].w[9]" 0.26463553645612464;
	setAttr -s 5 ".wl[1338].w";
	setAttr ".wl[1338].w[3]" 0.2221080372590136;
	setAttr ".wl[1338].w[4]" 0.46670486287171953;
	setAttr ".wl[1338].w[5]" 0.051461713281024377;
	setAttr ".wl[1338].w[8]" 0.051461713281024377;
	setAttr ".wl[1338].w[12]" 0.20826367330721815;
	setAttr -s 5 ".wl[1339].w";
	setAttr ".wl[1339].w[3]" 0.18647065055313303;
	setAttr ".wl[1339].w[4]" 0.45910270824102128;
	setAttr ".wl[1339].w[5]" 0.071827498902253414;
	setAttr ".wl[1339].w[8]" 0.071674925018345798;
	setAttr ".wl[1339].w[12]" 0.21092421728524652;
	setAttr -s 5 ".wl[1340].w";
	setAttr ".wl[1340].w[3]" 0.22399251596790809;
	setAttr ".wl[1340].w[4]" 0.3643173834451443;
	setAttr ".wl[1340].w[5]" 0.12942856640614844;
	setAttr ".wl[1340].w[8]" 0.12942856640614844;
	setAttr ".wl[1340].w[12]" 0.15283296777465083;
	setAttr -s 5 ".wl[1341].w";
	setAttr ".wl[1341].w[3]" 0.27229697322232116;
	setAttr ".wl[1341].w[4]" 0.38969172891833748;
	setAttr ".wl[1341].w[5]" 0.089829456366003535;
	setAttr ".wl[1341].w[8]" 0.089829456366003535;
	setAttr ".wl[1341].w[12]" 0.15835238512733429;
	setAttr -s 5 ".wl[1342].w";
	setAttr ".wl[1342].w[3]" 0.15035979749785586;
	setAttr ".wl[1342].w[4]" 0.31738040814941121;
	setAttr ".wl[1342].w[5]" 0.13603429089717292;
	setAttr ".wl[1342].w[8]" 0.13603429089717292;
	setAttr ".wl[1342].w[12]" 0.26019121255838717;
	setAttr -s 5 ".wl[1343].w";
	setAttr ".wl[1343].w[3]" 0.15701670898504416;
	setAttr ".wl[1343].w[4]" 0.47128954958423136;
	setAttr ".wl[1343].w[5]" 0.039319373754239405;
	setAttr ".wl[1343].w[8]" 0.039319373754239405;
	setAttr ".wl[1343].w[9]" 0.29305499392224565;
	setAttr -s 5 ".wl[1344].w";
	setAttr ".wl[1344].w[3]" 0.14516497379927909;
	setAttr ".wl[1344].w[4]" 0.45668385522557853;
	setAttr ".wl[1344].w[5]" 0.059772923918673587;
	setAttr ".wl[1344].w[8]" 0.059757562340324227;
	setAttr ".wl[1344].w[9]" 0.27862068471614465;
	setAttr -s 5 ".wl[1345].w";
	setAttr ".wl[1345].w[3]" 0.083998277220393353;
	setAttr ".wl[1345].w[4]" 0.50182051060573196;
	setAttr ".wl[1345].w[5]" 0.036707355593665815;
	setAttr ".wl[1345].w[6]" 0.035604151906003342;
	setAttr ".wl[1345].w[9]" 0.34186970467420563;
	setAttr -s 5 ".wl[1346].w";
	setAttr ".wl[1346].w[3]" 0.085969994802120545;
	setAttr ".wl[1346].w[4]" 0.51343073766301939;
	setAttr ".wl[1346].w[5]" 0.022402052195736828;
	setAttr ".wl[1346].w[6]" 0.021895812583846035;
	setAttr ".wl[1346].w[9]" 0.35630140275527727;
	setAttr -s 5 ".wl[1347].w";
	setAttr ".wl[1347].w[3]" 0.083697972156461839;
	setAttr ".wl[1347].w[4]" 0.37030695335655617;
	setAttr ".wl[1347].w[5]" 0.090659652306293789;
	setAttr ".wl[1347].w[6]" 0.090659652306293789;
	setAttr ".wl[1347].w[9]" 0.36467576987439448;
	setAttr -s 5 ".wl[1348].w";
	setAttr ".wl[1348].w[3]" 0.037575149106068927;
	setAttr ".wl[1348].w[4]" 0.45358988248203991;
	setAttr ".wl[1348].w[5]" 0.4535898824820398;
	setAttr ".wl[1348].w[7]" 0.02458912488322066;
	setAttr ".wl[1348].w[12]" 0.030655961046630695;
	setAttr -s 5 ".wl[1349].w";
	setAttr ".wl[1349].w[3]" 0.032539756319789365;
	setAttr ".wl[1349].w[4]" 0.45696200389306529;
	setAttr ".wl[1349].w[5]" 0.45696200389306518;
	setAttr ".wl[1349].w[7]" 0.026300713699890312;
	setAttr ".wl[1349].w[12]" 0.027235522194189888;
	setAttr -s 5 ".wl[1350].w";
	setAttr ".wl[1350].w[3]" 0.03124060713535308;
	setAttr ".wl[1350].w[4]" 0.45468402887958131;
	setAttr ".wl[1350].w[5]" 0.45468402887958154;
	setAttr ".wl[1350].w[7]" 0.029497782002259439;
	setAttr ".wl[1350].w[12]" 0.029893553103224526;
	setAttr -s 5 ".wl[1351].w";
	setAttr ".wl[1351].w[3]" 0.036095331219118058;
	setAttr ".wl[1351].w[4]" 0.45109514301722797;
	setAttr ".wl[1351].w[5]" 0.4510951430172282;
	setAttr ".wl[1351].w[7]" 0.02754173674297989;
	setAttr ".wl[1351].w[12]" 0.034172646003445772;
	setAttr -s 5 ".wl[1352].w";
	setAttr ".wl[1352].w[3]" 0.061006509725128694;
	setAttr ".wl[1352].w[4]" 0.41460715973085333;
	setAttr ".wl[1352].w[5]" 0.41460715973085355;
	setAttr ".wl[1352].w[7]" 0.046694413790428005;
	setAttr ".wl[1352].w[12]" 0.063084757022736346;
	setAttr -s 5 ".wl[1353].w";
	setAttr ".wl[1353].w[4]" 0.3273171817611345;
	setAttr ".wl[1353].w[5]" 0.48909082540365034;
	setAttr ".wl[1353].w[7]" 0.10754353452306019;
	setAttr ".wl[1353].w[8]" 0.030847859799030233;
	setAttr ".wl[1353].w[12]" 0.045200598513124766;
	setAttr -s 5 ".wl[1354].w";
	setAttr ".wl[1354].w[4]" 0.35533820010282474;
	setAttr ".wl[1354].w[5]" 0.46808320213776994;
	setAttr ".wl[1354].w[7]" 0.10487148666292692;
	setAttr ".wl[1354].w[8]" 0.032895697367761573;
	setAttr ".wl[1354].w[12]" 0.038811413728716937;
	setAttr -s 5 ".wl[1355].w";
	setAttr ".wl[1355].w[4]" 0.29980816393850579;
	setAttr ".wl[1355].w[5]" 0.5017143431198855;
	setAttr ".wl[1355].w[7]" 0.13103997067564688;
	setAttr ".wl[1355].w[8]" 0.034743033652977665;
	setAttr ".wl[1355].w[12]" 0.032694488612984067;
	setAttr -s 5 ".wl[1356].w";
	setAttr ".wl[1356].w[4]" 0.26231879161358262;
	setAttr ".wl[1356].w[5]" 0.53531576328692154;
	setAttr ".wl[1356].w[7]" 0.13478886797176881;
	setAttr ".wl[1356].w[8]" 0.031520759287740736;
	setAttr ".wl[1356].w[12]" 0.036055817839986147;
	setAttr -s 5 ".wl[1357].w";
	setAttr ".wl[1357].w[4]" 0.24542702252373558;
	setAttr ".wl[1357].w[5]" 0.40693502160092132;
	setAttr ".wl[1357].w[7]" 0.21350364036714103;
	setAttr ".wl[1357].w[8]" 0.048958818082746505;
	setAttr ".wl[1357].w[12]" 0.085175497425455507;
	setAttr -s 5 ".wl[1358].w";
	setAttr ".wl[1358].w[4]" 0.37894452832931164;
	setAttr ".wl[1358].w[5]" 0.44408942151668312;
	setAttr ".wl[1358].w[6]" 0.094337186061962328;
	setAttr ".wl[1358].w[8]" 0.031974215437292342;
	setAttr ".wl[1358].w[9]" 0.050654648654750681;
	setAttr -s 5 ".wl[1359].w";
	setAttr ".wl[1359].w[4]" 0.39172755561822797;
	setAttr ".wl[1359].w[5]" 0.43132074367157608;
	setAttr ".wl[1359].w[6]" 0.096494406007631428;
	setAttr ".wl[1359].w[8]" 0.034780868367000589;
	setAttr ".wl[1359].w[9]" 0.045676426335563985;
	setAttr -s 5 ".wl[1360].w";
	setAttr ".wl[1360].w[4]" 0.42498416910677661;
	setAttr ".wl[1360].w[5]" 0.43101067848981189;
	setAttr ".wl[1360].w[6]" 0.071339701281206591;
	setAttr ".wl[1360].w[8]" 0.030137737964994041;
	setAttr ".wl[1360].w[9]" 0.042527713157210795;
	setAttr -s 5 ".wl[1361].w";
	setAttr ".wl[1361].w[3]" 0.028914449439112644;
	setAttr ".wl[1361].w[4]" 0.41964733787148722;
	setAttr ".wl[1361].w[5]" 0.43605702109510169;
	setAttr ".wl[1361].w[6]" 0.06818662972056605;
	setAttr ".wl[1361].w[9]" 0.047194561873732367;
	setAttr -s 5 ".wl[1362].w";
	setAttr ".wl[1362].w[3]" 0.050432465860465882;
	setAttr ".wl[1362].w[4]" 0.34932773267832729;
	setAttr ".wl[1362].w[5]" 0.36785932307219565;
	setAttr ".wl[1362].w[6]" 0.12509280478521126;
	setAttr ".wl[1362].w[9]" 0.10728767360379997;
	setAttr -s 5 ".wl[1363].w";
	setAttr ".wl[1363].w[3]" 0.12074781157876749;
	setAttr ".wl[1363].w[4]" 0.4282594464847776;
	setAttr ".wl[1363].w[5]" 0.31195169545762469;
	setAttr ".wl[1363].w[9]" 0.10935827887235608;
	setAttr ".wl[1363].w[12]" 0.029682767606474196;
	setAttr -s 5 ".wl[1364].w";
	setAttr ".wl[1364].w[3]" 0.089169123782557105;
	setAttr ".wl[1364].w[4]" 0.42401186562850685;
	setAttr ".wl[1364].w[5]" 0.36288027474584472;
	setAttr ".wl[1364].w[6]" 0.03068108046182054;
	setAttr ".wl[1364].w[9]" 0.093257655381270843;
	setAttr -s 5 ".wl[1365].w";
	setAttr ".wl[1365].w[3]" 0.07506673275237162;
	setAttr ".wl[1365].w[4]" 0.43789876236934483;
	setAttr ".wl[1365].w[5]" 0.39960271441107098;
	setAttr ".wl[1365].w[6]" 0.025181868010694681;
	setAttr ".wl[1365].w[9]" 0.062249922456517982;
	setAttr -s 5 ".wl[1366].w";
	setAttr ".wl[1366].w[3]" 0.10407642015149107;
	setAttr ".wl[1366].w[4]" 0.43957340650373472;
	setAttr ".wl[1366].w[5]" 0.35029287427734845;
	setAttr ".wl[1366].w[9]" 0.074538667722792079;
	setAttr ".wl[1366].w[12]" 0.031518631344633771;
	setAttr -s 5 ".wl[1367].w";
	setAttr ".wl[1367].w[3]" 0.12575381956851053;
	setAttr ".wl[1367].w[4]" 0.3602696522158545;
	setAttr ".wl[1367].w[5]" 0.33535424903695732;
	setAttr ".wl[1367].w[9]" 0.13517239305406359;
	setAttr ".wl[1367].w[12]" 0.043449886124614132;
	setAttr -s 5 ".wl[1368].w";
	setAttr ".wl[1368].w[3]" 0.041796785228116584;
	setAttr ".wl[1368].w[4]" 0.31992968016779932;
	setAttr ".wl[1368].w[5]" 0.23059655079583249;
	setAttr ".wl[1368].w[7]" 0.096320315690509153;
	setAttr ".wl[1368].w[12]" 0.31135666811774243;
	setAttr -s 5 ".wl[1369].w";
	setAttr ".wl[1369].w[3]" 0.041548172865104739;
	setAttr ".wl[1369].w[4]" 0.21665119892548393;
	setAttr ".wl[1369].w[5]" 0.39494386120110042;
	setAttr ".wl[1369].w[7]" 0.13289270668230416;
	setAttr ".wl[1369].w[12]" 0.21396406032600684;
	setAttr -s 5 ".wl[1370].w";
	setAttr ".wl[1370].w[4]" 0.1319763876112959;
	setAttr ".wl[1370].w[5]" 0.59238187749961968;
	setAttr ".wl[1370].w[7]" 0.14925708570337845;
	setAttr ".wl[1370].w[8]" 0.02901305487804829;
	setAttr ".wl[1370].w[12]" 0.097371594307657697;
	setAttr -s 5 ".wl[1371].w";
	setAttr ".wl[1371].w[3]" 0.038254650118729507;
	setAttr ".wl[1371].w[4]" 0.19170162291887224;
	setAttr ".wl[1371].w[5]" 0.44172039298197968;
	setAttr ".wl[1371].w[7]" 0.15146967249804449;
	setAttr ".wl[1371].w[12]" 0.17685366148237416;
	setAttr -s 5 ".wl[1372].w";
	setAttr ".wl[1372].w[4]" 0.16445502577779703;
	setAttr ".wl[1372].w[5]" 0.35839513153870844;
	setAttr ".wl[1372].w[7]" 0.27862939738470849;
	setAttr ".wl[1372].w[8]" 0.027539526048283837;
	setAttr ".wl[1372].w[12]" 0.17098091925050235;
	setAttr -s 5 ".wl[1373].w";
	setAttr ".wl[1373].w[3]" 0.037680727994205507;
	setAttr ".wl[1373].w[4]" 0.29734769918087445;
	setAttr ".wl[1373].w[5]" 0.24689285303493549;
	setAttr ".wl[1373].w[6]" 0.12087919952619029;
	setAttr ".wl[1373].w[9]" 0.2971995202637942;
	setAttr -s 5 ".wl[1374].w";
	setAttr ".wl[1374].w[3]" 0.034063990315074191;
	setAttr ".wl[1374].w[4]" 0.1685069105010551;
	setAttr ".wl[1374].w[5]" 0.45328351913171466;
	setAttr ".wl[1374].w[6]" 0.17595562094267619;
	setAttr ".wl[1374].w[9]" 0.1681899591094799;
	setAttr -s 5 ".wl[1375].w";
	setAttr ".wl[1375].w[3]" 0.04610651514438685;
	setAttr ".wl[1375].w[4]" 0.22187387019231772;
	setAttr ".wl[1375].w[5]" 0.38217089788795344;
	setAttr ".wl[1375].w[6]" 0.12880121089076646;
	setAttr ".wl[1375].w[9]" 0.22104750588457556;
	setAttr -s 5 ".wl[1376].w";
	setAttr ".wl[1376].w[3]" 0.044625631871236741;
	setAttr ".wl[1376].w[4]" 0.34041142522043671;
	setAttr ".wl[1376].w[5]" 0.19362818723927033;
	setAttr ".wl[1376].w[6]" 0.081567351435582566;
	setAttr ".wl[1376].w[9]" 0.33976740423347374;
	setAttr -s 5 ".wl[1377].w";
	setAttr ".wl[1377].w[4]" 0.28097441551710073;
	setAttr ".wl[1377].w[5]" 0.18850879491319611;
	setAttr ".wl[1377].w[6]" 0.13864788410548262;
	setAttr ".wl[1377].w[9]" 0.33330013204853903;
	setAttr ".wl[1377].w[10]" 0.058568773415681633;
	setAttr -s 5 ".wl[1378].w";
	setAttr ".wl[1378].w[1]" 0.024742041331885376;
	setAttr ".wl[1378].w[2]" 0.4290998490854378;
	setAttr ".wl[1378].w[3]" 0.45045512267286975;
	setAttr ".wl[1378].w[4]" 0.056929005914678962;
	setAttr ".wl[1378].w[12]" 0.03877398099512816;
	setAttr -s 5 ".wl[1379].w";
	setAttr ".wl[1379].w[1]" 0.01984692593357849;
	setAttr ".wl[1379].w[2]" 0.38195165345591658;
	setAttr ".wl[1379].w[3]" 0.45735344173510167;
	setAttr ".wl[1379].w[4]" 0.083544208300223383;
	setAttr ".wl[1379].w[12]" 0.057303770575179831;
	setAttr -s 5 ".wl[1380].w";
	setAttr ".wl[1380].w[2]" 0.36468996587073926;
	setAttr ".wl[1380].w[3]" 0.48539156436349556;
	setAttr ".wl[1380].w[4]" 0.083084338637140515;
	setAttr ".wl[1380].w[9]" 0.01940393370857493;
	setAttr ".wl[1380].w[12]" 0.047430197420049724;
	setAttr -s 5 ".wl[1381].w";
	setAttr ".wl[1381].w[1]" 0.020448260245385722;
	setAttr ".wl[1381].w[2]" 0.42116677768080074;
	setAttr ".wl[1381].w[3]" 0.46783330562143977;
	setAttr ".wl[1381].w[4]" 0.057495672567863539;
	setAttr ".wl[1381].w[12]" 0.033055983884510177;
	setAttr -s 5 ".wl[1382].w";
	setAttr ".wl[1382].w[2]" 0.34935526639919556;
	setAttr ".wl[1382].w[3]" 0.38711553819950356;
	setAttr ".wl[1382].w[4]" 0.12454647757281304;
	setAttr ".wl[1382].w[5]" 0.047746356075742388;
	setAttr ".wl[1382].w[12]" 0.091236361752745493;
	setAttr -s 5 ".wl[1383].w";
	setAttr ".wl[1383].w[1]" 0.022724758618047358;
	setAttr ".wl[1383].w[2]" 0.41448248642168073;
	setAttr ".wl[1383].w[3]" 0.4632510111441977;
	setAttr ".wl[1383].w[4]" 0.064586339514206739;
	setAttr ".wl[1383].w[9]" 0.034955404301867436;
	setAttr -s 5 ".wl[1384].w";
	setAttr ".wl[1384].w[2]" 0.35568409853511002;
	setAttr ".wl[1384].w[3]" 0.4778409093786441;
	setAttr ".wl[1384].w[4]" 0.093236831072699936;
	setAttr ".wl[1384].w[9]" 0.045349651210095175;
	setAttr ".wl[1384].w[12]" 0.027888509803450621;
	setAttr -s 5 ".wl[1385].w";
	setAttr ".wl[1385].w[2]" 0.36856314553658515;
	setAttr ".wl[1385].w[3]" 0.48364221046383021;
	setAttr ".wl[1385].w[4]" 0.083612032820157306;
	setAttr ".wl[1385].w[9]" 0.033502264813036309;
	setAttr ".wl[1385].w[12]" 0.030680346366391043;
	setAttr -s 5 ".wl[1386].w";
	setAttr ".wl[1386].w[2]" 0.4265534554788038;
	setAttr ".wl[1386].w[3]" 0.46842869287201805;
	setAttr ".wl[1386].w[4]" 0.056749364682693169;
	setAttr ".wl[1386].w[9]" 0.025805448454311642;
	setAttr ".wl[1386].w[12]" 0.022463038512173433;
	setAttr -s 5 ".wl[1387].w";
	setAttr ".wl[1387].w[2]" 0.36943392426613064;
	setAttr ".wl[1387].w[3]" 0.40077600352282999;
	setAttr ".wl[1387].w[4]" 0.11487738224629171;
	setAttr ".wl[1387].w[9]" 0.064116313671560302;
	setAttr ".wl[1387].w[12]" 0.05079637629318718;
	setAttr -s 5 ".wl[1388].w";
	setAttr ".wl[1388].w[1]" 0.024298105618133541;
	setAttr ".wl[1388].w[2]" 0.37844586157128046;
	setAttr ".wl[1388].w[3]" 0.40522270995128634;
	setAttr ".wl[1388].w[4]" 0.099766432342149131;
	setAttr ".wl[1388].w[12]" 0.092266890517150535;
	setAttr -s 5 ".wl[1389].w";
	setAttr ".wl[1389].w[2]" 0.31163324750587479;
	setAttr ".wl[1389].w[3]" 0.383971894204297;
	setAttr ".wl[1389].w[4]" 0.14681776139294686;
	setAttr ".wl[1389].w[12]" 0.13410486647961872;
	setAttr ".wl[1389].w[13]" 0.023472230417262704;
	setAttr -s 5 ".wl[1390].w";
	setAttr ".wl[1390].w[2]" 0.27295577212202737;
	setAttr ".wl[1390].w[3]" 0.35505802660477115;
	setAttr ".wl[1390].w[4]" 0.17771774218485198;
	setAttr ".wl[1390].w[12]" 0.16747245852737955;
	setAttr ".wl[1390].w[13]" 0.026796000560969768;
	setAttr -s 5 ".wl[1391].w";
	setAttr ".wl[1391].w[2]" 0.3502742449590433;
	setAttr ".wl[1391].w[3]" 0.38788826738173304;
	setAttr ".wl[1391].w[4]" 0.12210915457476272;
	setAttr ".wl[1391].w[12]" 0.11610041195822955;
	setAttr ".wl[1391].w[13]" 0.023627921126231421;
	setAttr -s 5 ".wl[1392].w";
	setAttr ".wl[1392].w[2]" 0.23634411380392961;
	setAttr ".wl[1392].w[3]" 0.25272249748914211;
	setAttr ".wl[1392].w[4]" 0.21620856045554313;
	setAttr ".wl[1392].w[12]" 0.22175504602787999;
	setAttr ".wl[1392].w[13]" 0.072969782223505114;
	setAttr -s 5 ".wl[1393].w";
	setAttr ".wl[1393].w[1]" 0.019072185117610167;
	setAttr ".wl[1393].w[2]" 0.37135855053879302;
	setAttr ".wl[1393].w[3]" 0.41717346342081035;
	setAttr ".wl[1393].w[4]" 0.10030425196044662;
	setAttr ".wl[1393].w[9]" 0.092091548962339731;
	setAttr -s 5 ".wl[1394].w";
	setAttr ".wl[1394].w[2]" 0.28222859606959738;
	setAttr ".wl[1394].w[3]" 0.37682745071999113;
	setAttr ".wl[1394].w[4]" 0.16554797101993704;
	setAttr ".wl[1394].w[9]" 0.15299031714593508;
	setAttr ".wl[1394].w[10]" 0.022405665044539481;
	setAttr -s 5 ".wl[1395].w";
	setAttr ".wl[1395].w[2]" 0.2920590224577489;
	setAttr ".wl[1395].w[3]" 0.37005478944663012;
	setAttr ".wl[1395].w[4]" 0.16248642493828247;
	setAttr ".wl[1395].w[9]" 0.15043424158850177;
	setAttr ".wl[1395].w[10]" 0.024965521568836836;
	setAttr -s 5 ".wl[1396].w";
	setAttr ".wl[1396].w[1]" 0.02307637240255677;
	setAttr ".wl[1396].w[2]" 0.37252668917954285;
	setAttr ".wl[1396].w[3]" 0.40684498963679261;
	setAttr ".wl[1396].w[4]" 0.10293558239841119;
	setAttr ".wl[1396].w[9]" 0.094616366382696632;
	setAttr -s 5 ".wl[1397].w";
	setAttr ".wl[1397].w[2]" 0.24758541585210503;
	setAttr ".wl[1397].w[3]" 0.26239595944890037;
	setAttr ".wl[1397].w[4]" 0.20894859986118752;
	setAttr ".wl[1397].w[9]" 0.21413939785356492;
	setAttr ".wl[1397].w[10]" 0.06693062698424207;
	setAttr -s 5 ".wl[1398].w";
	setAttr ".wl[1398].w[1]" 0.02655034573102388;
	setAttr ".wl[1398].w[2]" 0.40075613856749581;
	setAttr ".wl[1398].w[3]" 0.43042318407177965;
	setAttr ".wl[1398].w[4]" 0.079372209087736803;
	setAttr ".wl[1398].w[9]" 0.062898122541963919;
	setAttr -s 5 ".wl[1399].w";
	setAttr ".wl[1399].w[1]" 0.021767458168069714;
	setAttr ".wl[1399].w[2]" 0.34848900634896779;
	setAttr ".wl[1399].w[3]" 0.43111415096329175;
	setAttr ".wl[1399].w[4]" 0.11317692810829363;
	setAttr ".wl[1399].w[9]" 0.085452456411377012;
	setAttr -s 5 ".wl[1400].w";
	setAttr ".wl[1400].w[2]" 0.31561751475364352;
	setAttr ".wl[1400].w[3]" 0.41249804766321424;
	setAttr ".wl[1400].w[4]" 0.13837452579280002;
	setAttr ".wl[1400].w[5]" 0.020290261298992158;
	setAttr ".wl[1400].w[9]" 0.11321965049135023;
	setAttr -s 5 ".wl[1401].w";
	setAttr ".wl[1401].w[1]" 0.0243449344086577;
	setAttr ".wl[1401].w[2]" 0.37607681505187379;
	setAttr ".wl[1401].w[3]" 0.41807835258339554;
	setAttr ".wl[1401].w[4]" 0.098017262550466833;
	setAttr ".wl[1401].w[9]" 0.083482635405606187;
	setAttr -s 5 ".wl[1402].w";
	setAttr ".wl[1402].w[2]" 0.30690334391313179;
	setAttr ".wl[1402].w[3]" 0.32931785022781285;
	setAttr ".wl[1402].w[4]" 0.16365212573103757;
	setAttr ".wl[1402].w[9]" 0.15596162136007727;
	setAttr ".wl[1402].w[10]" 0.044165058767940524;
	setAttr -s 4 ".wl[1403].w[1:4]"  0.20670765376084979 0.58935713493166308 
		0.19159784504510688 0.012337366262380248;
	setAttr -s 4 ".wl[1404].w[1:4]"  0.14235761510984934 0.58553337580201292 
		0.25741483686629202 0.01469417222184574;
	setAttr -s 4 ".wl[1405].w[1:4]"  0.13077549319682566 0.58150227514124964 
		0.27187712811651477 0.015845103545409887;
	setAttr -s 4 ".wl[1406].w[1:4]"  0.193294854692285 0.58775755606329705 
		0.20539562434038761 0.013551964904030338;
	setAttr -s 4 ".wl[1407].w[1:4]"  0.20643532735110895 0.46863102176595756 
		0.28820469973447532 0.036728951148457985;
	setAttr -s 4 ".wl[1408].w[1:4]"  0.20531743136536604 0.55374367136888347 
		0.22488075783837722 0.016058139427373171;
	setAttr -s 4 ".wl[1409].w[1:4]"  0.15213547707081485 0.53453612014411356 
		0.29267221485557399 0.020656187929497465;
	setAttr -s 4 ".wl[1410].w[1:4]"  0.13802625523644871 0.52853667115330527 
		0.3109173964561715 0.022519677154074554;
	setAttr -s 4 ".wl[1411].w[1:4]"  0.18892662813847672 0.54930959015163738 
		0.24388522710986146 0.017878554600024434;
	setAttr -s 4 ".wl[1412].w[1:4]"  0.22739634323862068 0.44901825731101563 
		0.28466933959107532 0.038916059859288345;
	setAttr -s 5 ".wl[1413].w";
	setAttr ".wl[1413].w[4]" 0.0010017994559196333;
	setAttr ".wl[1413].w[5]" 0.00059920970311455525;
	setAttr ".wl[1413].w[9]" 0.015994797767802715;
	setAttr ".wl[1413].w[10]" 0.76731813464849108;
	setAttr ".wl[1413].w[11]" 0.21508605842467202;
	setAttr -s 5 ".wl[1414].w";
	setAttr ".wl[1414].w[4]" 0.0011186864649563449;
	setAttr ".wl[1414].w[5]" 0.00069851621317749632;
	setAttr ".wl[1414].w[9]" 0.016144168218172043;
	setAttr ".wl[1414].w[10]" 0.70640404509736876;
	setAttr ".wl[1414].w[11]" 0.27563458400632546;
	setAttr -s 5 ".wl[1415].w";
	setAttr ".wl[1415].w[4]" 0.00069571374775522128;
	setAttr ".wl[1415].w[5]" 0.00044304720534379868;
	setAttr ".wl[1415].w[9]" 0.0085738393808844141;
	setAttr ".wl[1415].w[10]" 0.61012191805522575;
	setAttr ".wl[1415].w[11]" 0.38016548161079078;
	setAttr -s 5 ".wl[1416].w";
	setAttr ".wl[1416].w[4]" 0.00069218959433688815;
	setAttr ".wl[1416].w[5]" 0.00042427102612254411;
	setAttr ".wl[1416].w[9]" 0.0092118810083897228;
	setAttr ".wl[1416].w[10]" 0.66214650272966769;
	setAttr ".wl[1416].w[11]" 0.32752515564148321;
	setAttr -s 5 ".wl[1417].w";
	setAttr ".wl[1417].w[4]" 0.0045424214860571837;
	setAttr ".wl[1417].w[5]" 0.0035505808302622659;
	setAttr ".wl[1417].w[9]" 0.047518424859341712;
	setAttr ".wl[1417].w[10]" 0.5560687311579513;
	setAttr ".wl[1417].w[11]" 0.38831984166638761;
	setAttr -s 5 ".wl[1418].w";
	setAttr ".wl[1418].w[4]" 0.0074641367812112473;
	setAttr ".wl[1418].w[5]" 0.0012532834709439681;
	setAttr ".wl[1418].w[9]" 0.3482202482136873;
	setAttr ".wl[1418].w[10]" 0.62545528954929508;
	setAttr ".wl[1418].w[11]" 0.017607041984862425;
	setAttr -s 5 ".wl[1419].w";
	setAttr ".wl[1419].w[3]" 0.0011602357476679767;
	setAttr ".wl[1419].w[4]" 0.0077445150844146328;
	setAttr ".wl[1419].w[9]" 0.42634770159602448;
	setAttr ".wl[1419].w[10]" 0.55475634401842766;
	setAttr ".wl[1419].w[11]" 0.0099912035534653314;
	setAttr -s 5 ".wl[1420].w";
	setAttr ".wl[1420].w[3]" 0.0014784212885780982;
	setAttr ".wl[1420].w[4]" 0.0089027957874202208;
	setAttr ".wl[1420].w[9]" 0.38917696788566086;
	setAttr ".wl[1420].w[10]" 0.58417273525669977;
	setAttr ".wl[1420].w[11]" 0.016269079781641081;
	setAttr -s 5 ".wl[1421].w";
	setAttr ".wl[1421].w[3]" 0.0014015253511155872;
	setAttr ".wl[1421].w[4]" 0.0078052296525042949;
	setAttr ".wl[1421].w[9]" 0.30577454554152161;
	setAttr ".wl[1421].w[10]" 0.65903301790529201;
	setAttr ".wl[1421].w[11]" 0.02598568154956651;
	setAttr -s 5 ".wl[1422].w";
	setAttr ".wl[1422].w[3]" 0.0058879910685096272;
	setAttr ".wl[1422].w[4]" 0.030472744561398989;
	setAttr ".wl[1422].w[9]" 0.42532339262647378;
	setAttr ".wl[1422].w[10]" 0.49392523386331294;
	setAttr ".wl[1422].w[11]" 0.044390637880304659;
	setAttr -s 5 ".wl[1423].w";
	setAttr ".wl[1423].w[4]" 0.0096587826149608647;
	setAttr ".wl[1423].w[5]" 0.0026391690590663354;
	setAttr ".wl[1423].w[9]" 0.40995172764776638;
	setAttr ".wl[1423].w[10]" 0.56264218559289503;
	setAttr ".wl[1423].w[11]" 0.015108135085311362;
	setAttr -s 5 ".wl[1424].w";
	setAttr ".wl[1424].w[4]" 0.010002756229807681;
	setAttr ".wl[1424].w[5]" 0.0022125287674668809;
	setAttr ".wl[1424].w[9]" 0.44072209157181902;
	setAttr ".wl[1424].w[10]" 0.53524376598177337;
	setAttr ".wl[1424].w[11]" 0.011818857449133005;
	setAttr -s 5 ".wl[1425].w";
	setAttr ".wl[1425].w[4]" 0.0084751144424042614;
	setAttr ".wl[1425].w[5]" 0.0020323555929943051;
	setAttr ".wl[1425].w[9]" 0.36012179105700576;
	setAttr ".wl[1425].w[10]" 0.61022160317689245;
	setAttr ".wl[1425].w[11]" 0.019149135730703256;
	setAttr -s 5 ".wl[1426].w";
	setAttr ".wl[1426].w[4]" 0.007702202519423669;
	setAttr ".wl[1426].w[5]" 0.0022199409529347794;
	setAttr ".wl[1426].w[9]" 0.31770961080399196;
	setAttr ".wl[1426].w[10]" 0.64918865803361203;
	setAttr ".wl[1426].w[11]" 0.023179587690037553;
	setAttr -s 5 ".wl[1427].w";
	setAttr ".wl[1427].w[4]" 0.030386510315401065;
	setAttr ".wl[1427].w[5]" 0.0089569720624216535;
	setAttr ".wl[1427].w[9]" 0.408529302470449;
	setAttr ".wl[1427].w[10]" 0.49888776244432315;
	setAttr ".wl[1427].w[11]" 0.053239452707405072;
	setAttr -s 5 ".wl[1428].w[4:8]"  0.0073414586988415301 0.41153638394782699 
		0.004194840809334216 0.36187047159777175 0.21505684494622557;
	setAttr -s 5 ".wl[1429].w";
	setAttr ".wl[1429].w[4]" 0.0034270111878748877;
	setAttr ".wl[1429].w[5]" 0.4935745388429077;
	setAttr ".wl[1429].w[7]" 0.4935745388429077;
	setAttr ".wl[1429].w[8]" 0.0059944856656650542;
	setAttr ".wl[1429].w[12]" 0.0034294254606447279;
	setAttr -s 5 ".wl[1430].w[4:8]"  0.0048494430319604969 0.59280918024733453 
		0.0028203099981739029 0.36005053814571675 0.039470528576814289;
	setAttr -s 5 ".wl[1431].w";
	setAttr ".wl[1431].w[4]" 0.0043122820587541445;
	setAttr ".wl[1431].w[5]" 0.49281374734620714;
	setAttr ".wl[1431].w[7]" 0.49281374734620703;
	setAttr ".wl[1431].w[8]" 0.0080449303512197118;
	setAttr ".wl[1431].w[12]" 0.0020152928976120326;
	setAttr -s 5 ".wl[1432].w[4:8]"  0.0033066733016368045 0.54239058859274891 
		0.0020879298750166894 0.42159221806327268 0.03062259016732502;
	setAttr -s 5 ".wl[1433].w";
	setAttr ".wl[1433].w[4]" 0.002695743175840763;
	setAttr ".wl[1433].w[5]" 0.49495035677650534;
	setAttr ".wl[1433].w[7]" 0.49495035677650534;
	setAttr ".wl[1433].w[8]" 0.0060591167748965799;
	setAttr ".wl[1433].w[12]" 0.0013444264962520259;
	setAttr -s 5 ".wl[1434].w[4:8]"  0.0049667785086336272 0.43693796693045805 
		0.0030858435907030498 0.41968699584382685 0.13532241512637846;
	setAttr -s 5 ".wl[1435].w";
	setAttr ".wl[1435].w[4]" 0.0019538594692850952;
	setAttr ".wl[1435].w[5]" 0.49581923538246692;
	setAttr ".wl[1435].w[7]" 0.49581923538246669;
	setAttr ".wl[1435].w[8]" 0.0044537050989639443;
	setAttr ".wl[1435].w[12]" 0.001953964666817441;
	setAttr -s 5 ".wl[1436].w";
	setAttr ".wl[1436].w[4]" 0.0070236098293899422;
	setAttr ".wl[1436].w[5]" 0.48367483817127388;
	setAttr ".wl[1436].w[7]" 0.48367483817127388;
	setAttr ".wl[1436].w[8]" 0.018691477306174802;
	setAttr ".wl[1436].w[12]" 0.0069352365218875878;
	setAttr -s 5 ".wl[1437].w";
	setAttr ".wl[1437].w[4]" 0.0039680197622225539;
	setAttr ".wl[1437].w[5]" 0.48920323569883067;
	setAttr ".wl[1437].w[7]" 0.48920323569883067;
	setAttr ".wl[1437].w[8]" 0.01370101588125;
	setAttr ".wl[1437].w[12]" 0.0039244929588659865;
	setAttr -s 5 ".wl[1438].w";
	setAttr ".wl[1438].w[4]" 0.0011036551515506406;
	setAttr ".wl[1438].w[5]" 0.49746250261671715;
	setAttr ".wl[1438].w[7]" 0.49746250261671715;
	setAttr ".wl[1438].w[8]" 0.0035286581621841672;
	setAttr ".wl[1438].w[12]" 0.00044268145283091274;
	setAttr -s 5 ".wl[1439].w";
	setAttr ".wl[1439].w[4]" 0.0017844539811203253;
	setAttr ".wl[1439].w[5]" 0.50242800209707861;
	setAttr ".wl[1439].w[7]" 0.49042576870900761;
	setAttr ".wl[1439].w[8]" 0.0047176351249457174;
	setAttr ".wl[1439].w[12]" 0.00064414008784774478;
	setAttr -s 5 ".wl[1440].w";
	setAttr ".wl[1440].w[4]" 0.0072276970853081485;
	setAttr ".wl[1440].w[5]" 0.4577243097840859;
	setAttr ".wl[1440].w[7]" 0.45628715062363401;
	setAttr ".wl[1440].w[8]" 0.072581471547249074;
	setAttr ".wl[1440].w[12]" 0.0061793709597229435;
	setAttr -s 5 ".wl[1441].w";
	setAttr ".wl[1441].w[4]" 0.0044187471375469155;
	setAttr ".wl[1441].w[5]" 0.47123120035766436;
	setAttr ".wl[1441].w[7]" 0.47123120035766425;
	setAttr ".wl[1441].w[8]" 0.049312244671964545;
	setAttr ".wl[1441].w[12]" 0.0038066074751599982;
	setAttr -s 5 ".wl[1442].w[4:8]"  0.0011381091986953952 0.52318902616711516 
		0.0005655367066352894 0.46869168045123399 0.0064156474763202307;
	setAttr -s 5 ".wl[1443].w[4:8]"  0.0016646996267232186 0.57567784951674184 
		0.00074026299648428506 0.41396555440816052 0.0079516334518901308;
	setAttr -s 5 ".wl[1444].w[4:8]"  0.0043705676542472854 0.49636344475972188 
		0.0030424700134164943 0.33822147801768332 0.15800203955493097;
	setAttr -s 5 ".wl[1445].w";
	setAttr ".wl[1445].w[4]" 0.00028903023169367858;
	setAttr ".wl[1445].w[5]" 0.55102087035829705;
	setAttr ".wl[1445].w[7]" 0.44552560115657491;
	setAttr ".wl[1445].w[8]" 0.0030150429168579089;
	setAttr ".wl[1445].w[12]" 0.00014945533657646407;
	setAttr -s 5 ".wl[1446].w";
	setAttr ".wl[1446].w[4]" 4.839686246107344e-005;
	setAttr ".wl[1446].w[5]" 0.49984744830366151;
	setAttr ".wl[1446].w[7]" 0.49984744830366151;
	setAttr ".wl[1446].w[8]" 0.00021316838873350864;
	setAttr ".wl[1446].w[12]" 4.3538141482403242e-005;
	setAttr -s 5 ".wl[1447].w";
	setAttr ".wl[1447].w[4]" 0.0017033118355284679;
	setAttr ".wl[1447].w[5]" 0.49672849642130906;
	setAttr ".wl[1447].w[7]" 0.49672849642130906;
	setAttr ".wl[1447].w[8]" 0.0031192525951200356;
	setAttr ".wl[1447].w[12]" 0.0017204427267334343;
	setAttr -s 5 ".wl[1448].w";
	setAttr ".wl[1448].w[4]" 0.0010085613800062117;
	setAttr ".wl[1448].w[5]" 0.49783264737540117;
	setAttr ".wl[1448].w[7]" 0.49783264737540117;
	setAttr ".wl[1448].w[8]" 0.0023121944157576137;
	setAttr ".wl[1448].w[12]" 0.0010139494534337668;
	setAttr -s 5 ".wl[1449].w";
	setAttr ".wl[1449].w[4]" 7.9090437058149698e-005;
	setAttr ".wl[1449].w[5]" 0.49971302200546408;
	setAttr ".wl[1449].w[7]" 0.49971302200546408;
	setAttr ".wl[1449].w[8]" 0.00042017974198690184;
	setAttr ".wl[1449].w[12]" 7.4685810026785563e-005;
	setAttr -s 5 ".wl[1450].w";
	setAttr ".wl[1450].w[4]" 0.00035453166981321439;
	setAttr ".wl[1450].w[5]" 0.49747680097567804;
	setAttr ".wl[1450].w[7]" 0.49747680097567804;
	setAttr ".wl[1450].w[8]" 0.004488229867075531;
	setAttr ".wl[1450].w[12]" 0.0002036365117550916;
	setAttr -s 5 ".wl[1451].w[4:8]"  0.0031214444738091691 0.48806822130907923 
		0.0022826791093696406 0.40927466606359464 0.097252989044147337;
	setAttr -s 5 ".wl[1452].w[4:8]"  0.0031290742181741475 0.53679754863742446 
		0.0022772056846987751 0.40374791719588143 0.054048254263821226;
	setAttr -s 5 ".wl[1453].w[4:8]"  0.00031899331223158954 0.51628362657917992 
		0.00017104778245213273 0.48063323687759624 0.0025930954485401687;
	setAttr -s 5 ".wl[1454].w";
	setAttr ".wl[1454].w[4]" 0.00010616237297904887;
	setAttr ".wl[1454].w[5]" 0.49970941152471143;
	setAttr ".wl[1454].w[7]" 0.49970941152471143;
	setAttr ".wl[1454].w[8]" 0.00041221480834016937;
	setAttr ".wl[1454].w[12]" 6.2799769257929511e-005;
	setAttr -s 5 ".wl[1455].w";
	setAttr ".wl[1455].w[4]" 0.00078697424656330804;
	setAttr ".wl[1455].w[5]" 0.49822119796002512;
	setAttr ".wl[1455].w[7]" 0.49822119796002512;
	setAttr ".wl[1455].w[8]" 0.0020870979155616959;
	setAttr ".wl[1455].w[12]" 0.00068353191782476457;
	setAttr -s 5 ".wl[1456].w";
	setAttr ".wl[1456].w[4]" 0.0012486168240365076;
	setAttr ".wl[1456].w[5]" 0.49741709429365827;
	setAttr ".wl[1456].w[7]" 0.49741709429365849;
	setAttr ".wl[1456].w[8]" 0.0028215306283293309;
	setAttr ".wl[1456].w[12]" 0.0010956639603173113;
	setAttr -s 5 ".wl[1457].w";
	setAttr ".wl[1457].w[4]" 9.3791831988086739e-005;
	setAttr ".wl[1457].w[5]" 0.49976905254258236;
	setAttr ".wl[1457].w[7]" 0.49976905254258225;
	setAttr ".wl[1457].w[8]" 0.00031598405479970828;
	setAttr ".wl[1457].w[12]" 5.2119028047570106e-005;
	setAttr -s 5 ".wl[1458].w[4:8]"  0.00024733522038506296 0.62793491151347702 
		0.00011417352144206743 0.3699817526366469 0.0017218271080489465;
	setAttr -s 5 ".wl[1459].w[4:8]"  0.0044174324813547943 0.58315331631110479 
		0.0030488695696030237 0.33397931179381191 0.075401069844125465;
	setAttr -s 5 ".wl[1460].w";
	setAttr ".wl[1460].w[4]" 0.0017023213310178031;
	setAttr ".wl[1460].w[5]" 0.49724399371883637;
	setAttr ".wl[1460].w[7]" 0.4956335063154062;
	setAttr ".wl[1460].w[8]" 0.0047843679833726562;
	setAttr ".wl[1460].w[12]" 0.0006358106513670677;
	setAttr -s 5 ".wl[1461].w";
	setAttr ".wl[1461].w[4]" 0.0040915911074862537;
	setAttr ".wl[1461].w[5]" 0.49294611413298578;
	setAttr ".wl[1461].w[7]" 0.49294611413298578;
	setAttr ".wl[1461].w[8]" 0.008054415488552933;
	setAttr ".wl[1461].w[12]" 0.0019617651379892935;
	setAttr -s 5 ".wl[1462].w";
	setAttr ".wl[1462].w[4]" 0.0011929930257498225;
	setAttr ".wl[1462].w[5]" 0.49745517628960711;
	setAttr ".wl[1462].w[7]" 0.49745517628960711;
	setAttr ".wl[1462].w[8]" 0.0028353427521056822;
	setAttr ".wl[1462].w[12]" 0.0010613116429301873;
	setAttr -s 5 ".wl[1463].w";
	setAttr ".wl[1463].w[4]" 0.0016309774820706866;
	setAttr ".wl[1463].w[5]" 0.49679306503374837;
	setAttr ".wl[1463].w[7]" 0.49679306503374826;
	setAttr ".wl[1463].w[8]" 0.0031341240920266106;
	setAttr ".wl[1463].w[12]" 0.0016487683584059862;
	setAttr -s 5 ".wl[1464].w";
	setAttr ".wl[1464].w[4]" 0.0032452096387570258;
	setAttr ".wl[1464].w[5]" 0.49374804068576966;
	setAttr ".wl[1464].w[7]" 0.49374804068576955;
	setAttr ".wl[1464].w[8]" 0.0060101705523992006;
	setAttr ".wl[1464].w[12]" 0.0032485384373046032;
	setAttr -s 5 ".wl[1465].w";
	setAttr ".wl[1465].w[4]" 0.0065955124443171686;
	setAttr ".wl[1465].w[5]" 0.48410165879364703;
	setAttr ".wl[1465].w[7]" 0.48410165879364692;
	setAttr ".wl[1465].w[8]" 0.018677273680814756;
	setAttr ".wl[1465].w[12]" 0.0065238962875741593;
	setAttr -s 5 ".wl[1466].w";
	setAttr ".wl[1466].w[4]" 0.0067421285183968394;
	setAttr ".wl[1466].w[5]" 0.45760229736457403;
	setAttr ".wl[1466].w[7]" 0.45760229736457425;
	setAttr ".wl[1466].w[8]" 0.072179519255228752;
	setAttr ".wl[1466].w[12]" 0.0058737574972259985;
	setAttr -s 5 ".wl[1467].w[4:8]"  0.0069515188948649836 0.40197643988862414 
		0.0042375938698572682 0.37086476797243395 0.21596967937421974;
	setAttr -s 5 ".wl[1468].w[4:8]"  0.0042672775493891694 0.47183848326918115 
		0.0031661090094422741 0.35741865702314263 0.16330947314884481;
	setAttr -s 5 ".wl[1469].w[4:8]"  0.0044154940805352667 0.55054580794769081 
		0.0032481408652182686 0.3612977562088322 0.080492800897723288;
	setAttr -s 5 ".wl[1470].w[4:8]"  0.0048249583922658004 0.56298088826036941 
		0.0029919183005813364 0.38706992744994451 0.042132307596838996;
	setAttr -s 5 ".wl[1471].w[4:8]"  0.0016643252353218982 0.5465107811156088 
		0.00078717098096792247 0.44252839057265431 0.0085093320954471065;
	setAttr -s 5 ".wl[1472].w[4:8]"  0.0053124050352687264 0.57241105835620432 
		0.0038117024628537511 0.31074414619281926 0.10772068795285382;
	setAttr -s 5 ".wl[1473].w";
	setAttr ".wl[1473].w[4]" 0.0058954294973151532;
	setAttr ".wl[1473].w[5]" 0.4905227832252369;
	setAttr ".wl[1473].w[7]" 0.49052278322523679;
	setAttr ".wl[1473].w[8]" 0.009096092220377397;
	setAttr ".wl[1473].w[12]" 0.0039629118318337047;
	setAttr -s 5 ".wl[1474].w[4:8]"  0.012207825781592758 0.5532794093167166 
		0.0083666345418346548 0.31836627259437017 0.10777985776548581;
	setAttr -s 5 ".wl[1475].w";
	setAttr ".wl[1475].w[4]" 0.031886404301392744;
	setAttr ".wl[1475].w[5]" 0.46098409579302657;
	setAttr ".wl[1475].w[7]" 0.46098409579302646;
	setAttr ".wl[1475].w[8]" 0.034484955237128322;
	setAttr ".wl[1475].w[12]" 0.011660448875425958;
	setAttr -s 5 ".wl[1476].w[4:8]"  0.01108916620907518 0.46644004411386758 
		0.0097883009391819091 0.38522888951794504 0.1274535992199303;
	setAttr -s 5 ".wl[1477].w";
	setAttr ".wl[1477].w[4]" 0.026336465283957219;
	setAttr ".wl[1477].w[5]" 0.46427804538198847;
	setAttr ".wl[1477].w[7]" 0.46427804538198869;
	setAttr ".wl[1477].w[8]" 0.034440461710723416;
	setAttr ".wl[1477].w[12]" 0.010666982241342084;
	setAttr -s 5 ".wl[1478].w[4:8]"  0.0052188293640288803 0.4640805371947474 
		0.0048133535622149667 0.39403737044800363 0.131849909431005;
	setAttr -s 5 ".wl[1479].w";
	setAttr ".wl[1479].w[4]" 0.0047849894751466745;
	setAttr ".wl[1479].w[5]" 0.49135395258026632;
	setAttr ".wl[1479].w[7]" 0.49135395258026654;
	setAttr ".wl[1479].w[8]" 0.0090420775094662321;
	setAttr ".wl[1479].w[12]" 0.0034650278548541357;
	setAttr -s 5 ".wl[1480].w";
	setAttr ".wl[1480].w[4]" 0.00037864761738360975;
	setAttr ".wl[1480].w[5]" 0.50730539084571136;
	setAttr ".wl[1480].w[7]" 0.4909634687856021;
	setAttr ".wl[1480].w[8]" 0.0011888174602683113;
	setAttr ".wl[1480].w[12]" 0.00016367529103459788;
	setAttr -s 5 ".wl[1481].w";
	setAttr ".wl[1481].w[4]" 0.00040701628862132878;
	setAttr ".wl[1481].w[5]" 0.49888584615842507;
	setAttr ".wl[1481].w[7]" 0.49888584615842496;
	setAttr ".wl[1481].w[8]" 0.0016220108320328084;
	setAttr ".wl[1481].w[12]" 0.00019928056249589993;
	setAttr -s 5 ".wl[1482].w[4:8]"  0.0072387288661551518 0.48566794583071282 
		0.0031032838595056534 0.48566794583071304 0.018322095612913206;
	setAttr -s 5 ".wl[1483].w[4:8]"  0.0086033505652821207 0.49895946382950795 
		0.0029644302337402503 0.472344619032603 0.017128136338866725;
	setAttr -s 5 ".wl[1484].w[4:8]"  0.004421340659385411 0.58795540226544252 
		0.34602098736422249 0.0028332038739809079 0.05876906583696876;
	setAttr -s 5 ".wl[1485].w";
	setAttr ".wl[1485].w[4]" 0.011121776677129781;
	setAttr ".wl[1485].w[5]" 0.4836245041875607;
	setAttr ".wl[1485].w[6]" 0.4836245041875607;
	setAttr ".wl[1485].w[8]" 0.015309617025686012;
	setAttr ".wl[1485].w[9]" 0.0063195979220628624;
	setAttr -s 5 ".wl[1486].w[4:8]"  0.01374917609283037 0.54127123723729265 
		0.34604804761819952 0.0086362658676752725 0.090295273184002267;
	setAttr -s 5 ".wl[1487].w";
	setAttr ".wl[1487].w[4]" 0.045249356927229631;
	setAttr ".wl[1487].w[5]" 0.44711466925281146;
	setAttr ".wl[1487].w[6]" 0.44708586447482396;
	setAttr ".wl[1487].w[8]" 0.045818203053855582;
	setAttr ".wl[1487].w[9]" 0.014731906291279413;
	setAttr -s 5 ".wl[1488].w[4:8]"  0.012442821724367533 0.46641594168092104 
		0.40447071743468183 0.010068352889405023 0.10660216627062453;
	setAttr -s 5 ".wl[1489].w";
	setAttr ".wl[1489].w[4]" 0.038118040581496482;
	setAttr ".wl[1489].w[5]" 0.45095805115638776;
	setAttr ".wl[1489].w[6]" 0.45095805115638776;
	setAttr ".wl[1489].w[8]" 0.046232283030731472;
	setAttr ".wl[1489].w[9]" 0.013733574074996606;
	setAttr -s 5 ".wl[1490].w[4:8]"  0.0045564191831001981 0.48338781737119163 
		0.431740895035783 0.0037613776631951735 0.076553490746730027;
	setAttr -s 5 ".wl[1491].w";
	setAttr ".wl[1491].w[4]" 0.0092895300635635909;
	setAttr ".wl[1491].w[5]" 0.48472897620232752;
	setAttr ".wl[1491].w[6]" 0.48472897620232752;
	setAttr ".wl[1491].w[8]" 0.015528792244209905;
	setAttr ".wl[1491].w[9]" 0.0057237252875714977;
	setAttr -s 5 ".wl[1492].w";
	setAttr ".wl[1492].w[4]" 0.001563913628233577;
	setAttr ".wl[1492].w[5]" 0.49761423042174074;
	setAttr ".wl[1492].w[6]" 0.49618868470063721;
	setAttr ".wl[1492].w[8]" 0.0040298348030762057;
	setAttr ".wl[1492].w[9]" 0.00060333644631223749;
	setAttr -s 5 ".wl[1493].w";
	setAttr ".wl[1493].w[4]" 0.0015088664676461553;
	setAttr ".wl[1493].w[5]" 0.49646265677017615;
	setAttr ".wl[1493].w[6]" 0.49646265677017615;
	setAttr ".wl[1493].w[8]" 0.0049050408320762936;
	setAttr ".wl[1493].w[9]" 0.00066077915992525632;
	setAttr -s 5 ".wl[1494].w[4:8]"  0.012947343033302154 0.47658846374503577 
		0.47658846374503577 0.0054927743710045142 0.028382955105621867;
	setAttr -s 5 ".wl[1495].w[4:8]"  0.015358007803494885 0.48433256900521948 
		0.46829778843270869 0.0052789956327748862 0.026732639125802093;
	setAttr -s 5 ".wl[1496].w[4:8]"  0.0057364226626362598 0.43909153608610046 
		0.39667763912720888 0.0032040399776280845 0.15529036214642636;
	setAttr -s 5 ".wl[1497].w";
	setAttr ".wl[1497].w[4]" 0.0028091836167794064;
	setAttr ".wl[1497].w[5]" 0.49460732127143386;
	setAttr ".wl[1497].w[6]" 0.49460732127143386;
	setAttr ".wl[1497].w[8]" 0.005160622913074224;
	setAttr ".wl[1497].w[9]" 0.0028155509272787004;
	setAttr -s 5 ".wl[1498].w[4:8]"  0.0052255128352570821 0.57485690382366317 
		0.3782641674776947 0.0029427228192000993 0.038710693044184952;
	setAttr -s 5 ".wl[1499].w";
	setAttr ".wl[1499].w[4]" 0.0056259887990960785;
	setAttr ".wl[1499].w[5]" 0.49085655106202186;
	setAttr ".wl[1499].w[6]" 0.49085655106202186;
	setAttr ".wl[1499].w[8]" 0.010170216138207722;
	setAttr ".wl[1499].w[9]" 0.0024906929386524662;
	setAttr -s 5 ".wl[1500].w[4:8]"  0.0032815404681010509 0.52907089018602727 
		0.43802445844093735 0.0019908886404599179 0.027632222264474381;
	setAttr -s 5 ".wl[1501].w";
	setAttr ".wl[1501].w[4]" 0.0033905770474696364;
	setAttr ".wl[1501].w[5]" 0.49375318042602945;
	setAttr ".wl[1501].w[6]" 0.49375318042602945;
	setAttr ".wl[1501].w[8]" 0.0074954613207725233;
	setAttr ".wl[1501].w[9]" 0.001607600779699028;
	setAttr -s 5 ".wl[1502].w[4:8]"  0.0034210496677429467 0.46041857289333232 
		0.45137656890323302 0.0020622889252565769 0.082721519610435099;
	setAttr -s 5 ".wl[1503].w";
	setAttr ".wl[1503].w[4]" 0.0014577368662528818;
	setAttr ".wl[1503].w[5]" 0.49675296613391456;
	setAttr ".wl[1503].w[6]" 0.49675296613391456;
	setAttr ".wl[1503].w[8]" 0.0035779817027930018;
	setAttr ".wl[1503].w[9]" 0.0014583491631250272;
	setAttr -s 5 ".wl[1504].w";
	setAttr ".wl[1504].w[4]" 0.0050567936221157577;
	setAttr ".wl[1504].w[5]" 0.48802913829550965;
	setAttr ".wl[1504].w[6]" 0.48802913829550942;
	setAttr ".wl[1504].w[8]" 0.013869434500689653;
	setAttr ".wl[1504].w[9]" 0.005015495286175539;
	setAttr -s 5 ".wl[1505].w";
	setAttr ".wl[1505].w[4]" 0.0025321833464161483;
	setAttr ".wl[1505].w[5]" 0.49286406009538564;
	setAttr ".wl[1505].w[6]" 0.49286406009538564;
	setAttr ".wl[1505].w[8]" 0.0092269845464372328;
	setAttr ".wl[1505].w[9]" 0.0025127119163754247;
	setAttr -s 5 ".wl[1506].w[4:8]"  0.0015031263692365335 0.49662857860679199 
		0.49662857860679221 0.00059011231463741514 0.0046496041025417267;
	setAttr -s 5 ".wl[1507].w[4:8]"  0.0025194684565544505 0.49741101773649132 
		0.49282880562836728 0.00087764249422013843 0.0063630656843668629;
	setAttr -s 5 ".wl[1508].w";
	setAttr ".wl[1508].w[4]" 0.0051431918818478561;
	setAttr ".wl[1508].w[5]" 0.47006316205327048;
	setAttr ".wl[1508].w[6]" 0.4700631620532707;
	setAttr ".wl[1508].w[8]" 0.050349704640024695;
	setAttr ".wl[1508].w[9]" 0.0043807793715861963;
	setAttr -s 5 ".wl[1509].w";
	setAttr ".wl[1509].w[4]" 0.0028031263172320011;
	setAttr ".wl[1509].w[5]" 0.48237310311817966;
	setAttr ".wl[1509].w[6]" 0.48237310311817966;
	setAttr ".wl[1509].w[8]" 0.030077452667755761;
	setAttr ".wl[1509].w[9]" 0.0023732147786529822;
	setAttr -s 5 ".wl[1510].w[4:8]"  0.001339812843160414 0.51027260895971638 
		0.48065815528887434 0.00064943547787881971 0.0070799874303701287;
	setAttr -s 5 ".wl[1511].w[4:8]"  0.0021034836161092259 0.55167066346065541 
		0.43594946021901493 0.00091457234780181285 0.0093618203564186479;
	setAttr -s 5 ".wl[1512].w[4:8]"  0.003592344709423071 0.515843508591181 
		0.36433601763583601 0.0024175484898077532 0.11381058057375205;
	setAttr -s 5 ".wl[1513].w";
	setAttr ".wl[1513].w[4]" 0.00014905462577340439;
	setAttr ".wl[1513].w[5]" 0.53767015165663501;
	setAttr ".wl[1513].w[6]" 0.46065466106213238;
	setAttr ".wl[1513].w[8]" 0.0014510118674226624;
	setAttr ".wl[1513].w[9]" 7.5120788036537404e-005;
	setAttr -s 5 ".wl[1514].w";
	setAttr ".wl[1514].w[4]" 3.5019833535603059e-005;
	setAttr ".wl[1514].w[5]" 0.49989291002120989;
	setAttr ".wl[1514].w[6]" 0.49989291002120989;
	setAttr ".wl[1514].w[8]" 0.00014907164363086873;
	setAttr ".wl[1514].w[9]" 3.0088480413797984e-005;
	setAttr -s 5 ".wl[1515].w";
	setAttr ".wl[1515].w[4]" 0.0017299204750018324;
	setAttr ".wl[1515].w[5]" 0.49659124022699414;
	setAttr ".wl[1515].w[6]" 0.49659124022699402;
	setAttr ".wl[1515].w[8]" 0.003334882543681853;
	setAttr ".wl[1515].w[9]" 0.0017527165273282582;
	setAttr -s 5 ".wl[1516].w";
	setAttr ".wl[1516].w[4]" 0.00096435889195518442;
	setAttr ".wl[1516].w[5]" 0.49784438729556524;
	setAttr ".wl[1516].w[6]" 0.49784438729556524;
	setAttr ".wl[1516].w[8]" 0.0023764002936158507;
	setAttr ".wl[1516].w[9]" 0.00097046622329852628;
	setAttr -s 5 ".wl[1517].w";
	setAttr ".wl[1517].w[4]" 7.7772559791249827e-005;
	setAttr ".wl[1517].w[5]" 0.49972701514801482;
	setAttr ".wl[1517].w[6]" 0.49972701514801471;
	setAttr ".wl[1517].w[8]" 0.00039797722806070751;
	setAttr ".wl[1517].w[9]" 7.0219916118567156e-005;
	setAttr -s 5 ".wl[1518].w";
	setAttr ".wl[1518].w[4]" 0.00023195003909992241;
	setAttr ".wl[1518].w[5]" 0.49845164956050636;
	setAttr ".wl[1518].w[6]" 0.49845164956050636;
	setAttr ".wl[1518].w[8]" 0.0027350604336193688;
	setAttr ".wl[1518].w[9]" 0.00012969040626807733;
	setAttr -s 5 ".wl[1519].w[4:8]"  0.0023154005008996482 0.49913568536376307 
		0.43608549554727416 0.0016173662015973834 0.060846052386465788;
	setAttr -s 5 ".wl[1520].w[4:8]"  0.0027608358602367669 0.53012942509215599 
		0.42360022020842047 0.0019141898324513545 0.041595329006735376;
	setAttr -s 5 ".wl[1521].w[4:8]"  0.00037804375559149058 0.50501346738975583 
		0.49156868055093178 0.00019799360901528732 0.0028418146947055242;
	setAttr -s 5 ".wl[1522].w";
	setAttr ".wl[1522].w[4]" 0.00022895469842117898;
	setAttr ".wl[1522].w[5]" 0.4993980277954207;
	setAttr ".wl[1522].w[6]" 0.4993980277954207;
	setAttr ".wl[1522].w[8]" 0.00084649227693921701;
	setAttr ".wl[1522].w[9]" 0.00012849743379826249;
	setAttr -s 5 ".wl[1523].w";
	setAttr ".wl[1523].w[4]" 0.0010352372382998727;
	setAttr ".wl[1523].w[5]" 0.49771612761531764;
	setAttr ".wl[1523].w[6]" 0.49771612761531786;
	setAttr ".wl[1523].w[8]" 0.0026960189855476813;
	setAttr ".wl[1523].w[9]" 0.0008364885455168276;
	setAttr -s 5 ".wl[1524].w";
	setAttr ".wl[1524].w[4]" 0.0016879155310031459;
	setAttr ".wl[1524].w[5]" 0.49661373694279126;
	setAttr ".wl[1524].w[6]" 0.49661373694279115;
	setAttr ".wl[1524].w[8]" 0.0037004542246331373;
	setAttr ".wl[1524].w[9]" 0.0013841563587813578;
	setAttr -s 5 ".wl[1525].w";
	setAttr ".wl[1525].w[4]" 0.00020207234287127935;
	setAttr ".wl[1525].w[5]" 0.49952097864759065;
	setAttr ".wl[1525].w[6]" 0.49952097864759054;
	setAttr ".wl[1525].w[8]" 0.00064949337094364506;
	setAttr ".wl[1525].w[9]" 0.00010647699100387358;
	setAttr -s 5 ".wl[1526].w[4:8]"  0.00029666764986040634 0.58538771137688594 
		0.41226939667370915 0.00013379432906893493 0.001912429970475597;
	setAttr -s 5 ".wl[1527].w[4:8]"  0.0042472727314378858 0.57623342239545927 
		0.35241630144017044 0.0028236009966859472 0.064279402436246386;
	setAttr -s 5 ".wl[1528].w[4:8]"  0.005191439438550452 0.54531859953525164 
		0.40481564211901622 0.003138948899315216 0.041535370007866529;
	setAttr -s 5 ".wl[1529].w[4:8]"  0.0020959502922691295 0.52668797275954393 
		0.4601939670656971 0.00097543107889018477 0.010046678803599718;
	setAttr -s 5 ".wl[1530].w[4:8]"  0.0024059505093392362 0.49510459382502509 
		0.49510459382502509 0.00089034216330500084 0.0064945196773055847;
	setAttr -s 5 ".wl[1531].w";
	setAttr ".wl[1531].w[4]" 0.0053512994888832979;
	setAttr ".wl[1531].w[5]" 0.49097795211428641;
	setAttr ".wl[1531].w[6]" 0.49097795211428641;
	setAttr ".wl[1531].w[8]" 0.01025531841829575;
	setAttr ".wl[1531].w[9]" 0.0024374778642481379;
	setAttr -s 5 ".wl[1532].w";
	setAttr ".wl[1532].w[4]" 0.0016260447107779895;
	setAttr ".wl[1532].w[5]" 0.49662657532601606;
	setAttr ".wl[1532].w[6]" 0.49662657532601606;
	setAttr ".wl[1532].w[8]" 0.0037659355961336093;
	setAttr ".wl[1532].w[9]" 0.0013548690410562817;
	setAttr -s 5 ".wl[1533].w";
	setAttr ".wl[1533].w[4]" 0.0016773083696802482;
	setAttr ".wl[1533].w[5]" 0.496609685941304;
	setAttr ".wl[1533].w[6]" 0.496609685941304;
	setAttr ".wl[1533].w[8]" 0.0034023029729776519;
	setAttr ".wl[1533].w[9]" 0.0017010167747341103;
	setAttr -s 5 ".wl[1534].w";
	setAttr ".wl[1534].w[4]" 0.0026908350299311096;
	setAttr ".wl[1534].w[5]" 0.49467957725953315;
	setAttr ".wl[1534].w[6]" 0.49467957725953304;
	setAttr ".wl[1534].w[8]" 0.0052516269215210328;
	setAttr ".wl[1534].w[9]" 0.0026983835294816772;
	setAttr -s 5 ".wl[1535].w";
	setAttr ".wl[1535].w[4]" 0.004786264287148922;
	setAttr ".wl[1535].w[5]" 0.4882177985729223;
	setAttr ".wl[1535].w[6]" 0.48821779857292252;
	setAttr ".wl[1535].w[8]" 0.014024329546603934;
	setAttr ".wl[1535].w[9]" 0.0047538090204023451;
	setAttr -s 5 ".wl[1536].w";
	setAttr ".wl[1536].w[4]" 0.0048141298006158439;
	setAttr ".wl[1536].w[5]" 0.47025768620440156;
	setAttr ".wl[1536].w[6]" 0.47025768620440156;
	setAttr ".wl[1536].w[8]" 0.050487904050628349;
	setAttr ".wl[1536].w[9]" 0.0041825937399527493;
	setAttr -s 5 ".wl[1537].w[4:8]"  0.0054475494725598787 0.42808072843048478 
		0.4060288003607968 0.003267770115270385 0.15717515162088819;
	setAttr -s 5 ".wl[1538].w[4:8]"  0.00354550777256692 0.48756253507036784 
		0.38674127119389373 0.0025609131376636363 0.11958977282550785;
	setAttr -s 5 ".wl[1539].w[4:8]"  0.0042657208556582289 0.54184830702696218 
		0.38144569543099655 0.00304420227733062 0.069396074409052488;
	setAttr -s 15 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.57230964874220991 1.3926860721763035 0.26257625752009517 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -40.352460557501651 0 1;
	setAttr ".pm[2]" -type "matrix" 0.99999385132493424 -0.0035067523900789263 0 0 0.0035067523900789263 0.99999385132493424 0 0
		 0 0 1 0 -0.18994696684288531 -54.165728797419177 0 1;
	setAttr ".pm[3]" -type "matrix" 0.99999308685042898 0.0037183667584653763 0 0 -0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 0.51398399154690999 -97.427062129398365 0 1;
	setAttr ".pm[4]" -type "matrix" 0.99999308685042898 0.0037183667584653763 0 0 -0.0037183667584653763 0.99999308685042898 0 0
		 0 0 1 0 0.51398399154690999 -138.22747235176357 0 1;
	setAttr ".pm[5]" -type "matrix" 0.10680253104629513 0.62607794119939131 -0.7724115683401156 0
		 0.9863369185469486 -0.16471561548401623 0.0028721293550296877 0 -0.12543007005266102 -0.76216479685121485 -0.63511583192936649 0
		 -174.38940453763485 29.030234154332419 -0.39700992560143977 1;
	setAttr ".pm[6]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 -23.331451378769671 -170.61007839517504 29.000406755479887 1;
	setAttr ".pm[7]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 24.590050937571 -170.610083864199 29.148076936850188 1;
	setAttr ".pm[8]" -type "matrix" 0.99999308685042909 0.0037183667584656395 -4.4517113946773246e-016 0
		 -0.0037183667584659188 0.99999308685042898 -2.901413854729592e-016 0 4.9834312279951674e-016 3.3904224199484949e-016 1.0000000000000002 0
		 0.51398399154698793 -163.76676111029278 32.500962007041984 1;
	setAttr ".pm[9]" -type "matrix" 0.0069515601636596118 0.99997583761373499 1.2246383329808592e-016 0
		 0.99997583761373499 -0.0069515601636593897 -4.553685948810993e-019 0 3.9595711295480444e-019 1.2246403980182624e-016 -1 0
		 -139.839186636341 -29.032922447137789 -3.6108592907480876e-015 1;
	setAttr ".pm[10]" -type "matrix" 0.0069515601636596118 0.99997583761373499 1.2246383329808592e-016 0
		 0.99997583761373499 -0.0069515601636593897 -4.553685948810993e-019 0 3.9595711295480444e-019 1.2246403980182624e-016 -1 0
		 -139.83918663634097 -67.032922447137807 -8.2645171275080327e-015 1;
	setAttr ".pm[11]" -type "matrix" 0.99997583761373476 0.0069515601636593888 -1.2161251270773539e-016 0
		 -0.0069515601636589448 0.99997583761373476 1.2291708947071648e-016 0 1.2246403980182619e-016 -1.2206872280178055e-016 1 0
		 -114.03292244713785 -139.83918663634088 -1.9296940534924869e-014 1;
	setAttr ".pm[12]" -type "matrix" -0.0023850622471857531 -0.99999715573499348 0 0
		 0.99999715573499348 -0.0023850622471857531 0 0 0 0 1 0 -140.16017085808713 -29.659082560661336 0 1;
	setAttr ".pm[13]" -type "matrix" 0.0037183667584559459 -0.99999308685042887 0 0 0.99999308685042887 0.0037183667584559459 0 0
		 0 0 1 0 -139.74460534857457 -68.513983991545643 4.6537445170846357e-015 1;
	setAttr ".pm[14]" -type "matrix" 0.99999308685042865 0.003718366758455723 0 0 -0.003718366758455723 0.99999308685042865 0 0
		 0 0 1 0 117.55535127298513 -139.74460534857502 -1.1225503696090776e-014 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 15 ".ma";
	setAttr -s 15 ".dpf[0:14]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 15 ".lw";
	setAttr -s 15 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 15 ".ifcl";
	setAttr -s 15 ".ifcl";
createNode groupId -n "groupId5";
	rename -uid "5F642024-436C-5546-141B-709BA46A2A04";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "FF3AB7AC-40D1-0301-F986-38A518F30379";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[1122:1386]";
createNode groupId -n "groupId4";
	rename -uid "593A3A0C-4BD1-FD7E-2B04-FB8D9940DD0A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "B9F4ECB9-47A6-E28C-B229-138A59D46E3D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:1121]" "f[1387:1490]";
createNode animCurveTA -n "ctrl_upper_torso_rotateY";
	rename -uid "FD03196A-49ED-6F42-43B5-628305881DB5";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 -59.999999999999993 7 100;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "055AFD79-449E-CD52-A8CE-BE81CF46228D";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".fv" -type "string" "FBX201600";
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "E7A22387-48A3-D66E-F621-6F81C78601DE";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".eti" 2;
	setAttr ".ac[0].acn" -type "string" "sagaro_melee01";
	setAttr ".ac[0].acs" 1;
	setAttr ".ac[0].ace" 7;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201600";
	setAttr ".exp" -type "string" "C:/Users/andrew/Documents/capstone-2016-17-t18/Maya Files/Characters//Animations";
createNode gameFbxExporter -n "gameExporterPreset3";
	rename -uid "F51E5740-4037-972F-43C8-7494AF279263";
	setAttr ".pn" -type "string" "TE Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 3;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201600";
select -ne :time1;
	setAttr ".o" 2;
	setAttr ".unw" 2;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 6 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 5 ".u";
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
	setAttr -s 3 ".tx";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
connectAttr "___CTRLS___.di" "Controls.do";
connectAttr "ctrl_pot_translateX.o" "ctrl_pot.tx";
connectAttr "ctrl_pot_translateY.o" "ctrl_pot.ty";
connectAttr "ctrl_pot_translateZ.o" "ctrl_pot.tz";
connectAttr "ctrl_pot_rotateX.o" "ctrl_pot.rx";
connectAttr "ctrl_pot_rotateY.o" "ctrl_pot.ry";
connectAttr "ctrl_pot_rotateZ.o" "ctrl_pot.rz";
connectAttr "ctrl_pot_scaleX.o" "ctrl_pot.sx";
connectAttr "ctrl_pot_scaleY.o" "ctrl_pot.sy";
connectAttr "ctrl_pot_scaleZ.o" "ctrl_pot.sz";
connectAttr "ctrl_pot_visibility.o" "ctrl_pot.v";
connectAttr "ctrl_upper_torso_rotateY.o" "ctrl_upper_torso.ry";
connectAttr "jnt_pot.msg" "ik_spine_handle.hsj";
connectAttr "effector1.hp" "ik_spine_handle.hee";
connectAttr "ikSplineSolver.msg" "ik_spine_handle.hsv";
connectAttr "ik_spine_curveShape.ws" "ik_spine_handle.ic";
connectAttr "cluster1GroupId.id" "ik_spine_curveShape.iog.og[0].gid";
connectAttr "cluster1Set.mwc" "ik_spine_curveShape.iog.og[0].gco";
connectAttr "groupId3.id" "ik_spine_curveShape.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "ik_spine_curveShape.iog.og[1].gco";
connectAttr "cluster2GroupId.id" "ik_spine_curveShape.iog.og[2].gid";
connectAttr "cluster2Set.mwc" "ik_spine_curveShape.iog.og[2].gco";
connectAttr "cluster3GroupId.id" "ik_spine_curveShape.iog.og[5].gid";
connectAttr "cluster3Set.mwc" "ik_spine_curveShape.iog.og[5].gco";
connectAttr "mid_back_clstrCluster.og[0]" "ik_spine_curveShape.cr";
connectAttr "tweak1.pl[0].cp[0]" "ik_spine_curveShape.twl";
connectAttr "___JNTS___.di" "jnt_origin.do";
connectAttr "jnt_origin_parentConstraint1.ctx" "jnt_origin.tx";
connectAttr "jnt_origin_parentConstraint1.cty" "jnt_origin.ty";
connectAttr "jnt_origin_parentConstraint1.ctz" "jnt_origin.tz";
connectAttr "jnt_origin_parentConstraint1.crx" "jnt_origin.rx";
connectAttr "jnt_origin_parentConstraint1.cry" "jnt_origin.ry";
connectAttr "jnt_origin_parentConstraint1.crz" "jnt_origin.rz";
connectAttr "___JNTS___.di" "jnt_body.do";
connectAttr "jnt_origin.s" "jnt_body.is";
connectAttr "jnt_body_parentConstraint1.ctx" "jnt_body.tx";
connectAttr "jnt_body_parentConstraint1.cty" "jnt_body.ty";
connectAttr "jnt_body_parentConstraint1.ctz" "jnt_body.tz";
connectAttr "jnt_body_parentConstraint1.crx" "jnt_body.rx";
connectAttr "jnt_body_parentConstraint1.cry" "jnt_body.ry";
connectAttr "jnt_body_parentConstraint1.crz" "jnt_body.rz";
connectAttr "___JNTS___.di" "jnt_pot.do";
connectAttr "jnt_body.s" "jnt_pot.is";
connectAttr "___JNTS___.di" "spine01.do";
connectAttr "jnt_pot.s" "spine01.is";
connectAttr "___JNTS___.di" "jnt_torso.do";
connectAttr "spine01.s" "jnt_torso.is";
connectAttr "___JNTS___.di" "jnt_head.do";
connectAttr "jnt_torso.s" "jnt_head.is";
connectAttr "jnt_head_parentConstraint1.ctx" "jnt_head.tx";
connectAttr "jnt_head_parentConstraint1.cty" "jnt_head.ty";
connectAttr "jnt_head_parentConstraint1.ctz" "jnt_head.tz";
connectAttr "jnt_head_parentConstraint1.crx" "jnt_head.rx";
connectAttr "jnt_head_parentConstraint1.cry" "jnt_head.ry";
connectAttr "jnt_head_parentConstraint1.crz" "jnt_head.rz";
connectAttr "___JNTS___.di" "jnt_eye_r.do";
connectAttr "jnt_head.s" "jnt_eye_r.is";
connectAttr "___JNTS___.di" "jnt_eye_l.do";
connectAttr "jnt_head.s" "jnt_eye_l.is";
connectAttr "___JNTS___.di" "jnt_mouth.do";
connectAttr "jnt_head.s" "jnt_mouth.is";
connectAttr "jnt_head.ro" "jnt_head_parentConstraint1.cro";
connectAttr "jnt_head.pim" "jnt_head_parentConstraint1.cpim";
connectAttr "jnt_head.rp" "jnt_head_parentConstraint1.crp";
connectAttr "jnt_head.rpt" "jnt_head_parentConstraint1.crt";
connectAttr "jnt_head.jo" "jnt_head_parentConstraint1.cjo";
connectAttr "ctrl_head.t" "jnt_head_parentConstraint1.tg[0].tt";
connectAttr "ctrl_head.rp" "jnt_head_parentConstraint1.tg[0].trp";
connectAttr "ctrl_head.rpt" "jnt_head_parentConstraint1.tg[0].trt";
connectAttr "ctrl_head.r" "jnt_head_parentConstraint1.tg[0].tr";
connectAttr "ctrl_head.ro" "jnt_head_parentConstraint1.tg[0].tro";
connectAttr "ctrl_head.s" "jnt_head_parentConstraint1.tg[0].ts";
connectAttr "ctrl_head.pm" "jnt_head_parentConstraint1.tg[0].tpm";
connectAttr "jnt_head_parentConstraint1.w0" "jnt_head_parentConstraint1.tg[0].tw"
		;
connectAttr "___JNTS___.di" "jnt_shoulder_r.do";
connectAttr "jnt_torso.s" "jnt_shoulder_r.is";
connectAttr "jnt_shoulder_r_parentConstraint1.crx" "jnt_shoulder_r.rx";
connectAttr "jnt_shoulder_r_parentConstraint1.cry" "jnt_shoulder_r.ry";
connectAttr "jnt_shoulder_r_parentConstraint1.crz" "jnt_shoulder_r.rz";
connectAttr "jnt_shoulder_r_parentConstraint1.ctx" "jnt_shoulder_r.tx";
connectAttr "jnt_shoulder_r_parentConstraint1.cty" "jnt_shoulder_r.ty";
connectAttr "jnt_shoulder_r_parentConstraint1.ctz" "jnt_shoulder_r.tz";
connectAttr "___JNTS___.di" "jnt_elbow_r.do";
connectAttr "jnt_shoulder_r.s" "jnt_elbow_r.is";
connectAttr "jnt_elbow_r_parentConstraint1.ctx" "jnt_elbow_r.tx";
connectAttr "jnt_elbow_r_parentConstraint1.cty" "jnt_elbow_r.ty";
connectAttr "jnt_elbow_r_parentConstraint1.ctz" "jnt_elbow_r.tz";
connectAttr "jnt_elbow_r_parentConstraint1.crx" "jnt_elbow_r.rx";
connectAttr "jnt_elbow_r_parentConstraint1.cry" "jnt_elbow_r.ry";
connectAttr "jnt_elbow_r_parentConstraint1.crz" "jnt_elbow_r.rz";
connectAttr "___JNTS___.di" "jnt_paw_r.do";
connectAttr "jnt_elbow_r.s" "jnt_paw_r.is";
connectAttr "jnt_elbow_r.ro" "jnt_elbow_r_parentConstraint1.cro";
connectAttr "jnt_elbow_r.pim" "jnt_elbow_r_parentConstraint1.cpim";
connectAttr "jnt_elbow_r.rp" "jnt_elbow_r_parentConstraint1.crp";
connectAttr "jnt_elbow_r.rpt" "jnt_elbow_r_parentConstraint1.crt";
connectAttr "jnt_elbow_r.jo" "jnt_elbow_r_parentConstraint1.cjo";
connectAttr "ctrl_elbow_r.t" "jnt_elbow_r_parentConstraint1.tg[0].tt";
connectAttr "ctrl_elbow_r.rp" "jnt_elbow_r_parentConstraint1.tg[0].trp";
connectAttr "ctrl_elbow_r.rpt" "jnt_elbow_r_parentConstraint1.tg[0].trt";
connectAttr "ctrl_elbow_r.r" "jnt_elbow_r_parentConstraint1.tg[0].tr";
connectAttr "ctrl_elbow_r.ro" "jnt_elbow_r_parentConstraint1.tg[0].tro";
connectAttr "ctrl_elbow_r.s" "jnt_elbow_r_parentConstraint1.tg[0].ts";
connectAttr "ctrl_elbow_r.pm" "jnt_elbow_r_parentConstraint1.tg[0].tpm";
connectAttr "jnt_elbow_r_parentConstraint1.w0" "jnt_elbow_r_parentConstraint1.tg[0].tw"
		;
connectAttr "jnt_shoulder_r.ro" "jnt_shoulder_r_parentConstraint1.cro";
connectAttr "jnt_shoulder_r.pim" "jnt_shoulder_r_parentConstraint1.cpim";
connectAttr "jnt_shoulder_r.rp" "jnt_shoulder_r_parentConstraint1.crp";
connectAttr "jnt_shoulder_r.rpt" "jnt_shoulder_r_parentConstraint1.crt";
connectAttr "jnt_shoulder_r.jo" "jnt_shoulder_r_parentConstraint1.cjo";
connectAttr "ctrl_shoulder_r.t" "jnt_shoulder_r_parentConstraint1.tg[0].tt";
connectAttr "ctrl_shoulder_r.rp" "jnt_shoulder_r_parentConstraint1.tg[0].trp";
connectAttr "ctrl_shoulder_r.rpt" "jnt_shoulder_r_parentConstraint1.tg[0].trt";
connectAttr "ctrl_shoulder_r.r" "jnt_shoulder_r_parentConstraint1.tg[0].tr";
connectAttr "ctrl_shoulder_r.ro" "jnt_shoulder_r_parentConstraint1.tg[0].tro";
connectAttr "ctrl_shoulder_r.s" "jnt_shoulder_r_parentConstraint1.tg[0].ts";
connectAttr "ctrl_shoulder_r.pm" "jnt_shoulder_r_parentConstraint1.tg[0].tpm";
connectAttr "jnt_shoulder_r_parentConstraint1.w0" "jnt_shoulder_r_parentConstraint1.tg[0].tw"
		;
connectAttr "___JNTS___.di" "jnt_shoulder_l.do";
connectAttr "jnt_torso.s" "jnt_shoulder_l.is";
connectAttr "jnt_shoulder_l_parentConstraint1.ctx" "jnt_shoulder_l.tx";
connectAttr "jnt_shoulder_l_parentConstraint1.cty" "jnt_shoulder_l.ty";
connectAttr "jnt_shoulder_l_parentConstraint1.ctz" "jnt_shoulder_l.tz";
connectAttr "jnt_shoulder_l_parentConstraint1.crx" "jnt_shoulder_l.rx";
connectAttr "jnt_shoulder_l_parentConstraint1.cry" "jnt_shoulder_l.ry";
connectAttr "jnt_shoulder_l_parentConstraint1.crz" "jnt_shoulder_l.rz";
connectAttr "___JNTS___.di" "jnt_elbow_l.do";
connectAttr "jnt_shoulder_l.s" "jnt_elbow_l.is";
connectAttr "jnt_elbow_l_parentConstraint1.ctx" "jnt_elbow_l.tx";
connectAttr "jnt_elbow_l_parentConstraint1.cty" "jnt_elbow_l.ty";
connectAttr "jnt_elbow_l_parentConstraint1.ctz" "jnt_elbow_l.tz";
connectAttr "jnt_elbow_l_parentConstraint1.crx" "jnt_elbow_l.rx";
connectAttr "jnt_elbow_l_parentConstraint1.cry" "jnt_elbow_l.ry";
connectAttr "jnt_elbow_l_parentConstraint1.crz" "jnt_elbow_l.rz";
connectAttr "___JNTS___.di" "jnt_paw_l.do";
connectAttr "jnt_elbow_l.s" "jnt_paw_l.is";
connectAttr "jnt_elbow_l.ro" "jnt_elbow_l_parentConstraint1.cro";
connectAttr "jnt_elbow_l.pim" "jnt_elbow_l_parentConstraint1.cpim";
connectAttr "jnt_elbow_l.rp" "jnt_elbow_l_parentConstraint1.crp";
connectAttr "jnt_elbow_l.rpt" "jnt_elbow_l_parentConstraint1.crt";
connectAttr "jnt_elbow_l.jo" "jnt_elbow_l_parentConstraint1.cjo";
connectAttr "ctrl_elbow_l.t" "jnt_elbow_l_parentConstraint1.tg[0].tt";
connectAttr "ctrl_elbow_l.rp" "jnt_elbow_l_parentConstraint1.tg[0].trp";
connectAttr "ctrl_elbow_l.rpt" "jnt_elbow_l_parentConstraint1.tg[0].trt";
connectAttr "ctrl_elbow_l.r" "jnt_elbow_l_parentConstraint1.tg[0].tr";
connectAttr "ctrl_elbow_l.ro" "jnt_elbow_l_parentConstraint1.tg[0].tro";
connectAttr "ctrl_elbow_l.s" "jnt_elbow_l_parentConstraint1.tg[0].ts";
connectAttr "ctrl_elbow_l.pm" "jnt_elbow_l_parentConstraint1.tg[0].tpm";
connectAttr "jnt_elbow_l_parentConstraint1.w0" "jnt_elbow_l_parentConstraint1.tg[0].tw"
		;
connectAttr "jnt_shoulder_l.ro" "jnt_shoulder_l_parentConstraint1.cro";
connectAttr "jnt_shoulder_l.pim" "jnt_shoulder_l_parentConstraint1.cpim";
connectAttr "jnt_shoulder_l.rp" "jnt_shoulder_l_parentConstraint1.crp";
connectAttr "jnt_shoulder_l.rpt" "jnt_shoulder_l_parentConstraint1.crt";
connectAttr "jnt_shoulder_l.jo" "jnt_shoulder_l_parentConstraint1.cjo";
connectAttr "ctrl_shoulder_l.t" "jnt_shoulder_l_parentConstraint1.tg[0].tt";
connectAttr "ctrl_shoulder_l.rp" "jnt_shoulder_l_parentConstraint1.tg[0].trp";
connectAttr "ctrl_shoulder_l.rpt" "jnt_shoulder_l_parentConstraint1.tg[0].trt";
connectAttr "ctrl_shoulder_l.r" "jnt_shoulder_l_parentConstraint1.tg[0].tr";
connectAttr "ctrl_shoulder_l.ro" "jnt_shoulder_l_parentConstraint1.tg[0].tro";
connectAttr "ctrl_shoulder_l.s" "jnt_shoulder_l_parentConstraint1.tg[0].ts";
connectAttr "ctrl_shoulder_l.pm" "jnt_shoulder_l_parentConstraint1.tg[0].tpm";
connectAttr "jnt_shoulder_l_parentConstraint1.w0" "jnt_shoulder_l_parentConstraint1.tg[0].tw"
		;
connectAttr "jnt_torso.tx" "effector1.tx";
connectAttr "jnt_torso.ty" "effector1.ty";
connectAttr "jnt_torso.tz" "effector1.tz";
connectAttr "jnt_body.ro" "jnt_body_parentConstraint1.cro";
connectAttr "jnt_body.pim" "jnt_body_parentConstraint1.cpim";
connectAttr "jnt_body.rp" "jnt_body_parentConstraint1.crp";
connectAttr "jnt_body.rpt" "jnt_body_parentConstraint1.crt";
connectAttr "jnt_body.jo" "jnt_body_parentConstraint1.cjo";
connectAttr "ctrl_bot_back.t" "jnt_body_parentConstraint1.tg[0].tt";
connectAttr "ctrl_bot_back.rp" "jnt_body_parentConstraint1.tg[0].trp";
connectAttr "ctrl_bot_back.rpt" "jnt_body_parentConstraint1.tg[0].trt";
connectAttr "ctrl_bot_back.r" "jnt_body_parentConstraint1.tg[0].tr";
connectAttr "ctrl_bot_back.ro" "jnt_body_parentConstraint1.tg[0].tro";
connectAttr "ctrl_bot_back.s" "jnt_body_parentConstraint1.tg[0].ts";
connectAttr "ctrl_bot_back.pm" "jnt_body_parentConstraint1.tg[0].tpm";
connectAttr "jnt_body_parentConstraint1.w0" "jnt_body_parentConstraint1.tg[0].tw"
		;
connectAttr "jnt_origin.ro" "jnt_origin_parentConstraint1.cro";
connectAttr "jnt_origin.pim" "jnt_origin_parentConstraint1.cpim";
connectAttr "jnt_origin.rp" "jnt_origin_parentConstraint1.crp";
connectAttr "jnt_origin.rpt" "jnt_origin_parentConstraint1.crt";
connectAttr "jnt_origin.jo" "jnt_origin_parentConstraint1.cjo";
connectAttr "ctrl_pot.t" "jnt_origin_parentConstraint1.tg[0].tt";
connectAttr "ctrl_pot.rp" "jnt_origin_parentConstraint1.tg[0].trp";
connectAttr "ctrl_pot.rpt" "jnt_origin_parentConstraint1.tg[0].trt";
connectAttr "ctrl_pot.r" "jnt_origin_parentConstraint1.tg[0].tr";
connectAttr "ctrl_pot.ro" "jnt_origin_parentConstraint1.tg[0].tro";
connectAttr "ctrl_pot.s" "jnt_origin_parentConstraint1.tg[0].ts";
connectAttr "ctrl_pot.pm" "jnt_origin_parentConstraint1.tg[0].tpm";
connectAttr "jnt_origin_parentConstraint1.w0" "jnt_origin_parentConstraint1.tg[0].tw"
		;
connectAttr "skinCluster1.og[0]" "sagaro_bodyShape.i";
connectAttr "groupId4.id" "sagaro_bodyShape.iog.og[0].gid";
connectAttr "phongE1SG.mwc" "sagaro_bodyShape.iog.og[0].gco";
connectAttr "groupId5.id" "sagaro_bodyShape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "sagaro_bodyShape.iog.og[1].gco";
connectAttr "skinCluster1GroupId.id" "sagaro_bodyShape.iog.og[2].gid";
connectAttr "skinCluster1Set.mwc" "sagaro_bodyShape.iog.og[2].gco";
connectAttr "groupId7.id" "sagaro_bodyShape.iog.og[3].gid";
connectAttr "tweakSet2.mwc" "sagaro_bodyShape.iog.og[3].gco";
connectAttr "tweak2.vl[0].vt[0]" "sagaro_bodyShape.twl";
connectAttr "___MESH___.di" "pot_low_body.do";
connectAttr "skinCluster2GroupId.id" "pot_low_bodyShape.iog.og[0].gid";
connectAttr "skinCluster2Set.mwc" "pot_low_bodyShape.iog.og[0].gco";
connectAttr "skinCluster2.og[0]" "pot_low_bodyShape.i";
connectAttr "tweak3.vl[0].vt[0]" "pot_low_bodyShape.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phongE1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phongE1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Cage1.oc" "lambert2SG.ss";
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "Cage1.msg" "materialInfo1.m";
connectAttr "bump2d1.o" "Mat_Leg_Arm_Face.n";
connectAttr "Mat_Leg_Arm_Face.oc" "phong1SG.ss";
connectAttr "phong1SG.msg" "materialInfo2.sg";
connectAttr "Mat_Leg_Arm_Face.msg" "materialInfo2.m";
connectAttr "file1.msg" "materialInfo2.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "file1.oa" "bump2d1.bv";
connectAttr "Mat_Body.oc" "lambert3SG.ss";
connectAttr "lambert3SG.msg" "materialInfo3.sg";
connectAttr "Mat_Body.msg" "materialInfo3.m";
connectAttr "file2.oc" "Main_Character_Mat.c";
connectAttr "bump2d2.o" "Main_Character_Mat.n";
connectAttr "Main_Character_Mat.oc" "phongE1SG.ss";
connectAttr "pot_low_bodyShape.iog" "phongE1SG.dsm" -na;
connectAttr "sagaro_bodyShape.iog.og[0]" "phongE1SG.dsm" -na;
connectAttr "groupId4.msg" "phongE1SG.gn" -na;
connectAttr "phongE1SG.msg" "materialInfo4.sg";
connectAttr "Main_Character_Mat.msg" "materialInfo4.m";
connectAttr "file2.msg" "materialInfo4.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file2.ws";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr ":defaultColorMgtGlobals.cme" "file3.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file3.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file3.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file3.ws";
connectAttr "place2dTexture3.c" "file3.c";
connectAttr "place2dTexture3.tf" "file3.tf";
connectAttr "place2dTexture3.rf" "file3.rf";
connectAttr "place2dTexture3.mu" "file3.mu";
connectAttr "place2dTexture3.mv" "file3.mv";
connectAttr "place2dTexture3.s" "file3.s";
connectAttr "place2dTexture3.wu" "file3.wu";
connectAttr "place2dTexture3.wv" "file3.wv";
connectAttr "place2dTexture3.re" "file3.re";
connectAttr "place2dTexture3.of" "file3.of";
connectAttr "place2dTexture3.r" "file3.ro";
connectAttr "place2dTexture3.n" "file3.n";
connectAttr "place2dTexture3.vt1" "file3.vt1";
connectAttr "place2dTexture3.vt2" "file3.vt2";
connectAttr "place2dTexture3.vt3" "file3.vt3";
connectAttr "place2dTexture3.vc1" "file3.vc1";
connectAttr "place2dTexture3.o" "file3.uv";
connectAttr "place2dTexture3.ofs" "file3.fs";
connectAttr "file3.oa" "bump2d2.bv";
connectAttr "back.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "backShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "layerManager.dli[2]" "___MESH___.id";
connectAttr "layerManager.dli[4]" "___CTRLS___.id";
connectAttr "layerManager.dli[7]" "___JNTS___.id";
connectAttr "layerManager.dli[1]" "Spikes.id";
connectAttr "cluster1GroupParts.og" "upper_torso_cstrCluster.ip[0].ig";
connectAttr "cluster1GroupId.id" "upper_torso_cstrCluster.ip[0].gi";
connectAttr "upper_torso_cstr.wm" "upper_torso_cstrCluster.ma";
connectAttr "upper_torso_cstrShape.x" "upper_torso_cstrCluster.x";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId3.id" "tweak1.ip[0].gi";
connectAttr "cluster2GroupParts.og" "lower_back_cstrCluster.ip[0].ig";
connectAttr "cluster2GroupId.id" "lower_back_cstrCluster.ip[0].gi";
connectAttr "lower_back_cstr.wm" "lower_back_cstrCluster.ma";
connectAttr "lower_back_cstrShape.x" "lower_back_cstrCluster.x";
connectAttr "cluster3GroupParts.og" "mid_back_clstrCluster.ip[0].ig";
connectAttr "cluster3GroupId.id" "mid_back_clstrCluster.ip[0].gi";
connectAttr "mid_back_clstr.wm" "mid_back_clstrCluster.ma";
connectAttr "mid_back_clstrShape.x" "mid_back_clstrCluster.x";
connectAttr "groupId3.msg" "tweakSet1.gn" -na;
connectAttr "ik_spine_curveShape.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "ik_spine_curveShapeOrig.ws" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "cluster3GroupId.msg" "cluster3Set.gn" -na;
connectAttr "ik_spine_curveShape.iog.og[5]" "cluster3Set.dsm" -na;
connectAttr "mid_back_clstrCluster.msg" "cluster3Set.ub[0]";
connectAttr "lower_back_cstrCluster.og[0]" "cluster3GroupParts.ig";
connectAttr "cluster3GroupId.id" "cluster3GroupParts.gi";
connectAttr "cluster2GroupId.msg" "cluster2Set.gn" -na;
connectAttr "ik_spine_curveShape.iog.og[2]" "cluster2Set.dsm" -na;
connectAttr "lower_back_cstrCluster.msg" "cluster2Set.ub[0]";
connectAttr "upper_torso_cstrCluster.og[0]" "cluster2GroupParts.ig";
connectAttr "cluster2GroupId.id" "cluster2GroupParts.gi";
connectAttr "cluster1GroupId.msg" "cluster1Set.gn" -na;
connectAttr "ik_spine_curveShape.iog.og[0]" "cluster1Set.dsm" -na;
connectAttr "upper_torso_cstrCluster.msg" "cluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "cluster1GroupParts.ig";
connectAttr "cluster1GroupId.id" "cluster1GroupParts.gi";
connectAttr "jnt_origin.msg" "bindPose1.m[0]";
connectAttr "jnt_body.msg" "bindPose1.m[1]";
connectAttr "jnt_pot.msg" "bindPose1.m[2]";
connectAttr "spine01.msg" "bindPose1.m[3]";
connectAttr "jnt_torso.msg" "bindPose1.m[4]";
connectAttr "jnt_head.msg" "bindPose1.m[5]";
connectAttr "jnt_eye_r.msg" "bindPose1.m[6]";
connectAttr "jnt_eye_l.msg" "bindPose1.m[7]";
connectAttr "jnt_mouth.msg" "bindPose1.m[8]";
connectAttr "jnt_shoulder_r.msg" "bindPose1.m[9]";
connectAttr "jnt_elbow_r.msg" "bindPose1.m[10]";
connectAttr "jnt_paw_r.msg" "bindPose1.m[11]";
connectAttr "jnt_shoulder_l.msg" "bindPose1.m[12]";
connectAttr "jnt_elbow_l.msg" "bindPose1.m[13]";
connectAttr "jnt_paw_l.msg" "bindPose1.m[14]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[5]" "bindPose1.p[7]";
connectAttr "bindPose1.m[5]" "bindPose1.p[8]";
connectAttr "bindPose1.m[4]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[4]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "jnt_origin.bps" "bindPose1.wm[0]";
connectAttr "jnt_body.bps" "bindPose1.wm[1]";
connectAttr "jnt_pot.bps" "bindPose1.wm[2]";
connectAttr "spine01.bps" "bindPose1.wm[3]";
connectAttr "jnt_torso.bps" "bindPose1.wm[4]";
connectAttr "jnt_head.bps" "bindPose1.wm[5]";
connectAttr "jnt_eye_r.bps" "bindPose1.wm[6]";
connectAttr "jnt_eye_l.bps" "bindPose1.wm[7]";
connectAttr "jnt_mouth.bps" "bindPose1.wm[8]";
connectAttr "jnt_shoulder_r.bps" "bindPose1.wm[9]";
connectAttr "jnt_elbow_r.bps" "bindPose1.wm[10]";
connectAttr "jnt_paw_r.bps" "bindPose1.wm[11]";
connectAttr "jnt_shoulder_l.bps" "bindPose1.wm[12]";
connectAttr "jnt_elbow_l.bps" "bindPose1.wm[13]";
connectAttr "jnt_paw_l.bps" "bindPose1.wm[14]";
connectAttr "skinCluster2GroupParts.og" "skinCluster2.ip[0].ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2.ip[0].gi";
connectAttr "bindPose2.msg" "skinCluster2.bp";
connectAttr "jnt_origin.wm" "skinCluster2.ma[0]";
connectAttr "jnt_body.wm" "skinCluster2.ma[1]";
connectAttr "jnt_pot.wm" "skinCluster2.ma[2]";
connectAttr "spine01.wm" "skinCluster2.ma[3]";
connectAttr "jnt_torso.wm" "skinCluster2.ma[4]";
connectAttr "jnt_shoulder_r.wm" "skinCluster2.ma[9]";
connectAttr "jnt_shoulder_l.wm" "skinCluster2.ma[12]";
connectAttr "jnt_origin.liw" "skinCluster2.lw[0]";
connectAttr "jnt_body.liw" "skinCluster2.lw[1]";
connectAttr "jnt_pot.liw" "skinCluster2.lw[2]";
connectAttr "spine01.liw" "skinCluster2.lw[3]";
connectAttr "jnt_torso.liw" "skinCluster2.lw[4]";
connectAttr "jnt_shoulder_r.liw" "skinCluster2.lw[9]";
connectAttr "jnt_shoulder_l.liw" "skinCluster2.lw[12]";
connectAttr "jnt_origin.obcc" "skinCluster2.ifcl[0]";
connectAttr "jnt_body.obcc" "skinCluster2.ifcl[1]";
connectAttr "jnt_pot.obcc" "skinCluster2.ifcl[2]";
connectAttr "spine01.obcc" "skinCluster2.ifcl[3]";
connectAttr "jnt_torso.obcc" "skinCluster2.ifcl[4]";
connectAttr "jnt_shoulder_r.obcc" "skinCluster2.ifcl[9]";
connectAttr "jnt_shoulder_l.obcc" "skinCluster2.ifcl[12]";
connectAttr "pot_low_bodyShapeOrig.w" "tweak3.ip[0].ig";
connectAttr "skinCluster2GroupId.msg" "skinCluster2Set.gn" -na;
connectAttr "pot_low_bodyShape.iog.og[0]" "skinCluster2Set.dsm" -na;
connectAttr "skinCluster2.msg" "skinCluster2Set.ub[0]";
connectAttr "tweak3.og[0]" "skinCluster2GroupParts.ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2GroupParts.gi";
connectAttr "jnt_origin.msg" "bindPose2.m[0]";
connectAttr "jnt_body.msg" "bindPose2.m[1]";
connectAttr "jnt_pot.msg" "bindPose2.m[2]";
connectAttr "spine01.msg" "bindPose2.m[3]";
connectAttr "jnt_torso.msg" "bindPose2.m[4]";
connectAttr "jnt_head.msg" "bindPose2.m[5]";
connectAttr "jnt_eye_r.msg" "bindPose2.m[6]";
connectAttr "jnt_eye_l.msg" "bindPose2.m[7]";
connectAttr "jnt_mouth.msg" "bindPose2.m[8]";
connectAttr "jnt_shoulder_r.msg" "bindPose2.m[9]";
connectAttr "jnt_elbow_r.msg" "bindPose2.m[10]";
connectAttr "jnt_paw_r.msg" "bindPose2.m[11]";
connectAttr "jnt_shoulder_l.msg" "bindPose2.m[12]";
connectAttr "jnt_elbow_l.msg" "bindPose2.m[13]";
connectAttr "jnt_paw_l.msg" "bindPose2.m[14]";
connectAttr "bindPose2.w" "bindPose2.p[0]";
connectAttr "bindPose2.m[0]" "bindPose2.p[1]";
connectAttr "bindPose2.m[1]" "bindPose2.p[2]";
connectAttr "bindPose2.m[2]" "bindPose2.p[3]";
connectAttr "bindPose2.m[3]" "bindPose2.p[4]";
connectAttr "bindPose2.m[4]" "bindPose2.p[5]";
connectAttr "bindPose2.m[5]" "bindPose2.p[6]";
connectAttr "bindPose2.m[5]" "bindPose2.p[7]";
connectAttr "bindPose2.m[5]" "bindPose2.p[8]";
connectAttr "bindPose2.m[4]" "bindPose2.p[9]";
connectAttr "bindPose2.m[9]" "bindPose2.p[10]";
connectAttr "bindPose2.m[10]" "bindPose2.p[11]";
connectAttr "bindPose2.m[4]" "bindPose2.p[12]";
connectAttr "bindPose2.m[12]" "bindPose2.p[13]";
connectAttr "bindPose2.m[13]" "bindPose2.p[14]";
connectAttr "groupId7.msg" "tweakSet2.gn" -na;
connectAttr "sagaro_bodyShape.iog.og[3]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "sagaro_bodyShape.iog.og[2]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "groupParts4.og" "groupParts6.ig";
connectAttr "groupId7.id" "groupParts6.gi";
connectAttr "groupParts6.og" "tweak2.ip[0].ig";
connectAttr "groupId7.id" "tweak2.ip[0].gi";
connectAttr "tweak2.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "jnt_origin.wm" "skinCluster1.ma[0]";
connectAttr "jnt_body.wm" "skinCluster1.ma[1]";
connectAttr "jnt_pot.wm" "skinCluster1.ma[2]";
connectAttr "spine01.wm" "skinCluster1.ma[3]";
connectAttr "jnt_torso.wm" "skinCluster1.ma[4]";
connectAttr "jnt_head.wm" "skinCluster1.ma[5]";
connectAttr "jnt_eye_r.wm" "skinCluster1.ma[6]";
connectAttr "jnt_eye_l.wm" "skinCluster1.ma[7]";
connectAttr "jnt_mouth.wm" "skinCluster1.ma[8]";
connectAttr "jnt_shoulder_r.wm" "skinCluster1.ma[9]";
connectAttr "jnt_elbow_r.wm" "skinCluster1.ma[10]";
connectAttr "jnt_paw_r.wm" "skinCluster1.ma[11]";
connectAttr "jnt_shoulder_l.wm" "skinCluster1.ma[12]";
connectAttr "jnt_elbow_l.wm" "skinCluster1.ma[13]";
connectAttr "jnt_paw_l.wm" "skinCluster1.ma[14]";
connectAttr "jnt_origin.liw" "skinCluster1.lw[0]";
connectAttr "jnt_body.liw" "skinCluster1.lw[1]";
connectAttr "jnt_pot.liw" "skinCluster1.lw[2]";
connectAttr "spine01.liw" "skinCluster1.lw[3]";
connectAttr "jnt_torso.liw" "skinCluster1.lw[4]";
connectAttr "jnt_head.liw" "skinCluster1.lw[5]";
connectAttr "jnt_eye_r.liw" "skinCluster1.lw[6]";
connectAttr "jnt_eye_l.liw" "skinCluster1.lw[7]";
connectAttr "jnt_mouth.liw" "skinCluster1.lw[8]";
connectAttr "jnt_shoulder_r.liw" "skinCluster1.lw[9]";
connectAttr "jnt_elbow_r.liw" "skinCluster1.lw[10]";
connectAttr "jnt_paw_r.liw" "skinCluster1.lw[11]";
connectAttr "jnt_shoulder_l.liw" "skinCluster1.lw[12]";
connectAttr "jnt_elbow_l.liw" "skinCluster1.lw[13]";
connectAttr "jnt_paw_l.liw" "skinCluster1.lw[14]";
connectAttr "jnt_origin.obcc" "skinCluster1.ifcl[0]";
connectAttr "jnt_body.obcc" "skinCluster1.ifcl[1]";
connectAttr "jnt_pot.obcc" "skinCluster1.ifcl[2]";
connectAttr "spine01.obcc" "skinCluster1.ifcl[3]";
connectAttr "jnt_torso.obcc" "skinCluster1.ifcl[4]";
connectAttr "jnt_head.obcc" "skinCluster1.ifcl[5]";
connectAttr "jnt_eye_r.obcc" "skinCluster1.ifcl[6]";
connectAttr "jnt_eye_l.obcc" "skinCluster1.ifcl[7]";
connectAttr "jnt_mouth.obcc" "skinCluster1.ifcl[8]";
connectAttr "jnt_shoulder_r.obcc" "skinCluster1.ifcl[9]";
connectAttr "jnt_elbow_r.obcc" "skinCluster1.ifcl[10]";
connectAttr "jnt_paw_r.obcc" "skinCluster1.ifcl[11]";
connectAttr "jnt_shoulder_l.obcc" "skinCluster1.ifcl[12]";
connectAttr "jnt_elbow_l.obcc" "skinCluster1.ifcl[13]";
connectAttr "jnt_paw_l.obcc" "skinCluster1.ifcl[14]";
connectAttr "spine01.msg" "skinCluster1.ptt";
connectAttr "groupParts3.og" "groupParts4.ig";
connectAttr "groupId5.id" "groupParts4.gi";
connectAttr "sagaro_bodyShapeOrig.w" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "phong1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "phongE1SG.pa" ":renderPartition.st" -na;
connectAttr "Cage1.msg" ":defaultShaderList1.s" -na;
connectAttr "Mat_Leg_Arm_Face.msg" ":defaultShaderList1.s" -na;
connectAttr "Mat_Body.msg" ":defaultShaderList1.s" -na;
connectAttr "Main_Character_Mat.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file3.msg" ":defaultTextureList1.tx" -na;
connectAttr "sagaro_bodyShape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Rig_Tester.ma
