//Maya ASCII 2017 scene
//Name: Making_My_Way_Down_Town3.ma
//Last modified: Wed, Nov 16, 2016 06:05:38 PM
//Codeset: 1252
file -rdi 1 -ns "sagaro_skinWeighting" -rfn "sagaro_skinWeightingRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/scott.delong/Desktop/repo/Maya Files/Characters//scenes/sagaro_skinWeighting.ma";
file -r -ns "sagaro_skinWeighting" -dr 1 -rfn "sagaro_skinWeightingRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/scott.delong/Desktop/repo/Maya Files/Characters//scenes/sagaro_skinWeighting.ma";
requires maya "2017";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "183DAF57-4643-64FF-18E1-53A3D517D0B3";
	setAttr ".t" -type "double3" 261.26968780201997 258.09797240595617 327.06549566108066 ;
	setAttr ".r" -type "double3" -29.138352741881686 -2.6000000000093775 -9.9494756703952162e-017 ;
	setAttr ".rp" -type "double3" 5.6843418860808015e-014 -7.1054273576010019e-014 0 ;
	setAttr ".rpt" -type "double3" -1.0802388439474324e-013 9.4165950089496259e-016 
		-2.728855724444568e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "4C748BC9-4CE4-1933-BCB7-74975796C8B7";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 372.42186119512269;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 276.02583983976325 76.758260726928711 2.1097946166992187 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "9BF6D462-4268-A339-36B0-E492AFABDD63";
	setAttr ".t" -type "double3" 271.76563348571966 1040.9719823824171 1.6375189065165676 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2A2DDD5C-4F6B-3A77-2107-D6B4036CAAC2";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 923.97965003558977;
	setAttr ".ow" 316.17293426794208;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 271.76563348571966 116.99233234682698 1.6375189065163624 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "74240459-45B1-1A41-7A6F-0DACB2A1A954";
	setAttr ".t" -type "double3" 211.84733401937368 39.063520032478934 1033.6532825184183 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6B7714AD-4640-18A6-8139-21AC4D161C89";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1091.3968081886192;
	setAttr ".ow" 18.315284470965743;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 267.24936405690306 26.302956737316293 -57.74352567020091 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "BD9AAD54-4699-EC9C-4281-18A86E3533D1";
	setAttr ".t" -type "double3" 1012.8890566925147 42.117343342953653 85.946656375352887 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "4BB34EA4-48F4-2AE8-E587-12861F7A4084";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 745.6396926356116;
	setAttr ".ow" 25.984176265846113;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 267.24936405690306 26.816519514689531 63.274654889341704 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube18";
	rename -uid "F28FC029-4BB4-FB8A-98D9-FDAA6AB88600";
	setAttr ".t" -type "double3" 0 0 -207.87097999043726 ;
	setAttr ".s" -type "double3" 10.432084689007166 10.432084689007166 5.4970321724841655 ;
createNode mesh -n "pCubeShape18" -p "pCube18";
	rename -uid "6B677948-4A49-E093-7F75-D8A1CA23A89B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.37500002235174179 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 21 ".pt";
	setAttr ".pt[1]" -type "float3" -0.078201421 0.0027488621 0 ;
	setAttr ".pt[2]" -type "float3" 0.063954264 -0.0062240241 0 ;
	setAttr ".pt[3]" -type "float3" -0.017522372 0.13234748 0 ;
	setAttr ".pt[4]" -type "float3" -0.064330228 -0.0062240241 0 ;
	setAttr ".pt[5]" -type "float3" -0.12297657 0.13234748 0.26538259 ;
	setAttr ".pt[7]" -type "float3" -0.078201421 0.0027488621 0 ;
	setAttr ".pt[8]" -type "float3" 0 -0.31064835 0 ;
	setAttr ".pt[9]" -type "float3" 0 -0.31064835 0 ;
	setAttr ".pt[10]" -type "float3" -0.1282845 0 0 ;
	setAttr ".pt[11]" -type "float3" -0.1282845 0 0 ;
	setAttr ".pt[12]" -type "float3" -0.063954264 0.0062240236 0 ;
	setAttr ".pt[13]" -type "float3" -0.1282845 0 0 ;
	setAttr ".pt[14]" -type "float3" -0.017612031 -0.002748861 0 ;
	setAttr ".pt[15]" -type "float3" -0.12306623 -0.002748861 0.26538259 ;
	setAttr ".pt[16]" -type "float3" -0.1282845 0 0 ;
	setAttr ".pt[17]" -type "float3" -0.19223873 0.0062240236 0 ;
	setAttr ".pt[18]" -type "float3" 0 -0.31064835 0 ;
	setAttr ".pt[19]" -type "float3" -0.1282845 0 0 ;
	setAttr ".pt[20]" -type "float3" -0.11076212 0.31422716 0 ;
	setAttr ".pt[21]" -type "float3" 0.017522372 0.31422716 0 ;
	setAttr ".pt[23]" -type "float3" 0 -0.31064835 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube19";
	rename -uid "F38662A9-48FC-77AE-8DAC-36955132E9E8";
	setAttr ".rp" -type "double3" 279.72131025307522 76.754107154193463 -6.063689139246172 ;
	setAttr ".sp" -type "double3" 279.72131025307522 76.754107154193463 -6.063689139246172 ;
createNode transform -n "polySurface1" -p "pCube19";
	rename -uid "EB13569B-4136-2315-51A2-7FB97279DA13";
	setAttr ".t" -type "double3" -1.4807519803437685 0 0 ;
	setAttr ".rp" -type "double3" 199.79065704345703 76.758264541625977 -88.998802185058594 ;
	setAttr ".sp" -type "double3" 199.79065704345703 76.758264541625977 -88.998802185058594 ;
createNode transform -n "transform44" -p "polySurface1";
	rename -uid "6CC63042-44BC-DEF4-3F79-B0A6BA4B35D2";
createNode mesh -n "polySurfaceShape2" -p "transform44";
	rename -uid "6322AAF1-41F7-E874-D19C-00BA90C4C535";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface2" -p "pCube19";
	rename -uid "EAF602C0-4102-C793-A850-03B2BF242FDD";
	setAttr ".t" -type "double3" -3.3799047247332297 0 0 ;
createNode transform -n "transform47" -p "|pCube19|polySurface2";
	rename -uid "F3AB1096-4DF7-3539-4C9F-10B1424FA8E1";
createNode mesh -n "polySurfaceShape3" -p "transform47";
	rename -uid "DE6106D3-45CE-0718-16E0-98A1DDF2E283";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface3" -p "pCube19";
	rename -uid "2FD33DC6-4003-1387-7354-77B08CA59F17";
	setAttr ".t" -type "double3" -2.3300495663210938 0 0 ;
	setAttr ".rp" -type "double3" 199.79066467285156 76.758258819580078 93.218345642089844 ;
	setAttr ".sp" -type "double3" 199.79066467285156 76.758258819580078 93.218345642089844 ;
createNode transform -n "transform46" -p "polySurface3";
	rename -uid "6A02D274-4BCB-BF3F-1FC5-69BEF83FC2E1";
createNode mesh -n "polySurfaceShape4" -p "transform46";
	rename -uid "B236B30F-4529-FCC0-36D7-5AB13B3C7CA6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface4" -p "pCube19";
	rename -uid "4B1BB924-470B-E4FB-7047-D9862147665B";
	setAttr ".t" -type "double3" -2.1802272291512281 0 0 ;
createNode transform -n "transform49" -p "|pCube19|polySurface4";
	rename -uid "EE70D52B-48D9-671C-94D7-F1B85E52CCBE";
createNode mesh -n "polySurfaceShape5" -p "transform49";
	rename -uid "9D497FB3-4C5B-7FC5-8199-87856413AD21";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  1.5258789e-005 -1.9073486e-006 
		0 1.5258789e-005 -1.9073486e-006 0 1.5258789e-005 -1.5258789e-005 -1.335144e-005 
		1.5258789e-005 -1.5258789e-005 -1.335144e-005;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface5" -p "pCube19";
	rename -uid "6E1ED85B-434E-BDDD-0A4A-21993AB75212";
	setAttr ".t" -type "double3" -2.6716976214511519 0 0 ;
createNode transform -n "transform45" -p "polySurface5";
	rename -uid "B2DB179F-4020-2D10-11D9-6FA43505125C";
createNode mesh -n "polySurfaceShape6" -p "transform45";
	rename -uid "7EDC3310-4CB2-1D4E-7683-E7B48D983ABA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.3724520206451416 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface6" -p "pCube19";
	rename -uid "B618FE60-459C-5834-9E30-34BDEAF6149E";
createNode transform -n "transform13" -p "polySurface6";
	rename -uid "D4FC43E2-490C-0745-D036-6BB7BE7653E3";
createNode mesh -n "polySurfaceShape7" -p "transform13";
	rename -uid "2409BD05-4D20-D0B2-A325-5683B1B972FE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface7" -p "pCube19";
	rename -uid "F590CFF5-427B-6794-8977-3498AB18E371";
createNode transform -n "transform40" -p "|pCube19|polySurface7";
	rename -uid "43D775FC-4700-5594-ED36-8B85F7AB5B63";
createNode mesh -n "polySurfaceShape8" -p "transform40";
	rename -uid "CF96DB0B-4419-CF5E-1A3A-8CBAD574A5F6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface8" -p "pCube19";
	rename -uid "E7C35616-4A64-30FF-66C4-179D1590F9EC";
createNode transform -n "transform38" -p "polySurface8";
	rename -uid "0F56FF78-4344-DA77-6025-E6B3D6BBBF1A";
createNode mesh -n "polySurfaceShape9" -p "transform38";
	rename -uid "BC3CE121-43D5-0DF1-BDB7-31BA546BFC98";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface9" -p "pCube19";
	rename -uid "47BD6047-4324-6C24-9184-4B97362797B6";
createNode transform -n "transform39" -p "polySurface9";
	rename -uid "AFD171DB-4852-95DE-925C-EBA55BEFA55F";
createNode mesh -n "polySurfaceShape10" -p "transform39";
	rename -uid "1E5ACEC8-441C-5FB3-309F-5FB8D6788A0A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface10" -p "pCube19";
	rename -uid "95E7899A-479E-5A4B-A274-FD8410887FEA";
createNode transform -n "transform14" -p "polySurface10";
	rename -uid "5D31E755-466B-3069-8152-668C7F64BCAA";
createNode mesh -n "polySurfaceShape11" -p "transform14";
	rename -uid "8A737281-49CB-4876-60C7-EC86A63E6275";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface11" -p "pCube19";
	rename -uid "16798229-4F7B-317E-0E5A-7C99783BBDB7";
createNode transform -n "transform17" -p "polySurface11";
	rename -uid "85DF3E74-4C56-9804-C1A8-BE9B47C89010";
createNode mesh -n "polySurfaceShape12" -p "transform17";
	rename -uid "D7014187-4687-097C-16C8-91A2EA93840A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface12" -p "pCube19";
	rename -uid "551ADEFE-4C52-304A-60DC-35B94D9A653D";
createNode transform -n "transform18" -p "polySurface12";
	rename -uid "E9680E43-44E0-EDC8-01DD-4A8958D69589";
createNode mesh -n "polySurfaceShape13" -p "transform18";
	rename -uid "4D89C391-487B-6835-6A05-A38C424236FA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface13" -p "pCube19";
	rename -uid "3D28F61E-433D-9772-228C-C0A8834FD083";
createNode transform -n "transform19" -p "polySurface13";
	rename -uid "A9AB3880-44FB-B387-41F9-52841454F13E";
createNode mesh -n "polySurfaceShape14" -p "transform19";
	rename -uid "0ABBF245-4BAD-5CE8-E2F6-4085E729167B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface14" -p "pCube19";
	rename -uid "10B2827F-4899-DD2B-E7F1-529390DFEA29";
createNode transform -n "transform20" -p "polySurface14";
	rename -uid "70ABB97B-4EAA-CE50-9B93-A989525B8067";
createNode mesh -n "polySurfaceShape15" -p "transform20";
	rename -uid "C5763B48-4A92-865C-A396-5D9B67C9645E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface15" -p "pCube19";
	rename -uid "C08F7F95-48EF-FBE8-3FC6-CDA00727D81B";
createNode transform -n "transform21" -p "polySurface15";
	rename -uid "4D861807-4046-CBD9-A071-20A5128BE1CE";
createNode mesh -n "polySurfaceShape16" -p "transform21";
	rename -uid "FBEA66CB-4E93-D224-B237-DAAA709CB769";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface16" -p "pCube19";
	rename -uid "6C5E57D4-4CAB-CDEC-EED8-26B9ECB46D86";
createNode transform -n "transform42" -p "|pCube19|polySurface16";
	rename -uid "9BCA771E-4E7D-C555-047A-C6A7BED0C1B9";
createNode mesh -n "polySurfaceShape17" -p "transform42";
	rename -uid "8382DB08-4AA0-1760-C70F-45B16C166511";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -0.15241497755050659 0.025421082973480225 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface17" -p "pCube19";
	rename -uid "1CB14DFF-4408-6DBB-25D2-069A239AC1F6";
createNode transform -n "transform15" -p "polySurface17";
	rename -uid "72F2DB3D-443B-1840-75A5-9CAA38BA7DD3";
createNode mesh -n "polySurfaceShape18" -p "transform15";
	rename -uid "6293991D-4288-470B-E2C3-7D8692C1A35A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface18" -p "pCube19";
	rename -uid "8E1C01C9-40C4-86AA-2462-DAA5CC34FC38";
createNode transform -n "transform16" -p "polySurface18";
	rename -uid "0431F281-45CE-C3B6-27D9-85948BBBAA79";
createNode mesh -n "polySurfaceShape19" -p "transform16";
	rename -uid "0DDE3DDC-4456-E39A-8385-6EB45673971E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface19" -p "pCube19";
	rename -uid "E255514F-4C42-31F2-17C6-6BB80CDF7FF8";
createNode transform -n "transform22" -p "polySurface19";
	rename -uid "C754F10B-479D-62D6-3AF7-F08773F6ACC8";
createNode mesh -n "polySurfaceShape20" -p "transform22";
	rename -uid "D9ECEDEE-47B6-AD50-69B5-8F8EBC496BAB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface20" -p "pCube19";
	rename -uid "E605DED6-41A8-E7C6-2F09-2C91B226E5B6";
createNode transform -n "transform10" -p "polySurface20";
	rename -uid "25918ED1-4E64-7EAF-0A96-2191B04B68B6";
createNode mesh -n "polySurfaceShape21" -p "transform10";
	rename -uid "134F5522-450E-4E24-9B76-728C62FD2872";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface21" -p "pCube19";
	rename -uid "562BE059-4BFC-36BA-93FE-EA89BB732573";
createNode transform -n "transform6" -p "polySurface21";
	rename -uid "A6BE2129-4A69-4B07-7FD5-EEB9289A4201";
createNode mesh -n "polySurfaceShape22" -p "transform6";
	rename -uid "317B5C94-4F28-B1BC-610D-E7A991FB5641";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface22" -p "pCube19";
	rename -uid "EDDB80CF-46B4-F61C-5B81-88A96D789C5D";
createNode transform -n "transform7" -p "polySurface22";
	rename -uid "26A0E6B5-4166-FB8C-F7A1-7F87D1EBFA0A";
createNode mesh -n "polySurfaceShape23" -p "transform7";
	rename -uid "A22A30E5-4B68-8DE7-79D9-9882363269D0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface23" -p "pCube19";
	rename -uid "FDD32087-48C0-7B36-AC90-938E0D60D55E";
createNode transform -n "transform8" -p "polySurface23";
	rename -uid "46F82F11-40F1-5E89-086C-359C5595743F";
createNode mesh -n "polySurfaceShape24" -p "transform8";
	rename -uid "637D044E-42E5-35C8-96A9-19BF946BDEEB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface24" -p "pCube19";
	rename -uid "586BCE7C-4417-424A-1720-5FB345E17917";
createNode transform -n "transform9" -p "polySurface24";
	rename -uid "470F4858-45C3-DAF5-6890-05BD73B06DDD";
createNode mesh -n "polySurfaceShape25" -p "transform9";
	rename -uid "DC8AC888-49BB-79FE-F039-29ABCB46D9E8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface25" -p "pCube19";
	rename -uid "17833A2C-49DD-EBC0-FE93-73B82FA0A88F";
createNode transform -n "transform2" -p "polySurface25";
	rename -uid "39E7BEBD-4B91-5777-8FE0-AB99A01FD077";
createNode mesh -n "polySurfaceShape26" -p "transform2";
	rename -uid "5B5095AA-487E-2407-298F-D3A4513C7487";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface26" -p "pCube19";
	rename -uid "845D31AF-4586-ADA8-4496-0DBF65D4412E";
createNode transform -n "transform3" -p "polySurface26";
	rename -uid "E49185BB-46A7-6E25-9EF1-EC84FA71DFEB";
createNode mesh -n "polySurfaceShape27" -p "transform3";
	rename -uid "7AAD40D9-410F-57F4-EAC4-02B6774FDB3D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface27" -p "pCube19";
	rename -uid "707A7A4D-4611-DEA3-E3B7-DE9574A1E2BF";
createNode transform -n "transform4" -p "polySurface27";
	rename -uid "098B9752-4AA4-74F5-3305-458054B0DE5A";
createNode mesh -n "polySurfaceShape28" -p "transform4";
	rename -uid "EE934996-47D2-C6E1-CD3A-A1A649624DC5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface28" -p "pCube19";
	rename -uid "A41489F7-49B0-F1DE-5BCF-65A9CF2CA4CC";
createNode transform -n "transform5" -p "polySurface28";
	rename -uid "B987E97E-4BF7-5D2F-4A04-E580FE62B087";
createNode mesh -n "polySurfaceShape29" -p "transform5";
	rename -uid "7DAEE693-4A93-9F96-E919-18924C254801";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface29" -p "pCube19";
	rename -uid "6A3C8EA5-4825-054C-8AA4-E6B90A2CBD23";
createNode transform -n "transform11" -p "polySurface29";
	rename -uid "F5B9E1BE-4085-B87E-2C1C-43BE98C6DF40";
createNode mesh -n "polySurfaceShape30" -p "transform11";
	rename -uid "67F7B8F1-4026-D39C-8D76-4EB8775F8E4E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface30" -p "pCube19";
	rename -uid "B8AB2162-4FA7-989B-5636-2D92AD12856E";
createNode transform -n "transform12" -p "polySurface30";
	rename -uid "76635D20-43CC-0FFA-FCBB-A19CF8360538";
createNode mesh -n "polySurfaceShape31" -p "transform12";
	rename -uid "7FACDF8E-403C-68A7-60A7-27945659B9C2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface31" -p "pCube19";
	rename -uid "8A5D6AAC-45A8-BB36-7F5C-C689F9FFE704";
createNode transform -n "transform23" -p "polySurface31";
	rename -uid "DB00F31C-4785-E2E4-683E-9C993BCDDA69";
createNode mesh -n "polySurfaceShape32" -p "transform23";
	rename -uid "F3E38F7F-4A49-1673-EA69-CAAE7C4ED151";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface32" -p "pCube19";
	rename -uid "247DF45D-4867-2D29-4F32-069AD001D5FF";
createNode transform -n "transform24" -p "polySurface32";
	rename -uid "2F862807-4B6A-ACAD-6387-A08B16FF1679";
createNode mesh -n "polySurfaceShape33" -p "transform24";
	rename -uid "76887D72-43A6-3F5C-156A-3B85A6C597AF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface33" -p "pCube19";
	rename -uid "CD9A66D0-4AAF-12AA-E6CE-1B938472AE0D";
createNode transform -n "transform25" -p "polySurface33";
	rename -uid "5C29F828-4040-B317-E03A-608E8B5A5117";
createNode mesh -n "polySurfaceShape34" -p "transform25";
	rename -uid "99C47490-45B3-5145-975A-73A392556BE5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface34" -p "pCube19";
	rename -uid "DB913A53-4DA4-6AD9-85AE-77AC5C5BE987";
createNode transform -n "transform26" -p "polySurface34";
	rename -uid "A63B7EBD-4C6C-DB1E-01FB-E3B0AFFEF8F6";
createNode mesh -n "polySurfaceShape35" -p "transform26";
	rename -uid "85D7EFC8-4EF6-78CF-09E3-CD89CCB219A9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface35" -p "pCube19";
	rename -uid "A5A7C662-497B-5999-F1F3-39A4A602B39C";
createNode transform -n "transform27" -p "polySurface35";
	rename -uid "A3FD7A23-4CC9-86F2-6D3D-F0B015814617";
createNode mesh -n "polySurfaceShape36" -p "transform27";
	rename -uid "7093C3F9-4589-839A-7272-7F92C04B7F4D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface36" -p "pCube19";
	rename -uid "785554B9-433E-D60C-50C8-4BB5BB3E7736";
createNode transform -n "transform28" -p "polySurface36";
	rename -uid "B8C8ACF8-4581-2BF4-DF9B-7DAF1A668B15";
createNode mesh -n "polySurfaceShape37" -p "transform28";
	rename -uid "F50B2D7B-44C4-9031-334A-FA9AA8C1C8FF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface37" -p "pCube19";
	rename -uid "25605B6E-4790-37DE-871A-618DED06E444";
createNode transform -n "transform29" -p "polySurface37";
	rename -uid "134C5860-411C-33D9-CB6F-99B17A84FAAE";
createNode mesh -n "polySurfaceShape38" -p "transform29";
	rename -uid "785BCF46-46F2-E712-29F7-51BFDC194942";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface38" -p "pCube19";
	rename -uid "D7D2CE8B-41C1-A4C6-549B-64B1D8ED7079";
createNode transform -n "transform30" -p "polySurface38";
	rename -uid "90A070A5-420B-AE2E-B1C7-08B4191326E4";
createNode mesh -n "polySurfaceShape39" -p "transform30";
	rename -uid "39121FF9-4478-C258-3549-F6AAA4A9308B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface39" -p "pCube19";
	rename -uid "770A610F-4983-349F-B073-DD83A2A43C19";
createNode transform -n "transform31" -p "polySurface39";
	rename -uid "5EEBF41D-4BDA-35F6-CDDD-AFBB9720998B";
createNode mesh -n "polySurfaceShape40" -p "transform31";
	rename -uid "6881121E-4349-DE44-6A74-0AA22DF13EE3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface40" -p "pCube19";
	rename -uid "25F01243-484C-40CA-17AB-19B842AD1A84";
createNode transform -n "transform32" -p "polySurface40";
	rename -uid "2797CE60-4FA4-128E-AC40-8DA866844646";
createNode mesh -n "polySurfaceShape41" -p "transform32";
	rename -uid "48F2E0B7-4721-743A-FF08-2E9DD8544A7B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface41" -p "pCube19";
	rename -uid "039941C4-477F-FC66-29BF-A1B0ABE8D337";
createNode transform -n "transform33" -p "polySurface41";
	rename -uid "20E6E8B0-4D6F-2311-8497-7DB706F9289C";
createNode mesh -n "polySurfaceShape42" -p "transform33";
	rename -uid "C7E36CFC-4AFE-B7E0-2D2F-599C4B23CABC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface42" -p "pCube19";
	rename -uid "E8E2E90D-47EC-8FD7-D2CC-24AF1E32FCD8";
createNode transform -n "transform34" -p "polySurface42";
	rename -uid "DEA4B43A-4743-7996-06DA-93B5AB435623";
createNode mesh -n "polySurfaceShape43" -p "transform34";
	rename -uid "EF99F417-4086-C046-F4E5-37916C6FE4A7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface43" -p "pCube19";
	rename -uid "1D6FF48E-4B27-C41C-BC54-EBA1292096AC";
createNode transform -n "transform35" -p "polySurface43";
	rename -uid "6CB6A73A-42A3-0364-88B5-C293BD438E09";
createNode mesh -n "polySurfaceShape44" -p "transform35";
	rename -uid "322BE3F0-4217-F98E-80D3-B4834D3E8521";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface44" -p "pCube19";
	rename -uid "509736D4-4CFA-D898-F586-86ABC9BDE866";
createNode transform -n "transform36" -p "polySurface44";
	rename -uid "DF492F8E-4F43-C415-F8D7-2AA0F0AEC8F1";
createNode mesh -n "polySurfaceShape45" -p "transform36";
	rename -uid "9F1A3E31-42A9-C233-DF45-0D99F3E77F3B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface45" -p "pCube19";
	rename -uid "4C32147B-4B0B-1EBF-FAEF-8B873287FDDA";
createNode transform -n "transform37" -p "polySurface45";
	rename -uid "918C271B-469D-BFE0-513F-518AF0F4B5B5";
createNode mesh -n "polySurfaceShape46" -p "transform37";
	rename -uid "EC24E923-439E-89F5-3317-4DBA15F4F3B8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform1" -p "pCube19";
	rename -uid "F47F968C-435C-03DD-2528-6C9E1D6042D6";
createNode mesh -n "pCube19Shape" -p "transform1";
	rename -uid "AE255405-4D79-0AA6-536D-A3AFF71B3125";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:296]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 696 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.47918677 0.25 0.625 0.25
		 0.625 0 0.47918677 0 0.125 0 0.125 0.25 0.25509599 0.25 0.25509599 0 0.47918677 0.36990398
		 0.625 0.36990398 0.74490404 0.25 0.74490404 0 0.47918677 1 0.625 1 0.625 0.88009602
		 0.47918677 0.88009602 0.375 0.25 0.375 0 0.375 0.88009602 0.375 1 0.375 0.75 0.47918677
		 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0 0 0.58325303 0 0.58325303 1
		 0 3.877163e-009 7.4505806e-009 1 0.52038401 1 0.52038401 0 0.47918677 0.25 0.47918677
		 0 0.625 0 0.625 0.25 0.125 0 0.25509599 0 0.25509599 0.25 0.125 0.25 0.47918677 0.36990398
		 0.625 0.36990398 0.74490404 0 0.74490404 0.25 0.47918677 1 0.47918677 0.88009602
		 0.625 0.88009602 0.625 1 0.375 0 0.375 0.25 0.375 0.88009602 0.375 1 0.375 0.75 0.47918677
		 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0.58325303 1 0.58325303 0 0 0
		 0 3.877163e-009 0.52038401 0 0.52038401 1 7.4505806e-009 1 0.47918677 0.25 0.47918677
		 0 0.625 0 0.625 0.25 0.125 0 0.25509599 0 0.25509599 0.25 0.125 0.25 0.47918677 0.36990398
		 0.625 0.36990398 0.74490404 0 0.74490404 0.25 0.47918677 1 0.47918677 0.88009602
		 0.625 0.88009602 0.625 1 0.375 0 0.375 0.25 0.375 0.88009602 0.375 1 0.375 0.75 0.47918677
		 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0.58325303 1 0.58325303 0 0 0
		 0 3.877163e-009 0.52038401 0 0.52038401 1 7.4505806e-009 1 0 0 1 0 1 1 0 1 0.47918677
		 0.25 0.625 0.25 0.625 0 0.47918677 0 0.125 0 0.125 0.25 0.25509599 0.25 0.25509599
		 0 0.47918677 0.36990398 0.625 0.36990398 0.74490404 0.25 0.74490404 0 0.47918677
		 1 0.625 1 0.625 0.88009602 0.47918677 0.88009602 0.375 0.25 0.375 0 0.375 0.88009602
		 0.375 1 0.375 0.75 0.47918677 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1
		 0 0 0.58325303 0 0.58325303 1 0 3.877163e-009 7.4505806e-009 1 0.52038401 1 0.52038401
		 0 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75
		 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25
		 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875
		 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5
		 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0;
	setAttr ".uvst[0].uvsp[250:499]" 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5
		 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25;
	setAttr ".uvst[0].uvsp[500:695]" 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625
		 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125
		 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75
		 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25
		 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875
		 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5
		 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 388 ".pt";
	setAttr ".pt[0:165]" -type "float3"  -12.304459 12.757596 -20.80504 -10.52472 
		12.432518 -20.815172 -15.124069 -12.412487 -25.206631 -13.333607 -12.705115 -25.5123 
		-15.134672 -12.565935 -21.762461 -12.469914 12.498539 -17.303017 -15.192253 -12.48613 
		-24.214401 -13.361811 -12.760245 -24.453457 -10.59291 12.358894 -19.822937 -12.372644 
		12.683973 -19.812805 -11.844348 12.678749 -20.867178 -11.867729 12.603697 -19.803299 
		-11.870933 12.38814 -17.463364 -14.674577 -12.644866 -21.824614 -14.732151 -12.565029 
		-24.276533 -14.663962 -12.491409 -25.268784 9.6764116 12.757598 -21.015959 7.8966699 
		12.43252 -21.026089 12.496028 -12.412504 -25.417553 10.705571 -12.705115 -25.723209 
		12.506645 -12.565935 -21.973379 9.8418655 12.498537 -17.513926 12.564212 -12.486124 
		-24.42531 10.733765 -12.760245 -24.664373 7.9648752 12.358898 -20.033846 9.744627 
		12.683966 -20.023705 9.2163048 12.67874 -21.0781 9.2397013 12.603697 -20.014221 9.242877 
		12.388144 -17.674284 12.046526 -12.644859 -22.035522 12.104123 -12.565046 -24.487442 
		12.035908 -12.491407 -25.479706 -12.304452 12.757596 25.024603 -10.524717 12.432516 
		25.034744 -15.124069 -12.412487 29.426195 -13.333608 -12.705115 29.731861 -15.134677 
		-12.565935 25.982025 -12.469916 12.498537 21.52257 -15.192253 -12.48613 28.433956 
		-13.361811 -12.760242 28.673021 -10.59291 12.358891 24.042492 -12.372644 12.683969 
		24.032364 -11.844343 12.678749 25.086742 -11.867729 12.603695 24.022869 -11.870933 
		12.38814 21.682924 -14.67458 -12.644866 26.044159 -14.732149 -12.565029 28.49609 
		-14.663962 -12.491407 29.488348 -11.810346 11.889719 23.596136 9.3540802 11.889719 
		23.596136 -11.810346 11.889719 -19.389071 9.3540802 11.889719 -19.389071 9.676405 
		12.7576 25.235514 7.8966632 12.432518 25.245657 12.496028 -12.41251 29.6371 10.705571 
		-12.705115 29.942781 12.506639 -12.565935 26.192949 9.8418703 12.498535 21.73349 
		12.564212 -12.486124 28.644878 10.733772 -12.760245 28.883926 7.96487 12.358898 24.253412 
		9.7446346 12.683966 24.243271 9.216301 12.67874 25.297663 9.2397127 12.603697 24.23378 
		9.2428837 12.388142 21.893839 12.046526 -12.644859 26.255083 12.104125 -12.565046 
		28.70701 12.035908 -12.491407 29.699268 -14.587852 -10.253962 27.525166 -14.587852 
		-10.1206 28.892986 -14.587852 -12.617061 27.989357 -14.587852 -12.483727 29.357143 
		11.725793 -12.617061 27.989357 11.725793 -12.483727 29.357143 11.725793 -10.253962 
		27.525166 11.725793 -10.1206 28.892986 -14.34613 -7.8261213 27.048277 -14.34613 -7.692771 
		28.41609 -14.34613 -10.189171 27.512463 -14.34613 -10.055818 28.88026 11.509721 -10.189171 
		27.512463 11.509721 -10.055818 28.88026 11.509721 -7.8261213 27.048277 11.509721 
		-7.692771 28.41609 -14.167424 -5.3234954 26.556717 -14.167424 -5.1901484 27.924501 
		-14.167424 -7.6866035 27.020874 -14.167424 -7.5532389 28.388674 11.322705 -7.6866035 
		27.020874 11.322705 -7.5532389 28.388674 11.322705 -5.3234954 26.556717 11.322705 
		-5.1901484 27.924501 -13.64503 -2.8740876 26.075579 -13.64503 -2.7407286 27.443365 
		-13.64503 -5.2371855 26.539738 -13.64503 -5.1038032 27.907553 11.082242 -5.2371855 
		26.539738 11.082242 -5.1038032 27.907553 11.082242 -2.8740876 26.075579 11.082242 
		-2.7407286 27.443365 -13.708383 -0.42053398 25.59363 -13.708383 -0.28717685 26.961424 
		-13.708383 -2.7836151 26.057789 -13.708383 -2.6502671 27.425606 10.766587 -2.7836151 
		26.057789 10.766587 -2.6502671 27.425606 10.766587 -0.42053398 25.59363 10.766587 
		-0.28717685 26.961424 -13.365231 2.0362978 25.111038 -13.365231 2.1696446 26.478838 
		-13.365231 -0.32678971 25.575211 -13.365231 -0.19343081 26.94301 10.583171 -0.32678971 
		25.575211 10.583171 -0.19343081 26.94301 10.583171 2.0362978 25.111038 10.583171 
		2.1696446 26.478838 -12.66468 6.9673195 24.142448 -12.66468 7.1006918 25.510252 -12.66468 
		4.6042566 24.606625 -12.66468 4.7375989 25.974411 9.9513464 4.6042566 24.606625 9.9513464 
		4.7375989 25.974411 9.9513464 6.9673195 24.142448 9.9513464 7.1006918 25.510252 -13.002965 
		4.5196486 24.623228 -13.002965 4.6530185 25.991043 -13.002965 2.1565661 25.087416 
		-13.002965 2.2899292 26.455214 10.272472 2.1565661 25.087416 10.272472 2.2899292 
		26.455214 10.272472 4.5196486 24.623228 10.272472 4.6530185 25.991043 -12.554102 
		9.4589396 23.65304 -12.554102 9.5923042 25.02083 -12.554102 7.0958443 24.117201 -12.554102 
		7.2291698 25.485018 9.7433538 7.0958443 24.117201 9.7433538 7.2291698 25.485018 9.7433538 
		9.4589396 23.65304 9.7433538 9.5923042 25.02083 -11.798039 11.935098 23.166651 -11.798039 
		12.068421 24.534468 -11.798039 9.5719738 23.630836 -11.798039 9.7053175 24.998627 
		9.3359785 9.5719738 23.630836 9.3359785 9.7053175 24.998627 9.3359785 11.935098 23.166651 
		9.3359785 12.068421 24.534468 11.638391 -10.253962 -23.449581 11.638391 -10.1206 
		-24.817383 11.638391 -12.617061 -23.913755 11.638391 -12.483727 -25.281555 -14.615817 
		-12.617061 -23.913755 -14.615817 -12.483727 -25.281555 -14.615817 -10.253962 -23.449581 
		-14.615817 -10.1206 -24.817383 11.638391 -7.8261213 -22.972681 11.638391 -7.692771 
		-24.340487 11.638391 -10.189171 -23.436859 11.638391 -10.055818 -24.804657 -14.100446 
		-10.189171 -23.436859 -14.100446 -10.055818 -24.804657 -14.100446 -7.8261213 -22.972681 
		-14.100446 -7.692771 -24.340487 11.304287 -5.3234954 -22.4811 11.304287 -5.1901484 
		-23.848917;
	setAttr ".pt[166:331]" 11.304287 -7.6866035 -22.945276 11.304287 -7.5532389 
		-24.313078 -14.100446 -7.6866035 -22.945276 -14.100446 -7.5532389 -24.313078 -14.100446 
		-5.3234954 -22.4811 -14.100446 -5.1901484 -23.848917 10.99051 -2.8740876 -21.999973 
		10.99051 -2.7407286 -23.367777 10.99051 -5.2371855 -22.464149 10.99051 -5.1038032 
		-23.831951 -13.452574 -5.2371855 -22.464149 -13.452574 -5.1038032 -23.831951 -13.452574 
		-2.8740876 -21.999973 -13.452574 -2.7407286 -23.367777 10.841375 -0.42053398 -21.518021 
		10.841375 -0.28717685 -22.885832 10.841375 -2.7836151 -21.982201 10.841375 -2.6502671 
		-23.349995 -13.303432 -2.7836151 -21.982201 -13.303432 -2.6502671 -23.349995 -13.303432 
		-0.42053398 -21.518021 -13.303432 -0.28717685 -22.885832 10.530366 2.0362978 -21.035435 
		10.530366 2.1696446 -22.40324 10.530366 -0.32678971 -21.499615 10.530366 -0.19343081 
		-22.867414 -12.99242 -0.32678971 -21.499615 -12.99242 -0.19343081 -22.867414 -12.99242 
		2.0362978 -21.035435 -12.99242 2.1696446 -22.40324 9.9135237 6.9673195 -20.066845 
		9.9135237 7.1006918 -21.434647 9.9135237 4.6042566 -20.531021 9.9135237 4.7375989 
		-21.898823 -12.375578 4.6042566 -20.531021 -12.375578 4.7375989 -21.898823 -12.375578 
		6.9673195 -20.066845 -12.375578 7.1006918 -21.434647 10.294963 4.5196486 -20.547638 
		10.294963 4.6530185 -21.915438 10.294963 2.1565661 -21.011812 10.294963 2.2899292 
		-22.37961 -12.757019 2.1565661 -21.011812 -12.757019 2.2899292 -22.37961 -12.757019 
		4.5196486 -20.547638 -12.757019 4.6530185 -21.915438 9.5382433 9.4589396 -19.577436 
		9.5382433 9.5923042 -20.945236 9.5382433 7.0958443 -20.041609 9.5382433 7.2291698 
		-21.409403 -12.000299 7.0958443 -20.041609 -12.000299 7.2291698 -21.409403 -12.000299 
		9.4589396 -19.577436 -12.000299 9.5923042 -20.945236 9.5761385 11.935098 -19.091055 
		9.5761385 12.068421 -20.458853 9.5761385 9.5719738 -19.555227 9.5761385 9.7053175 
		-20.923035 -12.038195 9.5719738 -19.555227 -12.038195 9.7053175 -20.923035 -12.038195 
		11.935098 -19.091055 -12.038195 12.068421 -20.458853 -13.887073 -10.253962 -23.533037 
		-14.594525 -10.1206 -23.533037 -14.12715 -12.617061 -23.533037 -14.834611 -12.483727 
		-23.533037 -14.12715 -12.617061 27.752604 -14.834611 -12.483727 27.752604 -13.887073 
		-10.253962 27.752604 -14.594525 -10.1206 27.752604 -13.640406 -7.8261213 -23.533037 
		-14.347868 -7.692771 -23.533037 -13.88049 -10.189171 -23.533037 -14.587953 -10.055818 
		-23.533037 -13.88049 -10.189171 27.50337 -14.587953 -10.055818 27.50337 -13.640406 
		-7.8261213 27.50337 -14.347868 -7.692771 27.50337 -13.386153 -5.3234954 -22.513409 
		-14.093614 -5.1901484 -22.513409 -13.626238 -7.6866035 -22.513409 -14.333689 -7.5532389 
		-22.513409 -13.626238 -7.6866035 26.988338 -14.333689 -7.5532389 26.988338 -13.386153 
		-5.3234954 26.988338 -14.093614 -5.1901484 26.988338 -13.137298 -2.8740876 -21.973875 
		-13.844759 -2.7407286 -21.973875 -13.377381 -5.2371855 -21.973875 -14.084839 -5.1038032 
		-21.973875 -13.377381 -5.2371855 26.392298 -14.084839 -5.1038032 26.392298 -13.137298 
		-2.8740876 26.392298 -13.844759 -2.7407286 26.392298 -12.888023 -0.42053398 -21.466221 
		-13.595486 -0.28717685 -21.466221 -13.12811 -2.7836151 -21.466221 -13.83556 -2.6502671 
		-21.466221 -13.12811 -2.7836151 25.655619 -13.83556 -2.6502671 25.655619 -12.888023 
		-0.42053398 25.655619 -13.595486 -0.28717685 25.655619 -12.63842 2.0362978 -21.15484 
		-13.345881 2.1696446 -21.07086 -12.878504 -0.32678971 -21.15484 -13.585962 -0.19343081 
		-21.07086 -12.878504 -0.32678971 25.188543 -13.585962 -0.19343081 25.188543 -12.63842 
		2.0362978 25.188543 -13.345881 2.1696446 25.188543 -12.137443 6.9673195 -20.371506 
		-12.844903 7.1006918 -20.371506 -12.377524 4.6042566 -20.371506 -13.084984 4.7375989 
		-20.371506 -12.377524 4.6042566 24.41852 -13.084984 4.7375989 24.41852 -12.137443 
		6.9673195 24.41852 -12.844903 7.1006918 24.41852 -12.386124 4.5196486 -21.027664 
		-13.093575 4.6530185 -21.027664 -12.626197 2.1565661 -21.027664 -13.33366 2.2899292 
		-21.027664 -12.626197 2.1565661 25.028412 -13.33366 2.2899292 25.028412 -12.386124 
		4.5196486 25.028412 -13.093575 4.6530185 25.028412 -11.884309 9.4589396 -19.778242 
		-12.591768 9.5923042 -19.778242 -12.12439 7.0958443 -19.778242 -12.831844 7.2291698 
		-19.778242 -12.12439 7.0958443 23.797302 -12.831844 7.2291698 23.797302 -11.884309 
		9.4589396 23.797302 -12.591768 9.5923042 23.797302 -11.632744 11.935098 -19.526827 
		-12.340199 12.068421 -19.526827 -11.872824 9.5719738 -19.526827 -12.580286 9.7053175 
		-19.526827 -11.872824 9.5719738 23.364283 -12.580286 9.7053175 23.364283 -11.632744 
		11.935098 23.364283 -12.340199 12.068421 23.364283 11.162034 -10.253962 28.650057 
		11.915684 -10.1206 28.650057 11.41779 -12.617061 28.650057 12.171442 -12.483727 28.650057 
		11.41779 -12.617061 -24.753103 12.171442 -12.483727 -24.753103 11.162034 -10.253962 
		-24.753103 11.915684 -10.1206 -24.753103 10.899267 -7.8261213 28.370041 11.652918 
		-7.692771 28.370041 11.155024 -10.189171 28.370041 11.908678 -10.055818 28.370041 
		11.155024 -10.189171 -24.051823 11.908678 -10.055818 -24.051823 10.899267 -7.8261213 
		-24.051823 11.652918 -7.692771 -24.051823 10.628409 -5.3234954 27.642363 11.38206 
		-5.1901484 27.642363 10.884165 -7.6866035 27.642363 11.637818 -7.5532389 27.642363 
		10.884165 -7.6866035 -23.645325 11.637818 -7.5532389 -23.645325 10.628409 -5.3234954 
		-23.645325 11.38206 -5.1901484 -23.645325;
	setAttr ".pt[332:387]" 10.363307 -2.8740876 27.262615 11.116961 -2.7407286 
		27.262615 10.619064 -5.2371855 27.262615 11.372718 -5.1038032 27.262615 10.619064 
		-5.2371855 -23.191862 11.372718 -5.1038032 -23.191862 10.363307 -2.8740876 -23.191862 
		11.116961 -2.7407286 -23.191862 10.097756 -0.42053398 26.687086 10.85141 -0.28717685 
		26.687086 10.353513 -2.7836151 26.687086 11.107167 -2.6502671 26.687086 10.353513 
		-2.7836151 -22.496841 11.107167 -2.6502671 -22.496841 10.097756 -0.42053398 -22.496841 
		10.85141 -0.28717685 -22.496841 9.8318548 2.0362978 26.257641 10.585506 2.1696446 
		26.257641 10.087612 -0.32678971 26.257641 10.841264 -0.19343081 26.257641 10.087612 
		-0.32678971 -22.025827 10.841264 -0.19343081 -22.025827 9.8318548 2.0362978 -22.025827 
		10.585506 2.1696446 -22.025827 9.2981663 6.9673195 25.373087 10.051819 7.1006918 
		25.373087 9.5539236 4.6042566 25.373087 10.307575 4.7375989 25.373087 9.5539236 4.6042566 
		-21.42363 10.307575 4.7375989 -21.42363 9.2981663 6.9673195 -21.42363 10.051819 7.1006918 
		-21.42363 9.5630798 4.5196486 25.860308 10.316731 4.6530185 25.860308 9.8188362 2.1565661 
		25.860308 10.57249 2.2899292 25.860308 9.8188362 2.1565661 -21.77994 10.57249 2.2899292 
		-21.77994 9.5630798 4.5196486 -21.77994 10.316731 4.6530185 -21.77994 9.0285015 9.4589396 
		24.836241 9.7821531 9.5923042 24.836241 9.2842598 7.0958443 24.836241 10.03791 7.2291698 
		24.836241 9.2842598 7.0958443 -20.457058 10.03791 7.2291698 -20.457058 9.0285015 
		9.4589396 -20.457058 9.7821531 9.5923042 -20.457058 8.7605104 11.935098 24.077755 
		9.514163 12.068421 24.077755 9.0162687 9.5719738 24.077755 9.7699194 9.7053175 24.077755 
		9.0162687 9.5719738 -20.042849 9.7699194 9.7053175 -20.042849 8.7605104 11.935098 
		-20.042849 9.514163 12.068421 -20.042849;
	setAttr -s 388 ".vt";
	setAttr ".vt[0:165]"  215.53907776 24.70993042 -65.85915375 224.82249451 26.036052704 -65.88826752
		 200.83148193 127.390625 -78.50971222 210.17085266 128.58441162 -79.38825226 200.77615356 128.016601563 -68.61087799
		 214.67602539 25.76683044 -55.79403687 200.47579956 127.69102478 -75.6579361 210.023727417 128.80924988 -76.34499359
		 224.46681213 26.33645248 -63.036506653 215.18338013 25.0103302 -63.0073776245 217.93908691 25.031673431 -66.037757874
		 217.81710815 25.33769989 -62.98006439 217.80041504 26.21710205 -56.25489807 203.17611694 128.33834839 -68.78948212
		 202.8757782 128.01272583 -75.83652496 203.23147583 127.71239471 -78.6883316 330.19519043 24.70992279 -66.46533966
		 320.91174316 26.036048889 -66.49445343 344.90280151 127.39059448 -79.11589813 335.56347656 128.58441162 -79.99440765
		 344.95819092 128.016601563 -69.21704865 331.058227539 25.76683426 -56.40023041 345.25848389 127.69100952 -76.26412201
		 335.71054077 128.80924988 -76.9511795 321.26751709 26.33644867 -63.64267731 330.55102539 25.01033783 -63.61354828
		 327.79519653 25.031684875 -66.64394379 327.91723633 25.33769989 -63.58626556 327.93380737 26.21709442 -56.86108398
		 342.55813599 128.33833313 -69.39566803 342.85855103 128.012756348 -76.44271088 342.50274658 127.71238708 -79.29451752
		 215.53909302 24.70992661 65.85913849 224.82250977 26.036056519 65.88826752 200.83148193 127.390625 78.50971222
		 210.1708374 128.58441162 79.38824463 200.77613831 128.016601563 68.61087036 214.67601013 25.76683426 55.79402161
		 200.47579956 127.69102478 75.6579361 210.023727417 128.80923462 76.34498596 224.46681213 26.3364563 63.036491394
		 215.18338013 25.010334015 63.0073661804 217.93910217 25.031673431 66.037757874 217.81710815 25.33770752 62.98006821
		 217.80041504 26.21710205 56.25489807 203.17610168 128.33834839 68.78947449 202.87579346 128.01272583 75.83653259
		 203.23147583 127.71238708 78.68832397 218.11645508 28.2504158 61.75363922 328.51385498 28.2504158 61.75363922
		 218.11645508 28.2504158 -61.78953552 328.51385498 28.2504158 -61.78953552 330.19515991 24.70991898 66.46533966
		 320.91171265 26.036052704 66.49446869 344.90280151 127.39060974 79.1158905 335.56347656 128.58441162 79.99443054
		 344.9581604 128.016601563 69.21707153 331.058258057 25.76683807 56.40022278 345.25848389 127.69100952 76.26411438
		 335.71057129 128.80924988 76.95117188 321.26748657 26.33644867 63.64268494 330.55105591 25.01033783 63.61355972
		 327.79516602 25.031684875 66.64394379 327.91729736 25.3377037 63.58626175 327.93383789 26.21709824 56.86108398
		 342.55813599 128.33833313 69.39567566 342.85858154 128.012756348 76.44271851 342.50274658 127.71238708 79.29450989
		 203.62846375 118.58490753 73.046020508 203.62846375 118.040878296 76.97720337 203.62846375 128.22503662 74.38009644
		 203.62846375 127.68100739 78.3112793 340.88513184 128.22503662 74.38009644 340.88513184 127.68100739 78.3112793
		 340.88513184 118.58490753 73.046020508 340.88513184 118.040878296 76.97720337 204.889328 108.6805191 71.67538452
		 204.889328 108.13648987 75.60656738 204.889328 118.32064819 73.0094604492 204.889328 117.77661896 76.94064331
		 339.75805664 118.32064819 73.0094604492 339.75805664 117.77661896 76.94064331 339.75805664 108.6805191 71.67538452
		 339.75805664 108.13648987 75.60656738 205.82150269 98.47116852 70.26254272 205.82150269 97.92713928 74.19372559
		 205.82150269 108.11129761 71.59661865 205.82150269 107.56726837 75.52780151 338.78256226 108.11129761 71.59661865
		 338.78256226 107.56726837 75.52780151 338.78256226 98.47116852 70.26254272 338.78256226 97.92713928 74.19372559
		 208.54640198 88.47885132 68.87973022 208.54640198 87.93482208 72.81091309 208.54640198 98.11898041 70.21380615
		 208.54640198 97.57495117 74.14498901 337.52825928 98.11898041 70.21380615 337.52825928 97.57495117 74.14498901
		 337.52825928 88.47885132 68.87973022 337.52825928 87.93482208 72.81091309 208.21594238 78.46966553 67.49456787
		 208.21594238 77.92563629 71.42575073 208.21594238 88.10979462 68.8286438 208.21594238 87.56576538 72.75982666
		 335.88174438 88.10979462 68.8286438 335.88174438 87.56576538 72.75982666 335.88174438 78.46966553 67.49456787
		 335.88174438 77.92563629 71.42575073 210.0058898926 68.44710541 66.10757446 210.0058898926 67.90307617 70.038757324
		 210.0058898926 78.087234497 67.44165039 210.0058898926 77.54320526 71.37283325 334.92501831 78.087234497 67.44165039
		 334.92501831 77.54320526 71.37283325 334.92501831 68.44710541 66.10757446 334.92501831 67.90307617 70.038757324
		 213.66009521 48.33108902 63.32376099 213.66009521 47.7870636 67.25494385 213.66009521 57.97122192 64.65783691
		 213.66009521 57.4271965 68.58901978 331.62930298 57.97122192 64.65783691 331.62930298 57.4271965 68.58901978
		 331.62930298 48.33108902 63.32376099 331.62930298 47.7870636 67.25494385 211.89553833 58.31632233 64.70559692
		 211.89553833 57.77229691 68.63677979 211.89553833 67.95645905 66.039672852 211.89553833 67.41242981 69.97085571
		 333.30435181 67.95645905 66.039672852 333.30435181 67.41242981 69.97085571 333.30435181 58.31632233 64.70559692
		 333.30435181 57.77229691 68.63677979 214.23687744 38.16670227 61.91714478 214.23687744 37.62267685 65.84832764
		 214.23687744 47.80683517 63.2512207 214.23687744 47.26280975 67.18240356 330.54437256 47.80683517 63.2512207
		 330.54437256 47.26280975 67.18240356 330.54437256 38.16670227 61.91714478 330.54437256 37.62267685 65.84832764
		 218.1806488 28.065368652 60.51925659 218.1806488 27.52134323 64.45043945 218.1806488 37.70550156 61.85333252
		 218.1806488 37.16147614 65.78451538 328.41943359 37.70550156 61.85333252 328.41943359 37.16147614 65.78451538
		 328.41943359 28.065368652 60.51925659 328.41943359 27.52134323 64.45043945 340.42922974 118.58490753 -73.45977783
		 340.42922974 118.040878296 -77.39096069 340.42922974 128.22503662 -74.79385376 340.42922974 127.68100739 -78.72503662
		 203.48262024 128.22503662 -74.79385376 203.48262024 127.68100739 -78.72503662 203.48262024 118.58490753 -73.45977783
		 203.48262024 118.040878296 -77.39096069 340.42922974 108.6805191 -72.089141846 340.42922974 108.13648987 -76.020324707
		 340.42922974 118.32064819 -73.42321777 340.42922974 117.77661896 -77.35440063 206.17086792 118.32064819 -73.42321777
		 206.17086792 117.77661896 -77.35440063 206.17086792 108.6805191 -72.089141846 206.17086792 108.13648987 -76.020324707
		 338.68649292 98.47116852 -70.67630005 338.68649292 97.92713928 -74.60748291;
	setAttr ".vt[166:331]" 338.68649292 108.11129761 -72.010375977 338.68649292 107.56726837 -75.94155884
		 206.17086792 108.11129761 -72.010375977 206.17086792 107.56726837 -75.94155884 206.17086792 98.47116852 -70.67630005
		 206.17086792 97.92713928 -74.60748291 337.04977417 88.47885132 -69.29348755 337.04977417 87.93482208 -73.22467041
		 337.04977417 98.11898041 -70.62756348 337.04977417 97.57495117 -74.55874634 209.55030823 98.11898041 -70.62756348
		 209.55030823 97.57495117 -74.55874634 209.55030823 88.47885132 -69.29348755 209.55030823 87.93482208 -73.22467041
		 336.27185059 78.46966553 -67.9083252 336.27185059 77.92563629 -71.83950806 336.27185059 88.10979462 -69.24240112
		 336.27185059 87.56576538 -73.17358398 210.32824707 88.10979462 -69.24240112 210.32824707 87.56576538 -73.17358398
		 210.32824707 78.46966553 -67.9083252 210.32824707 77.92563629 -71.83950806 334.64956665 68.44710541 -66.52133179
		 334.64956665 67.90307617 -70.45251465 334.64956665 78.087234497 -67.85540771 334.64956665 77.54320526 -71.78659058
		 211.95053101 78.087234497 -67.85540771 211.95053101 77.54320526 -71.78659058 211.95053101 68.44710541 -66.52133179
		 211.95053101 67.90307617 -70.45251465 331.43200684 48.33108902 -63.73751831 331.43200684 47.7870636 -67.66870117
		 331.43200684 57.97122192 -65.071594238 331.43200684 57.4271965 -69.0027770996 215.16809082 57.97122192 -65.071594238
		 215.16809082 57.4271965 -69.0027770996 215.16809082 48.33108902 -63.73751831 215.16809082 47.7870636 -67.66870117
		 333.42166138 58.31632233 -65.11935425 333.42166138 57.77229691 -69.050537109 333.42166138 67.95645905 -66.45343018
		 333.42166138 67.41242981 -70.38461304 213.17843628 67.95645905 -66.45343018 213.17843628 67.41242981 -70.38461304
		 213.17843628 58.31632233 -65.11935425 213.17843628 57.77229691 -69.050537109 329.4744873 38.16670227 -62.3309021
		 329.4744873 37.62267685 -66.26208496 329.4744873 47.80683517 -63.66497803 329.4744873 47.26280975 -67.59616089
		 217.12561035 47.80683517 -63.66497803 217.12561035 47.26280975 -67.59616089 217.12561035 38.16670227 -62.3309021
		 217.12561035 37.62267685 -66.26208496 329.67214966 28.065368652 -60.93301392 329.67214966 27.52134323 -64.86419678
		 329.67214966 37.70550156 -62.26708984 329.67214966 37.16147614 -66.19827271 216.927948 37.70550156 -62.26708984
		 216.927948 37.16147614 -66.19827271 216.927948 28.065368652 -60.93301392 216.927948 27.52134323 -64.86419678
		 207.28387451 118.58490753 -73.699646 203.59365845 118.040878296 -73.699646 206.031585693 128.22503662 -73.699646
		 202.34133911 127.68100739 -73.699646 206.031585693 128.22503662 73.699646 202.34133911 127.68100739 73.699646
		 207.28387451 118.58490753 73.699646 203.59365845 118.040878296 73.699646 208.57052612 108.6805191 -73.699646
		 204.88027954 108.13648987 -73.699646 207.31820679 118.32064819 -73.699646 203.62796021 117.77661896 -73.699646
		 207.31820679 118.32064819 72.98336029 203.62796021 117.77661896 72.98336029 208.57052612 108.6805191 72.98336029
		 204.88027954 108.13648987 72.98336029 209.89675903 98.47116852 -70.76914215 206.20651245 97.92713928 -70.76914215
		 208.6444397 108.11129761 -70.76914215 204.95422363 107.56726837 -70.76914215 208.6444397 108.11129761 71.50312805
		 204.95422363 107.56726837 71.50312805 209.89675903 98.47116852 71.50312805 206.20651245 97.92713928 71.50312805
		 211.19482422 88.47885132 -69.21847534 207.50457764 87.93482208 -69.21847534 209.94250488 98.11898041 -69.21847534
		 206.25228882 97.57495117 -69.21847534 209.94250488 98.11898041 69.79000092 206.25228882 97.57495117 69.79000092
		 211.19482422 88.47885132 69.79000092 207.50457764 87.93482208 69.79000092 212.49508667 78.46966553 -67.75943756
		 208.80484009 77.92563629 -67.75943756 211.24276733 88.10979462 -67.75943756 207.55255127 87.56576538 -67.75943756
		 211.24276733 88.10979462 67.67272186 207.55255127 87.56576538 67.67272186 212.49508667 78.46966553 67.67272186
		 208.80484009 77.92563629 67.67272186 213.79705811 68.44710541 -66.86452484 210.10681152 67.90307617 -66.6231308
		 212.54473877 78.087234497 -66.86452484 208.85452271 77.54320526 -66.6231308 212.54473877 78.087234497 66.33035278
		 208.85452271 77.54320526 66.33035278 213.79705811 68.44710541 66.33035278 210.10681152 67.90307617 66.33035278
		 216.4102478 48.33108902 -64.61313629 212.72000122 47.7870636 -64.61313629 215.15792847 57.97122192 -64.61313629
		 211.4677124 57.4271965 -64.61313629 215.15792847 57.97122192 64.11721802 211.4677124 57.4271965 64.11721802
		 216.4102478 48.33108902 64.11721802 212.72000122 47.7870636 64.11721802 215.11309814 58.31632233 -66.49899292
		 211.42288208 57.77229691 -66.49899292 213.86080933 67.95645905 -66.49899292 210.17056274 67.41242981 -66.49899292
		 213.86080933 67.95645905 65.87011719 210.17056274 67.41242981 65.87011719 215.11309814 58.31632233 65.87011719
		 211.42288208 57.77229691 65.87011719 217.73065186 38.16670227 -62.90806198 214.040405273 37.62267685 -62.90806198
		 216.47833252 47.80683517 -62.90806198 212.78811646 47.26280975 -62.90806198 216.47833252 47.80683517 62.33179474
		 212.78811646 47.26280975 62.33179474 217.73065186 38.16670227 62.33179474 214.040405273 37.62267685 62.33179474
		 219.04284668 28.065368652 -62.18546295 215.35263062 27.52134323 -62.18546295 217.79055786 37.70550156 -62.18546295
		 214.10031128 37.16147614 -62.18546295 217.79055786 37.70550156 61.087280273 214.10031128 37.16147614 61.087280273
		 219.04284668 28.065368652 61.087280273 215.35263062 27.52134323 61.087280273 337.94445801 118.58490753 76.27902985
		 341.87564087 118.040878296 76.27902985 339.27853394 128.22503662 76.27902985 343.2097168 127.68100739 76.27902985
		 339.27853394 128.22503662 -77.20620728 343.2097168 127.68100739 -77.20620728 337.94445801 118.58490753 -77.20620728
		 341.87564087 118.040878296 -77.20620728 336.57382202 108.6805191 75.47422791 340.50500488 108.13648987 75.47422791
		 337.90789795 118.32064819 75.47422791 341.83908081 117.77661896 75.47422791 337.90789795 118.32064819 -75.19068909
		 341.83908081 117.77661896 -75.19068909 336.57382202 108.6805191 -75.19068909 340.50500488 108.13648987 -75.19068909
		 335.16098022 98.47116852 73.38280487 339.092163086 97.92713928 73.38280487 336.49505615 108.11129761 73.38280487
		 340.42623901 107.56726837 73.38280487 336.49505615 108.11129761 -74.022361755 340.42623901 107.56726837 -74.022361755
		 335.16098022 98.47116852 -74.022361755 339.092163086 97.92713928 -74.022361755;
	setAttr ".vt[332:387]" 333.77816772 88.47885132 72.29137421 337.70935059 87.93482208 72.29137421
		 335.11224365 98.11898041 72.29137421 339.043426514 97.57495117 72.29137421 335.11224365 98.11898041 -72.71907806
		 339.043426514 97.57495117 -72.71907806 333.77816772 88.47885132 -72.71907806 337.70935059 87.93482208 -72.71907806
		 332.39300537 78.46966553 70.63729095 336.32418823 77.92563629 70.63729095 333.7270813 88.10979462 70.63729095
		 337.65826416 87.56576538 70.63729095 333.7270813 88.10979462 -70.7215271 337.65826416 87.56576538 -70.7215271
		 332.39300537 78.46966553 -70.7215271 336.32418823 77.92563629 -70.7215271 331.0060119629 68.44710541 69.40299988
		 334.93719482 67.90307617 69.40299988 332.34008789 78.087234497 69.40299988 336.27127075 77.54320526 69.40299988
		 332.34008789 78.087234497 -69.36778259 336.27127075 77.54320526 -69.36778259 331.0060119629 68.44710541 -69.36778259
		 334.93719482 67.90307617 -69.36778259 328.22219849 48.33108902 66.86075592 332.15338135 47.7870636 66.86075592
		 329.55627441 57.97122192 66.86075592 333.48745728 57.4271965 66.86075592 329.55627441 57.97122192 -67.63702393
		 333.48745728 57.4271965 -67.63702393 328.22219849 48.33108902 -67.63702393 332.15338135 47.7870636 -67.63702393
		 329.60403442 58.31632233 68.26103973 333.53521729 57.77229691 68.26103973 330.93811035 67.95645905 68.26103973
		 334.86929321 67.41242981 68.26103973 330.93811035 67.95645905 -68.66109467 334.86929321 67.41242981 -68.66109467
		 329.60403442 58.31632233 -68.66109467 333.53521729 57.77229691 -68.66109467 326.81558228 38.16670227 65.31777191
		 330.74676514 37.62267685 65.31777191 328.1496582 47.80683517 65.31777191 332.080841064 47.26280975 65.31777191
		 328.1496582 47.80683517 -64.85901642 332.080841064 47.26280975 -64.85901642 326.81558228 38.16670227 -64.85901642
		 330.74676514 37.62267685 -64.85901642 325.41769409 28.065368652 63.13781357 329.34887695 27.52134323 63.13781357
		 326.75177002 37.70550156 63.13781357 330.68295288 37.16147614 63.13781357 326.75177002 37.70550156 -63.66854858
		 330.68295288 37.16147614 -63.66854858 325.41769409 28.065368652 -63.66854858 329.34887695 27.52134323 -63.66854858;
	setAttr -s 596 ".ed";
	setAttr ".ed[0:165]"  15 10 1 10 1 0 1 3 0 15 3 0 5 9 0 9 6 1 6 4 0 4 5 0
		 14 15 1 3 7 0 14 7 0 8 1 0 7 8 0 10 11 1 8 11 0 9 0 0 0 2 0 2 6 0 11 9 1 0 10 0 5 12 0
		 11 12 0 4 13 0 12 13 0 6 14 1 13 14 0 2 15 0 11 14 0 31 19 0 17 19 0 26 17 0 31 26 1
		 20 21 0 22 20 0 25 22 1 21 25 0 30 23 0 19 23 0 30 31 1 23 24 0 24 17 0 24 27 0 26 27 1
		 18 22 0 16 18 0 25 16 0 16 26 0 27 25 1 27 28 0 21 28 0 28 29 0 20 29 0 29 30 0 22 30 1
		 18 31 0 27 30 0 32 42 0 34 47 0 36 45 0 37 44 0 32 34 0 33 35 0 34 38 0 35 39 0 36 37 0
		 37 41 0 38 36 0 40 33 0 41 32 0 38 46 1 39 40 0 40 43 0 41 38 1 42 33 0 43 41 1 46 39 0
		 47 35 0 42 43 1 43 44 0 44 45 0 45 46 0 46 47 1 47 42 1 43 46 0 48 49 0 48 50 0 49 51 0
		 50 51 0 67 62 1 62 53 0 53 55 0 67 55 0 57 61 0 61 58 1 58 56 0 56 57 0 66 67 1 55 59 0
		 66 59 0 60 53 0 59 60 0 62 63 1 60 63 0 61 52 0 52 54 0 54 58 0 63 61 1 52 62 0 57 64 0
		 63 64 0 56 65 0 64 65 0 58 66 1 65 66 0 54 67 0 63 66 0 68 69 0 70 71 0 72 73 0 74 75 0
		 68 70 0 69 71 0 70 72 0 71 73 0 72 74 0 73 75 0 74 68 0 75 69 0 76 77 0 78 79 0 80 81 0
		 82 83 0 76 78 0 77 79 0 78 80 0 79 81 0 80 82 0 81 83 0 82 76 0 83 77 0 84 85 0 86 87 0
		 88 89 0 90 91 0 84 86 0 85 87 0 86 88 0 87 89 0 88 90 0 89 91 0 90 84 0 91 85 0 92 93 0
		 94 95 0 96 97 0 98 99 0 92 94 0 93 95 0 94 96 0 95 97 0 96 98 0 97 99 0 98 92 0 99 93 0
		 100 101 0 102 103 0;
	setAttr ".ed[166:331]" 104 105 0 106 107 0 100 102 0 101 103 0 102 104 0 103 105 0
		 104 106 0 105 107 0 106 100 0 107 101 0 108 109 0 110 111 0 112 113 0 114 115 0 108 110 0
		 109 111 0 110 112 0 111 113 0 112 114 0 113 115 0 114 108 0 115 109 0 116 117 0 118 119 0
		 120 121 0 122 123 0 116 118 0 117 119 0 118 120 0 119 121 0 120 122 0 121 123 0 122 116 0
		 123 117 0 124 125 0 126 127 0 128 129 0 130 131 0 124 126 0 125 127 0 126 128 0 127 129 0
		 128 130 0 129 131 0 130 124 0 131 125 0 132 133 0 134 135 0 136 137 0 138 139 0 132 134 0
		 133 135 0 134 136 0 135 137 0 136 138 0 137 139 0 138 132 0 139 133 0 140 141 0 142 143 0
		 144 145 0 146 147 0 140 142 0 141 143 0 142 144 0 143 145 0 144 146 0 145 147 0 146 140 0
		 147 141 0 148 149 0 150 151 0 152 153 0 154 155 0 148 150 0 149 151 0 150 152 0 151 153 0
		 152 154 0 153 155 0 154 148 0 155 149 0 156 157 0 158 159 0 160 161 0 162 163 0 156 158 0
		 157 159 0 158 160 0 159 161 0 160 162 0 161 163 0 162 156 0 163 157 0 164 165 0 166 167 0
		 168 169 0 170 171 0 164 166 0 165 167 0 166 168 0 167 169 0 168 170 0 169 171 0 170 164 0
		 171 165 0 172 173 0 174 175 0 176 177 0 178 179 0 172 174 0 173 175 0 174 176 0 175 177 0
		 176 178 0 177 179 0 178 172 0 179 173 0 180 181 0 182 183 0 184 185 0 186 187 0 180 182 0
		 181 183 0 182 184 0 183 185 0 184 186 0 185 187 0 186 180 0 187 181 0 188 189 0 190 191 0
		 192 193 0 194 195 0 188 190 0 189 191 0 190 192 0 191 193 0 192 194 0 193 195 0 194 188 0
		 195 189 0 196 197 0 198 199 0 200 201 0 202 203 0 196 198 0 197 199 0 198 200 0 199 201 0
		 200 202 0 201 203 0 202 196 0 203 197 0 204 205 0 206 207 0 208 209 0 210 211 0 204 206 0
		 205 207 0 206 208 0 207 209 0 208 210 0 209 211 0 210 204 0 211 205 0;
	setAttr ".ed[332:497]" 212 213 0 214 215 0 216 217 0 218 219 0 212 214 0 213 215 0
		 214 216 0 215 217 0 216 218 0 217 219 0 218 212 0 219 213 0 220 221 0 222 223 0 224 225 0
		 226 227 0 220 222 0 221 223 0 222 224 0 223 225 0 224 226 0 225 227 0 226 220 0 227 221 0
		 228 229 0 230 231 0 232 233 0 234 235 0 228 230 0 229 231 0 230 232 0 231 233 0 232 234 0
		 233 235 0 234 228 0 235 229 0 236 237 0 238 239 0 240 241 0 242 243 0 236 238 0 237 239 0
		 238 240 0 239 241 0 240 242 0 241 243 0 242 236 0 243 237 0 244 245 0 246 247 0 248 249 0
		 250 251 0 244 246 0 245 247 0 246 248 0 247 249 0 248 250 0 249 251 0 250 244 0 251 245 0
		 252 253 0 254 255 0 256 257 0 258 259 0 252 254 0 253 255 0 254 256 0 255 257 0 256 258 0
		 257 259 0 258 252 0 259 253 0 260 261 0 262 263 0 264 265 0 266 267 0 260 262 0 261 263 0
		 262 264 0 263 265 0 264 266 0 265 267 0 266 260 0 267 261 0 268 269 0 270 271 0 272 273 0
		 274 275 0 268 270 0 269 271 0 270 272 0 271 273 0 272 274 0 273 275 0 274 268 0 275 269 0
		 276 277 0 278 279 0 280 281 0 282 283 0 276 278 0 277 279 0 278 280 0 279 281 0 280 282 0
		 281 283 0 282 276 0 283 277 0 284 285 0 286 287 0 288 289 0 290 291 0 284 286 0 285 287 0
		 286 288 0 287 289 0 288 290 0 289 291 0 290 284 0 291 285 0 292 293 0 294 295 0 296 297 0
		 298 299 0 292 294 0 293 295 0 294 296 0 295 297 0 296 298 0 297 299 0 298 292 0 299 293 0
		 300 301 0 302 303 0 304 305 0 306 307 0 300 302 0 301 303 0 302 304 0 303 305 0 304 306 0
		 305 307 0 306 300 0 307 301 0 308 309 0 310 311 0 312 313 0 314 315 0 308 310 0 309 311 0
		 310 312 0 311 313 0 312 314 0 313 315 0 314 308 0 315 309 0 316 317 0 318 319 0 320 321 0
		 322 323 0 316 318 0 317 319 0 318 320 0 319 321 0 320 322 0 321 323 0;
	setAttr ".ed[498:595]" 322 316 0 323 317 0 324 325 0 326 327 0 328 329 0 330 331 0
		 324 326 0 325 327 0 326 328 0 327 329 0 328 330 0 329 331 0 330 324 0 331 325 0 332 333 0
		 334 335 0 336 337 0 338 339 0 332 334 0 333 335 0 334 336 0 335 337 0 336 338 0 337 339 0
		 338 332 0 339 333 0 340 341 0 342 343 0 344 345 0 346 347 0 340 342 0 341 343 0 342 344 0
		 343 345 0 344 346 0 345 347 0 346 340 0 347 341 0 348 349 0 350 351 0 352 353 0 354 355 0
		 348 350 0 349 351 0 350 352 0 351 353 0 352 354 0 353 355 0 354 348 0 355 349 0 356 357 0
		 358 359 0 360 361 0 362 363 0 356 358 0 357 359 0 358 360 0 359 361 0 360 362 0 361 363 0
		 362 356 0 363 357 0 364 365 0 366 367 0 368 369 0 370 371 0 364 366 0 365 367 0 366 368 0
		 367 369 0 368 370 0 369 371 0 370 364 0 371 365 0 372 373 0 374 375 0 376 377 0 378 379 0
		 372 374 0 373 375 0 374 376 0 375 377 0 376 378 0 377 379 0 378 372 0 379 373 0 380 381 0
		 382 383 0 384 385 0 386 387 0 380 382 0 381 383 0 382 384 0 383 385 0 384 386 0 385 387 0
		 386 380 0 387 381 0;
	setAttr -s 297 -ch 1188 ".fc[0:296]" -type "polyFaces" 
		f 4 3 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 -8 -7 -6 -5
		mu 0 4 4 5 6 7
		f 4 10 -10 -4 -9
		mu 0 4 8 9 1 0
		f 4 2 9 12 11
		mu 0 4 2 1 10 11
		f 4 1 -12 14 -14
		mu 0 4 12 13 14 15
		f 4 -18 -17 -16 5
		mu 0 4 6 16 17 7
		f 4 15 19 13 18
		mu 0 4 18 19 12 15
		f 4 4 -19 21 -21
		mu 0 4 20 18 15 21
		f 4 7 20 23 -23
		mu 0 4 22 20 21 23
		f 4 6 22 25 -25
		mu 0 4 24 22 23 8
		f 4 17 24 8 -27
		mu 0 4 16 24 8 0
		f 4 16 26 0 -20
		mu 0 4 17 16 0 3
		f 4 -13 -11 -28 -15
		mu 0 4 25 26 27 28
		f 4 27 -26 -24 -22
		mu 0 4 29 30 31 32
		f 4 31 30 29 -29
		mu 0 4 33 34 35 36
		f 4 35 34 33 32
		mu 0 4 37 38 39 40
		f 4 38 28 37 -37
		mu 0 4 41 33 36 42
		f 4 -41 -40 -38 -30
		mu 0 4 35 43 44 36
		f 4 42 -42 40 -31
		mu 0 4 45 46 47 48
		f 4 -35 45 44 43
		mu 0 4 39 38 49 50
		f 4 -48 -43 -47 -46
		mu 0 4 51 46 45 52
		f 4 49 -49 47 -36
		mu 0 4 53 54 46 51
		f 4 51 -51 -50 -33
		mu 0 4 55 56 54 53
		f 4 53 -53 -52 -34
		mu 0 4 57 41 56 55
		f 4 54 -39 -54 -44
		mu 0 4 50 33 41 57
		f 4 46 -32 -55 -45
		mu 0 4 49 34 33 50
		f 4 41 55 36 39
		mu 0 4 58 59 60 61
		f 4 48 50 52 -56
		mu 0 4 62 63 64 65
		f 4 82 73 61 -77
		mu 0 4 66 67 68 69
		f 4 65 72 66 64
		mu 0 4 70 71 72 73
		f 4 81 76 63 -76
		mu 0 4 74 66 69 75
		f 4 -68 -71 -64 -62
		mu 0 4 68 76 77 69
		f 4 77 -72 67 -74
		mu 0 4 78 79 80 81
		f 4 -73 68 60 62
		mu 0 4 72 71 82 83
		f 4 -75 -78 -57 -69
		mu 0 4 84 79 78 85
		f 4 59 -79 74 -66
		mu 0 4 86 87 79 84
		f 4 58 -80 -60 -65
		mu 0 4 88 89 87 86
		f 4 69 -81 -59 -67
		mu 0 4 90 74 89 88
		f 4 57 -82 -70 -63
		mu 0 4 83 66 74 90
		f 4 56 -83 -58 -61
		mu 0 4 82 67 66 83
		f 4 71 83 75 70
		mu 0 4 91 92 93 94
		f 4 78 79 80 -84
		mu 0 4 95 96 97 98
		f 4 84 86 -88 -86
		mu 0 4 99 100 101 102
		f 4 91 -91 -90 -89
		mu 0 4 103 104 105 106
		f 4 -96 -95 -94 -93
		mu 0 4 107 108 109 110
		f 4 98 -98 -92 -97
		mu 0 4 111 112 104 103
		f 4 90 97 100 99
		mu 0 4 105 104 113 114
		f 4 89 -100 102 -102
		mu 0 4 115 116 117 118
		f 4 -106 -105 -104 93
		mu 0 4 109 119 120 110
		f 4 103 107 101 106
		mu 0 4 121 122 115 118
		f 4 92 -107 109 -109
		mu 0 4 123 121 118 124
		f 4 95 108 111 -111
		mu 0 4 125 123 124 126
		f 4 94 110 113 -113
		mu 0 4 127 125 126 111
		f 4 105 112 96 -115
		mu 0 4 119 127 111 103
		f 4 104 114 88 -108
		mu 0 4 120 119 103 106
		f 4 -101 -99 -116 -103
		mu 0 4 128 129 130 131
		f 4 115 -114 -112 -110
		mu 0 4 132 133 134 135
		f 4 116 121 -118 -121
		mu 0 4 136 137 138 139
		f 4 117 123 -119 -123
		mu 0 4 139 138 140 141
		f 4 118 125 -120 -125
		mu 0 4 141 140 142 143
		f 4 119 127 -117 -127
		mu 0 4 143 142 144 145
		f 4 -128 -126 -124 -122
		mu 0 4 137 146 147 138
		f 4 126 120 122 124
		mu 0 4 148 136 139 149
		f 4 128 133 -130 -133
		mu 0 4 150 151 152 153
		f 4 129 135 -131 -135
		mu 0 4 153 152 154 155
		f 4 130 137 -132 -137
		mu 0 4 155 154 156 157
		f 4 131 139 -129 -139
		mu 0 4 157 156 158 159
		f 4 -140 -138 -136 -134
		mu 0 4 151 160 161 152
		f 4 138 132 134 136
		mu 0 4 162 150 153 163
		f 4 140 145 -142 -145
		mu 0 4 164 165 166 167
		f 4 141 147 -143 -147
		mu 0 4 167 166 168 169
		f 4 142 149 -144 -149
		mu 0 4 169 168 170 171
		f 4 143 151 -141 -151
		mu 0 4 171 170 172 173
		f 4 -152 -150 -148 -146
		mu 0 4 165 174 175 166
		f 4 150 144 146 148
		mu 0 4 176 164 167 177
		f 4 152 157 -154 -157
		mu 0 4 178 179 180 181
		f 4 153 159 -155 -159
		mu 0 4 181 180 182 183
		f 4 154 161 -156 -161
		mu 0 4 183 182 184 185
		f 4 155 163 -153 -163
		mu 0 4 185 184 186 187
		f 4 -164 -162 -160 -158
		mu 0 4 179 188 189 180
		f 4 162 156 158 160
		mu 0 4 190 178 181 191
		f 4 164 169 -166 -169
		mu 0 4 192 193 194 195
		f 4 165 171 -167 -171
		mu 0 4 195 194 196 197
		f 4 166 173 -168 -173
		mu 0 4 197 196 198 199
		f 4 167 175 -165 -175
		mu 0 4 199 198 200 201
		f 4 -176 -174 -172 -170
		mu 0 4 193 202 203 194
		f 4 174 168 170 172
		mu 0 4 204 192 195 205
		f 4 176 181 -178 -181
		mu 0 4 206 207 208 209
		f 4 177 183 -179 -183
		mu 0 4 209 208 210 211
		f 4 178 185 -180 -185
		mu 0 4 211 210 212 213
		f 4 179 187 -177 -187
		mu 0 4 213 212 214 215
		f 4 -188 -186 -184 -182
		mu 0 4 207 216 217 208
		f 4 186 180 182 184
		mu 0 4 218 206 209 219
		f 4 188 193 -190 -193
		mu 0 4 220 221 222 223
		f 4 189 195 -191 -195
		mu 0 4 223 222 224 225
		f 4 190 197 -192 -197
		mu 0 4 225 224 226 227
		f 4 191 199 -189 -199
		mu 0 4 227 226 228 229
		f 4 -200 -198 -196 -194
		mu 0 4 221 230 231 222
		f 4 198 192 194 196
		mu 0 4 232 220 223 233
		f 4 200 205 -202 -205
		mu 0 4 234 235 236 237
		f 4 201 207 -203 -207
		mu 0 4 237 236 238 239
		f 4 202 209 -204 -209
		mu 0 4 239 238 240 241
		f 4 203 211 -201 -211
		mu 0 4 241 240 242 243
		f 4 -212 -210 -208 -206
		mu 0 4 235 244 245 236
		f 4 210 204 206 208
		mu 0 4 246 234 237 247
		f 4 212 217 -214 -217
		mu 0 4 248 249 250 251
		f 4 213 219 -215 -219
		mu 0 4 251 250 252 253
		f 4 214 221 -216 -221
		mu 0 4 253 252 254 255
		f 4 215 223 -213 -223
		mu 0 4 255 254 256 257
		f 4 -224 -222 -220 -218
		mu 0 4 249 258 259 250
		f 4 222 216 218 220
		mu 0 4 260 248 251 261
		f 4 224 229 -226 -229
		mu 0 4 262 263 264 265
		f 4 225 231 -227 -231
		mu 0 4 265 264 266 267
		f 4 226 233 -228 -233
		mu 0 4 267 266 268 269
		f 4 227 235 -225 -235
		mu 0 4 269 268 270 271
		f 4 -236 -234 -232 -230
		mu 0 4 263 272 273 264
		f 4 234 228 230 232
		mu 0 4 274 262 265 275
		f 4 236 241 -238 -241
		mu 0 4 276 277 278 279
		f 4 237 243 -239 -243
		mu 0 4 279 278 280 281
		f 4 238 245 -240 -245
		mu 0 4 281 280 282 283
		f 4 239 247 -237 -247
		mu 0 4 283 282 284 285
		f 4 -248 -246 -244 -242
		mu 0 4 277 286 287 278
		f 4 246 240 242 244
		mu 0 4 288 276 279 289
		f 4 248 253 -250 -253
		mu 0 4 290 291 292 293
		f 4 249 255 -251 -255
		mu 0 4 293 292 294 295
		f 4 250 257 -252 -257
		mu 0 4 295 294 296 297
		f 4 251 259 -249 -259
		mu 0 4 297 296 298 299
		f 4 -260 -258 -256 -254
		mu 0 4 291 300 301 292
		f 4 258 252 254 256
		mu 0 4 302 290 293 303
		f 4 260 265 -262 -265
		mu 0 4 304 305 306 307
		f 4 261 267 -263 -267
		mu 0 4 307 306 308 309
		f 4 262 269 -264 -269
		mu 0 4 309 308 310 311
		f 4 263 271 -261 -271
		mu 0 4 311 310 312 313
		f 4 -272 -270 -268 -266
		mu 0 4 305 314 315 306
		f 4 270 264 266 268
		mu 0 4 316 304 307 317
		f 4 272 277 -274 -277
		mu 0 4 318 319 320 321
		f 4 273 279 -275 -279
		mu 0 4 321 320 322 323
		f 4 274 281 -276 -281
		mu 0 4 323 322 324 325
		f 4 275 283 -273 -283
		mu 0 4 325 324 326 327
		f 4 -284 -282 -280 -278
		mu 0 4 319 328 329 320
		f 4 282 276 278 280
		mu 0 4 330 318 321 331
		f 4 284 289 -286 -289
		mu 0 4 332 333 334 335
		f 4 285 291 -287 -291
		mu 0 4 335 334 336 337
		f 4 286 293 -288 -293
		mu 0 4 337 336 338 339
		f 4 287 295 -285 -295
		mu 0 4 339 338 340 341
		f 4 -296 -294 -292 -290
		mu 0 4 333 342 343 334
		f 4 294 288 290 292
		mu 0 4 344 332 335 345
		f 4 296 301 -298 -301
		mu 0 4 346 347 348 349
		f 4 297 303 -299 -303
		mu 0 4 349 348 350 351
		f 4 298 305 -300 -305
		mu 0 4 351 350 352 353
		f 4 299 307 -297 -307
		mu 0 4 353 352 354 355
		f 4 -308 -306 -304 -302
		mu 0 4 347 356 357 348
		f 4 306 300 302 304
		mu 0 4 358 346 349 359
		f 4 308 313 -310 -313
		mu 0 4 360 361 362 363
		f 4 309 315 -311 -315
		mu 0 4 363 362 364 365
		f 4 310 317 -312 -317
		mu 0 4 365 364 366 367
		f 4 311 319 -309 -319
		mu 0 4 367 366 368 369
		f 4 -320 -318 -316 -314
		mu 0 4 361 370 371 362
		f 4 318 312 314 316
		mu 0 4 372 360 363 373
		f 4 320 325 -322 -325
		mu 0 4 374 375 376 377
		f 4 321 327 -323 -327
		mu 0 4 377 376 378 379
		f 4 322 329 -324 -329
		mu 0 4 379 378 380 381
		f 4 323 331 -321 -331
		mu 0 4 381 380 382 383
		f 4 -332 -330 -328 -326
		mu 0 4 375 384 385 376
		f 4 330 324 326 328
		mu 0 4 386 374 377 387
		f 4 332 337 -334 -337
		mu 0 4 388 389 390 391
		f 4 333 339 -335 -339
		mu 0 4 391 390 392 393
		f 4 334 341 -336 -341
		mu 0 4 393 392 394 395
		f 4 335 343 -333 -343
		mu 0 4 395 394 396 397
		f 4 -344 -342 -340 -338
		mu 0 4 389 398 399 390
		f 4 342 336 338 340
		mu 0 4 400 388 391 401
		f 4 344 349 -346 -349
		mu 0 4 402 403 404 405
		f 4 345 351 -347 -351
		mu 0 4 405 404 406 407
		f 4 346 353 -348 -353
		mu 0 4 407 406 408 409
		f 4 347 355 -345 -355
		mu 0 4 409 408 410 411
		f 4 -356 -354 -352 -350
		mu 0 4 403 412 413 404
		f 4 354 348 350 352
		mu 0 4 414 402 405 415
		f 4 356 361 -358 -361
		mu 0 4 416 417 418 419
		f 4 357 363 -359 -363
		mu 0 4 419 418 420 421
		f 4 358 365 -360 -365
		mu 0 4 421 420 422 423
		f 4 359 367 -357 -367
		mu 0 4 423 422 424 425
		f 4 -368 -366 -364 -362
		mu 0 4 417 426 427 418
		f 4 366 360 362 364
		mu 0 4 428 416 419 429
		f 4 368 373 -370 -373
		mu 0 4 430 431 432 433
		f 4 369 375 -371 -375
		mu 0 4 433 432 434 435
		f 4 370 377 -372 -377
		mu 0 4 435 434 436 437
		f 4 371 379 -369 -379
		mu 0 4 437 436 438 439
		f 4 -380 -378 -376 -374
		mu 0 4 431 440 441 432
		f 4 378 372 374 376
		mu 0 4 442 430 433 443
		f 4 380 385 -382 -385
		mu 0 4 444 445 446 447
		f 4 381 387 -383 -387
		mu 0 4 447 446 448 449
		f 4 382 389 -384 -389
		mu 0 4 449 448 450 451
		f 4 383 391 -381 -391
		mu 0 4 451 450 452 453
		f 4 -392 -390 -388 -386
		mu 0 4 445 454 455 446
		f 4 390 384 386 388
		mu 0 4 456 444 447 457
		f 4 392 397 -394 -397
		mu 0 4 458 459 460 461
		f 4 393 399 -395 -399
		mu 0 4 461 460 462 463
		f 4 394 401 -396 -401
		mu 0 4 463 462 464 465
		f 4 395 403 -393 -403
		mu 0 4 465 464 466 467
		f 4 -404 -402 -400 -398
		mu 0 4 459 468 469 460
		f 4 402 396 398 400
		mu 0 4 470 458 461 471
		f 4 404 409 -406 -409
		mu 0 4 472 473 474 475
		f 4 405 411 -407 -411
		mu 0 4 475 474 476 477
		f 4 406 413 -408 -413
		mu 0 4 477 476 478 479
		f 4 407 415 -405 -415
		mu 0 4 479 478 480 481
		f 4 -416 -414 -412 -410
		mu 0 4 473 482 483 474
		f 4 414 408 410 412
		mu 0 4 484 472 475 485
		f 4 416 421 -418 -421
		mu 0 4 486 487 488 489
		f 4 417 423 -419 -423
		mu 0 4 489 488 490 491
		f 4 418 425 -420 -425
		mu 0 4 491 490 492 493
		f 4 419 427 -417 -427
		mu 0 4 493 492 494 495
		f 4 -428 -426 -424 -422
		mu 0 4 487 496 497 488
		f 4 426 420 422 424
		mu 0 4 498 486 489 499
		f 4 428 433 -430 -433
		mu 0 4 500 501 502 503
		f 4 429 435 -431 -435
		mu 0 4 503 502 504 505
		f 4 430 437 -432 -437
		mu 0 4 505 504 506 507
		f 4 431 439 -429 -439
		mu 0 4 507 506 508 509
		f 4 -440 -438 -436 -434
		mu 0 4 501 510 511 502
		f 4 438 432 434 436
		mu 0 4 512 500 503 513
		f 4 440 445 -442 -445
		mu 0 4 514 515 516 517
		f 4 441 447 -443 -447
		mu 0 4 517 516 518 519
		f 4 442 449 -444 -449
		mu 0 4 519 518 520 521
		f 4 443 451 -441 -451
		mu 0 4 521 520 522 523
		f 4 -452 -450 -448 -446
		mu 0 4 515 524 525 516
		f 4 450 444 446 448
		mu 0 4 526 514 517 527
		f 4 452 457 -454 -457
		mu 0 4 528 529 530 531
		f 4 453 459 -455 -459
		mu 0 4 531 530 532 533
		f 4 454 461 -456 -461
		mu 0 4 533 532 534 535
		f 4 455 463 -453 -463
		mu 0 4 535 534 536 537
		f 4 -464 -462 -460 -458
		mu 0 4 529 538 539 530
		f 4 462 456 458 460
		mu 0 4 540 528 531 541
		f 4 464 469 -466 -469
		mu 0 4 542 543 544 545
		f 4 465 471 -467 -471
		mu 0 4 545 544 546 547
		f 4 466 473 -468 -473
		mu 0 4 547 546 548 549
		f 4 467 475 -465 -475
		mu 0 4 549 548 550 551
		f 4 -476 -474 -472 -470
		mu 0 4 543 552 553 544
		f 4 474 468 470 472
		mu 0 4 554 542 545 555
		f 4 476 481 -478 -481
		mu 0 4 556 557 558 559
		f 4 477 483 -479 -483
		mu 0 4 559 558 560 561
		f 4 478 485 -480 -485
		mu 0 4 561 560 562 563
		f 4 479 487 -477 -487
		mu 0 4 563 562 564 565
		f 4 -488 -486 -484 -482
		mu 0 4 557 566 567 558
		f 4 486 480 482 484
		mu 0 4 568 556 559 569
		f 4 488 493 -490 -493
		mu 0 4 570 571 572 573
		f 4 489 495 -491 -495
		mu 0 4 573 572 574 575
		f 4 490 497 -492 -497
		mu 0 4 575 574 576 577
		f 4 491 499 -489 -499
		mu 0 4 577 576 578 579
		f 4 -500 -498 -496 -494
		mu 0 4 571 580 581 572
		f 4 498 492 494 496
		mu 0 4 582 570 573 583
		f 4 500 505 -502 -505
		mu 0 4 584 585 586 587
		f 4 501 507 -503 -507
		mu 0 4 587 586 588 589
		f 4 502 509 -504 -509
		mu 0 4 589 588 590 591
		f 4 503 511 -501 -511
		mu 0 4 591 590 592 593
		f 4 -512 -510 -508 -506
		mu 0 4 585 594 595 586
		f 4 510 504 506 508
		mu 0 4 596 584 587 597
		f 4 512 517 -514 -517
		mu 0 4 598 599 600 601
		f 4 513 519 -515 -519
		mu 0 4 601 600 602 603
		f 4 514 521 -516 -521
		mu 0 4 603 602 604 605
		f 4 515 523 -513 -523
		mu 0 4 605 604 606 607
		f 4 -524 -522 -520 -518
		mu 0 4 599 608 609 600
		f 4 522 516 518 520
		mu 0 4 610 598 601 611
		f 4 524 529 -526 -529
		mu 0 4 612 613 614 615
		f 4 525 531 -527 -531
		mu 0 4 615 614 616 617
		f 4 526 533 -528 -533
		mu 0 4 617 616 618 619
		f 4 527 535 -525 -535
		mu 0 4 619 618 620 621
		f 4 -536 -534 -532 -530
		mu 0 4 613 622 623 614
		f 4 534 528 530 532
		mu 0 4 624 612 615 625
		f 4 536 541 -538 -541
		mu 0 4 626 627 628 629
		f 4 537 543 -539 -543
		mu 0 4 629 628 630 631
		f 4 538 545 -540 -545
		mu 0 4 631 630 632 633
		f 4 539 547 -537 -547
		mu 0 4 633 632 634 635
		f 4 -548 -546 -544 -542
		mu 0 4 627 636 637 628
		f 4 546 540 542 544
		mu 0 4 638 626 629 639
		f 4 548 553 -550 -553
		mu 0 4 640 641 642 643
		f 4 549 555 -551 -555
		mu 0 4 643 642 644 645
		f 4 550 557 -552 -557
		mu 0 4 645 644 646 647
		f 4 551 559 -549 -559
		mu 0 4 647 646 648 649
		f 4 -560 -558 -556 -554
		mu 0 4 641 650 651 642
		f 4 558 552 554 556
		mu 0 4 652 640 643 653
		f 4 560 565 -562 -565
		mu 0 4 654 655 656 657
		f 4 561 567 -563 -567
		mu 0 4 657 656 658 659
		f 4 562 569 -564 -569
		mu 0 4 659 658 660 661
		f 4 563 571 -561 -571
		mu 0 4 661 660 662 663
		f 4 -572 -570 -568 -566
		mu 0 4 655 664 665 656
		f 4 570 564 566 568
		mu 0 4 666 654 657 667
		f 4 572 577 -574 -577
		mu 0 4 668 669 670 671
		f 4 573 579 -575 -579
		mu 0 4 671 670 672 673
		f 4 574 581 -576 -581
		mu 0 4 673 672 674 675
		f 4 575 583 -573 -583
		mu 0 4 675 674 676 677
		f 4 -584 -582 -580 -578
		mu 0 4 669 678 679 670
		f 4 582 576 578 580
		mu 0 4 680 668 671 681
		f 4 584 589 -586 -589
		mu 0 4 682 683 684 685
		f 4 585 591 -587 -591
		mu 0 4 685 684 686 687
		f 4 586 593 -588 -593
		mu 0 4 687 686 688 689
		f 4 587 595 -585 -595
		mu 0 4 689 688 690 691
		f 4 -596 -594 -592 -590
		mu 0 4 683 692 693 684
		f 4 594 588 590 592
		mu 0 4 694 682 685 695;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder2";
	rename -uid "D0C1F2B6-4B33-686A-2775-C9B8590C3B30";
	setAttr ".t" -type "double3" 351.84754671497501 26.840860492558136 -58.777693576407188 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 14.469168496331024 14.469168496331024 14.469168496331024 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "EF75F508-437A-8944-B20A-659A924ADA90";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.328125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCylinder2";
	rename -uid "D47D6AF4-4388-E42A-10EB-27873B92E692";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999994039535522 0.48599746823310852 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[84:111]" -type "float3"  -7.1054274e-015 -0.04860916 
		1.7763568e-015 -7.1054274e-015 -0.04860916 3.5527137e-015 -7.1054274e-015 0.04860916 
		1.7763568e-015 -7.1054274e-015 0.04860916 3.5527137e-015 -4.4408921e-015 -0.04860916 
		3.5527137e-015 -4.4408921e-015 0.04860916 3.5527137e-015 -2.6645353e-015 -0.04860916 
		3.5527137e-015 -2.6645353e-015 0.04860916 3.5527137e-015 0 -0.04860916 3.5527137e-015 
		0 0.04860916 3.5527137e-015 0 -0.04860916 1.7763568e-015 0 0.04860916 1.7763568e-015 
		0 -0.04860916 8.4703295e-022 0 0.04860916 8.4703295e-022 0 -0.04860916 -1.7763568e-015 
		0 0.04860916 -1.7763568e-015 0 -0.04860916 -3.5527137e-015 0 0.04860916 -3.5527137e-015 
		-2.6645353e-015 -0.04860916 -3.5527137e-015 -2.6645353e-015 0.04860916 -3.5527137e-015 
		-4.4408921e-015 -0.04860916 -3.5527137e-015 -4.4408921e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -3.5527137e-015 -7.1054274e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -1.7763568e-015 -7.1054274e-015 0.04860916 -1.7763568e-015 
		-7.1054274e-015 -0.04860916 -8.4703295e-022 -7.1054274e-015 0.04860916 -8.4703295e-022;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.28067017 -0.77260256 1.11022878 0.28067017 -1.39218235
		 1.6043272 -0.35766983 -0.77260256 1.11022878 -0.35766983 -1.39218235 0.39623618 0.28067017 -1.73602343
		 0.39623618 -0.35766983 -1.73602343 -0.3962357 0.28067017 -1.73602343 -0.3962357 -0.35766983 -1.7360239
		 -1.11022842 0.28067017 -1.39218283 -1.11022842 -0.35766983 -1.39218283 -1.60432696 0.28067017 -0.77260351
		 -1.60432696 -0.35766983 -0.77260351 -1.78066862 0.28067017 -4.7683716e-007 -1.78066862 -0.35766983 -4.7683716e-007
		 -1.60432744 0.28067017 0.77260303 -1.60432744 -0.35766983 0.77260303 -1.11022902 0.28067017 1.39218235
		 -1.11022902 -0.35766983 1.39218235 -0.39623618 0.28067017 1.73602343 -0.39623618 -0.35766983 1.73602343
		 0.39623594 0.28067017 1.73602438 0.39623594 -0.35766983 1.73602438 1.11022854 0.28067017 1.39218235
		 1.11022854 -0.35766983 1.39218235 1.6043272 0.28067017 0.77260303 1.6043272 -0.35766983 0.77260303
		 1.7806685 0.28067017 4.7683716e-007 1.7806685 -0.35766983 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 0 1 113 0 112 113 0 2 114 0 113 114 0 3 115 0 114 115 0 4 116 0 115 116 0 5 117 0
		 116 117 0 6 118 0 117 118 0 7 119 0 118 119 0 8 120 1 119 120 0 9 121 0 120 121 0
		 10 122 0 121 122 0 11 123 0 122 123 0 12 124 0 123 124 0 13 125 0 124 125 0 125 112 0
		 42 126 0 43 127 0 126 127 0 127 128 1 126 128 1 44 129 0 127 129 0 129 128 1 45 130 0
		 129 130 0 130 128 1 46 131 0 130 131 0 131 128 1 47 132 0 131 132 0 132 128 1 48 133 0
		 132 133 0 133 128 0 49 134 0 133 134 0 134 128 0 50 135 0 134 135 0 135 128 1 51 136 0
		 135 136 0 136 128 1 52 137 0 136 137 0 137 128 1 53 138 0 137 138 0 138 128 1 54 139 0
		 138 139 0 139 128 1 55 140 0 139 140 0 140 128 1 140 126 0 112 141 0 113 142 0 141 142 0
		 114 143 0 142 143 0 115 144 0 143 144 0 116 145 0 144 145 0 117 146 0 145 146 0 118 147 0
		 146 147 0 119 148 0 147 148 0 120 149 0 148 149 0 121 150 0 149 150 0 122 151 0 150 151 0
		 123 152 0 151 152 0 124 153 0 152 153 0 125 154 0 153 154 0 154 141 0 134 155 0 128 155 0
		 133 155 0 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder3";
	rename -uid "26F7F662-4815-6F85-7A00-8FA99CE9413E";
	setAttr ".t" -type "double3" 351.84754671497501 26.840860492558136 60.854375012398648 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 14.469168496331024 14.469168496331024 14.469168496331024 ;
createNode mesh -n "pCylinderShape3" -p "pCylinder3";
	rename -uid "66FA3A78-4874-6309-07EC-51A59A916B56";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.328125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.232061 -0.77260256 1.11022878 0.232061 -1.39218235
		 1.6043272 -0.30906066 -0.77260256 1.11022878 -0.30906066 -1.39218235 0.39623618 0.232061 -1.73602343
		 0.39623618 -0.30906066 -1.73602343 -0.3962357 0.232061 -1.73602343 -0.3962357 -0.30906066 -1.7360239
		 -1.11022842 0.232061 -1.39218283 -1.11022842 -0.30906066 -1.39218283 -1.60432696 0.232061 -0.77260351
		 -1.60432696 -0.30906066 -0.77260351 -1.78066862 0.232061 -4.7683716e-007 -1.78066862 -0.30906066 -4.7683716e-007
		 -1.60432744 0.232061 0.77260303 -1.60432744 -0.30906066 0.77260303 -1.11022902 0.232061 1.39218235
		 -1.11022902 -0.30906066 1.39218235 -0.39623618 0.232061 1.73602343 -0.39623618 -0.30906066 1.73602343
		 0.39623594 0.232061 1.73602438 0.39623594 -0.30906066 1.73602438 1.11022854 0.232061 1.39218235
		 1.11022854 -0.30906066 1.39218235 1.6043272 0.232061 0.77260303 1.6043272 -0.30906066 0.77260303
		 1.7806685 0.232061 4.7683716e-007 1.7806685 -0.30906066 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 1 1 113 1 112 113 0 2 114 1 113 114 0 3 115 1 114 115 0 4 116 1 115 116 0 5 117 1
		 116 117 0 6 118 1 117 118 0 7 119 1 118 119 0 8 120 1 119 120 0 9 121 1 120 121 0
		 10 122 1 121 122 0 11 123 1 122 123 0 12 124 1 123 124 0 13 125 1 124 125 0 125 112 0
		 42 126 1 43 127 1 126 127 0 127 128 1 126 128 1 44 129 1 127 129 0 129 128 1 45 130 1
		 129 130 0 130 128 1 46 131 1 130 131 0 131 128 1 47 132 1 131 132 0 132 128 1 48 133 1
		 132 133 0 133 128 1 49 134 1 133 134 1 134 128 1 50 135 1 134 135 0 135 128 1 51 136 1
		 135 136 0 136 128 1 52 137 1 136 137 0 137 128 1 53 138 1 137 138 0 138 128 1 54 139 1
		 138 139 0 139 128 1 55 140 1 139 140 0 140 128 1 140 126 0 112 141 1 113 142 1 141 142 0
		 114 143 1 142 143 0 115 144 1 143 144 0 116 145 1 144 145 0 117 146 1 145 146 0 118 147 1
		 146 147 0 119 148 1 147 148 0 120 149 1 148 149 0 121 150 1 149 150 0 122 151 1 150 151 0
		 123 152 1 151 152 0 124 153 1 152 153 0 125 154 1 153 154 0 154 141 0 134 155 1 128 155 1
		 133 155 1 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCylinder3";
	rename -uid "E689A510-445B-8090-79D8-9AB25C763F9B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999994039535522 0.48599746823310852 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[84:111]" -type "float3"  -7.1054274e-015 -0.04860916 
		1.7763568e-015 -7.1054274e-015 -0.04860916 3.5527137e-015 -7.1054274e-015 0.04860916 
		1.7763568e-015 -7.1054274e-015 0.04860916 3.5527137e-015 -4.4408921e-015 -0.04860916 
		3.5527137e-015 -4.4408921e-015 0.04860916 3.5527137e-015 -2.6645353e-015 -0.04860916 
		3.5527137e-015 -2.6645353e-015 0.04860916 3.5527137e-015 0 -0.04860916 3.5527137e-015 
		0 0.04860916 3.5527137e-015 0 -0.04860916 1.7763568e-015 0 0.04860916 1.7763568e-015 
		0 -0.04860916 8.4703295e-022 0 0.04860916 8.4703295e-022 0 -0.04860916 -1.7763568e-015 
		0 0.04860916 -1.7763568e-015 0 -0.04860916 -3.5527137e-015 0 0.04860916 -3.5527137e-015 
		-2.6645353e-015 -0.04860916 -3.5527137e-015 -2.6645353e-015 0.04860916 -3.5527137e-015 
		-4.4408921e-015 -0.04860916 -3.5527137e-015 -4.4408921e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -3.5527137e-015 -7.1054274e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -1.7763568e-015 -7.1054274e-015 0.04860916 -1.7763568e-015 
		-7.1054274e-015 -0.04860916 -8.4703295e-022 -7.1054274e-015 0.04860916 -8.4703295e-022;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.28067017 -0.77260256 1.11022878 0.28067017 -1.39218235
		 1.6043272 -0.35766983 -0.77260256 1.11022878 -0.35766983 -1.39218235 0.39623618 0.28067017 -1.73602343
		 0.39623618 -0.35766983 -1.73602343 -0.3962357 0.28067017 -1.73602343 -0.3962357 -0.35766983 -1.7360239
		 -1.11022842 0.28067017 -1.39218283 -1.11022842 -0.35766983 -1.39218283 -1.60432696 0.28067017 -0.77260351
		 -1.60432696 -0.35766983 -0.77260351 -1.78066862 0.28067017 -4.7683716e-007 -1.78066862 -0.35766983 -4.7683716e-007
		 -1.60432744 0.28067017 0.77260303 -1.60432744 -0.35766983 0.77260303 -1.11022902 0.28067017 1.39218235
		 -1.11022902 -0.35766983 1.39218235 -0.39623618 0.28067017 1.73602343 -0.39623618 -0.35766983 1.73602343
		 0.39623594 0.28067017 1.73602438 0.39623594 -0.35766983 1.73602438 1.11022854 0.28067017 1.39218235
		 1.11022854 -0.35766983 1.39218235 1.6043272 0.28067017 0.77260303 1.6043272 -0.35766983 0.77260303
		 1.7806685 0.28067017 4.7683716e-007 1.7806685 -0.35766983 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 0 1 113 0 112 113 0 2 114 0 113 114 0 3 115 0 114 115 0 4 116 0 115 116 0 5 117 0
		 116 117 0 6 118 0 117 118 0 7 119 0 118 119 0 8 120 1 119 120 0 9 121 0 120 121 0
		 10 122 0 121 122 0 11 123 0 122 123 0 12 124 0 123 124 0 13 125 0 124 125 0 125 112 0
		 42 126 0 43 127 0 126 127 0 127 128 1 126 128 1 44 129 0 127 129 0 129 128 1 45 130 0
		 129 130 0 130 128 1 46 131 0 130 131 0 131 128 1 47 132 0 131 132 0 132 128 1 48 133 0
		 132 133 0 133 128 0 49 134 0 133 134 0 134 128 0 50 135 0 134 135 0 135 128 1 51 136 0
		 135 136 0 136 128 1 52 137 0 136 137 0 137 128 1 53 138 0 137 138 0 138 128 1 54 139 0
		 138 139 0 139 128 1 55 140 0 139 140 0 140 128 1 140 126 0 112 141 0 113 142 0 141 142 0
		 114 143 0 142 143 0 115 144 0 143 144 0 116 145 0 144 145 0 117 146 0 145 146 0 118 147 0
		 146 147 0 119 148 0 147 148 0 120 149 0 148 149 0 121 150 0 149 150 0 122 151 0 150 151 0
		 123 152 0 151 152 0 124 153 0 152 153 0 125 154 0 153 154 0 154 141 0 134 155 0 128 155 0
		 133 155 0 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder4";
	rename -uid "350B154B-4125-CDA6-43D2-E49E71EE0271";
	setAttr ".t" -type "double3" 192.20423263649528 26.840860492558136 60.854375012398648 ;
	setAttr ".r" -type "double3" 180 0 90 ;
	setAttr ".s" -type "double3" 14.469168496331024 14.469168496331024 14.469168496331024 ;
createNode mesh -n "pCylinderShape4" -p "pCylinder4";
	rename -uid "BC10726B-49A6-A6FA-0791-0DAC78397C52";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.328125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.232061 -0.77260256 1.11022878 0.232061 -1.39218235
		 1.6043272 -0.30906066 -0.77260256 1.11022878 -0.30906066 -1.39218235 0.39623618 0.232061 -1.73602343
		 0.39623618 -0.30906066 -1.73602343 -0.3962357 0.232061 -1.73602343 -0.3962357 -0.30906066 -1.7360239
		 -1.11022842 0.232061 -1.39218283 -1.11022842 -0.30906066 -1.39218283 -1.60432696 0.232061 -0.77260351
		 -1.60432696 -0.30906066 -0.77260351 -1.78066862 0.232061 -4.7683716e-007 -1.78066862 -0.30906066 -4.7683716e-007
		 -1.60432744 0.232061 0.77260303 -1.60432744 -0.30906066 0.77260303 -1.11022902 0.232061 1.39218235
		 -1.11022902 -0.30906066 1.39218235 -0.39623618 0.232061 1.73602343 -0.39623618 -0.30906066 1.73602343
		 0.39623594 0.232061 1.73602438 0.39623594 -0.30906066 1.73602438 1.11022854 0.232061 1.39218235
		 1.11022854 -0.30906066 1.39218235 1.6043272 0.232061 0.77260303 1.6043272 -0.30906066 0.77260303
		 1.7806685 0.232061 4.7683716e-007 1.7806685 -0.30906066 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 1 1 113 1 112 113 0 2 114 1 113 114 0 3 115 1 114 115 0 4 116 1 115 116 0 5 117 1
		 116 117 0 6 118 1 117 118 0 7 119 1 118 119 0 8 120 1 119 120 0 9 121 1 120 121 0
		 10 122 1 121 122 0 11 123 1 122 123 0 12 124 1 123 124 0 13 125 1 124 125 0 125 112 0
		 42 126 1 43 127 1 126 127 0 127 128 1 126 128 1 44 129 1 127 129 0 129 128 1 45 130 1
		 129 130 0 130 128 1 46 131 1 130 131 0 131 128 1 47 132 1 131 132 0 132 128 1 48 133 1
		 132 133 0 133 128 1 49 134 1 133 134 1 134 128 1 50 135 1 134 135 0 135 128 1 51 136 1
		 135 136 0 136 128 1 52 137 1 136 137 0 137 128 1 53 138 1 137 138 0 138 128 1 54 139 1
		 138 139 0 139 128 1 55 140 1 139 140 0 140 128 1 140 126 0 112 141 1 113 142 1 141 142 0
		 114 143 1 142 143 0 115 144 1 143 144 0 116 145 1 144 145 0 117 146 1 145 146 0 118 147 1
		 146 147 0 119 148 1 147 148 0 120 149 1 148 149 0 121 150 1 149 150 0 122 151 1 150 151 0
		 123 152 1 151 152 0 124 153 1 152 153 0 125 154 1 153 154 0 154 141 0 134 155 1 128 155 1
		 133 155 1 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCylinder4";
	rename -uid "DBBAD71A-4CEE-BCAD-5162-CD9525766E36";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999994039535522 0.48599746823310852 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[84:111]" -type "float3"  -7.1054274e-015 -0.04860916 
		1.7763568e-015 -7.1054274e-015 -0.04860916 3.5527137e-015 -7.1054274e-015 0.04860916 
		1.7763568e-015 -7.1054274e-015 0.04860916 3.5527137e-015 -4.4408921e-015 -0.04860916 
		3.5527137e-015 -4.4408921e-015 0.04860916 3.5527137e-015 -2.6645353e-015 -0.04860916 
		3.5527137e-015 -2.6645353e-015 0.04860916 3.5527137e-015 0 -0.04860916 3.5527137e-015 
		0 0.04860916 3.5527137e-015 0 -0.04860916 1.7763568e-015 0 0.04860916 1.7763568e-015 
		0 -0.04860916 8.4703295e-022 0 0.04860916 8.4703295e-022 0 -0.04860916 -1.7763568e-015 
		0 0.04860916 -1.7763568e-015 0 -0.04860916 -3.5527137e-015 0 0.04860916 -3.5527137e-015 
		-2.6645353e-015 -0.04860916 -3.5527137e-015 -2.6645353e-015 0.04860916 -3.5527137e-015 
		-4.4408921e-015 -0.04860916 -3.5527137e-015 -4.4408921e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -3.5527137e-015 -7.1054274e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -1.7763568e-015 -7.1054274e-015 0.04860916 -1.7763568e-015 
		-7.1054274e-015 -0.04860916 -8.4703295e-022 -7.1054274e-015 0.04860916 -8.4703295e-022;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.28067017 -0.77260256 1.11022878 0.28067017 -1.39218235
		 1.6043272 -0.35766983 -0.77260256 1.11022878 -0.35766983 -1.39218235 0.39623618 0.28067017 -1.73602343
		 0.39623618 -0.35766983 -1.73602343 -0.3962357 0.28067017 -1.73602343 -0.3962357 -0.35766983 -1.7360239
		 -1.11022842 0.28067017 -1.39218283 -1.11022842 -0.35766983 -1.39218283 -1.60432696 0.28067017 -0.77260351
		 -1.60432696 -0.35766983 -0.77260351 -1.78066862 0.28067017 -4.7683716e-007 -1.78066862 -0.35766983 -4.7683716e-007
		 -1.60432744 0.28067017 0.77260303 -1.60432744 -0.35766983 0.77260303 -1.11022902 0.28067017 1.39218235
		 -1.11022902 -0.35766983 1.39218235 -0.39623618 0.28067017 1.73602343 -0.39623618 -0.35766983 1.73602343
		 0.39623594 0.28067017 1.73602438 0.39623594 -0.35766983 1.73602438 1.11022854 0.28067017 1.39218235
		 1.11022854 -0.35766983 1.39218235 1.6043272 0.28067017 0.77260303 1.6043272 -0.35766983 0.77260303
		 1.7806685 0.28067017 4.7683716e-007 1.7806685 -0.35766983 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 0 1 113 0 112 113 0 2 114 0 113 114 0 3 115 0 114 115 0 4 116 0 115 116 0 5 117 0
		 116 117 0 6 118 0 117 118 0 7 119 0 118 119 0 8 120 1 119 120 0 9 121 0 120 121 0
		 10 122 0 121 122 0 11 123 0 122 123 0 12 124 0 123 124 0 13 125 0 124 125 0 125 112 0
		 42 126 0 43 127 0 126 127 0 127 128 1 126 128 1 44 129 0 127 129 0 129 128 1 45 130 0
		 129 130 0 130 128 1 46 131 0 130 131 0 131 128 1 47 132 0 131 132 0 132 128 1 48 133 0
		 132 133 0 133 128 0 49 134 0 133 134 0 134 128 0 50 135 0 134 135 0 135 128 1 51 136 0
		 135 136 0 136 128 1 52 137 0 136 137 0 137 128 1 53 138 0 137 138 0 138 128 1 54 139 0
		 138 139 0 139 128 1 55 140 0 139 140 0 140 128 1 140 126 0 112 141 0 113 142 0 141 142 0
		 114 143 0 142 143 0 115 144 0 143 144 0 116 145 0 144 145 0 117 146 0 145 146 0 118 147 0
		 146 147 0 119 148 0 147 148 0 120 149 0 148 149 0 121 150 0 149 150 0 122 151 0 150 151 0
		 123 152 0 151 152 0 124 153 0 152 153 0 125 154 0 153 154 0 154 141 0 134 155 0 128 155 0
		 133 155 0 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder5";
	rename -uid "011E6749-489C-E88D-F978-16B5D2D2C23F";
	setAttr ".t" -type "double3" 192.20423263649528 26.840860492558136 -58.777693576407188 ;
	setAttr ".r" -type "double3" 180 0 90 ;
	setAttr ".s" -type "double3" 14.469168496331024 14.469168496331024 14.469168496331024 ;
createNode mesh -n "pCylinderShape5" -p "pCylinder5";
	rename -uid "D602BB25-4444-9F5A-2C6F-61AEB9EBEE09";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.328125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.232061 -0.77260256 1.11022878 0.232061 -1.39218235
		 1.6043272 -0.30906066 -0.77260256 1.11022878 -0.30906066 -1.39218235 0.39623618 0.232061 -1.73602343
		 0.39623618 -0.30906066 -1.73602343 -0.3962357 0.232061 -1.73602343 -0.3962357 -0.30906066 -1.7360239
		 -1.11022842 0.232061 -1.39218283 -1.11022842 -0.30906066 -1.39218283 -1.60432696 0.232061 -0.77260351
		 -1.60432696 -0.30906066 -0.77260351 -1.78066862 0.232061 -4.7683716e-007 -1.78066862 -0.30906066 -4.7683716e-007
		 -1.60432744 0.232061 0.77260303 -1.60432744 -0.30906066 0.77260303 -1.11022902 0.232061 1.39218235
		 -1.11022902 -0.30906066 1.39218235 -0.39623618 0.232061 1.73602343 -0.39623618 -0.30906066 1.73602343
		 0.39623594 0.232061 1.73602438 0.39623594 -0.30906066 1.73602438 1.11022854 0.232061 1.39218235
		 1.11022854 -0.30906066 1.39218235 1.6043272 0.232061 0.77260303 1.6043272 -0.30906066 0.77260303
		 1.7806685 0.232061 4.7683716e-007 1.7806685 -0.30906066 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 1 1 113 1 112 113 0 2 114 1 113 114 0 3 115 1 114 115 0 4 116 1 115 116 0 5 117 1
		 116 117 0 6 118 1 117 118 0 7 119 1 118 119 0 8 120 1 119 120 0 9 121 1 120 121 0
		 10 122 1 121 122 0 11 123 1 122 123 0 12 124 1 123 124 0 13 125 1 124 125 0 125 112 0
		 42 126 1 43 127 1 126 127 0 127 128 1 126 128 1 44 129 1 127 129 0 129 128 1 45 130 1
		 129 130 0 130 128 1 46 131 1 130 131 0 131 128 1 47 132 1 131 132 0 132 128 1 48 133 1
		 132 133 0 133 128 1 49 134 1 133 134 1 134 128 1 50 135 1 134 135 0 135 128 1 51 136 1
		 135 136 0 136 128 1 52 137 1 136 137 0 137 128 1 53 138 1 137 138 0 138 128 1 54 139 1
		 138 139 0 139 128 1 55 140 1 139 140 0 140 128 1 140 126 0 112 141 1 113 142 1 141 142 0
		 114 143 1 142 143 0 115 144 1 143 144 0 116 145 1 144 145 0 117 146 1 145 146 0 118 147 1
		 146 147 0 119 148 1 147 148 0 120 149 1 148 149 0 121 150 1 149 150 0 122 151 1 150 151 0
		 123 152 1 151 152 0 124 153 1 152 153 0 125 154 1 153 154 0 154 141 0 134 155 1 128 155 1
		 133 155 1 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCylinder5";
	rename -uid "BB6445D4-4597-01B2-9F5D-09BE9DB15396";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999994039535522 0.48599746823310852 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 208 ".uvst[0].uvsp[0:207]" -type "float2" 0.5703882 0.12235284
		 0.54871011 0.095169432 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986 0.095169432
		 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986 0.21733057
		 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882 0.19014716
		 0.578125 0.15625 0.64077634 0.088455684 0.59742028 0.034088865 0.53476888 0.0039175451
		 0.46523112 0.0039175451 0.40257972 0.034088865 0.35922363 0.088455684 0.34375 0.15625
		 0.35922363 0.22404432 0.40257972 0.27841115 0.46523112 0.30858248 0.53476888 0.30858248
		 0.59742028 0.27841115 0.6407764 0.22404432 0.65625 0.15625 0.375 0.3125 0.39285713
		 0.3125 0.41071427 0.3125 0.4285714 0.3125 0.44642854 0.3125 0.46428567 0.3125 0.48214281
		 0.3125 0.49999994 0.3125 0.51785707 0.3125 0.53571421 0.3125 0.55357134 0.3125 0.57142848
		 0.3125 0.58928561 0.3125 0.60714275 0.3125 0.62499988 0.3125 0.375 0.68843985 0.39285713
		 0.68843985 0.41071427 0.68843985 0.4285714 0.68843985 0.44642854 0.68843985 0.46428567
		 0.68843985 0.48214281 0.68843985 0.49999994 0.68843985 0.51785707 0.68843985 0.53571421
		 0.68843985 0.55357134 0.68843985 0.57142848 0.68843985 0.58928561 0.68843985 0.60714275
		 0.68843985 0.62499988 0.68843985 0.64077634 0.77595568 0.59742028 0.72158885 0.53476888
		 0.69141757 0.46523112 0.69141757 0.40257972 0.72158885 0.35922363 0.77595568 0.34375
		 0.84375 0.35922363 0.91154432 0.40257972 0.96591115 0.46523112 0.99608248 0.53476888
		 0.99608248 0.59742028 0.96591115 0.6407764 0.91154432 0.65625 0.84375 0.5703882 0.80985284
		 0.54871011 0.78266943 0.51738447 0.76758379 0.48261556 0.76758379 0.45128986 0.78266943
		 0.4296118 0.80985284 0.421875 0.84375 0.4296118 0.87764716 0.45128986 0.90483057
		 0.48261556 0.91991627 0.51738447 0.91991627 0.54871017 0.90483057 0.5703882 0.87764716
		 0.578125 0.84375 0.5 0.15000001 0.5 0.83749998 0.62499988 0.60598683 0.375 0.60598683
		 0.39285713 0.60598683 0.41071427 0.60598683 0.4285714 0.60598683 0.44642854 0.60598683
		 0.46428567 0.60598683 0.48214281 0.60598683 0.49999994 0.60598683 0.51785707 0.60598683
		 0.53571421 0.60598683 0.55357134 0.60598683 0.57142848 0.60598683 0.58928561 0.60598683
		 0.60714275 0.60598683 0.62499988 0.3660081 0.375 0.3660081 0.39285713 0.3660081 0.41071427
		 0.3660081 0.4285714 0.3660081 0.44642854 0.3660081 0.4642857 0.3660081 0.48214284
		 0.3660081 0.49999997 0.3660081 0.51785707 0.3660081 0.53571421 0.3660081 0.55357134
		 0.3660081 0.57142848 0.3660081 0.58928561 0.3660081 0.60714275 0.3660081 0.39285713
		 0.60598683 0.375 0.60598683 0.375 0.3660081 0.39285713 0.3660081 0.41071427 0.60598683
		 0.41071427 0.3660081 0.4285714 0.60598683 0.4285714 0.3660081 0.44642854 0.60598683
		 0.44642854 0.3660081 0.46428567 0.60598683 0.4642857 0.3660081 0.48214281 0.60598683
		 0.48214284 0.3660081 0.49999994 0.60598683 0.49999997 0.3660081 0.51785707 0.60598683
		 0.51785707 0.3660081 0.53571421 0.60598683 0.53571421 0.3660081 0.55357134 0.60598683
		 0.55357134 0.3660081 0.57142848 0.60598683 0.57142848 0.3660081 0.58928561 0.60598683
		 0.58928561 0.3660081 0.60714275 0.60598683 0.60714275 0.3660081 0.62499988 0.60598683
		 0.62499988 0.3660081 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5703882 0.87764716
		 0.54871017 0.90483057 0.51738447 0.91991627 0.48261556 0.91991627 0.45128986 0.90483057
		 0.4296118 0.87764716 0.421875 0.84375 0.4296118 0.80985284 0.45128986 0.78266943
		 0.48261556 0.76758379 0.51738447 0.76758379 0.54871011 0.78266943 0.5703882 0.80985284
		 0.578125 0.84375 0.54871011 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773
		 0.48261556 0.080083773 0.45128986 0.095169432 0.4296118 0.12235284 0.421875 0.15625
		 0.4296118 0.19014716 0.45128986 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624
		 0.54871017 0.21733057 0.5703882 0.19014716 0.578125 0.15625 0.5 0.83749998 0.54871011
		 0.095169432 0.5703882 0.12235284 0.51738447 0.080083773 0.48261556 0.080083773 0.45128986
		 0.095169432 0.4296118 0.12235284 0.421875 0.15625 0.4296118 0.19014716 0.45128986
		 0.21733057 0.48261556 0.23241624 0.51738447 0.23241624 0.54871017 0.21733057 0.5703882
		 0.19014716 0.578125 0.15625 0 0 0.45652914 0 0.22824413 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[84:111]" -type "float3"  -7.1054274e-015 -0.04860916 
		1.7763568e-015 -7.1054274e-015 -0.04860916 3.5527137e-015 -7.1054274e-015 0.04860916 
		1.7763568e-015 -7.1054274e-015 0.04860916 3.5527137e-015 -4.4408921e-015 -0.04860916 
		3.5527137e-015 -4.4408921e-015 0.04860916 3.5527137e-015 -2.6645353e-015 -0.04860916 
		3.5527137e-015 -2.6645353e-015 0.04860916 3.5527137e-015 0 -0.04860916 3.5527137e-015 
		0 0.04860916 3.5527137e-015 0 -0.04860916 1.7763568e-015 0 0.04860916 1.7763568e-015 
		0 -0.04860916 8.4703295e-022 0 0.04860916 8.4703295e-022 0 -0.04860916 -1.7763568e-015 
		0 0.04860916 -1.7763568e-015 0 -0.04860916 -3.5527137e-015 0 0.04860916 -3.5527137e-015 
		-2.6645353e-015 -0.04860916 -3.5527137e-015 -2.6645353e-015 0.04860916 -3.5527137e-015 
		-4.4408921e-015 -0.04860916 -3.5527137e-015 -4.4408921e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -3.5527137e-015 -7.1054274e-015 0.04860916 -3.5527137e-015 
		-7.1054274e-015 -0.04860916 -1.7763568e-015 -7.1054274e-015 0.04860916 -1.7763568e-015 
		-7.1054274e-015 -0.04860916 -8.4703295e-022 -7.1054274e-015 0.04860916 -8.4703295e-022;
	setAttr -s 171 ".vt";
	setAttr ".vt[0:165]"  1.096515894 -0.5 -0.52805328 0.75881267 -0.5 -0.95152044
		 0.27081728 -0.5 -1.18652678 -0.27081692 -0.5 -1.18652678 -0.75881219 -0.5 -0.95152044
		 -1.096515775 -0.5 -0.52805424 -1.2170409 -0.5 0 -1.096515894 -0.5 0.52805376 -0.75881279 -0.5 0.95152092
		 -0.27081728 -0.5 1.18652773 0.27081704 -0.5 1.18652773 0.75881243 -0.5 0.95152092
		 1.096515894 -0.5 0.52805376 1.21704078 -0.5 0 1.80193758 -0.5 -0.86776638 1.24697971 -0.5 -1.56366253
		 0.44504213 -0.5 -1.94985533 -0.44504154 -0.5 -1.94985533 -1.24697912 -0.5 -1.56366301
		 -1.80193746 -0.5 -0.86776781 -1.99999976 -0.5 -4.7683716e-007 -1.80193758 -0.5 0.86776733
		 -1.24697971 -0.5 1.56366301 -0.44504213 -0.5 1.94985533 0.44504166 -0.5 1.94985628
		 1.24697924 -0.5 1.56366301 1.80193758 -0.5 0.86776733 1.99999976 -0.5 0 1.80193758 0.49999619 -0.86776638
		 1.24697971 0.49999619 -1.56366253 0.44504213 0.49999619 -1.94985533 -0.44504154 0.49999619 -1.94985533
		 -1.24697912 0.49999619 -1.56366301 -1.80193746 0.49999619 -0.86776781 -1.99999976 0.49999619 -4.7683716e-007
		 -1.80193758 0.49999619 0.86776733 -1.24697971 0.49999619 1.56366301 -0.44504213 0.49999619 1.94985533
		 0.44504166 0.49999619 1.94985628 1.24697924 0.49999619 1.56366301 1.80193758 0.49999619 0.86776733
		 1.99999976 0.49999619 0 1.096515894 0.49999619 -0.52805328 0.75881267 0.49999619 -0.95152044
		 0.27081728 0.49999619 -1.18652678 -0.27081692 0.49999619 -1.18652678 -0.75881219 0.49999619 -0.95152044
		 -1.096515775 0.49999619 -0.52805424 -1.2170409 0.49999619 0 -1.096515894 0.49999619 0.52805376
		 -0.75881279 0.49999619 0.95152092 -0.27081728 0.49999619 1.18652773 0.27081704 0.49999619 1.18652773
		 0.75881243 0.49999619 0.95152092 1.096515894 0.49999619 0.52805376 1.21704078 0.49999619 0
		 1.80193758 0.28067017 -0.86776638 1.24697971 0.28067017 -1.56366253 0.44504213 0.28067017 -1.94985533
		 -0.44504154 0.28067017 -1.94985533 -1.24697912 0.28067017 -1.56366301 -1.80193746 0.28067017 -0.86776781
		 -1.99999976 0.28067017 -4.7683716e-007 -1.80193782 0.28067017 0.86776733 -1.24697971 0.28067017 1.56366301
		 -0.44504213 0.28067017 1.94985533 0.44504166 0.28067017 1.94985628 1.24697924 0.28067017 1.56366301
		 1.80193758 0.28067017 0.86776733 1.99999976 0.28067017 0 1.80193758 -0.35766983 -0.86776638
		 1.24697995 -0.35766983 -1.56366253 0.44504213 -0.35766983 -1.94985533 -0.44504154 -0.35766983 -1.9498558
		 -1.24697912 -0.35766983 -1.56366301 -1.80193746 -0.35766983 -0.86776781 -1.99999976 -0.35766983 -4.7683716e-007
		 -1.80193782 -0.35766983 0.86776733 -1.24697971 -0.35766983 1.56366301 -0.44504213 -0.35766983 1.94985533
		 0.44504166 -0.35766983 1.94985628 1.24697924 -0.35766983 1.56366301 1.80193758 -0.35766983 0.86776733
		 1.99999976 -0.35766983 0 1.6043272 0.28067017 -0.77260256 1.11022878 0.28067017 -1.39218235
		 1.6043272 -0.35766983 -0.77260256 1.11022878 -0.35766983 -1.39218235 0.39623618 0.28067017 -1.73602343
		 0.39623618 -0.35766983 -1.73602343 -0.3962357 0.28067017 -1.73602343 -0.3962357 -0.35766983 -1.7360239
		 -1.11022842 0.28067017 -1.39218283 -1.11022842 -0.35766983 -1.39218283 -1.60432696 0.28067017 -0.77260351
		 -1.60432696 -0.35766983 -0.77260351 -1.78066862 0.28067017 -4.7683716e-007 -1.78066862 -0.35766983 -4.7683716e-007
		 -1.60432744 0.28067017 0.77260303 -1.60432744 -0.35766983 0.77260303 -1.11022902 0.28067017 1.39218235
		 -1.11022902 -0.35766983 1.39218235 -0.39623618 0.28067017 1.73602343 -0.39623618 -0.35766983 1.73602343
		 0.39623594 0.28067017 1.73602438 0.39623594 -0.35766983 1.73602438 1.11022854 0.28067017 1.39218235
		 1.11022854 -0.35766983 1.39218235 1.6043272 0.28067017 0.77260303 1.6043272 -0.35766983 0.77260303
		 1.7806685 0.28067017 4.7683716e-007 1.7806685 -0.35766983 4.7683716e-007 1.059872389 -0.31980515 -0.51040649
		 0.73345447 -0.31980515 -0.91972208 0.26176715 -0.31980515 -1.1468749 -0.26176679 -0.31980515 -1.1468749
		 -0.73345411 -0.31980515 -0.91972208 -1.05987215 -0.31980515 -0.51040745 -1.17636955 -0.31980515 0
		 -1.059872389 -0.31980515 0.51040697 -0.73345459 -0.31980896 0.91972303 -0.26176703 -0.31980515 1.14687634
		 0.26176667 -0.31980515 1.14687634 0.73345423 -0.31980515 0.91972303 1.059872389 -0.31980515 0.51040697
		 1.17636943 -0.31980515 0 1.060157061 0.28275299 -0.51054382 0.73365164 0.28275299 -0.91996956
		 0 0.28275299 0 0.26183748 0.28275299 -1.14718342 -0.26183712 0.28275299 -1.14718342
		 -0.73365116 0.28275299 -0.91996956 -1.060157299 0.28275299 -0.51054478 -1.17668581 0.28275299 0
		 -1.060157299 0.28275299 0.5105443 -0.733652 0.28275299 0.91997051 -0.26183748 0.28275299 1.1471839
		 0.26183724 0.28275299 1.1471839 0.7336514 0.28275299 0.91997051 1.060157061 0.28275299 0.5105443
		 1.17668581 0.28275299 0 0.32580662 -0.31980515 -0.15695238 0.22543144 -0.31980515 -0.28281879
		 0.080385089 -0.31980515 -0.35266924 -0.080604196 -0.31980515 -0.35266924 -0.22565043 -0.31980515 -0.28281879
		 -0.32602561 -0.31980515 -0.15695238 -0.36184907 -0.31980515 4.7683716e-007 -0.32602561 -0.31980515 0.15695333
		 -0.22565055 -0.31980896 0.28282022 -0.080604196 -0.31980515 0.35267067 0.080385089 -0.31980515 0.35267067
		 0.22543144 -0.31980515 0.28282022 0.32580662 -0.31980515 0.15695333 0.3616302 -0.31980515 4.7683716e-007
		 -0.00010967255 0.28275299 4.7683716e-007 0.32580447 -0.51156998 -0.15695238 0.22543359 -0.51156998 -0.28281879
		 -0.0001090765 -0.51156616 4.7683716e-007 0.08038938 -0.51156998 -0.35266924 -0.080606341 -0.51156998 -0.35266924
		 -0.22565138 -0.51156998 -0.28281879 -0.32602453 -0.51156998 -0.15695238 -0.36184907 -0.51156998 4.7683716e-007
		 -0.32602775 -0.51156998 0.15695143 -0.2256484 -0.51157379 0.28281879;
	setAttr ".vt[166:170]" -0.080599904 -0.51156998 0.35267115 0.080385089 -0.51156998 0.35267067
		 0.22543144 -0.51156998 0.28282022 0.32580662 -0.51156998 0.15695333 0.3616302 -0.51156998 4.7683716e-007;
	setAttr -s 353 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 14 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 28 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 42 0 0 14 1 1 15 1 2 16 1 3 17 1 4 18 1 5 19 1 6 20 1 7 21 1 8 22 1
		 9 23 1 10 24 1 11 25 1 12 26 1 13 27 1 14 70 1 15 71 1 16 72 1 17 73 1 18 74 1 19 75 1
		 20 76 1 21 77 1 22 78 1 23 79 1 24 80 1 25 81 1 26 82 1 27 83 1 28 42 1 29 43 1 30 44 1
		 31 45 1 32 46 1 33 47 1 34 48 1 35 49 1 36 50 1 37 51 1 38 52 1 39 53 1 40 54 1 41 55 1
		 56 28 1 57 29 1 58 30 1 59 31 1 60 32 1 61 33 1 62 34 1 63 35 1 64 36 1 65 37 1 66 38 1
		 67 39 1 68 40 1 69 41 1 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 56 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 83 0 83 70 0 56 84 1 57 85 1
		 84 85 0 70 86 1 86 84 1 71 87 1 86 87 0 87 85 1 58 88 1 85 88 0 72 89 1 87 89 0 89 88 1
		 59 90 1 88 90 0 73 91 1 89 91 0 91 90 1 60 92 1 90 92 0 74 93 1 91 93 0 93 92 1 61 94 1
		 92 94 0 75 95 1;
	setAttr ".ed[166:331]" 93 95 0 95 94 1 62 96 1 94 96 0 76 97 1 95 97 0 97 96 1
		 63 98 1 96 98 0 77 99 1 97 99 0 99 98 1 64 100 1 98 100 0 78 101 1 99 101 0 101 100 1
		 65 102 1 100 102 0 79 103 1 101 103 0 103 102 1 66 104 1 102 104 0 80 105 1 103 105 0
		 105 104 1 67 106 1 104 106 0 81 107 1 105 107 0 107 106 1 68 108 1 106 108 0 82 109 1
		 107 109 0 109 108 1 69 110 1 108 110 0 83 111 1 109 111 0 111 110 1 110 84 0 111 86 0
		 0 112 0 1 113 0 112 113 0 2 114 0 113 114 0 3 115 0 114 115 0 4 116 0 115 116 0 5 117 0
		 116 117 0 6 118 0 117 118 0 7 119 0 118 119 0 8 120 1 119 120 0 9 121 0 120 121 0
		 10 122 0 121 122 0 11 123 0 122 123 0 12 124 0 123 124 0 13 125 0 124 125 0 125 112 0
		 42 126 0 43 127 0 126 127 0 127 128 1 126 128 1 44 129 0 127 129 0 129 128 1 45 130 0
		 129 130 0 130 128 1 46 131 0 130 131 0 131 128 1 47 132 0 131 132 0 132 128 1 48 133 0
		 132 133 0 133 128 0 49 134 0 133 134 0 134 128 0 50 135 0 134 135 0 135 128 1 51 136 0
		 135 136 0 136 128 1 52 137 0 136 137 0 137 128 1 53 138 0 137 138 0 138 128 1 54 139 0
		 138 139 0 139 128 1 55 140 0 139 140 0 140 128 1 140 126 0 112 141 0 113 142 0 141 142 0
		 114 143 0 142 143 0 115 144 0 143 144 0 116 145 0 144 145 0 117 146 0 145 146 0 118 147 0
		 146 147 0 119 148 0 147 148 0 120 149 0 148 149 0 121 150 0 149 150 0 122 151 0 150 151 0
		 123 152 0 151 152 0 124 153 0 152 153 0 125 154 0 153 154 0 154 141 0 134 155 0 128 155 0
		 133 155 0 141 156 1 142 157 1 156 157 0 158 156 1 158 157 1 143 159 1 157 159 0 158 159 1
		 144 160 1 159 160 0 158 160 1 145 161 1 160 161 0 158 161 1 146 162 1 161 162 0 158 162 1
		 147 163 1 162 163 0 158 163 1 148 164 1;
	setAttr ".ed[332:352]" 163 164 0 158 164 1 149 165 1 164 165 0 158 165 1 150 166 1
		 165 166 0 158 166 1 151 167 1 166 167 0 158 167 1 152 168 1 167 168 0 158 168 1 153 169 1
		 168 169 0 158 169 1 154 170 1 169 170 0 158 170 1 170 156 0;
	setAttr -s 184 -ch 706 ".fc[0:183]" -type "polyFaces" 
		f 4 0 57 -15 -57
		mu 0 4 0 1 15 14
		f 4 1 58 -16 -58
		mu 0 4 1 2 16 15
		f 4 2 59 -17 -59
		mu 0 4 2 3 17 16
		f 4 3 60 -18 -60
		mu 0 4 3 4 18 17
		f 4 4 61 -19 -61
		mu 0 4 4 5 19 18
		f 4 5 62 -20 -62
		mu 0 4 5 6 20 19
		f 4 6 63 -21 -63
		mu 0 4 6 7 21 20
		f 4 7 64 -22 -64
		mu 0 4 7 8 22 21
		f 4 8 65 -23 -65
		mu 0 4 8 9 23 22
		f 4 9 66 -24 -66
		mu 0 4 9 10 24 23
		f 4 10 67 -25 -67
		mu 0 4 10 11 25 24
		f 4 11 68 -26 -68
		mu 0 4 11 12 26 25
		f 4 12 69 -27 -69
		mu 0 4 12 13 27 26
		f 4 13 56 -28 -70
		mu 0 4 13 0 14 27
		f 4 112 99 -29 -99
		mu 0 4 89 90 44 43
		f 4 113 100 -30 -100
		mu 0 4 90 91 45 44
		f 4 114 101 -31 -101
		mu 0 4 91 92 46 45
		f 4 115 102 -32 -102
		mu 0 4 92 93 47 46
		f 4 116 103 -33 -103
		mu 0 4 93 94 48 47
		f 4 117 104 -34 -104
		mu 0 4 94 95 49 48
		f 4 118 105 -35 -105
		mu 0 4 95 96 50 49
		f 4 119 106 -36 -106
		mu 0 4 96 97 51 50
		f 4 120 107 -37 -107
		mu 0 4 97 98 52 51
		f 4 121 108 -38 -108
		mu 0 4 98 99 53 52
		f 4 122 109 -39 -109
		mu 0 4 99 100 54 53
		f 4 123 110 -40 -110
		mu 0 4 100 101 55 54
		f 4 124 111 -41 -111
		mu 0 4 101 102 56 55
		f 4 125 98 -42 -112
		mu 0 4 102 88 57 56
		f 4 28 85 -43 -85
		mu 0 4 70 69 83 84
		f 4 29 86 -44 -86
		mu 0 4 69 68 82 83
		f 4 30 87 -45 -87
		mu 0 4 68 67 81 82
		f 4 31 88 -46 -88
		mu 0 4 67 66 80 81
		f 4 32 89 -47 -89
		mu 0 4 66 65 79 80
		f 4 33 90 -48 -90
		mu 0 4 65 64 78 79
		f 4 34 91 -49 -91
		mu 0 4 64 63 77 78
		f 4 35 92 -50 -92
		mu 0 4 63 62 76 77
		f 4 36 93 -51 -93
		mu 0 4 62 61 75 76
		f 4 37 94 -52 -94
		mu 0 4 61 60 74 75
		f 4 38 95 -53 -95
		mu 0 4 60 59 73 74
		f 4 39 96 -54 -96
		mu 0 4 59 58 72 73
		f 4 40 97 -55 -97
		mu 0 4 58 71 85 72
		f 4 41 84 -56 -98
		mu 0 4 71 70 84 85
		f 3 -314 -315 315
		mu 0 3 191 192 86
		f 3 -318 -316 318
		mu 0 3 193 191 86
		f 3 -321 -319 321
		mu 0 3 194 193 86
		f 3 -324 -322 324
		mu 0 3 195 194 86
		f 3 -327 -325 327
		mu 0 3 196 195 86
		f 3 -330 -328 330
		mu 0 3 197 196 86
		f 3 -333 -331 333
		mu 0 3 198 197 86
		f 3 -336 -334 336
		mu 0 3 199 198 86
		f 3 -339 -337 339
		mu 0 3 200 199 86
		f 3 -342 -340 342
		mu 0 3 201 200 86
		f 3 -345 -343 345
		mu 0 3 202 201 86
		f 3 -348 -346 348
		mu 0 3 203 202 86
		f 3 -351 -349 351
		mu 0 3 204 203 86
		f 3 -353 -352 314
		mu 0 3 192 204 86
		f 3 240 241 -243
		mu 0 3 162 163 87
		f 3 244 245 -242
		mu 0 3 163 164 87
		f 3 247 248 -246
		mu 0 3 164 165 87
		f 3 250 251 -249
		mu 0 3 165 166 87
		f 3 253 254 -252
		mu 0 3 166 167 87
		f 3 256 257 -255
		mu 0 3 167 168 87
		f 3 262 263 -261
		mu 0 3 169 170 87
		f 3 265 266 -264
		mu 0 3 170 171 87
		f 3 268 269 -267
		mu 0 3 171 172 87
		f 3 271 272 -270
		mu 0 3 172 173 87
		f 3 274 275 -273
		mu 0 3 173 174 87
		f 3 277 278 -276
		mu 0 3 174 175 87
		f 3 279 242 -279
		mu 0 3 175 162 87
		f 4 -143 -145 146 147
		mu 0 4 118 119 120 121
		f 4 -150 -148 151 152
		mu 0 4 122 118 121 123
		f 4 -155 -153 156 157
		mu 0 4 124 122 123 125
		f 4 -160 -158 161 162
		mu 0 4 126 124 125 127
		f 4 -165 -163 166 167
		mu 0 4 128 126 127 129
		f 4 -170 -168 171 172
		mu 0 4 130 128 129 131
		f 4 -175 -173 176 177
		mu 0 4 132 130 131 133
		f 4 -180 -178 181 182
		mu 0 4 134 132 133 135
		f 4 -185 -183 186 187
		mu 0 4 136 134 135 137
		f 4 -190 -188 191 192
		mu 0 4 138 136 137 139
		f 4 -195 -193 196 197
		mu 0 4 140 138 139 141
		f 4 -200 -198 201 202
		mu 0 4 142 140 141 143
		f 4 -205 -203 206 207
		mu 0 4 144 142 143 145
		f 4 -209 -208 209 144
		mu 0 4 146 144 145 147
		f 4 14 71 -127 -71
		mu 0 4 28 29 105 104
		f 4 15 72 -128 -72
		mu 0 4 29 30 106 105
		f 4 16 73 -129 -73
		mu 0 4 30 31 107 106
		f 4 17 74 -130 -74
		mu 0 4 31 32 108 107
		f 4 18 75 -131 -75
		mu 0 4 32 33 109 108
		f 4 19 76 -132 -76
		mu 0 4 33 34 110 109
		f 4 20 77 -133 -77
		mu 0 4 34 35 111 110
		f 4 21 78 -134 -78
		mu 0 4 35 36 112 111
		f 4 22 79 -135 -79
		mu 0 4 36 37 113 112
		f 4 23 80 -136 -80
		mu 0 4 37 38 114 113
		f 4 24 81 -137 -81
		mu 0 4 38 39 115 114
		f 4 25 82 -138 -82
		mu 0 4 39 40 116 115
		f 4 26 83 -139 -83
		mu 0 4 40 41 117 116
		f 4 27 70 -140 -84
		mu 0 4 41 42 103 117
		f 4 -113 140 142 -142
		mu 0 4 90 89 119 118
		f 4 126 145 -147 -144
		mu 0 4 104 105 121 120
		f 4 -114 141 149 -149
		mu 0 4 91 90 118 122
		f 4 127 150 -152 -146
		mu 0 4 105 106 123 121
		f 4 -115 148 154 -154
		mu 0 4 92 91 122 124
		f 4 128 155 -157 -151
		mu 0 4 106 107 125 123
		f 4 -116 153 159 -159
		mu 0 4 93 92 124 126
		f 4 129 160 -162 -156
		mu 0 4 107 108 127 125
		f 4 -117 158 164 -164
		mu 0 4 94 93 126 128
		f 4 130 165 -167 -161
		mu 0 4 108 109 129 127
		f 4 -118 163 169 -169
		mu 0 4 95 94 128 130
		f 4 131 170 -172 -166
		mu 0 4 109 110 131 129
		f 4 -119 168 174 -174
		mu 0 4 96 95 130 132
		f 4 132 175 -177 -171
		mu 0 4 110 111 133 131
		f 4 -120 173 179 -179
		mu 0 4 97 96 132 134
		f 4 133 180 -182 -176
		mu 0 4 111 112 135 133
		f 4 -121 178 184 -184
		mu 0 4 98 97 134 136
		f 4 134 185 -187 -181
		mu 0 4 112 113 137 135
		f 4 -122 183 189 -189
		mu 0 4 99 98 136 138
		f 4 135 190 -192 -186
		mu 0 4 113 114 139 137
		f 4 -123 188 194 -194
		mu 0 4 100 99 138 140
		f 4 136 195 -197 -191
		mu 0 4 114 115 141 139
		f 4 -124 193 199 -199
		mu 0 4 101 100 140 142
		f 4 137 200 -202 -196
		mu 0 4 115 116 143 141
		f 4 -125 198 204 -204
		mu 0 4 102 101 142 144
		f 4 138 205 -207 -201
		mu 0 4 116 117 145 143
		f 4 -126 203 208 -141
		mu 0 4 88 102 144 146
		f 4 139 143 -210 -206
		mu 0 4 117 103 147 145
		f 4 -1 210 212 -212
		mu 0 4 1 0 149 148
		f 4 -2 211 214 -214
		mu 0 4 2 1 148 150
		f 4 -3 213 216 -216
		mu 0 4 3 2 150 151
		f 4 -4 215 218 -218
		mu 0 4 4 3 151 152
		f 4 -5 217 220 -220
		mu 0 4 5 4 152 153
		f 4 -6 219 222 -222
		mu 0 4 6 5 153 154
		f 4 -7 221 224 -224
		mu 0 4 7 6 154 155
		f 4 -8 223 226 -226
		mu 0 4 8 7 155 156
		f 4 -9 225 228 -228
		mu 0 4 9 8 156 157
		f 4 -10 227 230 -230
		mu 0 4 10 9 157 158
		f 4 -11 229 232 -232
		mu 0 4 11 10 158 159
		f 4 -12 231 234 -234
		mu 0 4 12 11 159 160
		f 4 -13 233 236 -236
		mu 0 4 13 12 160 161
		f 4 -14 235 237 -211
		mu 0 4 0 13 161 149
		f 4 42 239 -241 -239
		mu 0 4 84 83 163 162
		f 4 43 243 -245 -240
		mu 0 4 83 82 164 163
		f 4 44 246 -248 -244
		mu 0 4 82 81 165 164
		f 4 45 249 -251 -247
		mu 0 4 81 80 166 165
		f 4 46 252 -254 -250
		mu 0 4 80 79 167 166
		f 4 47 255 -257 -253
		mu 0 4 79 78 168 167
		f 4 48 258 -260 -256
		mu 0 4 78 77 169 168
		f 4 49 261 -263 -259
		mu 0 4 77 76 170 169
		f 4 50 264 -266 -262
		mu 0 4 76 75 171 170
		f 4 51 267 -269 -265
		mu 0 4 75 74 172 171
		f 4 52 270 -272 -268
		mu 0 4 74 73 173 172
		f 4 53 273 -275 -271
		mu 0 4 73 72 174 173
		f 4 54 276 -278 -274
		mu 0 4 72 85 175 174
		f 4 55 238 -280 -277
		mu 0 4 85 84 162 175
		f 4 -213 280 282 -282
		mu 0 4 148 149 177 176
		f 4 -215 281 284 -284
		mu 0 4 150 148 176 178
		f 4 -217 283 286 -286
		mu 0 4 151 150 178 179
		f 4 -219 285 288 -288
		mu 0 4 152 151 179 180
		f 4 -221 287 290 -290
		mu 0 4 153 152 180 181
		f 4 -223 289 292 -292
		mu 0 4 154 153 181 182
		f 4 -225 291 294 -294
		mu 0 4 155 154 182 183
		f 4 -227 293 296 -296
		mu 0 4 156 155 183 184
		f 4 -229 295 298 -298
		mu 0 4 157 156 184 185
		f 4 -231 297 300 -300
		mu 0 4 158 157 185 186
		f 4 -233 299 302 -302
		mu 0 4 159 158 186 187
		f 4 -235 301 304 -304
		mu 0 4 160 159 187 188
		f 4 -237 303 306 -306
		mu 0 4 161 160 188 189
		f 4 -238 305 307 -281
		mu 0 4 149 161 189 177
		f 3 260 309 -309
		mu 0 3 169 87 190
		f 3 -258 310 -310
		mu 0 3 87 168 190
		f 4 -283 311 313 -313
		mu 0 4 176 177 192 191
		f 4 -285 312 317 -317
		mu 0 4 178 176 191 193
		f 4 -287 316 320 -320
		mu 0 4 179 178 193 194
		f 4 -289 319 323 -323
		mu 0 4 180 179 194 195
		f 4 -291 322 326 -326
		mu 0 4 181 180 195 196
		f 4 -293 325 329 -329
		mu 0 4 182 181 196 197
		f 4 -295 328 332 -332
		mu 0 4 183 182 197 198
		f 4 -297 331 335 -335
		mu 0 4 184 183 198 199
		f 4 -299 334 338 -338
		mu 0 4 185 184 199 200
		f 4 -301 337 341 -341
		mu 0 4 186 185 200 201
		f 4 -303 340 344 -344
		mu 0 4 187 186 201 202
		f 4 -305 343 347 -347
		mu 0 4 188 187 202 203
		f 4 -307 346 350 -350
		mu 0 4 189 188 203 204
		f 4 -308 349 352 -312
		mu 0 4 177 189 204 192
		f 3 259 308 -311
		mu 0 3 205 206 207;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder6";
	rename -uid "9DAD4F5E-4366-2025-276B-39B27EA43A99";
	setAttr ".t" -type "double3" 267.24936405690306 26.816519759872797 -58.713588448944869 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 4.1134926085195804 4.1134926085195804 4.1134926085195804 ;
createNode mesh -n "pCylinderShape6" -p "pCylinder6";
	rename -uid "F59919CE-4F30-EF33-17DF-11823509D8A6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".pt[0:21]" -type "float3"  -4.1078252e-014 -18.731421 
		0 -4.1910919e-014 -18.731421 0 -4.1910919e-014 -18.731421 0 -4.1078252e-014 -18.731421 
		0 -3.6415315e-014 -18.731421 0 -4.1078252e-014 -18.731421 0 -4.1910919e-014 -18.731421 
		0 -4.1910919e-014 -18.731421 0 -4.1078252e-014 -18.731421 0 -4.0856207e-014 -18.731421 
		-2.1104995e-030 4.1078252e-014 18.731421 0 4.1910919e-014 18.731421 0 4.1910919e-014 
		18.731421 0 4.1078252e-014 18.731421 0 3.6415315e-014 18.731421 0 4.1078252e-014 
		18.731421 0 4.1910919e-014 18.731421 0 4.1910919e-014 18.731421 0 4.1078252e-014 
		18.731421 0 3.663736e-014 18.731421 -2.1104995e-030 -4.205071e-014 -18.731421 0 4.205071e-014 
		18.731421 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder7";
	rename -uid "504D908A-42E6-7330-F71E-F9AB52A8FB7F";
	setAttr ".t" -type "double3" 267.24936405690306 26.816519759872797 60.85029616851439 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 4.1134926085195804 4.1134926085195804 4.1134926085195804 ;
createNode mesh -n "pCylinderShape7" -p "pCylinder7";
	rename -uid "AEC254F3-4CD7-6D6D-5D69-299B1327BB66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625001
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809144
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.68843985 0.40000001 0.68843985 0.42500001
		 0.68843985 0.45000002 0.68843985 0.47500002 0.68843985 0.5 0.68843985 0.52499998
		 0.68843985 0.54999995 0.68843985 0.57499993 0.68843985 0.5999999 0.68843985 0.62499988
		 0.68843985 0.62640893 0.75190854 0.54828387 0.6951474 0.45171607 0.6951474 0.37359107
		 0.75190854 0.34375 0.84375 0.37359107 0.93559146 0.4517161 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".pt[0:21]" -type "float3"  -4.1078252e-014 -18.731421 
		0 -4.1910919e-014 -18.731421 0 -4.1910919e-014 -18.731421 0 -4.1078252e-014 -18.731421 
		0 -3.6415315e-014 -18.731421 0 -4.1078252e-014 -18.731421 0 -4.1910919e-014 -18.731421 
		0 -4.1910919e-014 -18.731421 0 -4.1078252e-014 -18.731421 0 -4.0856207e-014 -18.731421 
		-2.1104995e-030 4.1078252e-014 18.731421 0 4.1910919e-014 18.731421 0 4.1910919e-014 
		18.731421 0 4.1078252e-014 18.731421 0 3.6415315e-014 18.731421 0 4.1078252e-014 
		18.731421 0 4.1910919e-014 18.731421 0 4.1910919e-014 18.731421 0 4.1078252e-014 
		18.731421 0 3.663736e-014 18.731421 -2.1104995e-030 -4.205071e-014 -18.731421 0 4.205071e-014 
		18.731421 0;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-008
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-008 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface7";
	rename -uid "B21A981B-40ED-DAB3-C71C-DF97EAB86518";
	setAttr ".rp" -type "double3" 271.44394683837891 77.598871231079102 2.7546844482421875 ;
	setAttr ".sp" -type "double3" 271.44394683837891 77.598871231079102 2.7546844482421875 ;
createNode transform -n "transform41" -p "|polySurface7";
	rename -uid "83D1BC85-429B-86CF-40C4-899CEC0FEB04";
createNode mesh -n "polySurface7Shape" -p "transform41";
	rename -uid "D01A831C-47BD-BFAF-2011-E08CD03E7BCF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface16";
	rename -uid "9BA5587F-4385-E27A-B65A-EFA4B6EB09B4";
	setAttr ".t" -type "double3" -2.1802272291512281 0 0 ;
	setAttr ".rp" -type "double3" 271.44394683837891 77.598871231079102 1.8309173583984375 ;
	setAttr ".sp" -type "double3" 271.44394683837891 77.598871231079102 1.8309173583984375 ;
createNode transform -n "transform43" -p "|polySurface16";
	rename -uid "9068CDE7-4445-D783-B754-C1915B220133";
createNode mesh -n "polySurface16Shape" -p "transform43";
	rename -uid "113025E8-4F7D-989D-FB1F-D3BE1073BD3C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999985098838806 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 91 ".pt";
	setAttr ".pt[0]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[1]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[2]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[3]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[45]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[46]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[55]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[62]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[63]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[68]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[69]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[71]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[76]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[77]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[79]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[86]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[87]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[93]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[94]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[101]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[108]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[112]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[113]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[120]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[121]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[122]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[123]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[128]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[136]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[137]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[138]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[144]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[146]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[147]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[152]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[153]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[154]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[155]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[200]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[201]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[202]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[203]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[204]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[205]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[206]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[207]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[208]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[209]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[210]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[211]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[212]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[213]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[214]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[215]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[232]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[240]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[241]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[243]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[252]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[253]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[254]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[255]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[264]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[265]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[266]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[267]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[268]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[269]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[270]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[271]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".pt[272]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[273]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[274]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[275]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[276]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[277]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[278]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[279]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[280]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[281]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[282]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[283]" -type "float3" 9.5367432e-007 0 0 ;
	setAttr ".pt[290]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[291]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[299]" -type "float3" 1.9073486e-006 0 0 ;
	setAttr ".pt[304]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[305]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[312]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[313]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[314]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".pt[315]" -type "float3" -1.9073486e-006 0 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube20";
	rename -uid "CCD6EF3F-4F2F-8460-BBB5-C58DAAE6AAEA";
	setAttr ".t" -type "double3" 269.7827049465925 76.841466334399655 1.6375189065163624 ;
	setAttr ".s" -type "double3" 178.78907440684017 178.78907440684017 178.78907440684017 ;
createNode mesh -n "pCubeShape19" -p "pCube20";
	rename -uid "7A5288A7-417C-B36B-EE7A-EF8E3644F2F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.17746800184249878 0.15389610826969147 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.0035519397 0 -0.0083546424 
		0.0044777682 0 -0.0083546424 0.0047420212 -0.0079905652 -0.0083546424 0.0044777682 
		-0.0079905652 -0.0083546424 0.0047420212 -0.0079905652 0.00091970293 0.0044777682 
		-0.0079905652 0.00091970293 0.0035519397 0 0.0098513383 0.0044777682 0 0.0098513383 
		0 -0.0079905652 0 0.010278301 -0.0079905652 0 0.010278301 -0.0079905652 0 0 -0.0079905652 
		0 0 0.0039360877 0.0015126272 0.012619235 0.0039360877 0.0015126272 0.012619235 0.0039360877 
		-0.0028250467 0 0.0039360877 -0.0028250467;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface2";
	rename -uid "15370121-4F90-4513-F6C4-F6B3C7046CEE";
	setAttr ".rp" -type "double3" 269.05224225084044 76.758260726928711 2.1097946166992187 ;
	setAttr ".sp" -type "double3" 269.05224225084044 76.758260726928711 2.1097946166992187 ;
createNode transform -n "transform48" -p "|polySurface2";
	rename -uid "87FC060D-4C96-1B03-2A0C-6E8A40C29B93";
createNode mesh -n "polySurface2Shape" -p "transform48";
	rename -uid "5F532D5B-47ED-D67E-D8FA-FEB7A558E8FD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface4";
	rename -uid "E8C669A1-44F7-DB5C-DBC5-1087A7164560";
	setAttr ".rp" -type "double3" 269.05224609375 76.758260726928711 2.1097946166992187 ;
	setAttr ".sp" -type "double3" 269.05224609375 76.758260726928711 2.1097946166992187 ;
createNode transform -n "polySurface46" -p "|polySurface4";
	rename -uid "DAFB57C1-42CD-E8F3-899D-39808C8FA001";
createNode mesh -n "polySurfaceShape47" -p "polySurface46";
	rename -uid "8BB1DAF8-427E-0EFF-73E8-A6BCB0ADFBC4";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface47" -p "|polySurface4";
	rename -uid "BA4DB9D1-4307-4B8D-83E9-F6BDBEA4D588";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape48" -p "polySurface47";
	rename -uid "745D19ED-49D4-8982-B960-22B4B1ED20B6";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface48" -p "|polySurface4";
	rename -uid "E2AF0559-42F8-7071-5860-B0918D358524";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape49" -p "polySurface48";
	rename -uid "0043512D-43DA-841B-F468-34AC70044A99";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface49" -p "|polySurface4";
	rename -uid "B3EBAD1D-4B1B-DDAE-64FF-F7B70DC0C9FA";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape50" -p "polySurface49";
	rename -uid "B9FB43C9-4FBE-591F-3C53-BF882A68AB78";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface50" -p "|polySurface4";
	rename -uid "D1D3701B-4F96-0D18-0F22-DAA1C97EF7CF";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape51" -p "polySurface50";
	rename -uid "8E035BE8-41F1-C8AE-A916-10A59DA7827D";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface51" -p "|polySurface4";
	rename -uid "F29626CF-4402-0B02-81B1-8D80552A2F63";
createNode mesh -n "polySurfaceShape52" -p "polySurface51";
	rename -uid "09702619-42B0-DC66-65F8-0589705810AD";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface52" -p "|polySurface4";
	rename -uid "7A250431-4D27-A06F-08BC-A9AD911307D1";
createNode mesh -n "polySurfaceShape53" -p "polySurface52";
	rename -uid "595F4833-47A2-38BE-07A4-67A19C9F7AA7";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface53" -p "|polySurface4";
	rename -uid "298B7031-48BD-F699-BFCB-19A578BB4AB7";
createNode mesh -n "polySurfaceShape54" -p "polySurface53";
	rename -uid "5CF9F476-4999-1EF4-FFC5-708CC58D4357";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface54" -p "|polySurface4";
	rename -uid "06DC61A3-4EBA-5677-4750-C89CB522F3A1";
createNode mesh -n "polySurfaceShape55" -p "polySurface54";
	rename -uid "1BB36E7E-496A-81E2-8382-AD80D88EEBD9";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface55" -p "|polySurface4";
	rename -uid "4CA6E9B5-491A-3C8D-5A99-408322FB9598";
createNode mesh -n "polySurfaceShape56" -p "polySurface55";
	rename -uid "FDB926B4-41F4-C061-9E23-BBBE37087571";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface56" -p "|polySurface4";
	rename -uid "8B590FFF-4F72-43B9-E11B-5390D448C163";
createNode mesh -n "polySurfaceShape57" -p "polySurface56";
	rename -uid "E5053E74-4937-1F3F-E174-1FAE492AD4BC";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface57" -p "|polySurface4";
	rename -uid "8066BA9B-4DA0-3E09-F4B6-38A263E5D9DD";
createNode mesh -n "polySurfaceShape58" -p "polySurface57";
	rename -uid "119807F9-4343-787B-4E82-CAB87937EBC4";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface58" -p "|polySurface4";
	rename -uid "9FC09163-4C24-7EC2-38FE-67A62A50F074";
createNode mesh -n "polySurfaceShape59" -p "polySurface58";
	rename -uid "59DDAA6B-4CB3-8E28-E28E-D28388092DD6";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface59" -p "|polySurface4";
	rename -uid "5536165B-450E-4E55-4A96-42B067E57DED";
createNode mesh -n "polySurfaceShape60" -p "polySurface59";
	rename -uid "E157044B-4505-591D-7A9B-56AF309D004B";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface60" -p "|polySurface4";
	rename -uid "DEE3D9CA-40A4-FA2D-9FFD-798E9757AB6C";
createNode mesh -n "polySurfaceShape61" -p "polySurface60";
	rename -uid "69F5839D-4348-D176-1345-45ADCCFB06A7";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface61" -p "|polySurface4";
	rename -uid "CC2564CC-49AA-5187-0AC7-AF9F53DA694F";
createNode mesh -n "polySurfaceShape62" -p "polySurface61";
	rename -uid "FBA58DEA-414B-3375-8552-9CB280F9DAE3";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface62" -p "|polySurface4";
	rename -uid "1F1515B4-4253-610D-0124-DDA25606F278";
createNode mesh -n "polySurfaceShape63" -p "polySurface62";
	rename -uid "9BCD59F2-49BB-7BC1-9D91-E48866748B7A";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface63" -p "|polySurface4";
	rename -uid "65651880-45D9-0917-07F7-4AB603382F1E";
createNode mesh -n "polySurfaceShape64" -p "polySurface63";
	rename -uid "F5B304F0-4F45-AF11-E80B-AFB06229C30C";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface64" -p "|polySurface4";
	rename -uid "8C9A3FA6-42E8-8363-7D53-E28BBFA874E6";
createNode mesh -n "polySurfaceShape65" -p "polySurface64";
	rename -uid "0EC8F768-4E25-83D3-A499-F3B97E1E51A9";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface65" -p "|polySurface4";
	rename -uid "A1766422-4D14-A987-8615-2E813AF597BF";
createNode mesh -n "polySurfaceShape66" -p "polySurface65";
	rename -uid "1E2CE00C-418C-369C-6453-6D86B80CF773";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface66" -p "|polySurface4";
	rename -uid "430FB8E7-495F-A8C4-07A4-A09211BD0AD9";
createNode mesh -n "polySurfaceShape67" -p "polySurface66";
	rename -uid "449785A8-4A8C-A657-1AB0-E28ED5EE965F";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface67" -p "|polySurface4";
	rename -uid "741552B7-459C-1680-199F-8AB9404D19D8";
createNode mesh -n "polySurfaceShape68" -p "polySurface67";
	rename -uid "CE9D5F82-41AF-1B5D-D92E-0C86DD7A2A05";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface68" -p "|polySurface4";
	rename -uid "F6AB3903-433D-C1C7-6D64-478A3D94747B";
createNode mesh -n "polySurfaceShape69" -p "polySurface68";
	rename -uid "EAD2EC06-4329-41E8-801C-398491A779E7";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface69" -p "|polySurface4";
	rename -uid "B1744276-498C-204D-BFA2-2B9E8E204D6F";
createNode mesh -n "polySurfaceShape70" -p "polySurface69";
	rename -uid "9908BE0F-48F6-F419-DDB6-36A47048C34F";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface70" -p "|polySurface4";
	rename -uid "F889D08F-4D13-F37A-2FB5-0687F1A1385D";
createNode mesh -n "polySurfaceShape71" -p "polySurface70";
	rename -uid "F4E02CAD-4550-44FB-39EE-37A8116EF965";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface71" -p "|polySurface4";
	rename -uid "18CFA9D1-4D91-EE16-0381-13AE7AF85EE5";
createNode mesh -n "polySurfaceShape72" -p "polySurface71";
	rename -uid "B8ED44B4-4DEA-48BA-3A0F-B198D8DD7747";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface72" -p "|polySurface4";
	rename -uid "4E72FA15-4CF7-4679-E718-90B998A7CA38";
createNode mesh -n "polySurfaceShape73" -p "polySurface72";
	rename -uid "6745C763-49CA-F556-542F-64AF65D0ABAB";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface73" -p "|polySurface4";
	rename -uid "644CC896-47A5-BFFB-655C-3EB3E60E4C67";
createNode mesh -n "polySurfaceShape74" -p "polySurface73";
	rename -uid "B5AB237F-40F1-729D-8CE9-78B4A3A7173C";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface74" -p "|polySurface4";
	rename -uid "82F31C38-4E92-E22E-8A53-23BC33601D24";
createNode mesh -n "polySurfaceShape75" -p "polySurface74";
	rename -uid "7333B883-48A8-CBB6-B74E-0E853C32F03D";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface75" -p "|polySurface4";
	rename -uid "4E056B50-4BEF-EE3E-6D7A-708FF2BACB86";
createNode mesh -n "polySurfaceShape76" -p "polySurface75";
	rename -uid "56BCE497-47A3-01F0-64C6-F7BDED7BFE8F";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface76" -p "|polySurface4";
	rename -uid "7229BD44-476D-3848-61CF-CFBB34EA0C08";
createNode mesh -n "polySurfaceShape77" -p "polySurface76";
	rename -uid "42E8B93C-44E2-863F-CC11-769BD4BDA381";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface77" -p "|polySurface4";
	rename -uid "776AC1D3-45D1-E043-1198-BDAD464278A8";
createNode mesh -n "polySurfaceShape78" -p "polySurface77";
	rename -uid "F258CCB1-4654-BBE9-9A95-8099F15A29E3";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface78" -p "|polySurface4";
	rename -uid "7675FEA7-46AB-DC8A-1CD8-3BB76A2F541D";
createNode mesh -n "polySurfaceShape79" -p "polySurface78";
	rename -uid "7AB382B6-4D14-70E9-0513-FA89414FF9AD";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface79" -p "|polySurface4";
	rename -uid "BD976967-4DD1-CD29-1AF3-E693CD1C6C2E";
createNode mesh -n "polySurfaceShape80" -p "polySurface79";
	rename -uid "9AF03B12-4902-A26D-27A3-2786B19263C9";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface80" -p "|polySurface4";
	rename -uid "C71248D9-4C82-0A38-2651-7286E6FA3E4E";
createNode mesh -n "polySurfaceShape81" -p "polySurface80";
	rename -uid "666A1FE4-4ACC-BB61-F316-D7A4118647F0";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface81" -p "|polySurface4";
	rename -uid "9F4C46EE-4ABA-9ADD-04AF-6C84A882FFF4";
createNode mesh -n "polySurfaceShape82" -p "polySurface81";
	rename -uid "CDF609DF-4983-D389-9DB9-0FB8F734F9C4";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface82" -p "|polySurface4";
	rename -uid "F431347A-495B-426A-E561-AC994AC25293";
createNode mesh -n "polySurfaceShape83" -p "polySurface82";
	rename -uid "781968BD-4106-2009-144A-328C15CAF2DE";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface83" -p "|polySurface4";
	rename -uid "F31D172F-4FB0-8726-015D-8DA448524F2D";
createNode mesh -n "polySurfaceShape84" -p "polySurface83";
	rename -uid "13BB0584-4BC0-D818-2C41-ECB89EE18621";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface84" -p "|polySurface4";
	rename -uid "79E3D191-4351-79E4-4A69-FA9DAB6AF1DD";
createNode mesh -n "polySurfaceShape85" -p "polySurface84";
	rename -uid "CFE68A59-4B9C-52A9-1979-88A086A479A8";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface85" -p "|polySurface4";
	rename -uid "E1E39E27-45B5-B393-22C4-12AF42BCA7D4";
createNode mesh -n "polySurfaceShape86" -p "polySurface85";
	rename -uid "9F573478-4396-0D57-1955-1CAE1960C1AF";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface86" -p "|polySurface4";
	rename -uid "1B8E07C3-421E-4223-A9C0-3A93AC50DD4D";
createNode mesh -n "polySurfaceShape87" -p "polySurface86";
	rename -uid "56AC1E1F-45AD-AC4F-3C07-C1BAE0365436";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface87" -p "|polySurface4";
	rename -uid "8F42921D-4447-CCBA-8C4B-8A9966EDB1F3";
createNode mesh -n "polySurfaceShape88" -p "polySurface87";
	rename -uid "D739F16B-4285-76D9-4868-33955249CED2";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface88" -p "|polySurface4";
	rename -uid "948CAE24-4845-70DB-6F76-2385FEC1CB68";
createNode mesh -n "polySurfaceShape89" -p "polySurface88";
	rename -uid "4E43386F-4EFD-3AB3-AC23-8CA264291819";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface89" -p "|polySurface4";
	rename -uid "0DD0B487-46B3-8903-402A-EB9AF876F3AF";
createNode mesh -n "polySurfaceShape90" -p "polySurface89";
	rename -uid "BED04001-42D8-A3CD-27B3-A9814779BDFA";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface90" -p "|polySurface4";
	rename -uid "480FA44C-47C0-1E6A-594F-1685D10A2043";
createNode mesh -n "polySurfaceShape91" -p "polySurface90";
	rename -uid "14465CA8-453B-7FA4-B840-92831D2AE60A";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform50" -p "|polySurface4";
	rename -uid "EAEDA9B1-4377-8C61-CB3A-0B9AB5E5BA6B";
	setAttr ".v" no;
createNode mesh -n "polySurface4Shape" -p "transform50";
	rename -uid "A8099493-4DDC-A78D-5D9D-2FA09CCA5658";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface91" -p "|polySurface4";
	rename -uid "CD5CAB26-42C4-2F0C-B678-2DB6C7D0029B";
	setAttr ".t" -type "double3" 0.057720481752397745 0 0 ;
createNode mesh -n "polySurfaceShape91" -p "polySurface91";
	rename -uid "CC69702A-41AF-8E51-A4AB-E38A613920FB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[18:19]" "e[23]" "e[26:27]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 33 ".uvst[0].uvsp[0:32]" -type "float2" 0.47918677 0.25 0.625
		 0.25 0.625 0 0.47918677 0 0.125 0 0.125 0.25 0.25509599 0.25 0.25509599 0 0.47918677
		 0.36990398 0.625 0.36990398 0.74490404 0.25 0.74490404 0 0.47918677 1 0.625 1 0.625
		 0.88009602 0.47918677 0.88009602 0.375 0.25 0.375 0 0.375 0.88009602 0.375 1 0.375
		 0.75 0.47918677 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0 0 0.58325303
		 0 0.58325303 1 0 3.877163e-009 7.4505806e-009 1 0.52038401 1 0.52038401 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  201.75386047 37.46752548 -86.6641922 212.8170166 38.46857071 -86.70343781
		 184.22665405 114.97813416 -103.71633911 195.35649109 115.87929535 -104.90055084 184.16072083 115.45066833 -90.37333679
		 200.72535706 38.26536942 -73.097053528 183.80278015 115.20489502 -99.87233734 195.18115234 116.049003601 -100.79844666
		 212.3931427 38.69534683 -82.85944366 201.32997131 37.69430542 -82.8201828 204.61398315 37.71042252 -86.90493774
		 204.46861267 37.94139862 -82.78336334 204.44873047 38.60523987 -73.71826172 187.020782471 115.69348145 -90.6140976
		 186.66287231 115.44769287 -100.11306 187.086761475 115.22098541 -103.95711517;
	setAttr -s 28 ".ed[0:27]"  15 10 1 10 1 0 1 3 0 15 3 0 5 9 0 9 6 1 6 4 0
		 4 5 0 14 15 1 3 7 0 14 7 0 8 1 0 7 8 0 10 11 1 8 11 0 9 0 0 0 2 0 2 6 0 11 9 1 0 10 0
		 5 12 0 11 12 0 4 13 0 12 13 0 6 14 1 13 14 0 2 15 0 11 14 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 3 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 -8 -7 -6 -5
		mu 0 4 4 5 6 7
		f 4 10 -10 -4 -9
		mu 0 4 8 9 1 0
		f 4 2 9 12 11
		mu 0 4 2 1 10 11
		f 4 1 -12 14 -14
		mu 0 4 12 13 14 15
		f 4 -18 -17 -16 5
		mu 0 4 6 16 17 7
		f 4 15 19 13 18
		mu 0 4 18 19 12 15
		f 4 4 -19 21 -21
		mu 0 4 20 18 15 21
		f 4 7 20 23 -23
		mu 0 4 22 20 21 23
		f 4 6 22 25 -25
		mu 0 4 24 22 23 8
		f 4 17 24 8 -27
		mu 0 4 16 24 8 0
		f 4 16 26 0 -20
		mu 0 4 17 16 0 3
		f 4 -13 -11 -28 -15
		mu 0 4 25 26 27 28
		f 4 27 -26 -24 -22
		mu 0 4 29 30 31 32;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface92" -p "|polySurface4";
	rename -uid "D0BAC180-455C-0941-6061-AD9A99DDFA5B";
	setAttr ".t" -type "double3" 0.057720481752397745 0 0 ;
createNode mesh -n "polySurfaceShape92" -p "polySurface92";
	rename -uid "9D4DE4CB-4C3A-A69A-C31D-B5879FB6844C";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[18:19]" "e[23]" "e[26:27]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 33 ".uvst[0].uvsp[0:32]" -type "float2" 0.47918677 0.25 0.47918677
		 0 0.625 0 0.625 0.25 0.125 0 0.25509599 0 0.25509599 0.25 0.125 0.25 0.47918677 0.36990398
		 0.625 0.36990398 0.74490404 0 0.74490404 0.25 0.47918677 1 0.47918677 0.88009602
		 0.625 0.88009602 0.625 1 0.375 0 0.375 0.25 0.375 0.88009602 0.375 1 0.375 0.75 0.47918677
		 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0.58325303 1 0.58325303 0 0 0
		 0 3.877163e-009 0.52038401 0 0.52038401 1 7.4505806e-009 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  336.49169922 37.46752167 -87.48130035 325.42849731 38.46856689 -87.52053833
		 354.018920898 114.97808838 -104.53344727 342.88912964 115.87929535 -105.71762085
		 354.08493042 115.45066833 -91.19042969 337.52017212 38.26537323 -73.91415405 354.44277954 115.20488739 -100.68943024
		 343.06439209 116.049003601 -101.61555481 325.85247803 38.69534683 -83.6765213 336.91574097 37.69430542 -83.63725281
		 333.6315918 37.71042633 -87.7220459 333.77703857 37.94139862 -83.60048676 333.79678345 38.60523987 -74.53536987
		 351.22476196 115.69347382 -91.43119049 351.58276367 115.44770813 -100.93015289 351.15875244 115.22097778 -104.77422333;
	setAttr -s 28 ".ed[0:27]"  15 3 0 1 3 0 10 1 0 15 10 1 4 5 0 6 4 0 9 6 1
		 5 9 0 14 7 0 3 7 0 14 15 1 7 8 0 8 1 0 8 11 0 10 11 1 2 6 0 0 2 0 9 0 0 0 10 0 11 9 1
		 11 12 0 5 12 0 12 13 0 4 13 0 13 14 0 6 14 1 2 15 0 11 14 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 3 2 1 -1
		mu 0 4 0 1 2 3
		f 4 7 6 5 4
		mu 0 4 4 5 6 7
		f 4 10 0 9 -9
		mu 0 4 8 0 3 9
		f 4 -13 -12 -10 -2
		mu 0 4 2 10 11 3
		f 4 14 -14 12 -3
		mu 0 4 12 13 14 15
		f 4 -7 17 16 15
		mu 0 4 6 5 16 17
		f 4 -20 -15 -19 -18
		mu 0 4 18 13 12 19
		f 4 21 -21 19 -8
		mu 0 4 20 21 13 18
		f 4 23 -23 -22 -5
		mu 0 4 22 23 21 20
		f 4 25 -25 -24 -6
		mu 0 4 24 8 23 22
		f 4 26 -11 -26 -16
		mu 0 4 17 0 8 24
		f 4 18 -4 -27 -17
		mu 0 4 16 1 0 17
		f 4 13 27 8 11
		mu 0 4 25 26 27 28
		f 4 20 22 24 -28
		mu 0 4 29 30 31 32;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface93" -p "|polySurface4";
	rename -uid "01A81661-4C94-FEE5-6FB5-939B872512FC";
	setAttr ".t" -type "double3" 0.057720481752397745 0 0 ;
createNode mesh -n "polySurfaceShape93" -p "polySurface93";
	rename -uid "8FB42268-4E2F-E818-2A0C-65A3C092CD9F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[18:19]" "e[23]" "e[26:27]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 33 ".uvst[0].uvsp[0:32]" -type "float2" 0.47918677 0.25 0.625
		 0.25 0.625 0 0.47918677 0 0.125 0 0.125 0.25 0.25509599 0.25 0.25509599 0 0.47918677
		 0.36990398 0.625 0.36990398 0.74490404 0.25 0.74490404 0 0.47918677 1 0.625 1 0.625
		 0.88009602 0.47918677 0.88009602 0.375 0.25 0.375 0 0.375 0.88009602 0.375 1 0.375
		 0.75 0.47918677 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0 0 0.58325303
		 0 0.58325303 1 0 3.877163e-009 7.4505806e-009 1 0.52038401 1 0.52038401 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  337.19985962 37.46751785 91.70085144 326.13668823 38.46857071 91.74012756
		 354.72714233 114.97810364 108.75299072 343.59735107 115.87929535 109.93721008 354.79312134 115.45066833 95.41001892
		 338.22842407 38.26537323 78.13371277 355.15100098 115.20488739 104.90899658 343.77264404 116.049003601 105.83509827
		 326.56066895 38.69534683 87.89609528 337.62399292 37.69430542 87.85682678 334.33978271 37.71042633 91.94160461
		 334.48532104 37.94139862 87.82003784 334.5050354 38.60523987 78.75492096 351.9329834 115.69347382 95.65075684
		 352.29101563 115.44770813 105.14972687 351.86697388 115.22097778 108.99377441;
	setAttr -s 28 ".ed[0:27]"  15 10 1 10 1 0 1 3 0 15 3 0 5 9 0 9 6 1 6 4 0
		 4 5 0 14 15 1 3 7 0 14 7 0 8 1 0 7 8 0 10 11 1 8 11 0 9 0 0 0 2 0 2 6 0 11 9 1 0 10 0
		 5 12 0 11 12 0 4 13 0 12 13 0 6 14 1 13 14 0 2 15 0 11 14 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 3 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 -8 -7 -6 -5
		mu 0 4 4 5 6 7
		f 4 10 -10 -4 -9
		mu 0 4 8 9 1 0
		f 4 2 9 12 11
		mu 0 4 2 1 10 11
		f 4 1 -12 14 -14
		mu 0 4 12 13 14 15
		f 4 -18 -17 -16 5
		mu 0 4 6 16 17 7
		f 4 15 19 13 18
		mu 0 4 18 19 12 15
		f 4 4 -19 21 -21
		mu 0 4 20 18 15 21
		f 4 7 20 23 -23
		mu 0 4 22 20 21 23
		f 4 6 22 25 -25
		mu 0 4 24 22 23 8
		f 4 17 24 8 -27
		mu 0 4 16 24 8 0
		f 4 16 26 0 -20
		mu 0 4 17 16 0 3
		f 4 -13 -11 -28 -15
		mu 0 4 25 26 27 28
		f 4 27 -26 -24 -22
		mu 0 4 29 30 31 32;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "polySurface94" -p "|polySurface4";
	rename -uid "69D59732-4D49-02CB-F3DB-4CB2633E0610";
	setAttr ".t" -type "double3" 0.057720481752397745 0 0 ;
createNode mesh -n "polySurfaceShape94" -p "polySurface94";
	rename -uid "4EC46BD1-4AB7-BBC5-3694-44BF5F261676";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[18:19]" "e[23]" "e[26:27]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 33 ".uvst[0].uvsp[0:32]" -type "float2" 0.47918677 0.25 0.47918677
		 0 0.625 0 0.625 0.25 0.125 0 0.25509599 0 0.25509599 0.25 0.125 0.25 0.47918677 0.36990398
		 0.625 0.36990398 0.74490404 0 0.74490404 0.25 0.47918677 1 0.47918677 0.88009602
		 0.625 0.88009602 0.625 1 0.375 0 0.375 0.25 0.375 0.88009602 0.375 1 0.375 0.75 0.47918677
		 0.75 0.375 0.5 0.47918677 0.5 0.375 0.36990398 0 1 0.58325303 1 0.58325303 0 0 0
		 0 3.877163e-009 0.52038401 0 0.52038401 1 7.4505806e-009 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  200.90458679 37.46752167 90.88374329 211.96774292 38.46857452 90.92301178
		 183.37736511 114.97813416 107.93590546 194.50718689 115.87929535 109.12010193 183.31141663 115.45066833 94.59289551
		 199.87605286 38.26537323 77.31658936 182.95349121 115.20489502 104.091888428 194.3318634 116.048995972 105.018005371
		 211.54385376 38.69534683 87.078979492 200.48068237 37.69430161 87.039733887 203.76470947 37.71042252 91.12449646
		 203.61932373 37.94140244 87.0029373169 203.59944153 38.60523987 77.93782043 186.17147827 115.69348145 94.83363342
		 185.81359863 115.44769287 104.33262634 186.23747253 115.22097778 108.17667389;
	setAttr -s 28 ".ed[0:27]"  0 10 0 2 15 0 4 13 0 5 12 0 0 2 0 1 3 0 2 6 0
		 3 7 0 4 5 0 5 9 0 6 4 0 8 1 0 9 0 0 6 14 1 7 8 0 8 11 0 9 6 1 10 1 0 11 9 1 14 7 0
		 15 3 0 10 11 1 11 12 0 12 13 0 13 14 0 14 15 1 15 10 1 11 14 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 26 17 5 -21
		mu 0 4 0 1 2 3
		f 4 9 16 10 8
		mu 0 4 4 5 6 7
		f 4 25 20 7 -20
		mu 0 4 8 0 3 9
		f 4 -12 -15 -8 -6
		mu 0 4 2 10 11 3
		f 4 21 -16 11 -18
		mu 0 4 12 13 14 15
		f 4 -17 12 4 6
		mu 0 4 6 5 16 17
		f 4 -19 -22 -1 -13
		mu 0 4 18 13 12 19
		f 4 3 -23 18 -10
		mu 0 4 20 21 13 18
		f 4 2 -24 -4 -9
		mu 0 4 22 23 21 20
		f 4 13 -25 -3 -11
		mu 0 4 24 8 23 22
		f 4 1 -26 -14 -7
		mu 0 4 17 0 8 24
		f 4 0 -27 -2 -5
		mu 0 4 16 1 0 17
		f 4 15 27 19 14
		mu 0 4 25 26 27 28
		f 4 22 23 24 -28
		mu 0 4 29 30 31 32;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "8278C7A6-4BFA-6F4D-ACF8-D9A753A6BB14";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "9F70149F-4EED-7700-12E6-2B86495912F6";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "B1260349-4AB2-8E6E-622F-218F12E1B3E0";
createNode displayLayerManager -n "layerManager";
	rename -uid "BDF2ABAC-4DF2-6917-D795-EAA8D824B0E4";
createNode displayLayer -n "defaultLayer";
	rename -uid "B19C4F74-4A50-6709-0AF7-F78EB7452D06";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "02D82D24-4D66-C64F-E233-6DA6E7723068";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "025DFE67-489C-86E5-B843-6D9018CDA2A5";
	setAttr ".g" yes;
createNode reference -n "sagaro_skinWeightingRN";
	rename -uid "53E56C3E-42F9-68A3-0842-20A263D1ED31";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sagaro_skinWeightingRN"
		"sagaro_skinWeightingRN" 0
		"sagaro_skinWeightingRN" 11
		2 "|sagaro_skinWeighting:back" "visibility" " 1"
		2 "|sagaro_skinWeighting:Sagaro" "visibility" " 1"
		2 "|sagaro_skinWeighting:Controls" "visibility" " 1"
		2 "|sagaro_skinWeighting:rig_grp" "visibility" " 1"
		2 "|sagaro_skinWeighting:jnt_origin" "visibility" " 1"
		2 "|sagaro_skinWeighting:jnt_origin|sagaro_skinWeighting:jnt_body|sagaro_skinWeighting:jnt_pot|sagaro_skinWeighting:spine01|sagaro_skinWeighting:effector1" 
		"visibility" " 1"
		2 "|sagaro_skinWeighting:Left_Arm_Low" "visibility" " 1"
		2 "sagaro_skinWeighting:___MESH___" "visibility" " 1"
		2 "sagaro_skinWeighting:___CTRLS___" "visibility" " 0"
		2 "sagaro_skinWeighting:___JNTS___" "visibility" " 0"
		2 "sagaro_skinWeighting:Spikes" "visibility" " 1";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode objectSet -n "set1";
	rename -uid "90738DB4-4EF2-F424-72A2-058C4E64898E";
	setAttr ".ihi" 0;
	setAttr -s 97 ".dsm";
	setAttr -s 97 ".gn";
createNode polyCube -n "polyCube4";
	rename -uid "E1084351-4C4F-6F80-C70B-D6A13E36C6BB";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit3";
	rename -uid "14975D7A-4F31-3C4B-A7AB-FBA9CE0A4B0F";
	setAttr -s 5 ".e[0:4]"  0.22993299 0.22993299 0.22993299 0.22993299
		 0.22993299;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "7F8D9232-485F-55E4-B8A0-8A9A312C601B";
	setAttr -s 7 ".e[0:6]"  0.33662099 0.66337901 0.33662099 0.66337901
		 0.33662099 0.66337901 0.33662099;
	setAttr -s 7 ".d[0:6]"  -2147483644 -2147483629 -2147483643 -2147483639 -2147483631 -2147483640 
		-2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "5FFB0264-42A1-B28D-843E-9EA55EFFF3EF";
	setAttr -s 7 ".e[0:6]"  0.36434901 0.36434901 0.36434901 0.36434901
		 0.63565099 0.36434901 0.36434901;
	setAttr -s 7 ".d[0:6]"  -2147483636 -2147483621 -2147483633 -2147483634 -2147483619 -2147483635 
		-2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "34A44425-43E0-FC0C-A2D8-8FA64176052A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 928\n                -height 716\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 928\n            -height 716\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n"
		+ "                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n"
		+ "            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n"
		+ "                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n"
		+ "            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n"
		+ "                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n"
		+ "                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 928\\n    -height 716\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 928\\n    -height 716\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "872E5468-43F7-2521-53A9-BA8462ACBC16";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode groupId -n "groupId1";
	rename -uid "1137F28A-4911-25EC-B520-9195AE3BD4D8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "03855608-4158-343C-1CA2-9D9D2AD5D30A";
	setAttr ".ihi" 0;
createNode objectSet -n "set2";
	rename -uid "8EC16F47-4928-F9A1-DA4E-F5BE5F2E40CC";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr -s 4 ".gn";
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "8F73BE95-4F75-D5CD-BDA3-9F99A10C52AB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 82 "e[0:55]" "e[112:139]" "e[142]" "e[146]" "e[149]" "e[151]" "e[154]" "e[156]" "e[159]" "e[161]" "e[164]" "e[166]" "e[169]" "e[171]" "e[174]" "e[176]" "e[179]" "e[181]" "e[184]" "e[186]" "e[189]" "e[191]" "e[194]" "e[196]" "e[199]" "e[201]" "e[204]" "e[206]" "e[208:209]" "e[212]" "e[214]" "e[216]" "e[218]" "e[220]" "e[222]" "e[224]" "e[226]" "e[228]" "e[230]" "e[232]" "e[234]" "e[236:237]" "e[240]" "e[244]" "e[247]" "e[250]" "e[253]" "e[256]" "e[262]" "e[265]" "e[268]" "e[271]" "e[274]" "e[277]" "e[279]" "e[282]" "e[284]" "e[286]" "e[288]" "e[290]" "e[292]" "e[294]" "e[296]" "e[298]" "e[300]" "e[302]" "e[304]" "e[306:307]" "e[313]" "e[317]" "e[320]" "e[323]" "e[326]" "e[329]" "e[332]" "e[335]" "e[338]" "e[341]" "e[344]" "e[347]" "e[350]" "e[352]";
	setAttr ".ix" -type "matrix" 3.2128008023615316e-015 14.469168496331024 0 0 -14.469168496331024 3.2128008023615316e-015 0 0
		 0 0 14.469168496331024 0 351.84754671497501 26.840860492558136 -58.777693576407188 1;
	setAttr ".a" 0;
createNode groupId -n "groupId3";
	rename -uid "60231552-488A-6052-1D04-C6B6324A55A0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "ED855E11-4581-8FB1-50C8-6094B91F9BBF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[259]" "e[308]" "e[310]";
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "E955E0E5-4B05-9698-459B-97AB533896CB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 81 "e[56:111]" "e[140:141]" "e[143:145]" "e[147:148]" "e[150]" "e[152:153]" "e[155]" "e[157:158]" "e[160]" "e[162:163]" "e[165]" "e[167:168]" "e[170]" "e[172:173]" "e[175]" "e[177:178]" "e[180]" "e[182:183]" "e[185]" "e[187:188]" "e[190]" "e[192:193]" "e[195]" "e[197:198]" "e[200]" "e[202:203]" "e[205]" "e[207]" "e[210:211]" "e[213]" "e[215]" "e[217]" "e[219]" "e[221]" "e[223]" "e[225]" "e[227]" "e[229]" "e[231]" "e[233]" "e[235]" "e[238:239]" "e[241:243]" "e[245:246]" "e[248:249]" "e[251:252]" "e[254:255]" "e[257:261]" "e[263:264]" "e[266:267]" "e[269:270]" "e[272:273]" "e[275:276]" "e[278]" "e[280:281]" "e[283]" "e[285]" "e[287]" "e[289]" "e[291]" "e[293]" "e[295]" "e[297]" "e[299]" "e[301]" "e[303]" "e[305]" "e[308:312]" "e[314:316]" "e[318:319]" "e[321:322]" "e[324:325]" "e[327:328]" "e[330:331]" "e[333:334]" "e[336:337]" "e[339:340]" "e[342:343]" "e[345:346]" "e[348:349]" "e[351]";
	setAttr ".ix" -type "matrix" 3.2128008023615316e-015 14.469168496331024 0 0 -14.469168496331024 3.2128008023615316e-015 0 0
		 0 0 14.469168496331024 0 351.84754671497501 26.840860492558136 -58.777693576407188 1;
	setAttr ".a" 180;
createNode groupId -n "groupId4";
	rename -uid "D0553C14-46AB-FB72-25D2-898D521D91B1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "17BF2AFE-4DBD-42B2-061B-DCA8383A3D90";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "82B7CB51-4B1D-B2CA-FC0D-8C8042E41973";
	setAttr ".ihi" 0;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "C88D6499-46EF-E7AD-B39F-99AA14E0B847";
	setAttr ".sa" 10;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySeparate -n "polySeparate1";
	rename -uid "21B94A9D-4266-BEF2-9EA0-999FD53C8A0D";
	setAttr ".ic" 45;
	setAttr -s 45 ".out";
createNode groupId -n "groupId7";
	rename -uid "79ACC6B7-4DA5-7DFF-D603-6AA980B01073";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "4F68EFBC-4B70-2194-B0C1-16904C4A1C12";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId8";
	rename -uid "C619CE71-4073-5883-052B-11A5A51E7EAF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "9C5DD59C-4FA3-3216-C66C-DC8612E1D36B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId9";
	rename -uid "C779B8B8-4A51-9062-9AAA-DBA519FBF60D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "FD1304A0-4EE5-DDEC-5686-11883DFA019B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId10";
	rename -uid "3EBCCBBF-460D-014D-4B55-C58D5A175308";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "4A2FF48A-4AF3-7C81-086F-C7AFDC88DA80";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId11";
	rename -uid "EE5B9C89-4BFD-7C82-F94E-6FA4B77BBE20";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "3512A0D7-4E05-6BAF-4C76-3C952AC217C6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId12";
	rename -uid "B181F522-45A1-D1CD-8061-05AA4907B4AE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "2369F590-45FB-72E0-C57E-BAAA505397AB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId13";
	rename -uid "A2A6C77A-4C0C-CAE5-DC0B-17BA272BFB4E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "C70D4DD8-46F6-F10A-28C8-AC8BCD0D9DF4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId14";
	rename -uid "8B59E382-4179-CAEA-D0DD-A393629FDF64";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "8343AA1A-4283-FD1F-A107-D0917E2020AA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId15";
	rename -uid "231D8A1D-41A1-F24F-C324-C9A957D85D8C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "C3376FB5-42A8-F752-3A78-34944FF46B52";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId16";
	rename -uid "98FF9A47-417C-05D9-C0E7-BD99658C6493";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "CB8071D6-4354-8909-DE36-CFA37C91821D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId17";
	rename -uid "82FC9D14-44CE-C0D5-B5AC-DA813D114DA5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "60E0D7FA-4154-D274-1E0D-DE824606E0B5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId18";
	rename -uid "0DBED7B1-4F7F-0057-E71B-F7B3EA334023";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "29C9DD8C-43C8-BE5B-FEC6-66B09D91D3A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId19";
	rename -uid "BB6F9251-4B9D-17AB-3AE7-6183B593C4DC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "25017B0D-49A7-A9D8-B0CC-5896AC12DF60";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId20";
	rename -uid "9E59EBAF-4E91-9A9D-2CA4-4B9999107DAC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "1AC7852E-453E-1543-25C6-B586FACFAF19";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId21";
	rename -uid "FACDB5A3-4144-7893-8C16-CD89427FBFBC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "A4B7257D-4F04-4E53-03A1-78AD0E9B25C2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId22";
	rename -uid "857246E1-4F97-5459-A2F3-C9BE6A70C4FB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "DF42842F-4B45-9650-E123-668B8A02187E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId23";
	rename -uid "386D541D-4F09-D0CA-C0C8-B89AC95D4CD1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "E4FD1B86-4CA1-F9A9-266B-169175EDECA0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId24";
	rename -uid "592F2B65-4017-223E-795A-E28418D7E775";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "394D46CF-4982-4784-21E1-E29F3FCDE8D2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId25";
	rename -uid "D2CCF7A4-4877-10A2-7A4B-EAADEA8E8D0A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "7F917D0B-4575-01E3-882D-81B48CBAAB53";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId26";
	rename -uid "BE50990C-4FAD-AA73-B17E-50AFA4E58821";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "ACA885A0-49ED-52BB-FA23-F28DE8003922";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId27";
	rename -uid "68A992F1-4405-F963-24A0-CCBF6003CFDF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts22";
	rename -uid "FDFE8333-47BA-E56F-D569-8088CFD945A1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId28";
	rename -uid "E9F92DF5-40EA-A848-B2F0-7690170BB391";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "91348CC5-4571-AD56-C1A3-93B22258523F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId29";
	rename -uid "3E5A833A-482E-5E82-D7E5-D99AD2ADFE49";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "211CDC76-4B0A-BC2B-95A6-A6A0840EE288";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId30";
	rename -uid "46A9FB28-401B-B92B-D9E9-12ACD16B444F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "C4B8932C-4440-B357-AA65-1BB5432EF384";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId31";
	rename -uid "A59C5C3E-4515-499E-E731-1EA85271FB6E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts26";
	rename -uid "A9410868-4DD6-5F93-991D-AA832DC0FCA3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId32";
	rename -uid "AABA221E-4959-737B-FB0E-C289EB0EA6E3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "F8E736CF-414A-2599-24A3-17A18340C2FA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId33";
	rename -uid "DDC8A99B-4B1B-4551-FF68-95A3E2C44CCF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts28";
	rename -uid "8DAF0590-4EEF-DC68-0AD1-80A699A800D2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId34";
	rename -uid "24217495-44BC-3E13-5891-D6ADA7879EA0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "7231EE2E-4062-FC7F-6F28-F99E9DA75D7C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId35";
	rename -uid "11FC8080-4F3C-7D5B-5F44-6586F78195A6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts30";
	rename -uid "F965423E-4FC6-71EB-0589-7CAECE1A918A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId36";
	rename -uid "E091E965-4AB1-A472-4D2B-A3AFA1B2E872";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "805B8559-47DF-F208-11B0-3B9FEB955B14";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId37";
	rename -uid "3B29074E-48FB-1BBF-0FA3-DAB41A8E6AB9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts32";
	rename -uid "0E434C31-442D-BA4B-DEDE-9E8C456244FC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId38";
	rename -uid "B6076D64-4269-A929-F3CA-2495252CB264";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "46D5F89C-4D09-0A91-F144-08ADB04B57D2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId39";
	rename -uid "C34EC4A9-4CE5-4CA4-BE8E-2393C0C93D35";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts34";
	rename -uid "98E21422-43AE-594A-C162-CABA1401B315";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId40";
	rename -uid "BE6A37F4-4219-5811-8D8D-63AF8C25E1F8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "5F314348-46E4-0018-3438-E3B8D9510F5D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId41";
	rename -uid "214E8272-4D3E-2DF8-CAF9-97A865433287";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts36";
	rename -uid "DFAA93D8-4F26-BF25-741C-CABE14580143";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId42";
	rename -uid "7777774B-450B-24AF-A367-F6B49B5DFA37";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "DF9ED9CA-47EF-8667-A7AD-C8B166554EF7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId43";
	rename -uid "B853CFC6-4170-4B99-14C9-F6A1E1776067";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts38";
	rename -uid "054B258F-492A-20A3-F8FF-41BC13CA1666";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId44";
	rename -uid "66043A44-4AC7-E089-E9E3-AF9DF8AB5C6C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	rename -uid "90E9ACA2-4F06-109A-4337-B3817A417D74";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId45";
	rename -uid "684AC192-4E27-0A1C-A229-B9B2D59BD366";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts40";
	rename -uid "31673645-4D13-C1E4-9D89-CF80AF8BE4BF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId46";
	rename -uid "C9B84EA5-4D69-91C9-137C-D49B4FACFFAA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts41";
	rename -uid "27360D8E-448C-3D3D-1BBB-68A1E2055E7A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId47";
	rename -uid "79660383-4A9F-66B9-C32C-1097758C04FE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts42";
	rename -uid "E41556BD-40A7-9CF5-7FF9-449CA9F5B33C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId48";
	rename -uid "91459984-42B4-1639-929C-FEBFAC3AAC50";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts43";
	rename -uid "4F281728-4B1C-18E8-310B-C88A857FA07B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId49";
	rename -uid "4D926475-4EA8-ACD0-8279-50AA7ED9566E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts44";
	rename -uid "0C3EC42D-476A-1422-BC2C-7FAE829A3828";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId50";
	rename -uid "12322001-4810-DCEC-63D8-39BAE272FFF8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts45";
	rename -uid "3932AF5B-4198-31B0-37A3-E6B61FAD6536";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId51";
	rename -uid "A5C0C7E4-4443-A910-29C6-90ADD14E4486";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts46";
	rename -uid "E41D5120-4F0D-37EC-C3CD-DBA97774D35E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId52";
	rename -uid "38B60C29-4DA9-45EC-7089-D78EC6EAE678";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts47";
	rename -uid "2ED568AC-43CA-1A85-5ED2-BC91A4AA9A41";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId53";
	rename -uid "C8752A76-403A-C6CC-0772-9EBB6BE33858";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts48";
	rename -uid "2E9894EB-47D6-0AE9-39E6-9F9936639BAE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId54";
	rename -uid "484CD758-484F-7206-E6F9-BC875927939D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts49";
	rename -uid "9E458824-47CA-0403-379E-1A924C147E62";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId55";
	rename -uid "9023E460-4E1D-6241-CD45-A7AE70C8BA22";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts50";
	rename -uid "82921B48-4950-16D7-99AC-2AB241A04FC1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId56";
	rename -uid "1FF7D38C-4EA1-2587-969D-A99E836B3D09";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts51";
	rename -uid "27241872-4C0B-458C-DB96-0C9E8DD064E6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId57";
	rename -uid "3D50D97D-49DE-F4A8-21CF-EBBD4BB71FFE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts52";
	rename -uid "87E19D1A-4799-4E55-6693-B0A25C4B73B8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId58";
	rename -uid "FD36EFD6-4B17-3842-ED61-2B94FDE6D22E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts53";
	rename -uid "0D93F1A6-44B5-EEEB-F5D0-C6A5059F1912";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId59";
	rename -uid "7526F987-4C06-8D94-3BBA-4B991E84CDA5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts54";
	rename -uid "6B92367A-4453-44AF-71C0-6F8219B1964E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId60";
	rename -uid "8E35BA15-496F-A7AB-5CE5-53A5EBA54F88";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts55";
	rename -uid "3FDE23FB-4D02-21F6-1ED8-4D9DD04A7492";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId61";
	rename -uid "9A0D3627-455B-354C-CB3F-96B7E7F994E4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts56";
	rename -uid "04BCC39A-4303-5864-5989-FD97A21A1094";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId62";
	rename -uid "DB117353-4B0A-4AA0-591F-04966A6287E5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts57";
	rename -uid "DE55D5CD-45B8-F88C-CD66-ECA4ED6C8504";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId63";
	rename -uid "E652A76F-429E-67F6-232B-88925FD773BF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts58";
	rename -uid "FB0C6BA3-443A-7C4B-5712-709557699A0D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId64";
	rename -uid "4A254E0F-4285-6751-2802-AE881D36B7C4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts59";
	rename -uid "253E3556-46BE-6396-82A0-A4A13ED2E4B5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId65";
	rename -uid "404749CE-4936-879D-C496-8C8FF266ACED";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts60";
	rename -uid "2032E0D2-4F3B-F66C-E84A-F99843A33A62";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId66";
	rename -uid "1DDEDF53-4378-E093-AC0E-419421F7F57B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts61";
	rename -uid "004EE564-49C1-616E-3E4D-F9BC4AB32058";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId67";
	rename -uid "03DA02BA-47D7-0ABE-7EA7-B5868422337A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts62";
	rename -uid "EFA1ED66-4B97-96E1-AEF3-298E789046A2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId68";
	rename -uid "C893EA55-4209-A911-60AE-9B865638DA35";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts63";
	rename -uid "990087CF-4A88-2E72-53BB-399CBD001EB8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId69";
	rename -uid "0872341E-453E-DA25-6197-91B7A258C3CC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts64";
	rename -uid "133CC933-4D6F-C71B-D76E-83B4C0F057DE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId70";
	rename -uid "34EBC1B4-49A1-FBB3-34E8-789BAF7497B7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts65";
	rename -uid "5BF422D6-4257-5315-F5C5-3BA89EBE7D68";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId71";
	rename -uid "BF8DBE5F-4350-EAF3-A041-D695815AE579";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts66";
	rename -uid "7CC32FB5-4172-8D48-537E-BF998E0BBFDB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId72";
	rename -uid "1C95E3DA-44F3-DE91-397C-D9A8ED07E9B9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts67";
	rename -uid "F0DE3404-4ADA-D16C-68FB-F5AA1C6187F5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId73";
	rename -uid "E0DDA388-44CA-0ACB-C237-09A43A16B5BB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts68";
	rename -uid "D48859C0-4CA6-54BD-C8AD-CF9504ECDAE9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId74";
	rename -uid "7421062A-4891-2DD2-CF3A-A3913CD070C8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts69";
	rename -uid "D223330A-4DBB-6A50-E2C4-CFBC4C2C508C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId75";
	rename -uid "042EEC00-4C5F-8851-5ADE-56BB6FF0F271";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts70";
	rename -uid "60A438ED-44BA-FBD1-BC9D-5285969355ED";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId76";
	rename -uid "F6480AEB-4154-BFC1-C296-B6863ADCB232";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts71";
	rename -uid "9167CDEB-4A99-F8F6-11CE-70A273D1618F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId77";
	rename -uid "379270FA-4937-0E9C-E6B5-79A24040C43D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts72";
	rename -uid "20DDBD04-4DC6-86AB-5054-09B970AA7CBE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId78";
	rename -uid "5EFEDF2B-470A-9E75-FAC7-6BA4E9D1FC6B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts73";
	rename -uid "6ADF9541-415F-3B96-537E-ACA73EE4EE48";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId79";
	rename -uid "694231E3-45C7-24F0-ED23-80A4F76C6D6E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts74";
	rename -uid "AE3309D1-4C11-E2C3-D29A-B6A73E2E9970";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId80";
	rename -uid "15119A58-4A47-A566-1C8A-2DBC4A36995A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts75";
	rename -uid "61A28E61-41BF-9242-66F4-039D2BF03DCB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId81";
	rename -uid "EBEA50B6-45D9-1979-D7E6-AC99F150E1E5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts76";
	rename -uid "A2A1363A-4FAE-CB69-9807-EBBA381673B1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId82";
	rename -uid "6F31C41A-4713-278B-DC3E-23B97D06F5F4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts77";
	rename -uid "FF4B3A88-4B2D-E186-89B9-CFA007A8BC3B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId83";
	rename -uid "B8CEE299-478B-6499-AFF5-58883C8F12AD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts78";
	rename -uid "62EA080B-4629-6519-E899-CAB05C9727C2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId84";
	rename -uid "4B1C43FB-4B25-0D04-555D-E0BBA3E97996";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts79";
	rename -uid "FFAEC932-41B2-0008-DC5E-5A947B0371F9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId85";
	rename -uid "861B4961-458D-E0E7-30D3-8DBFCCEB0BBC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts80";
	rename -uid "82356D16-4755-E208-7EEC-D398AF6DD83A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId86";
	rename -uid "A2A0875D-4CDA-E4AC-75AD-5681D30FA3DF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts81";
	rename -uid "898684CC-480E-443E-919A-D69B57AA450D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId87";
	rename -uid "4EEF2581-4AA1-BE5C-9E54-CEAFEF81D7AF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts82";
	rename -uid "01D0BCAC-401F-858F-5AB7-8CA995B10873";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId88";
	rename -uid "224A2811-4F06-2F09-9CD2-16ADA433350E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts83";
	rename -uid "83CA70D8-480C-0705-D32F-7A8B5CB3B726";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId89";
	rename -uid "CB1D552D-407D-4510-576C-8D8F7EF3A599";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts84";
	rename -uid "971E0199-4566-BEDA-E723-1FA062F9535E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId90";
	rename -uid "88D9C5D9-4412-C1AF-A7E9-22BE1105DEB2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts85";
	rename -uid "42C33EBC-4D42-4209-36D8-6DA6A66C9321";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId91";
	rename -uid "37C55142-47D2-1F1A-4E0C-918D057D50F8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts86";
	rename -uid "0F04E91C-4975-04E9-A8AE-D582FFA2B7D0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId92";
	rename -uid "F1264BBF-4424-3982-699B-7B8FCE359259";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts87";
	rename -uid "65E277B6-48FC-D303-05F7-05BEDD6A0062";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId93";
	rename -uid "6C882A69-4F48-0CC7-ECB8-45A5B10EC251";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts88";
	rename -uid "B74CA55D-4559-B72E-E54A-7781B8B8ED18";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId94";
	rename -uid "150E60ED-4C36-F744-9565-C7BD3D93CEB9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts89";
	rename -uid "F6B2BEFA-49FD-7AA4-366A-7F8F0177E748";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId95";
	rename -uid "52505E29-469D-3153-ACAA-62A73882DFC0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts90";
	rename -uid "6A9E7E87-4C7E-4AA5-4EE1-17B681AB77FC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId96";
	rename -uid "CCAAAC57-4ECE-B01A-8D48-FD92E2B5B997";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts91";
	rename -uid "AD964461-4F8E-2835-881F-65A9AE51C49A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "4119D95D-4EDA-44E1-3371-5BB9A2D46979";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 270.46721649169922 111.76412963867187 -100.45797729492187 ;
	setAttr ".ic" -type "double2" -0.65273702271633605 0.55354649539916745 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 163.20082092285156 7.09722900390625 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "AFDC268B-4320-1E80-36C3-7BB266AB92AF";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk[0:7]" -type "float2" 1.70749068 -0.74701124 1.70749068
		 -0.717233 1.70749068 -0.30923951 1.70749068 -0.11962751 -0.7068463 -0.30923951 -0.7068463
		 -0.11962751 -0.7068463 -0.717233 -0.7068463 -0.74701124;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "87C7C022-4236-F82A-76BB-18A4A3FCC016";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 270.46721649169922 111.76412963867187 -100.45797729492187 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 163.20082092285156 7.09722900390625 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyUnite -n "polyUnite1";
	rename -uid "4146699C-4A96-E68B-9DC5-A18753994075";
	setAttr -s 39 ".ip";
	setAttr -s 39 ".im";
createNode groupId -n "groupId97";
	rename -uid "54161940-436E-4AD8-A621-C7AED5A076E3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts92";
	rename -uid "20546FBD-4E91-5D5D-F955-AE852E8F2856";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:233]";
createNode polyUnite -n "polyUnite2";
	rename -uid "5109A54D-4652-F4D8-B55E-EFA2F8B97D72";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId98";
	rename -uid "CA520F1A-4E75-D92E-BFAA-6F957FC60D73";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts93";
	rename -uid "08956034-438E-8B55-B1C5-3BA849EDBB38";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:239]";
createNode polyCube -n "polyCube5";
	rename -uid "D350C136-4D3F-219B-A4FE-3BBB73F4729C";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "0721BF39-42D9-27D9-DE90-A69EB79A67A8";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 271.76563 116.99233 1.6375189 ;
	setAttr ".rs" 45719;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 186.34418658785916 116.99233234682698 -104.57702039525441 ;
	setAttr ".cbx" -type "double3" 357.18708038358017 116.99233234682698 107.85205820828713 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "BD5B70C7-4C1E-222A-0309-2DA48A4B58E6";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0.11777779 0.28458789 -0.0049355421
		 -0.11777779 0.28458789 -0.0049355421 0.022222223 -0.27542886 0.094077349 -0.022222223
		 -0.27542886 0.094077349 0.022222223 -0.27542886 -0.094077349 -0.022222223 -0.27542886
		 -0.094077349 0.11777779 0.28458789 0.0049355421 -0.11777779 0.28458789 0.0049355421;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "7593051D-4E93-A6B5-71BE-C3BA0A3A79CB";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 271.76563 116.99234 1.6375189 ;
	setAttr ".rs" 34047;
	setAttr ".lt" -type "double3" 0 7.6475706719444297e-016 -77.555840357154153 ;
	setAttr ".ls" -type "double3" 0.35653208990696023 0.80302696103368887 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 192.53037729394356 116.99234300348625 -101.52134057290826 ;
	setAttr ".cbx" -type "double3" 351.00086836417722 116.99234300348625 104.79637838594098 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "C9248A3C-4151-9D32-2474-779A20E591AC";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.034600556 -6.1062266e-016
		 -0.017090976 -0.034600556 -1.831868e-015 -0.017090976 -0.034600556 -1.831868e-015
		 0.017090976 0.034600556 -6.1062266e-016 0.017090976;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "F2217F9B-4336-D7D3-22B0-61ABC76E461A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:13]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 271.83888244628906 77.388313293457031 2.2466087341308594 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 170.98941040039062 213.20206451416016 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweak -n "polyTweak3";
	rename -uid "0910C385-4B0F-ACBE-9E93-9CA34B0869FC";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[0:15]" -type "float3"  -0.0016600678 -0.0030419203
		 -0.0037246433 0 -0.0015284643 -0.0037246433 0 0 0.0055684303 0.00081958837 0 0.0055684303
		 0 0 0.0012450393 0.00081958837 0 0.0012450393 -0.0016600678 -0.0014211969 0.0014813249
		 0 9.2259143e-005 0.0014813249 -0.0028264257 0 -0.012769691 -0.0011025314 0 -0.012769691
		 -0.0011025314 0 0.016180489 -0.0028264257 0 0.016180489 -0.20170707 0.0021586344
		 -0.0069152769 0.18888542 0.0021586344 -0.0069152769 0.18888542 0.0021586344 0.0090801474
		 -0.20170707 0.0021586344 0.0090801474;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "4A5844D0-40D9-ABEC-F9BF-1D8DDD1F5502";
	setAttr ".dc" -type "componentList" 1 "f[3]";
createNode polyUnite -n "polyUnite3";
	rename -uid "1954E919-4F2E-6E3E-F057-7DAC732C42A9";
	setAttr -s 5 ".ip";
	setAttr -s 5 ".im";
createNode groupId -n "groupId99";
	rename -uid "1F9F4C37-4612-0EDE-56FA-0FA578970BE5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts94";
	rename -uid "4F023FF3-463B-9BDB-F276-D59B32E55FD4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[14:15]" "e[19]" "e[22:24]" "e[42:43]" "e[47]" "e[50:52]" "e[70:71]" "e[75]" "e[78:80]" "e[98:99]" "e[103]" "e[106:108]";
createNode groupId -n "groupId100";
	rename -uid "20AE6459-4192-121B-38E3-468158CC9835";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts95";
	rename -uid "0640BB35-4391-3422-E474-70857D5188F6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:295]";
createNode polyUnite -n "polyUnite4";
	rename -uid "37B37BF6-4FE8-9801-D17A-28B043A908DA";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId101";
	rename -uid "D393E9C6-4DF4-83E7-9763-FFBA4FCADCB1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts96";
	rename -uid "28A642A5-4956-616C-1ECF-FAA6EC485C90";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId102";
	rename -uid "40C02682-48F4-B7BF-2974-BF87348E19D1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts97";
	rename -uid "DB65B87F-49AE-4BC2-8E16-438842A3D270";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "131ECAB4-4CAC-D200-BBC5-BFA8CC2F4753";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[3:4]" "f[10]" "f[12]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 270.301025390625 77.388315200805664 2.2466087341308594 ;
	setAttr ".ic" -type "double2" -0.69625629441695458 0.49810718935663978 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 213.20206451416016 79.208065032958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweak -n "polyTweak4";
	rename -uid "E880DC68-45C3-6EA1-CD15-DBBC75E76B8D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[1]" -type "float3" -0.017202975 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.017202975 0 0 ;
	setAttr ".tk[5]" -type "float3" -0.017202975 0 0 ;
	setAttr ".tk[7]" -type "float3" -0.017202975 0 0 ;
	setAttr ".tk[9]" -type "float3" -0.011554228 0 0 ;
	setAttr ".tk[10]" -type "float3" -0.011554228 0 0 ;
	setAttr ".tk[13]" -type "float3" -0.011554228 0 0 ;
	setAttr ".tk[14]" -type "float3" -0.011554228 0 0 ;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "26ED8939-410A-A696-852E-96BAE4609043";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[0]" "f[2]" "f[9]" "f[11]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 270.301025390625 77.388315200805664 2.2466087341308594 ;
	setAttr ".ic" -type "double2" 1.5826876588710412 0.46214378713279647 ;
	setAttr ".ps" -type "double2" 167.9136962890625 79.208065032958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "345FDADC-4517-D486-6E7F-D4873D5D8CE6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[1]" "f[5:8]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 270.301025390625 78.407398223876953 2.2466087341308594 ;
	setAttr ".ic" -type "double2" 1.8701180587089257 -0.60830466891488766 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 167.9136962890625 213.20206451416016 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "B68F28D9-42B6-2A32-80B6-F8B2A12DF285";
	setAttr ".uopa" yes;
	setAttr -s 24 ".uvtk";
	setAttr ".uvtk[16]" -type "float2" -0.9912529 1.2622697 ;
	setAttr ".uvtk[17]" -type "float2" -0.99125296 1.2622697 ;
	setAttr ".uvtk[18]" -type "float2" -0.99125296 1.2622699 ;
	setAttr ".uvtk[19]" -type "float2" -0.99125302 1.2622699 ;
	setAttr ".uvtk[20]" -type "float2" 0.029700466 1.251132 ;
	setAttr ".uvtk[21]" -type "float2" 0.02970051 1.251132 ;
	setAttr ".uvtk[22]" -type "float2" 0.02970051 1.251132 ;
	setAttr ".uvtk[23]" -type "float2" 0.029700466 1.251132 ;
	setAttr ".uvtk[28]" -type "float2" -1.1174798 0.014850184 ;
	setAttr ".uvtk[29]" -type "float2" -1.1174796 0.014850184 ;
	setAttr ".uvtk[30]" -type "float2" -1.1174796 0.014850229 ;
	setAttr ".uvtk[31]" -type "float2" -1.1174798 0.014850229 ;
	setAttr ".uvtk[32]" -type "float2" 1.1397555 2.9802322e-008 ;
	setAttr ".uvtk[33]" -type "float2" 1.1397555 2.9802322e-008 ;
	setAttr ".uvtk[34]" -type "float2" 1.1397552 0 ;
	setAttr ".uvtk[35]" -type "float2" 1.1397555 0 ;
	setAttr ".uvtk[36]" -type "float2" -0.055688351 1.3068204 ;
	setAttr ".uvtk[37]" -type "float2" -0.055688351 1.3068204 ;
	setAttr ".uvtk[38]" -type "float2" -0.055688351 1.3068204 ;
	setAttr ".uvtk[39]" -type "float2" -0.055688351 1.3068204 ;
	setAttr ".uvtk[40]" -type "float2" 1.1137674 1.2845452 ;
	setAttr ".uvtk[41]" -type "float2" 1.1137674 1.2845452 ;
	setAttr ".uvtk[42]" -type "float2" 1.1137676 1.2845452 ;
	setAttr ".uvtk[43]" -type "float2" 1.1137674 1.2845452 ;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "D9DF6769-46FF-3FDD-278E-C9B58F7DF07E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12]" "e[15]" "e[17]";
createNode polyMapCut -n "polyMapCut2";
	rename -uid "BDE09CC4-411F-BC85-2BD9-2487052AC16E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[13]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "EA3173B4-4365-43FB-CE9C-6B892EE9D033";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[2]" "f[4]" "f[9:10]";
	setAttr ".ix" -type "matrix" 178.78907440684017 0 0 0 0 178.78907440684017 0 0 0 0 178.78907440684017 0
		 271.76563348571966 76.841466334399655 1.6375189065163624 1;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "FBC43437-4F71-0963-D388-92AADB2336AF";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[21]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[22]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[23]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[24]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[25]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[26]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[27]" -type "float2" -3.1599855 0 ;
	setAttr ".uvtk[32]" -type "float2" 3.408463 0 ;
	setAttr ".uvtk[33]" -type "float2" 3.408463 0 ;
	setAttr ".uvtk[34]" -type "float2" 3.408463 0 ;
	setAttr ".uvtk[35]" -type "float2" 3.408463 0 ;
	setAttr ".uvtk[36]" -type "float2" 2.4330707 0 ;
	setAttr ".uvtk[37]" -type "float2" 2.4330707 0 ;
	setAttr ".uvtk[38]" -type "float2" 2.4330707 0 ;
	setAttr ".uvtk[39]" -type "float2" 2.4330704 0 ;
	setAttr ".uvtk[40]" -type "float2" -0.97539222 0 ;
	setAttr ".uvtk[41]" -type "float2" -0.97539234 0 ;
	setAttr ".uvtk[42]" -type "float2" -0.97539234 0 ;
	setAttr ".uvtk[43]" -type "float2" -0.97539222 0 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "32070676-4889-A891-C17A-8DBD8AFBF85C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[1:2]" "e[4]" "e[6:9]";
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "4C30CB19-4FA7-D00B-3A77-A3B865675FFF";
	setAttr ".uopa" yes;
	setAttr -s 18 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[1]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[6]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[7]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[8]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[9]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[10]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[11]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[12]" -type "float2" -1.8082861 2.1063113 ;
	setAttr ".uvtk[13]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[14]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[15]" -type "float2" -1.8082863 2.1063113 ;
	setAttr ".uvtk[16]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[17]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[34]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[35]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[36]" -type "float2" -1.8082862 2.1063113 ;
	setAttr ".uvtk[37]" -type "float2" -1.8082862 2.1063113 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "6B1E1669-4FA7-306F-F115-6AB6BA8982B5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12]" "e[15]" "e[17]";
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "0F61EA3A-432F-A63F-C496-309921376079";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[14]" "e[16]" "e[18:19]" "e[23]" "e[25]";
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "39E0A0C4-43C5-D513-3EA5-97A2A06116AF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20]";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "E7EF3C0B-4FCB-9C03-B649-49ADCD317350";
	setAttr ".uopa" yes;
	setAttr -s 24 ".uvtk[0:23]" -type "float2" 1.012084007 -1.31449699 0.22839689
		 -1.10861492 -1.058293343 1.14673901 -1.91415489 1.13991201 -1.9052099 0.2245138 -1.049348354
		 0.23134087 0.99110627 -2.3003099 3.62982345 -1.99021244 3.60876775 -2.027243376 1.06248188
		 -2.39336157 -0.003999263 -2.070122719 2.8498075 -2.22859097 2.8577857 -2.23005199
		 1.95128798 -2.39005017 2.038140297 -2.42335343 3.15508413 -1.19106102 2.51098657
		 -1.36576295 1.79512143 -1.43779945 3.26202273 -3.021766186 3.92423534 -2.84759331
		 1.20192027 -3.32505703 2.45720792 -3.2462039 0.027291805 -2.10752511 0.011862621
		 -3.11214924;
createNode polyStraightenUVBorder -n "polyStraightenUVBorder1";
	rename -uid "AEB96AEB-429F-FB65-E230-AC957BFB4F2A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "map[0:1]" "map[6:23]";
createNode polyPlanarProj -n "polyPlanarProj7";
	rename -uid "979C0DD3-41E9-FCEE-99EF-6284A5581792";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:183]";
	setAttr ".ix" -type "matrix" 3.2128008023615316e-015 14.469168496331024 0 0 -14.469168496331024 3.2128008023615316e-015 0 0
		 0 0 14.469168496331024 0 351.84754671497501 26.840860492558136 -58.777693576407188 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 351.93128967285156 26.840860366821289 -58.777690887451172 ;
	setAttr ".ic" -type "double2" -0.74894693075079477 0.55991258008764411 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 14.636566162109375 56.425590515136719 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "DCB653EF-4B6E-2F69-5F6A-89A744B183D3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:183]";
	setAttr ".ix" -type "matrix" 3.2128008023615316e-015 14.469168496331024 0 0 -14.469168496331024 3.2128008023615316e-015 0 0
		 0 0 14.469168496331024 0 351.84754671497501 26.840860492558136 -58.777693576407188 1;
	setAttr ".s" -type "double3" 57.876667085886908 57.876667085886908 57.876667085886908 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polySeparate -n "polySeparate2";
	rename -uid "A8C7F7CF-4E44-909C-9228-D29E60F40BBC";
	setAttr ".ic" 45;
	setAttr -s 45 ".out";
createNode groupId -n "groupId103";
	rename -uid "0F5D2B44-43D8-99A2-7794-B58264F5971F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts98";
	rename -uid "0F2625D3-44BE-AA13-E4EC-5EAC4BE75E27";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId104";
	rename -uid "7393A59E-48AD-5091-C10A-3486057C1792";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts99";
	rename -uid "722BD23D-414D-3BB5-F0BC-1D83B091BACA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId105";
	rename -uid "9EAC3EA0-46A2-D51E-A8B5-34877DEA5954";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts100";
	rename -uid "5B15E21B-4170-6C08-9EF5-E1B0D42CBBDB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId106";
	rename -uid "72397882-4953-1D30-CFEF-309694B6BC08";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts101";
	rename -uid "2848E8B1-40B9-3E1D-3942-F7B484A129A5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId107";
	rename -uid "83164490-419C-F356-12B3-A6805246A194";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts102";
	rename -uid "D4BE6C06-4773-E4BC-E313-F88D0992A8CD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId108";
	rename -uid "A3BB26EB-43AF-9D2E-8060-66A528186B83";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts103";
	rename -uid "E23DF059-4EB3-D02A-F227-37AE44BDA2E5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId109";
	rename -uid "CC024E48-447E-A813-2F22-9E8DD538D63E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts104";
	rename -uid "876014A7-4D5D-68AD-67E8-C88DAAD6F821";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId110";
	rename -uid "81EB2C16-4243-01FA-2F7D-2FAE008D5667";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts105";
	rename -uid "9C1FB895-45BB-9873-986F-B99F4737A1A1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId111";
	rename -uid "369B0590-4AE3-2C48-5344-F7B8BD227D81";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts106";
	rename -uid "07FD0C2D-470B-AC46-9013-BBB85AD66686";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId112";
	rename -uid "8488CC10-4B9D-9232-2DFC-86BCBB9C20C0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts107";
	rename -uid "3E1BEC90-43B0-3216-B0E0-66A3FCCB5EE9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId113";
	rename -uid "4625BA63-4DAB-E56E-1FDF-5A9F2AA82BDB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts108";
	rename -uid "E275D8F9-4B9C-D252-298A-D0BB71E0D919";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId114";
	rename -uid "D7BB7367-426C-8D64-4BE1-91B9CCF53D00";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts109";
	rename -uid "DD9713BB-49BE-D8F5-2EE5-1F99DCCDDCDA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId115";
	rename -uid "9CBD6DD8-4DD9-58E6-134A-DA9BBBD1C829";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts110";
	rename -uid "8FE5EE3B-4B4B-9569-BF45-E285BE8B3415";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId116";
	rename -uid "C49441C1-4475-A3E2-A793-39B120BA8F9E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts111";
	rename -uid "F6A30057-44DE-80A9-9339-F9A545EC05B6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId117";
	rename -uid "1B4A66B6-45AB-50DB-F25B-5794C97ED839";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts112";
	rename -uid "C43F4B64-4DD0-A9F0-E6C5-B78AF14EDB8B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId118";
	rename -uid "712ED697-4B0F-EA53-8860-838F695EF077";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts113";
	rename -uid "14911594-456E-099A-4B2E-20941AED143F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId119";
	rename -uid "025A5FFD-459B-77B9-F15A-89B0762DDA65";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts114";
	rename -uid "38987419-4BB6-D467-75ED-A6B447079D42";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId120";
	rename -uid "B5A8C536-46AA-2D4B-23A2-C39552BEC1B4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts115";
	rename -uid "73385557-4A6A-39B0-9364-1CA11AEAAD0D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId121";
	rename -uid "00B65E2A-4D59-38F7-C14E-8E844F1E1B63";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts116";
	rename -uid "FE5884B2-4147-D2DE-422D-E28C6F6F9491";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId122";
	rename -uid "0B2AC616-4B20-E984-1880-43A37E1F0851";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts117";
	rename -uid "41879958-4942-A03F-9498-78B7C27B31B8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId123";
	rename -uid "73234E05-4B1D-24E1-3362-93A3475CDCBC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts118";
	rename -uid "E15C4518-4323-D831-4A3F-78AED643565B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId124";
	rename -uid "090FB6DA-45D5-E09D-7556-A19745B9C226";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts119";
	rename -uid "25830B92-4DDA-217B-106B-F08C2587EE3A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId125";
	rename -uid "1799C388-4478-402F-70D9-45B2D71973E2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts120";
	rename -uid "0CE5A70B-4F29-7F17-4952-8FB6AEE16A11";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId126";
	rename -uid "48F6A9FD-4B2E-7842-A364-8F8FE2D88E1B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts121";
	rename -uid "8F7A4D0F-47DE-168E-B39B-BE8ECF976F42";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId127";
	rename -uid "0AD0F9D8-4C87-2250-75FC-4F87E91E324D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts122";
	rename -uid "F6690E2A-43CF-AEEC-39BE-0791E4A05146";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId128";
	rename -uid "1429A594-4BD3-77BD-8936-E590BAC712D5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts123";
	rename -uid "8EB8A1C7-4D45-768C-60B3-3892B5F474B0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId129";
	rename -uid "E0A835EE-484C-9FDE-953F-438ED7553AE7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts124";
	rename -uid "70FD853C-4E00-29B2-E719-04947A95A984";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId130";
	rename -uid "E6869CDD-45E6-8B66-0AED-B9911276A124";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts125";
	rename -uid "D1FEF7E5-4865-95D2-5781-F1BC4B53E74C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId131";
	rename -uid "50AE45AB-4D36-8FB6-CE89-57A64056DC18";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts126";
	rename -uid "8165DEE5-4FA0-33F9-80B6-2E8E0E4F51AE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId132";
	rename -uid "CE94518E-45A0-9F00-C621-AA92BF476BFC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts127";
	rename -uid "5014B34B-4462-1C4C-2F1D-7A837319D934";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId133";
	rename -uid "F79C6845-4290-47EF-2EE3-5B998793D29B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts128";
	rename -uid "080A5E3D-4050-F7C5-4A7E-75ADF8305BF6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId134";
	rename -uid "EA688873-4F1D-5980-70A5-FB8F1112ADA5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts129";
	rename -uid "E823BBCE-4F96-3979-D431-C1A71EE08571";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId135";
	rename -uid "76910320-469D-A8FF-B071-9AB5A56E7449";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts130";
	rename -uid "7FEF3004-42EB-EE5F-FABA-9486BB5ACAA6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId136";
	rename -uid "13E3138B-4C87-8AE1-D3AD-A181EBD34EAF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts131";
	rename -uid "481C5058-4BD2-CEEB-FFE3-4390F1E4E4BF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId137";
	rename -uid "B29B4B6A-4557-2F09-B823-8CBA07F41B5E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts132";
	rename -uid "57F5D420-4254-517F-1D46-2092C5DDB528";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId138";
	rename -uid "7C1E288D-48D5-D2C3-548D-E2AB64D31DDE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts133";
	rename -uid "2B052597-4972-0902-A3B9-A5898DF4CFA9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId139";
	rename -uid "B14CA011-493E-A94F-641C-FF8FA8E884E7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts134";
	rename -uid "5B187308-491D-A524-0E51-EF9B4875A05F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId140";
	rename -uid "2A29CCC8-44FC-4126-E277-CF875626F713";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts135";
	rename -uid "51C6C839-43D3-BB51-6621-30B8BABC3867";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId141";
	rename -uid "9A011747-4759-0882-92C9-2FAA079970E1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts136";
	rename -uid "BE52913C-4269-E9DC-D76C-669D9E3277FD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId142";
	rename -uid "F326BD28-443B-0009-1FC7-75AD1E97D658";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts137";
	rename -uid "2A9F7E90-4F0C-FDF8-6E90-C98F5611B430";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId143";
	rename -uid "5A521807-4054-3895-9ABD-F185E2042289";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts138";
	rename -uid "792DB0EA-4829-B87B-C37D-D482A419DBE5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId144";
	rename -uid "2A670410-4443-7877-860F-66AD4D318440";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts139";
	rename -uid "3C438F41-418C-3573-CBE6-C3AF5BE4DB4A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId145";
	rename -uid "C6B9767F-4E76-05E2-527A-E787F90B8D9D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts140";
	rename -uid "989627DE-41F9-9882-2E15-368FD979EF93";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId146";
	rename -uid "BBFE256D-4ADE-992A-5475-A99E664763AD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts141";
	rename -uid "D6958F73-4704-9B36-72F6-EB8DF08B59FA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId147";
	rename -uid "8AB8E7D9-4C27-5B10-B2E6-57A0927287E5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts142";
	rename -uid "0B1B324F-4345-D948-4250-A89B6F23B836";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId148";
	rename -uid "EC837CC4-40B4-B0A6-86A6-4EB4A385790E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts143";
	rename -uid "8406B8B2-4EF1-FD6D-0796-ED870A4B5A7F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId149";
	rename -uid "B42BF8F1-4FCB-3CC8-56EF-C399AEE4235D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts144";
	rename -uid "5E1AABA2-45C2-5193-8A5A-37837A3D01FC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId150";
	rename -uid "5B97B935-45E0-0263-C709-52BB780084B0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts145";
	rename -uid "7B9F1CBB-4D57-88A1-E390-8FB4FF534FEB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId151";
	rename -uid "02EB9E36-4693-0D84-1222-E8982E0DAA55";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts146";
	rename -uid "6EBF4737-496E-9C8D-0B86-72ABB6602D11";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId152";
	rename -uid "38CA79D7-4FD9-87F4-A008-0282C9AA7B00";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts147";
	rename -uid "93D36A88-46AE-0410-C4BA-DCA8AED669C1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId153";
	rename -uid "14D2F8AE-406A-FE58-5252-9E80B243544C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts148";
	rename -uid "E8F74CA8-4B53-AEAA-79F5-518D4567DA25";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId154";
	rename -uid "25C63E3C-435A-7CC0-3CE5-029F74BAFFE0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts149";
	rename -uid "01537EE8-414B-1BC9-8AC7-4D9E0D7A1675";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId155";
	rename -uid "69B372EE-46D2-C2F1-A7E7-AE9132B13C96";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts150";
	rename -uid "E4F3F3BA-4FA1-978F-EB9C-4F8845187A25";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId156";
	rename -uid "CAAE8459-436F-6411-F26E-A49CC1F3E9E2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts151";
	rename -uid "63C083CE-4004-05EE-20B9-FA98A577391F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId157";
	rename -uid "F0C9D5A3-42E9-AC85-B78A-09BAC8311423";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts152";
	rename -uid "01E31BEC-43C4-6E6A-423F-429D8CEDB7F3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId158";
	rename -uid "11A9D6E2-4A73-50BF-FFAD-30801B4F7C88";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts153";
	rename -uid "84168FEC-4BFA-ED69-DB0C-778C3514B757";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId159";
	rename -uid "1E4D930A-44E1-4657-87FA-028DE0B599A8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts154";
	rename -uid "3CF42779-4630-91AB-AB1B-48AD8F72F969";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId160";
	rename -uid "155FA73B-4BCE-F593-2E07-B2BC45178BBB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts155";
	rename -uid "7A51D50E-4DEB-D0A8-B727-079D5C1FDE9C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId161";
	rename -uid "BA37D951-497C-B5EF-DE7B-38AE90CF724D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts156";
	rename -uid "691AE3AD-4AA8-4924-3B4F-088917AABF95";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId162";
	rename -uid "EC52193A-4E09-9317-ED36-E68EA116F761";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts157";
	rename -uid "AF876D55-4421-DE48-7F37-319F0E20DD9F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId163";
	rename -uid "FE3BDA0D-4082-7A08-695D-62B13FBAE6AA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts158";
	rename -uid "F6EB5566-413E-1030-EA9C-6EB48EC641CD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId164";
	rename -uid "D42D28F5-4E69-9129-23CC-A0A3D1729ABC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts159";
	rename -uid "00BD25FB-482C-8011-7B4C-528CC629E6C6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId165";
	rename -uid "9FB8C455-471B-D661-B0DE-DDAFD4151566";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts160";
	rename -uid "ED7121D0-4028-9243-39FA-8D8F4948A58F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId166";
	rename -uid "99BC31ED-45B7-33F5-1E62-DEB3EB25F21F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts161";
	rename -uid "F98CA1FC-49CA-EDE4-C2DA-A8A11C4DEFB6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId167";
	rename -uid "2EBC6FAF-4FDC-82B0-B3B4-A28026837DEB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts162";
	rename -uid "F6731ACA-40F5-6C41-F33F-48B190455071";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId168";
	rename -uid "604B4132-45F6-43BC-F327-ABB1453268F3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts163";
	rename -uid "E44AA445-4EEB-304A-2890-88825B7BD8EE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId169";
	rename -uid "A515B6CE-43DE-E2F9-B3E4-CD80F767A08D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts164";
	rename -uid "ED40CE20-4066-0956-2475-1BB22484BEE3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId170";
	rename -uid "D3570D7A-4534-6F70-90AE-DDB83112E44B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts165";
	rename -uid "4F068E25-4848-8279-17FE-1082116025E9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId171";
	rename -uid "9ECA9583-471D-2AD1-0AE3-A182B0BBA80A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts166";
	rename -uid "D079684E-4811-6458-AB7D-CEA6D8371967";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId172";
	rename -uid "C1ACB4D7-437E-6D34-54F2-8093B377B20D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts167";
	rename -uid "171F8547-48A1-6BBE-7D4C-53A931AFA4D2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId173";
	rename -uid "00936580-4C9B-B920-4BE0-55B981B31CB2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts168";
	rename -uid "BF84AD78-4E91-A92F-31EB-DF982E05F693";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId174";
	rename -uid "5DFF38D3-4737-1FD3-FE54-76A1F1E0FE51";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts169";
	rename -uid "A1495B4A-42FB-4258-7534-538B0BF3BDC9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId175";
	rename -uid "0646F8AB-4029-67D8-37E4-B6B253B0F25C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts170";
	rename -uid "974C6821-4A4D-C15C-91A6-D6B4552620B9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId176";
	rename -uid "B5C8E91F-42FE-6912-6E61-C6A21DA29974";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts171";
	rename -uid "D2A2FCFF-4D17-7A2E-2261-7AA9CF356094";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId177";
	rename -uid "A935BCF2-437F-855A-AA8F-628551CD0ACB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts172";
	rename -uid "EE57CE8D-4C3B-1C2D-DA67-B2B40EB7B2DC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId178";
	rename -uid "D17D61ED-4177-4440-0E7C-E2A2F063FB79";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts173";
	rename -uid "C0B007A4-42AA-5282-5619-138E9A085086";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId179";
	rename -uid "E6E3B79A-4F6E-3AEE-DD3B-7EA30C9FE49B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts174";
	rename -uid "7CE96DE4-492E-0596-4326-D1878F97E52A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId180";
	rename -uid "8778E1C5-4FD0-D46A-1B90-0C87C5C019F3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts175";
	rename -uid "567EDC06-4F6D-078F-4221-2C9E67F23F7C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId181";
	rename -uid "0D4B83D4-4F6E-0904-E91F-DC84298C1833";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts176";
	rename -uid "2E3E5590-4ABA-3951-6150-C9B53AF7745F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId182";
	rename -uid "19C50D5A-4C11-E6ED-278F-41952D87AB8A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts177";
	rename -uid "E3E05728-419B-7CAF-DEA9-07AF7D2C5F13";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId183";
	rename -uid "039B1485-4B51-1FE3-6602-099CB0C48497";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts178";
	rename -uid "2D92690E-4693-69F1-D006-D68053E7C56B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId184";
	rename -uid "5BDF1939-417A-168A-635F-E49E8B745DD8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts179";
	rename -uid "983483F3-4D80-E954-5223-A4AD16FEA5EE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId185";
	rename -uid "7ABBC7DA-48C2-9126-422E-4BBF342B3A2E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts180";
	rename -uid "A4F4C2C8-4E88-3B3E-46DA-D2B3C2632D06";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId186";
	rename -uid "7C87B69B-4B7B-86BC-C165-BCADD95F1B33";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts181";
	rename -uid "FB9B9776-4AEA-CC09-AD64-CCBAC7F28792";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId187";
	rename -uid "B85F9C15-45C4-8F8E-414A-D9A7063270FB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts182";
	rename -uid "15E22D96-46E8-4645-E3FF-0491BF32D045";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId188";
	rename -uid "1FE6A65B-4FC3-2D5C-DE54-C6947957B65E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts183";
	rename -uid "7CA6872F-4113-3F23-1E97-6783368CEFF9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId189";
	rename -uid "5515A93F-4F34-344E-7BC5-EF8AA0001715";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts184";
	rename -uid "B4C3B94F-4EAA-2309-A1B5-208599ECD670";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId190";
	rename -uid "BDB8F7D5-4F3A-289F-45B6-5392AF03067F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts185";
	rename -uid "4BB7390F-490E-6E39-30CA-82B5FECFF36E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId191";
	rename -uid "DC75714E-4DDC-B76B-BF94-DFBD509F3A07";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts186";
	rename -uid "7E488583-4108-EE9E-91B0-3EBC8E2E6AA2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:296]";
createNode groupId -n "groupId192";
	rename -uid "28C448FF-4025-33FF-B2B8-30AB2FAC0C95";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts187";
	rename -uid "7E8E505F-4A0B-821D-60DA-6BA4546DAB29";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 12 "e[18:19]" "e[23]" "e[26:28]" "e[46:47]" "e[51]" "e[54:56]" "e[74:75]" "e[79]" "e[82:84]" "e[102:103]" "e[107]" "e[110:112]";
createNode groupId -n "groupId193";
	rename -uid "8A2673BC-46BC-34E5-3DE7-779E55068F12";
	setAttr ".ihi" 0;
createNode groupId -n "groupId194";
	rename -uid "64391092-4D06-7A76-F8E8-A2AEE88C557B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId195";
	rename -uid "CD00F1EB-4E90-0289-67D3-10BF21072006";
	setAttr ".ihi" 0;
createNode groupId -n "groupId196";
	rename -uid "68F67A53-4480-3A49-12C4-51AAB117D5D8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId197";
	rename -uid "507B702C-4197-8C09-D228-B8AABACB2A32";
	setAttr ".ihi" 0;
createNode groupId -n "groupId198";
	rename -uid "292E6793-4158-EEFA-0B24-18A91A0D3CA8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId199";
	rename -uid "452F3341-48CF-F052-D09E-1C8BF1F228C8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId200";
	rename -uid "ACAFA849-44A8-8045-D37E-EEB5E114D9CE";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 6 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 5 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :defaultTextureList1;
	setAttr -s 3 ".tx";
select -ne :initialShadingGroup;
	setAttr -s 108 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 100 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polySplit5.out" "pCubeShape18.i";
connectAttr "groupParts3.og" "polySurfaceShape2.i";
connectAttr "groupId7.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupId8.id" "polySurfaceShape2.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape2.iog.og[1].gco";
connectAttr "groupParts5.og" "polySurfaceShape3.i";
connectAttr "groupId9.id" "polySurfaceShape3.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape3.iog.og[0].gco";
connectAttr "groupId10.id" "polySurfaceShape3.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape3.iog.og[1].gco";
connectAttr "groupParts7.og" "polySurfaceShape4.i";
connectAttr "groupId11.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "groupId12.id" "polySurfaceShape4.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape4.iog.og[1].gco";
connectAttr "groupParts9.og" "polySurfaceShape5.i";
connectAttr "groupId13.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "groupId14.id" "polySurfaceShape5.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape5.iog.og[1].gco";
connectAttr "groupParts11.og" "polySurfaceShape6.i";
connectAttr "groupId15.id" "polySurfaceShape6.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape6.iog.og[0].gco";
connectAttr "groupId16.id" "polySurfaceShape6.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape6.iog.og[1].gco";
connectAttr "groupParts13.og" "polySurfaceShape7.i";
connectAttr "groupId17.id" "polySurfaceShape7.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape7.iog.og[0].gco";
connectAttr "groupId18.id" "polySurfaceShape7.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape7.iog.og[1].gco";
connectAttr "groupParts15.og" "polySurfaceShape8.i";
connectAttr "groupId19.id" "polySurfaceShape8.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape8.iog.og[0].gco";
connectAttr "groupId20.id" "polySurfaceShape8.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape8.iog.og[1].gco";
connectAttr "groupParts17.og" "polySurfaceShape9.i";
connectAttr "groupId21.id" "polySurfaceShape9.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape9.iog.og[0].gco";
connectAttr "groupId22.id" "polySurfaceShape9.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape9.iog.og[1].gco";
connectAttr "groupParts19.og" "polySurfaceShape10.i";
connectAttr "groupId23.id" "polySurfaceShape10.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape10.iog.og[0].gco";
connectAttr "groupId24.id" "polySurfaceShape10.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape10.iog.og[1].gco";
connectAttr "groupParts21.og" "polySurfaceShape11.i";
connectAttr "groupId25.id" "polySurfaceShape11.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape11.iog.og[0].gco";
connectAttr "groupId26.id" "polySurfaceShape11.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape11.iog.og[1].gco";
connectAttr "groupParts23.og" "polySurfaceShape12.i";
connectAttr "groupId27.id" "polySurfaceShape12.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape12.iog.og[0].gco";
connectAttr "groupId28.id" "polySurfaceShape12.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape12.iog.og[1].gco";
connectAttr "groupParts25.og" "polySurfaceShape13.i";
connectAttr "groupId29.id" "polySurfaceShape13.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape13.iog.og[0].gco";
connectAttr "groupId30.id" "polySurfaceShape13.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape13.iog.og[1].gco";
connectAttr "groupParts27.og" "polySurfaceShape14.i";
connectAttr "groupId31.id" "polySurfaceShape14.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape14.iog.og[0].gco";
connectAttr "groupId32.id" "polySurfaceShape14.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape14.iog.og[1].gco";
connectAttr "groupParts29.og" "polySurfaceShape15.i";
connectAttr "groupId33.id" "polySurfaceShape15.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape15.iog.og[0].gco";
connectAttr "groupId34.id" "polySurfaceShape15.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape15.iog.og[1].gco";
connectAttr "groupParts31.og" "polySurfaceShape16.i";
connectAttr "groupId35.id" "polySurfaceShape16.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape16.iog.og[0].gco";
connectAttr "groupId36.id" "polySurfaceShape16.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape16.iog.og[1].gco";
connectAttr "polyPlanarProj2.out" "polySurfaceShape17.i";
connectAttr "groupId37.id" "polySurfaceShape17.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape17.iog.og[0].gco";
connectAttr "groupId38.id" "polySurfaceShape17.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape17.iog.og[1].gco";
connectAttr "polyTweakUV1.uvtk[0]" "polySurfaceShape17.uvst[0].uvtw";
connectAttr "groupParts35.og" "polySurfaceShape18.i";
connectAttr "groupId39.id" "polySurfaceShape18.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape18.iog.og[0].gco";
connectAttr "groupId40.id" "polySurfaceShape18.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape18.iog.og[1].gco";
connectAttr "groupParts37.og" "polySurfaceShape19.i";
connectAttr "groupId41.id" "polySurfaceShape19.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape19.iog.og[0].gco";
connectAttr "groupId42.id" "polySurfaceShape19.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape19.iog.og[1].gco";
connectAttr "groupParts39.og" "polySurfaceShape20.i";
connectAttr "groupId43.id" "polySurfaceShape20.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape20.iog.og[0].gco";
connectAttr "groupId44.id" "polySurfaceShape20.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape20.iog.og[1].gco";
connectAttr "groupParts41.og" "polySurfaceShape21.i";
connectAttr "groupId45.id" "polySurfaceShape21.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape21.iog.og[0].gco";
connectAttr "groupId46.id" "polySurfaceShape21.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape21.iog.og[1].gco";
connectAttr "groupParts43.og" "polySurfaceShape22.i";
connectAttr "groupId47.id" "polySurfaceShape22.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape22.iog.og[0].gco";
connectAttr "groupId48.id" "polySurfaceShape22.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape22.iog.og[1].gco";
connectAttr "groupParts45.og" "polySurfaceShape23.i";
connectAttr "groupId49.id" "polySurfaceShape23.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape23.iog.og[0].gco";
connectAttr "groupId50.id" "polySurfaceShape23.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape23.iog.og[1].gco";
connectAttr "groupParts47.og" "polySurfaceShape24.i";
connectAttr "groupId51.id" "polySurfaceShape24.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape24.iog.og[0].gco";
connectAttr "groupId52.id" "polySurfaceShape24.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape24.iog.og[1].gco";
connectAttr "groupParts49.og" "polySurfaceShape25.i";
connectAttr "groupId53.id" "polySurfaceShape25.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape25.iog.og[0].gco";
connectAttr "groupId54.id" "polySurfaceShape25.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape25.iog.og[1].gco";
connectAttr "groupParts51.og" "polySurfaceShape26.i";
connectAttr "groupId55.id" "polySurfaceShape26.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape26.iog.og[0].gco";
connectAttr "groupId56.id" "polySurfaceShape26.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape26.iog.og[1].gco";
connectAttr "groupParts53.og" "polySurfaceShape27.i";
connectAttr "groupId57.id" "polySurfaceShape27.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape27.iog.og[0].gco";
connectAttr "groupId58.id" "polySurfaceShape27.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape27.iog.og[1].gco";
connectAttr "groupParts55.og" "polySurfaceShape28.i";
connectAttr "groupId59.id" "polySurfaceShape28.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape28.iog.og[0].gco";
connectAttr "groupId60.id" "polySurfaceShape28.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape28.iog.og[1].gco";
connectAttr "groupParts57.og" "polySurfaceShape29.i";
connectAttr "groupId61.id" "polySurfaceShape29.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape29.iog.og[0].gco";
connectAttr "groupId62.id" "polySurfaceShape29.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape29.iog.og[1].gco";
connectAttr "groupParts59.og" "polySurfaceShape30.i";
connectAttr "groupId63.id" "polySurfaceShape30.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape30.iog.og[0].gco";
connectAttr "groupId64.id" "polySurfaceShape30.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape30.iog.og[1].gco";
connectAttr "groupParts61.og" "polySurfaceShape31.i";
connectAttr "groupId65.id" "polySurfaceShape31.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape31.iog.og[0].gco";
connectAttr "groupId66.id" "polySurfaceShape31.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape31.iog.og[1].gco";
connectAttr "groupParts63.og" "polySurfaceShape32.i";
connectAttr "groupId67.id" "polySurfaceShape32.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape32.iog.og[0].gco";
connectAttr "groupId68.id" "polySurfaceShape32.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape32.iog.og[1].gco";
connectAttr "groupParts65.og" "polySurfaceShape33.i";
connectAttr "groupId69.id" "polySurfaceShape33.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape33.iog.og[0].gco";
connectAttr "groupId70.id" "polySurfaceShape33.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape33.iog.og[1].gco";
connectAttr "groupParts67.og" "polySurfaceShape34.i";
connectAttr "groupId71.id" "polySurfaceShape34.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape34.iog.og[0].gco";
connectAttr "groupId72.id" "polySurfaceShape34.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape34.iog.og[1].gco";
connectAttr "groupParts69.og" "polySurfaceShape35.i";
connectAttr "groupId73.id" "polySurfaceShape35.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape35.iog.og[0].gco";
connectAttr "groupId74.id" "polySurfaceShape35.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape35.iog.og[1].gco";
connectAttr "groupParts71.og" "polySurfaceShape36.i";
connectAttr "groupId75.id" "polySurfaceShape36.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape36.iog.og[0].gco";
connectAttr "groupId76.id" "polySurfaceShape36.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape36.iog.og[1].gco";
connectAttr "groupParts73.og" "polySurfaceShape37.i";
connectAttr "groupId77.id" "polySurfaceShape37.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape37.iog.og[0].gco";
connectAttr "groupId78.id" "polySurfaceShape37.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape37.iog.og[1].gco";
connectAttr "groupParts75.og" "polySurfaceShape38.i";
connectAttr "groupId79.id" "polySurfaceShape38.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape38.iog.og[0].gco";
connectAttr "groupId80.id" "polySurfaceShape38.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape38.iog.og[1].gco";
connectAttr "groupParts77.og" "polySurfaceShape39.i";
connectAttr "groupId81.id" "polySurfaceShape39.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape39.iog.og[0].gco";
connectAttr "groupId82.id" "polySurfaceShape39.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape39.iog.og[1].gco";
connectAttr "groupParts79.og" "polySurfaceShape40.i";
connectAttr "groupId83.id" "polySurfaceShape40.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape40.iog.og[0].gco";
connectAttr "groupId84.id" "polySurfaceShape40.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape40.iog.og[1].gco";
connectAttr "groupParts81.og" "polySurfaceShape41.i";
connectAttr "groupId85.id" "polySurfaceShape41.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape41.iog.og[0].gco";
connectAttr "groupId86.id" "polySurfaceShape41.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape41.iog.og[1].gco";
connectAttr "groupParts83.og" "polySurfaceShape42.i";
connectAttr "groupId87.id" "polySurfaceShape42.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape42.iog.og[0].gco";
connectAttr "groupId88.id" "polySurfaceShape42.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape42.iog.og[1].gco";
connectAttr "groupParts85.og" "polySurfaceShape43.i";
connectAttr "groupId89.id" "polySurfaceShape43.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape43.iog.og[0].gco";
connectAttr "groupId90.id" "polySurfaceShape43.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape43.iog.og[1].gco";
connectAttr "groupParts87.og" "polySurfaceShape44.i";
connectAttr "groupId91.id" "polySurfaceShape44.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape44.iog.og[0].gco";
connectAttr "groupId92.id" "polySurfaceShape44.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape44.iog.og[1].gco";
connectAttr "groupParts89.og" "polySurfaceShape45.i";
connectAttr "groupId93.id" "polySurfaceShape45.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape45.iog.og[0].gco";
connectAttr "groupId94.id" "polySurfaceShape45.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape45.iog.og[1].gco";
connectAttr "groupParts91.og" "polySurfaceShape46.i";
connectAttr "groupId95.id" "polySurfaceShape46.iog.og[0].gid";
connectAttr "set1.mwc" "polySurfaceShape46.iog.og[0].gco";
connectAttr "groupId96.id" "polySurfaceShape46.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape46.iog.og[1].gco";
connectAttr "groupId1.id" "pCube19Shape.iog.og[0].gid";
connectAttr "set1.mwc" "pCube19Shape.iog.og[0].gco";
connectAttr "groupId2.id" "pCube19Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCube19Shape.iog.og[1].gco";
connectAttr "groupId3.id" "pCylinderShape2.iog.og[0].gid";
connectAttr "set2.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "polyAutoProj1.out" "pCylinderShape2.i";
connectAttr "groupId4.id" "pCylinderShape3.iog.og[0].gid";
connectAttr "set2.mwc" "pCylinderShape3.iog.og[0].gco";
connectAttr "groupId5.id" "pCylinderShape4.iog.og[0].gid";
connectAttr "set2.mwc" "pCylinderShape4.iog.og[0].gco";
connectAttr "groupId6.id" "pCylinderShape5.iog.og[0].gid";
connectAttr "set2.mwc" "pCylinderShape5.iog.og[0].gco";
connectAttr "polyCylinder1.out" "pCylinderShape6.i";
connectAttr "groupParts92.og" "polySurface7Shape.i";
connectAttr "groupId97.id" "polySurface7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurface7Shape.iog.og[0].gco";
connectAttr "groupParts93.og" "polySurface16Shape.i";
connectAttr "groupId98.id" "polySurface16Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurface16Shape.iog.og[0].gco";
connectAttr "polyStraightenUVBorder1.out" "pCubeShape19.i";
connectAttr "polyTweakUV5.uvtk[0]" "pCubeShape19.uvst[0].uvtw";
connectAttr "groupParts95.og" "polySurface2Shape.i";
connectAttr "groupId99.id" "polySurface2Shape.iog.og[0].gid";
connectAttr "set1.mwc" "polySurface2Shape.iog.og[0].gco";
connectAttr "groupId100.id" "polySurface2Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "polySurface2Shape.iog.og[1].gco";
connectAttr "groupParts99.og" "polySurfaceShape47.i";
connectAttr "groupId103.id" "polySurfaceShape47.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape47.iog.og[0].gco";
connectAttr "groupId104.id" "polySurfaceShape47.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape47.iog.og[1].gco";
connectAttr "groupParts101.og" "polySurfaceShape48.i";
connectAttr "groupId105.id" "polySurfaceShape48.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape48.iog.og[0].gco";
connectAttr "groupId106.id" "polySurfaceShape48.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape48.iog.og[1].gco";
connectAttr "groupParts103.og" "polySurfaceShape49.i";
connectAttr "groupId107.id" "polySurfaceShape49.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape49.iog.og[0].gco";
connectAttr "groupId108.id" "polySurfaceShape49.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape49.iog.og[1].gco";
connectAttr "groupParts105.og" "polySurfaceShape50.i";
connectAttr "groupId109.id" "polySurfaceShape50.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape50.iog.og[0].gco";
connectAttr "groupId110.id" "polySurfaceShape50.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape50.iog.og[1].gco";
connectAttr "groupParts107.og" "polySurfaceShape51.i";
connectAttr "groupId111.id" "polySurfaceShape51.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape51.iog.og[0].gco";
connectAttr "groupId112.id" "polySurfaceShape51.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape51.iog.og[1].gco";
connectAttr "groupParts109.og" "polySurfaceShape52.i";
connectAttr "groupId113.id" "polySurfaceShape52.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape52.iog.og[0].gco";
connectAttr "groupId114.id" "polySurfaceShape52.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape52.iog.og[1].gco";
connectAttr "groupParts111.og" "polySurfaceShape53.i";
connectAttr "groupId115.id" "polySurfaceShape53.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape53.iog.og[0].gco";
connectAttr "groupId116.id" "polySurfaceShape53.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape53.iog.og[1].gco";
connectAttr "groupParts113.og" "polySurfaceShape54.i";
connectAttr "groupId117.id" "polySurfaceShape54.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape54.iog.og[0].gco";
connectAttr "groupId118.id" "polySurfaceShape54.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape54.iog.og[1].gco";
connectAttr "groupParts115.og" "polySurfaceShape55.i";
connectAttr "groupId119.id" "polySurfaceShape55.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape55.iog.og[0].gco";
connectAttr "groupId120.id" "polySurfaceShape55.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape55.iog.og[1].gco";
connectAttr "groupParts117.og" "polySurfaceShape56.i";
connectAttr "groupId121.id" "polySurfaceShape56.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape56.iog.og[0].gco";
connectAttr "groupId122.id" "polySurfaceShape56.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape56.iog.og[1].gco";
connectAttr "groupParts119.og" "polySurfaceShape57.i";
connectAttr "groupId123.id" "polySurfaceShape57.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape57.iog.og[0].gco";
connectAttr "groupId124.id" "polySurfaceShape57.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape57.iog.og[1].gco";
connectAttr "groupParts121.og" "polySurfaceShape58.i";
connectAttr "groupId125.id" "polySurfaceShape58.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape58.iog.og[0].gco";
connectAttr "groupId126.id" "polySurfaceShape58.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape58.iog.og[1].gco";
connectAttr "groupParts123.og" "polySurfaceShape59.i";
connectAttr "groupId127.id" "polySurfaceShape59.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape59.iog.og[0].gco";
connectAttr "groupId128.id" "polySurfaceShape59.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape59.iog.og[1].gco";
connectAttr "groupParts125.og" "polySurfaceShape60.i";
connectAttr "groupId129.id" "polySurfaceShape60.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape60.iog.og[0].gco";
connectAttr "groupId130.id" "polySurfaceShape60.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape60.iog.og[1].gco";
connectAttr "groupParts127.og" "polySurfaceShape61.i";
connectAttr "groupId131.id" "polySurfaceShape61.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape61.iog.og[0].gco";
connectAttr "groupId132.id" "polySurfaceShape61.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape61.iog.og[1].gco";
connectAttr "groupParts129.og" "polySurfaceShape62.i";
connectAttr "groupId133.id" "polySurfaceShape62.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape62.iog.og[0].gco";
connectAttr "groupId134.id" "polySurfaceShape62.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape62.iog.og[1].gco";
connectAttr "groupParts131.og" "polySurfaceShape63.i";
connectAttr "groupId135.id" "polySurfaceShape63.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape63.iog.og[0].gco";
connectAttr "groupId136.id" "polySurfaceShape63.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape63.iog.og[1].gco";
connectAttr "groupParts133.og" "polySurfaceShape64.i";
connectAttr "groupId137.id" "polySurfaceShape64.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape64.iog.og[0].gco";
connectAttr "groupId138.id" "polySurfaceShape64.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape64.iog.og[1].gco";
connectAttr "groupParts135.og" "polySurfaceShape65.i";
connectAttr "groupId139.id" "polySurfaceShape65.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape65.iog.og[0].gco";
connectAttr "groupId140.id" "polySurfaceShape65.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape65.iog.og[1].gco";
connectAttr "groupParts137.og" "polySurfaceShape66.i";
connectAttr "groupId141.id" "polySurfaceShape66.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape66.iog.og[0].gco";
connectAttr "groupId142.id" "polySurfaceShape66.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape66.iog.og[1].gco";
connectAttr "groupParts139.og" "polySurfaceShape67.i";
connectAttr "groupId143.id" "polySurfaceShape67.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape67.iog.og[0].gco";
connectAttr "groupId144.id" "polySurfaceShape67.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape67.iog.og[1].gco";
connectAttr "groupParts141.og" "polySurfaceShape68.i";
connectAttr "groupId145.id" "polySurfaceShape68.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape68.iog.og[0].gco";
connectAttr "groupId146.id" "polySurfaceShape68.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape68.iog.og[1].gco";
connectAttr "groupParts143.og" "polySurfaceShape69.i";
connectAttr "groupId147.id" "polySurfaceShape69.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape69.iog.og[0].gco";
connectAttr "groupId148.id" "polySurfaceShape69.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape69.iog.og[1].gco";
connectAttr "groupParts145.og" "polySurfaceShape70.i";
connectAttr "groupId149.id" "polySurfaceShape70.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape70.iog.og[0].gco";
connectAttr "groupId150.id" "polySurfaceShape70.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape70.iog.og[1].gco";
connectAttr "groupParts147.og" "polySurfaceShape71.i";
connectAttr "groupId151.id" "polySurfaceShape71.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape71.iog.og[0].gco";
connectAttr "groupId152.id" "polySurfaceShape71.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape71.iog.og[1].gco";
connectAttr "groupParts149.og" "polySurfaceShape72.i";
connectAttr "groupId153.id" "polySurfaceShape72.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape72.iog.og[0].gco";
connectAttr "groupId154.id" "polySurfaceShape72.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape72.iog.og[1].gco";
connectAttr "groupParts151.og" "polySurfaceShape73.i";
connectAttr "groupId155.id" "polySurfaceShape73.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape73.iog.og[0].gco";
connectAttr "groupId156.id" "polySurfaceShape73.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape73.iog.og[1].gco";
connectAttr "groupParts153.og" "polySurfaceShape74.i";
connectAttr "groupId157.id" "polySurfaceShape74.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape74.iog.og[0].gco";
connectAttr "groupId158.id" "polySurfaceShape74.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape74.iog.og[1].gco";
connectAttr "groupParts155.og" "polySurfaceShape75.i";
connectAttr "groupId159.id" "polySurfaceShape75.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape75.iog.og[0].gco";
connectAttr "groupId160.id" "polySurfaceShape75.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape75.iog.og[1].gco";
connectAttr "groupParts157.og" "polySurfaceShape76.i";
connectAttr "groupId161.id" "polySurfaceShape76.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape76.iog.og[0].gco";
connectAttr "groupId162.id" "polySurfaceShape76.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape76.iog.og[1].gco";
connectAttr "groupParts159.og" "polySurfaceShape77.i";
connectAttr "groupId163.id" "polySurfaceShape77.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape77.iog.og[0].gco";
connectAttr "groupId164.id" "polySurfaceShape77.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape77.iog.og[1].gco";
connectAttr "groupParts161.og" "polySurfaceShape78.i";
connectAttr "groupId165.id" "polySurfaceShape78.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape78.iog.og[0].gco";
connectAttr "groupId166.id" "polySurfaceShape78.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape78.iog.og[1].gco";
connectAttr "groupParts163.og" "polySurfaceShape79.i";
connectAttr "groupId167.id" "polySurfaceShape79.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape79.iog.og[0].gco";
connectAttr "groupId168.id" "polySurfaceShape79.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape79.iog.og[1].gco";
connectAttr "groupParts165.og" "polySurfaceShape80.i";
connectAttr "groupId169.id" "polySurfaceShape80.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape80.iog.og[0].gco";
connectAttr "groupId170.id" "polySurfaceShape80.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape80.iog.og[1].gco";
connectAttr "groupParts167.og" "polySurfaceShape81.i";
connectAttr "groupId171.id" "polySurfaceShape81.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape81.iog.og[0].gco";
connectAttr "groupId172.id" "polySurfaceShape81.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape81.iog.og[1].gco";
connectAttr "groupParts169.og" "polySurfaceShape82.i";
connectAttr "groupId173.id" "polySurfaceShape82.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape82.iog.og[0].gco";
connectAttr "groupId174.id" "polySurfaceShape82.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape82.iog.og[1].gco";
connectAttr "groupParts171.og" "polySurfaceShape83.i";
connectAttr "groupId175.id" "polySurfaceShape83.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape83.iog.og[0].gco";
connectAttr "groupId176.id" "polySurfaceShape83.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape83.iog.og[1].gco";
connectAttr "groupParts173.og" "polySurfaceShape84.i";
connectAttr "groupId177.id" "polySurfaceShape84.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape84.iog.og[0].gco";
connectAttr "groupId178.id" "polySurfaceShape84.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape84.iog.og[1].gco";
connectAttr "groupParts175.og" "polySurfaceShape85.i";
connectAttr "groupId179.id" "polySurfaceShape85.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape85.iog.og[0].gco";
connectAttr "groupId180.id" "polySurfaceShape85.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape85.iog.og[1].gco";
connectAttr "groupParts177.og" "polySurfaceShape86.i";
connectAttr "groupId181.id" "polySurfaceShape86.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape86.iog.og[0].gco";
connectAttr "groupId182.id" "polySurfaceShape86.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape86.iog.og[1].gco";
connectAttr "groupParts179.og" "polySurfaceShape87.i";
connectAttr "groupId183.id" "polySurfaceShape87.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape87.iog.og[0].gco";
connectAttr "groupId184.id" "polySurfaceShape87.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape87.iog.og[1].gco";
connectAttr "groupParts181.og" "polySurfaceShape88.i";
connectAttr "groupId185.id" "polySurfaceShape88.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape88.iog.og[0].gco";
connectAttr "groupId186.id" "polySurfaceShape88.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape88.iog.og[1].gco";
connectAttr "groupParts183.og" "polySurfaceShape89.i";
connectAttr "groupId187.id" "polySurfaceShape89.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape89.iog.og[0].gco";
connectAttr "groupId188.id" "polySurfaceShape89.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape89.iog.og[1].gco";
connectAttr "groupParts185.og" "polySurfaceShape90.i";
connectAttr "groupId189.id" "polySurfaceShape90.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape90.iog.og[0].gco";
connectAttr "groupId190.id" "polySurfaceShape90.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape90.iog.og[1].gco";
connectAttr "groupParts187.og" "|polySurface4|polySurface90|polySurfaceShape91.i"
		;
connectAttr "groupId191.id" "|polySurface4|polySurface90|polySurfaceShape91.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|polySurface4|polySurface90|polySurfaceShape91.iog.og[0].gco"
		;
connectAttr "groupId192.id" "|polySurface4|polySurface90|polySurfaceShape91.iog.og[1].gid"
		;
connectAttr "set1.mwc" "|polySurface4|polySurface90|polySurfaceShape91.iog.og[1].gco"
		;
connectAttr "groupParts97.og" "polySurface4Shape.i";
connectAttr "groupId101.id" "polySurface4Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurface4Shape.iog.og[0].gco";
connectAttr "groupId102.id" "polySurface4Shape.iog.og[1].gid";
connectAttr "set1.mwc" "polySurface4Shape.iog.og[1].gco";
connectAttr "groupId193.id" "|polySurface4|polySurface91|polySurfaceShape91.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|polySurface4|polySurface91|polySurfaceShape91.iog.og[0].gco"
		;
connectAttr "groupId194.id" "|polySurface4|polySurface91|polySurfaceShape91.iog.og[1].gid"
		;
connectAttr "set1.mwc" "|polySurface4|polySurface91|polySurfaceShape91.iog.og[1].gco"
		;
connectAttr "groupId195.id" "polySurfaceShape92.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape92.iog.og[0].gco";
connectAttr "groupId196.id" "polySurfaceShape92.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape92.iog.og[1].gco";
connectAttr "groupId197.id" "polySurfaceShape93.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape93.iog.og[0].gco";
connectAttr "groupId198.id" "polySurfaceShape93.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape93.iog.og[1].gco";
connectAttr "groupId199.id" "polySurfaceShape94.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape94.iog.og[0].gco";
connectAttr "groupId200.id" "polySurfaceShape94.iog.og[1].gid";
connectAttr "set1.mwc" "polySurfaceShape94.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "groupId7.msg" "set1.gn" -na;
connectAttr "groupId9.msg" "set1.gn" -na;
connectAttr "groupId11.msg" "set1.gn" -na;
connectAttr "groupId13.msg" "set1.gn" -na;
connectAttr "groupId15.msg" "set1.gn" -na;
connectAttr "groupId17.msg" "set1.gn" -na;
connectAttr "groupId19.msg" "set1.gn" -na;
connectAttr "groupId21.msg" "set1.gn" -na;
connectAttr "groupId23.msg" "set1.gn" -na;
connectAttr "groupId25.msg" "set1.gn" -na;
connectAttr "groupId27.msg" "set1.gn" -na;
connectAttr "groupId29.msg" "set1.gn" -na;
connectAttr "groupId31.msg" "set1.gn" -na;
connectAttr "groupId33.msg" "set1.gn" -na;
connectAttr "groupId35.msg" "set1.gn" -na;
connectAttr "groupId37.msg" "set1.gn" -na;
connectAttr "groupId39.msg" "set1.gn" -na;
connectAttr "groupId41.msg" "set1.gn" -na;
connectAttr "groupId43.msg" "set1.gn" -na;
connectAttr "groupId45.msg" "set1.gn" -na;
connectAttr "groupId47.msg" "set1.gn" -na;
connectAttr "groupId49.msg" "set1.gn" -na;
connectAttr "groupId51.msg" "set1.gn" -na;
connectAttr "groupId53.msg" "set1.gn" -na;
connectAttr "groupId55.msg" "set1.gn" -na;
connectAttr "groupId57.msg" "set1.gn" -na;
connectAttr "groupId59.msg" "set1.gn" -na;
connectAttr "groupId61.msg" "set1.gn" -na;
connectAttr "groupId63.msg" "set1.gn" -na;
connectAttr "groupId65.msg" "set1.gn" -na;
connectAttr "groupId67.msg" "set1.gn" -na;
connectAttr "groupId69.msg" "set1.gn" -na;
connectAttr "groupId71.msg" "set1.gn" -na;
connectAttr "groupId73.msg" "set1.gn" -na;
connectAttr "groupId75.msg" "set1.gn" -na;
connectAttr "groupId77.msg" "set1.gn" -na;
connectAttr "groupId79.msg" "set1.gn" -na;
connectAttr "groupId81.msg" "set1.gn" -na;
connectAttr "groupId83.msg" "set1.gn" -na;
connectAttr "groupId85.msg" "set1.gn" -na;
connectAttr "groupId87.msg" "set1.gn" -na;
connectAttr "groupId89.msg" "set1.gn" -na;
connectAttr "groupId91.msg" "set1.gn" -na;
connectAttr "groupId93.msg" "set1.gn" -na;
connectAttr "groupId95.msg" "set1.gn" -na;
connectAttr "groupId99.msg" "set1.gn" -na;
connectAttr "groupId102.msg" "set1.gn" -na;
connectAttr "groupId104.msg" "set1.gn" -na;
connectAttr "groupId106.msg" "set1.gn" -na;
connectAttr "groupId108.msg" "set1.gn" -na;
connectAttr "groupId110.msg" "set1.gn" -na;
connectAttr "groupId112.msg" "set1.gn" -na;
connectAttr "groupId114.msg" "set1.gn" -na;
connectAttr "groupId116.msg" "set1.gn" -na;
connectAttr "groupId118.msg" "set1.gn" -na;
connectAttr "groupId120.msg" "set1.gn" -na;
connectAttr "groupId122.msg" "set1.gn" -na;
connectAttr "groupId124.msg" "set1.gn" -na;
connectAttr "groupId126.msg" "set1.gn" -na;
connectAttr "groupId128.msg" "set1.gn" -na;
connectAttr "groupId130.msg" "set1.gn" -na;
connectAttr "groupId132.msg" "set1.gn" -na;
connectAttr "groupId134.msg" "set1.gn" -na;
connectAttr "groupId136.msg" "set1.gn" -na;
connectAttr "groupId138.msg" "set1.gn" -na;
connectAttr "groupId140.msg" "set1.gn" -na;
connectAttr "groupId142.msg" "set1.gn" -na;
connectAttr "groupId144.msg" "set1.gn" -na;
connectAttr "groupId146.msg" "set1.gn" -na;
connectAttr "groupId148.msg" "set1.gn" -na;
connectAttr "groupId150.msg" "set1.gn" -na;
connectAttr "groupId152.msg" "set1.gn" -na;
connectAttr "groupId154.msg" "set1.gn" -na;
connectAttr "groupId156.msg" "set1.gn" -na;
connectAttr "groupId158.msg" "set1.gn" -na;
connectAttr "groupId160.msg" "set1.gn" -na;
connectAttr "groupId162.msg" "set1.gn" -na;
connectAttr "groupId164.msg" "set1.gn" -na;
connectAttr "groupId166.msg" "set1.gn" -na;
connectAttr "groupId168.msg" "set1.gn" -na;
connectAttr "groupId170.msg" "set1.gn" -na;
connectAttr "groupId172.msg" "set1.gn" -na;
connectAttr "groupId174.msg" "set1.gn" -na;
connectAttr "groupId176.msg" "set1.gn" -na;
connectAttr "groupId178.msg" "set1.gn" -na;
connectAttr "groupId180.msg" "set1.gn" -na;
connectAttr "groupId182.msg" "set1.gn" -na;
connectAttr "groupId184.msg" "set1.gn" -na;
connectAttr "groupId186.msg" "set1.gn" -na;
connectAttr "groupId188.msg" "set1.gn" -na;
connectAttr "groupId190.msg" "set1.gn" -na;
connectAttr "groupId192.msg" "set1.gn" -na;
connectAttr "groupId194.msg" "set1.gn" -na;
connectAttr "groupId196.msg" "set1.gn" -na;
connectAttr "groupId198.msg" "set1.gn" -na;
connectAttr "groupId200.msg" "set1.gn" -na;
connectAttr "pCube19Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape3.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape7.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape8.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape9.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape10.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape11.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape12.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape13.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape14.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape15.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape16.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape17.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape18.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape19.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape20.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape21.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape22.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape23.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape24.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape25.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape26.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape27.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape28.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape29.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape30.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape31.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape32.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape33.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape34.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape35.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape37.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape38.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape39.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape40.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape41.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape42.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape43.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape44.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape45.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurfaceShape46.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurface2Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "polySurface4Shape.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape47.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape48.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape49.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape50.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape51.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape52.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape53.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape54.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape55.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape56.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape57.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape58.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape59.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape60.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape61.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape62.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape63.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape64.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape65.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape66.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape67.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape68.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape69.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape70.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape71.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape72.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape73.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape74.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape75.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape76.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape77.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape78.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape79.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape80.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape81.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape82.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape83.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape84.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape85.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape86.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape87.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape88.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape89.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape90.iog.og[1]" "set1.dsm" -na;
connectAttr "|polySurface4|polySurface90|polySurfaceShape91.iog.og[1]" "set1.dsm"
		 -na;
connectAttr "|polySurface4|polySurface91|polySurfaceShape91.iog.og[1]" "set1.dsm"
		 -na;
connectAttr "polySurfaceShape92.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape93.iog.og[1]" "set1.dsm" -na;
connectAttr "polySurfaceShape94.iog.og[1]" "set1.dsm" -na;
connectAttr "polyCube4.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "groupId3.msg" "set2.gn" -na;
connectAttr "groupId4.msg" "set2.gn" -na;
connectAttr "groupId5.msg" "set2.gn" -na;
connectAttr "groupId6.msg" "set2.gn" -na;
connectAttr "pCylinderShape2.iog.og[0]" "set2.dsm" -na;
connectAttr "pCylinderShape3.iog.og[0]" "set2.dsm" -na;
connectAttr "pCylinderShape4.iog.og[0]" "set2.dsm" -na;
connectAttr "pCylinderShape5.iog.og[0]" "set2.dsm" -na;
connectAttr "groupParts1.og" "polySoftEdge1.ip";
connectAttr "pCylinderShape2.wm" "polySoftEdge1.mp";
connectAttr "|pCylinder2|polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId3.id" "groupParts1.gi";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCylinderShape2.wm" "polySoftEdge2.mp";
connectAttr "pCube19Shape.o" "polySeparate1.ip";
connectAttr "polySeparate1.out[0]" "groupParts2.ig";
connectAttr "groupId7.id" "groupParts2.gi";
connectAttr "groupParts2.og" "groupParts3.ig";
connectAttr "groupId8.id" "groupParts3.gi";
connectAttr "polySeparate1.out[1]" "groupParts4.ig";
connectAttr "groupId9.id" "groupParts4.gi";
connectAttr "groupParts4.og" "groupParts5.ig";
connectAttr "groupId10.id" "groupParts5.gi";
connectAttr "polySeparate1.out[2]" "groupParts6.ig";
connectAttr "groupId11.id" "groupParts6.gi";
connectAttr "groupParts6.og" "groupParts7.ig";
connectAttr "groupId12.id" "groupParts7.gi";
connectAttr "polySeparate1.out[3]" "groupParts8.ig";
connectAttr "groupId13.id" "groupParts8.gi";
connectAttr "groupParts8.og" "groupParts9.ig";
connectAttr "groupId14.id" "groupParts9.gi";
connectAttr "polySeparate1.out[4]" "groupParts10.ig";
connectAttr "groupId15.id" "groupParts10.gi";
connectAttr "groupParts10.og" "groupParts11.ig";
connectAttr "groupId16.id" "groupParts11.gi";
connectAttr "polySeparate1.out[5]" "groupParts12.ig";
connectAttr "groupId17.id" "groupParts12.gi";
connectAttr "groupParts12.og" "groupParts13.ig";
connectAttr "groupId18.id" "groupParts13.gi";
connectAttr "polySeparate1.out[6]" "groupParts14.ig";
connectAttr "groupId19.id" "groupParts14.gi";
connectAttr "groupParts14.og" "groupParts15.ig";
connectAttr "groupId20.id" "groupParts15.gi";
connectAttr "polySeparate1.out[7]" "groupParts16.ig";
connectAttr "groupId21.id" "groupParts16.gi";
connectAttr "groupParts16.og" "groupParts17.ig";
connectAttr "groupId22.id" "groupParts17.gi";
connectAttr "polySeparate1.out[8]" "groupParts18.ig";
connectAttr "groupId23.id" "groupParts18.gi";
connectAttr "groupParts18.og" "groupParts19.ig";
connectAttr "groupId24.id" "groupParts19.gi";
connectAttr "polySeparate1.out[9]" "groupParts20.ig";
connectAttr "groupId25.id" "groupParts20.gi";
connectAttr "groupParts20.og" "groupParts21.ig";
connectAttr "groupId26.id" "groupParts21.gi";
connectAttr "polySeparate1.out[10]" "groupParts22.ig";
connectAttr "groupId27.id" "groupParts22.gi";
connectAttr "groupParts22.og" "groupParts23.ig";
connectAttr "groupId28.id" "groupParts23.gi";
connectAttr "polySeparate1.out[11]" "groupParts24.ig";
connectAttr "groupId29.id" "groupParts24.gi";
connectAttr "groupParts24.og" "groupParts25.ig";
connectAttr "groupId30.id" "groupParts25.gi";
connectAttr "polySeparate1.out[12]" "groupParts26.ig";
connectAttr "groupId31.id" "groupParts26.gi";
connectAttr "groupParts26.og" "groupParts27.ig";
connectAttr "groupId32.id" "groupParts27.gi";
connectAttr "polySeparate1.out[13]" "groupParts28.ig";
connectAttr "groupId33.id" "groupParts28.gi";
connectAttr "groupParts28.og" "groupParts29.ig";
connectAttr "groupId34.id" "groupParts29.gi";
connectAttr "polySeparate1.out[14]" "groupParts30.ig";
connectAttr "groupId35.id" "groupParts30.gi";
connectAttr "groupParts30.og" "groupParts31.ig";
connectAttr "groupId36.id" "groupParts31.gi";
connectAttr "polySeparate1.out[15]" "groupParts32.ig";
connectAttr "groupId37.id" "groupParts32.gi";
connectAttr "groupParts32.og" "groupParts33.ig";
connectAttr "groupId38.id" "groupParts33.gi";
connectAttr "polySeparate1.out[16]" "groupParts34.ig";
connectAttr "groupId39.id" "groupParts34.gi";
connectAttr "groupParts34.og" "groupParts35.ig";
connectAttr "groupId40.id" "groupParts35.gi";
connectAttr "polySeparate1.out[17]" "groupParts36.ig";
connectAttr "groupId41.id" "groupParts36.gi";
connectAttr "groupParts36.og" "groupParts37.ig";
connectAttr "groupId42.id" "groupParts37.gi";
connectAttr "polySeparate1.out[18]" "groupParts38.ig";
connectAttr "groupId43.id" "groupParts38.gi";
connectAttr "groupParts38.og" "groupParts39.ig";
connectAttr "groupId44.id" "groupParts39.gi";
connectAttr "polySeparate1.out[19]" "groupParts40.ig";
connectAttr "groupId45.id" "groupParts40.gi";
connectAttr "groupParts40.og" "groupParts41.ig";
connectAttr "groupId46.id" "groupParts41.gi";
connectAttr "polySeparate1.out[20]" "groupParts42.ig";
connectAttr "groupId47.id" "groupParts42.gi";
connectAttr "groupParts42.og" "groupParts43.ig";
connectAttr "groupId48.id" "groupParts43.gi";
connectAttr "polySeparate1.out[21]" "groupParts44.ig";
connectAttr "groupId49.id" "groupParts44.gi";
connectAttr "groupParts44.og" "groupParts45.ig";
connectAttr "groupId50.id" "groupParts45.gi";
connectAttr "polySeparate1.out[22]" "groupParts46.ig";
connectAttr "groupId51.id" "groupParts46.gi";
connectAttr "groupParts46.og" "groupParts47.ig";
connectAttr "groupId52.id" "groupParts47.gi";
connectAttr "polySeparate1.out[23]" "groupParts48.ig";
connectAttr "groupId53.id" "groupParts48.gi";
connectAttr "groupParts48.og" "groupParts49.ig";
connectAttr "groupId54.id" "groupParts49.gi";
connectAttr "polySeparate1.out[24]" "groupParts50.ig";
connectAttr "groupId55.id" "groupParts50.gi";
connectAttr "groupParts50.og" "groupParts51.ig";
connectAttr "groupId56.id" "groupParts51.gi";
connectAttr "polySeparate1.out[25]" "groupParts52.ig";
connectAttr "groupId57.id" "groupParts52.gi";
connectAttr "groupParts52.og" "groupParts53.ig";
connectAttr "groupId58.id" "groupParts53.gi";
connectAttr "polySeparate1.out[26]" "groupParts54.ig";
connectAttr "groupId59.id" "groupParts54.gi";
connectAttr "groupParts54.og" "groupParts55.ig";
connectAttr "groupId60.id" "groupParts55.gi";
connectAttr "polySeparate1.out[27]" "groupParts56.ig";
connectAttr "groupId61.id" "groupParts56.gi";
connectAttr "groupParts56.og" "groupParts57.ig";
connectAttr "groupId62.id" "groupParts57.gi";
connectAttr "polySeparate1.out[28]" "groupParts58.ig";
connectAttr "groupId63.id" "groupParts58.gi";
connectAttr "groupParts58.og" "groupParts59.ig";
connectAttr "groupId64.id" "groupParts59.gi";
connectAttr "polySeparate1.out[29]" "groupParts60.ig";
connectAttr "groupId65.id" "groupParts60.gi";
connectAttr "groupParts60.og" "groupParts61.ig";
connectAttr "groupId66.id" "groupParts61.gi";
connectAttr "polySeparate1.out[30]" "groupParts62.ig";
connectAttr "groupId67.id" "groupParts62.gi";
connectAttr "groupParts62.og" "groupParts63.ig";
connectAttr "groupId68.id" "groupParts63.gi";
connectAttr "polySeparate1.out[31]" "groupParts64.ig";
connectAttr "groupId69.id" "groupParts64.gi";
connectAttr "groupParts64.og" "groupParts65.ig";
connectAttr "groupId70.id" "groupParts65.gi";
connectAttr "polySeparate1.out[32]" "groupParts66.ig";
connectAttr "groupId71.id" "groupParts66.gi";
connectAttr "groupParts66.og" "groupParts67.ig";
connectAttr "groupId72.id" "groupParts67.gi";
connectAttr "polySeparate1.out[33]" "groupParts68.ig";
connectAttr "groupId73.id" "groupParts68.gi";
connectAttr "groupParts68.og" "groupParts69.ig";
connectAttr "groupId74.id" "groupParts69.gi";
connectAttr "polySeparate1.out[34]" "groupParts70.ig";
connectAttr "groupId75.id" "groupParts70.gi";
connectAttr "groupParts70.og" "groupParts71.ig";
connectAttr "groupId76.id" "groupParts71.gi";
connectAttr "polySeparate1.out[35]" "groupParts72.ig";
connectAttr "groupId77.id" "groupParts72.gi";
connectAttr "groupParts72.og" "groupParts73.ig";
connectAttr "groupId78.id" "groupParts73.gi";
connectAttr "polySeparate1.out[36]" "groupParts74.ig";
connectAttr "groupId79.id" "groupParts74.gi";
connectAttr "groupParts74.og" "groupParts75.ig";
connectAttr "groupId80.id" "groupParts75.gi";
connectAttr "polySeparate1.out[37]" "groupParts76.ig";
connectAttr "groupId81.id" "groupParts76.gi";
connectAttr "groupParts76.og" "groupParts77.ig";
connectAttr "groupId82.id" "groupParts77.gi";
connectAttr "polySeparate1.out[38]" "groupParts78.ig";
connectAttr "groupId83.id" "groupParts78.gi";
connectAttr "groupParts78.og" "groupParts79.ig";
connectAttr "groupId84.id" "groupParts79.gi";
connectAttr "polySeparate1.out[39]" "groupParts80.ig";
connectAttr "groupId85.id" "groupParts80.gi";
connectAttr "groupParts80.og" "groupParts81.ig";
connectAttr "groupId86.id" "groupParts81.gi";
connectAttr "polySeparate1.out[40]" "groupParts82.ig";
connectAttr "groupId87.id" "groupParts82.gi";
connectAttr "groupParts82.og" "groupParts83.ig";
connectAttr "groupId88.id" "groupParts83.gi";
connectAttr "polySeparate1.out[41]" "groupParts84.ig";
connectAttr "groupId89.id" "groupParts84.gi";
connectAttr "groupParts84.og" "groupParts85.ig";
connectAttr "groupId90.id" "groupParts85.gi";
connectAttr "polySeparate1.out[42]" "groupParts86.ig";
connectAttr "groupId91.id" "groupParts86.gi";
connectAttr "groupParts86.og" "groupParts87.ig";
connectAttr "groupId92.id" "groupParts87.gi";
connectAttr "polySeparate1.out[43]" "groupParts88.ig";
connectAttr "groupId93.id" "groupParts88.gi";
connectAttr "groupParts88.og" "groupParts89.ig";
connectAttr "groupId94.id" "groupParts89.gi";
connectAttr "polySeparate1.out[44]" "groupParts90.ig";
connectAttr "groupId95.id" "groupParts90.gi";
connectAttr "groupParts90.og" "groupParts91.ig";
connectAttr "groupId96.id" "groupParts91.gi";
connectAttr "groupParts33.og" "polyPlanarProj1.ip";
connectAttr "polySurfaceShape17.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyPlanarProj2.ip";
connectAttr "polySurfaceShape17.wm" "polyPlanarProj2.mp";
connectAttr "polySurfaceShape8.o" "polyUnite1.ip[0]";
connectAttr "polySurfaceShape10.o" "polyUnite1.ip[1]";
connectAttr "polySurfaceShape9.o" "polyUnite1.ip[2]";
connectAttr "polySurfaceShape46.o" "polyUnite1.ip[3]";
connectAttr "polySurfaceShape45.o" "polyUnite1.ip[4]";
connectAttr "polySurfaceShape44.o" "polyUnite1.ip[5]";
connectAttr "polySurfaceShape43.o" "polyUnite1.ip[6]";
connectAttr "polySurfaceShape42.o" "polyUnite1.ip[7]";
connectAttr "polySurfaceShape41.o" "polyUnite1.ip[8]";
connectAttr "polySurfaceShape40.o" "polyUnite1.ip[9]";
connectAttr "polySurfaceShape39.o" "polyUnite1.ip[10]";
connectAttr "polySurfaceShape38.o" "polyUnite1.ip[11]";
connectAttr "polySurfaceShape37.o" "polyUnite1.ip[12]";
connectAttr "polySurfaceShape36.o" "polyUnite1.ip[13]";
connectAttr "polySurfaceShape35.o" "polyUnite1.ip[14]";
connectAttr "polySurfaceShape34.o" "polyUnite1.ip[15]";
connectAttr "polySurfaceShape33.o" "polyUnite1.ip[16]";
connectAttr "polySurfaceShape32.o" "polyUnite1.ip[17]";
connectAttr "polySurfaceShape20.o" "polyUnite1.ip[18]";
connectAttr "polySurfaceShape16.o" "polyUnite1.ip[19]";
connectAttr "polySurfaceShape15.o" "polyUnite1.ip[20]";
connectAttr "polySurfaceShape14.o" "polyUnite1.ip[21]";
connectAttr "polySurfaceShape13.o" "polyUnite1.ip[22]";
connectAttr "polySurfaceShape12.o" "polyUnite1.ip[23]";
connectAttr "polySurfaceShape19.o" "polyUnite1.ip[24]";
connectAttr "polySurfaceShape18.o" "polyUnite1.ip[25]";
connectAttr "polySurfaceShape11.o" "polyUnite1.ip[26]";
connectAttr "polySurfaceShape7.o" "polyUnite1.ip[27]";
connectAttr "polySurfaceShape31.o" "polyUnite1.ip[28]";
connectAttr "polySurfaceShape30.o" "polyUnite1.ip[29]";
connectAttr "polySurfaceShape21.o" "polyUnite1.ip[30]";
connectAttr "polySurfaceShape25.o" "polyUnite1.ip[31]";
connectAttr "polySurfaceShape24.o" "polyUnite1.ip[32]";
connectAttr "polySurfaceShape23.o" "polyUnite1.ip[33]";
connectAttr "polySurfaceShape22.o" "polyUnite1.ip[34]";
connectAttr "polySurfaceShape29.o" "polyUnite1.ip[35]";
connectAttr "polySurfaceShape28.o" "polyUnite1.ip[36]";
connectAttr "polySurfaceShape27.o" "polyUnite1.ip[37]";
connectAttr "polySurfaceShape26.o" "polyUnite1.ip[38]";
connectAttr "polySurfaceShape8.wm" "polyUnite1.im[0]";
connectAttr "polySurfaceShape10.wm" "polyUnite1.im[1]";
connectAttr "polySurfaceShape9.wm" "polyUnite1.im[2]";
connectAttr "polySurfaceShape46.wm" "polyUnite1.im[3]";
connectAttr "polySurfaceShape45.wm" "polyUnite1.im[4]";
connectAttr "polySurfaceShape44.wm" "polyUnite1.im[5]";
connectAttr "polySurfaceShape43.wm" "polyUnite1.im[6]";
connectAttr "polySurfaceShape42.wm" "polyUnite1.im[7]";
connectAttr "polySurfaceShape41.wm" "polyUnite1.im[8]";
connectAttr "polySurfaceShape40.wm" "polyUnite1.im[9]";
connectAttr "polySurfaceShape39.wm" "polyUnite1.im[10]";
connectAttr "polySurfaceShape38.wm" "polyUnite1.im[11]";
connectAttr "polySurfaceShape37.wm" "polyUnite1.im[12]";
connectAttr "polySurfaceShape36.wm" "polyUnite1.im[13]";
connectAttr "polySurfaceShape35.wm" "polyUnite1.im[14]";
connectAttr "polySurfaceShape34.wm" "polyUnite1.im[15]";
connectAttr "polySurfaceShape33.wm" "polyUnite1.im[16]";
connectAttr "polySurfaceShape32.wm" "polyUnite1.im[17]";
connectAttr "polySurfaceShape20.wm" "polyUnite1.im[18]";
connectAttr "polySurfaceShape16.wm" "polyUnite1.im[19]";
connectAttr "polySurfaceShape15.wm" "polyUnite1.im[20]";
connectAttr "polySurfaceShape14.wm" "polyUnite1.im[21]";
connectAttr "polySurfaceShape13.wm" "polyUnite1.im[22]";
connectAttr "polySurfaceShape12.wm" "polyUnite1.im[23]";
connectAttr "polySurfaceShape19.wm" "polyUnite1.im[24]";
connectAttr "polySurfaceShape18.wm" "polyUnite1.im[25]";
connectAttr "polySurfaceShape11.wm" "polyUnite1.im[26]";
connectAttr "polySurfaceShape7.wm" "polyUnite1.im[27]";
connectAttr "polySurfaceShape31.wm" "polyUnite1.im[28]";
connectAttr "polySurfaceShape30.wm" "polyUnite1.im[29]";
connectAttr "polySurfaceShape21.wm" "polyUnite1.im[30]";
connectAttr "polySurfaceShape25.wm" "polyUnite1.im[31]";
connectAttr "polySurfaceShape24.wm" "polyUnite1.im[32]";
connectAttr "polySurfaceShape23.wm" "polyUnite1.im[33]";
connectAttr "polySurfaceShape22.wm" "polyUnite1.im[34]";
connectAttr "polySurfaceShape29.wm" "polyUnite1.im[35]";
connectAttr "polySurfaceShape28.wm" "polyUnite1.im[36]";
connectAttr "polySurfaceShape27.wm" "polyUnite1.im[37]";
connectAttr "polySurfaceShape26.wm" "polyUnite1.im[38]";
connectAttr "polyUnite1.out" "groupParts92.ig";
connectAttr "groupId97.id" "groupParts92.gi";
connectAttr "polySurfaceShape17.o" "polyUnite2.ip[0]";
connectAttr "polySurface7Shape.o" "polyUnite2.ip[1]";
connectAttr "polySurfaceShape17.wm" "polyUnite2.im[0]";
connectAttr "polySurface7Shape.wm" "polyUnite2.im[1]";
connectAttr "polyUnite2.out" "groupParts93.ig";
connectAttr "groupId98.id" "groupParts93.gi";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape19.wm" "polyExtrudeFace1.mp";
connectAttr "polyCube5.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape19.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyPlanarProj3.ip";
connectAttr "pCubeShape19.wm" "polyPlanarProj3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak3.ip";
connectAttr "polyPlanarProj3.out" "deleteComponent1.ig";
connectAttr "polySurfaceShape3.o" "polyUnite3.ip[0]";
connectAttr "polySurfaceShape4.o" "polyUnite3.ip[1]";
connectAttr "polySurfaceShape6.o" "polyUnite3.ip[2]";
connectAttr "polySurfaceShape2.o" "polyUnite3.ip[3]";
connectAttr "polySurface16Shape.o" "polyUnite3.ip[4]";
connectAttr "polySurfaceShape3.wm" "polyUnite3.im[0]";
connectAttr "polySurfaceShape4.wm" "polyUnite3.im[1]";
connectAttr "polySurfaceShape6.wm" "polyUnite3.im[2]";
connectAttr "polySurfaceShape2.wm" "polyUnite3.im[3]";
connectAttr "polySurface16Shape.wm" "polyUnite3.im[4]";
connectAttr "polyUnite3.out" "groupParts94.ig";
connectAttr "groupId99.id" "groupParts94.gi";
connectAttr "groupParts94.og" "groupParts95.ig";
connectAttr "groupId100.id" "groupParts95.gi";
connectAttr "polySurfaceShape5.o" "polyUnite4.ip[0]";
connectAttr "polySurface2Shape.o" "polyUnite4.ip[1]";
connectAttr "polySurfaceShape5.wm" "polyUnite4.im[0]";
connectAttr "polySurface2Shape.wm" "polyUnite4.im[1]";
connectAttr "polyUnite4.out" "groupParts96.ig";
connectAttr "groupId101.id" "groupParts96.gi";
connectAttr "groupParts96.og" "groupParts97.ig";
connectAttr "groupId102.id" "groupParts97.gi";
connectAttr "polyTweak4.out" "polyPlanarProj4.ip";
connectAttr "pCubeShape19.wm" "polyPlanarProj4.mp";
connectAttr "deleteComponent1.og" "polyTweak4.ip";
connectAttr "polyPlanarProj4.out" "polyPlanarProj5.ip";
connectAttr "pCubeShape19.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyPlanarProj6.ip";
connectAttr "pCubeShape19.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyFlipUV1.ip";
connectAttr "pCubeShape19.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyStraightenUVBorder1.ip";
connectAttr "polySoftEdge2.out" "polyPlanarProj7.ip";
connectAttr "pCylinderShape2.wm" "polyPlanarProj7.mp";
connectAttr "polyPlanarProj7.out" "polyAutoProj1.ip";
connectAttr "pCylinderShape2.wm" "polyAutoProj1.mp";
connectAttr "polySurface4Shape.o" "polySeparate2.ip";
connectAttr "polySeparate2.out[0]" "groupParts98.ig";
connectAttr "groupId103.id" "groupParts98.gi";
connectAttr "groupParts98.og" "groupParts99.ig";
connectAttr "groupId104.id" "groupParts99.gi";
connectAttr "polySeparate2.out[1]" "groupParts100.ig";
connectAttr "groupId105.id" "groupParts100.gi";
connectAttr "groupParts100.og" "groupParts101.ig";
connectAttr "groupId106.id" "groupParts101.gi";
connectAttr "polySeparate2.out[2]" "groupParts102.ig";
connectAttr "groupId107.id" "groupParts102.gi";
connectAttr "groupParts102.og" "groupParts103.ig";
connectAttr "groupId108.id" "groupParts103.gi";
connectAttr "polySeparate2.out[3]" "groupParts104.ig";
connectAttr "groupId109.id" "groupParts104.gi";
connectAttr "groupParts104.og" "groupParts105.ig";
connectAttr "groupId110.id" "groupParts105.gi";
connectAttr "polySeparate2.out[4]" "groupParts106.ig";
connectAttr "groupId111.id" "groupParts106.gi";
connectAttr "groupParts106.og" "groupParts107.ig";
connectAttr "groupId112.id" "groupParts107.gi";
connectAttr "polySeparate2.out[5]" "groupParts108.ig";
connectAttr "groupId113.id" "groupParts108.gi";
connectAttr "groupParts108.og" "groupParts109.ig";
connectAttr "groupId114.id" "groupParts109.gi";
connectAttr "polySeparate2.out[6]" "groupParts110.ig";
connectAttr "groupId115.id" "groupParts110.gi";
connectAttr "groupParts110.og" "groupParts111.ig";
connectAttr "groupId116.id" "groupParts111.gi";
connectAttr "polySeparate2.out[7]" "groupParts112.ig";
connectAttr "groupId117.id" "groupParts112.gi";
connectAttr "groupParts112.og" "groupParts113.ig";
connectAttr "groupId118.id" "groupParts113.gi";
connectAttr "polySeparate2.out[8]" "groupParts114.ig";
connectAttr "groupId119.id" "groupParts114.gi";
connectAttr "groupParts114.og" "groupParts115.ig";
connectAttr "groupId120.id" "groupParts115.gi";
connectAttr "polySeparate2.out[9]" "groupParts116.ig";
connectAttr "groupId121.id" "groupParts116.gi";
connectAttr "groupParts116.og" "groupParts117.ig";
connectAttr "groupId122.id" "groupParts117.gi";
connectAttr "polySeparate2.out[10]" "groupParts118.ig";
connectAttr "groupId123.id" "groupParts118.gi";
connectAttr "groupParts118.og" "groupParts119.ig";
connectAttr "groupId124.id" "groupParts119.gi";
connectAttr "polySeparate2.out[11]" "groupParts120.ig";
connectAttr "groupId125.id" "groupParts120.gi";
connectAttr "groupParts120.og" "groupParts121.ig";
connectAttr "groupId126.id" "groupParts121.gi";
connectAttr "polySeparate2.out[12]" "groupParts122.ig";
connectAttr "groupId127.id" "groupParts122.gi";
connectAttr "groupParts122.og" "groupParts123.ig";
connectAttr "groupId128.id" "groupParts123.gi";
connectAttr "polySeparate2.out[13]" "groupParts124.ig";
connectAttr "groupId129.id" "groupParts124.gi";
connectAttr "groupParts124.og" "groupParts125.ig";
connectAttr "groupId130.id" "groupParts125.gi";
connectAttr "polySeparate2.out[14]" "groupParts126.ig";
connectAttr "groupId131.id" "groupParts126.gi";
connectAttr "groupParts126.og" "groupParts127.ig";
connectAttr "groupId132.id" "groupParts127.gi";
connectAttr "polySeparate2.out[15]" "groupParts128.ig";
connectAttr "groupId133.id" "groupParts128.gi";
connectAttr "groupParts128.og" "groupParts129.ig";
connectAttr "groupId134.id" "groupParts129.gi";
connectAttr "polySeparate2.out[16]" "groupParts130.ig";
connectAttr "groupId135.id" "groupParts130.gi";
connectAttr "groupParts130.og" "groupParts131.ig";
connectAttr "groupId136.id" "groupParts131.gi";
connectAttr "polySeparate2.out[17]" "groupParts132.ig";
connectAttr "groupId137.id" "groupParts132.gi";
connectAttr "groupParts132.og" "groupParts133.ig";
connectAttr "groupId138.id" "groupParts133.gi";
connectAttr "polySeparate2.out[18]" "groupParts134.ig";
connectAttr "groupId139.id" "groupParts134.gi";
connectAttr "groupParts134.og" "groupParts135.ig";
connectAttr "groupId140.id" "groupParts135.gi";
connectAttr "polySeparate2.out[19]" "groupParts136.ig";
connectAttr "groupId141.id" "groupParts136.gi";
connectAttr "groupParts136.og" "groupParts137.ig";
connectAttr "groupId142.id" "groupParts137.gi";
connectAttr "polySeparate2.out[20]" "groupParts138.ig";
connectAttr "groupId143.id" "groupParts138.gi";
connectAttr "groupParts138.og" "groupParts139.ig";
connectAttr "groupId144.id" "groupParts139.gi";
connectAttr "polySeparate2.out[21]" "groupParts140.ig";
connectAttr "groupId145.id" "groupParts140.gi";
connectAttr "groupParts140.og" "groupParts141.ig";
connectAttr "groupId146.id" "groupParts141.gi";
connectAttr "polySeparate2.out[22]" "groupParts142.ig";
connectAttr "groupId147.id" "groupParts142.gi";
connectAttr "groupParts142.og" "groupParts143.ig";
connectAttr "groupId148.id" "groupParts143.gi";
connectAttr "polySeparate2.out[23]" "groupParts144.ig";
connectAttr "groupId149.id" "groupParts144.gi";
connectAttr "groupParts144.og" "groupParts145.ig";
connectAttr "groupId150.id" "groupParts145.gi";
connectAttr "polySeparate2.out[24]" "groupParts146.ig";
connectAttr "groupId151.id" "groupParts146.gi";
connectAttr "groupParts146.og" "groupParts147.ig";
connectAttr "groupId152.id" "groupParts147.gi";
connectAttr "polySeparate2.out[25]" "groupParts148.ig";
connectAttr "groupId153.id" "groupParts148.gi";
connectAttr "groupParts148.og" "groupParts149.ig";
connectAttr "groupId154.id" "groupParts149.gi";
connectAttr "polySeparate2.out[26]" "groupParts150.ig";
connectAttr "groupId155.id" "groupParts150.gi";
connectAttr "groupParts150.og" "groupParts151.ig";
connectAttr "groupId156.id" "groupParts151.gi";
connectAttr "polySeparate2.out[27]" "groupParts152.ig";
connectAttr "groupId157.id" "groupParts152.gi";
connectAttr "groupParts152.og" "groupParts153.ig";
connectAttr "groupId158.id" "groupParts153.gi";
connectAttr "polySeparate2.out[28]" "groupParts154.ig";
connectAttr "groupId159.id" "groupParts154.gi";
connectAttr "groupParts154.og" "groupParts155.ig";
connectAttr "groupId160.id" "groupParts155.gi";
connectAttr "polySeparate2.out[29]" "groupParts156.ig";
connectAttr "groupId161.id" "groupParts156.gi";
connectAttr "groupParts156.og" "groupParts157.ig";
connectAttr "groupId162.id" "groupParts157.gi";
connectAttr "polySeparate2.out[30]" "groupParts158.ig";
connectAttr "groupId163.id" "groupParts158.gi";
connectAttr "groupParts158.og" "groupParts159.ig";
connectAttr "groupId164.id" "groupParts159.gi";
connectAttr "polySeparate2.out[31]" "groupParts160.ig";
connectAttr "groupId165.id" "groupParts160.gi";
connectAttr "groupParts160.og" "groupParts161.ig";
connectAttr "groupId166.id" "groupParts161.gi";
connectAttr "polySeparate2.out[32]" "groupParts162.ig";
connectAttr "groupId167.id" "groupParts162.gi";
connectAttr "groupParts162.og" "groupParts163.ig";
connectAttr "groupId168.id" "groupParts163.gi";
connectAttr "polySeparate2.out[33]" "groupParts164.ig";
connectAttr "groupId169.id" "groupParts164.gi";
connectAttr "groupParts164.og" "groupParts165.ig";
connectAttr "groupId170.id" "groupParts165.gi";
connectAttr "polySeparate2.out[34]" "groupParts166.ig";
connectAttr "groupId171.id" "groupParts166.gi";
connectAttr "groupParts166.og" "groupParts167.ig";
connectAttr "groupId172.id" "groupParts167.gi";
connectAttr "polySeparate2.out[35]" "groupParts168.ig";
connectAttr "groupId173.id" "groupParts168.gi";
connectAttr "groupParts168.og" "groupParts169.ig";
connectAttr "groupId174.id" "groupParts169.gi";
connectAttr "polySeparate2.out[36]" "groupParts170.ig";
connectAttr "groupId175.id" "groupParts170.gi";
connectAttr "groupParts170.og" "groupParts171.ig";
connectAttr "groupId176.id" "groupParts171.gi";
connectAttr "polySeparate2.out[37]" "groupParts172.ig";
connectAttr "groupId177.id" "groupParts172.gi";
connectAttr "groupParts172.og" "groupParts173.ig";
connectAttr "groupId178.id" "groupParts173.gi";
connectAttr "polySeparate2.out[38]" "groupParts174.ig";
connectAttr "groupId179.id" "groupParts174.gi";
connectAttr "groupParts174.og" "groupParts175.ig";
connectAttr "groupId180.id" "groupParts175.gi";
connectAttr "polySeparate2.out[39]" "groupParts176.ig";
connectAttr "groupId181.id" "groupParts176.gi";
connectAttr "groupParts176.og" "groupParts177.ig";
connectAttr "groupId182.id" "groupParts177.gi";
connectAttr "polySeparate2.out[40]" "groupParts178.ig";
connectAttr "groupId183.id" "groupParts178.gi";
connectAttr "groupParts178.og" "groupParts179.ig";
connectAttr "groupId184.id" "groupParts179.gi";
connectAttr "polySeparate2.out[41]" "groupParts180.ig";
connectAttr "groupId185.id" "groupParts180.gi";
connectAttr "groupParts180.og" "groupParts181.ig";
connectAttr "groupId186.id" "groupParts181.gi";
connectAttr "polySeparate2.out[42]" "groupParts182.ig";
connectAttr "groupId187.id" "groupParts182.gi";
connectAttr "groupParts182.og" "groupParts183.ig";
connectAttr "groupId188.id" "groupParts183.gi";
connectAttr "polySeparate2.out[43]" "groupParts184.ig";
connectAttr "groupId189.id" "groupParts184.gi";
connectAttr "groupParts184.og" "groupParts185.ig";
connectAttr "groupId190.id" "groupParts185.gi";
connectAttr "polySeparate2.out[44]" "groupParts186.ig";
connectAttr "groupId191.id" "groupParts186.gi";
connectAttr "groupParts186.og" "groupParts187.ig";
connectAttr "groupId192.id" "groupParts187.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCube19Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape3.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape7.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape8.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape9.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape10.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape11.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape12.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape13.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape14.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape15.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape16.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape17.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape18.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape19.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape20.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape21.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape22.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape23.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape24.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape25.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape26.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape27.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape28.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape29.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape30.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape31.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape32.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape33.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape34.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape35.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape37.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape38.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape39.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape40.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape41.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape42.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape43.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape44.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape45.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape46.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface16Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface2Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurface4Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape47.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape48.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape49.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape50.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape51.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape52.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape53.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape54.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape55.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape56.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape57.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape58.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape59.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape60.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape61.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape62.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape63.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape64.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape65.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape66.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape67.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape68.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape69.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape70.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape71.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape72.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape73.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape74.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape75.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape76.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape77.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape78.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape79.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape80.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape81.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape82.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape83.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape84.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape85.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape86.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape87.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape88.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape89.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape90.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "|polySurface4|polySurface90|polySurfaceShape91.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|polySurface4|polySurface91|polySurfaceShape91.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "polySurfaceShape92.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape93.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape94.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId14.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId16.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId18.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId20.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId22.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId24.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId26.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId28.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId30.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId32.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId34.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId36.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId38.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId40.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId42.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId44.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId46.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId48.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId50.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId52.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId54.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId56.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId58.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId60.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId62.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId64.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId66.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId68.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId70.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId72.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId74.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId76.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId78.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId80.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId82.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId84.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId86.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId88.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId90.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId92.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId94.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId96.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId97.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId98.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId100.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId101.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId103.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId105.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId107.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId109.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId111.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId113.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId115.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId117.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId119.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId121.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId123.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId125.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId127.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId129.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId131.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId133.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId135.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId137.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId139.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId141.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId143.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId145.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId147.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId149.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId151.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId153.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId155.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId157.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId159.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId161.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId163.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId165.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId167.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId169.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId171.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId173.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId175.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId177.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId179.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId181.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId183.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId185.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId187.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId189.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId191.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId193.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId195.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId197.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId199.msg" ":initialShadingGroup.gn" -na;
// End of Making_My_Way_Down_Town3.ma
