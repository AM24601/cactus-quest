//Maya ASCII 2017 scene
//Name: scene2.ma
//Last modified: Sat, Sep 17, 2016 01:29:36 PM
//Codeset: 1252
requires maya "2017";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "8C9A702E-4A25-E321-C6BF-A0A7165F1EA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1253.7302759098332 -761.85101142332064 915.80302750236228 ;
	setAttr ".r" -type "double3" 54.082924915629746 0 13331.994910498095 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D54102B7-4615-DD35-5E2F-EEAFEF4B3B81";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1084.769976859553;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -200 0 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "49D2A323-4D5D-CA14-B1FF-A193FBE60DBF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 567.133208210985 329.6831226488095 1292.4207738425698 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "74CFCAA5-4EE9-DC2B-4118-019EC0822F4B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1292.4207738425698;
	setAttr ".ow" 1920.6367611765663;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 500.00000000000006 0 0 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "3E8FAFD9-4B95-A61B-9B6B-C68BB06C0E1C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 914.56300199415625 -1000.1000000000003 129.52175837791174 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "05574B54-4206-1F4F-61DC-34A1E67DA07A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4864.9877895846903;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "C8D6090B-4386-2CA8-4378-B8A6EDE7522E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1000000000001 384.67214244850106 200.11656362890901 ;
	setAttr ".r" -type "double3" 90 1.2722218725854067e-014 89.999999999999986 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "1AAD4DEF-48F8-AFAA-8086-C78D0A329991";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 962.0524769778641;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube23";
	rename -uid "C9AAB5E5-4C00-22CC-242D-8DAA6525175C";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 399.99999745686853 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "1D54CD3D-4D93-B10D-81F9-4B85F6A84164";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr -av ".iog[0].og[0].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube23";
	rename -uid "C4D7CED8-45FF-9F80-4E59-8E9C067D70A6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube25";
	rename -uid "2342A84D-40B1-0DD1-9381-9D83D5B92BF2";
	setAttr ".t" -type "double3" -200 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape25" -p "pCube25";
	rename -uid "C3F882A4-4619-562A-CA46-FA875C9B3E31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube26";
	rename -uid "5B9CC957-4B72-1026-9709-CABB465C39ED";
	setAttr ".t" -type "double3" 200 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape26" -p "pCube26";
	rename -uid "ED3B155A-49DD-A53C-26F0-43A87E942FB6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube27";
	rename -uid "CB5C42F4-49B5-FE0D-4199-EBBD9CEF13FA";
	setAttr ".t" -type "double3" -200 -200.37467465211478 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape27" -p "pCube27";
	rename -uid "72E0339E-4906-840D-DA69-478E3C4EE628";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube28";
	rename -uid "1F458384-4EA7-64EC-9C47-00BEE607A5A9";
	setAttr ".t" -type "double3" 200 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape28" -p "pCube28";
	rename -uid "85083C01-40A2-721C-D39E-F3913020627A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube29";
	rename -uid "4CC3EF9A-497D-9849-16C4-EDBFBF8FA00A";
	setAttr ".t" -type "double3" -200 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape29" -p "pCube29";
	rename -uid "1F1BA746-4EF7-74D4-2748-759CD551158A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube30";
	rename -uid "6D7D19D5-4262-212D-5B9E-F380269AE3D5";
	setAttr ".t" -type "double3" 200 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape30" -p "pCube30";
	rename -uid "BEC8F1D1-43B7-4195-481C-26AC734671E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "pCube30";
	rename -uid "1366D246-4DA4-6BDF-9B06-D1B477CD55D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube31";
	rename -uid "CCEA3AAD-40FA-E360-8AF0-A29DC19EFA91";
	setAttr ".t" -type "double3" -200 400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape31" -p "pCube31";
	rename -uid "66BC1F02-47B7-631F-560A-6C96ED0116D0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube32";
	rename -uid "535EA409-4C5C-D4F6-1629-089663BE6F5C";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -2.5431314725210541e-006 
		-0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape32" -p "pCube32";
	rename -uid "F70DB8EC-4CC7-35E5-5C40-7EACFF0967A3";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube32";
	rename -uid "70C7550F-4C99-8B9B-9A42-0082D19D64AE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube33";
	rename -uid "AAC46097-482B-9576-98B3-13B9D84B164F";
	setAttr ".t" -type "double3" -200 100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape33" -p "pCube33";
	rename -uid "7A019566-4105-FC57-2F58-3BA60ECF1DC3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube34";
	rename -uid "7429FB78-4973-F7FC-2DC2-C8995501157E";
	setAttr ".t" -type "double3" 300.00001217297995 200.00000000000045 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
	setAttr ".rpt" -type "double3" -100.00001217297996 -100.00001217298063 0 ;
	setAttr ".sp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
createNode mesh -n "pCubeShape34" -p "pCube34";
	rename -uid "F365D67B-47FB-D6D1-4143-95BB0E6C662D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape3" -p "pCube34";
	rename -uid "84593712-49C2-EE98-E2FC-C185A6B3BE8D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[0:5]" -type "float3"  0 0 88.75 0 0 88.75 0 0 -11.25 
		0 0 -11.25 0 0 0 0 0 0;
	setAttr -s 4 ".vt[0:3]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube35";
	rename -uid "6AAD1C66-4E1D-7A52-25ED-829E4F763DC7";
	setAttr ".t" -type "double3" -200 -100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape35" -p "pCube35";
	rename -uid "0C0F421D-4590-997E-FE69-F49F2B719631";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube36";
	rename -uid "D0B99DA6-46EE-FC76-EC18-1EA822E9A71C";
	setAttr ".t" -type "double3" 200 -100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape36" -p "pCube36";
	rename -uid "07451007-43E5-9250-CE74-3983D10DAD8E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube37";
	rename -uid "E82024EB-4E50-04B4-06A5-A299719EFB62";
	setAttr ".t" -type "double3" -200 300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape37" -p "pCube37";
	rename -uid "960B7A2B-4B07-7C2D-2DDE-EC8F81C832FF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube38";
	rename -uid "90B8DBA8-4DFD-B8A0-55D7-6B80D0C7768F";
	setAttr ".t" -type "double3" -200 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape38" -p "pCube38";
	rename -uid "864F83D8-4C03-DAB2-3CA5-DAA091E90FA7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube39";
	rename -uid "6292D38B-4BFE-3D81-626F-E8B292F91D1E";
	setAttr ".t" -type "double3" 0 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape39" -p "pCube39";
	rename -uid "E58EA463-4EF3-D3C7-E9AC-41B78AADAA11";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube40";
	rename -uid "8E4A4334-4D34-C2E8-05C1-4FBE326B8B7A";
	setAttr ".t" -type "double3" 200 600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape40" -p "pCube40";
	rename -uid "0BB755A4-413D-55B1-7B3A-CCA75380619B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube41";
	rename -uid "70036698-4E1B-D496-3762-5A80DDAC26E7";
	setAttr ".t" -type "double3" -200 500.00000000000006 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape41" -p "pCube41";
	rename -uid "C0AC5543-437C-8A58-31F9-7BB85E1352A9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube42";
	rename -uid "AA6328FE-42D5-1F21-5A02-FB91222A2334";
	setAttr ".t" -type "double3" -100 600 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape42" -p "pCube42";
	rename -uid "F54E3ED2-4BF6-CF8F-F1A6-88B93DC6493B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube43";
	rename -uid "BC03C7F2-4F58-024C-F621-3C9F0C0FCBEC";
	setAttr ".t" -type "double3" 100 600 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape43" -p "pCube43";
	rename -uid "02802235-40FE-75DF-04F6-43A72313584D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube44";
	rename -uid "D5397314-4D81-ECB2-B399-738F6D1990B6";
	setAttr ".t" -type "double3" 399.99999745686853 399.99999745686853 -0.5 ;
	setAttr ".r" -type "double3" 0 -35 0 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape44" -p "pCube44";
	rename -uid "BA4C526C-4052-2708-0A5C-CCB266218491";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 2 "e[0:2]" "e[5]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "e[3:4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 6 ".uvst[0].uvsp[0:5]" -type "float2" 0.375 0 0.375 0.25
		 0.5 0.25 0.5 0 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[2]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr ".pt[3]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr ".pt[4]" -type "float3" 17.745449 0 -32.771744 ;
	setAttr -s 6 ".vt[0:5]"  -200 -200 0.5 -200 200 0.5 3.44259357 200 0.47166538
		 3.44259357 -200 0.47166538 3.44259357 3.9790393e-013 0.47166538 -200 0 0.5;
	setAttr -s 7 ".ed[0:6]"  0 3 0 1 2 0 0 5 0 2 4 0 4 3 0 5 1 0 4 5 1;
	setAttr -s 2 -ch 8 ".fc[0:1]" -type "polyFaces" 
		f 4 0 -5 6 -3
		mu 0 4 0 3 4 5
		f 4 -7 -4 -2 -6
		mu 0 4 5 4 2 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube44";
	rename -uid "49CD582F-4E31-80FE-C95A-F7ADC13CC1A2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube47";
	rename -uid "27521F4B-4478-4DEE-B6D0-8799C0F0E77A";
	setAttr ".t" -type "double3" 400 600.00000000000023 108.2499999999999 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape47" -p "pCube47";
	rename -uid "48FC8B69-4E83-35CB-B692-B5A86E5B9C75";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube48";
	rename -uid "C65C8EDA-4C17-B456-F641-0AA5DECE5FA3";
	setAttr ".t" -type "double3" 400 200 108.2499999999999 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape48" -p "pCube48";
	rename -uid "7DFC00C3-46DC-CC9F-9CC9-1A9831406D63";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube49";
	rename -uid "8384BC55-4385-927A-3F01-76BA876A9DEC";
	setAttr ".t" -type "double3" 300 200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "polySurfaceShape2" -p "pCube49";
	rename -uid "454DE9F3-41F5-39FA-9737-6BB4BA3AC1C0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform1" -p "pCube49";
	rename -uid "20667E40-407D-792F-2236-F9B61923EE91";
	setAttr ".v" no;
createNode mesh -n "pCubeShape49" -p "transform1";
	rename -uid "8BFF920C-4084-06C5-0395-5D96B07BA5C8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59375 0.8125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.4375 0.5 0.5 0.5
		 0.75 0.5 0.8125 0.5625 0 0.5625 1 0.5625 0.25 0.5625 0.4375 0.5625 0.5 0.5625 0.75
		 0.5625 0.8125 0.625 0.90625 0.71875 0 0.5625 0.90625 0.5 0.90625 0.28125 0 0.375
		 0.90625 0.28125 0.25 0.375 0.34375 0.5 0.34375 0.5625 0.34375 0.625 0.34375 0.71875
		 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[31]" -type "float3" 0 0 -5.4166679 ;
	setAttr -s 32 ".vt[0:31]"  -16.5 -16.5 25.083335876 16.5 -16.5 25.083335876
		 -16.5 16.5 25.083335876 16.5 16.5 25.083335876 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5
		 16.5 16.5 -16.5 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25
		 -6.45272827 -16.5 25.083335876 -6.45272827 16.5 25.083335876 -6.45272827 16.5 -8.25
		 -6.45272827 16.5 -16.5 -6.45272827 -16.5 -16.5 -6.45272827 -16.5 -8.25 7.5 -16.5 25.083335876
		 7.5 16.5 25.083335876 7.5 16.5 -8.25 7.5 16.5 -16.5 7.5 -16.5 -16.5 7.5 -16.5 -8.25
		 16.5 -16.5 8.41666794 7.5 -16.5 8.41666794 -6.45272827 -16.5 8.41666794 -16.5 -16.5 8.41666794
		 -16.5 16.5 8.41666794 -6.45272827 16.5 8.41666794 7.5 16.5 8.41666794 16.5 16.5 8.41666794;
	setAttr -s 60 ".ed[0:59]"  0 12 0 2 13 0 4 14 1 6 15 0 8 16 0 10 17 1
		 0 2 0 1 3 0 3 31 0 2 28 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 24 0 10 27 0 11 5 1
		 10 4 1 12 18 0 13 19 0 12 13 1 14 20 1 13 29 1 15 21 0 14 15 1 16 22 0 15 16 1 17 23 1
		 16 17 1 17 26 1 18 1 0 19 3 0 18 19 1 20 5 1 19 30 1 21 7 0 20 21 1 22 9 0 21 22 1
		 23 11 1 22 23 1 23 25 1 24 1 0 25 18 1 24 25 1 26 12 1 25 26 1 27 0 0 26 27 1 28 4 0
		 27 28 1 29 14 1 28 29 1 30 20 1 29 30 1 31 5 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 22 -2 -7
		mu 0 4 0 22 24 2
		f 4 -3 -52 54 53
		mu 0 4 25 4 43 44
		f 4 2 26 -4 -11
		mu 0 4 4 25 26 6
		f 4 3 28 -5 -13
		mu 0 4 6 26 27 8
		f 4 4 30 -6 -15
		mu 0 4 8 27 28 10
		f 4 31 50 -18 5
		mu 0 4 28 39 41 10
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -58 59 -17 18
		mu 0 4 17 47 37 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 51 -20 17 52
		mu 0 4 42 21 19 40
		f 4 20 34 -22 -23
		mu 0 4 22 29 31 24
		f 4 -24 -54 56 55
		mu 0 4 32 25 44 45
		f 4 -27 23 38 -26
		mu 0 4 26 25 32 33
		f 4 -29 25 40 -28
		mu 0 4 27 26 33 34
		f 4 -31 27 42 -30
		mu 0 4 28 27 34 35
		f 4 43 48 -32 29
		mu 0 4 35 38 39 28
		f 4 32 7 -34 -35
		mu 0 4 29 1 3 31
		f 4 -36 -56 58 57
		mu 0 4 5 32 45 46
		f 4 -39 35 11 -38
		mu 0 4 33 32 5 7
		f 4 -41 37 13 -40
		mu 0 4 34 33 7 9
		f 4 -43 39 15 -42
		mu 0 4 35 34 9 11
		f 4 46 -44 41 16
		mu 0 4 36 38 35 11
		f 4 -33 -46 -47 44
		mu 0 4 13 30 38 36
		f 4 -49 45 -21 -48
		mu 0 4 39 38 30 23
		f 4 -51 47 -1 -50
		mu 0 4 41 39 23 12
		f 4 9 -53 49 6
		mu 0 4 2 42 40 0
		f 4 -55 -10 1 24
		mu 0 4 44 43 2 24
		f 4 -57 -25 21 36
		mu 0 4 45 44 24 31
		f 4 -59 -37 33 8
		mu 0 4 46 45 31 3
		f 4 -60 -9 -8 -45
		mu 0 4 37 47 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube50";
	rename -uid "F65AE577-410B-CE90-4F4B-C2B562C86782";
	setAttr ".t" -type "double3" 300 0 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "polySurfaceShape2" -p "pCube50";
	rename -uid "F09072D3-4516-322B-F735-519CBF7D8D20";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[1]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[2]" -type "float3" 0 0 8.5833359 ;
	setAttr ".pt[3]" -type "float3" 0 0 8.5833359 ;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform2" -p "pCube50";
	rename -uid "B22A5369-4E55-65F0-F271-74A148E38C98";
	setAttr ".v" no;
createNode mesh -n "pCubeShape50" -p "transform2";
	rename -uid "E35C2843-4C92-9754-E412-5492FCAD8726";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59375 0.8125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.4375 0.5 0.5 0.5
		 0.75 0.5 0.8125 0.5625 0 0.5625 1 0.5625 0.25 0.5625 0.4375 0.5625 0.5 0.5625 0.75
		 0.5625 0.8125 0.625 0.90625 0.71875 0 0.5625 0.90625 0.5 0.90625 0.28125 0 0.375
		 0.90625 0.28125 0.25 0.375 0.34375 0.5 0.34375 0.5625 0.34375 0.625 0.34375 0.71875
		 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".pt[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".pt[31]" -type "float3" 0 0 -5.4166679 ;
	setAttr -s 32 ".vt[0:31]"  -16.5 -16.5 25.083335876 16.5 -16.5 25.083335876
		 -16.5 16.5 25.083335876 16.5 16.5 25.083335876 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5
		 16.5 16.5 -16.5 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25
		 -6.45272827 -16.5 25.083335876 -6.45272827 16.5 25.083335876 -6.45272827 16.5 -8.25
		 -6.45272827 16.5 -16.5 -6.45272827 -16.5 -16.5 -6.45272827 -16.5 -8.25 7.5 -16.5 25.083335876
		 7.5 16.5 25.083335876 7.5 16.5 -8.25 7.5 16.5 -16.5 7.5 -16.5 -16.5 7.5 -16.5 -8.25
		 16.5 -16.5 8.41666794 7.5 -16.5 8.41666794 -6.45272827 -16.5 8.41666794 -16.5 -16.5 8.41666794
		 -16.5 16.5 8.41666794 -6.45272827 16.5 8.41666794 7.5 16.5 8.41666794 16.5 16.5 8.41666794;
	setAttr -s 60 ".ed[0:59]"  0 12 0 2 13 0 4 14 1 6 15 0 8 16 0 10 17 1
		 0 2 0 1 3 0 3 31 0 2 28 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 24 0 10 27 0 11 5 1
		 10 4 1 12 18 0 13 19 0 12 13 1 14 20 1 13 29 1 15 21 0 14 15 1 16 22 0 15 16 1 17 23 1
		 16 17 1 17 26 1 18 1 0 19 3 0 18 19 1 20 5 1 19 30 1 21 7 0 20 21 1 22 9 0 21 22 1
		 23 11 1 22 23 1 23 25 1 24 1 0 25 18 1 24 25 1 26 12 1 25 26 1 27 0 0 26 27 1 28 4 0
		 27 28 1 29 14 1 28 29 1 30 20 1 29 30 1 31 5 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 22 -2 -7
		mu 0 4 0 22 24 2
		f 4 -3 -52 54 53
		mu 0 4 25 4 43 44
		f 4 2 26 -4 -11
		mu 0 4 4 25 26 6
		f 4 3 28 -5 -13
		mu 0 4 6 26 27 8
		f 4 4 30 -6 -15
		mu 0 4 8 27 28 10
		f 4 31 50 -18 5
		mu 0 4 28 39 41 10
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -58 59 -17 18
		mu 0 4 17 47 37 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 51 -20 17 52
		mu 0 4 42 21 19 40
		f 4 20 34 -22 -23
		mu 0 4 22 29 31 24
		f 4 -24 -54 56 55
		mu 0 4 32 25 44 45
		f 4 -27 23 38 -26
		mu 0 4 26 25 32 33
		f 4 -29 25 40 -28
		mu 0 4 27 26 33 34
		f 4 -31 27 42 -30
		mu 0 4 28 27 34 35
		f 4 43 48 -32 29
		mu 0 4 35 38 39 28
		f 4 32 7 -34 -35
		mu 0 4 29 1 3 31
		f 4 -36 -56 58 57
		mu 0 4 5 32 45 46
		f 4 -39 35 11 -38
		mu 0 4 33 32 5 7
		f 4 -41 37 13 -40
		mu 0 4 34 33 7 9
		f 4 -43 39 15 -42
		mu 0 4 35 34 9 11
		f 4 46 -44 41 16
		mu 0 4 36 38 35 11
		f 4 -33 -46 -47 44
		mu 0 4 13 30 38 36
		f 4 -49 45 -21 -48
		mu 0 4 39 38 30 23
		f 4 -51 47 -1 -50
		mu 0 4 41 39 23 12
		f 4 9 -53 49 6
		mu 0 4 2 42 40 0
		f 4 -55 -10 1 24
		mu 0 4 44 43 2 24
		f 4 -57 -25 21 36
		mu 0 4 45 44 24 31
		f 4 -59 -37 33 8
		mu 0 4 46 45 31 3
		f 4 -60 -9 -8 -45
		mu 0 4 37 47 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube51";
	rename -uid "EDED6133-4F7F-2197-A0F1-54AB6D29D4AC";
	setAttr ".t" -type "double3" 599.99999745686853 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape51" -p "pCube51";
	rename -uid "8AD2B6BC-454A-E0B8-2130-2D9FBDA4D271";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube51";
	rename -uid "DC09B2C0-4B34-5D6B-366B-3780D47807B4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube52";
	rename -uid "AEB8353F-4710-400E-9E46-1F87EE5A88AE";
	setAttr ".t" -type "double3" 200 100 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape52" -p "pCube52";
	rename -uid "B1FECF68-4E5C-762B-14D2-0897DD0B6FEA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube53";
	rename -uid "D5351627-4412-327B-F073-43B1E75B2F1C";
	setAttr ".t" -type "double3" 300.00001217297995 600.00000000000045 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
	setAttr ".rpt" -type "double3" -100.00001217297996 -100.00001217298063 0 ;
	setAttr ".sp" -type "double3" -2.2204463195447705e-014 100.00001217298018 -3.75 ;
createNode mesh -n "pCubeShape53" -p "pCube53";
	rename -uid "11E6ABEE-4BDB-A7D7-6F97-2286C1BF8E77";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 16 ".uvst[0].uvsp[0:15]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.375 0 0.625 0 0.625 0.25
		 0.375 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 96.25 7.5 -100 96.25 -7.5 100 -3.75
		 7.5 100 -3.75 -7.5 -100 96.25 7.5 -100 96.25 7.5 100 -3.75 -7.5 100 -3.75 -7.5 -93.29180908 109.66641235
		 7.5 -93.29180908 109.66641235 7.5 106.70819092 9.66640759 -7.5 106.70819092 9.66640759;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 0 2 0 1 3 0 0 4 0 1 5 0 4 5 0
		 3 6 0 5 6 0 2 7 0 7 6 0 4 7 0 4 8 0 5 9 0 8 9 0 6 10 0 9 10 0 7 11 0 11 10 0 8 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 14 16 -19 -20
		mu 0 4 12 13 14 15
		f 4 2 1 -4 -1
		mu 0 4 4 7 6 5
		f 4 0 5 -7 -5
		mu 0 4 0 1 9 8
		f 4 3 7 -9 -6
		mu 0 4 1 3 10 9
		f 4 -2 9 10 -8
		mu 0 4 3 2 11 10
		f 4 -3 4 11 -10
		mu 0 4 2 0 8 11
		f 4 6 13 -15 -13
		mu 0 4 8 9 13 12
		f 4 8 15 -17 -14
		mu 0 4 9 10 14 13
		f 4 -11 17 18 -16
		mu 0 4 10 11 15 14
		f 4 -12 12 19 -18
		mu 0 4 11 8 12 15;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape3" -p "pCube53";
	rename -uid "CF8D34A3-4F47-3A1C-17D1-BFB26D922545";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[0:5]" -type "float3"  0 0 88.75 0 0 88.75 0 0 -11.25 
		0 0 -11.25 0 0 0 0 0 0;
	setAttr -s 4 ".vt[0:3]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube54";
	rename -uid "1465DDC3-49A0-01B1-3F92-B0A6F528C5B5";
	setAttr ".t" -type "double3" 500 600 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape54" -p "pCube54";
	rename -uid "42353868-4BD2-E752-C01F-2CAB2A6F1E4C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube55";
	rename -uid "9A4D9D7C-48B8-3598-078E-CEBC4075845F";
	setAttr ".t" -type "double3" 500 200 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape55" -p "pCube55";
	rename -uid "35F1D2D2-4C35-E135-38DD-FBBFFF9EB28E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube56";
	rename -uid "F2799103-418A-53A5-70CD-CB830F9EE713";
	setAttr ".t" -type "double3" 799.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape56" -p "pCube56";
	rename -uid "B6560EFD-4EEA-715B-90E7-4F8B87A10BA7";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 600 0 0 600 0 0 600 0 0 
		600 0;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube56";
	rename -uid "12C20FF5-47D3-0A14-C1AD-B2B43F40AF9D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube57";
	rename -uid "84A7C9DC-4855-1B2E-2E1C-A5B7BCFD2324";
	setAttr ".t" -type "double3" 799.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape57" -p "pCube57";
	rename -uid "DF62DEFD-4B09-B929-1145-3FA8430B0002";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube57";
	rename -uid "A2019BAF-47E2-05F6-AC3F-E3ABF77AC179";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube58";
	rename -uid "1D605A42-465C-6879-2002-B8993DF4478B";
	setAttr ".t" -type "double3" 999.99999745686853 199.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape58" -p "pCube58";
	rename -uid "23912E8D-4207-5255-6DD3-05996722C73F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube58";
	rename -uid "40569976-4DC2-3BB9-9EAF-A89CC7CDE176";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube59";
	rename -uid "677F1B8C-4151-C1B4-9EF2-A2B59A6268CC";
	setAttr ".t" -type "double3" 999.99999745686853 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape59" -p "pCube59";
	rename -uid "BE9195EF-4AD8-C2BB-3FCA-22882B2D11DB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube59";
	rename -uid "E950CF3A-4C6E-61C3-72A9-6DA1319EC63D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube60";
	rename -uid "5F2D7326-459A-8E68-026F-05B74B3C563B";
	setAttr ".t" -type "double3" 999.99999745686853 599.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape60" -p "pCube60";
	rename -uid "E7481C46-485B-033B-8EE5-9CAB4B712BD0";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube60";
	rename -uid "71FF8E3B-42BB-56CE-4008-3D80B0ACF8AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube61";
	rename -uid "43ED6C75-4145-1353-E007-6DBD950C00B5";
	setAttr ".t" -type "double3" 999.99999745686853 799.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape61" -p "pCube61";
	rename -uid "32F87ED1-458E-3EA3-558A-FEA991C4CE8A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube61";
	rename -uid "6203D8F9-4326-C920-FE79-0AA7C12CBF5C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube62";
	rename -uid "AEAFC6FB-46B2-BCD8-B5B9-97BC99C462CE";
	setAttr ".t" -type "double3" 600 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape62" -p "pCube62";
	rename -uid "3C8FF7D1-4362-27C7-6CA1-549627E8C16D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube63";
	rename -uid "8EE060AE-4D35-E63D-B0DC-77909952035B";
	setAttr ".t" -type "double3" 600 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape63" -p "pCube63";
	rename -uid "433D9727-4AB8-56C2-5829-F5BEB4508D54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube64";
	rename -uid "1944D513-4382-854B-60A2-AB917026265A";
	setAttr ".t" -type "double3" 600 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape64" -p "pCube64";
	rename -uid "2C813450-4BCC-8E50-B57D-1FA2E0D81413";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube65";
	rename -uid "9BBF90AF-42A9-9D92-8DE5-FBB6165352FF";
	setAttr ".t" -type "double3" 600 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape65" -p "pCube65";
	rename -uid "C49E06A8-43DE-5B18-AC0B-44807B913215";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube66";
	rename -uid "206AAE98-4CE0-84B5-88F3-F2BF0BEADF43";
	setAttr ".t" -type "double3" 1000 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape66" -p "pCube66";
	rename -uid "2F8965C4-48A9-9459-F2D6-E0A41BA75181";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube67";
	rename -uid "49C78F79-4AF4-579E-8509-6E89438A2E88";
	setAttr ".t" -type "double3" 800 800 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape67" -p "pCube67";
	rename -uid "0FB48533-4785-487E-15DF-2BA1A4374E14";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube68";
	rename -uid "41316515-40AA-7E9C-3353-32B7AD8ABBB8";
	setAttr ".t" -type "double3" 800 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape68" -p "pCube68";
	rename -uid "2E42B045-4D66-3741-95A1-F7B08751EA47";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube69";
	rename -uid "C9AD7D4B-410E-1B8E-20AE-FB8E67FD0237";
	setAttr ".t" -type "double3" 1000 0 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape69" -p "pCube69";
	rename -uid "AD0BB4BE-41E6-4934-2ED7-6B87CB74649B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube70";
	rename -uid "0A697A83-4060-9187-1078-EA81855185EF";
	setAttr ".t" -type "double3" 600 100 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape70" -p "pCube70";
	rename -uid "3C4D4C86-4A0A-EF0C-7395-F5B21FB2E9D8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube71";
	rename -uid "80AA484A-4579-7B58-68C3-EAA847F54448";
	setAttr ".t" -type "double3" 600 700 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape71" -p "pCube71";
	rename -uid "86D55959-4423-EE15-2E08-68A0F1BEB81F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube72";
	rename -uid "EA8B5758-4A2A-E1C8-9588-539E97B1AF87";
	setAttr ".t" -type "double3" 900 800 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape72" -p "pCube72";
	rename -uid "085AE877-443E-756F-1116-D997AD65C7D9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube73";
	rename -uid "4BD7B4DE-4FB5-97CD-FE97-4284FE6D36BF";
	setAttr ".t" -type "double3" 700 800 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape73" -p "pCube73";
	rename -uid "F387FB5B-48FA-281F-3168-8F99CCC5CBC3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube74";
	rename -uid "446EEBF7-447B-3351-F5AB-95BF7313354D";
	setAttr ".t" -type "double3" 700 0 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape74" -p "pCube74";
	rename -uid "E8191174-435F-0895-C930-7389728B5C2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube75";
	rename -uid "18A69D26-459E-C8F7-D0C2-27A6414754C7";
	setAttr ".t" -type "double3" 900 0 103.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape75" -p "pCube75";
	rename -uid "92B569A1-4D75-FDA8-18B9-86B40A304966";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube76";
	rename -uid "4A802615-47C3-919D-1A8A-BD8A3903C1FA";
	setAttr ".t" -type "double3" 1199.9999974568686 399.99999745686853 99.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape76" -p "pCube76";
	rename -uid "92CBC984-4E9F-AF8D-8970-C9883FD8A779";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube76";
	rename -uid "DDCF9EA9-461D-B866-0F91-258E3DA6F3D0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube77";
	rename -uid "588D3056-4584-0E0F-DE8D-089159539B6D";
	setAttr ".t" -type "double3" 1000 200 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape77" -p "pCube77";
	rename -uid "FA3216C3-47BC-68D7-7E72-0C9A53756B83";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube78";
	rename -uid "E18F031A-4E15-5AE5-DD59-1CB5CD2FBE70";
	setAttr ".t" -type "double3" 1000 600 108.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape78" -p "pCube78";
	rename -uid "C2A6D337-4E68-735E-E193-47BDA905A3EA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube79";
	rename -uid "2D94739E-4FAA-EADF-05C2-ECAF3BDFDF19";
	setAttr ".t" -type "double3" 1000 700 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape79" -p "pCube79";
	rename -uid "8FB4E7DA-446B-9E43-4A8D-3EA0DDC4ED55";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube80";
	rename -uid "143FD86D-4FEE-FCE7-CAAA-BCB075FA6FA6";
	setAttr ".t" -type "double3" 1000 100 103.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape80" -p "pCube80";
	rename -uid "72D2861C-41E4-3B01-B48A-358FA8612D4A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube81";
	rename -uid "44B0B5F1-43D1-1EE7-BA02-2DB6A96554C3";
	setAttr ".t" -type "double3" 1000 400 200 ;
	setAttr ".rp" -type "double3" 0 0 -100 ;
	setAttr ".sp" -type "double3" 0 0 -100 ;
createNode mesh -n "pCubeShape81" -p "pCube81";
	rename -uid "0C78F203-4B86-0481-D45E-CF95384EE616";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube82";
	rename -uid "695D3A20-40DD-C566-1B53-899FE569F190";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape82" -p "pCube82";
	rename -uid "5223984C-42C8-FA2C-B847-388E4F5D4996";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "e[0:5]" "e[7]" "e[10]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.5 0.25 0.5 0 0.625 0.125 0.5 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5
		 0 200 0.5 0 -200 0.5 200 0 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 12 ".ed[0:11]"  0 5 0 2 4 0 0 8 0 1 6 0 4 3 0 5 1 0 4 7 1
		 6 3 0 7 5 1 6 7 1 8 2 0 7 8 1;
	setAttr -s 4 -ch 16 ".fc[0:3]" -type "polyFaces" 
		f 4 5 3 9 8
		mu 0 4 5 1 6 7
		f 4 0 -9 11 -3
		mu 0 4 0 5 7 8
		f 4 -10 7 -5 6
		mu 0 4 7 6 3 4
		f 4 -12 -7 -2 -11
		mu 0 4 8 7 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube82";
	rename -uid "5A35EE3E-46D9-95F2-FCCE-EBB4651731DA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube83";
	rename -uid "709D8024-429D-024A-52C4-4983614DEDCC";
	setAttr ".t" -type "double3" 399.99999745686853 -200.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape83" -p "pCube83";
	rename -uid "061AE160-4C96-DCE8-BF1C-728894CCB160";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube83";
	rename -uid "39B35676-499A-4EA3-DFC4-F5BAEAC4EBE0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube84";
	rename -uid "899D3AD8-43F4-A278-0AF1-10B6189183CF";
	setAttr ".t" -type "double3" 399.99999745686853 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape84" -p "pCube84";
	rename -uid "A2496AD3-47DF-C191-FC84-5C876B139149";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube84";
	rename -uid "5C939D03-44AE-706A-DE33-B29D4DDD822A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube85";
	rename -uid "203D579A-419A-FD61-70BC-E68F13347162";
	setAttr ".t" -type "double3" 399.99999745686853 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape85" -p "pCube85";
	rename -uid "893A0271-43F2-2BAA-0853-B684376DC62F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube85";
	rename -uid "560C33E2-4865-A3F7-11DD-B7827565FC84";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube86";
	rename -uid "30757124-45DB-56C5-7E40-BBB5F544B243";
	setAttr ".t" -type "double3" 199.99999745686853 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape86" -p "pCube86";
	rename -uid "EC1D9A28-46B5-CF2B-5E01-A6AF41C1D392";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube86";
	rename -uid "8D921317-4116-2836-199B-4BBD0003B0F6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube87";
	rename -uid "1D9DC85B-4400-D76B-D1AF-51BB0C8BD945";
	setAttr ".t" -type "double3" -2.5431314725210541e-006 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape87" -p "pCube87";
	rename -uid "76D6448A-4A93-08BD-16E6-CF93B26D60A6";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube87";
	rename -uid "3193ACE3-41CD-8449-8D1F-02B5F1D17703";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube88";
	rename -uid "EC166A8B-4622-87B0-B0C1-C0B0F6A260AB";
	setAttr ".t" -type "double3" -200.00000254313147 -600.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape88" -p "pCube88";
	rename -uid "8029877A-4AC9-8373-7E48-C084B158A3AF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube88";
	rename -uid "EBFCFE4F-41D7-A47F-518A-12AE08F17221";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube89";
	rename -uid "121A133D-4930-B620-83CA-509418E2C51D";
	setAttr ".t" -type "double3" -200.00000254313147 -400.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape89" -p "pCube89";
	rename -uid "9FA2DCC3-467C-60D6-EF7C-528911BA87FF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube89";
	rename -uid "DA5EA4E2-481E-E61F-45F9-E9A4B281B0A0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube90";
	rename -uid "72CF47FC-4B8C-D33E-EBF3-A0A7FEEDDB43";
	setAttr ".t" -type "double3" -200.00000254313147 -200.00000254313147 -0.5 ;
	setAttr ".rp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
	setAttr ".sp" -type "double3" -199.99999745686853 -199.99999745686853 0.5 ;
createNode mesh -n "pCubeShape90" -p "pCube90";
	rename -uid "4A94D19C-4FC2-AA83-12E1-9EA0AC4B3621";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4375 0.0625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.5 0 0.5
		 0.125 0.375 0.125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 0 -200 0.5 0 0 0.5 -200 0 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 3 0 2 1 1 2 3 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -3 3 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube90";
	rename -uid "F6E50C7A-40BA-4EA6-DBBD-26A57E7D681C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -200 -200 0.5 200 -200 0.5 -200 200 0.5 200 200 0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 3 0 0 2 0 1 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 3 -2 -3
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube91";
	rename -uid "BBE28AE6-458C-1609-58F6-18BACAAD9A8F";
	setAttr ".t" -type "double3" -400 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape91" -p "pCube91";
	rename -uid "9AAB023B-4667-44F0-9F33-E998AFAF3755";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube92";
	rename -uid "F83FE607-473A-A583-A66B-D2B3BE441EB4";
	setAttr ".t" -type "double3" -400 -700 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape92" -p "pCube92";
	rename -uid "3C2D63B3-4E47-0DAC-CB08-E5A285B78EF5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube93";
	rename -uid "CE312F37-42D6-FB0E-E116-3BA6719743C7";
	setAttr ".t" -type "double3" -400 -600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape93" -p "pCube93";
	rename -uid "F1439FE0-4B4B-BB45-D8D5-9A803FDC790C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube94";
	rename -uid "1ED28946-4D8F-540E-52A5-6F8D2CA20FC8";
	setAttr ".t" -type "double3" -400 -500 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape94" -p "pCube94";
	rename -uid "882D6DC6-4593-DF56-2A7C-F6A34DCDAA8D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube95";
	rename -uid "8E162955-4763-EA26-69BB-D5A2B155FFAE";
	setAttr ".t" -type "double3" -400 -400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape95" -p "pCube95";
	rename -uid "A828FCA8-4A64-16EE-5BED-D9B4E585F18F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube96";
	rename -uid "58D4A99D-416C-C784-CEFB-CEB844C4F6B0";
	setAttr ".t" -type "double3" -400 -300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape96" -p "pCube96";
	rename -uid "54AEB064-4CBB-9B59-55D9-B79FA362C6FF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube97";
	rename -uid "B000F82C-47C2-710A-E1D1-E2AFCC3CFCD1";
	setAttr ".t" -type "double3" -400 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape97" -p "pCube97";
	rename -uid "C446EF23-4246-3B97-284F-DC957A085879";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube98";
	rename -uid "6DBBBF2E-46B0-6698-173E-8389EC77B200";
	setAttr ".t" -type "double3" 400 -200 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape98" -p "pCube98";
	rename -uid "E94BF5B5-40AF-0590-ECEB-8A9113912124";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube99";
	rename -uid "5B46085B-4E9B-1C32-2E0D-DC8BAFF1CC7D";
	setAttr ".t" -type "double3" 400 -300 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape99" -p "pCube99";
	rename -uid "CFE93CC5-4CD9-C4AC-153C-AB8D9BBA0ECD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube100";
	rename -uid "702BCF6D-4470-BBF5-B553-89B8385658E3";
	setAttr ".t" -type "double3" 400 -400 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape100" -p "pCube100";
	rename -uid "D7A4A5DF-4FD7-4762-152F-83A3EFCCE685";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube101";
	rename -uid "2CC41129-43E7-935C-5F7C-D4BC70025FE4";
	setAttr ".t" -type "double3" 400 -500 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape101" -p "pCube101";
	rename -uid "90BC9095-487F-D99E-D481-DF9AA1157CE0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube102";
	rename -uid "58FDC524-41AC-B4AD-926D-81A0513FBA92";
	setAttr ".t" -type "double3" 400 -600 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape102" -p "pCube102";
	rename -uid "C6F476E9-40E2-C12B-4157-36B08E99DA52";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube103";
	rename -uid "4D64445B-43D9-0B9E-505F-079FE7FD4EE6";
	setAttr ".t" -type "double3" 400 -700 3.75 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape103" -p "pCube103";
	rename -uid "EE629DCE-4DB6-2B96-9AD4-319D9804AA2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube104";
	rename -uid "9FFB892D-4657-9AFE-D08A-418D19532C9B";
	setAttr ".t" -type "double3" 400 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape104" -p "pCube104";
	rename -uid "3218409F-4301-76CB-4EA8-0CA9DF27BA5D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube105";
	rename -uid "AF5819FD-4EF3-6CA9-D4B2-4A8D096508C1";
	setAttr ".t" -type "double3" -300 -200 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape105" -p "pCube105";
	rename -uid "07047953-4FD2-004F-64D8-618F9F33F0B9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube106";
	rename -uid "6D129E60-4114-797B-B7A7-D1A19D546C39";
	setAttr ".t" -type "double3" 300 -200 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape106" -p "pCube106";
	rename -uid "13E0B3AC-46BD-A48F-6F49-C9BC218B47F1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube107";
	rename -uid "EF9CBC0F-493E-098A-4B55-81A00DD46524";
	setAttr ".t" -type "double3" 100 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape107" -p "pCube107";
	rename -uid "E36A1D82-49A5-4771-4B31-AF8180EB7BC2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube108";
	rename -uid "8FEED2D3-4A66-0368-EABB-05B6CCF8F5D6";
	setAttr ".t" -type "double3" 0 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape108" -p "pCube108";
	rename -uid "82E7F982-4E67-FF73-475C-3CB7B3B3E115";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube109";
	rename -uid "91561E9C-47E4-D200-C0E7-19A7DFE7D1E1";
	setAttr ".t" -type "double3" -100 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape109" -p "pCube109";
	rename -uid "CC53E852-4569-C69A-75A7-5A8742E221FC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube110";
	rename -uid "685162BD-4B59-656D-78B4-318F74030DFD";
	setAttr ".t" -type "double3" -200 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape110" -p "pCube110";
	rename -uid "8B38F883-4202-44A7-AFCD-8AABCF559E3C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube111";
	rename -uid "5C40EB15-42B4-C51D-A1A2-10BC24A5EA91";
	setAttr ".t" -type "double3" 200 -800 8.25 ;
	setAttr ".rp" -type "double3" 0 0 -8.25 ;
	setAttr ".sp" -type "double3" 0 0 -8.25 ;
createNode mesh -n "pCubeShape111" -p "pCube111";
	rename -uid "D3F85173-4E8E-DE42-7D7C-DDBB181876C8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.28125 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -16.5 -16.5 16.5 16.5 -16.5 16.5 -16.5 16.5 16.5
		 16.5 16.5 16.5 -16.5 16.5 -8.25 16.5 16.5 -8.25 -16.5 16.5 -16.5 16.5 16.5 -16.5
		 -16.5 -16.5 -16.5 16.5 -16.5 -16.5 -16.5 -16.5 -8.25 16.5 -16.5 -8.25;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube112";
	rename -uid "02154B6B-40C5-0ABD-2FA9-D69B204A7E70";
	setAttr ".t" -type "double3" 300 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape112" -p "pCube112";
	rename -uid "73A7B5D8-4E56-35A6-EC22-D99D199CF1E6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube113";
	rename -uid "11C613FC-4088-1138-3A7D-BD97D63483F9";
	setAttr ".t" -type "double3" -300 -800 3.75 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".rp" -type "double3" 0 0 -3.75 ;
	setAttr ".sp" -type "double3" 0 0 -3.75 ;
createNode mesh -n "pCubeShape113" -p "pCube113";
	rename -uid "A4D9E96B-4C25-FD87-06C6-98927CD02EFA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.4375 0.625 0.4375 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75
		 0.375 0.8125 0.625 0.8125 0.375 1 0.625 1 0.875 0 0.8125 0 0.875 0.25 0.8125 0.25
		 0.125 0 0.1875 0 0.125 0.25 0.1875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -7.5 -100 7.5 7.5 -100 7.5 -7.5 100 7.5 7.5 100 7.5
		 -7.5 100 -3.75 7.5 100 -3.75 -7.5 100 -7.5 7.5 100 -7.5 -7.5 -100 -7.5 7.5 -100 -7.5
		 -7.5 -100 -3.75 7.5 -100 -3.75;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 1 6 7 0 8 9 0 10 11 1 0 2 0
		 1 3 0 3 5 0 2 4 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0 9 11 0 11 1 0 10 0 0 11 5 1 10 4 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 7 -2 -7
		mu 0 4 0 1 3 2
		f 4 -3 -10 1 8
		mu 0 4 5 4 2 3
		f 4 2 11 -4 -11
		mu 0 4 4 5 7 6
		f 4 3 13 -5 -13
		mu 0 4 6 7 9 8
		f 4 4 15 -6 -15
		mu 0 4 8 9 11 10
		f 4 -1 -18 5 16
		mu 0 4 13 12 10 11
		f 4 -16 -14 -12 -19
		mu 0 4 15 14 16 17
		f 4 -9 -8 -17 18
		mu 0 4 17 3 1 15
		f 4 14 19 10 12
		mu 0 4 18 19 21 20
		f 4 9 -20 17 6
		mu 0 4 2 21 19 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "54A743F2-4900-B3C9-DB83-52BC77806DAF";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "00DFEFA2-41F4-C5E2-BF30-4CAA5391F4DF";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8AE30D7C-4C73-2C40-5CBD-2787313914A4";
createNode displayLayerManager -n "layerManager";
	rename -uid "6EFF0C15-43D9-7851-67AF-67B7AB1539DB";
createNode displayLayer -n "defaultLayer";
	rename -uid "B93A9790-4572-D259-7F0B-75B3BD89CC54";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "66934461-425C-A3AA-2EED-40B4AAAF05EB";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1E65B464-4F38-6DFD-5EB7-8BAB0C7DFAA0";
	setAttr ".g" yes;
createNode objectSet -n "set1";
	rename -uid "A19DAB60-44EC-91BF-3BDB-D992E3AF69DE";
	setAttr ".ihi" 0;
	setAttr -s 20 ".dsm";
	setAttr -s 20 ".gn";
createNode polyCube -n "polyCube1";
	rename -uid "67390C7D-48DC-5406-71F9-629D8FF3EC20";
	setAttr ".w" 33;
	setAttr ".h" 33;
	setAttr ".d" 33;
	setAttr ".sd" 4;
	setAttr ".cuv" 4;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "D80DA864-413A-8F66-060C-A4B40BDA0218";
	setAttr ".ics" -type "componentList" 4 "e[2:3]" "e[8:9]" "e[31:32]" "e[34:35]";
	setAttr ".cv" yes;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "237D3091-486C-2C57-325A-FEBBA997764C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.5431314725210541e-006 399.99999745686853 -0.5 1;
	setAttr ".wt" 0.53279906511306763;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId3";
	rename -uid "DE55DFF8-42FF-F2AE-D8C1-838C0724B3EF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "1BA44530-4757-8233-592B-569A447B186A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "e[0:3]";
createNode polySplitRing -n "polySplitRing2";
	rename -uid "26FE3A55-4F12-0E5C-F661-B4A4720328A3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[2:3]" "e[6]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.5431314725210541e-006 399.99999745686853 -0.5 1;
	setAttr ".wt" 0.46319913864135742;
	setAttr ".re" 3;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode groupId -n "groupId4";
	rename -uid "2C6B874E-4536-163D-177D-8B932F9E19FA";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube2";
	rename -uid "937486F9-467F-53A6-53A4-D7A9DA7581B8";
	setAttr ".w" 15;
	setAttr ".h" 200;
	setAttr ".d" 15;
	setAttr ".sd" 4;
	setAttr ".cuv" 4;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "49D0D1FA-4A38-A474-ED2A-CEB3AE06D843";
	setAttr ".ics" -type "componentList" 4 "e[2:3]" "e[8:9]" "e[31:32]" "e[34:35]";
	setAttr ".cv" yes;
createNode objectSet -n "set2";
	rename -uid "7F8A473A-4F78-A0F9-0071-02A8634A32D6";
	setAttr ".ihi" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D4CCAE8B-4560-DEB1-5079-B49457664F06";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 790\n                -height 396\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n"
		+ "            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n"
		+ "            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n"
		+ "            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 790\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n"
		+ "                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n"
		+ "                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 789\n                -height 395\n"
		+ "                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n"
		+ "            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n"
		+ "            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n"
		+ "            -captureSequenceNumber -1\n            -width 789\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 790\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 790\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n"
		+ "                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n"
		+ "                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1586\n                -height 836\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1586\n            -height 836\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n"
		+ "                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n"
		+ "            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n"
		+ "                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n"
		+ "                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n"
		+ "                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n"
		+ "                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n"
		+ "                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n"
		+ "                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n"
		+ "            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1586\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1586\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 10000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "487FEB85-49C4-952F-3E2A-3C86B5033114";
	setAttr ".b" -type "string" "playbackOptions -min 1.25 -max 150 -ast 1.25 -aet 250 ";
	setAttr ".st" 6;
createNode groupId -n "groupId5";
	rename -uid "4C146A48-4F58-8E17-9866-A5AE6CCF6BDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "08869E1C-4696-28EB-D679-D39626FED4AB";
	setAttr ".ihi" 0;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "D6DD56ED-4BD4-C11B-9F75-9891DFB9C4E8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.71522927284240723;
	setAttr ".dr" no;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "922D6A40-4C16-07DD-3DAC-478020E558DB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[20:21]" "e[23]" "e[25]" "e[27]" "e[29]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.48539397120475769;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "63DB301C-44FF-37EE-3ABC-D79297A6A20A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[8:9]" "e[16:17]" "e[24]" "e[31]" "e[36]" "e[43]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 200 8.25 1;
	setAttr ".wt" 0.4409109354019165;
	setAttr ".re" 16;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "78D22434-44EC-4176-0D3C-CFBDC7601B40";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[12]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[13]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[14]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[15]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[16]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[17]" -type "float3" -6.4527283 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[20]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[21]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[22]" -type "float3" -0.75 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.75 0 0 ;
createNode groupId -n "groupId7";
	rename -uid "961A7870-4701-0CD7-309E-58B148A6CEF7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "8FF5333F-4E3D-4464-C037-1BA179397DA5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "396A1027-4C3D-C56E-F77E-A6B57374CBB9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "5B7BC0D3-4E24-58F6-E402-7BA1F4B600BB";
	setAttr ".ihi" 0;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "A4778372-4F1D-B9A1-AF6E-1D9BC3E7F45A";
	setAttr ".ics" -type "componentList" 7 "e[34]" "e[36]" "e[38]" "e[40]" "e[42:43]" "e[45]" "e[55]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "AAD14829-48A3-E71F-E6CB-968FD56F04EA";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[5]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[10]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[11]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[14]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[17]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[20]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[23]" -type "float3" 0 0 -3.75 ;
	setAttr ".tk[24]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[25]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[26]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[27]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[28]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[29]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[30]" -type "float3" 0 0 -5.4166679 ;
	setAttr ".tk[31]" -type "float3" 0 0 -5.4166679 ;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "CCE37D80-4444-CA9B-5B40-AC8D611FA5EC";
	setAttr ".ics" -type "componentList" 4 "e[21]" "e[23:27]" "e[33]" "e[39]";
	setAttr ".cv" yes;
createNode groupId -n "groupId11";
	rename -uid "50AE1E8F-4426-DCF9-E088-C89B7D2D4D12";
	setAttr ".ihi" 0;
createNode objectSet -n "set3";
	rename -uid "6A87856D-4538-91ED-DCFB-ABB8BE41B546";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr -s 2 ".gn";
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "E4183424-45E3-B694-1512-12B2283C6A48";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 300.00001217298018 200 3.75 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 300 200 50 ;
	setAttr ".rs" 57880;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 200.00001217298018 192.49999999999997 0 ;
	setAttr ".cbx" -type "double3" 400.00001217298018 207.50000000000003 100 ;
createNode groupId -n "groupId12";
	rename -uid "63452582-451A-296B-D298-E19AE77E131B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "459EA8D1-42FD-0D79-96DA-10B674492979";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "e[0:3]";
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "381B00F7-411E-6E76-1691-F293E6955F6C";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 300.00001217298018 200 3.75 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 300 200 50 ;
	setAttr ".rs" 33266;
	setAttr ".lt" -type "double3" 0 0 15 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 200.00001217298018 192.49999999999997 0 ;
	setAttr ".cbx" -type "double3" 400.00001217298018 207.50000000000003 100 ;
createNode groupId -n "groupId13";
	rename -uid "8807B28E-4D30-7964-0EEE-638C88851059";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "A884AF02-4392-3A69-D4EE-4F9D5B5ED734";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "5577C312-4B95-1147-CACD-E08BC7310D1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "495B92F3-42D1-EB74-E6FC-319D8914AF1F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "1EA7FB89-446B-ECC8-554C-A49ECBA41F27";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "A37E596D-4FF1-0781-C5C4-7BA80743A3DD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "5A71D228-4528-30A1-AAC1-D2A29A4A2606";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "264D067A-47C0-1126-F071-94BB3606642F";
	setAttr ".ihi" 0;
createNode polyCube -n "polyCube3";
	rename -uid "20D3E0F0-437A-EF02-6E19-C58B2962313D";
	setAttr ".w" 33;
	setAttr ".h" 250;
	setAttr ".d" 200;
	setAttr ".cuv" 4;
createNode groupId -n "groupId21";
	rename -uid "BC66C701-4802-649D-5D59-169F29A17E43";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "AE7B0E44-4D7C-02F9-7772-C283062316FE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "A7B947B5-4F04-FB10-A238-67B08C942634";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "FA15EB4A-410D-D758-6885-2AAFB6F82098";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "B3491FE0-4FE0-7340-F997-419FD148C6A4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "CEB573CA-4B99-8AAA-1DBB-489A0A6F95B2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "5FC0D3CC-4A08-A7DC-BD45-33AF5B08B528";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "DD8D3E5F-408C-C2D0-6AE5-B0B7AA206A13";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "30C7C7DD-422A-F73A-2E18-8C81ED64FB0D";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1.25;
	setAttr ".unw" 1.25;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 90 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 4 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId3.id" "pCubeShape23.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape23.iog.og[1].gco";
connectAttr "polySplitRing2.out" "pCubeShape23.i";
connectAttr "polyDelEdge1.out" "pCubeShape25.i";
connectAttr "polyDelEdge4.out" "pCubeShape30.i";
connectAttr "groupId4.id" "pCubeShape32.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape32.iog.og[1].gco";
connectAttr "polyDelEdge2.out" "pCubeShape33.i";
connectAttr "groupId12.id" "pCubeShape34.iog.og[0].gid";
connectAttr "set3.mwc" "pCubeShape34.iog.og[0].gco";
connectAttr "polyExtrudeFace2.out" "pCubeShape34.i";
connectAttr "groupId5.id" "pCubeShape44.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape44.iog.og[1].gco";
connectAttr "groupId6.id" "pCubeShape44.iog.og[2].gid";
connectAttr "set2.mwc" "pCubeShape44.iog.og[2].gco";
connectAttr "groupId9.id" "pCubeShape49.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape49.iog.og[0].gco";
connectAttr "groupId10.id" "pCubeShape49.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pCubeShape50.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape50.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape50.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pCubeShape51.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape51.iog.og[1].gco";
connectAttr "groupId13.id" "pCubeShape53.iog.og[0].gid";
connectAttr "set3.mwc" "pCubeShape53.iog.og[0].gco";
connectAttr "groupId14.id" "pCubeShape56.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape56.iog.og[1].gco";
connectAttr "groupId15.id" "pCubeShape57.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape57.iog.og[1].gco";
connectAttr "groupId16.id" "pCubeShape58.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape58.iog.og[1].gco";
connectAttr "groupId17.id" "pCubeShape59.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape59.iog.og[1].gco";
connectAttr "groupId18.id" "pCubeShape60.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape60.iog.og[1].gco";
connectAttr "groupId19.id" "pCubeShape61.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape61.iog.og[1].gco";
connectAttr "groupId20.id" "pCubeShape76.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape76.iog.og[1].gco";
connectAttr "polyCube3.out" "pCubeShape81.i";
connectAttr "groupId21.id" "pCubeShape82.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape82.iog.og[1].gco";
connectAttr "groupId22.id" "pCubeShape83.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape83.iog.og[1].gco";
connectAttr "groupId23.id" "pCubeShape84.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape84.iog.og[1].gco";
connectAttr "groupId24.id" "pCubeShape85.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape85.iog.og[1].gco";
connectAttr "groupId25.id" "pCubeShape86.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape86.iog.og[1].gco";
connectAttr "groupId26.id" "pCubeShape87.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape87.iog.og[1].gco";
connectAttr "groupId27.id" "pCubeShape88.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape88.iog.og[1].gco";
connectAttr "groupId28.id" "pCubeShape89.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape89.iog.og[1].gco";
connectAttr "groupId29.id" "pCubeShape90.iog.og[1].gid";
connectAttr "set1.mwc" "pCubeShape90.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupId3.msg" "set1.gn" -na;
connectAttr "groupId4.msg" "set1.gn" -na;
connectAttr "groupId5.msg" "set1.gn" -na;
connectAttr "groupId11.msg" "set1.gn" -na;
connectAttr "groupId14.msg" "set1.gn" -na;
connectAttr "groupId15.msg" "set1.gn" -na;
connectAttr "groupId16.msg" "set1.gn" -na;
connectAttr "groupId17.msg" "set1.gn" -na;
connectAttr "groupId18.msg" "set1.gn" -na;
connectAttr "groupId19.msg" "set1.gn" -na;
connectAttr "groupId20.msg" "set1.gn" -na;
connectAttr "groupId21.msg" "set1.gn" -na;
connectAttr "groupId22.msg" "set1.gn" -na;
connectAttr "groupId23.msg" "set1.gn" -na;
connectAttr "groupId24.msg" "set1.gn" -na;
connectAttr "groupId25.msg" "set1.gn" -na;
connectAttr "groupId26.msg" "set1.gn" -na;
connectAttr "groupId27.msg" "set1.gn" -na;
connectAttr "groupId28.msg" "set1.gn" -na;
connectAttr "groupId29.msg" "set1.gn" -na;
connectAttr "pCubeShape23.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape32.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape44.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape51.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape56.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape57.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape58.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape59.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape60.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape61.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape76.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape82.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape83.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape84.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape85.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape86.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape87.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape88.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape89.iog.og[1]" "set1.dsm" -na;
connectAttr "pCubeShape90.iog.og[1]" "set1.dsm" -na;
connectAttr "polyCube1.out" "polyDelEdge1.ip";
connectAttr "groupParts1.og" "polySplitRing1.ip";
connectAttr "pCubeShape23.wm" "polySplitRing1.mp";
connectAttr "|pCube23|polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId3.id" "groupParts1.gi";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape23.wm" "polySplitRing2.mp";
connectAttr "polyCube2.out" "polyDelEdge2.ip";
connectAttr "groupId6.msg" "set2.gn" -na;
connectAttr "pCubeShape44.iog.og[2]" "set2.dsm" -na;
connectAttr "|pCube30|polySurfaceShape2.o" "polySplitRing3.ip";
connectAttr "pCubeShape30.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape30.wm" "polySplitRing4.mp";
connectAttr "polyTweak1.out" "polySplitRing5.ip";
connectAttr "pCubeShape30.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyDelEdge3.ip";
connectAttr "polySplitRing5.out" "polyTweak2.ip";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "groupId12.msg" "set3.gn" -na;
connectAttr "groupId13.msg" "set3.gn" -na;
connectAttr "pCubeShape34.iog.og[0]" "set3.dsm" -na;
connectAttr "pCubeShape53.iog.og[0]" "set3.dsm" -na;
connectAttr "groupParts2.og" "polyExtrudeFace1.ip";
connectAttr "pCubeShape34.wm" "polyExtrudeFace1.mp";
connectAttr "|pCube34|polySurfaceShape3.o" "groupParts2.ig";
connectAttr "groupId12.id" "groupParts2.gi";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape34.wm" "polyExtrudeFace2.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape25.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape26.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape27.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape28.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape29.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape30.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape31.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape32.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape33.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape34.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape35.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape36.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape37.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape38.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape39.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape40.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape41.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape42.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape43.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape44.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape47.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape48.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape50.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape50.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape49.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape49.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape51.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape52.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape53.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape54.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape55.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape56.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape57.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape58.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape59.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape60.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape61.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape62.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape63.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape64.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape65.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape66.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape67.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape68.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape69.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape70.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape71.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape72.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape73.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape74.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape75.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape76.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape77.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape78.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape79.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape80.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape81.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape82.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape83.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape84.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape85.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape86.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape87.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape88.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape89.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape90.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape91.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape92.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape93.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape94.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape95.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape96.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape97.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape98.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape99.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape100.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape101.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape102.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape103.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape104.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape105.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape106.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape107.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape108.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape109.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape110.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape111.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape112.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape113.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
// End of scene2.ma
