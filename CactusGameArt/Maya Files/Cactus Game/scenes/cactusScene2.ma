//Maya ASCII 2017 scene
//Name: cactusScene2.ma
//Last modified: Fri, Sep 23, 2016 06:14:28 PM
//Codeset: 1252
requires maya "2017";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "B4E94D87-48A1-D142-5424-75B89CFACE0D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 103.68165991350693 -222.08376803726708 148.76400030323617 ;
	setAttr ".r" -type "double3" 63.861647270894984 3.1805546814635176e-015 21987.399999945785 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "4D0EE718-4D30-449D-1642-959A337DBBFF";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 323.92879970312867;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.8511989720249034e-005 -66.666666666666671 6.0335721889880611 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "64358069-4543-6910-F40A-B5A36347C997";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -67.209373845223752 -0.58265225242169727 787.60517386992694 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "972C5BC1-4EBD-C413-6AE4-8994EA274A10";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 730.63959388335468;
	setAttr ".ow" 98.872777000933965;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0 0 56.965579986572244 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "6CECEB01-4BA9-4C16-9E16-EC876DE1583F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.017887935082526951 -250.74484401764823 38.203055484188518 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "FAB96807-402A-A114-162C-5C9692D15F3E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 250.74484401764821;
	setAttr ".ow" 31.832891301103082;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "DF912CFA-4357-3A4E-75A9-4783F7906D37";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 194.95005215569626 -18.630854592897151 27.481845727589217 ;
	setAttr ".r" -type "double3" 90 1.2722218725854067e-014 89.999999999999986 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "7E3B210D-4010-463F-20E8-DF885ACD82D5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 10000.1;
	setAttr ".ow" 56.507120720952521;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "Cactus";
	rename -uid "5D74A646-4BBA-3D72-CEAA-10B8E4B38639";
createNode transform -n "Cactus2" -p "Cactus";
	rename -uid "B4164985-4F63-9375-E24D-B4B39DAAB96B";
createNode transform -n "transform3" -p "|Cactus|Cactus2";
	rename -uid "43270CDF-43E0-AE0A-91F8-EE8523D202D8";
	setAttr ".v" no;
createNode mesh -n "Cactus2Shape" -p "transform3";
	rename -uid "ED840AE0-4F88-9C34-AD56-E8BCB11D6D10";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform1" -p "Cactus";
	rename -uid "ED3B49C8-400E-6A3A-434C-F98ADB295CE8";
	setAttr ".v" no;
createNode mesh -n "CactusShape" -p "transform1";
	rename -uid "9013D808-4482-8D01-12CE-F4B7878641D2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".pv" -type "double2" 0.54209297895431519 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cactus4";
	rename -uid "EBECBA43-4FFB-4EA3-F011-7FA53AF02A93";
createNode transform -n "transform2" -p "Cactus4";
	rename -uid "80A6B689-4F24-F6C5-DC3A-4B9BE35A154D";
	setAttr ".v" no;
createNode mesh -n "Cactus4Shape" -p "transform2";
	rename -uid "82BE7020-4FE4-7C9E-8629-A6A2195CF1BC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cactus2";
	rename -uid "FBB0B1FB-4F55-9AAC-8288-B8AA7D03E8FC";
	setAttr ".rp" -type "double3" 0 4.4835821355257693e-015 56.965579986572266 ;
	setAttr ".sp" -type "double3" 0 4.4835821355257693e-015 56.965579986572266 ;
createNode transform -n "Cactus6" -p "|Cactus2";
	rename -uid "53741612-42CB-518C-6E48-ACA1FA1A3FF2";
createNode transform -n "transform6" -p "|Cactus2|Cactus6";
	rename -uid "B928B050-40A1-CC7D-0FE6-4CA5618D4D1A";
	setAttr ".v" no;
createNode mesh -n "Cactus6Shape" -p "transform6";
	rename -uid "998780F7-4B77-9850-72EE-45BE6DBBAC9D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "transform4" -p "|Cactus2";
	rename -uid "5E520E1B-492C-B57F-E413-9792720C97BA";
	setAttr ".v" no;
createNode mesh -n "Cactus2Shape" -p "transform4";
	rename -uid "F0DD658B-4AA4-B73D-D280-EC984706C788";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53770637512207031 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cactus8";
	rename -uid "763C8859-495E-BCAB-A2D3-99A3A09D2304";
createNode transform -n "transform5" -p "Cactus8";
	rename -uid "4B3867AA-4DD0-C55C-A176-54B1A47CDD4D";
	setAttr ".v" no;
createNode mesh -n "Cactus8Shape" -p "transform5";
	rename -uid "43AFB12F-4D1E-E9D3-3431-B480E84FC611";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cactus6";
	rename -uid "1BA72CE6-4AFB-FAEF-75F0-9F8B0A44037C";
	setAttr ".t" -type "double3" 0 -6.9026067256927579 0 ;
	setAttr ".s" -type "double3" 1 -1 1 ;
	setAttr ".rp" -type "double3" 0 6.9026067256927579 56.96558321144812 ;
	setAttr ".sp" -type "double3" 0 -6.902606725692749 56.96558321144812 ;
	setAttr ".spt" -type "double3" 0 13.805213451385507 0 ;
createNode mesh -n "Cactus6Shape" -p "|Cactus6";
	rename -uid "2D03609D-4946-9A9E-D4E3-998395C2A429";
	setAttr -k off ".v";
	setAttr -s 14 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.54209297895431519 0.17312933504581451 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 139 ".pt";
	setAttr ".pt[2]" -type "float3" 0 1.6014869 2.8066156 ;
	setAttr ".pt[3]" -type "float3" 0 3.9915946 4.1451473 ;
	setAttr ".pt[4]" -type "float3" 0 4.9159136 4.1451473 ;
	setAttr ".pt[5]" -type "float3" 1.2005631e-016 -2.7647862 0 ;
	setAttr ".pt[6]" -type "float3" -2.3841858e-007 1.5785928 4.1451473 ;
	setAttr ".pt[7]" -type "float3" 0 2.4260941 4.1451473 ;
	setAttr ".pt[8]" -type "float3" -2.3841858e-007 2.1992307 4.1451473 ;
	setAttr ".pt[10]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[11]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[12]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[13]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[14]" -type "float3" 7.4505806e-009 7.4505806e-009 4.1451473 ;
	setAttr ".pt[15]" -type "float3" 0 0 1.7881393e-007 ;
	setAttr ".pt[16]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[17]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[18]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[19]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[20]" -type "float3" -2.7252536 -2.7647862 1.7881393e-007 ;
	setAttr ".pt[22]" -type "float3" -1.4274061 -2.7647862 0 ;
	setAttr ".pt[24]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[25]" -type "float3" -9.3132257e-009 1.3473486 4.1451473 ;
	setAttr ".pt[26]" -type "float3" -7.4505806e-009 1.3473486 4.1451473 ;
	setAttr ".pt[27]" -type "float3" 0 0.93902016 4.1451473 ;
	setAttr ".pt[28]" -type "float3" 0 1.1713235 4.1451473 ;
	setAttr ".pt[29]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[30]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[31]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[32]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[33]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[34]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[35]" -type "float3" 0 -1.5881046 4.1451473 ;
	setAttr ".pt[36]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[37]" -type "float3" 0 2.6859179 4.1451473 ;
	setAttr ".pt[38]" -type "float3" 0 1.1068083 4.1451473 ;
	setAttr ".pt[39]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[40]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[41]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[42]" -type "float3" 0 2.7647824 0 ;
	setAttr ".pt[45]" -type "float3" 0 1.4732352 4.1451473 ;
	setAttr ".pt[46]" -type "float3" 0 2.1104479 4.1451473 ;
	setAttr ".pt[47]" -type "float3" 0 1.4732352 4.1451473 ;
	setAttr ".pt[48]" -type "float3" 0 1.1241542 4.1451473 ;
	setAttr ".pt[49]" -type "float3" 0 -3.4426782 4.1451473 ;
	setAttr ".pt[50]" -type "float3" 0 1.4732352 4.1451473 ;
	setAttr ".pt[51]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[52]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[53]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[54]" -type "float3" 0 3.063307 6.1019044 ;
	setAttr ".pt[55]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[56]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[57]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[58]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[60]" -type "float3" -1.4274061 2.7647858 0 ;
	setAttr ".pt[61]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[63]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[64]" -type "float3" 0 3.063307 4.1451473 ;
	setAttr ".pt[65]" -type "float3" -7.4505806e-009 -1.3473487 4.1451473 ;
	setAttr ".pt[66]" -type "float3" -9.3132257e-009 2.2951663 4.1451473 ;
	setAttr ".pt[68]" -type "float3" -2.7252536 2.7647862 1.7881393e-007 ;
	setAttr ".pt[69]" -type "float3" 0 0 1.7881393e-007 ;
	setAttr ".pt[70]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[71]" -type "float3" 7.4505806e-009 -7.4505806e-009 4.1451473 ;
	setAttr ".pt[72]" -type "float3" 0 3.6425145 6.1019044 ;
	setAttr ".pt[73]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[74]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[75]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[76]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[77]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[78]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[79]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[80]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[81]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[82]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[83]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[84]" -type "float3" 0 3.2259989 0 ;
	setAttr ".pt[87]" -type "float3" 0 -3.2259994 0 ;
	setAttr ".pt[90]" -type "float3" -7.4505806e-009 -1.3473486 2.2351742e-008 ;
	setAttr ".pt[93]" -type "float3" 0 1.3473486 2.2351742e-008 ;
	setAttr ".pt[95]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[96]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[97]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[98]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[99]" -type "float3" -7.4505806e-009 7.4505806e-009 4.1451473 ;
	setAttr ".pt[101]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[102]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[103]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[104]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[105]" -type "float3" 2.7252536 -2.7647862 0 ;
	setAttr ".pt[107]" -type "float3" 1.4274061 -2.7647862 0 ;
	setAttr ".pt[109]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[110]" -type "float3" 9.3132257e-009 1.3473486 4.1451473 ;
	setAttr ".pt[111]" -type "float3" 7.4505806e-009 1.3473486 4.1451473 ;
	setAttr ".pt[112]" -type "float3" 0 1.5842309 4.1451473 ;
	setAttr ".pt[113]" -type "float3" 0 1.3734385 4.1451473 ;
	setAttr ".pt[114]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[115]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[116]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[117]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[118]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[119]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[120]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[121]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[122]" -type "float3" 0 2.3833616 4.1451473 ;
	setAttr ".pt[123]" -type "float3" 0 1.3010784 4.1451473 ;
	setAttr ".pt[124]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[125]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[126]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[127]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[128]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[129]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[130]" -type "float3" 0 3.063307 6.1019044 ;
	setAttr ".pt[131]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[132]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[133]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[134]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[136]" -type "float3" 1.4274061 2.7647858 0 ;
	setAttr ".pt[137]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[139]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[140]" -type "float3" 0 3.063307 4.1451473 ;
	setAttr ".pt[141]" -type "float3" 7.4505806e-009 -1.3473487 4.1451473 ;
	setAttr ".pt[142]" -type "float3" 9.3132257e-009 2.2951663 4.1451473 ;
	setAttr ".pt[144]" -type "float3" 2.7252536 2.7647862 0 ;
	setAttr ".pt[146]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[147]" -type "float3" -7.4505806e-009 -7.4505806e-009 4.1451473 ;
	setAttr ".pt[148]" -type "float3" 0 3.6425145 6.1019044 ;
	setAttr ".pt[149]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[150]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[151]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[152]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[153]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[154]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[155]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[156]" -type "float3" 0 0 6.1019044 ;
	setAttr ".pt[157]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[158]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[159]" -type "float3" 0 0 4.1451473 ;
	setAttr ".pt[164]" -type "float3" 7.4505806e-009 -1.3473486 2.2351742e-008 ;
	setAttr ".pt[167]" -type "float3" 0 1.3473486 2.2351742e-008 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane1";
	rename -uid "57248018-4D47-098A-BB88-AC80CAA26056";
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 192.27902557412219 192.27902557412219 192.27902557412219 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "80CAB763-4D8E-AE2C-B6E4-DCB5C4CE57B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 11 ".pt[0:10]" -type "float3"  0 -1.323489e-023 5.9604645e-008 
		0 -1.323489e-023 5.9604645e-008 0 -1.323489e-023 5.9604645e-008 0 -1.323489e-023 
		5.9604645e-008 0 -1.323489e-023 5.9604645e-008 0 -1.323489e-023 5.9604645e-008 0 
		-1.323489e-023 5.9604645e-008 0 -1.323489e-023 5.9604645e-008 0 -1.323489e-023 5.9604645e-008 
		0 -1.323489e-023 5.9604645e-008 0 -1.323489e-023 5.9604645e-008;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder1";
	rename -uid "0EDB7487-49FE-0F51-078C-C9BF0123F37D";
	setAttr ".t" -type "double3" 66.666666666666671 0 33.333333333333336 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "2BEDC5D5-474F-2A2E-1E0C-48B57ABA658F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.54374983906745911 0.49582865834236145 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 206 ".pt";
	setAttr ".pt[20]" -type "float3" -2.9802322e-008 0 1.0430813e-007 ;
	setAttr ".pt[21]" -type "float3" 2.682209e-007 0 -2.9802322e-008 ;
	setAttr ".pt[22]" -type "float3" 5.9604645e-008 0 1.4901161e-007 ;
	setAttr ".pt[23]" -type "float3" 5.9604645e-008 0 5.9604645e-008 ;
	setAttr ".pt[24]" -type "float3" 0 0 -1.1920929e-007 ;
	setAttr ".pt[25]" -type "float3" 4.4703484e-008 0 -8.9406967e-008 ;
	setAttr ".pt[26]" -type "float3" 5.9604645e-008 0 1.4901161e-007 ;
	setAttr ".pt[27]" -type "float3" 1.4901161e-007 0 -2.0861626e-007 ;
	setAttr ".pt[28]" -type "float3" -8.9406967e-008 0 7.4505806e-008 ;
	setAttr ".pt[29]" -type "float3" -1.1920929e-007 0 7.1054274e-014 ;
	setAttr ".pt[30]" -type "float3" -8.9406967e-008 0 5.9604645e-008 ;
	setAttr ".pt[31]" -type "float3" 1.4901161e-007 0 2.9802322e-008 ;
	setAttr ".pt[32]" -type "float3" -2.0861626e-007 0 -1.4901161e-007 ;
	setAttr ".pt[33]" -type "float3" 4.4703484e-008 0 -5.9604645e-008 ;
	setAttr ".pt[34]" -type "float3" 0 0 1.1920929e-007 ;
	setAttr ".pt[35]" -type "float3" 5.9604645e-008 0 8.9406967e-008 ;
	setAttr ".pt[36]" -type "float3" 2.0861626e-007 0 -1.4901161e-007 ;
	setAttr ".pt[37]" -type "float3" -1.4901161e-007 0 2.9802322e-008 ;
	setAttr ".pt[38]" -type "float3" 8.9406967e-008 0 5.9604645e-008 ;
	setAttr ".pt[39]" -type "float3" 1.1920929e-007 0 7.1054274e-014 ;
	setAttr ".pt[140]" -type "float3" -2.3841858e-007 0 2.9802322e-008 ;
	setAttr ".pt[141]" -type "float3" -8.9406967e-008 0 2.9802322e-008 ;
	setAttr ".pt[142]" -type "float3" -2.9802322e-008 0 5.9604645e-008 ;
	setAttr ".pt[143]" -type "float3" -5.9604645e-008 0 2.3841858e-007 ;
	setAttr ".pt[144]" -type "float3" 0 0 -1.1920929e-007 ;
	setAttr ".pt[145]" -type "float3" -2.9802322e-008 0 -8.9406967e-008 ;
	setAttr ".pt[146]" -type "float3" -1.1920929e-007 0 1.1920929e-007 ;
	setAttr ".pt[147]" -type "float3" 5.9604645e-008 0 8.9406967e-008 ;
	setAttr ".pt[148]" -type "float3" 2.3841858e-007 0 4.4703484e-008 ;
	setAttr ".pt[149]" -type "float3" 5.9604645e-008 0 -7.8159701e-014 ;
	setAttr ".pt[150]" -type "float3" 2.3841858e-007 0 4.4703484e-008 ;
	setAttr ".pt[151]" -type "float3" 5.9604645e-008 0 0 ;
	setAttr ".pt[152]" -type "float3" 2.9802322e-008 0 2.9802322e-008 ;
	setAttr ".pt[153]" -type "float3" 1.4901161e-008 0 -8.9406967e-008 ;
	setAttr ".pt[154]" -type "float3" 0 0 1.1920929e-007 ;
	setAttr ".pt[155]" -type "float3" -1.4901161e-008 0 -2.3841858e-007 ;
	setAttr ".pt[156]" -type "float3" -2.9802322e-008 0 2.9802322e-008 ;
	setAttr ".pt[157]" -type "float3" -5.9604645e-008 0 0 ;
	setAttr ".pt[158]" -type "float3" -2.3841858e-007 0 1.4901161e-008 ;
	setAttr ".pt[159]" -type "float3" -5.9604645e-008 0 4.9737992e-014 ;
	setAttr ".pt[202]" -type "float3" 0 0 0.88888913 ;
	setAttr ".pt[203]" -type "float3" 0 0 0.84538388 ;
	setAttr ".pt[204]" -type "float3" 0 0 0.71912646 ;
	setAttr ".pt[205]" -type "float3" 0 0 0.52247608 ;
	setAttr ".pt[206]" -type "float3" 0 0 0.27468199 ;
	setAttr ".pt[207]" -type "float3" 0 0 1.6954215e-007 ;
	setAttr ".pt[208]" -type "float3" 0 0 -0.27468175 ;
	setAttr ".pt[209]" -type "float3" 0 0 -0.52247596 ;
	setAttr ".pt[210]" -type "float3" 0 0 -0.71912646 ;
	setAttr ".pt[211]" -type "float3" 0 0 -0.84538376 ;
	setAttr ".pt[212]" -type "float3" 0 0 -0.88888913 ;
	setAttr ".pt[213]" -type "float3" 0 0 -0.84538388 ;
	setAttr ".pt[214]" -type "float3" 0 0 -0.7191264 ;
	setAttr ".pt[215]" -type "float3" 0 0 -0.52247584 ;
	setAttr ".pt[216]" -type "float3" 0 0 -0.27468169 ;
	setAttr ".pt[217]" -type "float3" 0 0 1.6954215e-007 ;
	setAttr ".pt[218]" -type "float3" 0 0 0.27468199 ;
	setAttr ".pt[219]" -type "float3" 0 0 0.52247584 ;
	setAttr ".pt[220]" -type "float3" 0 0 0.71912646 ;
	setAttr ".pt[221]" -type "float3" 0 0 0.84538376 ;
	setAttr ".pt[222]" -type "float3" 0 0 0.88888913 ;
	setAttr ".pt[223]" -type "float3" 0 0 0.84538388 ;
	setAttr ".pt[224]" -type "float3" 0 0 0.71912646 ;
	setAttr ".pt[225]" -type "float3" 0 0 0.52247608 ;
	setAttr ".pt[226]" -type "float3" 0 0 0.27468199 ;
	setAttr ".pt[227]" -type "float3" 0 0 1.6954215e-007 ;
	setAttr ".pt[228]" -type "float3" 0 0 -0.27468175 ;
	setAttr ".pt[229]" -type "float3" 0 0 -0.52247596 ;
	setAttr ".pt[230]" -type "float3" 0 0 -0.71912646 ;
	setAttr ".pt[231]" -type "float3" 0 0 -0.84538376 ;
	setAttr ".pt[232]" -type "float3" 0 0 -0.88888913 ;
	setAttr ".pt[233]" -type "float3" 0 0 -0.84538388 ;
	setAttr ".pt[234]" -type "float3" 0 0 -0.7191264 ;
	setAttr ".pt[235]" -type "float3" 0 0 -0.52247584 ;
	setAttr ".pt[236]" -type "float3" 0 0 -0.27468169 ;
	setAttr ".pt[237]" -type "float3" 0 0 1.6954215e-007 ;
	setAttr ".pt[238]" -type "float3" 0 0 0.27468199 ;
	setAttr ".pt[239]" -type "float3" 0 0 0.52247584 ;
	setAttr ".pt[240]" -type "float3" 0 0 0.71912646 ;
	setAttr ".pt[241]" -type "float3" 0 0 0.84538376 ;
	setAttr ".pt[242]" -type "float3" 0 0 2.1134579 ;
	setAttr ".pt[243]" -type "float3" 0 0 1.7978144 ;
	setAttr ".pt[244]" -type "float3" 0 0 1.3061903 ;
	setAttr ".pt[245]" -type "float3" 0 0 0.68670499 ;
	setAttr ".pt[246]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[247]" -type "float3" 0 0 -0.68670511 ;
	setAttr ".pt[248]" -type "float3" 0 0 -1.3061879 ;
	setAttr ".pt[249]" -type "float3" 0 0 -1.7978144 ;
	setAttr ".pt[250]" -type "float3" 0 0 -2.1134577 ;
	setAttr ".pt[251]" -type "float3" 0 0 -2.2222214 ;
	setAttr ".pt[252]" -type "float3" 0 0 -2.1134579 ;
	setAttr ".pt[253]" -type "float3" 0 0 -1.7978158 ;
	setAttr ".pt[254]" -type "float3" 0 0 -1.3061892 ;
	setAttr ".pt[255]" -type "float3" 0 0 -0.68670505 ;
	setAttr ".pt[256]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[257]" -type "float3" 0 0 0.68670452 ;
	setAttr ".pt[258]" -type "float3" 0 0 1.30619 ;
	setAttr ".pt[259]" -type "float3" 0 0 1.7978144 ;
	setAttr ".pt[260]" -type "float3" 0 0 2.1134577 ;
	setAttr ".pt[261]" -type "float3" 0 0 2.2222214 ;
	setAttr ".pt[262]" -type "float3" 0 0 2.1134579 ;
	setAttr ".pt[263]" -type "float3" 0 0 1.7978144 ;
	setAttr ".pt[264]" -type "float3" 0 0 1.3061903 ;
	setAttr ".pt[265]" -type "float3" 0 0 0.68670499 ;
	setAttr ".pt[266]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[267]" -type "float3" 0 0 -0.68670511 ;
	setAttr ".pt[268]" -type "float3" 0 0 -1.3061879 ;
	setAttr ".pt[269]" -type "float3" 0 0 -1.7978144 ;
	setAttr ".pt[270]" -type "float3" 0 0 -2.1134577 ;
	setAttr ".pt[271]" -type "float3" 0 0 -2.2222214 ;
	setAttr ".pt[272]" -type "float3" 0 0 -2.1134579 ;
	setAttr ".pt[273]" -type "float3" 0 0 -1.7978158 ;
	setAttr ".pt[274]" -type "float3" 0 0 -1.3061892 ;
	setAttr ".pt[275]" -type "float3" 0 0 -0.68670505 ;
	setAttr ".pt[276]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[277]" -type "float3" 0 0 0.68670452 ;
	setAttr ".pt[278]" -type "float3" 0 0 1.30619 ;
	setAttr ".pt[279]" -type "float3" 0 0 1.7978144 ;
	setAttr ".pt[280]" -type "float3" 0 0 2.1134577 ;
	setAttr ".pt[281]" -type "float3" 0 0 2.2222214 ;
	setAttr ".pt[284]" -type "float3" 0 0 -0.53114474 ;
	setAttr ".pt[285]" -type "float3" 0 0 -0.27912974 ;
	setAttr ".pt[287]" -type "float3" 0 0 -0.73140407 ;
	setAttr ".pt[289]" -type "float3" 0 0 -0.86013556 ;
	setAttr ".pt[291]" -type "float3" 0 0 -0.90452492 ;
	setAttr ".pt[293]" -type "float3" 0 0 -0.86013573 ;
	setAttr ".pt[295]" -type "float3" 0 0 -0.73140424 ;
	setAttr ".pt[297]" -type "float3" 0 0 -0.53114498 ;
	setAttr ".pt[299]" -type "float3" 0 0 -0.2791298 ;
	setAttr ".pt[301]" -type "float3" 0 0 1.8080077e-007 ;
	setAttr ".pt[303]" -type "float3" 0 0 0.27913004 ;
	setAttr ".pt[305]" -type "float3" 0 0 0.53114498 ;
	setAttr ".pt[307]" -type "float3" 0 0 0.73140424 ;
	setAttr ".pt[309]" -type "float3" 0 0 0.86013556 ;
	setAttr ".pt[311]" -type "float3" 0 0 0.90452492 ;
	setAttr ".pt[313]" -type "float3" 0 0 0.86013573 ;
	setAttr ".pt[315]" -type "float3" 0 0 0.73140424 ;
	setAttr ".pt[317]" -type "float3" 0 0 0.53114498 ;
	setAttr ".pt[319]" -type "float3" 0 0 0.2791301 ;
	setAttr ".pt[321]" -type "float3" 0 0 1.7550252e-007 ;
	setAttr ".pt[322]" -type "float3" 0 0 0.90452492 ;
	setAttr ".pt[323]" -type "float3" 0 0 0.86013556 ;
	setAttr ".pt[326]" -type "float3" 0 0 0.73140424 ;
	setAttr ".pt[328]" -type "float3" 0 0 0.53114498 ;
	setAttr ".pt[330]" -type "float3" 0 0 0.27913007 ;
	setAttr ".pt[332]" -type "float3" 0 0 1.7285349e-007 ;
	setAttr ".pt[334]" -type "float3" 0 0 -0.2791298 ;
	setAttr ".pt[336]" -type "float3" 0 0 -0.53114498 ;
	setAttr ".pt[338]" -type "float3" 0 0 -0.73140424 ;
	setAttr ".pt[340]" -type "float3" 0 0 -0.86013573 ;
	setAttr ".pt[342]" -type "float3" 0 0 -0.90452492 ;
	setAttr ".pt[344]" -type "float3" 0 0 -0.86013556 ;
	setAttr ".pt[346]" -type "float3" 0 0 -0.73140407 ;
	setAttr ".pt[348]" -type "float3" 0 0 -0.53114474 ;
	setAttr ".pt[350]" -type "float3" 0 0 -0.27912974 ;
	setAttr ".pt[352]" -type "float3" 0 0 1.6887982e-007 ;
	setAttr ".pt[354]" -type "float3" 0 0 0.2791301 ;
	setAttr ".pt[356]" -type "float3" 0 0 0.53114498 ;
	setAttr ".pt[358]" -type "float3" 0 0 0.73140424 ;
	setAttr ".pt[360]" -type "float3" 0 0 0.86013573 ;
	setAttr ".pt[362]" -type "float3" 0 0 1.8300493 ;
	setAttr ".pt[363]" -type "float3" 0 0 1.8300493 ;
	setAttr ".pt[364]" -type "float3" 0 0 2.1509235 ;
	setAttr ".pt[365]" -type "float3" 0 0 2.1509235 ;
	setAttr ".pt[366]" -type "float3" 0 0 1.3299458 ;
	setAttr ".pt[367]" -type "float3" 0 0 1.3299458 ;
	setAttr ".pt[368]" -type "float3" 0 0 0.69934154 ;
	setAttr ".pt[369]" -type "float3" 0 0 0.69934154 ;
	setAttr ".pt[370]" -type "float3" 0 0 4.5862438e-007 ;
	setAttr ".pt[371]" -type "float3" 0 0 4.5862438e-007 ;
	setAttr ".pt[372]" -type "float3" 0 0 -0.69934034 ;
	setAttr ".pt[373]" -type "float3" 0 0 -0.69934034 ;
	setAttr ".pt[374]" -type "float3" 0 0 -1.3299448 ;
	setAttr ".pt[375]" -type "float3" 0 0 -1.3299448 ;
	setAttr ".pt[376]" -type "float3" 0 0 -1.8300493 ;
	setAttr ".pt[377]" -type "float3" 0 0 -1.8300493 ;
	setAttr ".pt[378]" -type "float3" 0 0 -2.1509233 ;
	setAttr ".pt[379]" -type "float3" 0 0 -2.1509233 ;
	setAttr ".pt[380]" -type "float3" 0 0 -2.2614434 ;
	setAttr ".pt[381]" -type "float3" 0 0 -2.2614434 ;
	setAttr ".pt[382]" -type "float3" 0 0 -2.1509235 ;
	setAttr ".pt[383]" -type "float3" 0 0 -2.1509235 ;
	setAttr ".pt[384]" -type "float3" 0 0 -1.8300505 ;
	setAttr ".pt[385]" -type "float3" 0 0 -1.8300505 ;
	setAttr ".pt[386]" -type "float3" 0 0 -1.3299456 ;
	setAttr ".pt[387]" -type "float3" 0 0 -1.3299456 ;
	setAttr ".pt[388]" -type "float3" 0 0 -0.69934034 ;
	setAttr ".pt[389]" -type "float3" 0 0 -0.69934034 ;
	setAttr ".pt[390]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[391]" -type "float3" 0 0 4.2385545e-007 ;
	setAttr ".pt[392]" -type "float3" 0 0 0.69934207 ;
	setAttr ".pt[393]" -type "float3" 0 0 0.69934207 ;
	setAttr ".pt[394]" -type "float3" 0 0 1.3299439 ;
	setAttr ".pt[395]" -type "float3" 0 0 1.3299439 ;
	setAttr ".pt[396]" -type "float3" 0 0 1.8300493 ;
	setAttr ".pt[397]" -type "float3" 0 0 1.8300493 ;
	setAttr ".pt[398]" -type "float3" 0 0 2.1509233 ;
	setAttr ".pt[399]" -type "float3" 0 0 2.1509233 ;
	setAttr ".pt[400]" -type "float3" 0 0 2.2614434 ;
	setAttr ".pt[401]" -type "float3" 0 0 2.2614434 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder2";
	rename -uid "D45D1BA3-46E8-B722-7FB3-07A6766AB27A";
	setAttr ".t" -type "double3" 66.666664123535156 -22.071066260477775 24.334165454384095 ;
	setAttr ".s" -type "double3" 2.4284985619715957 1 2.4284985619715957 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "5EFD516B-4830-CD73-C8A7-F08CC8619B26";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.57006266713142395 0.29375576972961426 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt";
	setAttr ".pt[41]" -type "float3" -0.10199792 8.8817842e-016 0.033141203 ;
	setAttr ".pt[42]" -type "float3" -0.086764738 8.8817842e-016 0.063038178 ;
	setAttr ".pt[44]" -type "float3" -0.063038118 8.8817842e-016 0.086764567 ;
	setAttr ".pt[45]" -type "float3" -0.033141255 8.8817842e-016 0.10199796 ;
	setAttr ".pt[46]" -type "float3" 1.0227865e-007 8.8817842e-016 0.10724701 ;
	setAttr ".pt[47]" -type "float3" 0.033141255 8.8817842e-016 0.10199796 ;
	setAttr ".pt[48]" -type "float3" 0.063038334 8.8817842e-016 0.086764567 ;
	setAttr ".pt[49]" -type "float3" 0.086764738 8.8817842e-016 0.063038178 ;
	setAttr ".pt[50]" -type "float3" 0.10199792 8.8817842e-016 0.033141203 ;
	setAttr ".pt[51]" -type "float3" 0.10724685 8.8817842e-016 -5.1139331e-008 ;
	setAttr ".pt[52]" -type "float3" 0.10199792 8.8817842e-016 -0.033141203 ;
	setAttr ".pt[53]" -type "float3" 0.086764738 8.8817842e-016 -0.063038178 ;
	setAttr ".pt[54]" -type "float3" 0.063038334 8.8817842e-016 -0.086764663 ;
	setAttr ".pt[55]" -type "float3" 0.033141255 8.8817842e-016 -0.10199784 ;
	setAttr ".pt[56]" -type "float3" 1.0227865e-007 8.8817842e-016 -0.10724701 ;
	setAttr ".pt[57]" -type "float3" -0.033141255 8.8817842e-016 -0.10199784 ;
	setAttr ".pt[58]" -type "float3" -0.063038118 8.8817842e-016 -0.086764663 ;
	setAttr ".pt[59]" -type "float3" -0.086764738 8.8817842e-016 -0.063038178 ;
	setAttr ".pt[60]" -type "float3" -0.10199792 8.8817842e-016 -0.033141203 ;
	setAttr ".pt[61]" -type "float3" -0.10724685 8.8817842e-016 -5.1139331e-008 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder3";
	rename -uid "1A6F67D9-4742-49AC-419D-4CB7D1762C84";
	setAttr ".t" -type "double3" -66.666666666666671 0 12.663419087727867 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 8.7952386900952995 9.9507327343782261 8.7952386900952995 ;
	setAttr ".rp" -type "double3" 0 -12.663419087727867 3.12177338585966e-031 ;
	setAttr ".rpt" -type "double3" 0 12.663419087727863 -12.663419087727867 ;
	setAttr ".sp" -type "double3" 0 -1.0000001411444472 2.4651903288156619e-032 ;
	setAttr ".spt" -type "double3" 0 -11.66341894658342 2.8752543529780938e-031 ;
createNode mesh -n "pCylinderShape3" -p "pCylinder3";
	rename -uid "022C03A7-4A03-E96F-E6FD-E4B0160616BF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999988079071045 0.6757718026638031 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder4";
	rename -uid "1F8F78A4-4E49-6493-7D3B-1DBDF409DD57";
	setAttr ".t" -type "double3" 67.687020416631867 0.13487191540551535 77.396634419759124 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 4.7901688370057718 5.41948792171726 4.7901688370057718 ;
	setAttr ".rp" -type "double3" 0 -12.663419087727867 3.12177338585966e-031 ;
	setAttr ".rpt" -type "double3" 0 12.663419087727863 -12.663419087727867 ;
	setAttr ".sp" -type "double3" 0 -1.0000001411444472 2.4651903288156619e-032 ;
	setAttr ".spt" -type "double3" 0 -11.66341894658342 2.8752543529780938e-031 ;
createNode mesh -n "pCylinderShape4" -p "pCylinder4";
	rename -uid "B9C19C78-49BB-2BB7-38FE-7D9B7D2E8A0B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999988079071045 0.6757718026638031 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 434 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.5 0.83749998 0.63374424 0.11216897
		 0.61376965 0.072966613 0.58265835 0.041855596 0.54345608 0.021881036 0.5 0.01499831
		 0.45654392 0.021881066 0.41734159 0.041855577 0.38623047 0.072966672 0.36625603 0.11216895
		 0.35937339 0.15562506 0.36625603 0.19908123 0.3862305 0.23828337 0.41734159 0.26939455
		 0.45654395 0.28936902 0.5 0.29625177 0.54345608 0.28936917 0.58265841 0.26939464
		 0.61376959 0.2382835 0.63374388 0.19908109 0.50000006 0.15000001 0.64062685 0.15562505
		 0.38749999 0.32853267 0.375 0.66310376 0.39999998 0.32853201 0.41249996 0.32853183
		 0.42499995 0.32853258 0.43749994 0.32853195 0.44999993 0.32853198 0.46249992 0.32853082
		 0.47499993 0.32853261 0.48749989 0.32853189 0.49999988 0.32853198 0.51249987 0.32853222
		 0.52499986 0.32853246 0.5374999 0.32853079 0.54999989 0.32853091 0.56249982 0.32853264
		 0.57499981 0.32853082 0.5874998 0.32853225 0.59999979 0.32853219 0.61249977 0.3285321
		 0.62499976 0.32853195 0.375 0.32853246 0.62499976 0.3125 0.64860266 0.10796607 0.375
		 0.3125 0.64130044 0.11004051 0.62640899 0.064408496 0.38749999 0.3125 0.62019736
		 0.068623349 0.59184152 0.029841021 0.39999998 0.3125 0.58732837 0.035754576 0.54828393
		 0.0076473355 0.41249996 0.3125 0.54591125 0.014651495 0.5 -7.4505806e-008 0.42499995
		 0.3125 0.5 0.0073798853 0.45171607 0.0076473504 0.43749994 0.3125 0.45408881 0.014651514
		 0.40815851 0.029841051 0.44999993 0.3125 0.4126716 0.035754576 0.37359107 0.064408526
		 0.46249992 0.3125 0.37980279 0.068623401 0.3513974 0.10796608 0.4749999 0.3125 0.35869971
		 0.11004052 0.34374997 0.15625 0.48749989 0.3125 0.35142815 0.15595177 0.3513974 0.20453392
		 0.49999988 0.3125 0.35869971 0.20186305 0.37359107 0.24809146 0.51249987 0.3125 0.37980279
		 0.2432801 0.40815854 0.28265893 0.52499986 0.3125 0.4126716 0.27614895 0.4517161
		 0.3048526 0.53749985 0.3125 0.45408881 0.29725197 0.5 0.3125 0.54999983 0.3125 0.5
		 0.30452359 0.54828387 0.3048526 0.56249982 0.3125 0.54591125 0.29725206 0.59184146
		 0.28265893 0.57499981 0.3125 0.58732837 0.27614897 0.62640893 0.24809146 0.5874998
		 0.3125 0.62019724 0.24328017 0.6486026 0.2045339 0.59999979 0.3125 0.6413002 0.20186298
		 0.65625 0.15625 0.61249977 0.3125 0.64857185 0.15595175 0.375 0.66463947 0.38749999
		 0.66310376 0.375 0.66310376 0.39999998 0.66310376 0.38749999 0.66310376 0.41249996
		 0.66310376 0.39999998 0.66310376 0.42499995 0.66310376 0.41249996 0.66310376 0.43749991
		 0.66310376 0.42499995 0.66310376 0.4499999 0.66310376 0.43749994 0.66310376 0.46249992
		 0.66310376 0.44999993 0.66310376 0.4749999 0.66310376 0.46249995 0.66310376 0.48749989
		 0.66310376 0.4749999 0.66310376 0.49999988 0.66310376 0.48749989 0.66310376 0.51249987
		 0.66310376 0.49999988 0.66310376 0.52499986 0.66310376 0.51249987 0.66310376 0.5374999
		 0.66310376 0.52499986 0.66310376 0.54999983 0.66310376 0.53749985 0.6631037 0.56249982
		 0.66310376 0.54999983 0.66310376 0.57499981 0.66310376 0.56249982 0.66310382 0.5874998
		 0.66310376 0.57499981 0.66310376 0.59999979 0.66310376 0.5874998 0.66310376 0.61249977
		 0.66310376 0.59999979 0.66310376 0.62499976 0.66310376 0.61249977 0.6631037 0.6249997
		 0.66310376 0.6249997 0.66364413 0.375 0.66364413 0.62499976 0.66415751 0.375 0.66415751
		 0.38749999 0.66415846 0.38749999 0.66364604 0.39999998 0.6641584 0.39999998 0.66364598
		 0.41249996 0.6641584 0.41249996 0.66364604 0.42499995 0.6641584 0.42499995 0.66364604
		 0.43749994 0.66415846 0.43749994 0.66364604 0.44999993 0.6641584 0.4499999 0.66364598
		 0.46249989 0.6641584 0.46249992 0.66364598 0.4749999 0.6641584 0.4749999 0.66364604
		 0.48749989 0.66415846 0.48749989 0.66364604 0.49999988 0.6641584 0.49999988 0.66364604
		 0.51249987 0.66415846 0.51249987 0.66364604 0.52499986 0.6641584 0.52499986 0.66364598
		 0.5374999 0.6641584 0.53749985 0.66364604 0.54999983 0.66415846 0.54999983 0.66364604
		 0.56249982 0.66415852 0.56249982 0.6636461 0.57499981 0.6641584 0.57499981 0.66364604
		 0.5874998 0.6641584 0.5874998 0.66364598 0.59999979 0.6641584 0.59999979 0.66364604
		 0.61249977 0.6641584 0.61249977 0.66364598 0.375 0.68843985 0.38749999 0.68843985
		 0.38749999 0.68843985 0.40000001 0.68843985 0.39999998 0.68843985 0.41249993 0.68843991
		 0.41249996 0.68843985 0.42499995 0.68843979 0.42499995 0.68843985 0.43749991 0.68843985
		 0.43749994 0.68843985 0.44999993 0.68843985 0.44999993 0.68843985 0.46249995 0.68843985
		 0.46249992 0.68843985 0.4749999 0.68843985 0.4749999 0.68843985 0.48749989 0.68843985
		 0.48749989 0.68843985 0.49999988 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985
		 0.51249987 0.68843985 0.52499986 0.68843985 0.52499986 0.68843985 0.5374999 0.68843985
		 0.53749985 0.68843985 0.54999983 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985
		 0.56249982 0.68843985 0.57499981 0.68843985 0.57499981 0.68843985 0.5874998 0.68843985
		 0.5874998 0.68843985 0.59999979 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985
		 0.62499976 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.38749999 0.66463947
		 0.375 0.68686891 0.39999998 0.66463947 0.38749999 0.68686891 0.41249996 0.66463947
		 0.39999998 0.68686891 0.42499995 0.66463947 0.41249996 0.68686891 0.43749994 0.66463947
		 0.42499995 0.68686885 0.44999987 0.66463947 0.43749994 0.68686891 0.46249986 0.66463947
		 0.44999993 0.68686885 0.4749999 0.66463947 0.46249992 0.68686891 0.48749989 0.66463947
		 0.47499987 0.68686891 0.49999988 0.66463947 0.48749989 0.68686891;
	setAttr ".uvst[0].uvsp[250:433]" 0.51249987 0.66463953 0.49999988 0.68686891
		 0.52499986 0.66463947 0.51249987 0.68686897 0.5374999 0.66463947 0.52499986 0.68686891
		 0.54999983 0.66463947 0.53749985 0.68686891 0.56249982 0.66463953 0.54999983 0.68686891
		 0.57499981 0.66463947 0.56249982 0.68686891 0.5874998 0.66463947 0.57499981 0.68686897
		 0.59999979 0.66463947 0.5874998 0.68686891 0.61249977 0.66463947 0.59999973 0.68686891
		 0.62499976 0.66463947 0.61249977 0.68686885 0.375 0.68843985 0.62499976 0.68686885
		 0.38749999 0.68788332 0.38749999 0.68736029 0.62499976 0.68788445 0.375 0.68815887
		 0.62499976 0.68736058 0.375 0.68789387 0.39999998 0.68815893 0.39999998 0.68789518
		 0.41249996 0.68815899 0.41249996 0.68789506 0.42499995 0.68815887 0.42499995 0.68789494
		 0.43749994 0.68815899 0.43749994 0.68789518 0.44999993 0.68815899 0.44999993 0.68789512
		 0.46249992 0.68815899 0.46249992 0.68789518 0.4749999 0.68815899 0.4749999 0.68789512
		 0.48749989 0.68815893 0.48749989 0.687895 0.49999988 0.68815899 0.49999988 0.68789506
		 0.51249987 0.68815899 0.51249987 0.68789512 0.52499986 0.68815893 0.52499986 0.68789512
		 0.53749985 0.68815899 0.53749985 0.68789518 0.54999983 0.68815899 0.54999983 0.68789512
		 0.56249982 0.68815899 0.56249982 0.68789512 0.57499981 0.68815893 0.57499981 0.68789506
		 0.5874998 0.68815905 0.5874998 0.68789518 0.59999979 0.68815887 0.59999979 0.68789506
		 0.61249977 0.68788326 0.61249977 0.68736029 0.62640893 0.93559146 0.6486026 0.89203393
		 0.59184152 0.97015887 0.62640893 0.9355914 0.54828387 0.9923526 0.59184146 0.97015893
		 0.5 1 0.54828399 0.99235255 0.4517161 0.9923526 0.5 1 0.40815854 0.97015893 0.45171615
		 0.99235266 0.37359107 0.93559146 0.40815854 0.97015893 0.3513974 0.89203393 0.37359107
		 0.93559146 0.34374997 0.84375 0.3513974 0.89203393 0.3513974 0.79546607 0.34374997
		 0.84375 0.37359107 0.75190854 0.3513974 0.79546607 0.40815851 0.71734107 0.37359104
		 0.75190854 0.45171607 0.69514734 0.40815857 0.71734107 0.5 0.68749994 0.45171613
		 0.69514734 0.54828393 0.69514734 0.5 0.68749994 0.59184152 0.71734101 0.54828399
		 0.69514734 0.62640899 0.75190848 0.59184152 0.71734101 0.64860266 0.79546607 0.62640899
		 0.75190848 0.65625 0.84375 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375
		 0.62640887 0.93559152 0.6486026 0.89203393 0.59184146 0.97015893 0.62640893 0.93559146
		 0.54828399 0.99235255 0.59184146 0.97015893 0.5 1 0.54828387 0.9923526 0.45171601
		 0.99235255 0.5 1 0.40815863 0.97015899 0.4517161 0.9923526 0.37359095 0.93559128
		 0.40815854 0.97015893 0.35139742 0.89203393 0.37359107 0.93559146 0.34374997 0.84375
		 0.3513974 0.89203393 0.35139745 0.79546595 0.34374997 0.84375 0.37359107 0.75190854
		 0.3513974 0.79546607 0.4081586 0.71734101 0.37359107 0.75190854 0.45171607 0.69514734
		 0.40815851 0.71734107 0.5 0.68749994 0.45171607 0.69514734 0.54828393 0.69514734
		 0.5 0.68749994 0.59184152 0.71734101 0.54828393 0.69514734 0.62640899 0.75190848
		 0.59184158 0.71734107 0.64860266 0.79546607 0.62640899 0.75190854 0.65625 0.84375
		 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375 0.64854199 0.89201421
		 0.64854008 0.89201355 0.62635726 0.93555391 0.62635559 0.93555278 0.59180397 0.97010726
		 0.59180272 0.97010565 0.54826427 0.99229181 0.54826361 0.9922899 0.5 0.99993616 0.5
		 0.99993414 0.45173576 0.99229187 0.45173642 0.9922899 0.40819615 0.97010732 0.40819737
		 0.97010571 0.37364268 0.93555385 0.37364432 0.93555266 0.35145813 0.89201421 0.35146007
		 0.89201355 0.34381381 0.84375 0.34381586 0.84375 0.35145813 0.79548573 0.3514601
		 0.79548633 0.37364265 0.75194609 0.37364438 0.75194722 0.40819615 0.71739268 0.40819734
		 0.71739429 0.45173576 0.69520807 0.45173645 0.69520998 0.5 0.68756378 0.5 0.6875658
		 0.54826427 0.69520807 0.54826361 0.69520998 0.59180403 0.71739268 0.59180278 0.71739429
		 0.62635732 0.75194603 0.62635571 0.75194722 0.64854205 0.79548579 0.64854014 0.79548645
		 0.65620756 0.84375 0.65622658 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 382 ".vt";
	setAttr ".vt[0:165]"  1.42563295 1.33444607 -0.46321517 1.21271753 1.33444607 -0.88108784
		 0.88109303 1.33444607 -1.21271324 0.46321726 1.33444607 -1.42562973 2.8610229e-006 1.33444607 -1.49899578
		 -0.46321487 1.33444607 -1.42562973 -0.8810873 1.33444607 -1.212713 -1.21270943 1.33444607 -0.8810876
		 -1.42562485 1.33444607 -0.4632149 -1.49898911 1.33444607 8.9227264e-008 -1.42562485 1.33444607 0.46321523
		 -1.21270943 1.33444607 0.88108778 -0.8810873 1.33444607 1.21271241 -0.46321487 1.33444607 1.42562973
		 2.8610229e-006 1.33444607 1.49899507 0.46321726 1.33444607 1.42562962 0.88109255 1.33444607 1.21271241
		 1.21271753 1.33444607 0.88108766 1.42563295 1.33444607 0.46321511 1.49899721 1.33444607 8.9227264e-008
		 2.8610229e-006 -1 0 0.86489248 -0.79374325 -0.28101873 0.73572159 -0.79374325 -0.53452981
		 2.8610229e-006 -0.79374325 2.4323526e-008 0.53453493 -0.79374325 -0.73571658 0.28102064 -0.79374325 -0.86488682
		 2.8610229e-006 -0.79374325 -0.90939558 -0.28101492 -0.79374325 -0.86488682 -0.53452969 -0.79374325 -0.73571599
		 -0.73571301 -0.79374325 -0.53452951 -0.86488438 -0.79374325 -0.28101861 -0.90939045 -0.79374319 3.1713974e-008
		 -0.86488438 -0.79374319 0.28101885 -0.73571301 -0.79374319 0.53452951 -0.53452969 -0.79374319 0.73571599
		 -0.28101492 -0.79374319 0.86488682 2.8610229e-006 -0.79374325 0.90939504 0.28102064 -0.79374319 0.86488682
		 0.5345335 -0.79374319 0.73571599 0.73572159 -0.79374319 0.53452951 0.86489248 -0.79374319 0.28101867
		 0.90940046 -0.79374325 1.3047155e-008 1.39365196 0.86521232 -0.45282313 1.1855092 0.86521232 -0.86132091
		 0.86132622 0.86521232 -1.18550658 0.45282841 0.86521232 -1.39364624 2.8610229e-006 0.86521232 -1.46536624
		 -0.45281887 0.86521232 -1.39364624 -0.86131573 0.86521232 -1.18550622 -1.1855011 0.86521232 -0.86132079
		 -1.39363956 0.86521232 -0.45282283 -1.46536255 0.86521232 8.3213898e-008 -1.39363956 0.86521232 0.45282313
		 -1.1855011 0.86521232 0.86132091 -0.86131573 0.86521232 1.18550599 -0.45281887 0.86521232 1.39364624
		 2.8610229e-006 0.86521232 1.46536601 0.45282793 0.86521232 1.39364624 0.86132574 0.86521232 1.18550551
		 1.1855092 0.86521232 0.86132079 1.39364767 0.86521232 0.45282307 1.46537066 0.86521232 8.3213898e-008
		 0.97129774 -0.91470987 -0.31559283 0.94874239 -0.95866162 -0.30826336 0.90754414 -0.98911774 -0.29487693
		 0.855968 -1 -0.2781187 0.82623386 -0.91470993 -0.6002934 0.80704784 -0.95866162 -0.58635175
		 0.77200222 -0.98911774 -0.56088918 0.72812748 -1 -0.52901357 0.6002984 -0.91470987 -0.82623309
		 0.58635712 -0.95866162 -0.80704379 0.56089449 -0.98911774 -0.77199805 0.52901888 -1 -0.72812486
		 0.31559801 -0.91470987 -0.97129512 0.30826712 -0.95866162 -0.94873714 0.29487896 -0.98911774 -0.90753806
		 0.27812338 -1 -0.8559621 2.8610229e-006 -0.91470987 -1.021279812 2.8610229e-006 -0.95866162 -0.99756092
		 2.8610229e-006 -0.98911774 -0.95424163 2.8610229e-006 -1 -0.90001106 -0.31559038 -0.91470993 -0.971295
		 -0.30826044 -0.95866162 -0.94873697 -0.29487324 -0.98911774 -0.90753788 -0.27811241 -1 -0.85596156
		 -0.60029316 -0.91470987 -0.82623267 -0.58634663 -0.95866162 -0.80704373 -0.56088448 -0.98911774 -0.77199745
		 -0.52901363 -1 -0.72812426 -0.826231 -0.91470987 -0.60029334 -0.80704308 -0.95866162 -0.58635169
		 -0.7719965 -0.98911774 -0.56088918 -0.72812462 -1 -0.52901351 -0.97128963 -0.91470987 -0.31559283
		 -0.94873428 -0.95866162 -0.30826333 -0.90753746 -0.98911774 -0.29487702 -0.85596085 -1 -0.27811921
		 -1.021273613 -0.91470987 3.805102e-009 -0.99755764 -0.95866162 1.8442573e-009 -0.95423889 -0.98911774 4.8549836e-010
		 -0.90000725 -1 -2.2201951e-017 -0.97128963 -0.91470987 0.31559283 -0.94873428 -0.95866162 0.30826333
		 -0.90753746 -0.98911774 0.29487702 -0.85596085 -1 0.27811921 -0.826231 -0.91470987 0.60029328
		 -0.80704308 -0.95866162 0.58635163 -0.7719965 -0.98911774 0.56088918 -0.72812462 -1 0.52901345
		 -0.60029316 -0.91470987 0.82623267 -0.58634663 -0.95866162 0.80704367 -0.56088448 -0.98911774 0.77199745
		 -0.52901363 -1 0.72812426 -0.31559038 -0.91470993 0.97129476 -0.30826044 -0.95866162 0.94873673
		 -0.29487324 -0.98911774 0.90753746 -0.27811241 -1 0.85596156 2.8610229e-006 -0.91470987 1.021279335
		 2.8610229e-006 -0.95866162 0.9975608 2.8610229e-006 -0.98911774 0.95424145 2.8610229e-006 -1 0.90001106
		 0.31559753 -0.91470987 0.97129476 0.30826712 -0.95866162 0.94873673 0.29487896 -0.98911774 0.90753746
		 0.27812004 -1 0.85596156 0.60029793 -0.91470987 0.82623267 0.58635664 -0.95866162 0.80704367
		 0.56089401 -0.98911774 0.77199745 0.5290184 -1 0.72812426 0.82623386 -0.91470993 0.60029316
		 0.80704784 -0.95866162 0.58635151 0.77200222 -0.98911774 0.56088918 0.72812748 -1 0.5290134
		 0.97129774 -0.91470987 0.3155928 0.9487381 -0.95866162 0.30826315 0.90754271 -0.98911774 0.29487672
		 0.85596657 -1 0.27811867 1.021281719 -0.91470987 3.805102e-009 0.99756575 -0.95866162 -2.4780354e-008
		 0.95424461 -0.98911774 -1.0071276e-007 0.90001535 -1 -2.0874633e-007 1.42490864 0.86441112 -0.46297929
		 1.44089222 0.86781001 -0.46817297 1.45301914 0.87795174 -0.47211251 1.45818567 0.89224756 -0.47379145
		 1.24040747 0.89224756 -0.90120488 1.23601103 0.87795174 -0.89801139 1.22569799 0.86781001 -0.89051795
		 1.21210098 0.86441112 -0.88063872 0.90120983 0.89224756 -1.24040222 0.89801502 0.87795174 -1.23600626
		 0.89052296 0.86781001 -1.22569287 0.88064384 0.86441112 -1.21209562 0.47379684 0.89224756 -1.45818019
		 0.47211552 0.87795174 -1.45301294 0.46817732 0.86781001 -1.44088817 0.46298218 0.86441112 -1.42490375
		 2.8610229e-006 0.89224756 -1.53322101 2.8610229e-006 0.87795174 -1.52778733 2.8610229e-006 0.86781001 -1.51503921
		 2.8610229e-006 0.86441112 -1.49823213 -0.47378826 0.89224756 -1.45818019 -0.47210884 0.87795174 -1.45301294
		 -0.46816921 0.86781001 -1.44088817 -0.4629755 0.86441112 -1.42490375;
	setAttr ".vt[166:331]" -0.90119743 0.89224756 -1.24040139 -0.8980093 0.87795174 -1.23600626
		 -0.89051533 0.86781001 -1.22569251 -0.88063717 0.86441112 -1.21209478 -1.2403965 0.89224756 -0.90120476
		 -1.23600292 0.87795174 -0.89801133 -1.22568989 0.86781001 -0.89051741 -1.21209335 0.86441112 -0.88063872
		 -1.45817661 0.89224756 -0.47379112 -1.45300579 0.87795174 -0.47211215 -1.4408865 0.86781001 -0.46817264
		 -1.42490196 0.86441112 -0.46297896 -1.53321838 0.89224756 9.8020692e-008 -1.52778053 0.87795174 9.7921699e-008
		 -1.51503658 0.86781001 9.8037063e-008 -1.49822998 0.86441112 9.833721e-008 -1.45817661 0.89224756 0.47379145
		 -1.45300579 0.87795174 0.47211251 -1.4408865 0.86781001 0.46817297 -1.42490196 0.86441112 0.46297929
		 -1.2403965 0.89224756 0.90120488 -1.23600292 0.87795174 0.89801139 -1.22568989 0.86781001 0.89051783
		 -1.21209335 0.86441112 0.88063872 -0.90119743 0.89224756 1.24040139 -0.8980093 0.87795174 1.2360059
		 -0.89051533 0.86781001 1.22569168 -0.88063717 0.86441112 1.21209478 -0.47378826 0.89224756 1.45818007
		 -0.47210884 0.87795174 1.45301294 -0.46816921 0.86781001 1.44088757 -0.4629755 0.86441112 1.42490363
		 2.8610229e-006 0.89224756 1.53322089 2.8610229e-006 0.87795174 1.52778733 2.8610229e-006 0.86781001 1.51503897
		 2.8610229e-006 0.86441112 1.49823189 0.47379637 0.89224756 1.45817995 0.47211552 0.87795174 1.45301282
		 0.46817732 0.86781001 1.44088757 0.46298218 0.86441112 1.42490304 0.90120935 0.89224756 1.24040115
		 0.89801502 0.87795174 1.23600566 0.89052248 0.86781001 1.22569168 0.88064337 0.86441112 1.21209455
		 1.24040461 0.89224756 0.90120476 1.23601103 0.87795174 0.89801133 1.22569799 0.86781001 0.89051777
		 1.21209812 0.86441112 0.88063872 1.45818377 0.89224756 0.47379136 1.45301676 0.87795174 0.47211242
		 1.44089222 0.86781001 0.46817291 1.42490768 0.86441112 0.46297923 1.53322601 0.89224756 9.8020685e-008
		 1.52779293 0.87795174 8.7899743e-008 1.51504326 0.86781001 6.0506437e-008 1.49823475 0.86441112 2.2840377e-008
		 1.23991776 1.33524764 -0.90084934 1.2530694 1.3316251 -0.91040558 1.2624855 1.32107341 -0.91724694
		 1.26534891 1.30675614 -0.91932589 1.45760965 1.33524764 -0.47360447 1.47307205 1.3316251 -0.47862846
		 1.48414135 1.32107341 -0.48222515 1.48750448 1.30675614 -0.48331812 0.90085316 1.33524764 -1.23991215
		 0.91040945 1.3316251 -1.25306535 0.91725063 1.32107341 -1.26248169 0.91932821 1.30675614 -1.26534307
		 0.47360992 1.33524764 -1.45760477 0.47863388 1.33162534 -1.47306669 0.48223066 1.32107341 -1.48413658
		 0.48332357 1.30675614 -1.48750043 2.8610229e-006 1.33524764 -1.53261542 2.8610229e-006 1.3316251 -1.54887342
		 2.8610229e-006 1.32107341 -1.5605129 2.8610229e-006 1.30675614 -1.5640496 -0.47359943 1.33524764 -1.45760477
		 -0.4786272 1.3316251 -1.47306669 -0.48222351 1.32107341 -1.48413658 -0.48331642 1.30675614 -1.48750043
		 -0.90084839 1.33524764 -1.23991215 -0.91040421 1.3316251 -1.25306535 -0.91724586 1.32107341 -1.26248169
		 -0.91932106 1.30675614 -1.26534307 -1.23990726 1.33524764 -0.90084922 -1.25306034 1.33162534 -0.9104054
		 -1.26247978 1.32107341 -0.9172467 -1.26534081 1.30675614 -0.91932577 -1.45759964 1.33524764 -0.47360426
		 -1.4730587 1.3316251 -0.47862822 -1.484128 1.32107341 -0.48222488 -1.48749542 1.30675614 -0.48331785
		 -1.53260708 1.33524764 1.0484373e-007 -1.54886723 1.33162534 1.0433673e-007 -1.56050491 1.32107341 1.0385151e-007
		 -1.56404495 1.30675614 1.0353334e-007 -1.45759964 1.33524764 0.47360468 -1.4730587 1.3316251 0.47862861
		 -1.484128 1.32107341 0.48222521 -1.48749542 1.30675614 0.48331824 -1.23990726 1.33524764 0.90084934
		 -1.25306034 1.33162534 0.9104057 -1.26247978 1.32107341 0.91724694 -1.26534081 1.30675614 0.91932589
		 -0.90084839 1.33524764 1.23991203 -0.91040421 1.3316251 1.25306511 -0.91724586 1.32107341 1.26248133
		 -0.91932106 1.30675614 1.26534283 -0.47359943 1.33524764 1.45760465 -0.4786272 1.3316251 1.47306657
		 -0.48222351 1.32107341 1.48413646 -0.48331642 1.30675614 1.48750043 2.8610229e-006 1.33524764 1.53261542
		 2.8610229e-006 1.3316251 1.54887342 2.8610229e-006 1.32107341 1.5605129 2.8610229e-006 1.30675614 1.5640496
		 0.47360992 1.33524764 1.45760453 0.47863388 1.33162534 1.47306645 0.48223066 1.32107341 1.48413634
		 0.48332357 1.30675614 1.48750031 0.90085316 1.33524764 1.23991203 0.91040945 1.3316251 1.25306499
		 0.91725063 1.32107341 1.26248121 0.91932821 1.30675614 1.26534271 1.23991537 1.33524764 0.90084922
		 1.2530694 1.3316251 0.9104054 1.2624855 1.32107341 0.91724682 1.26534796 1.30675614 0.91932589
		 1.45760918 1.33524764 0.47360441 1.47307014 1.3316251 0.47862846 1.48414087 1.32107341 0.48222515
		 1.48750448 1.30675614 0.48331812 1.53261948 1.33524764 8.8115613e-008 1.54887819 1.33162534 9.5848442e-008
		 1.56051731 1.32107341 1.0155723e-007 1.56405306 1.30675614 1.0353334e-007 1.29838896 1.33444607 -0.42187101
		 1.27576256 1.32949889 -0.41451934 1.25800467 1.31572211 -0.40874937 1.24892759 1.29606164 -0.4058001
		 1.10447693 1.33444607 -0.8024466 1.085229874 1.32949889 -0.78846288 1.070124149 1.31572211 -0.77748775
		 1.062402725 1.29606164 -0.77187777 0.80245066 1.33444607 -1.10447288 0.78846693 1.32949889 -1.085225821
		 0.77749157 1.31572211 -1.070119977 0.77188158 1.29606164 -1.062398553 0.42187643 1.33444607 -1.29838562
		 0.41452456 1.32949889 -1.27575958 0.40875483 1.31572211 -1.25800133 0.40580559 1.29606164 -1.24892437
		 2.8610229e-006 1.33444607 -1.3652029 2.8610229e-006 1.32949889 -1.34141243 2.8610229e-006 1.31572211 -1.32274055
		 2.8610229e-006 1.29606164 -1.31319606 -0.42186928 1.33444607 -1.29838562 -0.41451836 1.32949889 -1.27575958
		 -0.4087472 1.31572211 -1.25800133 -0.40579748 1.29606164 -1.24892437 -0.80244541 1.33444607 -1.10447228
		 -0.78845978 1.32949889 -1.085224986 -0.77748489 1.31572211 -1.070119143 -0.77187538 1.29606164 -1.062397838
		 -1.10447121 1.33444607 -0.80244642 -1.085224152 1.32949889 -0.78846252;
	setAttr ".vt[332:381]" -1.070116997 1.31572211 -0.77748752 -1.062396049 1.29606187 -0.77187753
		 -1.29838371 1.33444607 -0.42187077 -1.27575874 1.32949889 -0.41451913 -1.25800037 1.31572211 -0.40874916
		 -1.2489233 1.29606187 -0.40579984 -1.365201 1.33444607 1.4558977e-007 -1.34141064 1.32949889 1.2206371e-007
		 -1.32273865 1.31572211 1.0523855e-007 -1.31319427 1.29606187 9.8728684e-008 -1.29838276 1.33444607 0.42187101
		 -1.27575779 1.32949889 0.41451937 -1.25799942 1.31572211 0.40874943 -1.24892139 1.29606164 0.40580013
		 -1.10447121 1.33444607 0.80244654 -1.085226059 1.32949889 0.78846282 -1.07011795 1.31572211 0.7774877
		 -1.062397957 1.29606187 0.77187771 -0.80244541 1.33444607 1.10447228 -0.78845978 1.32949889 1.085224986
		 -0.77748489 1.31572211 1.070119143 -0.77187538 1.29606164 1.062397838 -0.42186928 1.33444607 1.29838538
		 -0.41451836 1.32949889 1.27575934 -0.4087472 1.31572211 1.25800133 -0.40579748 1.29606164 1.24892414
		 2.8610229e-006 1.33444607 1.3652029 2.8610229e-006 1.32949889 1.34141243 2.8610229e-006 1.31572211 1.32274055
		 2.8610229e-006 1.29606164 1.31319606 0.42187595 1.33444607 1.29838538 0.41452456 1.32949889 1.27575934
		 0.40875435 1.31572211 1.25800133 0.40580511 1.29606164 1.24892414 0.80245066 1.33444607 1.10447228
		 0.78846693 1.32949889 1.085225224 0.77749157 1.31572211 1.070119143 0.77188158 1.29606164 1.062397838
		 1.10447693 1.33444607 0.80244654 1.085229874 1.32949889 0.7884627 1.070124149 1.31572211 0.77748758
		 1.062402725 1.29606164 0.77187759 1.29838896 1.33444607 0.42187077 1.27576256 1.32949889 0.41451913
		 1.25800467 1.31572211 0.40874922 1.24892759 1.29606164 0.40579987 1.36520576 1.33444607 7.5683509e-008
		 1.34141493 1.32949889 7.4094409e-008 1.32274294 1.31572211 7.2690582e-008 1.31319809 1.29606164 7.177325e-008;
	setAttr -s 780 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 8 1 8 9 1
		 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1 17 18 1 18 19 1 19 0 1
		 21 22 0 22 23 1 21 23 1 22 24 0 24 23 1 24 25 0 25 23 1 25 26 0 26 23 1 26 27 0 27 23 1
		 27 28 0 28 23 1 28 29 0 29 23 1 29 30 0 30 23 1 30 31 0 31 23 1 31 32 0 32 23 1 32 33 0
		 33 23 1 33 34 0 34 23 1 34 35 0 35 23 1 35 36 0 36 23 1 36 37 0 37 23 1 37 38 0 38 23 1
		 38 39 0 39 23 1 39 40 0 40 23 1 40 41 0 41 23 1 41 21 0 42 43 0 43 44 0 44 45 0 45 46 0
		 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0
		 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0 67 66 1 66 62 1 68 67 1 65 69 1 69 68 1 65 64 1
		 141 65 1 64 63 1 63 62 1 62 138 1 71 70 1 70 66 1 72 71 1 69 73 1 73 72 1 75 74 1
		 74 70 1 76 75 1 73 77 1 77 76 1 79 78 1 78 74 1 80 79 1 77 81 1 81 80 1 83 82 1 82 78 1
		 84 83 1 81 85 1 85 84 1 87 86 1 86 82 1 88 87 1 85 89 1 89 88 1 91 90 1 90 86 1 92 91 1
		 89 93 1 93 92 1 95 94 1 94 90 1 96 95 1 93 97 1 97 96 1 99 98 1 98 94 1 100 99 1
		 97 101 1 101 100 1 103 102 1 102 98 1 104 103 1 101 105 1 105 104 1 107 106 1 106 102 1
		 108 107 1 105 109 1 109 108 1 111 110 1 110 106 1 112 111 1 109 113 1 113 112 1 115 114 1
		 114 110 1 116 115 1 113 117 1 117 116 1 119 118 1 118 114 1 120 119 1 117 121 1 121 120 1
		 123 122 1 122 118 1 124 123 1 121 125 1 125 124 1 127 126 1 126 122 1 128 127 1 125 129 1
		 129 128 1 131 130 1;
	setAttr ".ed[166:331]" 130 126 1 132 131 1 129 133 1 133 132 1 135 134 1 134 130 1
		 136 135 1 133 137 1 137 136 1 139 138 1 138 134 1 140 139 1 137 141 1 141 140 1 65 20 1
		 20 69 1 20 73 1 20 77 1 20 81 1 20 85 1 20 89 1 20 93 1 20 97 1 20 101 1 20 105 1
		 20 109 1 20 113 1 20 117 1 20 121 1 20 125 1 20 129 1 20 133 1 20 137 1 20 141 1
		 66 43 1 42 62 1 70 44 1 74 45 1 78 46 1 82 47 1 86 48 1 90 49 1 94 50 1 98 51 1 102 52 1
		 106 53 1 110 54 1 114 55 1 118 56 1 122 57 1 126 58 1 130 59 1 134 60 1 138 61 1
		 64 68 1 63 67 1 68 72 1 67 71 1 72 76 1 71 75 1 76 80 1 75 79 1 80 84 1 79 83 1 84 88 1
		 83 87 1 88 92 1 87 91 1 92 96 1 91 95 1 96 100 1 95 99 1 100 104 1 99 103 1 104 108 1
		 103 107 1 108 112 1 107 111 1 112 116 1 111 115 1 116 120 1 115 119 1 120 124 1 119 123 1
		 124 128 1 123 127 1 128 132 1 127 131 1 132 136 1 131 135 1 136 140 1 135 139 1 64 140 1
		 63 139 1 221 142 1 145 218 1 145 144 1 144 147 1 147 146 1 146 145 1 144 143 1 143 148 1
		 148 147 1 143 142 1 142 149 1 149 148 1 151 150 1 150 146 1 152 151 1 149 153 1 153 152 1
		 155 154 1 154 150 1 156 155 1 153 157 1 157 156 1 159 158 1 158 154 1 160 159 1 157 161 1
		 161 160 1 163 162 1 162 158 1 164 163 1 161 165 1 165 164 1 167 166 1 166 162 1 168 167 1
		 165 169 1 169 168 1 171 170 1 170 166 1 172 171 1 169 173 1 173 172 1 175 174 1 174 170 1
		 176 175 1 173 177 1 177 176 1 179 178 1 178 174 1 180 179 1 177 181 1 181 180 1 183 182 1
		 182 178 1 184 183 1 181 185 1 185 184 1 187 186 1 186 182 1 188 187 1 185 189 1 189 188 1
		 191 190 1 190 186 1 192 191 1 189 193 1 193 192 1 195 194 1 194 190 1 196 195 1 193 197 1
		 197 196 1;
	setAttr ".ed[332:497]" 199 198 1 198 194 1 200 199 1 197 201 1 201 200 1 203 202 1
		 202 198 1 204 203 1 201 205 1 205 204 1 207 206 1 206 202 1 208 207 1 205 209 1 209 208 1
		 211 210 1 210 206 1 212 211 1 209 213 1 213 212 1 215 214 1 214 210 1 216 215 1 213 217 1
		 217 216 1 219 218 1 218 214 1 220 219 1 217 221 1 221 220 1 43 149 1 142 42 1 44 153 1
		 45 157 1 46 161 1 47 165 1 48 169 1 49 173 1 50 177 1 51 181 1 52 185 1 53 189 1
		 54 193 1 55 197 1 56 201 1 57 205 1 58 209 1 59 213 1 60 217 1 61 221 1 148 152 1
		 147 151 1 152 156 1 151 155 1 156 160 1 155 159 1 160 164 1 159 163 1 164 168 1 163 167 1
		 168 172 1 167 171 1 172 176 1 171 175 1 176 180 1 175 179 1 180 184 1 179 183 1 184 188 1
		 183 187 1 188 192 1 187 191 1 192 196 1 191 195 1 196 200 1 195 199 1 200 204 1 199 203 1
		 204 208 1 203 207 1 208 212 1 207 211 1 212 216 1 211 215 1 216 220 1 215 219 1 143 220 1
		 144 219 1 231 230 1 230 222 1 232 231 1 225 233 1 233 232 1 225 224 1 229 225 1 224 223 1
		 223 222 1 222 226 1 229 228 1 301 229 1 228 227 1 227 226 1 226 298 1 235 234 1 234 230 1
		 236 235 1 233 237 1 237 236 1 239 238 1 238 234 1 240 239 1 237 241 1 241 240 1 243 242 1
		 242 238 1 244 243 1 241 245 1 245 244 1 247 246 1 246 242 1 248 247 1 245 249 1 249 248 1
		 251 250 1 250 246 1 252 251 1 249 253 1 253 252 1 255 254 1 254 250 1 256 255 1 253 257 1
		 257 256 1 259 258 1 258 254 1 260 259 1 257 261 1 261 260 1 263 262 1 262 258 1 264 263 1
		 261 265 1 265 264 1 267 266 1 266 262 1 268 267 1 265 269 1 269 268 1 271 270 1 270 266 1
		 272 271 1 269 273 1 273 272 1 275 274 1 274 270 1 276 275 1 273 277 1 277 276 1 279 278 1
		 278 274 1 280 279 1 277 281 1 281 280 1 283 282 1 282 278 1 284 283 1;
	setAttr ".ed[498:663]" 281 285 1 285 284 1 287 286 1 286 282 1 288 287 1 285 289 1
		 289 288 1 291 290 1 290 286 1 292 291 1 289 293 1 293 292 1 295 294 1 294 290 1 296 295 1
		 293 297 1 297 296 1 299 298 1 298 294 1 300 299 1 297 301 1 301 300 1 0 226 1 222 1 1
		 230 2 1 234 3 1 238 4 1 242 5 1 246 6 1 250 7 1 254 8 1 258 9 1 262 10 1 266 11 1
		 270 12 1 274 13 1 278 14 1 282 15 1 286 16 1 290 17 1 294 18 1 298 19 1 146 225 1
		 229 145 1 150 233 1 154 237 1 158 241 1 162 245 1 166 249 1 170 253 1 174 257 1 178 261 1
		 182 265 1 186 269 1 190 273 1 194 277 1 198 281 1 202 285 1 206 289 1 210 293 1 214 297 1
		 218 301 1 224 232 0 223 231 0 224 228 0 223 227 0 232 236 0 231 235 0 236 240 0 235 239 0
		 240 244 0 239 243 0 244 248 0 243 247 0 248 252 0 247 251 0 252 256 0 251 255 0 256 260 0
		 255 259 0 260 264 0 259 263 0 264 268 0 263 267 0 268 272 0 267 271 0 272 276 0 271 275 0
		 276 280 0 275 279 0 280 284 0 279 283 0 284 288 0 283 287 0 288 292 0 287 291 0 292 296 0
		 291 295 0 296 300 0 295 299 0 228 300 0 227 299 0 379 378 1 378 302 1 380 379 1 305 381 1
		 381 380 1 305 304 1 309 305 1 304 303 1 303 302 1 302 306 1 309 308 1 313 309 1 308 307 1
		 307 306 1 306 310 1 313 312 1 317 313 1 312 311 1 311 310 1 310 314 1 317 316 1 321 317 1
		 316 315 1 315 314 1 314 318 1 321 320 1 325 321 1 320 319 1 319 318 1 318 322 1 325 324 1
		 329 325 1 324 323 1 323 322 1 322 326 1 329 328 1 333 329 1 328 327 1 327 326 1 326 330 1
		 333 332 1 337 333 1 332 331 1 331 330 1 330 334 1 337 336 1 341 337 1 336 335 1 335 334 1
		 334 338 1 341 340 1 345 341 1 340 339 1 339 338 1 338 342 1 345 344 1 349 345 1 344 343 1
		 343 342 1 342 346 1 349 348 1 353 349 1 348 347 1 347 346 1;
	setAttr ".ed[664:779]" 346 350 1 353 352 1 357 353 1 352 351 1 351 350 1 350 354 1
		 357 356 1 361 357 1 356 355 1 355 354 1 354 358 1 361 360 1 365 361 1 360 359 1 359 358 1
		 358 362 1 365 364 1 369 365 1 364 363 1 363 362 1 362 366 1 369 368 1 373 369 1 368 367 1
		 367 366 1 366 370 1 373 372 1 377 373 1 372 371 1 371 370 1 370 374 1 377 376 1 381 377 1
		 376 375 1 375 374 1 374 378 1 1 306 1 302 0 1 2 310 1 3 314 1 4 318 1 5 322 1 6 326 1
		 7 330 1 8 334 1 9 338 1 10 342 1 11 346 1 12 350 1 13 354 1 14 358 1 15 362 1 16 366 1
		 17 370 1 18 374 1 19 378 1 309 22 1 21 305 1 313 24 1 317 25 1 321 26 1 325 27 1
		 329 28 1 333 29 1 337 30 1 341 31 1 345 32 1 349 33 1 353 34 1 357 35 1 361 36 1
		 365 37 1 369 38 1 373 39 1 377 40 1 381 41 1 304 380 1 303 379 1 304 308 1 303 307 1
		 308 312 1 307 311 1 312 316 1 311 315 1 316 320 1 315 319 1 320 324 1 319 323 1 324 328 1
		 323 327 1 328 332 1 327 331 1 332 336 1 331 335 1 336 340 1 335 339 1 340 344 1 339 343 1
		 344 348 1 343 347 1 348 352 1 347 351 1 352 356 1 351 355 1 356 360 1 355 359 1 360 364 1
		 359 363 1 364 368 1 363 367 1 368 372 1 367 371 1 372 376 1 371 375 1 376 380 1 375 379 1;
	setAttr -s 400 -ch 1560 ".fc[0:399]" -type "polyFaces" 
		f 3 20 21 -23
		mu 0 3 355 357 0
		f 3 23 24 -22
		mu 0 3 357 359 0
		f 3 25 26 -25
		mu 0 3 359 361 0
		f 3 27 28 -27
		mu 0 3 361 363 0
		f 3 29 30 -29
		mu 0 3 363 365 0
		f 3 31 32 -31
		mu 0 3 365 367 0
		f 3 33 34 -33
		mu 0 3 367 369 0
		f 3 35 36 -35
		mu 0 3 369 371 0
		f 3 37 38 -37
		mu 0 3 371 373 0
		f 3 39 40 -39
		mu 0 3 373 375 0
		f 3 41 42 -41
		mu 0 3 375 377 0
		f 3 43 44 -43
		mu 0 3 377 379 0
		f 3 45 46 -45
		mu 0 3 379 381 0
		f 3 47 48 -47
		mu 0 3 381 383 0
		f 3 49 50 -49
		mu 0 3 383 385 0
		f 3 51 52 -51
		mu 0 3 385 387 0
		f 3 53 54 -53
		mu 0 3 387 389 0
		f 3 55 56 -55
		mu 0 3 389 391 0
		f 3 57 58 -57
		mu 0 3 391 393 0
		f 3 59 22 -59
		mu 0 3 393 355 0
		f 3 -84 180 181
		mu 0 3 2 1 20
		f 3 -94 -182 182
		mu 0 3 3 2 20
		f 3 -99 -183 183
		mu 0 3 4 3 20
		f 3 -104 -184 184
		mu 0 3 5 4 20
		f 3 -109 -185 185
		mu 0 3 6 5 20
		f 3 -114 -186 186
		mu 0 3 7 6 20
		f 3 -119 -187 187
		mu 0 3 8 7 20
		f 3 -124 -188 188
		mu 0 3 9 8 20
		f 3 -129 -189 189
		mu 0 3 10 9 20
		f 3 -134 -190 190
		mu 0 3 11 10 20
		f 3 -139 -191 191
		mu 0 3 12 11 20
		f 3 -144 -192 192
		mu 0 3 13 12 20
		f 3 -149 -193 193
		mu 0 3 14 13 20
		f 3 -154 -194 194
		mu 0 3 15 14 20
		f 3 -159 -195 195
		mu 0 3 16 15 20
		f 3 -164 -196 196
		mu 0 3 17 16 20
		f 3 -169 -197 197
		mu 0 3 18 17 20
		f 3 -174 -198 198
		mu 0 3 19 18 20
		f 3 -179 -199 199
		mu 0 3 21 19 20
		f 3 -87 -200 -181
		mu 0 3 1 21 20
		f 4 -82 200 -61 201
		mu 0 4 43 22 106 23
		f 4 -92 202 -62 -201
		mu 0 4 22 24 108 106
		f 4 -97 203 -63 -203
		mu 0 4 24 25 110 108
		f 4 -102 204 -64 -204
		mu 0 4 25 26 112 110
		f 4 -107 205 -65 -205
		mu 0 4 26 27 114 112
		f 4 -112 206 -66 -206
		mu 0 4 27 28 116 114
		f 4 -117 207 -67 -207
		mu 0 4 28 29 118 116
		f 4 -122 208 -68 -208
		mu 0 4 29 30 120 118
		f 4 -127 209 -69 -209
		mu 0 4 30 31 122 120
		f 4 -132 210 -70 -210
		mu 0 4 31 32 124 122
		f 4 -137 211 -71 -211
		mu 0 4 32 33 126 124
		f 4 -142 212 -72 -212
		mu 0 4 33 34 128 126
		f 4 -147 213 -73 -213
		mu 0 4 34 35 130 128
		f 4 -152 214 -74 -214
		mu 0 4 35 36 132 130
		f 4 -157 215 -75 -215
		mu 0 4 36 37 134 132
		f 4 -162 216 -76 -216
		mu 0 4 37 38 136 134
		f 4 -167 217 -77 -217
		mu 0 4 38 39 138 136
		f 4 -172 218 -78 -218
		mu 0 4 39 40 140 138
		f 4 -177 219 -79 -219
		mu 0 4 40 41 142 140
		f 4 -90 -202 -80 -220
		mu 0 4 41 42 144 142
		f 4 -86 83 84 -221
		mu 0 4 47 1 2 50
		f 4 -89 221 80 81
		mu 0 4 43 46 49 22
		f 4 -88 220 82 -222
		mu 0 4 45 47 50 48
		f 4 -85 93 94 -223
		mu 0 4 50 2 3 53
		f 4 -81 223 90 91
		mu 0 4 22 49 52 24
		f 4 -83 222 92 -224
		mu 0 4 48 50 53 51
		f 4 -95 98 99 -225
		mu 0 4 53 3 4 56
		f 4 -91 225 95 96
		mu 0 4 24 52 55 25
		f 4 -93 224 97 -226
		mu 0 4 51 53 56 54
		f 4 -100 103 104 -227
		mu 0 4 56 4 5 59
		f 4 -96 227 100 101
		mu 0 4 25 55 58 26
		f 4 -98 226 102 -228
		mu 0 4 54 56 59 57
		f 4 -105 108 109 -229
		mu 0 4 59 5 6 62
		f 4 -101 229 105 106
		mu 0 4 26 58 61 27
		f 4 -103 228 107 -230
		mu 0 4 57 59 62 60
		f 4 -110 113 114 -231
		mu 0 4 62 6 7 65
		f 4 -106 231 110 111
		mu 0 4 27 61 64 28
		f 4 -108 230 112 -232
		mu 0 4 60 62 65 63
		f 4 -115 118 119 -233
		mu 0 4 65 7 8 68
		f 4 -111 233 115 116
		mu 0 4 28 64 67 29
		f 4 -113 232 117 -234
		mu 0 4 63 65 68 66
		f 4 -120 123 124 -235
		mu 0 4 68 8 9 71
		f 4 -116 235 120 121
		mu 0 4 29 67 70 30
		f 4 -118 234 122 -236
		mu 0 4 66 68 71 69
		f 4 -125 128 129 -237
		mu 0 4 71 9 10 74
		f 4 -121 237 125 126
		mu 0 4 30 70 73 31
		f 4 -123 236 127 -238
		mu 0 4 69 71 74 72
		f 4 -130 133 134 -239
		mu 0 4 74 10 11 77
		f 4 -126 239 130 131
		mu 0 4 31 73 76 32
		f 4 -128 238 132 -240
		mu 0 4 72 74 77 75
		f 4 -135 138 139 -241
		mu 0 4 77 11 12 80
		f 4 -131 241 135 136
		mu 0 4 32 76 79 33
		f 4 -133 240 137 -242
		mu 0 4 75 77 80 78
		f 4 -140 143 144 -243
		mu 0 4 80 12 13 83
		f 4 -136 243 140 141
		mu 0 4 33 79 82 34
		f 4 -138 242 142 -244
		mu 0 4 78 80 83 81
		f 4 -145 148 149 -245
		mu 0 4 83 13 14 86
		f 4 -141 245 145 146
		mu 0 4 34 82 85 35
		f 4 -143 244 147 -246
		mu 0 4 81 83 86 84
		f 4 -150 153 154 -247
		mu 0 4 86 14 15 89
		f 4 -146 247 150 151
		mu 0 4 35 85 88 36
		f 4 -148 246 152 -248
		mu 0 4 84 86 89 87
		f 4 -155 158 159 -249
		mu 0 4 89 15 16 92
		f 4 -151 249 155 156
		mu 0 4 36 88 91 37
		f 4 -153 248 157 -250
		mu 0 4 87 89 92 90
		f 4 -160 163 164 -251
		mu 0 4 92 16 17 95
		f 4 -156 251 160 161
		mu 0 4 37 91 94 38
		f 4 -158 250 162 -252
		mu 0 4 90 92 95 93
		f 4 -165 168 169 -253
		mu 0 4 95 17 18 98
		f 4 -161 253 165 166
		mu 0 4 38 94 97 39
		f 4 -163 252 167 -254
		mu 0 4 93 95 98 96
		f 4 -170 173 174 -255
		mu 0 4 98 18 19 101
		f 4 -166 255 170 171
		mu 0 4 39 97 100 40
		f 4 -168 254 172 -256
		mu 0 4 96 98 101 99
		f 4 -175 178 179 -257
		mu 0 4 101 19 21 104
		f 4 -171 257 175 176
		mu 0 4 40 100 103 41
		f 4 -173 256 177 -258
		mu 0 4 99 101 104 102
		f 4 85 258 -180 86
		mu 0 4 1 47 104 21
		f 4 87 259 -178 -259
		mu 0 4 47 45 102 104
		f 4 88 89 -176 -260
		mu 0 4 44 42 41 103
		f 4 262 263 264 265
		mu 0 4 105 150 151 230
		f 4 266 267 268 -264
		mu 0 4 150 148 152 151
		f 4 269 270 271 -268
		mu 0 4 148 107 109 152
		f 4 60 362 -271 363
		mu 0 4 23 106 109 107
		f 4 61 364 -276 -363
		mu 0 4 106 108 111 109
		f 4 62 365 -281 -365
		mu 0 4 108 110 113 111
		f 4 63 366 -286 -366
		mu 0 4 110 112 115 113
		f 4 64 367 -291 -367
		mu 0 4 112 114 117 115
		f 4 65 368 -296 -368
		mu 0 4 114 116 119 117
		f 4 66 369 -301 -369
		mu 0 4 116 118 121 119
		f 4 67 370 -306 -370
		mu 0 4 118 120 123 121
		f 4 68 371 -311 -371
		mu 0 4 120 122 125 123
		f 4 69 372 -316 -372
		mu 0 4 122 124 127 125
		f 4 70 373 -321 -373
		mu 0 4 124 126 129 127
		f 4 71 374 -326 -374
		mu 0 4 126 128 131 129
		f 4 72 375 -331 -375
		mu 0 4 128 130 133 131
		f 4 73 376 -336 -376
		mu 0 4 130 132 135 133
		f 4 74 377 -341 -377
		mu 0 4 132 134 137 135
		f 4 75 378 -346 -378
		mu 0 4 134 136 139 137
		f 4 76 379 -351 -379
		mu 0 4 136 138 141 139
		f 4 77 380 -356 -380
		mu 0 4 138 140 143 141
		f 4 78 381 -361 -381
		mu 0 4 140 142 145 143
		f 4 79 -364 -261 -382
		mu 0 4 142 144 146 145
		f 4 -272 275 276 -383
		mu 0 4 152 109 111 154
		f 4 -265 383 272 273
		mu 0 4 230 151 153 232
		f 4 -269 382 274 -384
		mu 0 4 151 152 154 153
		f 4 -277 280 281 -385
		mu 0 4 154 111 113 156
		f 4 -273 385 277 278
		mu 0 4 232 153 155 234
		f 4 -275 384 279 -386
		mu 0 4 153 154 156 155
		f 4 -282 285 286 -387
		mu 0 4 156 113 115 158
		f 4 -278 387 282 283
		mu 0 4 234 155 157 236
		f 4 -280 386 284 -388
		mu 0 4 155 156 158 157
		f 4 -287 290 291 -389
		mu 0 4 158 115 117 160
		f 4 -283 389 287 288
		mu 0 4 236 157 159 238
		f 4 -285 388 289 -390
		mu 0 4 157 158 160 159
		f 4 -292 295 296 -391
		mu 0 4 160 117 119 162
		f 4 -288 391 292 293
		mu 0 4 238 159 161 240
		f 4 -290 390 294 -392
		mu 0 4 159 160 162 161
		f 4 -297 300 301 -393
		mu 0 4 162 119 121 164
		f 4 -293 393 297 298
		mu 0 4 240 161 163 242
		f 4 -295 392 299 -394
		mu 0 4 161 162 164 163
		f 4 -302 305 306 -395
		mu 0 4 164 121 123 166
		f 4 -298 395 302 303
		mu 0 4 242 163 165 244
		f 4 -300 394 304 -396
		mu 0 4 163 164 166 165
		f 4 -307 310 311 -397
		mu 0 4 166 123 125 168
		f 4 -303 397 307 308
		mu 0 4 244 165 167 246
		f 4 -305 396 309 -398
		mu 0 4 165 166 168 167
		f 4 -312 315 316 -399
		mu 0 4 168 125 127 170
		f 4 -308 399 312 313
		mu 0 4 246 167 169 248
		f 4 -310 398 314 -400
		mu 0 4 167 168 170 169
		f 4 -317 320 321 -401
		mu 0 4 170 127 129 172
		f 4 -313 401 317 318
		mu 0 4 248 169 171 250
		f 4 -315 400 319 -402
		mu 0 4 169 170 172 171
		f 4 -322 325 326 -403
		mu 0 4 172 129 131 174
		f 4 -318 403 322 323
		mu 0 4 250 171 173 252
		f 4 -320 402 324 -404
		mu 0 4 171 172 174 173
		f 4 -327 330 331 -405
		mu 0 4 174 131 133 176
		f 4 -323 405 327 328
		mu 0 4 252 173 175 254
		f 4 -325 404 329 -406
		mu 0 4 173 174 176 175
		f 4 -332 335 336 -407
		mu 0 4 176 133 135 178
		f 4 -328 407 332 333
		mu 0 4 254 175 177 256
		f 4 -330 406 334 -408
		mu 0 4 175 176 178 177
		f 4 -337 340 341 -409
		mu 0 4 178 135 137 180
		f 4 -333 409 337 338
		mu 0 4 256 177 179 258
		f 4 -335 408 339 -410
		mu 0 4 177 178 180 179
		f 4 -342 345 346 -411
		mu 0 4 180 137 139 182
		f 4 -338 411 342 343
		mu 0 4 258 179 181 260
		f 4 -340 410 344 -412
		mu 0 4 179 180 182 181
		f 4 -347 350 351 -413
		mu 0 4 182 139 141 184
		f 4 -343 413 347 348
		mu 0 4 260 181 183 262
		f 4 -345 412 349 -414
		mu 0 4 181 182 184 183
		f 4 -352 355 356 -415
		mu 0 4 184 141 143 186
		f 4 -348 415 352 353
		mu 0 4 262 183 185 264
		f 4 -350 414 354 -416
		mu 0 4 183 184 186 185
		f 4 -357 360 361 -417
		mu 0 4 186 143 145 188
		f 4 -353 417 357 358
		mu 0 4 264 185 187 266
		f 4 -355 416 359 -418
		mu 0 4 185 186 188 187
		f 4 -270 418 -362 260
		mu 0 4 146 147 188 145
		f 4 -267 419 -360 -419
		mu 0 4 147 149 187 188
		f 4 -263 261 -358 -420
		mu 0 4 149 268 266 187
		f 4 -1 520 -430 521
		mu 0 4 191 189 270 190
		f 4 -2 -522 -422 522
		mu 0 4 193 191 190 192
		f 4 -3 -523 -437 523
		mu 0 4 195 193 192 194
		f 4 -4 -524 -442 524
		mu 0 4 197 195 194 196
		f 4 -5 -525 -447 525
		mu 0 4 199 197 196 198
		f 4 -6 -526 -452 526
		mu 0 4 201 199 198 200
		f 4 -7 -527 -457 527
		mu 0 4 203 201 200 202
		f 4 -8 -528 -462 528
		mu 0 4 205 203 202 204
		f 4 -9 -529 -467 529
		mu 0 4 207 205 204 206
		f 4 -10 -530 -472 530
		mu 0 4 209 207 206 208
		f 4 -11 -531 -477 531
		mu 0 4 211 209 208 210
		f 4 -12 -532 -482 532
		mu 0 4 213 211 210 212
		f 4 -13 -533 -487 533
		mu 0 4 215 213 212 214
		f 4 -14 -534 -492 534
		mu 0 4 217 215 214 216
		f 4 -15 -535 -497 535
		mu 0 4 219 217 216 218
		f 4 -16 -536 -502 536
		mu 0 4 221 219 218 220
		f 4 -17 -537 -507 537
		mu 0 4 223 221 220 222
		f 4 -18 -538 -512 538
		mu 0 4 225 223 222 224
		f 4 -19 -539 -517 539
		mu 0 4 228 225 224 226
		f 4 -20 -540 -435 -521
		mu 0 4 227 228 226 229
		f 4 -266 540 -427 541
		mu 0 4 105 230 233 231
		f 4 -274 542 -424 -541
		mu 0 4 230 232 235 233
		f 4 -279 543 -439 -543
		mu 0 4 232 234 237 235
		f 4 -284 544 -444 -544
		mu 0 4 234 236 239 237
		f 4 -289 545 -449 -545
		mu 0 4 236 238 241 239
		f 4 -294 546 -454 -546
		mu 0 4 238 240 243 241
		f 4 -299 547 -459 -547
		mu 0 4 240 242 245 243
		f 4 -304 548 -464 -548
		mu 0 4 242 244 247 245
		f 4 -309 549 -469 -549
		mu 0 4 244 246 249 247
		f 4 -314 550 -474 -550
		mu 0 4 246 248 251 249
		f 4 -319 551 -479 -551
		mu 0 4 248 250 253 251
		f 4 -324 552 -484 -552
		mu 0 4 250 252 255 253
		f 4 -329 553 -489 -553
		mu 0 4 252 254 257 255
		f 4 -334 554 -494 -554
		mu 0 4 254 256 259 257
		f 4 -339 555 -499 -555
		mu 0 4 256 258 261 259
		f 4 -344 556 -504 -556
		mu 0 4 258 260 263 261
		f 4 -349 557 -509 -557
		mu 0 4 260 262 265 263
		f 4 -354 558 -514 -558
		mu 0 4 262 264 267 265
		f 4 -359 559 -519 -559
		mu 0 4 264 266 269 267
		f 4 -262 -542 -432 -560
		mu 0 4 266 268 271 269
		f 4 -426 423 424 -561
		mu 0 4 273 233 235 279
		f 4 -429 561 420 421
		mu 0 4 190 272 278 192
		f 4 -428 560 422 -562
		mu 0 4 272 273 279 278
		f 4 425 562 -431 426
		mu 0 4 233 273 277 231
		f 4 427 563 -433 -563
		mu 0 4 273 272 275 277
		f 4 428 429 -434 -564
		mu 0 4 272 190 270 275
		f 4 -425 438 439 -565
		mu 0 4 279 235 237 281
		f 4 -421 565 435 436
		mu 0 4 192 278 280 194
		f 4 -423 564 437 -566
		mu 0 4 278 279 281 280
		f 4 -440 443 444 -567
		mu 0 4 281 237 239 283
		f 4 -436 567 440 441
		mu 0 4 194 280 282 196
		f 4 -438 566 442 -568
		mu 0 4 280 281 283 282
		f 4 -445 448 449 -569
		mu 0 4 283 239 241 285
		f 4 -441 569 445 446
		mu 0 4 196 282 284 198
		f 4 -443 568 447 -570
		mu 0 4 282 283 285 284
		f 4 -450 453 454 -571
		mu 0 4 285 241 243 287
		f 4 -446 571 450 451
		mu 0 4 198 284 286 200
		f 4 -448 570 452 -572
		mu 0 4 284 285 287 286
		f 4 -455 458 459 -573
		mu 0 4 287 243 245 289
		f 4 -451 573 455 456
		mu 0 4 200 286 288 202
		f 4 -453 572 457 -574
		mu 0 4 286 287 289 288
		f 4 -460 463 464 -575
		mu 0 4 289 245 247 291
		f 4 -456 575 460 461
		mu 0 4 202 288 290 204
		f 4 -458 574 462 -576
		mu 0 4 288 289 291 290
		f 4 -465 468 469 -577
		mu 0 4 291 247 249 293
		f 4 -461 577 465 466
		mu 0 4 204 290 292 206
		f 4 -463 576 467 -578
		mu 0 4 290 291 293 292
		f 4 -470 473 474 -579
		mu 0 4 293 249 251 295
		f 4 -466 579 470 471
		mu 0 4 206 292 294 208
		f 4 -468 578 472 -580
		mu 0 4 292 293 295 294
		f 4 -475 478 479 -581
		mu 0 4 295 251 253 297
		f 4 -471 581 475 476
		mu 0 4 208 294 296 210
		f 4 -473 580 477 -582
		mu 0 4 294 295 297 296
		f 4 -480 483 484 -583
		mu 0 4 297 253 255 299
		f 4 -476 583 480 481
		mu 0 4 210 296 298 212
		f 4 -478 582 482 -584
		mu 0 4 296 297 299 298
		f 4 -485 488 489 -585
		mu 0 4 299 255 257 301
		f 4 -481 585 485 486
		mu 0 4 212 298 300 214
		f 4 -483 584 487 -586
		mu 0 4 298 299 301 300
		f 4 -490 493 494 -587
		mu 0 4 301 257 259 303
		f 4 -486 587 490 491
		mu 0 4 214 300 302 216
		f 4 -488 586 492 -588
		mu 0 4 300 301 303 302
		f 4 -495 498 499 -589
		mu 0 4 303 259 261 305
		f 4 -491 589 495 496
		mu 0 4 216 302 304 218
		f 4 -493 588 497 -590
		mu 0 4 302 303 305 304
		f 4 -500 503 504 -591
		mu 0 4 305 261 263 307
		f 4 -496 591 500 501
		mu 0 4 218 304 306 220
		f 4 -498 590 502 -592
		mu 0 4 304 305 307 306
		f 4 -505 508 509 -593
		mu 0 4 307 263 265 309
		f 4 -501 593 505 506
		mu 0 4 220 306 308 222
		f 4 -503 592 507 -594
		mu 0 4 306 307 309 308
		f 4 -510 513 514 -595
		mu 0 4 309 265 267 311
		f 4 -506 595 510 511
		mu 0 4 222 308 310 224
		f 4 -508 594 512 -596
		mu 0 4 308 309 311 310
		f 4 -515 518 519 -597
		mu 0 4 311 267 269 313
		f 4 -511 597 515 516
		mu 0 4 224 310 312 226
		f 4 -513 596 517 -598
		mu 0 4 310 311 313 312
		f 4 430 598 -520 431
		mu 0 4 271 276 313 269
		f 4 432 599 -518 -599
		mu 0 4 276 274 312 313
		f 4 433 434 -516 -600
		mu 0 4 274 229 226 312
		f 4 0 700 -610 701
		mu 0 4 352 314 317 315
		f 4 1 702 -615 -701
		mu 0 4 314 316 319 317
		f 4 2 703 -620 -703
		mu 0 4 316 318 321 319
		f 4 3 704 -625 -704
		mu 0 4 318 320 323 321
		f 4 4 705 -630 -705
		mu 0 4 320 322 325 323
		f 4 5 706 -635 -706
		mu 0 4 322 324 327 325
		f 4 6 707 -640 -707
		mu 0 4 324 326 329 327
		f 4 7 708 -645 -708
		mu 0 4 326 328 331 329
		f 4 8 709 -650 -709
		mu 0 4 328 330 333 331
		f 4 9 710 -655 -710
		mu 0 4 330 332 335 333
		f 4 10 711 -660 -711
		mu 0 4 332 334 337 335
		f 4 11 712 -665 -712
		mu 0 4 334 336 339 337
		f 4 12 713 -670 -713
		mu 0 4 336 338 341 339
		f 4 13 714 -675 -714
		mu 0 4 338 340 343 341
		f 4 14 715 -680 -715
		mu 0 4 340 342 345 343
		f 4 15 716 -685 -716
		mu 0 4 342 344 347 345
		f 4 16 717 -690 -717
		mu 0 4 344 346 349 347
		f 4 17 718 -695 -718
		mu 0 4 346 348 351 349
		f 4 18 719 -700 -719
		mu 0 4 348 350 353 351
		f 4 19 -702 -602 -720
		mu 0 4 350 352 315 353
		f 4 -607 720 -21 721
		mu 0 4 392 354 357 355
		f 4 -612 722 -24 -721
		mu 0 4 354 356 359 357
		f 4 -617 723 -26 -723
		mu 0 4 356 358 361 359
		f 4 -622 724 -28 -724
		mu 0 4 358 360 363 361
		f 4 -627 725 -30 -725
		mu 0 4 360 362 365 363
		f 4 -632 726 -32 -726
		mu 0 4 362 364 367 365
		f 4 -637 727 -34 -727
		mu 0 4 364 366 369 367
		f 4 -642 728 -36 -728
		mu 0 4 366 368 371 369
		f 4 -647 729 -38 -729
		mu 0 4 368 370 373 371
		f 4 -652 730 -40 -730
		mu 0 4 370 372 375 373
		f 4 -657 731 -42 -731
		mu 0 4 372 374 377 375
		f 4 -662 732 -44 -732
		mu 0 4 374 376 379 377
		f 4 -667 733 -46 -733
		mu 0 4 376 378 381 379
		f 4 -672 734 -48 -734
		mu 0 4 378 380 383 381
		f 4 -677 735 -50 -735
		mu 0 4 380 382 385 383
		f 4 -682 736 -52 -736
		mu 0 4 382 384 387 385
		f 4 -687 737 -54 -737
		mu 0 4 384 386 389 387
		f 4 -692 738 -56 -738
		mu 0 4 386 388 391 389
		f 4 -697 739 -58 -739
		mu 0 4 388 390 393 391
		f 4 -604 -722 -60 -740
		mu 0 4 390 392 355 393
		f 4 -606 603 604 -741
		mu 0 4 395 392 390 433
		f 4 -609 741 600 601
		mu 0 4 315 394 432 353
		f 4 -608 740 602 -742
		mu 0 4 394 395 433 432
		f 4 605 742 -611 606
		mu 0 4 392 395 397 354
		f 4 607 743 -613 -743
		mu 0 4 395 394 396 397
		f 4 608 609 -614 -744
		mu 0 4 394 315 317 396
		f 4 610 744 -616 611
		mu 0 4 354 397 399 356
		f 4 612 745 -618 -745
		mu 0 4 397 396 398 399
		f 4 613 614 -619 -746
		mu 0 4 396 317 319 398
		f 4 615 746 -621 616
		mu 0 4 356 399 401 358
		f 4 617 747 -623 -747
		mu 0 4 399 398 400 401
		f 4 618 619 -624 -748
		mu 0 4 398 319 321 400
		f 4 620 748 -626 621
		mu 0 4 358 401 403 360
		f 4 622 749 -628 -749
		mu 0 4 401 400 402 403
		f 4 623 624 -629 -750
		mu 0 4 400 321 323 402
		f 4 625 750 -631 626
		mu 0 4 360 403 405 362
		f 4 627 751 -633 -751
		mu 0 4 403 402 404 405
		f 4 628 629 -634 -752
		mu 0 4 402 323 325 404
		f 4 630 752 -636 631
		mu 0 4 362 405 407 364
		f 4 632 753 -638 -753
		mu 0 4 405 404 406 407
		f 4 633 634 -639 -754
		mu 0 4 404 325 327 406
		f 4 635 754 -641 636
		mu 0 4 364 407 409 366
		f 4 637 755 -643 -755
		mu 0 4 407 406 408 409
		f 4 638 639 -644 -756
		mu 0 4 406 327 329 408
		f 4 640 756 -646 641
		mu 0 4 366 409 411 368
		f 4 642 757 -648 -757
		mu 0 4 409 408 410 411
		f 4 643 644 -649 -758
		mu 0 4 408 329 331 410
		f 4 645 758 -651 646
		mu 0 4 368 411 413 370
		f 4 647 759 -653 -759
		mu 0 4 411 410 412 413
		f 4 648 649 -654 -760
		mu 0 4 410 331 333 412
		f 4 650 760 -656 651
		mu 0 4 370 413 415 372
		f 4 652 761 -658 -761
		mu 0 4 413 412 414 415
		f 4 653 654 -659 -762
		mu 0 4 412 333 335 414
		f 4 655 762 -661 656
		mu 0 4 372 415 417 374
		f 4 657 763 -663 -763
		mu 0 4 415 414 416 417
		f 4 658 659 -664 -764
		mu 0 4 414 335 337 416
		f 4 660 764 -666 661
		mu 0 4 374 417 419 376
		f 4 662 765 -668 -765
		mu 0 4 417 416 418 419
		f 4 663 664 -669 -766
		mu 0 4 416 337 339 418
		f 4 665 766 -671 666
		mu 0 4 376 419 421 378
		f 4 667 767 -673 -767
		mu 0 4 419 418 420 421
		f 4 668 669 -674 -768
		mu 0 4 418 339 341 420
		f 4 670 768 -676 671
		mu 0 4 378 421 423 380
		f 4 672 769 -678 -769
		mu 0 4 421 420 422 423
		f 4 673 674 -679 -770
		mu 0 4 420 341 343 422
		f 4 675 770 -681 676
		mu 0 4 380 423 425 382
		f 4 677 771 -683 -771
		mu 0 4 423 422 424 425
		f 4 678 679 -684 -772
		mu 0 4 422 343 345 424
		f 4 680 772 -686 681
		mu 0 4 382 425 427 384
		f 4 682 773 -688 -773
		mu 0 4 425 424 426 427
		f 4 683 684 -689 -774
		mu 0 4 424 345 347 426
		f 4 685 774 -691 686
		mu 0 4 384 427 429 386
		f 4 687 775 -693 -775
		mu 0 4 427 426 428 429
		f 4 688 689 -694 -776
		mu 0 4 426 347 349 428
		f 4 690 776 -696 691
		mu 0 4 386 429 431 388
		f 4 692 777 -698 -777
		mu 0 4 429 428 430 431
		f 4 693 694 -699 -778
		mu 0 4 428 349 351 430
		f 4 695 778 -605 696
		mu 0 4 388 431 433 390
		f 4 697 779 -603 -779
		mu 0 4 431 430 432 433
		f 4 698 699 -601 -780
		mu 0 4 430 351 353 432;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid1";
	rename -uid "8974A607-4512-C3D8-B7EC-9A8B101CE635";
	setAttr ".t" -type "double3" -68.419595415538254 -38.80246750642123 0 ;
	setAttr ".s" -type "double3" 29.735424192291649 23.497414689165094 13.310613060434825 ;
createNode mesh -n "pSolidShape1" -p "pSolid1";
	rename -uid "C0D0D66C-44A9-0040-4995-EF8072CA86E8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid2";
	rename -uid "42400C26-46CF-5D9B-0104-E9B9EAD1116D";
	setAttr ".t" -type "double3" -43.260976965400268 -38.80246750642123 0 ;
	setAttr ".r" -type "double3" -33.001877683699561 -17.927662402110787 -94.993133169240465 ;
	setAttr ".s" -type "double3" 29.735424192291649 23.497414689165094 13.310613060434825 ;
createNode mesh -n "pSolidShape2" -p "pSolid2";
	rename -uid "24566542-4425-CF66-85A4-B99281259351";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid3";
	rename -uid "82962B3C-4AD4-D753-F34A-9789E3EFAEC2";
	setAttr ".t" -type "double3" -54.161585978115319 -54.919093406553991 0.82767768474474224 ;
	setAttr ".r" -type "double3" 71.017605141488659 -47.214782201164539 25.576562964166911 ;
	setAttr ".s" -type "double3" 16.439165081028111 12.990494991919265 18.404530199948024 ;
createNode mesh -n "pSolidShape3" -p "pSolid3";
	rename -uid "663BD613-446D-8EB5-AD4D-53AE012C9880";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid4";
	rename -uid "DEE52C28-41FD-4B31-0E40-C0B6657D5311";
	setAttr ".t" -type "double3" -20.41927294787666 -65.189658656830062 -0.0028615593058471944 ;
	setAttr -av ".tx";
	setAttr -av ".ty";
	setAttr -av ".tz";
	setAttr ".r" -type "double3" -58.762866460651168 -79.669525091393851 161.53094934954245 ;
	setAttr -av ".rx";
	setAttr -av ".ry";
	setAttr -av ".rz";
	setAttr ".s" -type "double3" 3.0380145987950962 3.7853161308763714 5.3629184330851913 ;
	setAttr -av ".sx";
	setAttr -av ".sy";
	setAttr -av ".sz";
createNode mesh -n "pSolidShape4" -p "pSolid4";
	rename -uid "D0F99468-4654-C349-54F1-88AF195C8768";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid5";
	rename -uid "1BC990FA-40FE-9280-69A7-46B8CDD32746";
	setAttr ".t" -type "double3" -43.260976965400268 60.693564443048956 0 ;
	setAttr ".r" -type "double3" -33.001877683699561 -17.927662402110787 -94.993133169240465 ;
	setAttr ".s" -type "double3" 29.735424192291649 35.7312693364984 13.310613060434825 ;
createNode mesh -n "pSolidShape5" -p "pSolid5";
	rename -uid "4BCDD47D-4129-6781-8046-45A7B7B370FB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid6";
	rename -uid "02EDBB35-4477-7399-FAF6-F28885D48941";
	setAttr ".t" -type "double3" -26.320112878222382 60.693564443048956 0 ;
	setAttr ".r" -type "double3" 74.064049257019875 44.195725247316751 -25.09147585352855 ;
	setAttr ".s" -type "double3" 29.735424192291649 35.7312693364984 13.310613060434825 ;
createNode mesh -n "pSolidShape6" -p "pSolid6";
	rename -uid "ECEE73A4-4DBB-82DB-3D9E-70AB4BBF2CFA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid7";
	rename -uid "F2217898-49B4-46E0-E586-DAB68ECE9E87";
	setAttr ".t" -type "double3" 17.593247591450059 60.693564443048956 0 ;
	setAttr ".r" -type "double3" 78.084045295807243 -17.563701544117777 -10.190199877029421 ;
	setAttr ".s" -type "double3" 26.942401077150389 47.194028911641993 29.073191041771 ;
createNode mesh -n "pSolidShape7" -p "pSolid7";
	rename -uid "DF7FD935-43C2-CD39-B2ED-2CA0FA12C17D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid8";
	rename -uid "EB054942-4F00-F2C7-67FF-7DB62E4CE9CB";
	setAttr ".t" -type "double3" 41.582091801188305 60.693564443048956 0 ;
	setAttr ".r" -type "double3" -17.606083900565622 22.71638887257982 -56.797420830645315 ;
	setAttr ".s" -type "double3" 34.300507163012512 20.708518150860563 31.368292656436488 ;
createNode mesh -n "pSolidShape8" -p "pSolid8";
	rename -uid "57291692-4875-B916-4F82-B7BE2405709E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pSolid9";
	rename -uid "2171854C-4492-DB03-1E92-39A6524C2506";
	setAttr ".t" -type "double3" -1.6076321510580911 68.017138890903041 49.476558516320942 ;
	setAttr ".r" -type "double3" -13.279240048660272 1.2380102691767176 -105.4766150002376 ;
	setAttr ".s" -type "double3" 18.725054822269243 24.055556078652607 60.551931615748259 ;
createNode mesh -n "pSolidShape9" -p "pSolid9";
	rename -uid "8BA8BB8C-4A30-747D-15E2-88BBDC1F0583";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.41982124745845795 0.46352550387382507 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.099106349 0.25
		 0.22160855 0.25 0.25946382 0.36803401 0.16035745 0.440983 0.061251096 0.36803401
		 0.037855253 0.440983 0.1982127 0.559017 0.099106349 0.63196599 0 0.559017 0.32071489
		 0.559017 0.35857016 0.67705101 0.25946379 0.75 0.16035745 0.67705101 0.35857016 0.440983
		 0.48107234 0.440983 0.51892757 0.559017 0.41982123 0.63196599 0.29731905 0.25 0.41982123
		 0.25 0.4576765 0.36803398 0.74053615 0.25 0.83964252 0.32294902 0.80178726 0.440983
		 0.67928505 0.440983 0.64142984 0.32294902 0.58017874 0.36803401 0.64142984 0.559017
		 0.74053615 0.63196599 0.70268089 0.75 0.58017874 0.75 0.54232347 0.63196599 0.83964252
		 0.559017 0.9387489 0.63196599 0.90089363 0.75 0.77839142 0.75 0.90089363 0.36803398
		 1 0.440983 0.96214473 0.559017;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.0038728253 0 ;
	setAttr ".pt[1]" -type "float3" 0 -0.0038729266 0 ;
	setAttr ".pt[3]" -type "float3" 0 -0.006266661 0 ;
	setAttr ".pt[4]" -type "float3" 0 0.006266661 0 ;
	setAttr ".pt[5]" -type "float3" 0.012670338 0.10482934 -0.17645906 ;
	setAttr ".pt[6]" -type "float3" 0 0.006266668 0 ;
	setAttr ".pt[7]" -type "float3" 0 -0.0062665972 0 ;
	setAttr ".pt[8]" -type "float3" 0 0.0038728253 0 ;
	setAttr ".pt[9]" -type "float3" 0 -0.0038729266 0 ;
	setAttr ".pt[10]" -type "float3" 0.012670338 0.10482934 -0.17645906 ;
	setAttr ".pt[11]" -type "float3" 0 0.006266668 0 ;
	setAttr ".pt[12]" -type "float3" 0 -0.0062665972 0 ;
	setAttr ".pt[14]" -type "float3" 0 -0.006266661 0 ;
	setAttr ".pt[15]" -type "float3" 0 0.006266661 0 ;
	setAttr ".pt[16]" -type "float3" 0 0.010139504 0 ;
	setAttr ".pt[17]" -type "float3" 0 -0.010139504 0 ;
	setAttr ".pt[18]" -type "float3" 0 -0.010139504 0 ;
	setAttr ".pt[19]" -type "float3" 0 0.010139504 0 ;
	setAttr -s 20 ".vt[0:19]"  0.93417233 -0.35682213 -7.923043e-017 0.93417233 0.35682213 7.923043e-017
		 0.35682213 2.0742793e-016 -0.93417233 0.57735026 0.57735026 -0.57735026 0.57735026 -0.57735026 -0.57735026
		 0.35682213 -2.0742793e-016 0.93417233 0.57735026 -0.57735026 0.57735026 0.57735026 0.57735026 0.57735026
		 -0.93417233 -0.35682213 -7.923043e-017 -0.93417233 0.35682213 7.923043e-017 -0.35682213 -2.0742793e-016 0.93417233
		 -0.57735026 -0.57735026 0.57735026 -0.57735026 0.57735026 0.57735026 -0.35682213 2.0742793e-016 -0.93417233
		 -0.57735026 0.57735026 -0.57735026 -0.57735026 -0.57735026 -0.57735026 0 -0.93417233 0.35682213
		 0 0.93417233 0.35682213 0 0.93417233 -0.35682213 0 -0.93417233 -0.35682213;
	setAttr -s 30 ".ed[0:29]"  0 4 0 4 2 0 2 3 0 3 1 0 1 0 0 7 5 0 5 6 0
		 6 0 0 1 7 0 6 16 0 16 19 0 19 4 0 5 10 0 10 11 0 11 16 0 11 8 0 8 15 0 15 19 0 15 13 0
		 13 2 0 13 14 0 14 18 0 18 3 0 18 17 0 17 7 0 17 12 0 12 10 0 12 9 0 9 8 0 14 9 0;
	setAttr -s 12 -ch 60 ".fc[0:11]" -type "polyFaces" 
		f 5 0 1 2 3 4
		mu 0 5 6 7 8 5 3
		f 5 5 6 7 -5 8
		mu 0 5 2 13 9 6 3
		f 5 -8 9 10 11 -1
		mu 0 5 6 9 10 11 12
		f 5 12 13 14 -10 -7
		mu 0 5 13 14 15 16 9
		f 5 -15 15 16 17 -11
		mu 0 5 29 30 26 27 28
		f 5 -12 -18 18 19 -2
		mu 0 5 33 34 27 31 32
		f 5 -20 20 21 22 -3
		mu 0 5 37 31 22 35 36
		f 5 -4 -23 23 24 -9
		mu 0 5 3 4 0 1 2
		f 5 -25 25 26 -13 -6
		mu 0 5 2 17 18 19 13
		f 5 -16 -14 -27 27 28
		mu 0 5 26 15 14 25 23
		f 5 -28 -26 -24 -22 29
		mu 0 5 23 24 20 21 22
		f 5 -30 -21 -19 -17 -29
		mu 0 5 23 22 31 27 26;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "group1";
	rename -uid "1CF11667-4B62-47D8-4EE8-95AF7674C2E7";
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode transform -n "pSphere1" -p "group1";
	rename -uid "6408CF3D-43D4-EBDC-2308-7388BE30D889";
	setAttr ".rp" -type "double3" -35.124383575035026 0 88.307041944406393 ;
	setAttr ".sp" -type "double3" -35.124383575035026 0 88.307041944406393 ;
createNode transform -n "transform7" -p "pSphere1";
	rename -uid "DB322E5B-4F91-2A6D-842C-84A4B9912354";
	setAttr ".v" no;
createNode mesh -n "pSphereShape1" -p "transform7";
	rename -uid "4954F09D-4D2F-2365-C243-829E053C6F4B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder6" -p "group1";
	rename -uid "F15502BE-4C92-E8AD-205B-DEA43D98E2B2";
	setAttr ".rp" -type "double3" -35.839104027499772 0 88.42806896447668 ;
	setAttr ".sp" -type "double3" -35.839104027499772 0 88.42806896447668 ;
createNode transform -n "transform8" -p "pCylinder6";
	rename -uid "7B298533-4292-DC05-96DB-DF9E18BEEF89";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape6" -p "transform8";
	rename -uid "5049E70A-4218-5774-1360-C7B093D7F8D4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:59]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 84 ".uvst[0].uvsp[0:83]" -type "float2" 0.64860266 0.10796607
		 0.62640899 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008
		 0.45171607 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608
		 0.34374997 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893
		 0.4517161 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893
		 0.24809146 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998
		 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 42 ".pt[0:41]" -type "float3"  -39.192932 3.4943538 89.779305 
		-38.812653 3.4284143 89.85981 -38.263138 3.240164 89.855888 -37.598167 2.9480298 
		89.767921 -36.882843 2.5806077 89.604515 -36.18718 2.1738639 89.381668 -35.579277 
		1.7676133 89.121193 -35.118637 1.4016224 88.848595 -34.850353 1.111717 88.590546 
		-34.800686 0.9262749 88.372314 -34.974499 0.8634485 88.215256 -35.354774 0.92938787 
		88.13475 -35.904293 1.1176382 88.138672 -36.56926 1.4097723 88.226639 -37.284584 
		1.7771941 88.390045 -37.980247 2.1839375 88.612892 -38.58815 2.5901883 88.873367 
		-39.04879 2.9561791 89.145966 -39.317074 3.2460845 89.404015 -39.366741 3.4315267 
		89.622246 -39.144169 1.5191854 89.829636 -38.763893 1.4532461 89.910149 -38.214375 
		1.2649956 89.906219 -37.549408 0.97286135 89.818253 -36.834084 0.60543942 89.654846 
		-36.13842 0.19869566 89.431999 -35.530514 -0.207555 89.171532 -35.069878 -0.57354593 
		88.898926 -34.801594 -0.86345142 88.640884 -34.751926 -1.0488935 88.422646 -34.925739 
		-1.1117198 88.265587 -35.306015 -1.0457805 88.185081 -35.85553 -0.85753012 88.189003 
		-36.520496 -0.56539601 88.276978 -37.235825 -0.19797425 88.440384 -37.931484 0.20876932 
		88.663223 -38.539391 0.61501986 88.923698 -39.000027 0.98101074 89.196297 -39.268311 
		1.2709162 89.454346 -39.317978 1.4563582 89.672585 -37.083714 2.178901 88.997276 
		-37.034954 0.20373252 89.047615;
	setAttr -s 42 ".vt[0:41]"  0.95105714 -1 -0.30901718 0.80901754 -1 -0.5877856
		 0.5877856 -1 -0.80901748 0.30901715 -1 -0.95105702 0 -1 -1.000000476837 -0.30901715 -1 -0.95105696
		 -0.58778548 -1 -0.8090173 -0.80901724 -1 -0.58778542 -0.95105678 -1 -0.30901706 -1.000000238419 -1 0
		 -0.95105678 -1 0.30901706 -0.80901718 -1 0.58778536 -0.58778536 -1 0.80901712 -0.30901706 -1 0.95105666
		 -2.9802322e-008 -1 1.000000119209 0.30901697 -1 0.9510566 0.58778524 -1 0.80901706
		 0.809017 -1 0.5877853 0.95105654 -1 0.309017 1 -1 0 0.95105714 1 -0.30901718 0.80901754 1 -0.5877856
		 0.5877856 1 -0.80901748 0.30901715 1 -0.95105702 0 1 -1.000000476837 -0.30901715 1 -0.95105696
		 -0.58778548 1 -0.8090173 -0.80901724 1 -0.58778542 -0.95105678 1 -0.30901706 -1.000000238419 1 0
		 -0.95105678 1 0.30901706 -0.80901718 1 0.58778536 -0.58778536 1 0.80901712 -0.30901706 1 0.95105666
		 -2.9802322e-008 1 1.000000119209 0.30901697 1 0.9510566 0.58778524 1 0.80901706 0.809017 1 0.5877853
		 0.95105654 1 0.309017 1 1 0 0 -1 0 0 1 0;
	setAttr -s 100 ".ed[0:99]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 0 20 1 1 21 1 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1
		 12 32 1 13 33 1 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1
		 40 3 1 40 4 1 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1
		 40 14 1 40 15 1 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1
		 25 41 1 26 41 1 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1
		 36 41 1 37 41 1 38 41 1 39 41 1;
	setAttr -s 60 -ch 200 ".fc[0:59]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder5" -p "group1";
	rename -uid "80DBAD4A-4194-D8C9-2FEC-A197037AC8A6";
	setAttr ".rp" -type "double3" -34.692829747718676 0 88.42806896447668 ;
	setAttr ".sp" -type "double3" -34.692829747718676 2.6469779601696886e-022 88.42806896447668 ;
createNode transform -n "transform9" -p "|group1|pCylinder5";
	rename -uid "FDE7D51A-406B-5C32-13C7-EFA310FEA143";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape5" -p "transform9";
	rename -uid "CEE5F394-46C2-424B-81F4-169F72C5FA05";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder5";
	rename -uid "C3BAD672-4DF5-2286-E4CE-00BBD78012BD";
	setAttr ".t" -type "double3" 8.519120068266421 -2.2608741126694198 0 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder5Shape" -p "|pCylinder5";
	rename -uid "938D7005-4AB1-6BA9-B6FD-46BFC91A406A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder7";
	rename -uid "FD4522B3-4106-2F6C-B726-E582DA3AFCF7";
	setAttr ".t" -type "double3" 57.707223307272805 -6.7600899595481181 16.657186049227576 ;
	setAttr ".r" -type "double3" 242.89195852190548 -12.481794614870061 6.3133727348782651 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder7Shape" -p "pCylinder7";
	rename -uid "523E537F-4C03-CC6D-2DA2-318975155839";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:519]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 607 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.64860266 0.10796607 0.62640899
		 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607
		 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997
		 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161
		 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146
		 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998 0.3125
		 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.375
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.38749999 0.3125 0.39999998 0.68843985
		 0.39999998 0.3125 0.41249996 0.68843985 0.41249996 0.3125 0.42499995 0.68843985 0.42499995
		 0.3125 0.43749994 0.68843985 0.43749994 0.3125 0.44999993 0.68843985 0.44999993 0.3125
		 0.46249992 0.68843985 0.46249992 0.3125 0.4749999 0.68843985 0.4749999 0.3125 0.48749989
		 0.68843985 0.48749989 0.3125 0.49999988 0.68843985 0.49999988 0.3125 0.51249987 0.68843985
		 0.51249987 0.3125 0.52499986 0.68843985 0.52499986 0.3125 0.53749985 0.68843985 0.53749985
		 0.3125 0.54999983 0.68843985 0.54999983 0.3125 0.56249982 0.68843985 0.56249982 0.3125
		 0.57499981 0.68843985 0.57499981 0.3125 0.5874998 0.68843985 0.5874998 0.3125 0.59999979
		 0.68843985 0.59999979 0.3125 0.61249977 0.68843985 0.61249977 0.3125 0.62499976 0.68843985
		 0.62499976 0.3125 0.62640899 0.064408496 0.5 0.15000001 0.64860266 0.10796607 0.59184152
		 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607 0.0076473504 0.40815851
		 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997 0.15625 0.3513974
		 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161 0.3048526 0.5 0.3125
		 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146 0.6486026 0.2045339
		 0.65625 0.15625 0.6486026 0.89203393 0.5 0.83749998 0.62640893 0.93559146 0.59184146
		 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893 0.37359107
		 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607 0.37359107
		 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994 0.54828393
		 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607 0.65625
		 0.84375 0 0.050000001 0.050000001 0.050000001 0.050000001 0.1 0 0.1 0.1 0.050000001
		 0.1 0.1 0.15000001 0.050000001 0.15000001 0.1 0.2 0.050000001 0.2 0.1 0.25 0.050000001
		 0.25 0.1 0.30000001 0.050000001 0.30000001 0.1 0.35000002 0.050000001 0.35000002
		 0.1 0.40000004 0.050000001 0.40000004 0.1 0.45000005 0.050000001 0.45000005 0.1 0.50000006
		 0.050000001 0.50000006 0.1 0.55000007 0.050000001 0.55000007 0.1 0.60000008 0.050000001
		 0.60000008 0.1 0.6500001 0.050000001 0.6500001 0.1 0.70000011 0.050000001 0.70000011
		 0.1 0.75000012 0.050000001 0.75000012 0.1 0.80000013 0.050000001 0.80000013 0.1 0.85000014
		 0.050000001 0.85000014 0.1 0.90000015 0.050000001 0.90000015 0.1 0.95000017 0.050000001
		 0.95000017 0.1 1.000000119209 0.050000001 1.000000119209 0.1 0.050000001 0.15000001
		 0 0.15000001 0.1 0.15000001 0.15000001 0.15000001 0.2 0.15000001 0.25 0.15000001
		 0.30000001 0.15000001 0.35000002 0.15000001 0.40000004 0.15000001 0.45000005 0.15000001
		 0.50000006 0.15000001 0.55000007 0.15000001 0.60000008 0.15000001 0.6500001 0.15000001
		 0.70000011 0.15000001 0.75000012 0.15000001 0.80000013 0.15000001 0.85000014 0.15000001
		 0.90000015 0.15000001 0.95000017 0.15000001 1.000000119209 0.15000001 0.050000001
		 0.2 0 0.2 0.1 0.2 0.15000001 0.2 0.2 0.2 0.25 0.2 0.30000001 0.2 0.35000002 0.2 0.40000004
		 0.2 0.45000005 0.2 0.50000006 0.2 0.55000007 0.2 0.60000008 0.2 0.6500001 0.2 0.70000011
		 0.2 0.75000012 0.2 0.80000013 0.2 0.85000014 0.2 0.90000015 0.2;
	setAttr ".uvst[0].uvsp[250:499]" 0.95000017 0.2 1.000000119209 0.2 0.050000001
		 0.25 0 0.25 0.1 0.25 0.15000001 0.25 0.2 0.25 0.25 0.25 0.30000001 0.25 0.35000002
		 0.25 0.40000004 0.25 0.45000005 0.25 0.50000006 0.25 0.55000007 0.25 0.60000008 0.25
		 0.6500001 0.25 0.70000011 0.25 0.75000012 0.25 0.80000013 0.25 0.85000014 0.25 0.90000015
		 0.25 0.95000017 0.25 1.000000119209 0.25 0.050000001 0.30000001 0 0.30000001 0.1
		 0.30000001 0.15000001 0.30000001 0.2 0.30000001 0.25 0.30000001 0.30000001 0.30000001
		 0.35000002 0.30000001 0.40000004 0.30000001 0.45000005 0.30000001 0.50000006 0.30000001
		 0.55000007 0.30000001 0.60000008 0.30000001 0.6500001 0.30000001 0.70000011 0.30000001
		 0.75000012 0.30000001 0.80000013 0.30000001 0.85000014 0.30000001 0.90000015 0.30000001
		 0.95000017 0.30000001 1.000000119209 0.30000001 0.050000001 0.35000002 0 0.35000002
		 0.1 0.35000002 0.15000001 0.35000002 0.2 0.35000002 0.25 0.35000002 0.30000001 0.35000002
		 0.35000002 0.35000002 0.40000004 0.35000002 0.45000005 0.35000002 0.50000006 0.35000002
		 0.55000007 0.35000002 0.60000008 0.35000002 0.6500001 0.35000002 0.70000011 0.35000002
		 0.75000012 0.35000002 0.80000013 0.35000002 0.85000014 0.35000002 0.90000015 0.35000002
		 0.95000017 0.35000002 1.000000119209 0.35000002 0.050000001 0.40000004 0 0.40000004
		 0.1 0.40000004 0.15000001 0.40000004 0.2 0.40000004 0.25 0.40000004 0.30000001 0.40000004
		 0.35000002 0.40000004 0.40000004 0.40000004 0.45000005 0.40000004 0.50000006 0.40000004
		 0.55000007 0.40000004 0.60000008 0.40000004 0.6500001 0.40000004 0.70000011 0.40000004
		 0.75000012 0.40000004 0.80000013 0.40000004 0.85000014 0.40000004 0.90000015 0.40000004
		 0.95000017 0.40000004 1.000000119209 0.40000004 0.050000001 0.45000005 0 0.45000005
		 0.1 0.45000005 0.15000001 0.45000005 0.2 0.45000005 0.25 0.45000005 0.30000001 0.45000005
		 0.35000002 0.45000005 0.40000004 0.45000005 0.45000005 0.45000005 0.50000006 0.45000005
		 0.55000007 0.45000005 0.60000008 0.45000005 0.6500001 0.45000005 0.70000011 0.45000005
		 0.75000012 0.45000005 0.80000013 0.45000005 0.85000014 0.45000005 0.90000015 0.45000005
		 0.95000017 0.45000005 1.000000119209 0.45000005 0.050000001 0.50000006 0 0.50000006
		 0.1 0.50000006 0.15000001 0.50000006 0.2 0.50000006 0.25 0.50000006 0.30000001 0.50000006
		 0.35000002 0.50000006 0.40000004 0.50000006 0.45000005 0.50000006 0.50000006 0.50000006
		 0.55000007 0.50000006 0.60000008 0.50000006 0.6500001 0.50000006 0.70000011 0.50000006
		 0.75000012 0.50000006 0.80000013 0.50000006 0.85000014 0.50000006 0.90000015 0.50000006
		 0.95000017 0.50000006 1.000000119209 0.50000006 0.050000001 0.55000007 0 0.55000007
		 0.1 0.55000007 0.15000001 0.55000007 0.2 0.55000007 0.25 0.55000007 0.30000001 0.55000007
		 0.35000002 0.55000007 0.40000004 0.55000007 0.45000005 0.55000007 0.50000006 0.55000007
		 0.55000007 0.55000007 0.60000008 0.55000007 0.6500001 0.55000007 0.70000011 0.55000007
		 0.75000012 0.55000007 0.80000013 0.55000007 0.85000014 0.55000007 0.90000015 0.55000007
		 0.95000017 0.55000007 1.000000119209 0.55000007 0.050000001 0.60000008 0 0.60000008
		 0.1 0.60000008 0.15000001 0.60000008 0.2 0.60000008 0.25 0.60000008 0.30000001 0.60000008
		 0.35000002 0.60000008 0.40000004 0.60000008 0.45000005 0.60000008 0.50000006 0.60000008
		 0.55000007 0.60000008 0.60000008 0.60000008 0.6500001 0.60000008 0.70000011 0.60000008
		 0.75000012 0.60000008 0.80000013 0.60000008 0.85000014 0.60000008 0.90000015 0.60000008
		 0.95000017 0.60000008 1.000000119209 0.60000008 0.050000001 0.6500001 0 0.6500001
		 0.1 0.6500001 0.15000001 0.6500001 0.2 0.6500001 0.25 0.6500001 0.30000001 0.6500001
		 0.35000002 0.6500001 0.40000004 0.6500001 0.45000005 0.6500001 0.50000006 0.6500001
		 0.55000007 0.6500001 0.60000008 0.6500001 0.6500001 0.6500001 0.70000011 0.6500001
		 0.75000012 0.6500001 0.80000013 0.6500001 0.85000014 0.6500001 0.90000015 0.6500001
		 0.95000017 0.6500001 1.000000119209 0.6500001 0.050000001 0.70000011 0 0.70000011
		 0.1 0.70000011 0.15000001 0.70000011 0.2 0.70000011 0.25 0.70000011 0.30000001 0.70000011
		 0.35000002 0.70000011 0.40000004 0.70000011 0.45000005 0.70000011 0.50000006 0.70000011
		 0.55000007 0.70000011 0.60000008 0.70000011 0.6500001 0.70000011 0.70000011 0.70000011
		 0.75000012 0.70000011 0.80000013 0.70000011 0.85000014 0.70000011 0.90000015 0.70000011
		 0.95000017 0.70000011 1.000000119209 0.70000011 0.050000001 0.75000012 0 0.75000012
		 0.1 0.75000012 0.15000001 0.75000012 0.2 0.75000012 0.25 0.75000012 0.30000001 0.75000012
		 0.35000002 0.75000012 0.40000004 0.75000012 0.45000005 0.75000012 0.50000006 0.75000012
		 0.55000007 0.75000012 0.60000008 0.75000012 0.6500001 0.75000012 0.70000011 0.75000012
		 0.75000012 0.75000012 0.80000013 0.75000012 0.85000014 0.75000012 0.90000015 0.75000012
		 0.95000017 0.75000012 1.000000119209 0.75000012 0.050000001 0.80000013 0 0.80000013
		 0.1 0.80000013 0.15000001 0.80000013 0.2 0.80000013 0.25 0.80000013 0.30000001 0.80000013
		 0.35000002 0.80000013 0.40000004 0.80000013 0.45000005 0.80000013 0.50000006 0.80000013
		 0.55000007 0.80000013 0.60000008 0.80000013 0.6500001 0.80000013 0.70000011 0.80000013
		 0.75000012 0.80000013 0.80000013 0.80000013;
	setAttr ".uvst[0].uvsp[500:606]" 0.85000014 0.80000013 0.90000015 0.80000013
		 0.95000017 0.80000013 1.000000119209 0.80000013 0.050000001 0.85000014 0 0.85000014
		 0.1 0.85000014 0.15000001 0.85000014 0.2 0.85000014 0.25 0.85000014 0.30000001 0.85000014
		 0.35000002 0.85000014 0.40000004 0.85000014 0.45000005 0.85000014 0.50000006 0.85000014
		 0.55000007 0.85000014 0.60000008 0.85000014 0.6500001 0.85000014 0.70000011 0.85000014
		 0.75000012 0.85000014 0.80000013 0.85000014 0.85000014 0.85000014 0.90000015 0.85000014
		 0.95000017 0.85000014 1.000000119209 0.85000014 0.050000001 0.90000015 0 0.90000015
		 0.1 0.90000015 0.15000001 0.90000015 0.2 0.90000015 0.25 0.90000015 0.30000001 0.90000015
		 0.35000002 0.90000015 0.40000004 0.90000015 0.45000005 0.90000015 0.50000006 0.90000015
		 0.55000007 0.90000015 0.60000008 0.90000015 0.6500001 0.90000015 0.70000011 0.90000015
		 0.75000012 0.90000015 0.80000013 0.90000015 0.85000014 0.90000015 0.90000015 0.90000015
		 0.95000017 0.90000015 1.000000119209 0.90000015 0.050000001 0.95000017 0 0.95000017
		 0.1 0.95000017 0.15000001 0.95000017 0.2 0.95000017 0.25 0.95000017 0.30000001 0.95000017
		 0.35000002 0.95000017 0.40000004 0.95000017 0.45000005 0.95000017 0.50000006 0.95000017
		 0.55000007 0.95000017 0.60000008 0.95000017 0.6500001 0.95000017 0.70000011 0.95000017
		 0.75000012 0.95000017 0.80000013 0.95000017 0.85000014 0.95000017 0.90000015 0.95000017
		 0.95000017 0.95000017 1.000000119209 0.95000017 0.025 0 0.075000003 0 0.125 0 0.175
		 0 0.22500001 0 0.27500001 0 0.32500002 0 0.375 0 0.42500001 0 0.47499999 0 0.52500004
		 0 0.57499999 0 0.625 0 0.67500001 0 0.72500002 0 0.77500004 0 0.82499999 0 0.875
		 0 0.92500001 0 0.97500002 0 0.025 1 0.075000003 1 0.125 1 0.175 1 0.22500001 1 0.27500001
		 1 0.32500002 1 0.375 1 0.42500001 1 0.47499999 1 0.52500004 1 0.57499999 1 0.625
		 1 0.67500001 1 0.72500002 1 0.77500004 1 0.82499999 1 0.875 1 0.92500001 1 0.97500002
		 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 466 ".vt";
	setAttr ".vt[0:165]"  -31.42638397 0.15162086 89.97360229 -31.61052513 0.26360402 89.74130249
		 -31.94010735 0.35247436 89.46312714 -32.38286972 0.40953261 89.16630554 -32.89547348 0.4291935 88.87989807
		 -33.42773819 0.40953258 88.63193512 -33.92756271 0.3524743 88.44669342 -34.34602356 0.26360396 88.34230042
		 -34.64215469 0.15162082 88.32897949 -34.78697205 0.0274866 88.40803528 -34.7663002 -0.09664762 88.57172394
		 -34.58216095 -0.20863073 88.80403137 -34.25257874 -0.29750097 89.082206726 -33.80981445 -0.35455921 89.37902069
		 -33.29721451 -0.37422013 89.66543579 -32.7649498 -0.35455918 89.91339111 -32.26512146 -0.29750094 90.098632813
		 -31.84666252 -0.2086307 90.20302582 -31.55052948 -0.096647598 90.21634674 -31.40571213 0.0274866 90.13729095
		 -31.40359116 0.096647523 89.929039 -31.58773232 0.20863067 89.69673157 -31.91731453 0.297501 89.41855621
		 -32.3600769 0.35455924 89.12174225 -32.87267685 0.37422013 88.83532715 -33.40494156 0.35455921 88.58737183
		 -33.9047699 0.29750094 88.4021225 -34.32322693 0.20863059 88.29773712 -34.61936188 0.096647471 88.2844162
		 -34.76417923 -0.027486745 88.36347198 -34.74350739 -0.15162097 88.52716064 -34.55936432 -0.26360404 88.75946045
		 -34.2297821 -0.35247433 89.037635803 -33.78702164 -0.40953258 89.3344574 -33.27441788 -0.4291935 89.62086487
		 -32.74215698 -0.40953255 89.86882782 -32.24232864 -0.3524743 90.054069519 -31.82386971 -0.26360402 90.15846252
		 -31.52773666 -0.15162094 90.17178345 -31.38291931 -0.027486745 90.092727661 -33.096343994 0.0274866 89.27266693
		 -33.073547363 -0.027486745 89.22809601 -38.24187469 2.49435377 89.47029114 -38.0036354065 2.42841434 89.27202606
		 -37.675354 2.24016404 89.046867371 -37.28915024 1.94802976 88.81686401 -36.88284302 1.58060765 88.60451508
		 -36.49619675 1.17386389 88.43061066 -36.16706085 0.76761329 88.31217194 -35.92765427 0.40162241 88.26081085
		 -35.80141068 0.11171699 88.28153229 -35.80068588 -0.073725104 88.37231445 -35.92555618 -0.1365515 88.5242691
		 -36.16379166 -0.070612133 88.72253418 -36.49207687 0.11763823 88.94768524 -36.87827682 0.40977228 89.17769623
		 -37.28458405 0.77719414 89.39004517 -37.67123032 1.18393755 89.56394958 -38.00036621094 1.59018826 89.68238068
		 -38.2397728 1.95617914 89.73374939 -38.36601639 2.24608445 89.71302795 -38.36674118 2.43152666 89.62224579
		 -38.19311142 2.51918554 89.52061462 -37.95487595 2.45324612 89.32236481 -37.62659073 2.26499557 89.097198486
		 -37.24039078 1.97286129 88.86719513 -36.83408356 1.60543942 88.65484619 -36.44743729 1.19869566 88.48094177
		 -36.11829758 0.792445 88.36251068 -35.87889481 0.42645407 88.31114197 -35.75265121 0.13654858 88.33187103
		 -35.75192642 -0.048893452 88.42264557 -35.87679672 -0.11171985 88.57460022 -36.1150322 -0.04578054 88.7728653
		 -36.4433136 0.14246988 88.99801636 -36.82951355 0.43460399 89.22803497 -37.23582458 0.80202574 89.44038391
		 -37.62246704 1.20876932 89.6142807 -37.95160675 1.6150198 89.73271179 -38.19100952 1.98101068 89.78408051
		 -38.31725311 2.27091622 89.76335907 -38.31797791 2.45635819 89.67258453 -37.083713531 1.17890096 88.99727631
		 -37.034954071 1.20373249 89.047615051 -34.97560501 -0.98768836 88.25870514 -34.99782562 -0.98768836 88.21509552
		 -35.032432556 -0.98768836 88.18048859 -35.076042175 -0.98768836 88.15826416 -35.12438202 -0.98768836 88.15061188
		 -35.17272186 -0.98768836 88.15826416 -35.21633148 -0.98768836 88.18048859 -35.25093842 -0.98768836 88.21509552
		 -35.27315903 -0.98768836 88.25870514 -35.28081512 -0.98768836 88.30704498 -35.27315903 -0.98768836 88.35538483
		 -35.25093842 -0.98768836 88.39899445 -35.21633148 -0.98768836 88.43360138 -35.17272186 -0.98768836 88.45582581
		 -35.12438202 -0.98768836 88.46347809 -35.076042175 -0.98768836 88.45582581 -35.032432556 -0.98768836 88.43360138
		 -34.99782562 -0.98768836 88.39899445 -34.97560501 -0.98768836 88.35538483 -34.96794891 -0.98768836 88.30704498
		 -34.83049011 -0.95105654 88.21155548 -34.87438202 -0.95105654 88.12541199 -34.94274521 -0.95105654 88.057044983
		 -35.028888702 -0.95105654 88.013153076 -35.12438202 -0.95105654 87.99803162 -35.21987534 -0.95105654 88.013153076
		 -35.30601883 -0.95105654 88.057044983 -35.37438202 -0.95105654 88.12541199 -35.41827393 -0.95105654 88.21155548
		 -35.4333992 -0.95105654 88.30704498 -35.41827393 -0.95105654 88.40253448 -35.37438202 -0.95105654 88.48867798
		 -35.30601883 -0.95105654 88.55704498 -35.21987534 -0.95105654 88.60093689 -35.12438202 -0.95105654 88.61605835
		 -35.028888702 -0.95105654 88.60093689 -34.94274521 -0.95105654 88.55704498 -34.87438202 -0.95105654 88.48867798
		 -34.83049011 -0.95105654 88.40253448 -34.81536484 -0.95105654 88.30704498 -34.69261169 -0.89100653 88.16675568
		 -34.75709534 -0.89100653 88.04019928 -34.8575325 -0.89100653 87.9397583 -34.98409271 -0.89100653 87.87527466
		 -35.12438202 -0.89100653 87.85305786 -35.26467133 -0.89100653 87.87527466 -35.39123154 -0.89100653 87.9397583
		 -35.4916687 -0.89100653 88.04019928 -35.55615234 -0.89100653 88.16675568 -35.57837296 -0.89100653 88.30704498
		 -35.55615234 -0.89100653 88.44733429 -35.4916687 -0.89100653 88.57389069 -35.39123154 -0.89100653 88.67433167
		 -35.26467133 -0.89100653 88.73881531 -35.12438202 -0.89100653 88.7610321 -34.98409271 -0.89100653 88.73881531
		 -34.8575325 -0.89100653 88.67433167 -34.75709534 -0.89100653 88.57389069 -34.69261169 -0.89100653 88.44733429
		 -34.67039108 -0.89100653 88.30704498 -34.56536484 -0.809017 88.12541199 -34.6488533 -0.809017 87.96155548
		 -34.7788887 -0.809017 87.83152008 -34.94274521 -0.809017 87.74802399 -35.12438202 -0.809017 87.71926117
		 -35.30601883 -0.809017 87.74802399 -35.46987534 -0.809017 87.83152008 -35.59991074 -0.809017 87.96155548
		 -35.6833992 -0.809017 88.12541199 -35.71216583 -0.809017 88.30704498 -35.6833992 -0.809017 88.48867798
		 -35.59991074 -0.809017 88.65253448 -35.46987534 -0.809017 88.78256989 -35.30601883 -0.809017 88.86605835
		 -35.12438202 -0.809017 88.8948288 -34.94274521 -0.809017 88.86605835 -34.7788887 -0.809017 88.78256989
		 -34.6488533 -0.809017 88.65253448 -34.56536484 -0.809017 88.48867798 -34.53659821 -0.809017 88.30704498
		 -34.45188141 -0.70710677 88.088539124 -34.55231857 -0.70710677 87.89141846;
	setAttr ".vt[166:331]" -34.70875549 -0.70710677 87.73498535 -34.90587234 -0.70710677 87.63454437
		 -35.12438202 -0.70710677 87.59993744 -35.34289169 -0.70710677 87.63454437 -35.54000854 -0.70710677 87.73498535
		 -35.69644165 -0.70710677 87.89141846 -35.79688263 -0.70710677 88.088539124 -35.83148956 -0.70710677 88.30704498
		 -35.79688263 -0.70710677 88.52555084 -35.69644165 -0.70710677 88.72267151 -35.54000854 -0.70710677 88.87910461
		 -35.34289169 -0.70710677 88.97954559 -35.12438202 -0.70710677 89.014152527 -34.90587234 -0.70710677 88.97954559
		 -34.70875549 -0.70710677 88.87910461 -34.55232239 -0.70710677 88.72267151 -34.45188522 -0.70710677 88.52555084
		 -34.41727448 -0.70710677 88.30704498 -34.3549614 -0.58778524 88.057044983 -34.46987152 -0.58778524 87.83152008
		 -34.6488533 -0.58778524 87.65253448 -34.87438202 -0.58778524 87.53762054 -35.12438202 -0.58778524 87.49802399
		 -35.37438202 -0.58778524 87.53762054 -35.59991074 -0.58778524 87.65253448 -35.77889252 -0.58778524 87.83152008
		 -35.89380264 -0.58778524 88.057044983 -35.9333992 -0.58778524 88.30704498 -35.89380264 -0.58778524 88.55704498
		 -35.77889252 -0.58778524 88.78256989 -35.59991074 -0.58778524 88.96155548 -35.37438202 -0.58778524 89.076469421
		 -35.12438202 -0.58778524 89.11605835 -34.87438202 -0.58778524 89.076469421 -34.6488533 -0.58778524 88.96155548
		 -34.46987534 -0.58778524 88.78256989 -34.3549614 -0.58778524 88.55704498 -34.31536484 -0.58778524 88.30704498
		 -34.27698517 -0.45399052 88.031707764 -34.40354156 -0.45399052 87.7833252 -34.60066223 -0.45399052 87.58620453
		 -34.8490448 -0.45399052 87.45964813 -35.12438202 -0.45399052 87.41603851 -35.39971924 -0.45399052 87.45964813
		 -35.64810181 -0.45399052 87.58620453 -35.84522247 -0.45399052 87.7833252 -35.97177887 -0.45399052 88.031707764
		 -36.015388489 -0.45399052 88.30704498 -35.97177887 -0.45399052 88.5823822 -35.84522247 -0.45399052 88.83076477
		 -35.64810181 -0.45399052 89.027885437 -35.39971924 -0.45399052 89.15444183 -35.12438202 -0.45399052 89.19805145
		 -34.8490448 -0.45399052 89.15444183 -34.60066223 -0.45399052 89.027885437 -34.40354156 -0.45399052 88.83076477
		 -34.27698517 -0.45399052 88.5823822 -34.23337555 -0.45399052 88.30704498 -34.21987152 -0.30901697 88.013153076
		 -34.3549614 -0.30901697 87.74802399 -34.56536484 -0.30901697 87.53762054 -34.83049011 -0.30901697 87.40253448
		 -35.12438202 -0.30901697 87.35598755 -35.41827393 -0.30901697 87.40253448 -35.6833992 -0.30901697 87.53762054
		 -35.89380264 -0.30901697 87.74803162 -36.028892517 -0.30901697 88.013153076 -36.075439453 -0.30901697 88.30704498
		 -36.028892517 -0.30901697 88.60093689 -35.89380264 -0.30901697 88.86605835 -35.6833992 -0.30901697 89.076469421
		 -35.41827393 -0.30901697 89.21155548 -35.12438202 -0.30901697 89.25810242 -34.83049011 -0.30901697 89.21155548
		 -34.56536484 -0.30901697 89.076469421 -34.3549614 -0.30901697 88.86605835 -34.21987534 -0.30901697 88.60093689
		 -34.17332458 -0.30901697 88.30704498 -34.18503571 -0.15643437 88.0018310547 -34.32532501 -0.15643437 87.72649384
		 -34.54383469 -0.15643437 87.50798798 -34.81916809 -0.15643437 87.36769867 -35.12438202 -0.15643437 87.31935883
		 -35.42959595 -0.15643437 87.36769867 -35.70492935 -0.15643437 87.50798798 -35.92343903 -0.15643437 87.72649384
		 -36.063728333 -0.15643437 88.0018310547 -36.11207199 -0.15643437 88.30704498 -36.063728333 -0.15643437 88.61225891
		 -35.92343903 -0.15643437 88.88759613 -35.70492935 -0.15643437 89.10610199 -35.42959595 -0.15643437 89.2463913
		 -35.12438202 -0.15643437 89.29473114 -34.81916809 -0.15643437 89.2463913 -34.54383469 -0.15643437 89.10610199
		 -34.32532501 -0.15643437 88.88759613 -34.18503571 -0.15643437 88.61225891 -34.13669205 -0.15643437 88.30704498
		 -34.17332458 0 87.99803162 -34.31536484 0 87.71926117 -34.53659821 0 87.49802399
		 -34.81536484 0 87.35598755 -35.12438202 0 87.30704498 -35.4333992 0 87.35598755 -35.71216583 0 87.49802399
		 -35.9333992 0 87.71926117 -36.075439453 0 87.99803162 -36.12438202 0 88.30704498
		 -36.075439453 0 88.61605835 -35.9333992 0 88.8948288 -35.71216583 0 89.11605835 -35.4333992 0 89.25810242
		 -35.12438202 0 89.30704498 -34.81536484 0 89.25810242 -34.53659821 0 89.11605835
		 -34.31536484 0 88.8948288 -34.17332458 0 88.61605835 -34.12438202 0 88.30704498 -34.18503571 0.15643437 88.0018310547
		 -34.32532501 0.15643437 87.72649384 -34.54383469 0.15643437 87.50798798 -34.81916809 0.15643437 87.36769867
		 -35.12438202 0.15643437 87.31935883 -35.42959595 0.15643437 87.36769867 -35.70492935 0.15643437 87.50798798
		 -35.92343903 0.15643437 87.72649384 -36.063728333 0.15643437 88.0018310547 -36.11207199 0.15643437 88.30704498
		 -36.063728333 0.15643437 88.61225891 -35.92343903 0.15643437 88.88759613 -35.70492935 0.15643437 89.10610199
		 -35.42959595 0.15643437 89.2463913 -35.12438202 0.15643437 89.29473114 -34.81916809 0.15643437 89.2463913
		 -34.54383469 0.15643437 89.10610199 -34.32532501 0.15643437 88.88759613 -34.18503571 0.15643437 88.61225891
		 -34.13669205 0.15643437 88.30704498 -34.21987152 0.30901697 88.013153076 -34.3549614 0.30901697 87.74802399
		 -34.56536484 0.30901697 87.53762054 -34.83049011 0.30901697 87.40253448 -35.12438202 0.30901697 87.35598755
		 -35.41827393 0.30901697 87.40253448 -35.6833992 0.30901697 87.53762054 -35.89380264 0.30901697 87.74803162
		 -36.028892517 0.30901697 88.013153076 -36.075439453 0.30901697 88.30704498 -36.028892517 0.30901697 88.60093689
		 -35.89380264 0.30901697 88.86605835 -35.6833992 0.30901697 89.076469421 -35.41827393 0.30901697 89.21155548
		 -35.12438202 0.30901697 89.25810242 -34.83049011 0.30901697 89.21155548 -34.56536484 0.30901697 89.076469421
		 -34.3549614 0.30901697 88.86605835 -34.21987534 0.30901697 88.60093689 -34.17332458 0.30901697 88.30704498
		 -34.27698517 0.45399052 88.031707764 -34.40354156 0.45399052 87.7833252 -34.60066223 0.45399052 87.58620453
		 -34.8490448 0.45399052 87.45964813 -35.12438202 0.45399052 87.41603851 -35.39971924 0.45399052 87.45964813
		 -35.64810181 0.45399052 87.58620453 -35.84522247 0.45399052 87.7833252;
	setAttr ".vt[332:465]" -35.97177887 0.45399052 88.031707764 -36.015388489 0.45399052 88.30704498
		 -35.97177887 0.45399052 88.5823822 -35.84522247 0.45399052 88.83076477 -35.64810181 0.45399052 89.027885437
		 -35.39971924 0.45399052 89.15444183 -35.12438202 0.45399052 89.19805145 -34.8490448 0.45399052 89.15444183
		 -34.60066223 0.45399052 89.027885437 -34.40354156 0.45399052 88.83076477 -34.27698517 0.45399052 88.5823822
		 -34.23337555 0.45399052 88.30704498 -34.3549614 0.58778524 88.057044983 -34.46987152 0.58778524 87.83152008
		 -34.6488533 0.58778524 87.65253448 -34.87438202 0.58778524 87.53762054 -35.12438202 0.58778524 87.49802399
		 -35.37438202 0.58778524 87.53762054 -35.59991074 0.58778524 87.65253448 -35.77889252 0.58778524 87.83152008
		 -35.89380264 0.58778524 88.057044983 -35.9333992 0.58778524 88.30704498 -35.89380264 0.58778524 88.55704498
		 -35.77889252 0.58778524 88.78256989 -35.59991074 0.58778524 88.96155548 -35.37438202 0.58778524 89.076469421
		 -35.12438202 0.58778524 89.11605835 -34.87438202 0.58778524 89.076469421 -34.6488533 0.58778524 88.96155548
		 -34.46987534 0.58778524 88.78256989 -34.3549614 0.58778524 88.55704498 -34.31536484 0.58778524 88.30704498
		 -34.45188141 0.70710677 88.088539124 -34.55231857 0.70710677 87.89141846 -34.70875549 0.70710677 87.73498535
		 -34.90587234 0.70710677 87.63454437 -35.12438202 0.70710677 87.59993744 -35.34289169 0.70710677 87.63454437
		 -35.54000854 0.70710677 87.73498535 -35.69644165 0.70710677 87.89141846 -35.79688263 0.70710677 88.088539124
		 -35.83148956 0.70710677 88.30704498 -35.79688263 0.70710677 88.52555084 -35.69644165 0.70710677 88.72267151
		 -35.54000854 0.70710677 88.87910461 -35.34289169 0.70710677 88.97954559 -35.12438202 0.70710677 89.014152527
		 -34.90587234 0.70710677 88.97954559 -34.70875549 0.70710677 88.87910461 -34.55232239 0.70710677 88.72267151
		 -34.45188522 0.70710677 88.52555084 -34.41727448 0.70710677 88.30704498 -34.56536484 0.809017 88.12541199
		 -34.6488533 0.809017 87.96155548 -34.7788887 0.809017 87.83152008 -34.94274521 0.809017 87.74802399
		 -35.12438202 0.809017 87.71926117 -35.30601883 0.809017 87.74802399 -35.46987534 0.809017 87.83152008
		 -35.59991074 0.809017 87.96155548 -35.6833992 0.809017 88.12541199 -35.71216583 0.809017 88.30704498
		 -35.6833992 0.809017 88.48867798 -35.59991074 0.809017 88.65253448 -35.46987534 0.809017 88.78256989
		 -35.30601883 0.809017 88.86605835 -35.12438202 0.809017 88.8948288 -34.94274521 0.809017 88.86605835
		 -34.7788887 0.809017 88.78256989 -34.6488533 0.809017 88.65253448 -34.56536484 0.809017 88.48867798
		 -34.53659821 0.809017 88.30704498 -34.69261169 0.89100653 88.16675568 -34.75709534 0.89100653 88.04019928
		 -34.8575325 0.89100653 87.9397583 -34.98409271 0.89100653 87.87527466 -35.12438202 0.89100653 87.85305786
		 -35.26467133 0.89100653 87.87527466 -35.39123154 0.89100653 87.9397583 -35.4916687 0.89100653 88.04019928
		 -35.55615234 0.89100653 88.16675568 -35.57837296 0.89100653 88.30704498 -35.55615234 0.89100653 88.44733429
		 -35.4916687 0.89100653 88.57389069 -35.39123154 0.89100653 88.67433167 -35.26467133 0.89100653 88.73881531
		 -35.12438202 0.89100653 88.7610321 -34.98409271 0.89100653 88.73881531 -34.8575325 0.89100653 88.67433167
		 -34.75709534 0.89100653 88.57389069 -34.69261169 0.89100653 88.44733429 -34.67039108 0.89100653 88.30704498
		 -34.83049011 0.95105654 88.21155548 -34.87438202 0.95105654 88.12541199 -34.94274521 0.95105654 88.057044983
		 -35.028888702 0.95105654 88.013153076 -35.12438202 0.95105654 87.99803162 -35.21987534 0.95105654 88.013153076
		 -35.30601883 0.95105654 88.057044983 -35.37438202 0.95105654 88.12541199 -35.41827393 0.95105654 88.21155548
		 -35.4333992 0.95105654 88.30704498 -35.41827393 0.95105654 88.40253448 -35.37438202 0.95105654 88.48867798
		 -35.30601883 0.95105654 88.55704498 -35.21987534 0.95105654 88.60093689 -35.12438202 0.95105654 88.61605835
		 -35.028888702 0.95105654 88.60093689 -34.94274521 0.95105654 88.55704498 -34.87438202 0.95105654 88.48867798
		 -34.83049011 0.95105654 88.40253448 -34.81536484 0.95105654 88.30704498 -34.97560501 0.98768836 88.25870514
		 -34.99782562 0.98768836 88.21509552 -35.032432556 0.98768836 88.18048859 -35.076042175 0.98768836 88.15826416
		 -35.12438202 0.98768836 88.15061188 -35.17272186 0.98768836 88.15826416 -35.21633148 0.98768836 88.18048859
		 -35.25093842 0.98768836 88.21509552 -35.27315903 0.98768836 88.25870514 -35.28081512 0.98768836 88.30704498
		 -35.27315903 0.98768836 88.35538483 -35.25093842 0.98768836 88.39899445 -35.21633148 0.98768836 88.43360138
		 -35.17272186 0.98768836 88.45582581 -35.12438202 0.98768836 88.46347809 -35.076042175 0.98768836 88.45582581
		 -35.032432556 0.98768836 88.43360138 -34.99782562 0.98768836 88.39899445 -34.97560501 0.98768836 88.35538483
		 -34.96794891 0.98768836 88.30704498 -35.12438202 -1 88.30704498 -35.12438202 1 88.30704498;
	setAttr -s 980 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0 0 20 1 1 21 1
		 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1
		 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1 40 3 1 40 4 1
		 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1 40 14 1 40 15 1
		 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1 25 41 1 26 41 1
		 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1 36 41 1 37 41 1
		 38 41 1 39 41 1 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 62 0 42 62 1 43 63 1
		 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1
		 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 82 42 1 82 43 1 82 44 1 82 45 1
		 82 46 1 82 47 1;
	setAttr ".ed[166:331]" 82 48 1 82 49 1 82 50 1 82 51 1 82 52 1 82 53 1 82 54 1
		 82 55 1 82 56 1 82 57 1 82 58 1 82 59 1 82 60 1 82 61 1 62 83 1 63 83 1 64 83 1 65 83 1
		 66 83 1 67 83 1 68 83 1 69 83 1 70 83 1 71 83 1 72 83 1 73 83 1 74 83 1 75 83 1 76 83 1
		 77 83 1 78 83 1 79 83 1 80 83 1 81 83 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1
		 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 84 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1
		 109 110 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 104 1 124 125 1 125 126 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 134 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 124 1 144 145 1
		 145 146 1 146 147 1 147 148 1 148 149 1 149 150 1 150 151 1 151 152 1 152 153 1 153 154 1
		 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1
		 163 144 1 164 165 1 165 166 1 166 167 1 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 180 1 180 181 1
		 181 182 1 182 183 1 183 164 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 190 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 200 1 200 201 1 201 202 1 202 203 1 203 184 1 204 205 1 205 206 1 206 207 1 207 208 1
		 208 209 1 209 210 1 210 211 1 211 212 1 212 213 1 213 214 1 214 215 1 215 216 1;
	setAttr ".ed[332:497]" 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1
		 222 223 1 223 204 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 232 1 232 233 1 233 234 1 234 235 1 235 236 1 236 237 1 237 238 1 238 239 1 239 240 1
		 240 241 1 241 242 1 242 243 1 243 224 1 244 245 1 245 246 1 246 247 1 247 248 1 248 249 1
		 249 250 1 250 251 1 251 252 1 252 253 1 253 254 1 254 255 1 255 256 1 256 257 1 257 258 1
		 258 259 1 259 260 1 260 261 1 261 262 1 262 263 1 263 244 1 264 265 1 265 266 1 266 267 1
		 267 268 1 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1 274 275 1 275 276 1
		 276 277 1 277 278 1 278 279 1 279 280 1 280 281 1 281 282 1 282 283 1 283 264 1 284 285 1
		 285 286 1 286 287 1 287 288 1 288 289 1 289 290 1 290 291 1 291 292 1 292 293 1 293 294 1
		 294 295 1 295 296 1 296 297 1 297 298 1 298 299 1 299 300 1 300 301 1 301 302 1 302 303 1
		 303 284 1 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1
		 312 313 1 313 314 1 314 315 1 315 316 1 316 317 1 317 318 1 318 319 1 319 320 1 320 321 1
		 321 322 1 322 323 1 323 304 1 324 325 1 325 326 1 326 327 1 327 328 1 328 329 1 329 330 1
		 330 331 1 331 332 1 332 333 1 333 334 1 334 335 1 335 336 1 336 337 1 337 338 1 338 339 1
		 339 340 1 340 341 1 341 342 1 342 343 1 343 324 1 344 345 1 345 346 1 346 347 1 347 348 1
		 348 349 1 349 350 1 350 351 1 351 352 1 352 353 1 353 354 1 354 355 1 355 356 1 356 357 1
		 357 358 1 358 359 1 359 360 1 360 361 1 361 362 1 362 363 1 363 344 1 364 365 1 365 366 1
		 366 367 1 367 368 1 368 369 1 369 370 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1
		 375 376 1 376 377 1 377 378 1 378 379 1 379 380 1 380 381 1 381 382 1;
	setAttr ".ed[498:663]" 382 383 1 383 364 1 384 385 1 385 386 1 386 387 1 387 388 1
		 388 389 1 389 390 1 390 391 1 391 392 1 392 393 1 393 394 1 394 395 1 395 396 1 396 397 1
		 397 398 1 398 399 1 399 400 1 400 401 1 401 402 1 402 403 1 403 384 1 404 405 1 405 406 1
		 406 407 1 407 408 1 408 409 1 409 410 1 410 411 1 411 412 1 412 413 1 413 414 1 414 415 1
		 415 416 1 416 417 1 417 418 1 418 419 1 419 420 1 420 421 1 421 422 1 422 423 1 423 404 1
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 432 1 432 433 1
		 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 440 1 440 441 1 441 442 1
		 442 443 1 443 424 1 444 445 1 445 446 1 446 447 1 447 448 1 448 449 1 449 450 1 450 451 1
		 451 452 1 452 453 1 453 454 1 454 455 1 455 456 1 456 457 1 457 458 1 458 459 1 459 460 1
		 460 461 1 461 462 1 462 463 1 463 444 1 84 104 1 85 105 1 86 106 1 87 107 1 88 108 1
		 89 109 1 90 110 1 91 111 1 92 112 1 93 113 1 94 114 1 95 115 1 96 116 1 97 117 1
		 98 118 1 99 119 1 100 120 1 101 121 1 102 122 1 103 123 1 104 124 1 105 125 1 106 126 1
		 107 127 1 108 128 1 109 129 1 110 130 1 111 131 1 112 132 1 113 133 1 114 134 1 115 135 1
		 116 136 1 117 137 1 118 138 1 119 139 1 120 140 1 121 141 1 122 142 1 123 143 1 124 144 1
		 125 145 1 126 146 1 127 147 1 128 148 1 129 149 1 130 150 1 131 151 1 132 152 1 133 153 1
		 134 154 1 135 155 1 136 156 1 137 157 1 138 158 1 139 159 1 140 160 1 141 161 1 142 162 1
		 143 163 1 144 164 1 145 165 1 146 166 1 147 167 1 148 168 1 149 169 1 150 170 1 151 171 1
		 152 172 1 153 173 1 154 174 1 155 175 1 156 176 1 157 177 1 158 178 1 159 179 1 160 180 1
		 161 181 1 162 182 1 163 183 1 164 184 1 165 185 1 166 186 1 167 187 1;
	setAttr ".ed[664:829]" 168 188 1 169 189 1 170 190 1 171 191 1 172 192 1 173 193 1
		 174 194 1 175 195 1 176 196 1 177 197 1 178 198 1 179 199 1 180 200 1 181 201 1 182 202 1
		 183 203 1 184 204 1 185 205 1 186 206 1 187 207 1 188 208 1 189 209 1 190 210 1 191 211 1
		 192 212 1 193 213 1 194 214 1 195 215 1 196 216 1 197 217 1 198 218 1 199 219 1 200 220 1
		 201 221 1 202 222 1 203 223 1 204 224 1 205 225 1 206 226 1 207 227 1 208 228 1 209 229 1
		 210 230 1 211 231 1 212 232 1 213 233 1 214 234 1 215 235 1 216 236 1 217 237 1 218 238 1
		 219 239 1 220 240 1 221 241 1 222 242 1 223 243 1 224 244 1 225 245 1 226 246 1 227 247 1
		 228 248 1 229 249 1 230 250 1 231 251 1 232 252 1 233 253 1 234 254 1 235 255 1 236 256 1
		 237 257 1 238 258 1 239 259 1 240 260 1 241 261 1 242 262 1 243 263 1 244 264 1 245 265 1
		 246 266 1 247 267 1 248 268 1 249 269 1 250 270 1 251 271 1 252 272 1 253 273 1 254 274 1
		 255 275 1 256 276 1 257 277 1 258 278 1 259 279 1 260 280 1 261 281 1 262 282 1 263 283 1
		 264 284 1 265 285 1 266 286 1 267 287 1 268 288 1 269 289 1 270 290 1 271 291 1 272 292 1
		 273 293 1 274 294 1 275 295 1 276 296 1 277 297 1 278 298 1 279 299 1 280 300 1 281 301 1
		 282 302 1 283 303 1 284 304 1 285 305 1 286 306 1 287 307 1 288 308 1 289 309 1 290 310 1
		 291 311 1 292 312 1 293 313 1 294 314 1 295 315 1 296 316 1 297 317 1 298 318 1 299 319 1
		 300 320 1 301 321 1 302 322 1 303 323 1 304 324 1 305 325 1 306 326 1 307 327 1 308 328 1
		 309 329 1 310 330 1 311 331 1 312 332 1 313 333 1 314 334 1 315 335 1 316 336 1 317 337 1
		 318 338 1 319 339 1 320 340 1 321 341 1 322 342 1 323 343 1 324 344 1 325 345 1 326 346 1
		 327 347 1 328 348 1 329 349 1 330 350 1 331 351 1 332 352 1 333 353 1;
	setAttr ".ed[830:979]" 334 354 1 335 355 1 336 356 1 337 357 1 338 358 1 339 359 1
		 340 360 1 341 361 1 342 362 1 343 363 1 344 364 1 345 365 1 346 366 1 347 367 1 348 368 1
		 349 369 1 350 370 1 351 371 1 352 372 1 353 373 1 354 374 1 355 375 1 356 376 1 357 377 1
		 358 378 1 359 379 1 360 380 1 361 381 1 362 382 1 363 383 1 364 384 1 365 385 1 366 386 1
		 367 387 1 368 388 1 369 389 1 370 390 1 371 391 1 372 392 1 373 393 1 374 394 1 375 395 1
		 376 396 1 377 397 1 378 398 1 379 399 1 380 400 1 381 401 1 382 402 1 383 403 1 384 404 1
		 385 405 1 386 406 1 387 407 1 388 408 1 389 409 1 390 410 1 391 411 1 392 412 1 393 413 1
		 394 414 1 395 415 1 396 416 1 397 417 1 398 418 1 399 419 1 400 420 1 401 421 1 402 422 1
		 403 423 1 404 424 1 405 425 1 406 426 1 407 427 1 408 428 1 409 429 1 410 430 1 411 431 1
		 412 432 1 413 433 1 414 434 1 415 435 1 416 436 1 417 437 1 418 438 1 419 439 1 420 440 1
		 421 441 1 422 442 1 423 443 1 424 444 1 425 445 1 426 446 1 427 447 1 428 448 1 429 449 1
		 430 450 1 431 451 1 432 452 1 433 453 1 434 454 1 435 455 1 436 456 1 437 457 1 438 458 1
		 439 459 1 440 460 1 441 461 1 442 462 1 443 463 1 464 84 1 464 85 1 464 86 1 464 87 1
		 464 88 1 464 89 1 464 90 1 464 91 1 464 92 1 464 93 1 464 94 1 464 95 1 464 96 1
		 464 97 1 464 98 1 464 99 1 464 100 1 464 101 1 464 102 1 464 103 1 444 465 1 445 465 1
		 446 465 1 447 465 1 448 465 1 449 465 1 450 465 1 451 465 1 452 465 1 453 465 1 454 465 1
		 455 465 1 456 465 1 457 465 1 458 465 1 459 465 1 460 465 1 461 465 1 462 465 1 463 465 1;
	setAttr -s 520 -ch 1960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80
		f 4 140 120 -142 -101
		mu 0 4 84 85 86 87
		f 4 141 121 -143 -102
		mu 0 4 87 86 88 89
		f 4 142 122 -144 -103
		mu 0 4 89 88 90 91
		f 4 143 123 -145 -104
		mu 0 4 91 90 92 93
		f 4 144 124 -146 -105
		mu 0 4 93 92 94 95
		f 4 145 125 -147 -106
		mu 0 4 95 94 96 97
		f 4 146 126 -148 -107
		mu 0 4 97 96 98 99
		f 4 147 127 -149 -108
		mu 0 4 99 98 100 101
		f 4 148 128 -150 -109
		mu 0 4 101 100 102 103
		f 4 149 129 -151 -110
		mu 0 4 103 102 104 105
		f 4 150 130 -152 -111
		mu 0 4 105 104 106 107
		f 4 151 131 -153 -112
		mu 0 4 107 106 108 109
		f 4 152 132 -154 -113
		mu 0 4 109 108 110 111
		f 4 153 133 -155 -114
		mu 0 4 111 110 112 113
		f 4 154 134 -156 -115
		mu 0 4 113 112 114 115
		f 4 155 135 -157 -116
		mu 0 4 115 114 116 117
		f 4 156 136 -158 -117
		mu 0 4 117 116 118 119
		f 4 157 137 -159 -118
		mu 0 4 119 118 120 121
		f 4 158 138 -160 -119
		mu 0 4 121 120 122 123
		f 4 159 139 -141 -120
		mu 0 4 123 122 124 125
		f 3 -162 160 100
		mu 0 3 126 127 128
		f 3 -163 161 101
		mu 0 3 129 127 126
		f 3 -164 162 102
		mu 0 3 130 127 129
		f 3 -165 163 103
		mu 0 3 131 127 130
		f 3 -166 164 104
		mu 0 3 132 127 131
		f 3 -167 165 105
		mu 0 3 133 127 132
		f 3 -168 166 106
		mu 0 3 134 127 133
		f 3 -169 167 107
		mu 0 3 135 127 134
		f 3 -170 168 108
		mu 0 3 136 127 135
		f 3 -171 169 109
		mu 0 3 137 127 136
		f 3 -172 170 110
		mu 0 3 138 127 137
		f 3 -173 171 111
		mu 0 3 139 127 138
		f 3 -174 172 112
		mu 0 3 140 127 139
		f 3 -175 173 113
		mu 0 3 141 127 140
		f 3 -176 174 114
		mu 0 3 142 127 141
		f 3 -177 175 115
		mu 0 3 143 127 142
		f 3 -178 176 116
		mu 0 3 144 127 143
		f 3 -179 177 117
		mu 0 3 145 127 144
		f 3 -180 178 118
		mu 0 3 146 127 145
		f 3 -161 179 119
		mu 0 3 128 127 146
		f 3 180 -182 -121
		mu 0 3 147 148 149
		f 3 181 -183 -122
		mu 0 3 149 148 150
		f 3 182 -184 -123
		mu 0 3 150 148 151
		f 3 183 -185 -124
		mu 0 3 151 148 152
		f 3 184 -186 -125
		mu 0 3 152 148 153
		f 3 185 -187 -126
		mu 0 3 153 148 154
		f 3 186 -188 -127
		mu 0 3 154 148 155
		f 3 187 -189 -128
		mu 0 3 155 148 156
		f 3 188 -190 -129
		mu 0 3 156 148 157
		f 3 189 -191 -130
		mu 0 3 157 148 158
		f 3 190 -192 -131
		mu 0 3 158 148 159
		f 3 191 -193 -132
		mu 0 3 159 148 160
		f 3 192 -194 -133
		mu 0 3 160 148 161
		f 3 193 -195 -134
		mu 0 3 161 148 162
		f 3 194 -196 -135
		mu 0 3 162 148 163
		f 3 195 -197 -136
		mu 0 3 163 148 164
		f 3 196 -198 -137
		mu 0 3 164 148 165
		f 3 197 -199 -138
		mu 0 3 165 148 166
		f 3 198 -200 -139
		mu 0 3 166 148 167
		f 3 199 -181 -140
		mu 0 3 167 148 147
		f 4 200 581 -221 -581
		mu 0 4 168 169 170 171
		f 4 201 582 -222 -582
		mu 0 4 169 172 173 170
		f 4 202 583 -223 -583
		mu 0 4 172 174 175 173
		f 4 203 584 -224 -584
		mu 0 4 174 176 177 175
		f 4 204 585 -225 -585
		mu 0 4 176 178 179 177
		f 4 205 586 -226 -586
		mu 0 4 178 180 181 179
		f 4 206 587 -227 -587
		mu 0 4 180 182 183 181
		f 4 207 588 -228 -588
		mu 0 4 182 184 185 183
		f 4 208 589 -229 -589
		mu 0 4 184 186 187 185
		f 4 209 590 -230 -590
		mu 0 4 186 188 189 187
		f 4 210 591 -231 -591
		mu 0 4 188 190 191 189
		f 4 211 592 -232 -592
		mu 0 4 190 192 193 191
		f 4 212 593 -233 -593
		mu 0 4 192 194 195 193
		f 4 213 594 -234 -594
		mu 0 4 194 196 197 195
		f 4 214 595 -235 -595
		mu 0 4 196 198 199 197
		f 4 215 596 -236 -596
		mu 0 4 198 200 201 199
		f 4 216 597 -237 -597
		mu 0 4 200 202 203 201
		f 4 217 598 -238 -598
		mu 0 4 202 204 205 203
		f 4 218 599 -239 -599
		mu 0 4 204 206 207 205
		f 4 219 580 -240 -600
		mu 0 4 206 208 209 207
		f 4 220 601 -241 -601
		mu 0 4 171 170 210 211
		f 4 221 602 -242 -602
		mu 0 4 170 173 212 210
		f 4 222 603 -243 -603
		mu 0 4 173 175 213 212
		f 4 223 604 -244 -604
		mu 0 4 175 177 214 213
		f 4 224 605 -245 -605
		mu 0 4 177 179 215 214
		f 4 225 606 -246 -606
		mu 0 4 179 181 216 215
		f 4 226 607 -247 -607
		mu 0 4 181 183 217 216
		f 4 227 608 -248 -608
		mu 0 4 183 185 218 217
		f 4 228 609 -249 -609
		mu 0 4 185 187 219 218
		f 4 229 610 -250 -610
		mu 0 4 187 189 220 219
		f 4 230 611 -251 -611
		mu 0 4 189 191 221 220
		f 4 231 612 -252 -612
		mu 0 4 191 193 222 221
		f 4 232 613 -253 -613
		mu 0 4 193 195 223 222
		f 4 233 614 -254 -614
		mu 0 4 195 197 224 223
		f 4 234 615 -255 -615
		mu 0 4 197 199 225 224
		f 4 235 616 -256 -616
		mu 0 4 199 201 226 225
		f 4 236 617 -257 -617
		mu 0 4 201 203 227 226
		f 4 237 618 -258 -618
		mu 0 4 203 205 228 227
		f 4 238 619 -259 -619
		mu 0 4 205 207 229 228
		f 4 239 600 -260 -620
		mu 0 4 207 209 230 229
		f 4 240 621 -261 -621
		mu 0 4 211 210 231 232
		f 4 241 622 -262 -622
		mu 0 4 210 212 233 231
		f 4 242 623 -263 -623
		mu 0 4 212 213 234 233
		f 4 243 624 -264 -624
		mu 0 4 213 214 235 234
		f 4 244 625 -265 -625
		mu 0 4 214 215 236 235
		f 4 245 626 -266 -626
		mu 0 4 215 216 237 236
		f 4 246 627 -267 -627
		mu 0 4 216 217 238 237
		f 4 247 628 -268 -628
		mu 0 4 217 218 239 238
		f 4 248 629 -269 -629
		mu 0 4 218 219 240 239
		f 4 249 630 -270 -630
		mu 0 4 219 220 241 240
		f 4 250 631 -271 -631
		mu 0 4 220 221 242 241
		f 4 251 632 -272 -632
		mu 0 4 221 222 243 242
		f 4 252 633 -273 -633
		mu 0 4 222 223 244 243
		f 4 253 634 -274 -634
		mu 0 4 223 224 245 244
		f 4 254 635 -275 -635
		mu 0 4 224 225 246 245
		f 4 255 636 -276 -636
		mu 0 4 225 226 247 246
		f 4 256 637 -277 -637
		mu 0 4 226 227 248 247
		f 4 257 638 -278 -638
		mu 0 4 227 228 249 248
		f 4 258 639 -279 -639
		mu 0 4 228 229 250 249
		f 4 259 620 -280 -640
		mu 0 4 229 230 251 250
		f 4 260 641 -281 -641
		mu 0 4 232 231 252 253
		f 4 261 642 -282 -642
		mu 0 4 231 233 254 252
		f 4 262 643 -283 -643
		mu 0 4 233 234 255 254
		f 4 263 644 -284 -644
		mu 0 4 234 235 256 255
		f 4 264 645 -285 -645
		mu 0 4 235 236 257 256
		f 4 265 646 -286 -646
		mu 0 4 236 237 258 257
		f 4 266 647 -287 -647
		mu 0 4 237 238 259 258
		f 4 267 648 -288 -648
		mu 0 4 238 239 260 259
		f 4 268 649 -289 -649
		mu 0 4 239 240 261 260
		f 4 269 650 -290 -650
		mu 0 4 240 241 262 261
		f 4 270 651 -291 -651
		mu 0 4 241 242 263 262
		f 4 271 652 -292 -652
		mu 0 4 242 243 264 263
		f 4 272 653 -293 -653
		mu 0 4 243 244 265 264
		f 4 273 654 -294 -654
		mu 0 4 244 245 266 265
		f 4 274 655 -295 -655
		mu 0 4 245 246 267 266
		f 4 275 656 -296 -656
		mu 0 4 246 247 268 267
		f 4 276 657 -297 -657
		mu 0 4 247 248 269 268
		f 4 277 658 -298 -658
		mu 0 4 248 249 270 269
		f 4 278 659 -299 -659
		mu 0 4 249 250 271 270
		f 4 279 640 -300 -660
		mu 0 4 250 251 272 271
		f 4 280 661 -301 -661
		mu 0 4 253 252 273 274
		f 4 281 662 -302 -662
		mu 0 4 252 254 275 273
		f 4 282 663 -303 -663
		mu 0 4 254 255 276 275
		f 4 283 664 -304 -664
		mu 0 4 255 256 277 276
		f 4 284 665 -305 -665
		mu 0 4 256 257 278 277
		f 4 285 666 -306 -666
		mu 0 4 257 258 279 278
		f 4 286 667 -307 -667
		mu 0 4 258 259 280 279
		f 4 287 668 -308 -668
		mu 0 4 259 260 281 280
		f 4 288 669 -309 -669
		mu 0 4 260 261 282 281
		f 4 289 670 -310 -670
		mu 0 4 261 262 283 282
		f 4 290 671 -311 -671
		mu 0 4 262 263 284 283
		f 4 291 672 -312 -672
		mu 0 4 263 264 285 284
		f 4 292 673 -313 -673
		mu 0 4 264 265 286 285
		f 4 293 674 -314 -674
		mu 0 4 265 266 287 286
		f 4 294 675 -315 -675
		mu 0 4 266 267 288 287
		f 4 295 676 -316 -676
		mu 0 4 267 268 289 288
		f 4 296 677 -317 -677
		mu 0 4 268 269 290 289
		f 4 297 678 -318 -678
		mu 0 4 269 270 291 290
		f 4 298 679 -319 -679
		mu 0 4 270 271 292 291
		f 4 299 660 -320 -680
		mu 0 4 271 272 293 292
		f 4 300 681 -321 -681
		mu 0 4 274 273 294 295
		f 4 301 682 -322 -682
		mu 0 4 273 275 296 294
		f 4 302 683 -323 -683
		mu 0 4 275 276 297 296
		f 4 303 684 -324 -684
		mu 0 4 276 277 298 297
		f 4 304 685 -325 -685
		mu 0 4 277 278 299 298
		f 4 305 686 -326 -686
		mu 0 4 278 279 300 299
		f 4 306 687 -327 -687
		mu 0 4 279 280 301 300
		f 4 307 688 -328 -688
		mu 0 4 280 281 302 301
		f 4 308 689 -329 -689
		mu 0 4 281 282 303 302
		f 4 309 690 -330 -690
		mu 0 4 282 283 304 303
		f 4 310 691 -331 -691
		mu 0 4 283 284 305 304
		f 4 311 692 -332 -692
		mu 0 4 284 285 306 305
		f 4 312 693 -333 -693
		mu 0 4 285 286 307 306
		f 4 313 694 -334 -694
		mu 0 4 286 287 308 307
		f 4 314 695 -335 -695
		mu 0 4 287 288 309 308
		f 4 315 696 -336 -696
		mu 0 4 288 289 310 309
		f 4 316 697 -337 -697
		mu 0 4 289 290 311 310
		f 4 317 698 -338 -698
		mu 0 4 290 291 312 311
		f 4 318 699 -339 -699
		mu 0 4 291 292 313 312
		f 4 319 680 -340 -700
		mu 0 4 292 293 314 313
		f 4 320 701 -341 -701
		mu 0 4 295 294 315 316
		f 4 321 702 -342 -702
		mu 0 4 294 296 317 315
		f 4 322 703 -343 -703
		mu 0 4 296 297 318 317
		f 4 323 704 -344 -704
		mu 0 4 297 298 319 318
		f 4 324 705 -345 -705
		mu 0 4 298 299 320 319
		f 4 325 706 -346 -706
		mu 0 4 299 300 321 320
		f 4 326 707 -347 -707
		mu 0 4 300 301 322 321
		f 4 327 708 -348 -708
		mu 0 4 301 302 323 322
		f 4 328 709 -349 -709
		mu 0 4 302 303 324 323
		f 4 329 710 -350 -710
		mu 0 4 303 304 325 324
		f 4 330 711 -351 -711
		mu 0 4 304 305 326 325
		f 4 331 712 -352 -712
		mu 0 4 305 306 327 326
		f 4 332 713 -353 -713
		mu 0 4 306 307 328 327
		f 4 333 714 -354 -714
		mu 0 4 307 308 329 328
		f 4 334 715 -355 -715
		mu 0 4 308 309 330 329
		f 4 335 716 -356 -716
		mu 0 4 309 310 331 330
		f 4 336 717 -357 -717
		mu 0 4 310 311 332 331
		f 4 337 718 -358 -718
		mu 0 4 311 312 333 332
		f 4 338 719 -359 -719
		mu 0 4 312 313 334 333
		f 4 339 700 -360 -720
		mu 0 4 313 314 335 334
		f 4 340 721 -361 -721
		mu 0 4 316 315 336 337
		f 4 341 722 -362 -722
		mu 0 4 315 317 338 336
		f 4 342 723 -363 -723
		mu 0 4 317 318 339 338
		f 4 343 724 -364 -724
		mu 0 4 318 319 340 339
		f 4 344 725 -365 -725
		mu 0 4 319 320 341 340
		f 4 345 726 -366 -726
		mu 0 4 320 321 342 341
		f 4 346 727 -367 -727
		mu 0 4 321 322 343 342
		f 4 347 728 -368 -728
		mu 0 4 322 323 344 343
		f 4 348 729 -369 -729
		mu 0 4 323 324 345 344
		f 4 349 730 -370 -730
		mu 0 4 324 325 346 345
		f 4 350 731 -371 -731
		mu 0 4 325 326 347 346
		f 4 351 732 -372 -732
		mu 0 4 326 327 348 347
		f 4 352 733 -373 -733
		mu 0 4 327 328 349 348
		f 4 353 734 -374 -734
		mu 0 4 328 329 350 349
		f 4 354 735 -375 -735
		mu 0 4 329 330 351 350
		f 4 355 736 -376 -736
		mu 0 4 330 331 352 351
		f 4 356 737 -377 -737
		mu 0 4 331 332 353 352
		f 4 357 738 -378 -738
		mu 0 4 332 333 354 353
		f 4 358 739 -379 -739
		mu 0 4 333 334 355 354
		f 4 359 720 -380 -740
		mu 0 4 334 335 356 355
		f 4 360 741 -381 -741
		mu 0 4 337 336 357 358
		f 4 361 742 -382 -742
		mu 0 4 336 338 359 357
		f 4 362 743 -383 -743
		mu 0 4 338 339 360 359
		f 4 363 744 -384 -744
		mu 0 4 339 340 361 360
		f 4 364 745 -385 -745
		mu 0 4 340 341 362 361
		f 4 365 746 -386 -746
		mu 0 4 341 342 363 362
		f 4 366 747 -387 -747
		mu 0 4 342 343 364 363
		f 4 367 748 -388 -748
		mu 0 4 343 344 365 364
		f 4 368 749 -389 -749
		mu 0 4 344 345 366 365
		f 4 369 750 -390 -750
		mu 0 4 345 346 367 366
		f 4 370 751 -391 -751
		mu 0 4 346 347 368 367
		f 4 371 752 -392 -752
		mu 0 4 347 348 369 368
		f 4 372 753 -393 -753
		mu 0 4 348 349 370 369
		f 4 373 754 -394 -754
		mu 0 4 349 350 371 370
		f 4 374 755 -395 -755
		mu 0 4 350 351 372 371
		f 4 375 756 -396 -756
		mu 0 4 351 352 373 372
		f 4 376 757 -397 -757
		mu 0 4 352 353 374 373
		f 4 377 758 -398 -758
		mu 0 4 353 354 375 374
		f 4 378 759 -399 -759
		mu 0 4 354 355 376 375
		f 4 379 740 -400 -760
		mu 0 4 355 356 377 376
		f 4 380 761 -401 -761
		mu 0 4 358 357 378 379
		f 4 381 762 -402 -762
		mu 0 4 357 359 380 378
		f 4 382 763 -403 -763
		mu 0 4 359 360 381 380
		f 4 383 764 -404 -764
		mu 0 4 360 361 382 381
		f 4 384 765 -405 -765
		mu 0 4 361 362 383 382
		f 4 385 766 -406 -766
		mu 0 4 362 363 384 383
		f 4 386 767 -407 -767
		mu 0 4 363 364 385 384
		f 4 387 768 -408 -768
		mu 0 4 364 365 386 385
		f 4 388 769 -409 -769
		mu 0 4 365 366 387 386
		f 4 389 770 -410 -770
		mu 0 4 366 367 388 387
		f 4 390 771 -411 -771
		mu 0 4 367 368 389 388
		f 4 391 772 -412 -772
		mu 0 4 368 369 390 389
		f 4 392 773 -413 -773
		mu 0 4 369 370 391 390
		f 4 393 774 -414 -774
		mu 0 4 370 371 392 391
		f 4 394 775 -415 -775
		mu 0 4 371 372 393 392
		f 4 395 776 -416 -776
		mu 0 4 372 373 394 393
		f 4 396 777 -417 -777
		mu 0 4 373 374 395 394
		f 4 397 778 -418 -778
		mu 0 4 374 375 396 395
		f 4 398 779 -419 -779
		mu 0 4 375 376 397 396
		f 4 399 760 -420 -780
		mu 0 4 376 377 398 397
		f 4 400 781 -421 -781
		mu 0 4 379 378 399 400
		f 4 401 782 -422 -782
		mu 0 4 378 380 401 399
		f 4 402 783 -423 -783
		mu 0 4 380 381 402 401
		f 4 403 784 -424 -784
		mu 0 4 381 382 403 402
		f 4 404 785 -425 -785
		mu 0 4 382 383 404 403
		f 4 405 786 -426 -786
		mu 0 4 383 384 405 404
		f 4 406 787 -427 -787
		mu 0 4 384 385 406 405
		f 4 407 788 -428 -788
		mu 0 4 385 386 407 406
		f 4 408 789 -429 -789
		mu 0 4 386 387 408 407
		f 4 409 790 -430 -790
		mu 0 4 387 388 409 408
		f 4 410 791 -431 -791
		mu 0 4 388 389 410 409
		f 4 411 792 -432 -792
		mu 0 4 389 390 411 410
		f 4 412 793 -433 -793
		mu 0 4 390 391 412 411
		f 4 413 794 -434 -794
		mu 0 4 391 392 413 412
		f 4 414 795 -435 -795
		mu 0 4 392 393 414 413
		f 4 415 796 -436 -796
		mu 0 4 393 394 415 414
		f 4 416 797 -437 -797
		mu 0 4 394 395 416 415
		f 4 417 798 -438 -798
		mu 0 4 395 396 417 416
		f 4 418 799 -439 -799
		mu 0 4 396 397 418 417
		f 4 419 780 -440 -800
		mu 0 4 397 398 419 418
		f 4 420 801 -441 -801
		mu 0 4 400 399 420 421
		f 4 421 802 -442 -802
		mu 0 4 399 401 422 420
		f 4 422 803 -443 -803
		mu 0 4 401 402 423 422
		f 4 423 804 -444 -804
		mu 0 4 402 403 424 423
		f 4 424 805 -445 -805
		mu 0 4 403 404 425 424
		f 4 425 806 -446 -806
		mu 0 4 404 405 426 425
		f 4 426 807 -447 -807
		mu 0 4 405 406 427 426
		f 4 427 808 -448 -808
		mu 0 4 406 407 428 427
		f 4 428 809 -449 -809
		mu 0 4 407 408 429 428
		f 4 429 810 -450 -810
		mu 0 4 408 409 430 429
		f 4 430 811 -451 -811
		mu 0 4 409 410 431 430
		f 4 431 812 -452 -812
		mu 0 4 410 411 432 431
		f 4 432 813 -453 -813
		mu 0 4 411 412 433 432
		f 4 433 814 -454 -814
		mu 0 4 412 413 434 433
		f 4 434 815 -455 -815
		mu 0 4 413 414 435 434
		f 4 435 816 -456 -816
		mu 0 4 414 415 436 435
		f 4 436 817 -457 -817
		mu 0 4 415 416 437 436
		f 4 437 818 -458 -818
		mu 0 4 416 417 438 437
		f 4 438 819 -459 -819
		mu 0 4 417 418 439 438
		f 4 439 800 -460 -820
		mu 0 4 418 419 440 439
		f 4 440 821 -461 -821
		mu 0 4 421 420 441 442
		f 4 441 822 -462 -822
		mu 0 4 420 422 443 441
		f 4 442 823 -463 -823
		mu 0 4 422 423 444 443
		f 4 443 824 -464 -824
		mu 0 4 423 424 445 444
		f 4 444 825 -465 -825
		mu 0 4 424 425 446 445
		f 4 445 826 -466 -826
		mu 0 4 425 426 447 446
		f 4 446 827 -467 -827
		mu 0 4 426 427 448 447
		f 4 447 828 -468 -828
		mu 0 4 427 428 449 448
		f 4 448 829 -469 -829
		mu 0 4 428 429 450 449
		f 4 449 830 -470 -830
		mu 0 4 429 430 451 450
		f 4 450 831 -471 -831
		mu 0 4 430 431 452 451
		f 4 451 832 -472 -832
		mu 0 4 431 432 453 452
		f 4 452 833 -473 -833
		mu 0 4 432 433 454 453
		f 4 453 834 -474 -834
		mu 0 4 433 434 455 454
		f 4 454 835 -475 -835
		mu 0 4 434 435 456 455
		f 4 455 836 -476 -836
		mu 0 4 435 436 457 456
		f 4 456 837 -477 -837
		mu 0 4 436 437 458 457
		f 4 457 838 -478 -838
		mu 0 4 437 438 459 458
		f 4 458 839 -479 -839
		mu 0 4 438 439 460 459
		f 4 459 820 -480 -840
		mu 0 4 439 440 461 460
		f 4 460 841 -481 -841
		mu 0 4 442 441 462 463
		f 4 461 842 -482 -842
		mu 0 4 441 443 464 462
		f 4 462 843 -483 -843
		mu 0 4 443 444 465 464
		f 4 463 844 -484 -844
		mu 0 4 444 445 466 465
		f 4 464 845 -485 -845
		mu 0 4 445 446 467 466
		f 4 465 846 -486 -846
		mu 0 4 446 447 468 467
		f 4 466 847 -487 -847
		mu 0 4 447 448 469 468
		f 4 467 848 -488 -848
		mu 0 4 448 449 470 469
		f 4 468 849 -489 -849
		mu 0 4 449 450 471 470
		f 4 469 850 -490 -850
		mu 0 4 450 451 472 471
		f 4 470 851 -491 -851
		mu 0 4 451 452 473 472
		f 4 471 852 -492 -852
		mu 0 4 452 453 474 473
		f 4 472 853 -493 -853
		mu 0 4 453 454 475 474
		f 4 473 854 -494 -854
		mu 0 4 454 455 476 475
		f 4 474 855 -495 -855
		mu 0 4 455 456 477 476
		f 4 475 856 -496 -856
		mu 0 4 456 457 478 477
		f 4 476 857 -497 -857
		mu 0 4 457 458 479 478
		f 4 477 858 -498 -858
		mu 0 4 458 459 480 479
		f 4 478 859 -499 -859
		mu 0 4 459 460 481 480
		f 4 479 840 -500 -860
		mu 0 4 460 461 482 481
		f 4 480 861 -501 -861
		mu 0 4 463 462 483 484
		f 4 481 862 -502 -862
		mu 0 4 462 464 485 483
		f 4 482 863 -503 -863
		mu 0 4 464 465 486 485
		f 4 483 864 -504 -864
		mu 0 4 465 466 487 486
		f 4 484 865 -505 -865
		mu 0 4 466 467 488 487
		f 4 485 866 -506 -866
		mu 0 4 467 468 489 488
		f 4 486 867 -507 -867
		mu 0 4 468 469 490 489
		f 4 487 868 -508 -868
		mu 0 4 469 470 491 490
		f 4 488 869 -509 -869
		mu 0 4 470 471 492 491
		f 4 489 870 -510 -870
		mu 0 4 471 472 493 492
		f 4 490 871 -511 -871
		mu 0 4 472 473 494 493
		f 4 491 872 -512 -872
		mu 0 4 473 474 495 494
		f 4 492 873 -513 -873
		mu 0 4 474 475 496 495
		f 4 493 874 -514 -874
		mu 0 4 475 476 497 496
		f 4 494 875 -515 -875
		mu 0 4 476 477 498 497
		f 4 495 876 -516 -876
		mu 0 4 477 478 499 498
		f 4 496 877 -517 -877
		mu 0 4 478 479 500 499
		f 4 497 878 -518 -878
		mu 0 4 479 480 501 500
		f 4 498 879 -519 -879
		mu 0 4 480 481 502 501
		f 4 499 860 -520 -880
		mu 0 4 481 482 503 502
		f 4 500 881 -521 -881
		mu 0 4 484 483 504 505
		f 4 501 882 -522 -882
		mu 0 4 483 485 506 504
		f 4 502 883 -523 -883
		mu 0 4 485 486 507 506
		f 4 503 884 -524 -884
		mu 0 4 486 487 508 507
		f 4 504 885 -525 -885
		mu 0 4 487 488 509 508
		f 4 505 886 -526 -886
		mu 0 4 488 489 510 509
		f 4 506 887 -527 -887
		mu 0 4 489 490 511 510
		f 4 507 888 -528 -888
		mu 0 4 490 491 512 511
		f 4 508 889 -529 -889
		mu 0 4 491 492 513 512
		f 4 509 890 -530 -890
		mu 0 4 492 493 514 513
		f 4 510 891 -531 -891
		mu 0 4 493 494 515 514
		f 4 511 892 -532 -892
		mu 0 4 494 495 516 515
		f 4 512 893 -533 -893
		mu 0 4 495 496 517 516
		f 4 513 894 -534 -894
		mu 0 4 496 497 518 517
		f 4 514 895 -535 -895
		mu 0 4 497 498 519 518
		f 4 515 896 -536 -896
		mu 0 4 498 499 520 519
		f 4 516 897 -537 -897
		mu 0 4 499 500 521 520
		f 4 517 898 -538 -898
		mu 0 4 500 501 522 521
		f 4 518 899 -539 -899
		mu 0 4 501 502 523 522
		f 4 519 880 -540 -900
		mu 0 4 502 503 524 523
		f 4 520 901 -541 -901
		mu 0 4 505 504 525 526
		f 4 521 902 -542 -902
		mu 0 4 504 506 527 525
		f 4 522 903 -543 -903
		mu 0 4 506 507 528 527
		f 4 523 904 -544 -904
		mu 0 4 507 508 529 528
		f 4 524 905 -545 -905
		mu 0 4 508 509 530 529
		f 4 525 906 -546 -906
		mu 0 4 509 510 531 530
		f 4 526 907 -547 -907
		mu 0 4 510 511 532 531
		f 4 527 908 -548 -908
		mu 0 4 511 512 533 532
		f 4 528 909 -549 -909
		mu 0 4 512 513 534 533
		f 4 529 910 -550 -910
		mu 0 4 513 514 535 534
		f 4 530 911 -551 -911
		mu 0 4 514 515 536 535
		f 4 531 912 -552 -912
		mu 0 4 515 516 537 536
		f 4 532 913 -553 -913
		mu 0 4 516 517 538 537
		f 4 533 914 -554 -914
		mu 0 4 517 518 539 538
		f 4 534 915 -555 -915
		mu 0 4 518 519 540 539
		f 4 535 916 -556 -916
		mu 0 4 519 520 541 540
		f 4 536 917 -557 -917
		mu 0 4 520 521 542 541
		f 4 537 918 -558 -918
		mu 0 4 521 522 543 542
		f 4 538 919 -559 -919
		mu 0 4 522 523 544 543
		f 4 539 900 -560 -920
		mu 0 4 523 524 545 544
		f 4 540 921 -561 -921
		mu 0 4 526 525 546 547
		f 4 541 922 -562 -922
		mu 0 4 525 527 548 546
		f 4 542 923 -563 -923
		mu 0 4 527 528 549 548
		f 4 543 924 -564 -924
		mu 0 4 528 529 550 549
		f 4 544 925 -565 -925
		mu 0 4 529 530 551 550
		f 4 545 926 -566 -926
		mu 0 4 530 531 552 551
		f 4 546 927 -567 -927
		mu 0 4 531 532 553 552
		f 4 547 928 -568 -928
		mu 0 4 532 533 554 553
		f 4 548 929 -569 -929
		mu 0 4 533 534 555 554
		f 4 549 930 -570 -930
		mu 0 4 534 535 556 555
		f 4 550 931 -571 -931
		mu 0 4 535 536 557 556
		f 4 551 932 -572 -932
		mu 0 4 536 537 558 557
		f 4 552 933 -573 -933
		mu 0 4 537 538 559 558
		f 4 553 934 -574 -934
		mu 0 4 538 539 560 559
		f 4 554 935 -575 -935
		mu 0 4 539 540 561 560
		f 4 555 936 -576 -936
		mu 0 4 540 541 562 561
		f 4 556 937 -577 -937
		mu 0 4 541 542 563 562
		f 4 557 938 -578 -938
		mu 0 4 542 543 564 563
		f 4 558 939 -579 -939
		mu 0 4 543 544 565 564
		f 4 559 920 -580 -940
		mu 0 4 544 545 566 565
		f 3 -201 -941 941
		mu 0 3 169 168 567
		f 3 -202 -942 942
		mu 0 3 172 169 568
		f 3 -203 -943 943
		mu 0 3 174 172 569
		f 3 -204 -944 944
		mu 0 3 176 174 570
		f 3 -205 -945 945
		mu 0 3 178 176 571
		f 3 -206 -946 946
		mu 0 3 180 178 572
		f 3 -207 -947 947
		mu 0 3 182 180 573
		f 3 -208 -948 948
		mu 0 3 184 182 574
		f 3 -209 -949 949
		mu 0 3 186 184 575
		f 3 -210 -950 950
		mu 0 3 188 186 576
		f 3 -211 -951 951
		mu 0 3 190 188 577
		f 3 -212 -952 952
		mu 0 3 192 190 578
		f 3 -213 -953 953
		mu 0 3 194 192 579
		f 3 -214 -954 954
		mu 0 3 196 194 580
		f 3 -215 -955 955
		mu 0 3 198 196 581
		f 3 -216 -956 956
		mu 0 3 200 198 582
		f 3 -217 -957 957
		mu 0 3 202 200 583
		f 3 -218 -958 958
		mu 0 3 204 202 584
		f 3 -219 -959 959
		mu 0 3 206 204 585
		f 3 -220 -960 940
		mu 0 3 208 206 586;
	setAttr ".fc[500:519]"
		f 3 560 961 -961
		mu 0 3 547 546 587
		f 3 561 962 -962
		mu 0 3 546 548 588
		f 3 562 963 -963
		mu 0 3 548 549 589
		f 3 563 964 -964
		mu 0 3 549 550 590
		f 3 564 965 -965
		mu 0 3 550 551 591
		f 3 565 966 -966
		mu 0 3 551 552 592
		f 3 566 967 -967
		mu 0 3 552 553 593
		f 3 567 968 -968
		mu 0 3 553 554 594
		f 3 568 969 -969
		mu 0 3 554 555 595
		f 3 569 970 -970
		mu 0 3 555 556 596
		f 3 570 971 -971
		mu 0 3 556 557 597
		f 3 571 972 -972
		mu 0 3 557 558 598
		f 3 572 973 -973
		mu 0 3 558 559 599
		f 3 573 974 -974
		mu 0 3 559 560 600
		f 3 574 975 -975
		mu 0 3 560 561 601
		f 3 575 976 -976
		mu 0 3 561 562 602
		f 3 576 977 -977
		mu 0 3 562 563 603
		f 3 577 978 -978
		mu 0 3 563 564 604
		f 3 578 979 -979
		mu 0 3 564 565 605
		f 3 579 960 -980
		mu 0 3 565 566 606;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder8";
	rename -uid "FDAAECE4-4A44-908F-9C9E-CF91EB342416";
	setAttr ".t" -type "double3" 49.038736510369162 -1.3649961587230521 -11.204326769068146 ;
	setAttr ".r" -type "double3" 62.947594251811275 -41.125190598458389 154.29438751735469 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder8Shape" -p "pCylinder8";
	rename -uid "19EA9BA5-403A-1D5F-63F6-2B81C03DC5CE";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:519]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 607 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.64860266 0.10796607 0.62640899
		 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607
		 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997
		 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161
		 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146
		 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998 0.3125
		 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.375
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.38749999 0.3125 0.39999998 0.68843985
		 0.39999998 0.3125 0.41249996 0.68843985 0.41249996 0.3125 0.42499995 0.68843985 0.42499995
		 0.3125 0.43749994 0.68843985 0.43749994 0.3125 0.44999993 0.68843985 0.44999993 0.3125
		 0.46249992 0.68843985 0.46249992 0.3125 0.4749999 0.68843985 0.4749999 0.3125 0.48749989
		 0.68843985 0.48749989 0.3125 0.49999988 0.68843985 0.49999988 0.3125 0.51249987 0.68843985
		 0.51249987 0.3125 0.52499986 0.68843985 0.52499986 0.3125 0.53749985 0.68843985 0.53749985
		 0.3125 0.54999983 0.68843985 0.54999983 0.3125 0.56249982 0.68843985 0.56249982 0.3125
		 0.57499981 0.68843985 0.57499981 0.3125 0.5874998 0.68843985 0.5874998 0.3125 0.59999979
		 0.68843985 0.59999979 0.3125 0.61249977 0.68843985 0.61249977 0.3125 0.62499976 0.68843985
		 0.62499976 0.3125 0.62640899 0.064408496 0.5 0.15000001 0.64860266 0.10796607 0.59184152
		 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607 0.0076473504 0.40815851
		 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997 0.15625 0.3513974
		 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161 0.3048526 0.5 0.3125
		 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146 0.6486026 0.2045339
		 0.65625 0.15625 0.6486026 0.89203393 0.5 0.83749998 0.62640893 0.93559146 0.59184146
		 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893 0.37359107
		 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607 0.37359107
		 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994 0.54828393
		 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607 0.65625
		 0.84375 0 0.050000001 0.050000001 0.050000001 0.050000001 0.1 0 0.1 0.1 0.050000001
		 0.1 0.1 0.15000001 0.050000001 0.15000001 0.1 0.2 0.050000001 0.2 0.1 0.25 0.050000001
		 0.25 0.1 0.30000001 0.050000001 0.30000001 0.1 0.35000002 0.050000001 0.35000002
		 0.1 0.40000004 0.050000001 0.40000004 0.1 0.45000005 0.050000001 0.45000005 0.1 0.50000006
		 0.050000001 0.50000006 0.1 0.55000007 0.050000001 0.55000007 0.1 0.60000008 0.050000001
		 0.60000008 0.1 0.6500001 0.050000001 0.6500001 0.1 0.70000011 0.050000001 0.70000011
		 0.1 0.75000012 0.050000001 0.75000012 0.1 0.80000013 0.050000001 0.80000013 0.1 0.85000014
		 0.050000001 0.85000014 0.1 0.90000015 0.050000001 0.90000015 0.1 0.95000017 0.050000001
		 0.95000017 0.1 1.000000119209 0.050000001 1.000000119209 0.1 0.050000001 0.15000001
		 0 0.15000001 0.1 0.15000001 0.15000001 0.15000001 0.2 0.15000001 0.25 0.15000001
		 0.30000001 0.15000001 0.35000002 0.15000001 0.40000004 0.15000001 0.45000005 0.15000001
		 0.50000006 0.15000001 0.55000007 0.15000001 0.60000008 0.15000001 0.6500001 0.15000001
		 0.70000011 0.15000001 0.75000012 0.15000001 0.80000013 0.15000001 0.85000014 0.15000001
		 0.90000015 0.15000001 0.95000017 0.15000001 1.000000119209 0.15000001 0.050000001
		 0.2 0 0.2 0.1 0.2 0.15000001 0.2 0.2 0.2 0.25 0.2 0.30000001 0.2 0.35000002 0.2 0.40000004
		 0.2 0.45000005 0.2 0.50000006 0.2 0.55000007 0.2 0.60000008 0.2 0.6500001 0.2 0.70000011
		 0.2 0.75000012 0.2 0.80000013 0.2 0.85000014 0.2 0.90000015 0.2;
	setAttr ".uvst[0].uvsp[250:499]" 0.95000017 0.2 1.000000119209 0.2 0.050000001
		 0.25 0 0.25 0.1 0.25 0.15000001 0.25 0.2 0.25 0.25 0.25 0.30000001 0.25 0.35000002
		 0.25 0.40000004 0.25 0.45000005 0.25 0.50000006 0.25 0.55000007 0.25 0.60000008 0.25
		 0.6500001 0.25 0.70000011 0.25 0.75000012 0.25 0.80000013 0.25 0.85000014 0.25 0.90000015
		 0.25 0.95000017 0.25 1.000000119209 0.25 0.050000001 0.30000001 0 0.30000001 0.1
		 0.30000001 0.15000001 0.30000001 0.2 0.30000001 0.25 0.30000001 0.30000001 0.30000001
		 0.35000002 0.30000001 0.40000004 0.30000001 0.45000005 0.30000001 0.50000006 0.30000001
		 0.55000007 0.30000001 0.60000008 0.30000001 0.6500001 0.30000001 0.70000011 0.30000001
		 0.75000012 0.30000001 0.80000013 0.30000001 0.85000014 0.30000001 0.90000015 0.30000001
		 0.95000017 0.30000001 1.000000119209 0.30000001 0.050000001 0.35000002 0 0.35000002
		 0.1 0.35000002 0.15000001 0.35000002 0.2 0.35000002 0.25 0.35000002 0.30000001 0.35000002
		 0.35000002 0.35000002 0.40000004 0.35000002 0.45000005 0.35000002 0.50000006 0.35000002
		 0.55000007 0.35000002 0.60000008 0.35000002 0.6500001 0.35000002 0.70000011 0.35000002
		 0.75000012 0.35000002 0.80000013 0.35000002 0.85000014 0.35000002 0.90000015 0.35000002
		 0.95000017 0.35000002 1.000000119209 0.35000002 0.050000001 0.40000004 0 0.40000004
		 0.1 0.40000004 0.15000001 0.40000004 0.2 0.40000004 0.25 0.40000004 0.30000001 0.40000004
		 0.35000002 0.40000004 0.40000004 0.40000004 0.45000005 0.40000004 0.50000006 0.40000004
		 0.55000007 0.40000004 0.60000008 0.40000004 0.6500001 0.40000004 0.70000011 0.40000004
		 0.75000012 0.40000004 0.80000013 0.40000004 0.85000014 0.40000004 0.90000015 0.40000004
		 0.95000017 0.40000004 1.000000119209 0.40000004 0.050000001 0.45000005 0 0.45000005
		 0.1 0.45000005 0.15000001 0.45000005 0.2 0.45000005 0.25 0.45000005 0.30000001 0.45000005
		 0.35000002 0.45000005 0.40000004 0.45000005 0.45000005 0.45000005 0.50000006 0.45000005
		 0.55000007 0.45000005 0.60000008 0.45000005 0.6500001 0.45000005 0.70000011 0.45000005
		 0.75000012 0.45000005 0.80000013 0.45000005 0.85000014 0.45000005 0.90000015 0.45000005
		 0.95000017 0.45000005 1.000000119209 0.45000005 0.050000001 0.50000006 0 0.50000006
		 0.1 0.50000006 0.15000001 0.50000006 0.2 0.50000006 0.25 0.50000006 0.30000001 0.50000006
		 0.35000002 0.50000006 0.40000004 0.50000006 0.45000005 0.50000006 0.50000006 0.50000006
		 0.55000007 0.50000006 0.60000008 0.50000006 0.6500001 0.50000006 0.70000011 0.50000006
		 0.75000012 0.50000006 0.80000013 0.50000006 0.85000014 0.50000006 0.90000015 0.50000006
		 0.95000017 0.50000006 1.000000119209 0.50000006 0.050000001 0.55000007 0 0.55000007
		 0.1 0.55000007 0.15000001 0.55000007 0.2 0.55000007 0.25 0.55000007 0.30000001 0.55000007
		 0.35000002 0.55000007 0.40000004 0.55000007 0.45000005 0.55000007 0.50000006 0.55000007
		 0.55000007 0.55000007 0.60000008 0.55000007 0.6500001 0.55000007 0.70000011 0.55000007
		 0.75000012 0.55000007 0.80000013 0.55000007 0.85000014 0.55000007 0.90000015 0.55000007
		 0.95000017 0.55000007 1.000000119209 0.55000007 0.050000001 0.60000008 0 0.60000008
		 0.1 0.60000008 0.15000001 0.60000008 0.2 0.60000008 0.25 0.60000008 0.30000001 0.60000008
		 0.35000002 0.60000008 0.40000004 0.60000008 0.45000005 0.60000008 0.50000006 0.60000008
		 0.55000007 0.60000008 0.60000008 0.60000008 0.6500001 0.60000008 0.70000011 0.60000008
		 0.75000012 0.60000008 0.80000013 0.60000008 0.85000014 0.60000008 0.90000015 0.60000008
		 0.95000017 0.60000008 1.000000119209 0.60000008 0.050000001 0.6500001 0 0.6500001
		 0.1 0.6500001 0.15000001 0.6500001 0.2 0.6500001 0.25 0.6500001 0.30000001 0.6500001
		 0.35000002 0.6500001 0.40000004 0.6500001 0.45000005 0.6500001 0.50000006 0.6500001
		 0.55000007 0.6500001 0.60000008 0.6500001 0.6500001 0.6500001 0.70000011 0.6500001
		 0.75000012 0.6500001 0.80000013 0.6500001 0.85000014 0.6500001 0.90000015 0.6500001
		 0.95000017 0.6500001 1.000000119209 0.6500001 0.050000001 0.70000011 0 0.70000011
		 0.1 0.70000011 0.15000001 0.70000011 0.2 0.70000011 0.25 0.70000011 0.30000001 0.70000011
		 0.35000002 0.70000011 0.40000004 0.70000011 0.45000005 0.70000011 0.50000006 0.70000011
		 0.55000007 0.70000011 0.60000008 0.70000011 0.6500001 0.70000011 0.70000011 0.70000011
		 0.75000012 0.70000011 0.80000013 0.70000011 0.85000014 0.70000011 0.90000015 0.70000011
		 0.95000017 0.70000011 1.000000119209 0.70000011 0.050000001 0.75000012 0 0.75000012
		 0.1 0.75000012 0.15000001 0.75000012 0.2 0.75000012 0.25 0.75000012 0.30000001 0.75000012
		 0.35000002 0.75000012 0.40000004 0.75000012 0.45000005 0.75000012 0.50000006 0.75000012
		 0.55000007 0.75000012 0.60000008 0.75000012 0.6500001 0.75000012 0.70000011 0.75000012
		 0.75000012 0.75000012 0.80000013 0.75000012 0.85000014 0.75000012 0.90000015 0.75000012
		 0.95000017 0.75000012 1.000000119209 0.75000012 0.050000001 0.80000013 0 0.80000013
		 0.1 0.80000013 0.15000001 0.80000013 0.2 0.80000013 0.25 0.80000013 0.30000001 0.80000013
		 0.35000002 0.80000013 0.40000004 0.80000013 0.45000005 0.80000013 0.50000006 0.80000013
		 0.55000007 0.80000013 0.60000008 0.80000013 0.6500001 0.80000013 0.70000011 0.80000013
		 0.75000012 0.80000013 0.80000013 0.80000013;
	setAttr ".uvst[0].uvsp[500:606]" 0.85000014 0.80000013 0.90000015 0.80000013
		 0.95000017 0.80000013 1.000000119209 0.80000013 0.050000001 0.85000014 0 0.85000014
		 0.1 0.85000014 0.15000001 0.85000014 0.2 0.85000014 0.25 0.85000014 0.30000001 0.85000014
		 0.35000002 0.85000014 0.40000004 0.85000014 0.45000005 0.85000014 0.50000006 0.85000014
		 0.55000007 0.85000014 0.60000008 0.85000014 0.6500001 0.85000014 0.70000011 0.85000014
		 0.75000012 0.85000014 0.80000013 0.85000014 0.85000014 0.85000014 0.90000015 0.85000014
		 0.95000017 0.85000014 1.000000119209 0.85000014 0.050000001 0.90000015 0 0.90000015
		 0.1 0.90000015 0.15000001 0.90000015 0.2 0.90000015 0.25 0.90000015 0.30000001 0.90000015
		 0.35000002 0.90000015 0.40000004 0.90000015 0.45000005 0.90000015 0.50000006 0.90000015
		 0.55000007 0.90000015 0.60000008 0.90000015 0.6500001 0.90000015 0.70000011 0.90000015
		 0.75000012 0.90000015 0.80000013 0.90000015 0.85000014 0.90000015 0.90000015 0.90000015
		 0.95000017 0.90000015 1.000000119209 0.90000015 0.050000001 0.95000017 0 0.95000017
		 0.1 0.95000017 0.15000001 0.95000017 0.2 0.95000017 0.25 0.95000017 0.30000001 0.95000017
		 0.35000002 0.95000017 0.40000004 0.95000017 0.45000005 0.95000017 0.50000006 0.95000017
		 0.55000007 0.95000017 0.60000008 0.95000017 0.6500001 0.95000017 0.70000011 0.95000017
		 0.75000012 0.95000017 0.80000013 0.95000017 0.85000014 0.95000017 0.90000015 0.95000017
		 0.95000017 0.95000017 1.000000119209 0.95000017 0.025 0 0.075000003 0 0.125 0 0.175
		 0 0.22500001 0 0.27500001 0 0.32500002 0 0.375 0 0.42500001 0 0.47499999 0 0.52500004
		 0 0.57499999 0 0.625 0 0.67500001 0 0.72500002 0 0.77500004 0 0.82499999 0 0.875
		 0 0.92500001 0 0.97500002 0 0.025 1 0.075000003 1 0.125 1 0.175 1 0.22500001 1 0.27500001
		 1 0.32500002 1 0.375 1 0.42500001 1 0.47499999 1 0.52500004 1 0.57499999 1 0.625
		 1 0.67500001 1 0.72500002 1 0.77500004 1 0.82499999 1 0.875 1 0.92500001 1 0.97500002
		 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 466 ".vt";
	setAttr ".vt[0:165]"  -31.42638397 0.15162086 89.97360229 -31.61052513 0.26360402 89.74130249
		 -31.94010735 0.35247436 89.46312714 -32.38286972 0.40953261 89.16630554 -32.89547348 0.4291935 88.87989807
		 -33.42773819 0.40953258 88.63193512 -33.92756271 0.3524743 88.44669342 -34.34602356 0.26360396 88.34230042
		 -34.64215469 0.15162082 88.32897949 -34.78697205 0.0274866 88.40803528 -34.7663002 -0.09664762 88.57172394
		 -34.58216095 -0.20863073 88.80403137 -34.25257874 -0.29750097 89.082206726 -33.80981445 -0.35455921 89.37902069
		 -33.29721451 -0.37422013 89.66543579 -32.7649498 -0.35455918 89.91339111 -32.26512146 -0.29750094 90.098632813
		 -31.84666252 -0.2086307 90.20302582 -31.55052948 -0.096647598 90.21634674 -31.40571213 0.0274866 90.13729095
		 -31.40359116 0.096647523 89.929039 -31.58773232 0.20863067 89.69673157 -31.91731453 0.297501 89.41855621
		 -32.3600769 0.35455924 89.12174225 -32.87267685 0.37422013 88.83532715 -33.40494156 0.35455921 88.58737183
		 -33.9047699 0.29750094 88.4021225 -34.32322693 0.20863059 88.29773712 -34.61936188 0.096647471 88.2844162
		 -34.76417923 -0.027486745 88.36347198 -34.74350739 -0.15162097 88.52716064 -34.55936432 -0.26360404 88.75946045
		 -34.2297821 -0.35247433 89.037635803 -33.78702164 -0.40953258 89.3344574 -33.27441788 -0.4291935 89.62086487
		 -32.74215698 -0.40953255 89.86882782 -32.24232864 -0.3524743 90.054069519 -31.82386971 -0.26360402 90.15846252
		 -31.52773666 -0.15162094 90.17178345 -31.38291931 -0.027486745 90.092727661 -33.096343994 0.0274866 89.27266693
		 -33.073547363 -0.027486745 89.22809601 -38.24187469 2.49435377 89.47029114 -38.0036354065 2.42841434 89.27202606
		 -37.675354 2.24016404 89.046867371 -37.28915024 1.94802976 88.81686401 -36.88284302 1.58060765 88.60451508
		 -36.49619675 1.17386389 88.43061066 -36.16706085 0.76761329 88.31217194 -35.92765427 0.40162241 88.26081085
		 -35.80141068 0.11171699 88.28153229 -35.80068588 -0.073725104 88.37231445 -35.92555618 -0.1365515 88.5242691
		 -36.16379166 -0.070612133 88.72253418 -36.49207687 0.11763823 88.94768524 -36.87827682 0.40977228 89.17769623
		 -37.28458405 0.77719414 89.39004517 -37.67123032 1.18393755 89.56394958 -38.00036621094 1.59018826 89.68238068
		 -38.2397728 1.95617914 89.73374939 -38.36601639 2.24608445 89.71302795 -38.36674118 2.43152666 89.62224579
		 -38.19311142 2.51918554 89.52061462 -37.95487595 2.45324612 89.32236481 -37.62659073 2.26499557 89.097198486
		 -37.24039078 1.97286129 88.86719513 -36.83408356 1.60543942 88.65484619 -36.44743729 1.19869566 88.48094177
		 -36.11829758 0.792445 88.36251068 -35.87889481 0.42645407 88.31114197 -35.75265121 0.13654858 88.33187103
		 -35.75192642 -0.048893452 88.42264557 -35.87679672 -0.11171985 88.57460022 -36.1150322 -0.04578054 88.7728653
		 -36.4433136 0.14246988 88.99801636 -36.82951355 0.43460399 89.22803497 -37.23582458 0.80202574 89.44038391
		 -37.62246704 1.20876932 89.6142807 -37.95160675 1.6150198 89.73271179 -38.19100952 1.98101068 89.78408051
		 -38.31725311 2.27091622 89.76335907 -38.31797791 2.45635819 89.67258453 -37.083713531 1.17890096 88.99727631
		 -37.034954071 1.20373249 89.047615051 -34.97560501 -0.98768836 88.25870514 -34.99782562 -0.98768836 88.21509552
		 -35.032432556 -0.98768836 88.18048859 -35.076042175 -0.98768836 88.15826416 -35.12438202 -0.98768836 88.15061188
		 -35.17272186 -0.98768836 88.15826416 -35.21633148 -0.98768836 88.18048859 -35.25093842 -0.98768836 88.21509552
		 -35.27315903 -0.98768836 88.25870514 -35.28081512 -0.98768836 88.30704498 -35.27315903 -0.98768836 88.35538483
		 -35.25093842 -0.98768836 88.39899445 -35.21633148 -0.98768836 88.43360138 -35.17272186 -0.98768836 88.45582581
		 -35.12438202 -0.98768836 88.46347809 -35.076042175 -0.98768836 88.45582581 -35.032432556 -0.98768836 88.43360138
		 -34.99782562 -0.98768836 88.39899445 -34.97560501 -0.98768836 88.35538483 -34.96794891 -0.98768836 88.30704498
		 -34.83049011 -0.95105654 88.21155548 -34.87438202 -0.95105654 88.12541199 -34.94274521 -0.95105654 88.057044983
		 -35.028888702 -0.95105654 88.013153076 -35.12438202 -0.95105654 87.99803162 -35.21987534 -0.95105654 88.013153076
		 -35.30601883 -0.95105654 88.057044983 -35.37438202 -0.95105654 88.12541199 -35.41827393 -0.95105654 88.21155548
		 -35.4333992 -0.95105654 88.30704498 -35.41827393 -0.95105654 88.40253448 -35.37438202 -0.95105654 88.48867798
		 -35.30601883 -0.95105654 88.55704498 -35.21987534 -0.95105654 88.60093689 -35.12438202 -0.95105654 88.61605835
		 -35.028888702 -0.95105654 88.60093689 -34.94274521 -0.95105654 88.55704498 -34.87438202 -0.95105654 88.48867798
		 -34.83049011 -0.95105654 88.40253448 -34.81536484 -0.95105654 88.30704498 -34.69261169 -0.89100653 88.16675568
		 -34.75709534 -0.89100653 88.04019928 -34.8575325 -0.89100653 87.9397583 -34.98409271 -0.89100653 87.87527466
		 -35.12438202 -0.89100653 87.85305786 -35.26467133 -0.89100653 87.87527466 -35.39123154 -0.89100653 87.9397583
		 -35.4916687 -0.89100653 88.04019928 -35.55615234 -0.89100653 88.16675568 -35.57837296 -0.89100653 88.30704498
		 -35.55615234 -0.89100653 88.44733429 -35.4916687 -0.89100653 88.57389069 -35.39123154 -0.89100653 88.67433167
		 -35.26467133 -0.89100653 88.73881531 -35.12438202 -0.89100653 88.7610321 -34.98409271 -0.89100653 88.73881531
		 -34.8575325 -0.89100653 88.67433167 -34.75709534 -0.89100653 88.57389069 -34.69261169 -0.89100653 88.44733429
		 -34.67039108 -0.89100653 88.30704498 -34.56536484 -0.809017 88.12541199 -34.6488533 -0.809017 87.96155548
		 -34.7788887 -0.809017 87.83152008 -34.94274521 -0.809017 87.74802399 -35.12438202 -0.809017 87.71926117
		 -35.30601883 -0.809017 87.74802399 -35.46987534 -0.809017 87.83152008 -35.59991074 -0.809017 87.96155548
		 -35.6833992 -0.809017 88.12541199 -35.71216583 -0.809017 88.30704498 -35.6833992 -0.809017 88.48867798
		 -35.59991074 -0.809017 88.65253448 -35.46987534 -0.809017 88.78256989 -35.30601883 -0.809017 88.86605835
		 -35.12438202 -0.809017 88.8948288 -34.94274521 -0.809017 88.86605835 -34.7788887 -0.809017 88.78256989
		 -34.6488533 -0.809017 88.65253448 -34.56536484 -0.809017 88.48867798 -34.53659821 -0.809017 88.30704498
		 -34.45188141 -0.70710677 88.088539124 -34.55231857 -0.70710677 87.89141846;
	setAttr ".vt[166:331]" -34.70875549 -0.70710677 87.73498535 -34.90587234 -0.70710677 87.63454437
		 -35.12438202 -0.70710677 87.59993744 -35.34289169 -0.70710677 87.63454437 -35.54000854 -0.70710677 87.73498535
		 -35.69644165 -0.70710677 87.89141846 -35.79688263 -0.70710677 88.088539124 -35.83148956 -0.70710677 88.30704498
		 -35.79688263 -0.70710677 88.52555084 -35.69644165 -0.70710677 88.72267151 -35.54000854 -0.70710677 88.87910461
		 -35.34289169 -0.70710677 88.97954559 -35.12438202 -0.70710677 89.014152527 -34.90587234 -0.70710677 88.97954559
		 -34.70875549 -0.70710677 88.87910461 -34.55232239 -0.70710677 88.72267151 -34.45188522 -0.70710677 88.52555084
		 -34.41727448 -0.70710677 88.30704498 -34.3549614 -0.58778524 88.057044983 -34.46987152 -0.58778524 87.83152008
		 -34.6488533 -0.58778524 87.65253448 -34.87438202 -0.58778524 87.53762054 -35.12438202 -0.58778524 87.49802399
		 -35.37438202 -0.58778524 87.53762054 -35.59991074 -0.58778524 87.65253448 -35.77889252 -0.58778524 87.83152008
		 -35.89380264 -0.58778524 88.057044983 -35.9333992 -0.58778524 88.30704498 -35.89380264 -0.58778524 88.55704498
		 -35.77889252 -0.58778524 88.78256989 -35.59991074 -0.58778524 88.96155548 -35.37438202 -0.58778524 89.076469421
		 -35.12438202 -0.58778524 89.11605835 -34.87438202 -0.58778524 89.076469421 -34.6488533 -0.58778524 88.96155548
		 -34.46987534 -0.58778524 88.78256989 -34.3549614 -0.58778524 88.55704498 -34.31536484 -0.58778524 88.30704498
		 -34.27698517 -0.45399052 88.031707764 -34.40354156 -0.45399052 87.7833252 -34.60066223 -0.45399052 87.58620453
		 -34.8490448 -0.45399052 87.45964813 -35.12438202 -0.45399052 87.41603851 -35.39971924 -0.45399052 87.45964813
		 -35.64810181 -0.45399052 87.58620453 -35.84522247 -0.45399052 87.7833252 -35.97177887 -0.45399052 88.031707764
		 -36.015388489 -0.45399052 88.30704498 -35.97177887 -0.45399052 88.5823822 -35.84522247 -0.45399052 88.83076477
		 -35.64810181 -0.45399052 89.027885437 -35.39971924 -0.45399052 89.15444183 -35.12438202 -0.45399052 89.19805145
		 -34.8490448 -0.45399052 89.15444183 -34.60066223 -0.45399052 89.027885437 -34.40354156 -0.45399052 88.83076477
		 -34.27698517 -0.45399052 88.5823822 -34.23337555 -0.45399052 88.30704498 -34.21987152 -0.30901697 88.013153076
		 -34.3549614 -0.30901697 87.74802399 -34.56536484 -0.30901697 87.53762054 -34.83049011 -0.30901697 87.40253448
		 -35.12438202 -0.30901697 87.35598755 -35.41827393 -0.30901697 87.40253448 -35.6833992 -0.30901697 87.53762054
		 -35.89380264 -0.30901697 87.74803162 -36.028892517 -0.30901697 88.013153076 -36.075439453 -0.30901697 88.30704498
		 -36.028892517 -0.30901697 88.60093689 -35.89380264 -0.30901697 88.86605835 -35.6833992 -0.30901697 89.076469421
		 -35.41827393 -0.30901697 89.21155548 -35.12438202 -0.30901697 89.25810242 -34.83049011 -0.30901697 89.21155548
		 -34.56536484 -0.30901697 89.076469421 -34.3549614 -0.30901697 88.86605835 -34.21987534 -0.30901697 88.60093689
		 -34.17332458 -0.30901697 88.30704498 -34.18503571 -0.15643437 88.0018310547 -34.32532501 -0.15643437 87.72649384
		 -34.54383469 -0.15643437 87.50798798 -34.81916809 -0.15643437 87.36769867 -35.12438202 -0.15643437 87.31935883
		 -35.42959595 -0.15643437 87.36769867 -35.70492935 -0.15643437 87.50798798 -35.92343903 -0.15643437 87.72649384
		 -36.063728333 -0.15643437 88.0018310547 -36.11207199 -0.15643437 88.30704498 -36.063728333 -0.15643437 88.61225891
		 -35.92343903 -0.15643437 88.88759613 -35.70492935 -0.15643437 89.10610199 -35.42959595 -0.15643437 89.2463913
		 -35.12438202 -0.15643437 89.29473114 -34.81916809 -0.15643437 89.2463913 -34.54383469 -0.15643437 89.10610199
		 -34.32532501 -0.15643437 88.88759613 -34.18503571 -0.15643437 88.61225891 -34.13669205 -0.15643437 88.30704498
		 -34.17332458 0 87.99803162 -34.31536484 0 87.71926117 -34.53659821 0 87.49802399
		 -34.81536484 0 87.35598755 -35.12438202 0 87.30704498 -35.4333992 0 87.35598755 -35.71216583 0 87.49802399
		 -35.9333992 0 87.71926117 -36.075439453 0 87.99803162 -36.12438202 0 88.30704498
		 -36.075439453 0 88.61605835 -35.9333992 0 88.8948288 -35.71216583 0 89.11605835 -35.4333992 0 89.25810242
		 -35.12438202 0 89.30704498 -34.81536484 0 89.25810242 -34.53659821 0 89.11605835
		 -34.31536484 0 88.8948288 -34.17332458 0 88.61605835 -34.12438202 0 88.30704498 -34.18503571 0.15643437 88.0018310547
		 -34.32532501 0.15643437 87.72649384 -34.54383469 0.15643437 87.50798798 -34.81916809 0.15643437 87.36769867
		 -35.12438202 0.15643437 87.31935883 -35.42959595 0.15643437 87.36769867 -35.70492935 0.15643437 87.50798798
		 -35.92343903 0.15643437 87.72649384 -36.063728333 0.15643437 88.0018310547 -36.11207199 0.15643437 88.30704498
		 -36.063728333 0.15643437 88.61225891 -35.92343903 0.15643437 88.88759613 -35.70492935 0.15643437 89.10610199
		 -35.42959595 0.15643437 89.2463913 -35.12438202 0.15643437 89.29473114 -34.81916809 0.15643437 89.2463913
		 -34.54383469 0.15643437 89.10610199 -34.32532501 0.15643437 88.88759613 -34.18503571 0.15643437 88.61225891
		 -34.13669205 0.15643437 88.30704498 -34.21987152 0.30901697 88.013153076 -34.3549614 0.30901697 87.74802399
		 -34.56536484 0.30901697 87.53762054 -34.83049011 0.30901697 87.40253448 -35.12438202 0.30901697 87.35598755
		 -35.41827393 0.30901697 87.40253448 -35.6833992 0.30901697 87.53762054 -35.89380264 0.30901697 87.74803162
		 -36.028892517 0.30901697 88.013153076 -36.075439453 0.30901697 88.30704498 -36.028892517 0.30901697 88.60093689
		 -35.89380264 0.30901697 88.86605835 -35.6833992 0.30901697 89.076469421 -35.41827393 0.30901697 89.21155548
		 -35.12438202 0.30901697 89.25810242 -34.83049011 0.30901697 89.21155548 -34.56536484 0.30901697 89.076469421
		 -34.3549614 0.30901697 88.86605835 -34.21987534 0.30901697 88.60093689 -34.17332458 0.30901697 88.30704498
		 -34.27698517 0.45399052 88.031707764 -34.40354156 0.45399052 87.7833252 -34.60066223 0.45399052 87.58620453
		 -34.8490448 0.45399052 87.45964813 -35.12438202 0.45399052 87.41603851 -35.39971924 0.45399052 87.45964813
		 -35.64810181 0.45399052 87.58620453 -35.84522247 0.45399052 87.7833252;
	setAttr ".vt[332:465]" -35.97177887 0.45399052 88.031707764 -36.015388489 0.45399052 88.30704498
		 -35.97177887 0.45399052 88.5823822 -35.84522247 0.45399052 88.83076477 -35.64810181 0.45399052 89.027885437
		 -35.39971924 0.45399052 89.15444183 -35.12438202 0.45399052 89.19805145 -34.8490448 0.45399052 89.15444183
		 -34.60066223 0.45399052 89.027885437 -34.40354156 0.45399052 88.83076477 -34.27698517 0.45399052 88.5823822
		 -34.23337555 0.45399052 88.30704498 -34.3549614 0.58778524 88.057044983 -34.46987152 0.58778524 87.83152008
		 -34.6488533 0.58778524 87.65253448 -34.87438202 0.58778524 87.53762054 -35.12438202 0.58778524 87.49802399
		 -35.37438202 0.58778524 87.53762054 -35.59991074 0.58778524 87.65253448 -35.77889252 0.58778524 87.83152008
		 -35.89380264 0.58778524 88.057044983 -35.9333992 0.58778524 88.30704498 -35.89380264 0.58778524 88.55704498
		 -35.77889252 0.58778524 88.78256989 -35.59991074 0.58778524 88.96155548 -35.37438202 0.58778524 89.076469421
		 -35.12438202 0.58778524 89.11605835 -34.87438202 0.58778524 89.076469421 -34.6488533 0.58778524 88.96155548
		 -34.46987534 0.58778524 88.78256989 -34.3549614 0.58778524 88.55704498 -34.31536484 0.58778524 88.30704498
		 -34.45188141 0.70710677 88.088539124 -34.55231857 0.70710677 87.89141846 -34.70875549 0.70710677 87.73498535
		 -34.90587234 0.70710677 87.63454437 -35.12438202 0.70710677 87.59993744 -35.34289169 0.70710677 87.63454437
		 -35.54000854 0.70710677 87.73498535 -35.69644165 0.70710677 87.89141846 -35.79688263 0.70710677 88.088539124
		 -35.83148956 0.70710677 88.30704498 -35.79688263 0.70710677 88.52555084 -35.69644165 0.70710677 88.72267151
		 -35.54000854 0.70710677 88.87910461 -35.34289169 0.70710677 88.97954559 -35.12438202 0.70710677 89.014152527
		 -34.90587234 0.70710677 88.97954559 -34.70875549 0.70710677 88.87910461 -34.55232239 0.70710677 88.72267151
		 -34.45188522 0.70710677 88.52555084 -34.41727448 0.70710677 88.30704498 -34.56536484 0.809017 88.12541199
		 -34.6488533 0.809017 87.96155548 -34.7788887 0.809017 87.83152008 -34.94274521 0.809017 87.74802399
		 -35.12438202 0.809017 87.71926117 -35.30601883 0.809017 87.74802399 -35.46987534 0.809017 87.83152008
		 -35.59991074 0.809017 87.96155548 -35.6833992 0.809017 88.12541199 -35.71216583 0.809017 88.30704498
		 -35.6833992 0.809017 88.48867798 -35.59991074 0.809017 88.65253448 -35.46987534 0.809017 88.78256989
		 -35.30601883 0.809017 88.86605835 -35.12438202 0.809017 88.8948288 -34.94274521 0.809017 88.86605835
		 -34.7788887 0.809017 88.78256989 -34.6488533 0.809017 88.65253448 -34.56536484 0.809017 88.48867798
		 -34.53659821 0.809017 88.30704498 -34.69261169 0.89100653 88.16675568 -34.75709534 0.89100653 88.04019928
		 -34.8575325 0.89100653 87.9397583 -34.98409271 0.89100653 87.87527466 -35.12438202 0.89100653 87.85305786
		 -35.26467133 0.89100653 87.87527466 -35.39123154 0.89100653 87.9397583 -35.4916687 0.89100653 88.04019928
		 -35.55615234 0.89100653 88.16675568 -35.57837296 0.89100653 88.30704498 -35.55615234 0.89100653 88.44733429
		 -35.4916687 0.89100653 88.57389069 -35.39123154 0.89100653 88.67433167 -35.26467133 0.89100653 88.73881531
		 -35.12438202 0.89100653 88.7610321 -34.98409271 0.89100653 88.73881531 -34.8575325 0.89100653 88.67433167
		 -34.75709534 0.89100653 88.57389069 -34.69261169 0.89100653 88.44733429 -34.67039108 0.89100653 88.30704498
		 -34.83049011 0.95105654 88.21155548 -34.87438202 0.95105654 88.12541199 -34.94274521 0.95105654 88.057044983
		 -35.028888702 0.95105654 88.013153076 -35.12438202 0.95105654 87.99803162 -35.21987534 0.95105654 88.013153076
		 -35.30601883 0.95105654 88.057044983 -35.37438202 0.95105654 88.12541199 -35.41827393 0.95105654 88.21155548
		 -35.4333992 0.95105654 88.30704498 -35.41827393 0.95105654 88.40253448 -35.37438202 0.95105654 88.48867798
		 -35.30601883 0.95105654 88.55704498 -35.21987534 0.95105654 88.60093689 -35.12438202 0.95105654 88.61605835
		 -35.028888702 0.95105654 88.60093689 -34.94274521 0.95105654 88.55704498 -34.87438202 0.95105654 88.48867798
		 -34.83049011 0.95105654 88.40253448 -34.81536484 0.95105654 88.30704498 -34.97560501 0.98768836 88.25870514
		 -34.99782562 0.98768836 88.21509552 -35.032432556 0.98768836 88.18048859 -35.076042175 0.98768836 88.15826416
		 -35.12438202 0.98768836 88.15061188 -35.17272186 0.98768836 88.15826416 -35.21633148 0.98768836 88.18048859
		 -35.25093842 0.98768836 88.21509552 -35.27315903 0.98768836 88.25870514 -35.28081512 0.98768836 88.30704498
		 -35.27315903 0.98768836 88.35538483 -35.25093842 0.98768836 88.39899445 -35.21633148 0.98768836 88.43360138
		 -35.17272186 0.98768836 88.45582581 -35.12438202 0.98768836 88.46347809 -35.076042175 0.98768836 88.45582581
		 -35.032432556 0.98768836 88.43360138 -34.99782562 0.98768836 88.39899445 -34.97560501 0.98768836 88.35538483
		 -34.96794891 0.98768836 88.30704498 -35.12438202 -1 88.30704498 -35.12438202 1 88.30704498;
	setAttr -s 980 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0 0 20 1 1 21 1
		 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1
		 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1 40 3 1 40 4 1
		 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1 40 14 1 40 15 1
		 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1 25 41 1 26 41 1
		 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1 36 41 1 37 41 1
		 38 41 1 39 41 1 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 62 0 42 62 1 43 63 1
		 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1
		 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 82 42 1 82 43 1 82 44 1 82 45 1
		 82 46 1 82 47 1;
	setAttr ".ed[166:331]" 82 48 1 82 49 1 82 50 1 82 51 1 82 52 1 82 53 1 82 54 1
		 82 55 1 82 56 1 82 57 1 82 58 1 82 59 1 82 60 1 82 61 1 62 83 1 63 83 1 64 83 1 65 83 1
		 66 83 1 67 83 1 68 83 1 69 83 1 70 83 1 71 83 1 72 83 1 73 83 1 74 83 1 75 83 1 76 83 1
		 77 83 1 78 83 1 79 83 1 80 83 1 81 83 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1
		 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 84 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1
		 109 110 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 104 1 124 125 1 125 126 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 134 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 124 1 144 145 1
		 145 146 1 146 147 1 147 148 1 148 149 1 149 150 1 150 151 1 151 152 1 152 153 1 153 154 1
		 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1
		 163 144 1 164 165 1 165 166 1 166 167 1 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 180 1 180 181 1
		 181 182 1 182 183 1 183 164 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 190 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 200 1 200 201 1 201 202 1 202 203 1 203 184 1 204 205 1 205 206 1 206 207 1 207 208 1
		 208 209 1 209 210 1 210 211 1 211 212 1 212 213 1 213 214 1 214 215 1 215 216 1;
	setAttr ".ed[332:497]" 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1
		 222 223 1 223 204 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 232 1 232 233 1 233 234 1 234 235 1 235 236 1 236 237 1 237 238 1 238 239 1 239 240 1
		 240 241 1 241 242 1 242 243 1 243 224 1 244 245 1 245 246 1 246 247 1 247 248 1 248 249 1
		 249 250 1 250 251 1 251 252 1 252 253 1 253 254 1 254 255 1 255 256 1 256 257 1 257 258 1
		 258 259 1 259 260 1 260 261 1 261 262 1 262 263 1 263 244 1 264 265 1 265 266 1 266 267 1
		 267 268 1 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1 274 275 1 275 276 1
		 276 277 1 277 278 1 278 279 1 279 280 1 280 281 1 281 282 1 282 283 1 283 264 1 284 285 1
		 285 286 1 286 287 1 287 288 1 288 289 1 289 290 1 290 291 1 291 292 1 292 293 1 293 294 1
		 294 295 1 295 296 1 296 297 1 297 298 1 298 299 1 299 300 1 300 301 1 301 302 1 302 303 1
		 303 284 1 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1
		 312 313 1 313 314 1 314 315 1 315 316 1 316 317 1 317 318 1 318 319 1 319 320 1 320 321 1
		 321 322 1 322 323 1 323 304 1 324 325 1 325 326 1 326 327 1 327 328 1 328 329 1 329 330 1
		 330 331 1 331 332 1 332 333 1 333 334 1 334 335 1 335 336 1 336 337 1 337 338 1 338 339 1
		 339 340 1 340 341 1 341 342 1 342 343 1 343 324 1 344 345 1 345 346 1 346 347 1 347 348 1
		 348 349 1 349 350 1 350 351 1 351 352 1 352 353 1 353 354 1 354 355 1 355 356 1 356 357 1
		 357 358 1 358 359 1 359 360 1 360 361 1 361 362 1 362 363 1 363 344 1 364 365 1 365 366 1
		 366 367 1 367 368 1 368 369 1 369 370 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1
		 375 376 1 376 377 1 377 378 1 378 379 1 379 380 1 380 381 1 381 382 1;
	setAttr ".ed[498:663]" 382 383 1 383 364 1 384 385 1 385 386 1 386 387 1 387 388 1
		 388 389 1 389 390 1 390 391 1 391 392 1 392 393 1 393 394 1 394 395 1 395 396 1 396 397 1
		 397 398 1 398 399 1 399 400 1 400 401 1 401 402 1 402 403 1 403 384 1 404 405 1 405 406 1
		 406 407 1 407 408 1 408 409 1 409 410 1 410 411 1 411 412 1 412 413 1 413 414 1 414 415 1
		 415 416 1 416 417 1 417 418 1 418 419 1 419 420 1 420 421 1 421 422 1 422 423 1 423 404 1
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 432 1 432 433 1
		 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 440 1 440 441 1 441 442 1
		 442 443 1 443 424 1 444 445 1 445 446 1 446 447 1 447 448 1 448 449 1 449 450 1 450 451 1
		 451 452 1 452 453 1 453 454 1 454 455 1 455 456 1 456 457 1 457 458 1 458 459 1 459 460 1
		 460 461 1 461 462 1 462 463 1 463 444 1 84 104 1 85 105 1 86 106 1 87 107 1 88 108 1
		 89 109 1 90 110 1 91 111 1 92 112 1 93 113 1 94 114 1 95 115 1 96 116 1 97 117 1
		 98 118 1 99 119 1 100 120 1 101 121 1 102 122 1 103 123 1 104 124 1 105 125 1 106 126 1
		 107 127 1 108 128 1 109 129 1 110 130 1 111 131 1 112 132 1 113 133 1 114 134 1 115 135 1
		 116 136 1 117 137 1 118 138 1 119 139 1 120 140 1 121 141 1 122 142 1 123 143 1 124 144 1
		 125 145 1 126 146 1 127 147 1 128 148 1 129 149 1 130 150 1 131 151 1 132 152 1 133 153 1
		 134 154 1 135 155 1 136 156 1 137 157 1 138 158 1 139 159 1 140 160 1 141 161 1 142 162 1
		 143 163 1 144 164 1 145 165 1 146 166 1 147 167 1 148 168 1 149 169 1 150 170 1 151 171 1
		 152 172 1 153 173 1 154 174 1 155 175 1 156 176 1 157 177 1 158 178 1 159 179 1 160 180 1
		 161 181 1 162 182 1 163 183 1 164 184 1 165 185 1 166 186 1 167 187 1;
	setAttr ".ed[664:829]" 168 188 1 169 189 1 170 190 1 171 191 1 172 192 1 173 193 1
		 174 194 1 175 195 1 176 196 1 177 197 1 178 198 1 179 199 1 180 200 1 181 201 1 182 202 1
		 183 203 1 184 204 1 185 205 1 186 206 1 187 207 1 188 208 1 189 209 1 190 210 1 191 211 1
		 192 212 1 193 213 1 194 214 1 195 215 1 196 216 1 197 217 1 198 218 1 199 219 1 200 220 1
		 201 221 1 202 222 1 203 223 1 204 224 1 205 225 1 206 226 1 207 227 1 208 228 1 209 229 1
		 210 230 1 211 231 1 212 232 1 213 233 1 214 234 1 215 235 1 216 236 1 217 237 1 218 238 1
		 219 239 1 220 240 1 221 241 1 222 242 1 223 243 1 224 244 1 225 245 1 226 246 1 227 247 1
		 228 248 1 229 249 1 230 250 1 231 251 1 232 252 1 233 253 1 234 254 1 235 255 1 236 256 1
		 237 257 1 238 258 1 239 259 1 240 260 1 241 261 1 242 262 1 243 263 1 244 264 1 245 265 1
		 246 266 1 247 267 1 248 268 1 249 269 1 250 270 1 251 271 1 252 272 1 253 273 1 254 274 1
		 255 275 1 256 276 1 257 277 1 258 278 1 259 279 1 260 280 1 261 281 1 262 282 1 263 283 1
		 264 284 1 265 285 1 266 286 1 267 287 1 268 288 1 269 289 1 270 290 1 271 291 1 272 292 1
		 273 293 1 274 294 1 275 295 1 276 296 1 277 297 1 278 298 1 279 299 1 280 300 1 281 301 1
		 282 302 1 283 303 1 284 304 1 285 305 1 286 306 1 287 307 1 288 308 1 289 309 1 290 310 1
		 291 311 1 292 312 1 293 313 1 294 314 1 295 315 1 296 316 1 297 317 1 298 318 1 299 319 1
		 300 320 1 301 321 1 302 322 1 303 323 1 304 324 1 305 325 1 306 326 1 307 327 1 308 328 1
		 309 329 1 310 330 1 311 331 1 312 332 1 313 333 1 314 334 1 315 335 1 316 336 1 317 337 1
		 318 338 1 319 339 1 320 340 1 321 341 1 322 342 1 323 343 1 324 344 1 325 345 1 326 346 1
		 327 347 1 328 348 1 329 349 1 330 350 1 331 351 1 332 352 1 333 353 1;
	setAttr ".ed[830:979]" 334 354 1 335 355 1 336 356 1 337 357 1 338 358 1 339 359 1
		 340 360 1 341 361 1 342 362 1 343 363 1 344 364 1 345 365 1 346 366 1 347 367 1 348 368 1
		 349 369 1 350 370 1 351 371 1 352 372 1 353 373 1 354 374 1 355 375 1 356 376 1 357 377 1
		 358 378 1 359 379 1 360 380 1 361 381 1 362 382 1 363 383 1 364 384 1 365 385 1 366 386 1
		 367 387 1 368 388 1 369 389 1 370 390 1 371 391 1 372 392 1 373 393 1 374 394 1 375 395 1
		 376 396 1 377 397 1 378 398 1 379 399 1 380 400 1 381 401 1 382 402 1 383 403 1 384 404 1
		 385 405 1 386 406 1 387 407 1 388 408 1 389 409 1 390 410 1 391 411 1 392 412 1 393 413 1
		 394 414 1 395 415 1 396 416 1 397 417 1 398 418 1 399 419 1 400 420 1 401 421 1 402 422 1
		 403 423 1 404 424 1 405 425 1 406 426 1 407 427 1 408 428 1 409 429 1 410 430 1 411 431 1
		 412 432 1 413 433 1 414 434 1 415 435 1 416 436 1 417 437 1 418 438 1 419 439 1 420 440 1
		 421 441 1 422 442 1 423 443 1 424 444 1 425 445 1 426 446 1 427 447 1 428 448 1 429 449 1
		 430 450 1 431 451 1 432 452 1 433 453 1 434 454 1 435 455 1 436 456 1 437 457 1 438 458 1
		 439 459 1 440 460 1 441 461 1 442 462 1 443 463 1 464 84 1 464 85 1 464 86 1 464 87 1
		 464 88 1 464 89 1 464 90 1 464 91 1 464 92 1 464 93 1 464 94 1 464 95 1 464 96 1
		 464 97 1 464 98 1 464 99 1 464 100 1 464 101 1 464 102 1 464 103 1 444 465 1 445 465 1
		 446 465 1 447 465 1 448 465 1 449 465 1 450 465 1 451 465 1 452 465 1 453 465 1 454 465 1
		 455 465 1 456 465 1 457 465 1 458 465 1 459 465 1 460 465 1 461 465 1 462 465 1 463 465 1;
	setAttr -s 520 -ch 1960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80
		f 4 140 120 -142 -101
		mu 0 4 84 85 86 87
		f 4 141 121 -143 -102
		mu 0 4 87 86 88 89
		f 4 142 122 -144 -103
		mu 0 4 89 88 90 91
		f 4 143 123 -145 -104
		mu 0 4 91 90 92 93
		f 4 144 124 -146 -105
		mu 0 4 93 92 94 95
		f 4 145 125 -147 -106
		mu 0 4 95 94 96 97
		f 4 146 126 -148 -107
		mu 0 4 97 96 98 99
		f 4 147 127 -149 -108
		mu 0 4 99 98 100 101
		f 4 148 128 -150 -109
		mu 0 4 101 100 102 103
		f 4 149 129 -151 -110
		mu 0 4 103 102 104 105
		f 4 150 130 -152 -111
		mu 0 4 105 104 106 107
		f 4 151 131 -153 -112
		mu 0 4 107 106 108 109
		f 4 152 132 -154 -113
		mu 0 4 109 108 110 111
		f 4 153 133 -155 -114
		mu 0 4 111 110 112 113
		f 4 154 134 -156 -115
		mu 0 4 113 112 114 115
		f 4 155 135 -157 -116
		mu 0 4 115 114 116 117
		f 4 156 136 -158 -117
		mu 0 4 117 116 118 119
		f 4 157 137 -159 -118
		mu 0 4 119 118 120 121
		f 4 158 138 -160 -119
		mu 0 4 121 120 122 123
		f 4 159 139 -141 -120
		mu 0 4 123 122 124 125
		f 3 -162 160 100
		mu 0 3 126 127 128
		f 3 -163 161 101
		mu 0 3 129 127 126
		f 3 -164 162 102
		mu 0 3 130 127 129
		f 3 -165 163 103
		mu 0 3 131 127 130
		f 3 -166 164 104
		mu 0 3 132 127 131
		f 3 -167 165 105
		mu 0 3 133 127 132
		f 3 -168 166 106
		mu 0 3 134 127 133
		f 3 -169 167 107
		mu 0 3 135 127 134
		f 3 -170 168 108
		mu 0 3 136 127 135
		f 3 -171 169 109
		mu 0 3 137 127 136
		f 3 -172 170 110
		mu 0 3 138 127 137
		f 3 -173 171 111
		mu 0 3 139 127 138
		f 3 -174 172 112
		mu 0 3 140 127 139
		f 3 -175 173 113
		mu 0 3 141 127 140
		f 3 -176 174 114
		mu 0 3 142 127 141
		f 3 -177 175 115
		mu 0 3 143 127 142
		f 3 -178 176 116
		mu 0 3 144 127 143
		f 3 -179 177 117
		mu 0 3 145 127 144
		f 3 -180 178 118
		mu 0 3 146 127 145
		f 3 -161 179 119
		mu 0 3 128 127 146
		f 3 180 -182 -121
		mu 0 3 147 148 149
		f 3 181 -183 -122
		mu 0 3 149 148 150
		f 3 182 -184 -123
		mu 0 3 150 148 151
		f 3 183 -185 -124
		mu 0 3 151 148 152
		f 3 184 -186 -125
		mu 0 3 152 148 153
		f 3 185 -187 -126
		mu 0 3 153 148 154
		f 3 186 -188 -127
		mu 0 3 154 148 155
		f 3 187 -189 -128
		mu 0 3 155 148 156
		f 3 188 -190 -129
		mu 0 3 156 148 157
		f 3 189 -191 -130
		mu 0 3 157 148 158
		f 3 190 -192 -131
		mu 0 3 158 148 159
		f 3 191 -193 -132
		mu 0 3 159 148 160
		f 3 192 -194 -133
		mu 0 3 160 148 161
		f 3 193 -195 -134
		mu 0 3 161 148 162
		f 3 194 -196 -135
		mu 0 3 162 148 163
		f 3 195 -197 -136
		mu 0 3 163 148 164
		f 3 196 -198 -137
		mu 0 3 164 148 165
		f 3 197 -199 -138
		mu 0 3 165 148 166
		f 3 198 -200 -139
		mu 0 3 166 148 167
		f 3 199 -181 -140
		mu 0 3 167 148 147
		f 4 200 581 -221 -581
		mu 0 4 168 169 170 171
		f 4 201 582 -222 -582
		mu 0 4 169 172 173 170
		f 4 202 583 -223 -583
		mu 0 4 172 174 175 173
		f 4 203 584 -224 -584
		mu 0 4 174 176 177 175
		f 4 204 585 -225 -585
		mu 0 4 176 178 179 177
		f 4 205 586 -226 -586
		mu 0 4 178 180 181 179
		f 4 206 587 -227 -587
		mu 0 4 180 182 183 181
		f 4 207 588 -228 -588
		mu 0 4 182 184 185 183
		f 4 208 589 -229 -589
		mu 0 4 184 186 187 185
		f 4 209 590 -230 -590
		mu 0 4 186 188 189 187
		f 4 210 591 -231 -591
		mu 0 4 188 190 191 189
		f 4 211 592 -232 -592
		mu 0 4 190 192 193 191
		f 4 212 593 -233 -593
		mu 0 4 192 194 195 193
		f 4 213 594 -234 -594
		mu 0 4 194 196 197 195
		f 4 214 595 -235 -595
		mu 0 4 196 198 199 197
		f 4 215 596 -236 -596
		mu 0 4 198 200 201 199
		f 4 216 597 -237 -597
		mu 0 4 200 202 203 201
		f 4 217 598 -238 -598
		mu 0 4 202 204 205 203
		f 4 218 599 -239 -599
		mu 0 4 204 206 207 205
		f 4 219 580 -240 -600
		mu 0 4 206 208 209 207
		f 4 220 601 -241 -601
		mu 0 4 171 170 210 211
		f 4 221 602 -242 -602
		mu 0 4 170 173 212 210
		f 4 222 603 -243 -603
		mu 0 4 173 175 213 212
		f 4 223 604 -244 -604
		mu 0 4 175 177 214 213
		f 4 224 605 -245 -605
		mu 0 4 177 179 215 214
		f 4 225 606 -246 -606
		mu 0 4 179 181 216 215
		f 4 226 607 -247 -607
		mu 0 4 181 183 217 216
		f 4 227 608 -248 -608
		mu 0 4 183 185 218 217
		f 4 228 609 -249 -609
		mu 0 4 185 187 219 218
		f 4 229 610 -250 -610
		mu 0 4 187 189 220 219
		f 4 230 611 -251 -611
		mu 0 4 189 191 221 220
		f 4 231 612 -252 -612
		mu 0 4 191 193 222 221
		f 4 232 613 -253 -613
		mu 0 4 193 195 223 222
		f 4 233 614 -254 -614
		mu 0 4 195 197 224 223
		f 4 234 615 -255 -615
		mu 0 4 197 199 225 224
		f 4 235 616 -256 -616
		mu 0 4 199 201 226 225
		f 4 236 617 -257 -617
		mu 0 4 201 203 227 226
		f 4 237 618 -258 -618
		mu 0 4 203 205 228 227
		f 4 238 619 -259 -619
		mu 0 4 205 207 229 228
		f 4 239 600 -260 -620
		mu 0 4 207 209 230 229
		f 4 240 621 -261 -621
		mu 0 4 211 210 231 232
		f 4 241 622 -262 -622
		mu 0 4 210 212 233 231
		f 4 242 623 -263 -623
		mu 0 4 212 213 234 233
		f 4 243 624 -264 -624
		mu 0 4 213 214 235 234
		f 4 244 625 -265 -625
		mu 0 4 214 215 236 235
		f 4 245 626 -266 -626
		mu 0 4 215 216 237 236
		f 4 246 627 -267 -627
		mu 0 4 216 217 238 237
		f 4 247 628 -268 -628
		mu 0 4 217 218 239 238
		f 4 248 629 -269 -629
		mu 0 4 218 219 240 239
		f 4 249 630 -270 -630
		mu 0 4 219 220 241 240
		f 4 250 631 -271 -631
		mu 0 4 220 221 242 241
		f 4 251 632 -272 -632
		mu 0 4 221 222 243 242
		f 4 252 633 -273 -633
		mu 0 4 222 223 244 243
		f 4 253 634 -274 -634
		mu 0 4 223 224 245 244
		f 4 254 635 -275 -635
		mu 0 4 224 225 246 245
		f 4 255 636 -276 -636
		mu 0 4 225 226 247 246
		f 4 256 637 -277 -637
		mu 0 4 226 227 248 247
		f 4 257 638 -278 -638
		mu 0 4 227 228 249 248
		f 4 258 639 -279 -639
		mu 0 4 228 229 250 249
		f 4 259 620 -280 -640
		mu 0 4 229 230 251 250
		f 4 260 641 -281 -641
		mu 0 4 232 231 252 253
		f 4 261 642 -282 -642
		mu 0 4 231 233 254 252
		f 4 262 643 -283 -643
		mu 0 4 233 234 255 254
		f 4 263 644 -284 -644
		mu 0 4 234 235 256 255
		f 4 264 645 -285 -645
		mu 0 4 235 236 257 256
		f 4 265 646 -286 -646
		mu 0 4 236 237 258 257
		f 4 266 647 -287 -647
		mu 0 4 237 238 259 258
		f 4 267 648 -288 -648
		mu 0 4 238 239 260 259
		f 4 268 649 -289 -649
		mu 0 4 239 240 261 260
		f 4 269 650 -290 -650
		mu 0 4 240 241 262 261
		f 4 270 651 -291 -651
		mu 0 4 241 242 263 262
		f 4 271 652 -292 -652
		mu 0 4 242 243 264 263
		f 4 272 653 -293 -653
		mu 0 4 243 244 265 264
		f 4 273 654 -294 -654
		mu 0 4 244 245 266 265
		f 4 274 655 -295 -655
		mu 0 4 245 246 267 266
		f 4 275 656 -296 -656
		mu 0 4 246 247 268 267
		f 4 276 657 -297 -657
		mu 0 4 247 248 269 268
		f 4 277 658 -298 -658
		mu 0 4 248 249 270 269
		f 4 278 659 -299 -659
		mu 0 4 249 250 271 270
		f 4 279 640 -300 -660
		mu 0 4 250 251 272 271
		f 4 280 661 -301 -661
		mu 0 4 253 252 273 274
		f 4 281 662 -302 -662
		mu 0 4 252 254 275 273
		f 4 282 663 -303 -663
		mu 0 4 254 255 276 275
		f 4 283 664 -304 -664
		mu 0 4 255 256 277 276
		f 4 284 665 -305 -665
		mu 0 4 256 257 278 277
		f 4 285 666 -306 -666
		mu 0 4 257 258 279 278
		f 4 286 667 -307 -667
		mu 0 4 258 259 280 279
		f 4 287 668 -308 -668
		mu 0 4 259 260 281 280
		f 4 288 669 -309 -669
		mu 0 4 260 261 282 281
		f 4 289 670 -310 -670
		mu 0 4 261 262 283 282
		f 4 290 671 -311 -671
		mu 0 4 262 263 284 283
		f 4 291 672 -312 -672
		mu 0 4 263 264 285 284
		f 4 292 673 -313 -673
		mu 0 4 264 265 286 285
		f 4 293 674 -314 -674
		mu 0 4 265 266 287 286
		f 4 294 675 -315 -675
		mu 0 4 266 267 288 287
		f 4 295 676 -316 -676
		mu 0 4 267 268 289 288
		f 4 296 677 -317 -677
		mu 0 4 268 269 290 289
		f 4 297 678 -318 -678
		mu 0 4 269 270 291 290
		f 4 298 679 -319 -679
		mu 0 4 270 271 292 291
		f 4 299 660 -320 -680
		mu 0 4 271 272 293 292
		f 4 300 681 -321 -681
		mu 0 4 274 273 294 295
		f 4 301 682 -322 -682
		mu 0 4 273 275 296 294
		f 4 302 683 -323 -683
		mu 0 4 275 276 297 296
		f 4 303 684 -324 -684
		mu 0 4 276 277 298 297
		f 4 304 685 -325 -685
		mu 0 4 277 278 299 298
		f 4 305 686 -326 -686
		mu 0 4 278 279 300 299
		f 4 306 687 -327 -687
		mu 0 4 279 280 301 300
		f 4 307 688 -328 -688
		mu 0 4 280 281 302 301
		f 4 308 689 -329 -689
		mu 0 4 281 282 303 302
		f 4 309 690 -330 -690
		mu 0 4 282 283 304 303
		f 4 310 691 -331 -691
		mu 0 4 283 284 305 304
		f 4 311 692 -332 -692
		mu 0 4 284 285 306 305
		f 4 312 693 -333 -693
		mu 0 4 285 286 307 306
		f 4 313 694 -334 -694
		mu 0 4 286 287 308 307
		f 4 314 695 -335 -695
		mu 0 4 287 288 309 308
		f 4 315 696 -336 -696
		mu 0 4 288 289 310 309
		f 4 316 697 -337 -697
		mu 0 4 289 290 311 310
		f 4 317 698 -338 -698
		mu 0 4 290 291 312 311
		f 4 318 699 -339 -699
		mu 0 4 291 292 313 312
		f 4 319 680 -340 -700
		mu 0 4 292 293 314 313
		f 4 320 701 -341 -701
		mu 0 4 295 294 315 316
		f 4 321 702 -342 -702
		mu 0 4 294 296 317 315
		f 4 322 703 -343 -703
		mu 0 4 296 297 318 317
		f 4 323 704 -344 -704
		mu 0 4 297 298 319 318
		f 4 324 705 -345 -705
		mu 0 4 298 299 320 319
		f 4 325 706 -346 -706
		mu 0 4 299 300 321 320
		f 4 326 707 -347 -707
		mu 0 4 300 301 322 321
		f 4 327 708 -348 -708
		mu 0 4 301 302 323 322
		f 4 328 709 -349 -709
		mu 0 4 302 303 324 323
		f 4 329 710 -350 -710
		mu 0 4 303 304 325 324
		f 4 330 711 -351 -711
		mu 0 4 304 305 326 325
		f 4 331 712 -352 -712
		mu 0 4 305 306 327 326
		f 4 332 713 -353 -713
		mu 0 4 306 307 328 327
		f 4 333 714 -354 -714
		mu 0 4 307 308 329 328
		f 4 334 715 -355 -715
		mu 0 4 308 309 330 329
		f 4 335 716 -356 -716
		mu 0 4 309 310 331 330
		f 4 336 717 -357 -717
		mu 0 4 310 311 332 331
		f 4 337 718 -358 -718
		mu 0 4 311 312 333 332
		f 4 338 719 -359 -719
		mu 0 4 312 313 334 333
		f 4 339 700 -360 -720
		mu 0 4 313 314 335 334
		f 4 340 721 -361 -721
		mu 0 4 316 315 336 337
		f 4 341 722 -362 -722
		mu 0 4 315 317 338 336
		f 4 342 723 -363 -723
		mu 0 4 317 318 339 338
		f 4 343 724 -364 -724
		mu 0 4 318 319 340 339
		f 4 344 725 -365 -725
		mu 0 4 319 320 341 340
		f 4 345 726 -366 -726
		mu 0 4 320 321 342 341
		f 4 346 727 -367 -727
		mu 0 4 321 322 343 342
		f 4 347 728 -368 -728
		mu 0 4 322 323 344 343
		f 4 348 729 -369 -729
		mu 0 4 323 324 345 344
		f 4 349 730 -370 -730
		mu 0 4 324 325 346 345
		f 4 350 731 -371 -731
		mu 0 4 325 326 347 346
		f 4 351 732 -372 -732
		mu 0 4 326 327 348 347
		f 4 352 733 -373 -733
		mu 0 4 327 328 349 348
		f 4 353 734 -374 -734
		mu 0 4 328 329 350 349
		f 4 354 735 -375 -735
		mu 0 4 329 330 351 350
		f 4 355 736 -376 -736
		mu 0 4 330 331 352 351
		f 4 356 737 -377 -737
		mu 0 4 331 332 353 352
		f 4 357 738 -378 -738
		mu 0 4 332 333 354 353
		f 4 358 739 -379 -739
		mu 0 4 333 334 355 354
		f 4 359 720 -380 -740
		mu 0 4 334 335 356 355
		f 4 360 741 -381 -741
		mu 0 4 337 336 357 358
		f 4 361 742 -382 -742
		mu 0 4 336 338 359 357
		f 4 362 743 -383 -743
		mu 0 4 338 339 360 359
		f 4 363 744 -384 -744
		mu 0 4 339 340 361 360
		f 4 364 745 -385 -745
		mu 0 4 340 341 362 361
		f 4 365 746 -386 -746
		mu 0 4 341 342 363 362
		f 4 366 747 -387 -747
		mu 0 4 342 343 364 363
		f 4 367 748 -388 -748
		mu 0 4 343 344 365 364
		f 4 368 749 -389 -749
		mu 0 4 344 345 366 365
		f 4 369 750 -390 -750
		mu 0 4 345 346 367 366
		f 4 370 751 -391 -751
		mu 0 4 346 347 368 367
		f 4 371 752 -392 -752
		mu 0 4 347 348 369 368
		f 4 372 753 -393 -753
		mu 0 4 348 349 370 369
		f 4 373 754 -394 -754
		mu 0 4 349 350 371 370
		f 4 374 755 -395 -755
		mu 0 4 350 351 372 371
		f 4 375 756 -396 -756
		mu 0 4 351 352 373 372
		f 4 376 757 -397 -757
		mu 0 4 352 353 374 373
		f 4 377 758 -398 -758
		mu 0 4 353 354 375 374
		f 4 378 759 -399 -759
		mu 0 4 354 355 376 375
		f 4 379 740 -400 -760
		mu 0 4 355 356 377 376
		f 4 380 761 -401 -761
		mu 0 4 358 357 378 379
		f 4 381 762 -402 -762
		mu 0 4 357 359 380 378
		f 4 382 763 -403 -763
		mu 0 4 359 360 381 380
		f 4 383 764 -404 -764
		mu 0 4 360 361 382 381
		f 4 384 765 -405 -765
		mu 0 4 361 362 383 382
		f 4 385 766 -406 -766
		mu 0 4 362 363 384 383
		f 4 386 767 -407 -767
		mu 0 4 363 364 385 384
		f 4 387 768 -408 -768
		mu 0 4 364 365 386 385
		f 4 388 769 -409 -769
		mu 0 4 365 366 387 386
		f 4 389 770 -410 -770
		mu 0 4 366 367 388 387
		f 4 390 771 -411 -771
		mu 0 4 367 368 389 388
		f 4 391 772 -412 -772
		mu 0 4 368 369 390 389
		f 4 392 773 -413 -773
		mu 0 4 369 370 391 390
		f 4 393 774 -414 -774
		mu 0 4 370 371 392 391
		f 4 394 775 -415 -775
		mu 0 4 371 372 393 392
		f 4 395 776 -416 -776
		mu 0 4 372 373 394 393
		f 4 396 777 -417 -777
		mu 0 4 373 374 395 394
		f 4 397 778 -418 -778
		mu 0 4 374 375 396 395
		f 4 398 779 -419 -779
		mu 0 4 375 376 397 396
		f 4 399 760 -420 -780
		mu 0 4 376 377 398 397
		f 4 400 781 -421 -781
		mu 0 4 379 378 399 400
		f 4 401 782 -422 -782
		mu 0 4 378 380 401 399
		f 4 402 783 -423 -783
		mu 0 4 380 381 402 401
		f 4 403 784 -424 -784
		mu 0 4 381 382 403 402
		f 4 404 785 -425 -785
		mu 0 4 382 383 404 403
		f 4 405 786 -426 -786
		mu 0 4 383 384 405 404
		f 4 406 787 -427 -787
		mu 0 4 384 385 406 405
		f 4 407 788 -428 -788
		mu 0 4 385 386 407 406
		f 4 408 789 -429 -789
		mu 0 4 386 387 408 407
		f 4 409 790 -430 -790
		mu 0 4 387 388 409 408
		f 4 410 791 -431 -791
		mu 0 4 388 389 410 409
		f 4 411 792 -432 -792
		mu 0 4 389 390 411 410
		f 4 412 793 -433 -793
		mu 0 4 390 391 412 411
		f 4 413 794 -434 -794
		mu 0 4 391 392 413 412
		f 4 414 795 -435 -795
		mu 0 4 392 393 414 413
		f 4 415 796 -436 -796
		mu 0 4 393 394 415 414
		f 4 416 797 -437 -797
		mu 0 4 394 395 416 415
		f 4 417 798 -438 -798
		mu 0 4 395 396 417 416
		f 4 418 799 -439 -799
		mu 0 4 396 397 418 417
		f 4 419 780 -440 -800
		mu 0 4 397 398 419 418
		f 4 420 801 -441 -801
		mu 0 4 400 399 420 421
		f 4 421 802 -442 -802
		mu 0 4 399 401 422 420
		f 4 422 803 -443 -803
		mu 0 4 401 402 423 422
		f 4 423 804 -444 -804
		mu 0 4 402 403 424 423
		f 4 424 805 -445 -805
		mu 0 4 403 404 425 424
		f 4 425 806 -446 -806
		mu 0 4 404 405 426 425
		f 4 426 807 -447 -807
		mu 0 4 405 406 427 426
		f 4 427 808 -448 -808
		mu 0 4 406 407 428 427
		f 4 428 809 -449 -809
		mu 0 4 407 408 429 428
		f 4 429 810 -450 -810
		mu 0 4 408 409 430 429
		f 4 430 811 -451 -811
		mu 0 4 409 410 431 430
		f 4 431 812 -452 -812
		mu 0 4 410 411 432 431
		f 4 432 813 -453 -813
		mu 0 4 411 412 433 432
		f 4 433 814 -454 -814
		mu 0 4 412 413 434 433
		f 4 434 815 -455 -815
		mu 0 4 413 414 435 434
		f 4 435 816 -456 -816
		mu 0 4 414 415 436 435
		f 4 436 817 -457 -817
		mu 0 4 415 416 437 436
		f 4 437 818 -458 -818
		mu 0 4 416 417 438 437
		f 4 438 819 -459 -819
		mu 0 4 417 418 439 438
		f 4 439 800 -460 -820
		mu 0 4 418 419 440 439
		f 4 440 821 -461 -821
		mu 0 4 421 420 441 442
		f 4 441 822 -462 -822
		mu 0 4 420 422 443 441
		f 4 442 823 -463 -823
		mu 0 4 422 423 444 443
		f 4 443 824 -464 -824
		mu 0 4 423 424 445 444
		f 4 444 825 -465 -825
		mu 0 4 424 425 446 445
		f 4 445 826 -466 -826
		mu 0 4 425 426 447 446
		f 4 446 827 -467 -827
		mu 0 4 426 427 448 447
		f 4 447 828 -468 -828
		mu 0 4 427 428 449 448
		f 4 448 829 -469 -829
		mu 0 4 428 429 450 449
		f 4 449 830 -470 -830
		mu 0 4 429 430 451 450
		f 4 450 831 -471 -831
		mu 0 4 430 431 452 451
		f 4 451 832 -472 -832
		mu 0 4 431 432 453 452
		f 4 452 833 -473 -833
		mu 0 4 432 433 454 453
		f 4 453 834 -474 -834
		mu 0 4 433 434 455 454
		f 4 454 835 -475 -835
		mu 0 4 434 435 456 455
		f 4 455 836 -476 -836
		mu 0 4 435 436 457 456
		f 4 456 837 -477 -837
		mu 0 4 436 437 458 457
		f 4 457 838 -478 -838
		mu 0 4 437 438 459 458
		f 4 458 839 -479 -839
		mu 0 4 438 439 460 459
		f 4 459 820 -480 -840
		mu 0 4 439 440 461 460
		f 4 460 841 -481 -841
		mu 0 4 442 441 462 463
		f 4 461 842 -482 -842
		mu 0 4 441 443 464 462
		f 4 462 843 -483 -843
		mu 0 4 443 444 465 464
		f 4 463 844 -484 -844
		mu 0 4 444 445 466 465
		f 4 464 845 -485 -845
		mu 0 4 445 446 467 466
		f 4 465 846 -486 -846
		mu 0 4 446 447 468 467
		f 4 466 847 -487 -847
		mu 0 4 447 448 469 468
		f 4 467 848 -488 -848
		mu 0 4 448 449 470 469
		f 4 468 849 -489 -849
		mu 0 4 449 450 471 470
		f 4 469 850 -490 -850
		mu 0 4 450 451 472 471
		f 4 470 851 -491 -851
		mu 0 4 451 452 473 472
		f 4 471 852 -492 -852
		mu 0 4 452 453 474 473
		f 4 472 853 -493 -853
		mu 0 4 453 454 475 474
		f 4 473 854 -494 -854
		mu 0 4 454 455 476 475
		f 4 474 855 -495 -855
		mu 0 4 455 456 477 476
		f 4 475 856 -496 -856
		mu 0 4 456 457 478 477
		f 4 476 857 -497 -857
		mu 0 4 457 458 479 478
		f 4 477 858 -498 -858
		mu 0 4 458 459 480 479
		f 4 478 859 -499 -859
		mu 0 4 459 460 481 480
		f 4 479 840 -500 -860
		mu 0 4 460 461 482 481
		f 4 480 861 -501 -861
		mu 0 4 463 462 483 484
		f 4 481 862 -502 -862
		mu 0 4 462 464 485 483
		f 4 482 863 -503 -863
		mu 0 4 464 465 486 485
		f 4 483 864 -504 -864
		mu 0 4 465 466 487 486
		f 4 484 865 -505 -865
		mu 0 4 466 467 488 487
		f 4 485 866 -506 -866
		mu 0 4 467 468 489 488
		f 4 486 867 -507 -867
		mu 0 4 468 469 490 489
		f 4 487 868 -508 -868
		mu 0 4 469 470 491 490
		f 4 488 869 -509 -869
		mu 0 4 470 471 492 491
		f 4 489 870 -510 -870
		mu 0 4 471 472 493 492
		f 4 490 871 -511 -871
		mu 0 4 472 473 494 493
		f 4 491 872 -512 -872
		mu 0 4 473 474 495 494
		f 4 492 873 -513 -873
		mu 0 4 474 475 496 495
		f 4 493 874 -514 -874
		mu 0 4 475 476 497 496
		f 4 494 875 -515 -875
		mu 0 4 476 477 498 497
		f 4 495 876 -516 -876
		mu 0 4 477 478 499 498
		f 4 496 877 -517 -877
		mu 0 4 478 479 500 499
		f 4 497 878 -518 -878
		mu 0 4 479 480 501 500
		f 4 498 879 -519 -879
		mu 0 4 480 481 502 501
		f 4 499 860 -520 -880
		mu 0 4 481 482 503 502
		f 4 500 881 -521 -881
		mu 0 4 484 483 504 505
		f 4 501 882 -522 -882
		mu 0 4 483 485 506 504
		f 4 502 883 -523 -883
		mu 0 4 485 486 507 506
		f 4 503 884 -524 -884
		mu 0 4 486 487 508 507
		f 4 504 885 -525 -885
		mu 0 4 487 488 509 508
		f 4 505 886 -526 -886
		mu 0 4 488 489 510 509
		f 4 506 887 -527 -887
		mu 0 4 489 490 511 510
		f 4 507 888 -528 -888
		mu 0 4 490 491 512 511
		f 4 508 889 -529 -889
		mu 0 4 491 492 513 512
		f 4 509 890 -530 -890
		mu 0 4 492 493 514 513
		f 4 510 891 -531 -891
		mu 0 4 493 494 515 514
		f 4 511 892 -532 -892
		mu 0 4 494 495 516 515
		f 4 512 893 -533 -893
		mu 0 4 495 496 517 516
		f 4 513 894 -534 -894
		mu 0 4 496 497 518 517
		f 4 514 895 -535 -895
		mu 0 4 497 498 519 518
		f 4 515 896 -536 -896
		mu 0 4 498 499 520 519
		f 4 516 897 -537 -897
		mu 0 4 499 500 521 520
		f 4 517 898 -538 -898
		mu 0 4 500 501 522 521
		f 4 518 899 -539 -899
		mu 0 4 501 502 523 522
		f 4 519 880 -540 -900
		mu 0 4 502 503 524 523
		f 4 520 901 -541 -901
		mu 0 4 505 504 525 526
		f 4 521 902 -542 -902
		mu 0 4 504 506 527 525
		f 4 522 903 -543 -903
		mu 0 4 506 507 528 527
		f 4 523 904 -544 -904
		mu 0 4 507 508 529 528
		f 4 524 905 -545 -905
		mu 0 4 508 509 530 529
		f 4 525 906 -546 -906
		mu 0 4 509 510 531 530
		f 4 526 907 -547 -907
		mu 0 4 510 511 532 531
		f 4 527 908 -548 -908
		mu 0 4 511 512 533 532
		f 4 528 909 -549 -909
		mu 0 4 512 513 534 533
		f 4 529 910 -550 -910
		mu 0 4 513 514 535 534
		f 4 530 911 -551 -911
		mu 0 4 514 515 536 535
		f 4 531 912 -552 -912
		mu 0 4 515 516 537 536
		f 4 532 913 -553 -913
		mu 0 4 516 517 538 537
		f 4 533 914 -554 -914
		mu 0 4 517 518 539 538
		f 4 534 915 -555 -915
		mu 0 4 518 519 540 539
		f 4 535 916 -556 -916
		mu 0 4 519 520 541 540
		f 4 536 917 -557 -917
		mu 0 4 520 521 542 541
		f 4 537 918 -558 -918
		mu 0 4 521 522 543 542
		f 4 538 919 -559 -919
		mu 0 4 522 523 544 543
		f 4 539 900 -560 -920
		mu 0 4 523 524 545 544
		f 4 540 921 -561 -921
		mu 0 4 526 525 546 547
		f 4 541 922 -562 -922
		mu 0 4 525 527 548 546
		f 4 542 923 -563 -923
		mu 0 4 527 528 549 548
		f 4 543 924 -564 -924
		mu 0 4 528 529 550 549
		f 4 544 925 -565 -925
		mu 0 4 529 530 551 550
		f 4 545 926 -566 -926
		mu 0 4 530 531 552 551
		f 4 546 927 -567 -927
		mu 0 4 531 532 553 552
		f 4 547 928 -568 -928
		mu 0 4 532 533 554 553
		f 4 548 929 -569 -929
		mu 0 4 533 534 555 554
		f 4 549 930 -570 -930
		mu 0 4 534 535 556 555
		f 4 550 931 -571 -931
		mu 0 4 535 536 557 556
		f 4 551 932 -572 -932
		mu 0 4 536 537 558 557
		f 4 552 933 -573 -933
		mu 0 4 537 538 559 558
		f 4 553 934 -574 -934
		mu 0 4 538 539 560 559
		f 4 554 935 -575 -935
		mu 0 4 539 540 561 560
		f 4 555 936 -576 -936
		mu 0 4 540 541 562 561
		f 4 556 937 -577 -937
		mu 0 4 541 542 563 562
		f 4 557 938 -578 -938
		mu 0 4 542 543 564 563
		f 4 558 939 -579 -939
		mu 0 4 543 544 565 564
		f 4 559 920 -580 -940
		mu 0 4 544 545 566 565
		f 3 -201 -941 941
		mu 0 3 169 168 567
		f 3 -202 -942 942
		mu 0 3 172 169 568
		f 3 -203 -943 943
		mu 0 3 174 172 569
		f 3 -204 -944 944
		mu 0 3 176 174 570
		f 3 -205 -945 945
		mu 0 3 178 176 571
		f 3 -206 -946 946
		mu 0 3 180 178 572
		f 3 -207 -947 947
		mu 0 3 182 180 573
		f 3 -208 -948 948
		mu 0 3 184 182 574
		f 3 -209 -949 949
		mu 0 3 186 184 575
		f 3 -210 -950 950
		mu 0 3 188 186 576
		f 3 -211 -951 951
		mu 0 3 190 188 577
		f 3 -212 -952 952
		mu 0 3 192 190 578
		f 3 -213 -953 953
		mu 0 3 194 192 579
		f 3 -214 -954 954
		mu 0 3 196 194 580
		f 3 -215 -955 955
		mu 0 3 198 196 581
		f 3 -216 -956 956
		mu 0 3 200 198 582
		f 3 -217 -957 957
		mu 0 3 202 200 583
		f 3 -218 -958 958
		mu 0 3 204 202 584
		f 3 -219 -959 959
		mu 0 3 206 204 585
		f 3 -220 -960 940
		mu 0 3 208 206 586;
	setAttr ".fc[500:519]"
		f 3 560 961 -961
		mu 0 3 547 546 587
		f 3 561 962 -962
		mu 0 3 546 548 588
		f 3 562 963 -963
		mu 0 3 548 549 589
		f 3 563 964 -964
		mu 0 3 549 550 590
		f 3 564 965 -965
		mu 0 3 550 551 591
		f 3 565 966 -966
		mu 0 3 551 552 592
		f 3 566 967 -967
		mu 0 3 552 553 593
		f 3 567 968 -968
		mu 0 3 553 554 594
		f 3 568 969 -969
		mu 0 3 554 555 595
		f 3 569 970 -970
		mu 0 3 555 556 596
		f 3 570 971 -971
		mu 0 3 556 557 597
		f 3 571 972 -972
		mu 0 3 557 558 598
		f 3 572 973 -973
		mu 0 3 558 559 599
		f 3 573 974 -974
		mu 0 3 559 560 600
		f 3 574 975 -975
		mu 0 3 560 561 601
		f 3 575 976 -976
		mu 0 3 561 562 602
		f 3 576 977 -977
		mu 0 3 562 563 603
		f 3 577 978 -978
		mu 0 3 563 564 604
		f 3 578 979 -979
		mu 0 3 564 565 605
		f 3 579 960 -980
		mu 0 3 565 566 606;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder9";
	rename -uid "E6F827FE-41E8-31EB-7552-FFBA46C09EB7";
	setAttr ".t" -type "double3" 14.31961877456348 -5.7649273808934325 -17.737909167668079 ;
	setAttr ".r" -type "double3" 75.183005149672042 41.735265437142459 230.99667512625459 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder9Shape" -p "pCylinder9";
	rename -uid "2A0F7100-44A7-7C85-8E65-19A0FFFBE628";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:519]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 607 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.64860266 0.10796607 0.62640899
		 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607
		 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997
		 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161
		 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146
		 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998 0.3125
		 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.375
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.38749999 0.3125 0.39999998 0.68843985
		 0.39999998 0.3125 0.41249996 0.68843985 0.41249996 0.3125 0.42499995 0.68843985 0.42499995
		 0.3125 0.43749994 0.68843985 0.43749994 0.3125 0.44999993 0.68843985 0.44999993 0.3125
		 0.46249992 0.68843985 0.46249992 0.3125 0.4749999 0.68843985 0.4749999 0.3125 0.48749989
		 0.68843985 0.48749989 0.3125 0.49999988 0.68843985 0.49999988 0.3125 0.51249987 0.68843985
		 0.51249987 0.3125 0.52499986 0.68843985 0.52499986 0.3125 0.53749985 0.68843985 0.53749985
		 0.3125 0.54999983 0.68843985 0.54999983 0.3125 0.56249982 0.68843985 0.56249982 0.3125
		 0.57499981 0.68843985 0.57499981 0.3125 0.5874998 0.68843985 0.5874998 0.3125 0.59999979
		 0.68843985 0.59999979 0.3125 0.61249977 0.68843985 0.61249977 0.3125 0.62499976 0.68843985
		 0.62499976 0.3125 0.62640899 0.064408496 0.5 0.15000001 0.64860266 0.10796607 0.59184152
		 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607 0.0076473504 0.40815851
		 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997 0.15625 0.3513974
		 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161 0.3048526 0.5 0.3125
		 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146 0.6486026 0.2045339
		 0.65625 0.15625 0.6486026 0.89203393 0.5 0.83749998 0.62640893 0.93559146 0.59184146
		 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893 0.37359107
		 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607 0.37359107
		 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994 0.54828393
		 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607 0.65625
		 0.84375 0 0.050000001 0.050000001 0.050000001 0.050000001 0.1 0 0.1 0.1 0.050000001
		 0.1 0.1 0.15000001 0.050000001 0.15000001 0.1 0.2 0.050000001 0.2 0.1 0.25 0.050000001
		 0.25 0.1 0.30000001 0.050000001 0.30000001 0.1 0.35000002 0.050000001 0.35000002
		 0.1 0.40000004 0.050000001 0.40000004 0.1 0.45000005 0.050000001 0.45000005 0.1 0.50000006
		 0.050000001 0.50000006 0.1 0.55000007 0.050000001 0.55000007 0.1 0.60000008 0.050000001
		 0.60000008 0.1 0.6500001 0.050000001 0.6500001 0.1 0.70000011 0.050000001 0.70000011
		 0.1 0.75000012 0.050000001 0.75000012 0.1 0.80000013 0.050000001 0.80000013 0.1 0.85000014
		 0.050000001 0.85000014 0.1 0.90000015 0.050000001 0.90000015 0.1 0.95000017 0.050000001
		 0.95000017 0.1 1.000000119209 0.050000001 1.000000119209 0.1 0.050000001 0.15000001
		 0 0.15000001 0.1 0.15000001 0.15000001 0.15000001 0.2 0.15000001 0.25 0.15000001
		 0.30000001 0.15000001 0.35000002 0.15000001 0.40000004 0.15000001 0.45000005 0.15000001
		 0.50000006 0.15000001 0.55000007 0.15000001 0.60000008 0.15000001 0.6500001 0.15000001
		 0.70000011 0.15000001 0.75000012 0.15000001 0.80000013 0.15000001 0.85000014 0.15000001
		 0.90000015 0.15000001 0.95000017 0.15000001 1.000000119209 0.15000001 0.050000001
		 0.2 0 0.2 0.1 0.2 0.15000001 0.2 0.2 0.2 0.25 0.2 0.30000001 0.2 0.35000002 0.2 0.40000004
		 0.2 0.45000005 0.2 0.50000006 0.2 0.55000007 0.2 0.60000008 0.2 0.6500001 0.2 0.70000011
		 0.2 0.75000012 0.2 0.80000013 0.2 0.85000014 0.2 0.90000015 0.2;
	setAttr ".uvst[0].uvsp[250:499]" 0.95000017 0.2 1.000000119209 0.2 0.050000001
		 0.25 0 0.25 0.1 0.25 0.15000001 0.25 0.2 0.25 0.25 0.25 0.30000001 0.25 0.35000002
		 0.25 0.40000004 0.25 0.45000005 0.25 0.50000006 0.25 0.55000007 0.25 0.60000008 0.25
		 0.6500001 0.25 0.70000011 0.25 0.75000012 0.25 0.80000013 0.25 0.85000014 0.25 0.90000015
		 0.25 0.95000017 0.25 1.000000119209 0.25 0.050000001 0.30000001 0 0.30000001 0.1
		 0.30000001 0.15000001 0.30000001 0.2 0.30000001 0.25 0.30000001 0.30000001 0.30000001
		 0.35000002 0.30000001 0.40000004 0.30000001 0.45000005 0.30000001 0.50000006 0.30000001
		 0.55000007 0.30000001 0.60000008 0.30000001 0.6500001 0.30000001 0.70000011 0.30000001
		 0.75000012 0.30000001 0.80000013 0.30000001 0.85000014 0.30000001 0.90000015 0.30000001
		 0.95000017 0.30000001 1.000000119209 0.30000001 0.050000001 0.35000002 0 0.35000002
		 0.1 0.35000002 0.15000001 0.35000002 0.2 0.35000002 0.25 0.35000002 0.30000001 0.35000002
		 0.35000002 0.35000002 0.40000004 0.35000002 0.45000005 0.35000002 0.50000006 0.35000002
		 0.55000007 0.35000002 0.60000008 0.35000002 0.6500001 0.35000002 0.70000011 0.35000002
		 0.75000012 0.35000002 0.80000013 0.35000002 0.85000014 0.35000002 0.90000015 0.35000002
		 0.95000017 0.35000002 1.000000119209 0.35000002 0.050000001 0.40000004 0 0.40000004
		 0.1 0.40000004 0.15000001 0.40000004 0.2 0.40000004 0.25 0.40000004 0.30000001 0.40000004
		 0.35000002 0.40000004 0.40000004 0.40000004 0.45000005 0.40000004 0.50000006 0.40000004
		 0.55000007 0.40000004 0.60000008 0.40000004 0.6500001 0.40000004 0.70000011 0.40000004
		 0.75000012 0.40000004 0.80000013 0.40000004 0.85000014 0.40000004 0.90000015 0.40000004
		 0.95000017 0.40000004 1.000000119209 0.40000004 0.050000001 0.45000005 0 0.45000005
		 0.1 0.45000005 0.15000001 0.45000005 0.2 0.45000005 0.25 0.45000005 0.30000001 0.45000005
		 0.35000002 0.45000005 0.40000004 0.45000005 0.45000005 0.45000005 0.50000006 0.45000005
		 0.55000007 0.45000005 0.60000008 0.45000005 0.6500001 0.45000005 0.70000011 0.45000005
		 0.75000012 0.45000005 0.80000013 0.45000005 0.85000014 0.45000005 0.90000015 0.45000005
		 0.95000017 0.45000005 1.000000119209 0.45000005 0.050000001 0.50000006 0 0.50000006
		 0.1 0.50000006 0.15000001 0.50000006 0.2 0.50000006 0.25 0.50000006 0.30000001 0.50000006
		 0.35000002 0.50000006 0.40000004 0.50000006 0.45000005 0.50000006 0.50000006 0.50000006
		 0.55000007 0.50000006 0.60000008 0.50000006 0.6500001 0.50000006 0.70000011 0.50000006
		 0.75000012 0.50000006 0.80000013 0.50000006 0.85000014 0.50000006 0.90000015 0.50000006
		 0.95000017 0.50000006 1.000000119209 0.50000006 0.050000001 0.55000007 0 0.55000007
		 0.1 0.55000007 0.15000001 0.55000007 0.2 0.55000007 0.25 0.55000007 0.30000001 0.55000007
		 0.35000002 0.55000007 0.40000004 0.55000007 0.45000005 0.55000007 0.50000006 0.55000007
		 0.55000007 0.55000007 0.60000008 0.55000007 0.6500001 0.55000007 0.70000011 0.55000007
		 0.75000012 0.55000007 0.80000013 0.55000007 0.85000014 0.55000007 0.90000015 0.55000007
		 0.95000017 0.55000007 1.000000119209 0.55000007 0.050000001 0.60000008 0 0.60000008
		 0.1 0.60000008 0.15000001 0.60000008 0.2 0.60000008 0.25 0.60000008 0.30000001 0.60000008
		 0.35000002 0.60000008 0.40000004 0.60000008 0.45000005 0.60000008 0.50000006 0.60000008
		 0.55000007 0.60000008 0.60000008 0.60000008 0.6500001 0.60000008 0.70000011 0.60000008
		 0.75000012 0.60000008 0.80000013 0.60000008 0.85000014 0.60000008 0.90000015 0.60000008
		 0.95000017 0.60000008 1.000000119209 0.60000008 0.050000001 0.6500001 0 0.6500001
		 0.1 0.6500001 0.15000001 0.6500001 0.2 0.6500001 0.25 0.6500001 0.30000001 0.6500001
		 0.35000002 0.6500001 0.40000004 0.6500001 0.45000005 0.6500001 0.50000006 0.6500001
		 0.55000007 0.6500001 0.60000008 0.6500001 0.6500001 0.6500001 0.70000011 0.6500001
		 0.75000012 0.6500001 0.80000013 0.6500001 0.85000014 0.6500001 0.90000015 0.6500001
		 0.95000017 0.6500001 1.000000119209 0.6500001 0.050000001 0.70000011 0 0.70000011
		 0.1 0.70000011 0.15000001 0.70000011 0.2 0.70000011 0.25 0.70000011 0.30000001 0.70000011
		 0.35000002 0.70000011 0.40000004 0.70000011 0.45000005 0.70000011 0.50000006 0.70000011
		 0.55000007 0.70000011 0.60000008 0.70000011 0.6500001 0.70000011 0.70000011 0.70000011
		 0.75000012 0.70000011 0.80000013 0.70000011 0.85000014 0.70000011 0.90000015 0.70000011
		 0.95000017 0.70000011 1.000000119209 0.70000011 0.050000001 0.75000012 0 0.75000012
		 0.1 0.75000012 0.15000001 0.75000012 0.2 0.75000012 0.25 0.75000012 0.30000001 0.75000012
		 0.35000002 0.75000012 0.40000004 0.75000012 0.45000005 0.75000012 0.50000006 0.75000012
		 0.55000007 0.75000012 0.60000008 0.75000012 0.6500001 0.75000012 0.70000011 0.75000012
		 0.75000012 0.75000012 0.80000013 0.75000012 0.85000014 0.75000012 0.90000015 0.75000012
		 0.95000017 0.75000012 1.000000119209 0.75000012 0.050000001 0.80000013 0 0.80000013
		 0.1 0.80000013 0.15000001 0.80000013 0.2 0.80000013 0.25 0.80000013 0.30000001 0.80000013
		 0.35000002 0.80000013 0.40000004 0.80000013 0.45000005 0.80000013 0.50000006 0.80000013
		 0.55000007 0.80000013 0.60000008 0.80000013 0.6500001 0.80000013 0.70000011 0.80000013
		 0.75000012 0.80000013 0.80000013 0.80000013;
	setAttr ".uvst[0].uvsp[500:606]" 0.85000014 0.80000013 0.90000015 0.80000013
		 0.95000017 0.80000013 1.000000119209 0.80000013 0.050000001 0.85000014 0 0.85000014
		 0.1 0.85000014 0.15000001 0.85000014 0.2 0.85000014 0.25 0.85000014 0.30000001 0.85000014
		 0.35000002 0.85000014 0.40000004 0.85000014 0.45000005 0.85000014 0.50000006 0.85000014
		 0.55000007 0.85000014 0.60000008 0.85000014 0.6500001 0.85000014 0.70000011 0.85000014
		 0.75000012 0.85000014 0.80000013 0.85000014 0.85000014 0.85000014 0.90000015 0.85000014
		 0.95000017 0.85000014 1.000000119209 0.85000014 0.050000001 0.90000015 0 0.90000015
		 0.1 0.90000015 0.15000001 0.90000015 0.2 0.90000015 0.25 0.90000015 0.30000001 0.90000015
		 0.35000002 0.90000015 0.40000004 0.90000015 0.45000005 0.90000015 0.50000006 0.90000015
		 0.55000007 0.90000015 0.60000008 0.90000015 0.6500001 0.90000015 0.70000011 0.90000015
		 0.75000012 0.90000015 0.80000013 0.90000015 0.85000014 0.90000015 0.90000015 0.90000015
		 0.95000017 0.90000015 1.000000119209 0.90000015 0.050000001 0.95000017 0 0.95000017
		 0.1 0.95000017 0.15000001 0.95000017 0.2 0.95000017 0.25 0.95000017 0.30000001 0.95000017
		 0.35000002 0.95000017 0.40000004 0.95000017 0.45000005 0.95000017 0.50000006 0.95000017
		 0.55000007 0.95000017 0.60000008 0.95000017 0.6500001 0.95000017 0.70000011 0.95000017
		 0.75000012 0.95000017 0.80000013 0.95000017 0.85000014 0.95000017 0.90000015 0.95000017
		 0.95000017 0.95000017 1.000000119209 0.95000017 0.025 0 0.075000003 0 0.125 0 0.175
		 0 0.22500001 0 0.27500001 0 0.32500002 0 0.375 0 0.42500001 0 0.47499999 0 0.52500004
		 0 0.57499999 0 0.625 0 0.67500001 0 0.72500002 0 0.77500004 0 0.82499999 0 0.875
		 0 0.92500001 0 0.97500002 0 0.025 1 0.075000003 1 0.125 1 0.175 1 0.22500001 1 0.27500001
		 1 0.32500002 1 0.375 1 0.42500001 1 0.47499999 1 0.52500004 1 0.57499999 1 0.625
		 1 0.67500001 1 0.72500002 1 0.77500004 1 0.82499999 1 0.875 1 0.92500001 1 0.97500002
		 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 466 ".vt";
	setAttr ".vt[0:165]"  -31.42638397 0.15162086 89.97360229 -31.61052513 0.26360402 89.74130249
		 -31.94010735 0.35247436 89.46312714 -32.38286972 0.40953261 89.16630554 -32.89547348 0.4291935 88.87989807
		 -33.42773819 0.40953258 88.63193512 -33.92756271 0.3524743 88.44669342 -34.34602356 0.26360396 88.34230042
		 -34.64215469 0.15162082 88.32897949 -34.78697205 0.0274866 88.40803528 -34.7663002 -0.09664762 88.57172394
		 -34.58216095 -0.20863073 88.80403137 -34.25257874 -0.29750097 89.082206726 -33.80981445 -0.35455921 89.37902069
		 -33.29721451 -0.37422013 89.66543579 -32.7649498 -0.35455918 89.91339111 -32.26512146 -0.29750094 90.098632813
		 -31.84666252 -0.2086307 90.20302582 -31.55052948 -0.096647598 90.21634674 -31.40571213 0.0274866 90.13729095
		 -31.40359116 0.096647523 89.929039 -31.58773232 0.20863067 89.69673157 -31.91731453 0.297501 89.41855621
		 -32.3600769 0.35455924 89.12174225 -32.87267685 0.37422013 88.83532715 -33.40494156 0.35455921 88.58737183
		 -33.9047699 0.29750094 88.4021225 -34.32322693 0.20863059 88.29773712 -34.61936188 0.096647471 88.2844162
		 -34.76417923 -0.027486745 88.36347198 -34.74350739 -0.15162097 88.52716064 -34.55936432 -0.26360404 88.75946045
		 -34.2297821 -0.35247433 89.037635803 -33.78702164 -0.40953258 89.3344574 -33.27441788 -0.4291935 89.62086487
		 -32.74215698 -0.40953255 89.86882782 -32.24232864 -0.3524743 90.054069519 -31.82386971 -0.26360402 90.15846252
		 -31.52773666 -0.15162094 90.17178345 -31.38291931 -0.027486745 90.092727661 -33.096343994 0.0274866 89.27266693
		 -33.073547363 -0.027486745 89.22809601 -38.24187469 2.49435377 89.47029114 -38.0036354065 2.42841434 89.27202606
		 -37.675354 2.24016404 89.046867371 -37.28915024 1.94802976 88.81686401 -36.88284302 1.58060765 88.60451508
		 -36.49619675 1.17386389 88.43061066 -36.16706085 0.76761329 88.31217194 -35.92765427 0.40162241 88.26081085
		 -35.80141068 0.11171699 88.28153229 -35.80068588 -0.073725104 88.37231445 -35.92555618 -0.1365515 88.5242691
		 -36.16379166 -0.070612133 88.72253418 -36.49207687 0.11763823 88.94768524 -36.87827682 0.40977228 89.17769623
		 -37.28458405 0.77719414 89.39004517 -37.67123032 1.18393755 89.56394958 -38.00036621094 1.59018826 89.68238068
		 -38.2397728 1.95617914 89.73374939 -38.36601639 2.24608445 89.71302795 -38.36674118 2.43152666 89.62224579
		 -38.19311142 2.51918554 89.52061462 -37.95487595 2.45324612 89.32236481 -37.62659073 2.26499557 89.097198486
		 -37.24039078 1.97286129 88.86719513 -36.83408356 1.60543942 88.65484619 -36.44743729 1.19869566 88.48094177
		 -36.11829758 0.792445 88.36251068 -35.87889481 0.42645407 88.31114197 -35.75265121 0.13654858 88.33187103
		 -35.75192642 -0.048893452 88.42264557 -35.87679672 -0.11171985 88.57460022 -36.1150322 -0.04578054 88.7728653
		 -36.4433136 0.14246988 88.99801636 -36.82951355 0.43460399 89.22803497 -37.23582458 0.80202574 89.44038391
		 -37.62246704 1.20876932 89.6142807 -37.95160675 1.6150198 89.73271179 -38.19100952 1.98101068 89.78408051
		 -38.31725311 2.27091622 89.76335907 -38.31797791 2.45635819 89.67258453 -37.083713531 1.17890096 88.99727631
		 -37.034954071 1.20373249 89.047615051 -34.97560501 -0.98768836 88.25870514 -34.99782562 -0.98768836 88.21509552
		 -35.032432556 -0.98768836 88.18048859 -35.076042175 -0.98768836 88.15826416 -35.12438202 -0.98768836 88.15061188
		 -35.17272186 -0.98768836 88.15826416 -35.21633148 -0.98768836 88.18048859 -35.25093842 -0.98768836 88.21509552
		 -35.27315903 -0.98768836 88.25870514 -35.28081512 -0.98768836 88.30704498 -35.27315903 -0.98768836 88.35538483
		 -35.25093842 -0.98768836 88.39899445 -35.21633148 -0.98768836 88.43360138 -35.17272186 -0.98768836 88.45582581
		 -35.12438202 -0.98768836 88.46347809 -35.076042175 -0.98768836 88.45582581 -35.032432556 -0.98768836 88.43360138
		 -34.99782562 -0.98768836 88.39899445 -34.97560501 -0.98768836 88.35538483 -34.96794891 -0.98768836 88.30704498
		 -34.83049011 -0.95105654 88.21155548 -34.87438202 -0.95105654 88.12541199 -34.94274521 -0.95105654 88.057044983
		 -35.028888702 -0.95105654 88.013153076 -35.12438202 -0.95105654 87.99803162 -35.21987534 -0.95105654 88.013153076
		 -35.30601883 -0.95105654 88.057044983 -35.37438202 -0.95105654 88.12541199 -35.41827393 -0.95105654 88.21155548
		 -35.4333992 -0.95105654 88.30704498 -35.41827393 -0.95105654 88.40253448 -35.37438202 -0.95105654 88.48867798
		 -35.30601883 -0.95105654 88.55704498 -35.21987534 -0.95105654 88.60093689 -35.12438202 -0.95105654 88.61605835
		 -35.028888702 -0.95105654 88.60093689 -34.94274521 -0.95105654 88.55704498 -34.87438202 -0.95105654 88.48867798
		 -34.83049011 -0.95105654 88.40253448 -34.81536484 -0.95105654 88.30704498 -34.69261169 -0.89100653 88.16675568
		 -34.75709534 -0.89100653 88.04019928 -34.8575325 -0.89100653 87.9397583 -34.98409271 -0.89100653 87.87527466
		 -35.12438202 -0.89100653 87.85305786 -35.26467133 -0.89100653 87.87527466 -35.39123154 -0.89100653 87.9397583
		 -35.4916687 -0.89100653 88.04019928 -35.55615234 -0.89100653 88.16675568 -35.57837296 -0.89100653 88.30704498
		 -35.55615234 -0.89100653 88.44733429 -35.4916687 -0.89100653 88.57389069 -35.39123154 -0.89100653 88.67433167
		 -35.26467133 -0.89100653 88.73881531 -35.12438202 -0.89100653 88.7610321 -34.98409271 -0.89100653 88.73881531
		 -34.8575325 -0.89100653 88.67433167 -34.75709534 -0.89100653 88.57389069 -34.69261169 -0.89100653 88.44733429
		 -34.67039108 -0.89100653 88.30704498 -34.56536484 -0.809017 88.12541199 -34.6488533 -0.809017 87.96155548
		 -34.7788887 -0.809017 87.83152008 -34.94274521 -0.809017 87.74802399 -35.12438202 -0.809017 87.71926117
		 -35.30601883 -0.809017 87.74802399 -35.46987534 -0.809017 87.83152008 -35.59991074 -0.809017 87.96155548
		 -35.6833992 -0.809017 88.12541199 -35.71216583 -0.809017 88.30704498 -35.6833992 -0.809017 88.48867798
		 -35.59991074 -0.809017 88.65253448 -35.46987534 -0.809017 88.78256989 -35.30601883 -0.809017 88.86605835
		 -35.12438202 -0.809017 88.8948288 -34.94274521 -0.809017 88.86605835 -34.7788887 -0.809017 88.78256989
		 -34.6488533 -0.809017 88.65253448 -34.56536484 -0.809017 88.48867798 -34.53659821 -0.809017 88.30704498
		 -34.45188141 -0.70710677 88.088539124 -34.55231857 -0.70710677 87.89141846;
	setAttr ".vt[166:331]" -34.70875549 -0.70710677 87.73498535 -34.90587234 -0.70710677 87.63454437
		 -35.12438202 -0.70710677 87.59993744 -35.34289169 -0.70710677 87.63454437 -35.54000854 -0.70710677 87.73498535
		 -35.69644165 -0.70710677 87.89141846 -35.79688263 -0.70710677 88.088539124 -35.83148956 -0.70710677 88.30704498
		 -35.79688263 -0.70710677 88.52555084 -35.69644165 -0.70710677 88.72267151 -35.54000854 -0.70710677 88.87910461
		 -35.34289169 -0.70710677 88.97954559 -35.12438202 -0.70710677 89.014152527 -34.90587234 -0.70710677 88.97954559
		 -34.70875549 -0.70710677 88.87910461 -34.55232239 -0.70710677 88.72267151 -34.45188522 -0.70710677 88.52555084
		 -34.41727448 -0.70710677 88.30704498 -34.3549614 -0.58778524 88.057044983 -34.46987152 -0.58778524 87.83152008
		 -34.6488533 -0.58778524 87.65253448 -34.87438202 -0.58778524 87.53762054 -35.12438202 -0.58778524 87.49802399
		 -35.37438202 -0.58778524 87.53762054 -35.59991074 -0.58778524 87.65253448 -35.77889252 -0.58778524 87.83152008
		 -35.89380264 -0.58778524 88.057044983 -35.9333992 -0.58778524 88.30704498 -35.89380264 -0.58778524 88.55704498
		 -35.77889252 -0.58778524 88.78256989 -35.59991074 -0.58778524 88.96155548 -35.37438202 -0.58778524 89.076469421
		 -35.12438202 -0.58778524 89.11605835 -34.87438202 -0.58778524 89.076469421 -34.6488533 -0.58778524 88.96155548
		 -34.46987534 -0.58778524 88.78256989 -34.3549614 -0.58778524 88.55704498 -34.31536484 -0.58778524 88.30704498
		 -34.27698517 -0.45399052 88.031707764 -34.40354156 -0.45399052 87.7833252 -34.60066223 -0.45399052 87.58620453
		 -34.8490448 -0.45399052 87.45964813 -35.12438202 -0.45399052 87.41603851 -35.39971924 -0.45399052 87.45964813
		 -35.64810181 -0.45399052 87.58620453 -35.84522247 -0.45399052 87.7833252 -35.97177887 -0.45399052 88.031707764
		 -36.015388489 -0.45399052 88.30704498 -35.97177887 -0.45399052 88.5823822 -35.84522247 -0.45399052 88.83076477
		 -35.64810181 -0.45399052 89.027885437 -35.39971924 -0.45399052 89.15444183 -35.12438202 -0.45399052 89.19805145
		 -34.8490448 -0.45399052 89.15444183 -34.60066223 -0.45399052 89.027885437 -34.40354156 -0.45399052 88.83076477
		 -34.27698517 -0.45399052 88.5823822 -34.23337555 -0.45399052 88.30704498 -34.21987152 -0.30901697 88.013153076
		 -34.3549614 -0.30901697 87.74802399 -34.56536484 -0.30901697 87.53762054 -34.83049011 -0.30901697 87.40253448
		 -35.12438202 -0.30901697 87.35598755 -35.41827393 -0.30901697 87.40253448 -35.6833992 -0.30901697 87.53762054
		 -35.89380264 -0.30901697 87.74803162 -36.028892517 -0.30901697 88.013153076 -36.075439453 -0.30901697 88.30704498
		 -36.028892517 -0.30901697 88.60093689 -35.89380264 -0.30901697 88.86605835 -35.6833992 -0.30901697 89.076469421
		 -35.41827393 -0.30901697 89.21155548 -35.12438202 -0.30901697 89.25810242 -34.83049011 -0.30901697 89.21155548
		 -34.56536484 -0.30901697 89.076469421 -34.3549614 -0.30901697 88.86605835 -34.21987534 -0.30901697 88.60093689
		 -34.17332458 -0.30901697 88.30704498 -34.18503571 -0.15643437 88.0018310547 -34.32532501 -0.15643437 87.72649384
		 -34.54383469 -0.15643437 87.50798798 -34.81916809 -0.15643437 87.36769867 -35.12438202 -0.15643437 87.31935883
		 -35.42959595 -0.15643437 87.36769867 -35.70492935 -0.15643437 87.50798798 -35.92343903 -0.15643437 87.72649384
		 -36.063728333 -0.15643437 88.0018310547 -36.11207199 -0.15643437 88.30704498 -36.063728333 -0.15643437 88.61225891
		 -35.92343903 -0.15643437 88.88759613 -35.70492935 -0.15643437 89.10610199 -35.42959595 -0.15643437 89.2463913
		 -35.12438202 -0.15643437 89.29473114 -34.81916809 -0.15643437 89.2463913 -34.54383469 -0.15643437 89.10610199
		 -34.32532501 -0.15643437 88.88759613 -34.18503571 -0.15643437 88.61225891 -34.13669205 -0.15643437 88.30704498
		 -34.17332458 0 87.99803162 -34.31536484 0 87.71926117 -34.53659821 0 87.49802399
		 -34.81536484 0 87.35598755 -35.12438202 0 87.30704498 -35.4333992 0 87.35598755 -35.71216583 0 87.49802399
		 -35.9333992 0 87.71926117 -36.075439453 0 87.99803162 -36.12438202 0 88.30704498
		 -36.075439453 0 88.61605835 -35.9333992 0 88.8948288 -35.71216583 0 89.11605835 -35.4333992 0 89.25810242
		 -35.12438202 0 89.30704498 -34.81536484 0 89.25810242 -34.53659821 0 89.11605835
		 -34.31536484 0 88.8948288 -34.17332458 0 88.61605835 -34.12438202 0 88.30704498 -34.18503571 0.15643437 88.0018310547
		 -34.32532501 0.15643437 87.72649384 -34.54383469 0.15643437 87.50798798 -34.81916809 0.15643437 87.36769867
		 -35.12438202 0.15643437 87.31935883 -35.42959595 0.15643437 87.36769867 -35.70492935 0.15643437 87.50798798
		 -35.92343903 0.15643437 87.72649384 -36.063728333 0.15643437 88.0018310547 -36.11207199 0.15643437 88.30704498
		 -36.063728333 0.15643437 88.61225891 -35.92343903 0.15643437 88.88759613 -35.70492935 0.15643437 89.10610199
		 -35.42959595 0.15643437 89.2463913 -35.12438202 0.15643437 89.29473114 -34.81916809 0.15643437 89.2463913
		 -34.54383469 0.15643437 89.10610199 -34.32532501 0.15643437 88.88759613 -34.18503571 0.15643437 88.61225891
		 -34.13669205 0.15643437 88.30704498 -34.21987152 0.30901697 88.013153076 -34.3549614 0.30901697 87.74802399
		 -34.56536484 0.30901697 87.53762054 -34.83049011 0.30901697 87.40253448 -35.12438202 0.30901697 87.35598755
		 -35.41827393 0.30901697 87.40253448 -35.6833992 0.30901697 87.53762054 -35.89380264 0.30901697 87.74803162
		 -36.028892517 0.30901697 88.013153076 -36.075439453 0.30901697 88.30704498 -36.028892517 0.30901697 88.60093689
		 -35.89380264 0.30901697 88.86605835 -35.6833992 0.30901697 89.076469421 -35.41827393 0.30901697 89.21155548
		 -35.12438202 0.30901697 89.25810242 -34.83049011 0.30901697 89.21155548 -34.56536484 0.30901697 89.076469421
		 -34.3549614 0.30901697 88.86605835 -34.21987534 0.30901697 88.60093689 -34.17332458 0.30901697 88.30704498
		 -34.27698517 0.45399052 88.031707764 -34.40354156 0.45399052 87.7833252 -34.60066223 0.45399052 87.58620453
		 -34.8490448 0.45399052 87.45964813 -35.12438202 0.45399052 87.41603851 -35.39971924 0.45399052 87.45964813
		 -35.64810181 0.45399052 87.58620453 -35.84522247 0.45399052 87.7833252;
	setAttr ".vt[332:465]" -35.97177887 0.45399052 88.031707764 -36.015388489 0.45399052 88.30704498
		 -35.97177887 0.45399052 88.5823822 -35.84522247 0.45399052 88.83076477 -35.64810181 0.45399052 89.027885437
		 -35.39971924 0.45399052 89.15444183 -35.12438202 0.45399052 89.19805145 -34.8490448 0.45399052 89.15444183
		 -34.60066223 0.45399052 89.027885437 -34.40354156 0.45399052 88.83076477 -34.27698517 0.45399052 88.5823822
		 -34.23337555 0.45399052 88.30704498 -34.3549614 0.58778524 88.057044983 -34.46987152 0.58778524 87.83152008
		 -34.6488533 0.58778524 87.65253448 -34.87438202 0.58778524 87.53762054 -35.12438202 0.58778524 87.49802399
		 -35.37438202 0.58778524 87.53762054 -35.59991074 0.58778524 87.65253448 -35.77889252 0.58778524 87.83152008
		 -35.89380264 0.58778524 88.057044983 -35.9333992 0.58778524 88.30704498 -35.89380264 0.58778524 88.55704498
		 -35.77889252 0.58778524 88.78256989 -35.59991074 0.58778524 88.96155548 -35.37438202 0.58778524 89.076469421
		 -35.12438202 0.58778524 89.11605835 -34.87438202 0.58778524 89.076469421 -34.6488533 0.58778524 88.96155548
		 -34.46987534 0.58778524 88.78256989 -34.3549614 0.58778524 88.55704498 -34.31536484 0.58778524 88.30704498
		 -34.45188141 0.70710677 88.088539124 -34.55231857 0.70710677 87.89141846 -34.70875549 0.70710677 87.73498535
		 -34.90587234 0.70710677 87.63454437 -35.12438202 0.70710677 87.59993744 -35.34289169 0.70710677 87.63454437
		 -35.54000854 0.70710677 87.73498535 -35.69644165 0.70710677 87.89141846 -35.79688263 0.70710677 88.088539124
		 -35.83148956 0.70710677 88.30704498 -35.79688263 0.70710677 88.52555084 -35.69644165 0.70710677 88.72267151
		 -35.54000854 0.70710677 88.87910461 -35.34289169 0.70710677 88.97954559 -35.12438202 0.70710677 89.014152527
		 -34.90587234 0.70710677 88.97954559 -34.70875549 0.70710677 88.87910461 -34.55232239 0.70710677 88.72267151
		 -34.45188522 0.70710677 88.52555084 -34.41727448 0.70710677 88.30704498 -34.56536484 0.809017 88.12541199
		 -34.6488533 0.809017 87.96155548 -34.7788887 0.809017 87.83152008 -34.94274521 0.809017 87.74802399
		 -35.12438202 0.809017 87.71926117 -35.30601883 0.809017 87.74802399 -35.46987534 0.809017 87.83152008
		 -35.59991074 0.809017 87.96155548 -35.6833992 0.809017 88.12541199 -35.71216583 0.809017 88.30704498
		 -35.6833992 0.809017 88.48867798 -35.59991074 0.809017 88.65253448 -35.46987534 0.809017 88.78256989
		 -35.30601883 0.809017 88.86605835 -35.12438202 0.809017 88.8948288 -34.94274521 0.809017 88.86605835
		 -34.7788887 0.809017 88.78256989 -34.6488533 0.809017 88.65253448 -34.56536484 0.809017 88.48867798
		 -34.53659821 0.809017 88.30704498 -34.69261169 0.89100653 88.16675568 -34.75709534 0.89100653 88.04019928
		 -34.8575325 0.89100653 87.9397583 -34.98409271 0.89100653 87.87527466 -35.12438202 0.89100653 87.85305786
		 -35.26467133 0.89100653 87.87527466 -35.39123154 0.89100653 87.9397583 -35.4916687 0.89100653 88.04019928
		 -35.55615234 0.89100653 88.16675568 -35.57837296 0.89100653 88.30704498 -35.55615234 0.89100653 88.44733429
		 -35.4916687 0.89100653 88.57389069 -35.39123154 0.89100653 88.67433167 -35.26467133 0.89100653 88.73881531
		 -35.12438202 0.89100653 88.7610321 -34.98409271 0.89100653 88.73881531 -34.8575325 0.89100653 88.67433167
		 -34.75709534 0.89100653 88.57389069 -34.69261169 0.89100653 88.44733429 -34.67039108 0.89100653 88.30704498
		 -34.83049011 0.95105654 88.21155548 -34.87438202 0.95105654 88.12541199 -34.94274521 0.95105654 88.057044983
		 -35.028888702 0.95105654 88.013153076 -35.12438202 0.95105654 87.99803162 -35.21987534 0.95105654 88.013153076
		 -35.30601883 0.95105654 88.057044983 -35.37438202 0.95105654 88.12541199 -35.41827393 0.95105654 88.21155548
		 -35.4333992 0.95105654 88.30704498 -35.41827393 0.95105654 88.40253448 -35.37438202 0.95105654 88.48867798
		 -35.30601883 0.95105654 88.55704498 -35.21987534 0.95105654 88.60093689 -35.12438202 0.95105654 88.61605835
		 -35.028888702 0.95105654 88.60093689 -34.94274521 0.95105654 88.55704498 -34.87438202 0.95105654 88.48867798
		 -34.83049011 0.95105654 88.40253448 -34.81536484 0.95105654 88.30704498 -34.97560501 0.98768836 88.25870514
		 -34.99782562 0.98768836 88.21509552 -35.032432556 0.98768836 88.18048859 -35.076042175 0.98768836 88.15826416
		 -35.12438202 0.98768836 88.15061188 -35.17272186 0.98768836 88.15826416 -35.21633148 0.98768836 88.18048859
		 -35.25093842 0.98768836 88.21509552 -35.27315903 0.98768836 88.25870514 -35.28081512 0.98768836 88.30704498
		 -35.27315903 0.98768836 88.35538483 -35.25093842 0.98768836 88.39899445 -35.21633148 0.98768836 88.43360138
		 -35.17272186 0.98768836 88.45582581 -35.12438202 0.98768836 88.46347809 -35.076042175 0.98768836 88.45582581
		 -35.032432556 0.98768836 88.43360138 -34.99782562 0.98768836 88.39899445 -34.97560501 0.98768836 88.35538483
		 -34.96794891 0.98768836 88.30704498 -35.12438202 -1 88.30704498 -35.12438202 1 88.30704498;
	setAttr -s 980 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0 0 20 1 1 21 1
		 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1
		 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1 40 3 1 40 4 1
		 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1 40 14 1 40 15 1
		 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1 25 41 1 26 41 1
		 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1 36 41 1 37 41 1
		 38 41 1 39 41 1 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 62 0 42 62 1 43 63 1
		 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1
		 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 82 42 1 82 43 1 82 44 1 82 45 1
		 82 46 1 82 47 1;
	setAttr ".ed[166:331]" 82 48 1 82 49 1 82 50 1 82 51 1 82 52 1 82 53 1 82 54 1
		 82 55 1 82 56 1 82 57 1 82 58 1 82 59 1 82 60 1 82 61 1 62 83 1 63 83 1 64 83 1 65 83 1
		 66 83 1 67 83 1 68 83 1 69 83 1 70 83 1 71 83 1 72 83 1 73 83 1 74 83 1 75 83 1 76 83 1
		 77 83 1 78 83 1 79 83 1 80 83 1 81 83 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1
		 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 84 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1
		 109 110 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 104 1 124 125 1 125 126 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 134 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 124 1 144 145 1
		 145 146 1 146 147 1 147 148 1 148 149 1 149 150 1 150 151 1 151 152 1 152 153 1 153 154 1
		 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1
		 163 144 1 164 165 1 165 166 1 166 167 1 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 180 1 180 181 1
		 181 182 1 182 183 1 183 164 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 190 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 200 1 200 201 1 201 202 1 202 203 1 203 184 1 204 205 1 205 206 1 206 207 1 207 208 1
		 208 209 1 209 210 1 210 211 1 211 212 1 212 213 1 213 214 1 214 215 1 215 216 1;
	setAttr ".ed[332:497]" 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1
		 222 223 1 223 204 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 232 1 232 233 1 233 234 1 234 235 1 235 236 1 236 237 1 237 238 1 238 239 1 239 240 1
		 240 241 1 241 242 1 242 243 1 243 224 1 244 245 1 245 246 1 246 247 1 247 248 1 248 249 1
		 249 250 1 250 251 1 251 252 1 252 253 1 253 254 1 254 255 1 255 256 1 256 257 1 257 258 1
		 258 259 1 259 260 1 260 261 1 261 262 1 262 263 1 263 244 1 264 265 1 265 266 1 266 267 1
		 267 268 1 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1 274 275 1 275 276 1
		 276 277 1 277 278 1 278 279 1 279 280 1 280 281 1 281 282 1 282 283 1 283 264 1 284 285 1
		 285 286 1 286 287 1 287 288 1 288 289 1 289 290 1 290 291 1 291 292 1 292 293 1 293 294 1
		 294 295 1 295 296 1 296 297 1 297 298 1 298 299 1 299 300 1 300 301 1 301 302 1 302 303 1
		 303 284 1 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1
		 312 313 1 313 314 1 314 315 1 315 316 1 316 317 1 317 318 1 318 319 1 319 320 1 320 321 1
		 321 322 1 322 323 1 323 304 1 324 325 1 325 326 1 326 327 1 327 328 1 328 329 1 329 330 1
		 330 331 1 331 332 1 332 333 1 333 334 1 334 335 1 335 336 1 336 337 1 337 338 1 338 339 1
		 339 340 1 340 341 1 341 342 1 342 343 1 343 324 1 344 345 1 345 346 1 346 347 1 347 348 1
		 348 349 1 349 350 1 350 351 1 351 352 1 352 353 1 353 354 1 354 355 1 355 356 1 356 357 1
		 357 358 1 358 359 1 359 360 1 360 361 1 361 362 1 362 363 1 363 344 1 364 365 1 365 366 1
		 366 367 1 367 368 1 368 369 1 369 370 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1
		 375 376 1 376 377 1 377 378 1 378 379 1 379 380 1 380 381 1 381 382 1;
	setAttr ".ed[498:663]" 382 383 1 383 364 1 384 385 1 385 386 1 386 387 1 387 388 1
		 388 389 1 389 390 1 390 391 1 391 392 1 392 393 1 393 394 1 394 395 1 395 396 1 396 397 1
		 397 398 1 398 399 1 399 400 1 400 401 1 401 402 1 402 403 1 403 384 1 404 405 1 405 406 1
		 406 407 1 407 408 1 408 409 1 409 410 1 410 411 1 411 412 1 412 413 1 413 414 1 414 415 1
		 415 416 1 416 417 1 417 418 1 418 419 1 419 420 1 420 421 1 421 422 1 422 423 1 423 404 1
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 432 1 432 433 1
		 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 440 1 440 441 1 441 442 1
		 442 443 1 443 424 1 444 445 1 445 446 1 446 447 1 447 448 1 448 449 1 449 450 1 450 451 1
		 451 452 1 452 453 1 453 454 1 454 455 1 455 456 1 456 457 1 457 458 1 458 459 1 459 460 1
		 460 461 1 461 462 1 462 463 1 463 444 1 84 104 1 85 105 1 86 106 1 87 107 1 88 108 1
		 89 109 1 90 110 1 91 111 1 92 112 1 93 113 1 94 114 1 95 115 1 96 116 1 97 117 1
		 98 118 1 99 119 1 100 120 1 101 121 1 102 122 1 103 123 1 104 124 1 105 125 1 106 126 1
		 107 127 1 108 128 1 109 129 1 110 130 1 111 131 1 112 132 1 113 133 1 114 134 1 115 135 1
		 116 136 1 117 137 1 118 138 1 119 139 1 120 140 1 121 141 1 122 142 1 123 143 1 124 144 1
		 125 145 1 126 146 1 127 147 1 128 148 1 129 149 1 130 150 1 131 151 1 132 152 1 133 153 1
		 134 154 1 135 155 1 136 156 1 137 157 1 138 158 1 139 159 1 140 160 1 141 161 1 142 162 1
		 143 163 1 144 164 1 145 165 1 146 166 1 147 167 1 148 168 1 149 169 1 150 170 1 151 171 1
		 152 172 1 153 173 1 154 174 1 155 175 1 156 176 1 157 177 1 158 178 1 159 179 1 160 180 1
		 161 181 1 162 182 1 163 183 1 164 184 1 165 185 1 166 186 1 167 187 1;
	setAttr ".ed[664:829]" 168 188 1 169 189 1 170 190 1 171 191 1 172 192 1 173 193 1
		 174 194 1 175 195 1 176 196 1 177 197 1 178 198 1 179 199 1 180 200 1 181 201 1 182 202 1
		 183 203 1 184 204 1 185 205 1 186 206 1 187 207 1 188 208 1 189 209 1 190 210 1 191 211 1
		 192 212 1 193 213 1 194 214 1 195 215 1 196 216 1 197 217 1 198 218 1 199 219 1 200 220 1
		 201 221 1 202 222 1 203 223 1 204 224 1 205 225 1 206 226 1 207 227 1 208 228 1 209 229 1
		 210 230 1 211 231 1 212 232 1 213 233 1 214 234 1 215 235 1 216 236 1 217 237 1 218 238 1
		 219 239 1 220 240 1 221 241 1 222 242 1 223 243 1 224 244 1 225 245 1 226 246 1 227 247 1
		 228 248 1 229 249 1 230 250 1 231 251 1 232 252 1 233 253 1 234 254 1 235 255 1 236 256 1
		 237 257 1 238 258 1 239 259 1 240 260 1 241 261 1 242 262 1 243 263 1 244 264 1 245 265 1
		 246 266 1 247 267 1 248 268 1 249 269 1 250 270 1 251 271 1 252 272 1 253 273 1 254 274 1
		 255 275 1 256 276 1 257 277 1 258 278 1 259 279 1 260 280 1 261 281 1 262 282 1 263 283 1
		 264 284 1 265 285 1 266 286 1 267 287 1 268 288 1 269 289 1 270 290 1 271 291 1 272 292 1
		 273 293 1 274 294 1 275 295 1 276 296 1 277 297 1 278 298 1 279 299 1 280 300 1 281 301 1
		 282 302 1 283 303 1 284 304 1 285 305 1 286 306 1 287 307 1 288 308 1 289 309 1 290 310 1
		 291 311 1 292 312 1 293 313 1 294 314 1 295 315 1 296 316 1 297 317 1 298 318 1 299 319 1
		 300 320 1 301 321 1 302 322 1 303 323 1 304 324 1 305 325 1 306 326 1 307 327 1 308 328 1
		 309 329 1 310 330 1 311 331 1 312 332 1 313 333 1 314 334 1 315 335 1 316 336 1 317 337 1
		 318 338 1 319 339 1 320 340 1 321 341 1 322 342 1 323 343 1 324 344 1 325 345 1 326 346 1
		 327 347 1 328 348 1 329 349 1 330 350 1 331 351 1 332 352 1 333 353 1;
	setAttr ".ed[830:979]" 334 354 1 335 355 1 336 356 1 337 357 1 338 358 1 339 359 1
		 340 360 1 341 361 1 342 362 1 343 363 1 344 364 1 345 365 1 346 366 1 347 367 1 348 368 1
		 349 369 1 350 370 1 351 371 1 352 372 1 353 373 1 354 374 1 355 375 1 356 376 1 357 377 1
		 358 378 1 359 379 1 360 380 1 361 381 1 362 382 1 363 383 1 364 384 1 365 385 1 366 386 1
		 367 387 1 368 388 1 369 389 1 370 390 1 371 391 1 372 392 1 373 393 1 374 394 1 375 395 1
		 376 396 1 377 397 1 378 398 1 379 399 1 380 400 1 381 401 1 382 402 1 383 403 1 384 404 1
		 385 405 1 386 406 1 387 407 1 388 408 1 389 409 1 390 410 1 391 411 1 392 412 1 393 413 1
		 394 414 1 395 415 1 396 416 1 397 417 1 398 418 1 399 419 1 400 420 1 401 421 1 402 422 1
		 403 423 1 404 424 1 405 425 1 406 426 1 407 427 1 408 428 1 409 429 1 410 430 1 411 431 1
		 412 432 1 413 433 1 414 434 1 415 435 1 416 436 1 417 437 1 418 438 1 419 439 1 420 440 1
		 421 441 1 422 442 1 423 443 1 424 444 1 425 445 1 426 446 1 427 447 1 428 448 1 429 449 1
		 430 450 1 431 451 1 432 452 1 433 453 1 434 454 1 435 455 1 436 456 1 437 457 1 438 458 1
		 439 459 1 440 460 1 441 461 1 442 462 1 443 463 1 464 84 1 464 85 1 464 86 1 464 87 1
		 464 88 1 464 89 1 464 90 1 464 91 1 464 92 1 464 93 1 464 94 1 464 95 1 464 96 1
		 464 97 1 464 98 1 464 99 1 464 100 1 464 101 1 464 102 1 464 103 1 444 465 1 445 465 1
		 446 465 1 447 465 1 448 465 1 449 465 1 450 465 1 451 465 1 452 465 1 453 465 1 454 465 1
		 455 465 1 456 465 1 457 465 1 458 465 1 459 465 1 460 465 1 461 465 1 462 465 1 463 465 1;
	setAttr -s 520 -ch 1960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80
		f 4 140 120 -142 -101
		mu 0 4 84 85 86 87
		f 4 141 121 -143 -102
		mu 0 4 87 86 88 89
		f 4 142 122 -144 -103
		mu 0 4 89 88 90 91
		f 4 143 123 -145 -104
		mu 0 4 91 90 92 93
		f 4 144 124 -146 -105
		mu 0 4 93 92 94 95
		f 4 145 125 -147 -106
		mu 0 4 95 94 96 97
		f 4 146 126 -148 -107
		mu 0 4 97 96 98 99
		f 4 147 127 -149 -108
		mu 0 4 99 98 100 101
		f 4 148 128 -150 -109
		mu 0 4 101 100 102 103
		f 4 149 129 -151 -110
		mu 0 4 103 102 104 105
		f 4 150 130 -152 -111
		mu 0 4 105 104 106 107
		f 4 151 131 -153 -112
		mu 0 4 107 106 108 109
		f 4 152 132 -154 -113
		mu 0 4 109 108 110 111
		f 4 153 133 -155 -114
		mu 0 4 111 110 112 113
		f 4 154 134 -156 -115
		mu 0 4 113 112 114 115
		f 4 155 135 -157 -116
		mu 0 4 115 114 116 117
		f 4 156 136 -158 -117
		mu 0 4 117 116 118 119
		f 4 157 137 -159 -118
		mu 0 4 119 118 120 121
		f 4 158 138 -160 -119
		mu 0 4 121 120 122 123
		f 4 159 139 -141 -120
		mu 0 4 123 122 124 125
		f 3 -162 160 100
		mu 0 3 126 127 128
		f 3 -163 161 101
		mu 0 3 129 127 126
		f 3 -164 162 102
		mu 0 3 130 127 129
		f 3 -165 163 103
		mu 0 3 131 127 130
		f 3 -166 164 104
		mu 0 3 132 127 131
		f 3 -167 165 105
		mu 0 3 133 127 132
		f 3 -168 166 106
		mu 0 3 134 127 133
		f 3 -169 167 107
		mu 0 3 135 127 134
		f 3 -170 168 108
		mu 0 3 136 127 135
		f 3 -171 169 109
		mu 0 3 137 127 136
		f 3 -172 170 110
		mu 0 3 138 127 137
		f 3 -173 171 111
		mu 0 3 139 127 138
		f 3 -174 172 112
		mu 0 3 140 127 139
		f 3 -175 173 113
		mu 0 3 141 127 140
		f 3 -176 174 114
		mu 0 3 142 127 141
		f 3 -177 175 115
		mu 0 3 143 127 142
		f 3 -178 176 116
		mu 0 3 144 127 143
		f 3 -179 177 117
		mu 0 3 145 127 144
		f 3 -180 178 118
		mu 0 3 146 127 145
		f 3 -161 179 119
		mu 0 3 128 127 146
		f 3 180 -182 -121
		mu 0 3 147 148 149
		f 3 181 -183 -122
		mu 0 3 149 148 150
		f 3 182 -184 -123
		mu 0 3 150 148 151
		f 3 183 -185 -124
		mu 0 3 151 148 152
		f 3 184 -186 -125
		mu 0 3 152 148 153
		f 3 185 -187 -126
		mu 0 3 153 148 154
		f 3 186 -188 -127
		mu 0 3 154 148 155
		f 3 187 -189 -128
		mu 0 3 155 148 156
		f 3 188 -190 -129
		mu 0 3 156 148 157
		f 3 189 -191 -130
		mu 0 3 157 148 158
		f 3 190 -192 -131
		mu 0 3 158 148 159
		f 3 191 -193 -132
		mu 0 3 159 148 160
		f 3 192 -194 -133
		mu 0 3 160 148 161
		f 3 193 -195 -134
		mu 0 3 161 148 162
		f 3 194 -196 -135
		mu 0 3 162 148 163
		f 3 195 -197 -136
		mu 0 3 163 148 164
		f 3 196 -198 -137
		mu 0 3 164 148 165
		f 3 197 -199 -138
		mu 0 3 165 148 166
		f 3 198 -200 -139
		mu 0 3 166 148 167
		f 3 199 -181 -140
		mu 0 3 167 148 147
		f 4 200 581 -221 -581
		mu 0 4 168 169 170 171
		f 4 201 582 -222 -582
		mu 0 4 169 172 173 170
		f 4 202 583 -223 -583
		mu 0 4 172 174 175 173
		f 4 203 584 -224 -584
		mu 0 4 174 176 177 175
		f 4 204 585 -225 -585
		mu 0 4 176 178 179 177
		f 4 205 586 -226 -586
		mu 0 4 178 180 181 179
		f 4 206 587 -227 -587
		mu 0 4 180 182 183 181
		f 4 207 588 -228 -588
		mu 0 4 182 184 185 183
		f 4 208 589 -229 -589
		mu 0 4 184 186 187 185
		f 4 209 590 -230 -590
		mu 0 4 186 188 189 187
		f 4 210 591 -231 -591
		mu 0 4 188 190 191 189
		f 4 211 592 -232 -592
		mu 0 4 190 192 193 191
		f 4 212 593 -233 -593
		mu 0 4 192 194 195 193
		f 4 213 594 -234 -594
		mu 0 4 194 196 197 195
		f 4 214 595 -235 -595
		mu 0 4 196 198 199 197
		f 4 215 596 -236 -596
		mu 0 4 198 200 201 199
		f 4 216 597 -237 -597
		mu 0 4 200 202 203 201
		f 4 217 598 -238 -598
		mu 0 4 202 204 205 203
		f 4 218 599 -239 -599
		mu 0 4 204 206 207 205
		f 4 219 580 -240 -600
		mu 0 4 206 208 209 207
		f 4 220 601 -241 -601
		mu 0 4 171 170 210 211
		f 4 221 602 -242 -602
		mu 0 4 170 173 212 210
		f 4 222 603 -243 -603
		mu 0 4 173 175 213 212
		f 4 223 604 -244 -604
		mu 0 4 175 177 214 213
		f 4 224 605 -245 -605
		mu 0 4 177 179 215 214
		f 4 225 606 -246 -606
		mu 0 4 179 181 216 215
		f 4 226 607 -247 -607
		mu 0 4 181 183 217 216
		f 4 227 608 -248 -608
		mu 0 4 183 185 218 217
		f 4 228 609 -249 -609
		mu 0 4 185 187 219 218
		f 4 229 610 -250 -610
		mu 0 4 187 189 220 219
		f 4 230 611 -251 -611
		mu 0 4 189 191 221 220
		f 4 231 612 -252 -612
		mu 0 4 191 193 222 221
		f 4 232 613 -253 -613
		mu 0 4 193 195 223 222
		f 4 233 614 -254 -614
		mu 0 4 195 197 224 223
		f 4 234 615 -255 -615
		mu 0 4 197 199 225 224
		f 4 235 616 -256 -616
		mu 0 4 199 201 226 225
		f 4 236 617 -257 -617
		mu 0 4 201 203 227 226
		f 4 237 618 -258 -618
		mu 0 4 203 205 228 227
		f 4 238 619 -259 -619
		mu 0 4 205 207 229 228
		f 4 239 600 -260 -620
		mu 0 4 207 209 230 229
		f 4 240 621 -261 -621
		mu 0 4 211 210 231 232
		f 4 241 622 -262 -622
		mu 0 4 210 212 233 231
		f 4 242 623 -263 -623
		mu 0 4 212 213 234 233
		f 4 243 624 -264 -624
		mu 0 4 213 214 235 234
		f 4 244 625 -265 -625
		mu 0 4 214 215 236 235
		f 4 245 626 -266 -626
		mu 0 4 215 216 237 236
		f 4 246 627 -267 -627
		mu 0 4 216 217 238 237
		f 4 247 628 -268 -628
		mu 0 4 217 218 239 238
		f 4 248 629 -269 -629
		mu 0 4 218 219 240 239
		f 4 249 630 -270 -630
		mu 0 4 219 220 241 240
		f 4 250 631 -271 -631
		mu 0 4 220 221 242 241
		f 4 251 632 -272 -632
		mu 0 4 221 222 243 242
		f 4 252 633 -273 -633
		mu 0 4 222 223 244 243
		f 4 253 634 -274 -634
		mu 0 4 223 224 245 244
		f 4 254 635 -275 -635
		mu 0 4 224 225 246 245
		f 4 255 636 -276 -636
		mu 0 4 225 226 247 246
		f 4 256 637 -277 -637
		mu 0 4 226 227 248 247
		f 4 257 638 -278 -638
		mu 0 4 227 228 249 248
		f 4 258 639 -279 -639
		mu 0 4 228 229 250 249
		f 4 259 620 -280 -640
		mu 0 4 229 230 251 250
		f 4 260 641 -281 -641
		mu 0 4 232 231 252 253
		f 4 261 642 -282 -642
		mu 0 4 231 233 254 252
		f 4 262 643 -283 -643
		mu 0 4 233 234 255 254
		f 4 263 644 -284 -644
		mu 0 4 234 235 256 255
		f 4 264 645 -285 -645
		mu 0 4 235 236 257 256
		f 4 265 646 -286 -646
		mu 0 4 236 237 258 257
		f 4 266 647 -287 -647
		mu 0 4 237 238 259 258
		f 4 267 648 -288 -648
		mu 0 4 238 239 260 259
		f 4 268 649 -289 -649
		mu 0 4 239 240 261 260
		f 4 269 650 -290 -650
		mu 0 4 240 241 262 261
		f 4 270 651 -291 -651
		mu 0 4 241 242 263 262
		f 4 271 652 -292 -652
		mu 0 4 242 243 264 263
		f 4 272 653 -293 -653
		mu 0 4 243 244 265 264
		f 4 273 654 -294 -654
		mu 0 4 244 245 266 265
		f 4 274 655 -295 -655
		mu 0 4 245 246 267 266
		f 4 275 656 -296 -656
		mu 0 4 246 247 268 267
		f 4 276 657 -297 -657
		mu 0 4 247 248 269 268
		f 4 277 658 -298 -658
		mu 0 4 248 249 270 269
		f 4 278 659 -299 -659
		mu 0 4 249 250 271 270
		f 4 279 640 -300 -660
		mu 0 4 250 251 272 271
		f 4 280 661 -301 -661
		mu 0 4 253 252 273 274
		f 4 281 662 -302 -662
		mu 0 4 252 254 275 273
		f 4 282 663 -303 -663
		mu 0 4 254 255 276 275
		f 4 283 664 -304 -664
		mu 0 4 255 256 277 276
		f 4 284 665 -305 -665
		mu 0 4 256 257 278 277
		f 4 285 666 -306 -666
		mu 0 4 257 258 279 278
		f 4 286 667 -307 -667
		mu 0 4 258 259 280 279
		f 4 287 668 -308 -668
		mu 0 4 259 260 281 280
		f 4 288 669 -309 -669
		mu 0 4 260 261 282 281
		f 4 289 670 -310 -670
		mu 0 4 261 262 283 282
		f 4 290 671 -311 -671
		mu 0 4 262 263 284 283
		f 4 291 672 -312 -672
		mu 0 4 263 264 285 284
		f 4 292 673 -313 -673
		mu 0 4 264 265 286 285
		f 4 293 674 -314 -674
		mu 0 4 265 266 287 286
		f 4 294 675 -315 -675
		mu 0 4 266 267 288 287
		f 4 295 676 -316 -676
		mu 0 4 267 268 289 288
		f 4 296 677 -317 -677
		mu 0 4 268 269 290 289
		f 4 297 678 -318 -678
		mu 0 4 269 270 291 290
		f 4 298 679 -319 -679
		mu 0 4 270 271 292 291
		f 4 299 660 -320 -680
		mu 0 4 271 272 293 292
		f 4 300 681 -321 -681
		mu 0 4 274 273 294 295
		f 4 301 682 -322 -682
		mu 0 4 273 275 296 294
		f 4 302 683 -323 -683
		mu 0 4 275 276 297 296
		f 4 303 684 -324 -684
		mu 0 4 276 277 298 297
		f 4 304 685 -325 -685
		mu 0 4 277 278 299 298
		f 4 305 686 -326 -686
		mu 0 4 278 279 300 299
		f 4 306 687 -327 -687
		mu 0 4 279 280 301 300
		f 4 307 688 -328 -688
		mu 0 4 280 281 302 301
		f 4 308 689 -329 -689
		mu 0 4 281 282 303 302
		f 4 309 690 -330 -690
		mu 0 4 282 283 304 303
		f 4 310 691 -331 -691
		mu 0 4 283 284 305 304
		f 4 311 692 -332 -692
		mu 0 4 284 285 306 305
		f 4 312 693 -333 -693
		mu 0 4 285 286 307 306
		f 4 313 694 -334 -694
		mu 0 4 286 287 308 307
		f 4 314 695 -335 -695
		mu 0 4 287 288 309 308
		f 4 315 696 -336 -696
		mu 0 4 288 289 310 309
		f 4 316 697 -337 -697
		mu 0 4 289 290 311 310
		f 4 317 698 -338 -698
		mu 0 4 290 291 312 311
		f 4 318 699 -339 -699
		mu 0 4 291 292 313 312
		f 4 319 680 -340 -700
		mu 0 4 292 293 314 313
		f 4 320 701 -341 -701
		mu 0 4 295 294 315 316
		f 4 321 702 -342 -702
		mu 0 4 294 296 317 315
		f 4 322 703 -343 -703
		mu 0 4 296 297 318 317
		f 4 323 704 -344 -704
		mu 0 4 297 298 319 318
		f 4 324 705 -345 -705
		mu 0 4 298 299 320 319
		f 4 325 706 -346 -706
		mu 0 4 299 300 321 320
		f 4 326 707 -347 -707
		mu 0 4 300 301 322 321
		f 4 327 708 -348 -708
		mu 0 4 301 302 323 322
		f 4 328 709 -349 -709
		mu 0 4 302 303 324 323
		f 4 329 710 -350 -710
		mu 0 4 303 304 325 324
		f 4 330 711 -351 -711
		mu 0 4 304 305 326 325
		f 4 331 712 -352 -712
		mu 0 4 305 306 327 326
		f 4 332 713 -353 -713
		mu 0 4 306 307 328 327
		f 4 333 714 -354 -714
		mu 0 4 307 308 329 328
		f 4 334 715 -355 -715
		mu 0 4 308 309 330 329
		f 4 335 716 -356 -716
		mu 0 4 309 310 331 330
		f 4 336 717 -357 -717
		mu 0 4 310 311 332 331
		f 4 337 718 -358 -718
		mu 0 4 311 312 333 332
		f 4 338 719 -359 -719
		mu 0 4 312 313 334 333
		f 4 339 700 -360 -720
		mu 0 4 313 314 335 334
		f 4 340 721 -361 -721
		mu 0 4 316 315 336 337
		f 4 341 722 -362 -722
		mu 0 4 315 317 338 336
		f 4 342 723 -363 -723
		mu 0 4 317 318 339 338
		f 4 343 724 -364 -724
		mu 0 4 318 319 340 339
		f 4 344 725 -365 -725
		mu 0 4 319 320 341 340
		f 4 345 726 -366 -726
		mu 0 4 320 321 342 341
		f 4 346 727 -367 -727
		mu 0 4 321 322 343 342
		f 4 347 728 -368 -728
		mu 0 4 322 323 344 343
		f 4 348 729 -369 -729
		mu 0 4 323 324 345 344
		f 4 349 730 -370 -730
		mu 0 4 324 325 346 345
		f 4 350 731 -371 -731
		mu 0 4 325 326 347 346
		f 4 351 732 -372 -732
		mu 0 4 326 327 348 347
		f 4 352 733 -373 -733
		mu 0 4 327 328 349 348
		f 4 353 734 -374 -734
		mu 0 4 328 329 350 349
		f 4 354 735 -375 -735
		mu 0 4 329 330 351 350
		f 4 355 736 -376 -736
		mu 0 4 330 331 352 351
		f 4 356 737 -377 -737
		mu 0 4 331 332 353 352
		f 4 357 738 -378 -738
		mu 0 4 332 333 354 353
		f 4 358 739 -379 -739
		mu 0 4 333 334 355 354
		f 4 359 720 -380 -740
		mu 0 4 334 335 356 355
		f 4 360 741 -381 -741
		mu 0 4 337 336 357 358
		f 4 361 742 -382 -742
		mu 0 4 336 338 359 357
		f 4 362 743 -383 -743
		mu 0 4 338 339 360 359
		f 4 363 744 -384 -744
		mu 0 4 339 340 361 360
		f 4 364 745 -385 -745
		mu 0 4 340 341 362 361
		f 4 365 746 -386 -746
		mu 0 4 341 342 363 362
		f 4 366 747 -387 -747
		mu 0 4 342 343 364 363
		f 4 367 748 -388 -748
		mu 0 4 343 344 365 364
		f 4 368 749 -389 -749
		mu 0 4 344 345 366 365
		f 4 369 750 -390 -750
		mu 0 4 345 346 367 366
		f 4 370 751 -391 -751
		mu 0 4 346 347 368 367
		f 4 371 752 -392 -752
		mu 0 4 347 348 369 368
		f 4 372 753 -393 -753
		mu 0 4 348 349 370 369
		f 4 373 754 -394 -754
		mu 0 4 349 350 371 370
		f 4 374 755 -395 -755
		mu 0 4 350 351 372 371
		f 4 375 756 -396 -756
		mu 0 4 351 352 373 372
		f 4 376 757 -397 -757
		mu 0 4 352 353 374 373
		f 4 377 758 -398 -758
		mu 0 4 353 354 375 374
		f 4 378 759 -399 -759
		mu 0 4 354 355 376 375
		f 4 379 740 -400 -760
		mu 0 4 355 356 377 376
		f 4 380 761 -401 -761
		mu 0 4 358 357 378 379
		f 4 381 762 -402 -762
		mu 0 4 357 359 380 378
		f 4 382 763 -403 -763
		mu 0 4 359 360 381 380
		f 4 383 764 -404 -764
		mu 0 4 360 361 382 381
		f 4 384 765 -405 -765
		mu 0 4 361 362 383 382
		f 4 385 766 -406 -766
		mu 0 4 362 363 384 383
		f 4 386 767 -407 -767
		mu 0 4 363 364 385 384
		f 4 387 768 -408 -768
		mu 0 4 364 365 386 385
		f 4 388 769 -409 -769
		mu 0 4 365 366 387 386
		f 4 389 770 -410 -770
		mu 0 4 366 367 388 387
		f 4 390 771 -411 -771
		mu 0 4 367 368 389 388
		f 4 391 772 -412 -772
		mu 0 4 368 369 390 389
		f 4 392 773 -413 -773
		mu 0 4 369 370 391 390
		f 4 393 774 -414 -774
		mu 0 4 370 371 392 391
		f 4 394 775 -415 -775
		mu 0 4 371 372 393 392
		f 4 395 776 -416 -776
		mu 0 4 372 373 394 393
		f 4 396 777 -417 -777
		mu 0 4 373 374 395 394
		f 4 397 778 -418 -778
		mu 0 4 374 375 396 395
		f 4 398 779 -419 -779
		mu 0 4 375 376 397 396
		f 4 399 760 -420 -780
		mu 0 4 376 377 398 397
		f 4 400 781 -421 -781
		mu 0 4 379 378 399 400
		f 4 401 782 -422 -782
		mu 0 4 378 380 401 399
		f 4 402 783 -423 -783
		mu 0 4 380 381 402 401
		f 4 403 784 -424 -784
		mu 0 4 381 382 403 402
		f 4 404 785 -425 -785
		mu 0 4 382 383 404 403
		f 4 405 786 -426 -786
		mu 0 4 383 384 405 404
		f 4 406 787 -427 -787
		mu 0 4 384 385 406 405
		f 4 407 788 -428 -788
		mu 0 4 385 386 407 406
		f 4 408 789 -429 -789
		mu 0 4 386 387 408 407
		f 4 409 790 -430 -790
		mu 0 4 387 388 409 408
		f 4 410 791 -431 -791
		mu 0 4 388 389 410 409
		f 4 411 792 -432 -792
		mu 0 4 389 390 411 410
		f 4 412 793 -433 -793
		mu 0 4 390 391 412 411
		f 4 413 794 -434 -794
		mu 0 4 391 392 413 412
		f 4 414 795 -435 -795
		mu 0 4 392 393 414 413
		f 4 415 796 -436 -796
		mu 0 4 393 394 415 414
		f 4 416 797 -437 -797
		mu 0 4 394 395 416 415
		f 4 417 798 -438 -798
		mu 0 4 395 396 417 416
		f 4 418 799 -439 -799
		mu 0 4 396 397 418 417
		f 4 419 780 -440 -800
		mu 0 4 397 398 419 418
		f 4 420 801 -441 -801
		mu 0 4 400 399 420 421
		f 4 421 802 -442 -802
		mu 0 4 399 401 422 420
		f 4 422 803 -443 -803
		mu 0 4 401 402 423 422
		f 4 423 804 -444 -804
		mu 0 4 402 403 424 423
		f 4 424 805 -445 -805
		mu 0 4 403 404 425 424
		f 4 425 806 -446 -806
		mu 0 4 404 405 426 425
		f 4 426 807 -447 -807
		mu 0 4 405 406 427 426
		f 4 427 808 -448 -808
		mu 0 4 406 407 428 427
		f 4 428 809 -449 -809
		mu 0 4 407 408 429 428
		f 4 429 810 -450 -810
		mu 0 4 408 409 430 429
		f 4 430 811 -451 -811
		mu 0 4 409 410 431 430
		f 4 431 812 -452 -812
		mu 0 4 410 411 432 431
		f 4 432 813 -453 -813
		mu 0 4 411 412 433 432
		f 4 433 814 -454 -814
		mu 0 4 412 413 434 433
		f 4 434 815 -455 -815
		mu 0 4 413 414 435 434
		f 4 435 816 -456 -816
		mu 0 4 414 415 436 435
		f 4 436 817 -457 -817
		mu 0 4 415 416 437 436
		f 4 437 818 -458 -818
		mu 0 4 416 417 438 437
		f 4 438 819 -459 -819
		mu 0 4 417 418 439 438
		f 4 439 800 -460 -820
		mu 0 4 418 419 440 439
		f 4 440 821 -461 -821
		mu 0 4 421 420 441 442
		f 4 441 822 -462 -822
		mu 0 4 420 422 443 441
		f 4 442 823 -463 -823
		mu 0 4 422 423 444 443
		f 4 443 824 -464 -824
		mu 0 4 423 424 445 444
		f 4 444 825 -465 -825
		mu 0 4 424 425 446 445
		f 4 445 826 -466 -826
		mu 0 4 425 426 447 446
		f 4 446 827 -467 -827
		mu 0 4 426 427 448 447
		f 4 447 828 -468 -828
		mu 0 4 427 428 449 448
		f 4 448 829 -469 -829
		mu 0 4 428 429 450 449
		f 4 449 830 -470 -830
		mu 0 4 429 430 451 450
		f 4 450 831 -471 -831
		mu 0 4 430 431 452 451
		f 4 451 832 -472 -832
		mu 0 4 431 432 453 452
		f 4 452 833 -473 -833
		mu 0 4 432 433 454 453
		f 4 453 834 -474 -834
		mu 0 4 433 434 455 454
		f 4 454 835 -475 -835
		mu 0 4 434 435 456 455
		f 4 455 836 -476 -836
		mu 0 4 435 436 457 456
		f 4 456 837 -477 -837
		mu 0 4 436 437 458 457
		f 4 457 838 -478 -838
		mu 0 4 437 438 459 458
		f 4 458 839 -479 -839
		mu 0 4 438 439 460 459
		f 4 459 820 -480 -840
		mu 0 4 439 440 461 460
		f 4 460 841 -481 -841
		mu 0 4 442 441 462 463
		f 4 461 842 -482 -842
		mu 0 4 441 443 464 462
		f 4 462 843 -483 -843
		mu 0 4 443 444 465 464
		f 4 463 844 -484 -844
		mu 0 4 444 445 466 465
		f 4 464 845 -485 -845
		mu 0 4 445 446 467 466
		f 4 465 846 -486 -846
		mu 0 4 446 447 468 467
		f 4 466 847 -487 -847
		mu 0 4 447 448 469 468
		f 4 467 848 -488 -848
		mu 0 4 448 449 470 469
		f 4 468 849 -489 -849
		mu 0 4 449 450 471 470
		f 4 469 850 -490 -850
		mu 0 4 450 451 472 471
		f 4 470 851 -491 -851
		mu 0 4 451 452 473 472
		f 4 471 852 -492 -852
		mu 0 4 452 453 474 473
		f 4 472 853 -493 -853
		mu 0 4 453 454 475 474
		f 4 473 854 -494 -854
		mu 0 4 454 455 476 475
		f 4 474 855 -495 -855
		mu 0 4 455 456 477 476
		f 4 475 856 -496 -856
		mu 0 4 456 457 478 477
		f 4 476 857 -497 -857
		mu 0 4 457 458 479 478
		f 4 477 858 -498 -858
		mu 0 4 458 459 480 479
		f 4 478 859 -499 -859
		mu 0 4 459 460 481 480
		f 4 479 840 -500 -860
		mu 0 4 460 461 482 481
		f 4 480 861 -501 -861
		mu 0 4 463 462 483 484
		f 4 481 862 -502 -862
		mu 0 4 462 464 485 483
		f 4 482 863 -503 -863
		mu 0 4 464 465 486 485
		f 4 483 864 -504 -864
		mu 0 4 465 466 487 486
		f 4 484 865 -505 -865
		mu 0 4 466 467 488 487
		f 4 485 866 -506 -866
		mu 0 4 467 468 489 488
		f 4 486 867 -507 -867
		mu 0 4 468 469 490 489
		f 4 487 868 -508 -868
		mu 0 4 469 470 491 490
		f 4 488 869 -509 -869
		mu 0 4 470 471 492 491
		f 4 489 870 -510 -870
		mu 0 4 471 472 493 492
		f 4 490 871 -511 -871
		mu 0 4 472 473 494 493
		f 4 491 872 -512 -872
		mu 0 4 473 474 495 494
		f 4 492 873 -513 -873
		mu 0 4 474 475 496 495
		f 4 493 874 -514 -874
		mu 0 4 475 476 497 496
		f 4 494 875 -515 -875
		mu 0 4 476 477 498 497
		f 4 495 876 -516 -876
		mu 0 4 477 478 499 498
		f 4 496 877 -517 -877
		mu 0 4 478 479 500 499
		f 4 497 878 -518 -878
		mu 0 4 479 480 501 500
		f 4 498 879 -519 -879
		mu 0 4 480 481 502 501
		f 4 499 860 -520 -880
		mu 0 4 481 482 503 502
		f 4 500 881 -521 -881
		mu 0 4 484 483 504 505
		f 4 501 882 -522 -882
		mu 0 4 483 485 506 504
		f 4 502 883 -523 -883
		mu 0 4 485 486 507 506
		f 4 503 884 -524 -884
		mu 0 4 486 487 508 507
		f 4 504 885 -525 -885
		mu 0 4 487 488 509 508
		f 4 505 886 -526 -886
		mu 0 4 488 489 510 509
		f 4 506 887 -527 -887
		mu 0 4 489 490 511 510
		f 4 507 888 -528 -888
		mu 0 4 490 491 512 511
		f 4 508 889 -529 -889
		mu 0 4 491 492 513 512
		f 4 509 890 -530 -890
		mu 0 4 492 493 514 513
		f 4 510 891 -531 -891
		mu 0 4 493 494 515 514
		f 4 511 892 -532 -892
		mu 0 4 494 495 516 515
		f 4 512 893 -533 -893
		mu 0 4 495 496 517 516
		f 4 513 894 -534 -894
		mu 0 4 496 497 518 517
		f 4 514 895 -535 -895
		mu 0 4 497 498 519 518
		f 4 515 896 -536 -896
		mu 0 4 498 499 520 519
		f 4 516 897 -537 -897
		mu 0 4 499 500 521 520
		f 4 517 898 -538 -898
		mu 0 4 500 501 522 521
		f 4 518 899 -539 -899
		mu 0 4 501 502 523 522
		f 4 519 880 -540 -900
		mu 0 4 502 503 524 523
		f 4 520 901 -541 -901
		mu 0 4 505 504 525 526
		f 4 521 902 -542 -902
		mu 0 4 504 506 527 525
		f 4 522 903 -543 -903
		mu 0 4 506 507 528 527
		f 4 523 904 -544 -904
		mu 0 4 507 508 529 528
		f 4 524 905 -545 -905
		mu 0 4 508 509 530 529
		f 4 525 906 -546 -906
		mu 0 4 509 510 531 530
		f 4 526 907 -547 -907
		mu 0 4 510 511 532 531
		f 4 527 908 -548 -908
		mu 0 4 511 512 533 532
		f 4 528 909 -549 -909
		mu 0 4 512 513 534 533
		f 4 529 910 -550 -910
		mu 0 4 513 514 535 534
		f 4 530 911 -551 -911
		mu 0 4 514 515 536 535
		f 4 531 912 -552 -912
		mu 0 4 515 516 537 536
		f 4 532 913 -553 -913
		mu 0 4 516 517 538 537
		f 4 533 914 -554 -914
		mu 0 4 517 518 539 538
		f 4 534 915 -555 -915
		mu 0 4 518 519 540 539
		f 4 535 916 -556 -916
		mu 0 4 519 520 541 540
		f 4 536 917 -557 -917
		mu 0 4 520 521 542 541
		f 4 537 918 -558 -918
		mu 0 4 521 522 543 542
		f 4 538 919 -559 -919
		mu 0 4 522 523 544 543
		f 4 539 900 -560 -920
		mu 0 4 523 524 545 544
		f 4 540 921 -561 -921
		mu 0 4 526 525 546 547
		f 4 541 922 -562 -922
		mu 0 4 525 527 548 546
		f 4 542 923 -563 -923
		mu 0 4 527 528 549 548
		f 4 543 924 -564 -924
		mu 0 4 528 529 550 549
		f 4 544 925 -565 -925
		mu 0 4 529 530 551 550
		f 4 545 926 -566 -926
		mu 0 4 530 531 552 551
		f 4 546 927 -567 -927
		mu 0 4 531 532 553 552
		f 4 547 928 -568 -928
		mu 0 4 532 533 554 553
		f 4 548 929 -569 -929
		mu 0 4 533 534 555 554
		f 4 549 930 -570 -930
		mu 0 4 534 535 556 555
		f 4 550 931 -571 -931
		mu 0 4 535 536 557 556
		f 4 551 932 -572 -932
		mu 0 4 536 537 558 557
		f 4 552 933 -573 -933
		mu 0 4 537 538 559 558
		f 4 553 934 -574 -934
		mu 0 4 538 539 560 559
		f 4 554 935 -575 -935
		mu 0 4 539 540 561 560
		f 4 555 936 -576 -936
		mu 0 4 540 541 562 561
		f 4 556 937 -577 -937
		mu 0 4 541 542 563 562
		f 4 557 938 -578 -938
		mu 0 4 542 543 564 563
		f 4 558 939 -579 -939
		mu 0 4 543 544 565 564
		f 4 559 920 -580 -940
		mu 0 4 544 545 566 565
		f 3 -201 -941 941
		mu 0 3 169 168 567
		f 3 -202 -942 942
		mu 0 3 172 169 568
		f 3 -203 -943 943
		mu 0 3 174 172 569
		f 3 -204 -944 944
		mu 0 3 176 174 570
		f 3 -205 -945 945
		mu 0 3 178 176 571
		f 3 -206 -946 946
		mu 0 3 180 178 572
		f 3 -207 -947 947
		mu 0 3 182 180 573
		f 3 -208 -948 948
		mu 0 3 184 182 574
		f 3 -209 -949 949
		mu 0 3 186 184 575
		f 3 -210 -950 950
		mu 0 3 188 186 576
		f 3 -211 -951 951
		mu 0 3 190 188 577
		f 3 -212 -952 952
		mu 0 3 192 190 578
		f 3 -213 -953 953
		mu 0 3 194 192 579
		f 3 -214 -954 954
		mu 0 3 196 194 580
		f 3 -215 -955 955
		mu 0 3 198 196 581
		f 3 -216 -956 956
		mu 0 3 200 198 582
		f 3 -217 -957 957
		mu 0 3 202 200 583
		f 3 -218 -958 958
		mu 0 3 204 202 584
		f 3 -219 -959 959
		mu 0 3 206 204 585
		f 3 -220 -960 940
		mu 0 3 208 206 586;
	setAttr ".fc[500:519]"
		f 3 560 961 -961
		mu 0 3 547 546 587
		f 3 561 962 -962
		mu 0 3 546 548 588
		f 3 562 963 -963
		mu 0 3 548 549 589
		f 3 563 964 -964
		mu 0 3 549 550 590
		f 3 564 965 -965
		mu 0 3 550 551 591
		f 3 565 966 -966
		mu 0 3 551 552 592
		f 3 566 967 -967
		mu 0 3 552 553 593
		f 3 567 968 -968
		mu 0 3 553 554 594
		f 3 568 969 -969
		mu 0 3 554 555 595
		f 3 569 970 -970
		mu 0 3 555 556 596
		f 3 570 971 -971
		mu 0 3 556 557 597
		f 3 571 972 -972
		mu 0 3 557 558 598
		f 3 572 973 -973
		mu 0 3 558 559 599
		f 3 573 974 -974
		mu 0 3 559 560 600
		f 3 574 975 -975
		mu 0 3 560 561 601
		f 3 575 976 -976
		mu 0 3 561 562 602
		f 3 576 977 -977
		mu 0 3 562 563 603
		f 3 577 978 -978
		mu 0 3 563 564 604
		f 3 578 979 -979
		mu 0 3 564 565 605
		f 3 579 960 -980
		mu 0 3 565 566 606;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder10";
	rename -uid "15D52512-4E21-9DE8-CA3F-51B08C61DEA7";
	setAttr ".t" -type "double3" 60.560099656505294 -67.664032016786564 8.3088346573765826 ;
	setAttr ".r" -type "double3" 90.000000000000156 54.076180972864854 -5.421008324132296e-015 ;
	setAttr ".s" -type "double3" 5.4900845656721531 6.2113566359103149 5.4900845656721531 ;
	setAttr ".rp" -type "double3" 0 -12.663419087727867 3.12177338585966e-031 ;
	setAttr ".rpt" -type "double3" 0 12.663419087727863 -12.663419087727867 ;
	setAttr ".sp" -type "double3" 0 -1.0000001411444472 2.4651903288156619e-032 ;
	setAttr ".spt" -type "double3" 0 -11.66341894658342 2.8752543529780938e-031 ;
createNode mesh -n "pCylinderShape10" -p "pCylinder10";
	rename -uid "BC972B2B-40CC-99C1-FEF4-3DBC8BF3EEB5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999988079071045 0.6757718026638031 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 434 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.5 0.83749998 0.63374424 0.11216897
		 0.61376965 0.072966613 0.58265835 0.041855596 0.54345608 0.021881036 0.5 0.01499831
		 0.45654392 0.021881066 0.41734159 0.041855577 0.38623047 0.072966672 0.36625603 0.11216895
		 0.35937339 0.15562506 0.36625603 0.19908123 0.3862305 0.23828337 0.41734159 0.26939455
		 0.45654395 0.28936902 0.5 0.29625177 0.54345608 0.28936917 0.58265841 0.26939464
		 0.61376959 0.2382835 0.63374388 0.19908109 0.50000006 0.15000001 0.64062685 0.15562505
		 0.38749999 0.32853267 0.375 0.66310376 0.39999998 0.32853201 0.41249996 0.32853183
		 0.42499995 0.32853258 0.43749994 0.32853195 0.44999993 0.32853198 0.46249992 0.32853082
		 0.47499993 0.32853261 0.48749989 0.32853189 0.49999988 0.32853198 0.51249987 0.32853222
		 0.52499986 0.32853246 0.5374999 0.32853079 0.54999989 0.32853091 0.56249982 0.32853264
		 0.57499981 0.32853082 0.5874998 0.32853225 0.59999979 0.32853219 0.61249977 0.3285321
		 0.62499976 0.32853195 0.375 0.32853246 0.62499976 0.3125 0.64860266 0.10796607 0.375
		 0.3125 0.64130044 0.11004051 0.62640899 0.064408496 0.38749999 0.3125 0.62019736
		 0.068623349 0.59184152 0.029841021 0.39999998 0.3125 0.58732837 0.035754576 0.54828393
		 0.0076473355 0.41249996 0.3125 0.54591125 0.014651495 0.5 -7.4505806e-008 0.42499995
		 0.3125 0.5 0.0073798853 0.45171607 0.0076473504 0.43749994 0.3125 0.45408881 0.014651514
		 0.40815851 0.029841051 0.44999993 0.3125 0.4126716 0.035754576 0.37359107 0.064408526
		 0.46249992 0.3125 0.37980279 0.068623401 0.3513974 0.10796608 0.4749999 0.3125 0.35869971
		 0.11004052 0.34374997 0.15625 0.48749989 0.3125 0.35142815 0.15595177 0.3513974 0.20453392
		 0.49999988 0.3125 0.35869971 0.20186305 0.37359107 0.24809146 0.51249987 0.3125 0.37980279
		 0.2432801 0.40815854 0.28265893 0.52499986 0.3125 0.4126716 0.27614895 0.4517161
		 0.3048526 0.53749985 0.3125 0.45408881 0.29725197 0.5 0.3125 0.54999983 0.3125 0.5
		 0.30452359 0.54828387 0.3048526 0.56249982 0.3125 0.54591125 0.29725206 0.59184146
		 0.28265893 0.57499981 0.3125 0.58732837 0.27614897 0.62640893 0.24809146 0.5874998
		 0.3125 0.62019724 0.24328017 0.6486026 0.2045339 0.59999979 0.3125 0.6413002 0.20186298
		 0.65625 0.15625 0.61249977 0.3125 0.64857185 0.15595175 0.375 0.66463947 0.38749999
		 0.66310376 0.375 0.66310376 0.39999998 0.66310376 0.38749999 0.66310376 0.41249996
		 0.66310376 0.39999998 0.66310376 0.42499995 0.66310376 0.41249996 0.66310376 0.43749991
		 0.66310376 0.42499995 0.66310376 0.4499999 0.66310376 0.43749994 0.66310376 0.46249992
		 0.66310376 0.44999993 0.66310376 0.4749999 0.66310376 0.46249995 0.66310376 0.48749989
		 0.66310376 0.4749999 0.66310376 0.49999988 0.66310376 0.48749989 0.66310376 0.51249987
		 0.66310376 0.49999988 0.66310376 0.52499986 0.66310376 0.51249987 0.66310376 0.5374999
		 0.66310376 0.52499986 0.66310376 0.54999983 0.66310376 0.53749985 0.6631037 0.56249982
		 0.66310376 0.54999983 0.66310376 0.57499981 0.66310376 0.56249982 0.66310382 0.5874998
		 0.66310376 0.57499981 0.66310376 0.59999979 0.66310376 0.5874998 0.66310376 0.61249977
		 0.66310376 0.59999979 0.66310376 0.62499976 0.66310376 0.61249977 0.6631037 0.6249997
		 0.66310376 0.6249997 0.66364413 0.375 0.66364413 0.62499976 0.66415751 0.375 0.66415751
		 0.38749999 0.66415846 0.38749999 0.66364604 0.39999998 0.6641584 0.39999998 0.66364598
		 0.41249996 0.6641584 0.41249996 0.66364604 0.42499995 0.6641584 0.42499995 0.66364604
		 0.43749994 0.66415846 0.43749994 0.66364604 0.44999993 0.6641584 0.4499999 0.66364598
		 0.46249989 0.6641584 0.46249992 0.66364598 0.4749999 0.6641584 0.4749999 0.66364604
		 0.48749989 0.66415846 0.48749989 0.66364604 0.49999988 0.6641584 0.49999988 0.66364604
		 0.51249987 0.66415846 0.51249987 0.66364604 0.52499986 0.6641584 0.52499986 0.66364598
		 0.5374999 0.6641584 0.53749985 0.66364604 0.54999983 0.66415846 0.54999983 0.66364604
		 0.56249982 0.66415852 0.56249982 0.6636461 0.57499981 0.6641584 0.57499981 0.66364604
		 0.5874998 0.6641584 0.5874998 0.66364598 0.59999979 0.6641584 0.59999979 0.66364604
		 0.61249977 0.6641584 0.61249977 0.66364598 0.375 0.68843985 0.38749999 0.68843985
		 0.38749999 0.68843985 0.40000001 0.68843985 0.39999998 0.68843985 0.41249993 0.68843991
		 0.41249996 0.68843985 0.42499995 0.68843979 0.42499995 0.68843985 0.43749991 0.68843985
		 0.43749994 0.68843985 0.44999993 0.68843985 0.44999993 0.68843985 0.46249995 0.68843985
		 0.46249992 0.68843985 0.4749999 0.68843985 0.4749999 0.68843985 0.48749989 0.68843985
		 0.48749989 0.68843985 0.49999988 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985
		 0.51249987 0.68843985 0.52499986 0.68843985 0.52499986 0.68843985 0.5374999 0.68843985
		 0.53749985 0.68843985 0.54999983 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985
		 0.56249982 0.68843985 0.57499981 0.68843985 0.57499981 0.68843985 0.5874998 0.68843985
		 0.5874998 0.68843985 0.59999979 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985
		 0.62499976 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.38749999 0.66463947
		 0.375 0.68686891 0.39999998 0.66463947 0.38749999 0.68686891 0.41249996 0.66463947
		 0.39999998 0.68686891 0.42499995 0.66463947 0.41249996 0.68686891 0.43749994 0.66463947
		 0.42499995 0.68686885 0.44999987 0.66463947 0.43749994 0.68686891 0.46249986 0.66463947
		 0.44999993 0.68686885 0.4749999 0.66463947 0.46249992 0.68686891 0.48749989 0.66463947
		 0.47499987 0.68686891 0.49999988 0.66463947 0.48749989 0.68686891;
	setAttr ".uvst[0].uvsp[250:433]" 0.51249987 0.66463953 0.49999988 0.68686891
		 0.52499986 0.66463947 0.51249987 0.68686897 0.5374999 0.66463947 0.52499986 0.68686891
		 0.54999983 0.66463947 0.53749985 0.68686891 0.56249982 0.66463953 0.54999983 0.68686891
		 0.57499981 0.66463947 0.56249982 0.68686891 0.5874998 0.66463947 0.57499981 0.68686897
		 0.59999979 0.66463947 0.5874998 0.68686891 0.61249977 0.66463947 0.59999973 0.68686891
		 0.62499976 0.66463947 0.61249977 0.68686885 0.375 0.68843985 0.62499976 0.68686885
		 0.38749999 0.68788332 0.38749999 0.68736029 0.62499976 0.68788445 0.375 0.68815887
		 0.62499976 0.68736058 0.375 0.68789387 0.39999998 0.68815893 0.39999998 0.68789518
		 0.41249996 0.68815899 0.41249996 0.68789506 0.42499995 0.68815887 0.42499995 0.68789494
		 0.43749994 0.68815899 0.43749994 0.68789518 0.44999993 0.68815899 0.44999993 0.68789512
		 0.46249992 0.68815899 0.46249992 0.68789518 0.4749999 0.68815899 0.4749999 0.68789512
		 0.48749989 0.68815893 0.48749989 0.687895 0.49999988 0.68815899 0.49999988 0.68789506
		 0.51249987 0.68815899 0.51249987 0.68789512 0.52499986 0.68815893 0.52499986 0.68789512
		 0.53749985 0.68815899 0.53749985 0.68789518 0.54999983 0.68815899 0.54999983 0.68789512
		 0.56249982 0.68815899 0.56249982 0.68789512 0.57499981 0.68815893 0.57499981 0.68789506
		 0.5874998 0.68815905 0.5874998 0.68789518 0.59999979 0.68815887 0.59999979 0.68789506
		 0.61249977 0.68788326 0.61249977 0.68736029 0.62640893 0.93559146 0.6486026 0.89203393
		 0.59184152 0.97015887 0.62640893 0.9355914 0.54828387 0.9923526 0.59184146 0.97015893
		 0.5 1 0.54828399 0.99235255 0.4517161 0.9923526 0.5 1 0.40815854 0.97015893 0.45171615
		 0.99235266 0.37359107 0.93559146 0.40815854 0.97015893 0.3513974 0.89203393 0.37359107
		 0.93559146 0.34374997 0.84375 0.3513974 0.89203393 0.3513974 0.79546607 0.34374997
		 0.84375 0.37359107 0.75190854 0.3513974 0.79546607 0.40815851 0.71734107 0.37359104
		 0.75190854 0.45171607 0.69514734 0.40815857 0.71734107 0.5 0.68749994 0.45171613
		 0.69514734 0.54828393 0.69514734 0.5 0.68749994 0.59184152 0.71734101 0.54828399
		 0.69514734 0.62640899 0.75190848 0.59184152 0.71734101 0.64860266 0.79546607 0.62640899
		 0.75190848 0.65625 0.84375 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375
		 0.62640887 0.93559152 0.6486026 0.89203393 0.59184146 0.97015893 0.62640893 0.93559146
		 0.54828399 0.99235255 0.59184146 0.97015893 0.5 1 0.54828387 0.9923526 0.45171601
		 0.99235255 0.5 1 0.40815863 0.97015899 0.4517161 0.9923526 0.37359095 0.93559128
		 0.40815854 0.97015893 0.35139742 0.89203393 0.37359107 0.93559146 0.34374997 0.84375
		 0.3513974 0.89203393 0.35139745 0.79546595 0.34374997 0.84375 0.37359107 0.75190854
		 0.3513974 0.79546607 0.4081586 0.71734101 0.37359107 0.75190854 0.45171607 0.69514734
		 0.40815851 0.71734107 0.5 0.68749994 0.45171607 0.69514734 0.54828393 0.69514734
		 0.5 0.68749994 0.59184152 0.71734101 0.54828393 0.69514734 0.62640899 0.75190848
		 0.59184158 0.71734107 0.64860266 0.79546607 0.62640899 0.75190854 0.65625 0.84375
		 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375 0.64854199 0.89201421
		 0.64854008 0.89201355 0.62635726 0.93555391 0.62635559 0.93555278 0.59180397 0.97010726
		 0.59180272 0.97010565 0.54826427 0.99229181 0.54826361 0.9922899 0.5 0.99993616 0.5
		 0.99993414 0.45173576 0.99229187 0.45173642 0.9922899 0.40819615 0.97010732 0.40819737
		 0.97010571 0.37364268 0.93555385 0.37364432 0.93555266 0.35145813 0.89201421 0.35146007
		 0.89201355 0.34381381 0.84375 0.34381586 0.84375 0.35145813 0.79548573 0.3514601
		 0.79548633 0.37364265 0.75194609 0.37364438 0.75194722 0.40819615 0.71739268 0.40819734
		 0.71739429 0.45173576 0.69520807 0.45173645 0.69520998 0.5 0.68756378 0.5 0.6875658
		 0.54826427 0.69520807 0.54826361 0.69520998 0.59180403 0.71739268 0.59180278 0.71739429
		 0.62635732 0.75194603 0.62635571 0.75194722 0.64854205 0.79548579 0.64854014 0.79548645
		 0.65620756 0.84375 0.65622658 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 382 ".vt";
	setAttr ".vt[0:165]"  1.42563295 1.33444607 -0.46321517 1.21271753 1.33444607 -0.88108784
		 0.88109303 1.33444607 -1.21271324 0.46321726 1.33444607 -1.42562973 2.8610229e-006 1.33444607 -1.49899578
		 -0.46321487 1.33444607 -1.42562973 -0.8810873 1.33444607 -1.212713 -1.21270943 1.33444607 -0.8810876
		 -1.42562485 1.33444607 -0.4632149 -1.49898911 1.33444607 8.9227264e-008 -1.42562485 1.33444607 0.46321523
		 -1.21270943 1.33444607 0.88108778 -0.8810873 1.33444607 1.21271241 -0.46321487 1.33444607 1.42562973
		 2.8610229e-006 1.33444607 1.49899507 0.46321726 1.33444607 1.42562962 0.88109255 1.33444607 1.21271241
		 1.21271753 1.33444607 0.88108766 1.42563295 1.33444607 0.46321511 1.49899721 1.33444607 8.9227264e-008
		 2.8610229e-006 -1 0 0.86489248 -0.79374325 -0.28101873 0.73572159 -0.79374325 -0.53452981
		 2.8610229e-006 -0.79374325 2.4323526e-008 0.53453493 -0.79374325 -0.73571658 0.28102064 -0.79374325 -0.86488682
		 2.8610229e-006 -0.79374325 -0.90939558 -0.28101492 -0.79374325 -0.86488682 -0.53452969 -0.79374325 -0.73571599
		 -0.73571301 -0.79374325 -0.53452951 -0.86488438 -0.79374325 -0.28101861 -0.90939045 -0.79374319 3.1713974e-008
		 -0.86488438 -0.79374319 0.28101885 -0.73571301 -0.79374319 0.53452951 -0.53452969 -0.79374319 0.73571599
		 -0.28101492 -0.79374319 0.86488682 2.8610229e-006 -0.79374325 0.90939504 0.28102064 -0.79374319 0.86488682
		 0.5345335 -0.79374319 0.73571599 0.73572159 -0.79374319 0.53452951 0.86489248 -0.79374319 0.28101867
		 0.90940046 -0.79374325 1.3047155e-008 1.39365196 0.86521232 -0.45282313 1.1855092 0.86521232 -0.86132091
		 0.86132622 0.86521232 -1.18550658 0.45282841 0.86521232 -1.39364624 2.8610229e-006 0.86521232 -1.46536624
		 -0.45281887 0.86521232 -1.39364624 -0.86131573 0.86521232 -1.18550622 -1.1855011 0.86521232 -0.86132079
		 -1.39363956 0.86521232 -0.45282283 -1.46536255 0.86521232 8.3213898e-008 -1.39363956 0.86521232 0.45282313
		 -1.1855011 0.86521232 0.86132091 -0.86131573 0.86521232 1.18550599 -0.45281887 0.86521232 1.39364624
		 2.8610229e-006 0.86521232 1.46536601 0.45282793 0.86521232 1.39364624 0.86132574 0.86521232 1.18550551
		 1.1855092 0.86521232 0.86132079 1.39364767 0.86521232 0.45282307 1.46537066 0.86521232 8.3213898e-008
		 0.97129774 -0.91470987 -0.31559283 0.94874239 -0.95866162 -0.30826336 0.90754414 -0.98911774 -0.29487693
		 0.855968 -1 -0.2781187 0.82623386 -0.91470993 -0.6002934 0.80704784 -0.95866162 -0.58635175
		 0.77200222 -0.98911774 -0.56088918 0.72812748 -1 -0.52901357 0.6002984 -0.91470987 -0.82623309
		 0.58635712 -0.95866162 -0.80704379 0.56089449 -0.98911774 -0.77199805 0.52901888 -1 -0.72812486
		 0.31559801 -0.91470987 -0.97129512 0.30826712 -0.95866162 -0.94873714 0.29487896 -0.98911774 -0.90753806
		 0.27812338 -1 -0.8559621 2.8610229e-006 -0.91470987 -1.021279812 2.8610229e-006 -0.95866162 -0.99756092
		 2.8610229e-006 -0.98911774 -0.95424163 2.8610229e-006 -1 -0.90001106 -0.31559038 -0.91470993 -0.971295
		 -0.30826044 -0.95866162 -0.94873697 -0.29487324 -0.98911774 -0.90753788 -0.27811241 -1 -0.85596156
		 -0.60029316 -0.91470987 -0.82623267 -0.58634663 -0.95866162 -0.80704373 -0.56088448 -0.98911774 -0.77199745
		 -0.52901363 -1 -0.72812426 -0.826231 -0.91470987 -0.60029334 -0.80704308 -0.95866162 -0.58635169
		 -0.7719965 -0.98911774 -0.56088918 -0.72812462 -1 -0.52901351 -0.97128963 -0.91470987 -0.31559283
		 -0.94873428 -0.95866162 -0.30826333 -0.90753746 -0.98911774 -0.29487702 -0.85596085 -1 -0.27811921
		 -1.021273613 -0.91470987 3.805102e-009 -0.99755764 -0.95866162 1.8442573e-009 -0.95423889 -0.98911774 4.8549836e-010
		 -0.90000725 -1 -2.2201951e-017 -0.97128963 -0.91470987 0.31559283 -0.94873428 -0.95866162 0.30826333
		 -0.90753746 -0.98911774 0.29487702 -0.85596085 -1 0.27811921 -0.826231 -0.91470987 0.60029328
		 -0.80704308 -0.95866162 0.58635163 -0.7719965 -0.98911774 0.56088918 -0.72812462 -1 0.52901345
		 -0.60029316 -0.91470987 0.82623267 -0.58634663 -0.95866162 0.80704367 -0.56088448 -0.98911774 0.77199745
		 -0.52901363 -1 0.72812426 -0.31559038 -0.91470993 0.97129476 -0.30826044 -0.95866162 0.94873673
		 -0.29487324 -0.98911774 0.90753746 -0.27811241 -1 0.85596156 2.8610229e-006 -0.91470987 1.021279335
		 2.8610229e-006 -0.95866162 0.9975608 2.8610229e-006 -0.98911774 0.95424145 2.8610229e-006 -1 0.90001106
		 0.31559753 -0.91470987 0.97129476 0.30826712 -0.95866162 0.94873673 0.29487896 -0.98911774 0.90753746
		 0.27812004 -1 0.85596156 0.60029793 -0.91470987 0.82623267 0.58635664 -0.95866162 0.80704367
		 0.56089401 -0.98911774 0.77199745 0.5290184 -1 0.72812426 0.82623386 -0.91470993 0.60029316
		 0.80704784 -0.95866162 0.58635151 0.77200222 -0.98911774 0.56088918 0.72812748 -1 0.5290134
		 0.97129774 -0.91470987 0.3155928 0.9487381 -0.95866162 0.30826315 0.90754271 -0.98911774 0.29487672
		 0.85596657 -1 0.27811867 1.021281719 -0.91470987 3.805102e-009 0.99756575 -0.95866162 -2.4780354e-008
		 0.95424461 -0.98911774 -1.0071276e-007 0.90001535 -1 -2.0874633e-007 1.42490864 0.86441112 -0.46297929
		 1.44089222 0.86781001 -0.46817297 1.45301914 0.87795174 -0.47211251 1.45818567 0.89224756 -0.47379145
		 1.24040747 0.89224756 -0.90120488 1.23601103 0.87795174 -0.89801139 1.22569799 0.86781001 -0.89051795
		 1.21210098 0.86441112 -0.88063872 0.90120983 0.89224756 -1.24040222 0.89801502 0.87795174 -1.23600626
		 0.89052296 0.86781001 -1.22569287 0.88064384 0.86441112 -1.21209562 0.47379684 0.89224756 -1.45818019
		 0.47211552 0.87795174 -1.45301294 0.46817732 0.86781001 -1.44088817 0.46298218 0.86441112 -1.42490375
		 2.8610229e-006 0.89224756 -1.53322101 2.8610229e-006 0.87795174 -1.52778733 2.8610229e-006 0.86781001 -1.51503921
		 2.8610229e-006 0.86441112 -1.49823213 -0.47378826 0.89224756 -1.45818019 -0.47210884 0.87795174 -1.45301294
		 -0.46816921 0.86781001 -1.44088817 -0.4629755 0.86441112 -1.42490375;
	setAttr ".vt[166:331]" -0.90119743 0.89224756 -1.24040139 -0.8980093 0.87795174 -1.23600626
		 -0.89051533 0.86781001 -1.22569251 -0.88063717 0.86441112 -1.21209478 -1.2403965 0.89224756 -0.90120476
		 -1.23600292 0.87795174 -0.89801133 -1.22568989 0.86781001 -0.89051741 -1.21209335 0.86441112 -0.88063872
		 -1.45817661 0.89224756 -0.47379112 -1.45300579 0.87795174 -0.47211215 -1.4408865 0.86781001 -0.46817264
		 -1.42490196 0.86441112 -0.46297896 -1.53321838 0.89224756 9.8020692e-008 -1.52778053 0.87795174 9.7921699e-008
		 -1.51503658 0.86781001 9.8037063e-008 -1.49822998 0.86441112 9.833721e-008 -1.45817661 0.89224756 0.47379145
		 -1.45300579 0.87795174 0.47211251 -1.4408865 0.86781001 0.46817297 -1.42490196 0.86441112 0.46297929
		 -1.2403965 0.89224756 0.90120488 -1.23600292 0.87795174 0.89801139 -1.22568989 0.86781001 0.89051783
		 -1.21209335 0.86441112 0.88063872 -0.90119743 0.89224756 1.24040139 -0.8980093 0.87795174 1.2360059
		 -0.89051533 0.86781001 1.22569168 -0.88063717 0.86441112 1.21209478 -0.47378826 0.89224756 1.45818007
		 -0.47210884 0.87795174 1.45301294 -0.46816921 0.86781001 1.44088757 -0.4629755 0.86441112 1.42490363
		 2.8610229e-006 0.89224756 1.53322089 2.8610229e-006 0.87795174 1.52778733 2.8610229e-006 0.86781001 1.51503897
		 2.8610229e-006 0.86441112 1.49823189 0.47379637 0.89224756 1.45817995 0.47211552 0.87795174 1.45301282
		 0.46817732 0.86781001 1.44088757 0.46298218 0.86441112 1.42490304 0.90120935 0.89224756 1.24040115
		 0.89801502 0.87795174 1.23600566 0.89052248 0.86781001 1.22569168 0.88064337 0.86441112 1.21209455
		 1.24040461 0.89224756 0.90120476 1.23601103 0.87795174 0.89801133 1.22569799 0.86781001 0.89051777
		 1.21209812 0.86441112 0.88063872 1.45818377 0.89224756 0.47379136 1.45301676 0.87795174 0.47211242
		 1.44089222 0.86781001 0.46817291 1.42490768 0.86441112 0.46297923 1.53322601 0.89224756 9.8020685e-008
		 1.52779293 0.87795174 8.7899743e-008 1.51504326 0.86781001 6.0506437e-008 1.49823475 0.86441112 2.2840377e-008
		 1.23991776 1.33524764 -0.90084934 1.2530694 1.3316251 -0.91040558 1.2624855 1.32107341 -0.91724694
		 1.26534891 1.30675614 -0.91932589 1.45760965 1.33524764 -0.47360447 1.47307205 1.3316251 -0.47862846
		 1.48414135 1.32107341 -0.48222515 1.48750448 1.30675614 -0.48331812 0.90085316 1.33524764 -1.23991215
		 0.91040945 1.3316251 -1.25306535 0.91725063 1.32107341 -1.26248169 0.91932821 1.30675614 -1.26534307
		 0.47360992 1.33524764 -1.45760477 0.47863388 1.33162534 -1.47306669 0.48223066 1.32107341 -1.48413658
		 0.48332357 1.30675614 -1.48750043 2.8610229e-006 1.33524764 -1.53261542 2.8610229e-006 1.3316251 -1.54887342
		 2.8610229e-006 1.32107341 -1.5605129 2.8610229e-006 1.30675614 -1.5640496 -0.47359943 1.33524764 -1.45760477
		 -0.4786272 1.3316251 -1.47306669 -0.48222351 1.32107341 -1.48413658 -0.48331642 1.30675614 -1.48750043
		 -0.90084839 1.33524764 -1.23991215 -0.91040421 1.3316251 -1.25306535 -0.91724586 1.32107341 -1.26248169
		 -0.91932106 1.30675614 -1.26534307 -1.23990726 1.33524764 -0.90084922 -1.25306034 1.33162534 -0.9104054
		 -1.26247978 1.32107341 -0.9172467 -1.26534081 1.30675614 -0.91932577 -1.45759964 1.33524764 -0.47360426
		 -1.4730587 1.3316251 -0.47862822 -1.484128 1.32107341 -0.48222488 -1.48749542 1.30675614 -0.48331785
		 -1.53260708 1.33524764 1.0484373e-007 -1.54886723 1.33162534 1.0433673e-007 -1.56050491 1.32107341 1.0385151e-007
		 -1.56404495 1.30675614 1.0353334e-007 -1.45759964 1.33524764 0.47360468 -1.4730587 1.3316251 0.47862861
		 -1.484128 1.32107341 0.48222521 -1.48749542 1.30675614 0.48331824 -1.23990726 1.33524764 0.90084934
		 -1.25306034 1.33162534 0.9104057 -1.26247978 1.32107341 0.91724694 -1.26534081 1.30675614 0.91932589
		 -0.90084839 1.33524764 1.23991203 -0.91040421 1.3316251 1.25306511 -0.91724586 1.32107341 1.26248133
		 -0.91932106 1.30675614 1.26534283 -0.47359943 1.33524764 1.45760465 -0.4786272 1.3316251 1.47306657
		 -0.48222351 1.32107341 1.48413646 -0.48331642 1.30675614 1.48750043 2.8610229e-006 1.33524764 1.53261542
		 2.8610229e-006 1.3316251 1.54887342 2.8610229e-006 1.32107341 1.5605129 2.8610229e-006 1.30675614 1.5640496
		 0.47360992 1.33524764 1.45760453 0.47863388 1.33162534 1.47306645 0.48223066 1.32107341 1.48413634
		 0.48332357 1.30675614 1.48750031 0.90085316 1.33524764 1.23991203 0.91040945 1.3316251 1.25306499
		 0.91725063 1.32107341 1.26248121 0.91932821 1.30675614 1.26534271 1.23991537 1.33524764 0.90084922
		 1.2530694 1.3316251 0.9104054 1.2624855 1.32107341 0.91724682 1.26534796 1.30675614 0.91932589
		 1.45760918 1.33524764 0.47360441 1.47307014 1.3316251 0.47862846 1.48414087 1.32107341 0.48222515
		 1.48750448 1.30675614 0.48331812 1.53261948 1.33524764 8.8115613e-008 1.54887819 1.33162534 9.5848442e-008
		 1.56051731 1.32107341 1.0155723e-007 1.56405306 1.30675614 1.0353334e-007 1.29838896 1.33444607 -0.42187101
		 1.27576256 1.32949889 -0.41451934 1.25800467 1.31572211 -0.40874937 1.24892759 1.29606164 -0.4058001
		 1.10447693 1.33444607 -0.8024466 1.085229874 1.32949889 -0.78846288 1.070124149 1.31572211 -0.77748775
		 1.062402725 1.29606164 -0.77187777 0.80245066 1.33444607 -1.10447288 0.78846693 1.32949889 -1.085225821
		 0.77749157 1.31572211 -1.070119977 0.77188158 1.29606164 -1.062398553 0.42187643 1.33444607 -1.29838562
		 0.41452456 1.32949889 -1.27575958 0.40875483 1.31572211 -1.25800133 0.40580559 1.29606164 -1.24892437
		 2.8610229e-006 1.33444607 -1.3652029 2.8610229e-006 1.32949889 -1.34141243 2.8610229e-006 1.31572211 -1.32274055
		 2.8610229e-006 1.29606164 -1.31319606 -0.42186928 1.33444607 -1.29838562 -0.41451836 1.32949889 -1.27575958
		 -0.4087472 1.31572211 -1.25800133 -0.40579748 1.29606164 -1.24892437 -0.80244541 1.33444607 -1.10447228
		 -0.78845978 1.32949889 -1.085224986 -0.77748489 1.31572211 -1.070119143 -0.77187538 1.29606164 -1.062397838
		 -1.10447121 1.33444607 -0.80244642 -1.085224152 1.32949889 -0.78846252;
	setAttr ".vt[332:381]" -1.070116997 1.31572211 -0.77748752 -1.062396049 1.29606187 -0.77187753
		 -1.29838371 1.33444607 -0.42187077 -1.27575874 1.32949889 -0.41451913 -1.25800037 1.31572211 -0.40874916
		 -1.2489233 1.29606187 -0.40579984 -1.365201 1.33444607 1.4558977e-007 -1.34141064 1.32949889 1.2206371e-007
		 -1.32273865 1.31572211 1.0523855e-007 -1.31319427 1.29606187 9.8728684e-008 -1.29838276 1.33444607 0.42187101
		 -1.27575779 1.32949889 0.41451937 -1.25799942 1.31572211 0.40874943 -1.24892139 1.29606164 0.40580013
		 -1.10447121 1.33444607 0.80244654 -1.085226059 1.32949889 0.78846282 -1.07011795 1.31572211 0.7774877
		 -1.062397957 1.29606187 0.77187771 -0.80244541 1.33444607 1.10447228 -0.78845978 1.32949889 1.085224986
		 -0.77748489 1.31572211 1.070119143 -0.77187538 1.29606164 1.062397838 -0.42186928 1.33444607 1.29838538
		 -0.41451836 1.32949889 1.27575934 -0.4087472 1.31572211 1.25800133 -0.40579748 1.29606164 1.24892414
		 2.8610229e-006 1.33444607 1.3652029 2.8610229e-006 1.32949889 1.34141243 2.8610229e-006 1.31572211 1.32274055
		 2.8610229e-006 1.29606164 1.31319606 0.42187595 1.33444607 1.29838538 0.41452456 1.32949889 1.27575934
		 0.40875435 1.31572211 1.25800133 0.40580511 1.29606164 1.24892414 0.80245066 1.33444607 1.10447228
		 0.78846693 1.32949889 1.085225224 0.77749157 1.31572211 1.070119143 0.77188158 1.29606164 1.062397838
		 1.10447693 1.33444607 0.80244654 1.085229874 1.32949889 0.7884627 1.070124149 1.31572211 0.77748758
		 1.062402725 1.29606164 0.77187759 1.29838896 1.33444607 0.42187077 1.27576256 1.32949889 0.41451913
		 1.25800467 1.31572211 0.40874922 1.24892759 1.29606164 0.40579987 1.36520576 1.33444607 7.5683509e-008
		 1.34141493 1.32949889 7.4094409e-008 1.32274294 1.31572211 7.2690582e-008 1.31319809 1.29606164 7.177325e-008;
	setAttr -s 780 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 8 1 8 9 1
		 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1 17 18 1 18 19 1 19 0 1
		 21 22 0 22 23 1 21 23 1 22 24 0 24 23 1 24 25 0 25 23 1 25 26 0 26 23 1 26 27 0 27 23 1
		 27 28 0 28 23 1 28 29 0 29 23 1 29 30 0 30 23 1 30 31 0 31 23 1 31 32 0 32 23 1 32 33 0
		 33 23 1 33 34 0 34 23 1 34 35 0 35 23 1 35 36 0 36 23 1 36 37 0 37 23 1 37 38 0 38 23 1
		 38 39 0 39 23 1 39 40 0 40 23 1 40 41 0 41 23 1 41 21 0 42 43 0 43 44 0 44 45 0 45 46 0
		 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0
		 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0 67 66 1 66 62 1 68 67 1 65 69 1 69 68 1 65 64 1
		 141 65 1 64 63 1 63 62 1 62 138 1 71 70 1 70 66 1 72 71 1 69 73 1 73 72 1 75 74 1
		 74 70 1 76 75 1 73 77 1 77 76 1 79 78 1 78 74 1 80 79 1 77 81 1 81 80 1 83 82 1 82 78 1
		 84 83 1 81 85 1 85 84 1 87 86 1 86 82 1 88 87 1 85 89 1 89 88 1 91 90 1 90 86 1 92 91 1
		 89 93 1 93 92 1 95 94 1 94 90 1 96 95 1 93 97 1 97 96 1 99 98 1 98 94 1 100 99 1
		 97 101 1 101 100 1 103 102 1 102 98 1 104 103 1 101 105 1 105 104 1 107 106 1 106 102 1
		 108 107 1 105 109 1 109 108 1 111 110 1 110 106 1 112 111 1 109 113 1 113 112 1 115 114 1
		 114 110 1 116 115 1 113 117 1 117 116 1 119 118 1 118 114 1 120 119 1 117 121 1 121 120 1
		 123 122 1 122 118 1 124 123 1 121 125 1 125 124 1 127 126 1 126 122 1 128 127 1 125 129 1
		 129 128 1 131 130 1;
	setAttr ".ed[166:331]" 130 126 1 132 131 1 129 133 1 133 132 1 135 134 1 134 130 1
		 136 135 1 133 137 1 137 136 1 139 138 1 138 134 1 140 139 1 137 141 1 141 140 1 65 20 1
		 20 69 1 20 73 1 20 77 1 20 81 1 20 85 1 20 89 1 20 93 1 20 97 1 20 101 1 20 105 1
		 20 109 1 20 113 1 20 117 1 20 121 1 20 125 1 20 129 1 20 133 1 20 137 1 20 141 1
		 66 43 1 42 62 1 70 44 1 74 45 1 78 46 1 82 47 1 86 48 1 90 49 1 94 50 1 98 51 1 102 52 1
		 106 53 1 110 54 1 114 55 1 118 56 1 122 57 1 126 58 1 130 59 1 134 60 1 138 61 1
		 64 68 1 63 67 1 68 72 1 67 71 1 72 76 1 71 75 1 76 80 1 75 79 1 80 84 1 79 83 1 84 88 1
		 83 87 1 88 92 1 87 91 1 92 96 1 91 95 1 96 100 1 95 99 1 100 104 1 99 103 1 104 108 1
		 103 107 1 108 112 1 107 111 1 112 116 1 111 115 1 116 120 1 115 119 1 120 124 1 119 123 1
		 124 128 1 123 127 1 128 132 1 127 131 1 132 136 1 131 135 1 136 140 1 135 139 1 64 140 1
		 63 139 1 221 142 1 145 218 1 145 144 1 144 147 1 147 146 1 146 145 1 144 143 1 143 148 1
		 148 147 1 143 142 1 142 149 1 149 148 1 151 150 1 150 146 1 152 151 1 149 153 1 153 152 1
		 155 154 1 154 150 1 156 155 1 153 157 1 157 156 1 159 158 1 158 154 1 160 159 1 157 161 1
		 161 160 1 163 162 1 162 158 1 164 163 1 161 165 1 165 164 1 167 166 1 166 162 1 168 167 1
		 165 169 1 169 168 1 171 170 1 170 166 1 172 171 1 169 173 1 173 172 1 175 174 1 174 170 1
		 176 175 1 173 177 1 177 176 1 179 178 1 178 174 1 180 179 1 177 181 1 181 180 1 183 182 1
		 182 178 1 184 183 1 181 185 1 185 184 1 187 186 1 186 182 1 188 187 1 185 189 1 189 188 1
		 191 190 1 190 186 1 192 191 1 189 193 1 193 192 1 195 194 1 194 190 1 196 195 1 193 197 1
		 197 196 1;
	setAttr ".ed[332:497]" 199 198 1 198 194 1 200 199 1 197 201 1 201 200 1 203 202 1
		 202 198 1 204 203 1 201 205 1 205 204 1 207 206 1 206 202 1 208 207 1 205 209 1 209 208 1
		 211 210 1 210 206 1 212 211 1 209 213 1 213 212 1 215 214 1 214 210 1 216 215 1 213 217 1
		 217 216 1 219 218 1 218 214 1 220 219 1 217 221 1 221 220 1 43 149 1 142 42 1 44 153 1
		 45 157 1 46 161 1 47 165 1 48 169 1 49 173 1 50 177 1 51 181 1 52 185 1 53 189 1
		 54 193 1 55 197 1 56 201 1 57 205 1 58 209 1 59 213 1 60 217 1 61 221 1 148 152 1
		 147 151 1 152 156 1 151 155 1 156 160 1 155 159 1 160 164 1 159 163 1 164 168 1 163 167 1
		 168 172 1 167 171 1 172 176 1 171 175 1 176 180 1 175 179 1 180 184 1 179 183 1 184 188 1
		 183 187 1 188 192 1 187 191 1 192 196 1 191 195 1 196 200 1 195 199 1 200 204 1 199 203 1
		 204 208 1 203 207 1 208 212 1 207 211 1 212 216 1 211 215 1 216 220 1 215 219 1 143 220 1
		 144 219 1 231 230 1 230 222 1 232 231 1 225 233 1 233 232 1 225 224 1 229 225 1 224 223 1
		 223 222 1 222 226 1 229 228 1 301 229 1 228 227 1 227 226 1 226 298 1 235 234 1 234 230 1
		 236 235 1 233 237 1 237 236 1 239 238 1 238 234 1 240 239 1 237 241 1 241 240 1 243 242 1
		 242 238 1 244 243 1 241 245 1 245 244 1 247 246 1 246 242 1 248 247 1 245 249 1 249 248 1
		 251 250 1 250 246 1 252 251 1 249 253 1 253 252 1 255 254 1 254 250 1 256 255 1 253 257 1
		 257 256 1 259 258 1 258 254 1 260 259 1 257 261 1 261 260 1 263 262 1 262 258 1 264 263 1
		 261 265 1 265 264 1 267 266 1 266 262 1 268 267 1 265 269 1 269 268 1 271 270 1 270 266 1
		 272 271 1 269 273 1 273 272 1 275 274 1 274 270 1 276 275 1 273 277 1 277 276 1 279 278 1
		 278 274 1 280 279 1 277 281 1 281 280 1 283 282 1 282 278 1 284 283 1;
	setAttr ".ed[498:663]" 281 285 1 285 284 1 287 286 1 286 282 1 288 287 1 285 289 1
		 289 288 1 291 290 1 290 286 1 292 291 1 289 293 1 293 292 1 295 294 1 294 290 1 296 295 1
		 293 297 1 297 296 1 299 298 1 298 294 1 300 299 1 297 301 1 301 300 1 0 226 1 222 1 1
		 230 2 1 234 3 1 238 4 1 242 5 1 246 6 1 250 7 1 254 8 1 258 9 1 262 10 1 266 11 1
		 270 12 1 274 13 1 278 14 1 282 15 1 286 16 1 290 17 1 294 18 1 298 19 1 146 225 1
		 229 145 1 150 233 1 154 237 1 158 241 1 162 245 1 166 249 1 170 253 1 174 257 1 178 261 1
		 182 265 1 186 269 1 190 273 1 194 277 1 198 281 1 202 285 1 206 289 1 210 293 1 214 297 1
		 218 301 1 224 232 0 223 231 0 224 228 0 223 227 0 232 236 0 231 235 0 236 240 0 235 239 0
		 240 244 0 239 243 0 244 248 0 243 247 0 248 252 0 247 251 0 252 256 0 251 255 0 256 260 0
		 255 259 0 260 264 0 259 263 0 264 268 0 263 267 0 268 272 0 267 271 0 272 276 0 271 275 0
		 276 280 0 275 279 0 280 284 0 279 283 0 284 288 0 283 287 0 288 292 0 287 291 0 292 296 0
		 291 295 0 296 300 0 295 299 0 228 300 0 227 299 0 379 378 1 378 302 1 380 379 1 305 381 1
		 381 380 1 305 304 1 309 305 1 304 303 1 303 302 1 302 306 1 309 308 1 313 309 1 308 307 1
		 307 306 1 306 310 1 313 312 1 317 313 1 312 311 1 311 310 1 310 314 1 317 316 1 321 317 1
		 316 315 1 315 314 1 314 318 1 321 320 1 325 321 1 320 319 1 319 318 1 318 322 1 325 324 1
		 329 325 1 324 323 1 323 322 1 322 326 1 329 328 1 333 329 1 328 327 1 327 326 1 326 330 1
		 333 332 1 337 333 1 332 331 1 331 330 1 330 334 1 337 336 1 341 337 1 336 335 1 335 334 1
		 334 338 1 341 340 1 345 341 1 340 339 1 339 338 1 338 342 1 345 344 1 349 345 1 344 343 1
		 343 342 1 342 346 1 349 348 1 353 349 1 348 347 1 347 346 1;
	setAttr ".ed[664:779]" 346 350 1 353 352 1 357 353 1 352 351 1 351 350 1 350 354 1
		 357 356 1 361 357 1 356 355 1 355 354 1 354 358 1 361 360 1 365 361 1 360 359 1 359 358 1
		 358 362 1 365 364 1 369 365 1 364 363 1 363 362 1 362 366 1 369 368 1 373 369 1 368 367 1
		 367 366 1 366 370 1 373 372 1 377 373 1 372 371 1 371 370 1 370 374 1 377 376 1 381 377 1
		 376 375 1 375 374 1 374 378 1 1 306 1 302 0 1 2 310 1 3 314 1 4 318 1 5 322 1 6 326 1
		 7 330 1 8 334 1 9 338 1 10 342 1 11 346 1 12 350 1 13 354 1 14 358 1 15 362 1 16 366 1
		 17 370 1 18 374 1 19 378 1 309 22 1 21 305 1 313 24 1 317 25 1 321 26 1 325 27 1
		 329 28 1 333 29 1 337 30 1 341 31 1 345 32 1 349 33 1 353 34 1 357 35 1 361 36 1
		 365 37 1 369 38 1 373 39 1 377 40 1 381 41 1 304 380 1 303 379 1 304 308 1 303 307 1
		 308 312 1 307 311 1 312 316 1 311 315 1 316 320 1 315 319 1 320 324 1 319 323 1 324 328 1
		 323 327 1 328 332 1 327 331 1 332 336 1 331 335 1 336 340 1 335 339 1 340 344 1 339 343 1
		 344 348 1 343 347 1 348 352 1 347 351 1 352 356 1 351 355 1 356 360 1 355 359 1 360 364 1
		 359 363 1 364 368 1 363 367 1 368 372 1 367 371 1 372 376 1 371 375 1 376 380 1 375 379 1;
	setAttr -s 400 -ch 1560 ".fc[0:399]" -type "polyFaces" 
		f 3 20 21 -23
		mu 0 3 355 357 0
		f 3 23 24 -22
		mu 0 3 357 359 0
		f 3 25 26 -25
		mu 0 3 359 361 0
		f 3 27 28 -27
		mu 0 3 361 363 0
		f 3 29 30 -29
		mu 0 3 363 365 0
		f 3 31 32 -31
		mu 0 3 365 367 0
		f 3 33 34 -33
		mu 0 3 367 369 0
		f 3 35 36 -35
		mu 0 3 369 371 0
		f 3 37 38 -37
		mu 0 3 371 373 0
		f 3 39 40 -39
		mu 0 3 373 375 0
		f 3 41 42 -41
		mu 0 3 375 377 0
		f 3 43 44 -43
		mu 0 3 377 379 0
		f 3 45 46 -45
		mu 0 3 379 381 0
		f 3 47 48 -47
		mu 0 3 381 383 0
		f 3 49 50 -49
		mu 0 3 383 385 0
		f 3 51 52 -51
		mu 0 3 385 387 0
		f 3 53 54 -53
		mu 0 3 387 389 0
		f 3 55 56 -55
		mu 0 3 389 391 0
		f 3 57 58 -57
		mu 0 3 391 393 0
		f 3 59 22 -59
		mu 0 3 393 355 0
		f 3 -84 180 181
		mu 0 3 2 1 20
		f 3 -94 -182 182
		mu 0 3 3 2 20
		f 3 -99 -183 183
		mu 0 3 4 3 20
		f 3 -104 -184 184
		mu 0 3 5 4 20
		f 3 -109 -185 185
		mu 0 3 6 5 20
		f 3 -114 -186 186
		mu 0 3 7 6 20
		f 3 -119 -187 187
		mu 0 3 8 7 20
		f 3 -124 -188 188
		mu 0 3 9 8 20
		f 3 -129 -189 189
		mu 0 3 10 9 20
		f 3 -134 -190 190
		mu 0 3 11 10 20
		f 3 -139 -191 191
		mu 0 3 12 11 20
		f 3 -144 -192 192
		mu 0 3 13 12 20
		f 3 -149 -193 193
		mu 0 3 14 13 20
		f 3 -154 -194 194
		mu 0 3 15 14 20
		f 3 -159 -195 195
		mu 0 3 16 15 20
		f 3 -164 -196 196
		mu 0 3 17 16 20
		f 3 -169 -197 197
		mu 0 3 18 17 20
		f 3 -174 -198 198
		mu 0 3 19 18 20
		f 3 -179 -199 199
		mu 0 3 21 19 20
		f 3 -87 -200 -181
		mu 0 3 1 21 20
		f 4 -82 200 -61 201
		mu 0 4 43 22 106 23
		f 4 -92 202 -62 -201
		mu 0 4 22 24 108 106
		f 4 -97 203 -63 -203
		mu 0 4 24 25 110 108
		f 4 -102 204 -64 -204
		mu 0 4 25 26 112 110
		f 4 -107 205 -65 -205
		mu 0 4 26 27 114 112
		f 4 -112 206 -66 -206
		mu 0 4 27 28 116 114
		f 4 -117 207 -67 -207
		mu 0 4 28 29 118 116
		f 4 -122 208 -68 -208
		mu 0 4 29 30 120 118
		f 4 -127 209 -69 -209
		mu 0 4 30 31 122 120
		f 4 -132 210 -70 -210
		mu 0 4 31 32 124 122
		f 4 -137 211 -71 -211
		mu 0 4 32 33 126 124
		f 4 -142 212 -72 -212
		mu 0 4 33 34 128 126
		f 4 -147 213 -73 -213
		mu 0 4 34 35 130 128
		f 4 -152 214 -74 -214
		mu 0 4 35 36 132 130
		f 4 -157 215 -75 -215
		mu 0 4 36 37 134 132
		f 4 -162 216 -76 -216
		mu 0 4 37 38 136 134
		f 4 -167 217 -77 -217
		mu 0 4 38 39 138 136
		f 4 -172 218 -78 -218
		mu 0 4 39 40 140 138
		f 4 -177 219 -79 -219
		mu 0 4 40 41 142 140
		f 4 -90 -202 -80 -220
		mu 0 4 41 42 144 142
		f 4 -86 83 84 -221
		mu 0 4 47 1 2 50
		f 4 -89 221 80 81
		mu 0 4 43 46 49 22
		f 4 -88 220 82 -222
		mu 0 4 45 47 50 48
		f 4 -85 93 94 -223
		mu 0 4 50 2 3 53
		f 4 -81 223 90 91
		mu 0 4 22 49 52 24
		f 4 -83 222 92 -224
		mu 0 4 48 50 53 51
		f 4 -95 98 99 -225
		mu 0 4 53 3 4 56
		f 4 -91 225 95 96
		mu 0 4 24 52 55 25
		f 4 -93 224 97 -226
		mu 0 4 51 53 56 54
		f 4 -100 103 104 -227
		mu 0 4 56 4 5 59
		f 4 -96 227 100 101
		mu 0 4 25 55 58 26
		f 4 -98 226 102 -228
		mu 0 4 54 56 59 57
		f 4 -105 108 109 -229
		mu 0 4 59 5 6 62
		f 4 -101 229 105 106
		mu 0 4 26 58 61 27
		f 4 -103 228 107 -230
		mu 0 4 57 59 62 60
		f 4 -110 113 114 -231
		mu 0 4 62 6 7 65
		f 4 -106 231 110 111
		mu 0 4 27 61 64 28
		f 4 -108 230 112 -232
		mu 0 4 60 62 65 63
		f 4 -115 118 119 -233
		mu 0 4 65 7 8 68
		f 4 -111 233 115 116
		mu 0 4 28 64 67 29
		f 4 -113 232 117 -234
		mu 0 4 63 65 68 66
		f 4 -120 123 124 -235
		mu 0 4 68 8 9 71
		f 4 -116 235 120 121
		mu 0 4 29 67 70 30
		f 4 -118 234 122 -236
		mu 0 4 66 68 71 69
		f 4 -125 128 129 -237
		mu 0 4 71 9 10 74
		f 4 -121 237 125 126
		mu 0 4 30 70 73 31
		f 4 -123 236 127 -238
		mu 0 4 69 71 74 72
		f 4 -130 133 134 -239
		mu 0 4 74 10 11 77
		f 4 -126 239 130 131
		mu 0 4 31 73 76 32
		f 4 -128 238 132 -240
		mu 0 4 72 74 77 75
		f 4 -135 138 139 -241
		mu 0 4 77 11 12 80
		f 4 -131 241 135 136
		mu 0 4 32 76 79 33
		f 4 -133 240 137 -242
		mu 0 4 75 77 80 78
		f 4 -140 143 144 -243
		mu 0 4 80 12 13 83
		f 4 -136 243 140 141
		mu 0 4 33 79 82 34
		f 4 -138 242 142 -244
		mu 0 4 78 80 83 81
		f 4 -145 148 149 -245
		mu 0 4 83 13 14 86
		f 4 -141 245 145 146
		mu 0 4 34 82 85 35
		f 4 -143 244 147 -246
		mu 0 4 81 83 86 84
		f 4 -150 153 154 -247
		mu 0 4 86 14 15 89
		f 4 -146 247 150 151
		mu 0 4 35 85 88 36
		f 4 -148 246 152 -248
		mu 0 4 84 86 89 87
		f 4 -155 158 159 -249
		mu 0 4 89 15 16 92
		f 4 -151 249 155 156
		mu 0 4 36 88 91 37
		f 4 -153 248 157 -250
		mu 0 4 87 89 92 90
		f 4 -160 163 164 -251
		mu 0 4 92 16 17 95
		f 4 -156 251 160 161
		mu 0 4 37 91 94 38
		f 4 -158 250 162 -252
		mu 0 4 90 92 95 93
		f 4 -165 168 169 -253
		mu 0 4 95 17 18 98
		f 4 -161 253 165 166
		mu 0 4 38 94 97 39
		f 4 -163 252 167 -254
		mu 0 4 93 95 98 96
		f 4 -170 173 174 -255
		mu 0 4 98 18 19 101
		f 4 -166 255 170 171
		mu 0 4 39 97 100 40
		f 4 -168 254 172 -256
		mu 0 4 96 98 101 99
		f 4 -175 178 179 -257
		mu 0 4 101 19 21 104
		f 4 -171 257 175 176
		mu 0 4 40 100 103 41
		f 4 -173 256 177 -258
		mu 0 4 99 101 104 102
		f 4 85 258 -180 86
		mu 0 4 1 47 104 21
		f 4 87 259 -178 -259
		mu 0 4 47 45 102 104
		f 4 88 89 -176 -260
		mu 0 4 44 42 41 103
		f 4 262 263 264 265
		mu 0 4 105 150 151 230
		f 4 266 267 268 -264
		mu 0 4 150 148 152 151
		f 4 269 270 271 -268
		mu 0 4 148 107 109 152
		f 4 60 362 -271 363
		mu 0 4 23 106 109 107
		f 4 61 364 -276 -363
		mu 0 4 106 108 111 109
		f 4 62 365 -281 -365
		mu 0 4 108 110 113 111
		f 4 63 366 -286 -366
		mu 0 4 110 112 115 113
		f 4 64 367 -291 -367
		mu 0 4 112 114 117 115
		f 4 65 368 -296 -368
		mu 0 4 114 116 119 117
		f 4 66 369 -301 -369
		mu 0 4 116 118 121 119
		f 4 67 370 -306 -370
		mu 0 4 118 120 123 121
		f 4 68 371 -311 -371
		mu 0 4 120 122 125 123
		f 4 69 372 -316 -372
		mu 0 4 122 124 127 125
		f 4 70 373 -321 -373
		mu 0 4 124 126 129 127
		f 4 71 374 -326 -374
		mu 0 4 126 128 131 129
		f 4 72 375 -331 -375
		mu 0 4 128 130 133 131
		f 4 73 376 -336 -376
		mu 0 4 130 132 135 133
		f 4 74 377 -341 -377
		mu 0 4 132 134 137 135
		f 4 75 378 -346 -378
		mu 0 4 134 136 139 137
		f 4 76 379 -351 -379
		mu 0 4 136 138 141 139
		f 4 77 380 -356 -380
		mu 0 4 138 140 143 141
		f 4 78 381 -361 -381
		mu 0 4 140 142 145 143
		f 4 79 -364 -261 -382
		mu 0 4 142 144 146 145
		f 4 -272 275 276 -383
		mu 0 4 152 109 111 154
		f 4 -265 383 272 273
		mu 0 4 230 151 153 232
		f 4 -269 382 274 -384
		mu 0 4 151 152 154 153
		f 4 -277 280 281 -385
		mu 0 4 154 111 113 156
		f 4 -273 385 277 278
		mu 0 4 232 153 155 234
		f 4 -275 384 279 -386
		mu 0 4 153 154 156 155
		f 4 -282 285 286 -387
		mu 0 4 156 113 115 158
		f 4 -278 387 282 283
		mu 0 4 234 155 157 236
		f 4 -280 386 284 -388
		mu 0 4 155 156 158 157
		f 4 -287 290 291 -389
		mu 0 4 158 115 117 160
		f 4 -283 389 287 288
		mu 0 4 236 157 159 238
		f 4 -285 388 289 -390
		mu 0 4 157 158 160 159
		f 4 -292 295 296 -391
		mu 0 4 160 117 119 162
		f 4 -288 391 292 293
		mu 0 4 238 159 161 240
		f 4 -290 390 294 -392
		mu 0 4 159 160 162 161
		f 4 -297 300 301 -393
		mu 0 4 162 119 121 164
		f 4 -293 393 297 298
		mu 0 4 240 161 163 242
		f 4 -295 392 299 -394
		mu 0 4 161 162 164 163
		f 4 -302 305 306 -395
		mu 0 4 164 121 123 166
		f 4 -298 395 302 303
		mu 0 4 242 163 165 244
		f 4 -300 394 304 -396
		mu 0 4 163 164 166 165
		f 4 -307 310 311 -397
		mu 0 4 166 123 125 168
		f 4 -303 397 307 308
		mu 0 4 244 165 167 246
		f 4 -305 396 309 -398
		mu 0 4 165 166 168 167
		f 4 -312 315 316 -399
		mu 0 4 168 125 127 170
		f 4 -308 399 312 313
		mu 0 4 246 167 169 248
		f 4 -310 398 314 -400
		mu 0 4 167 168 170 169
		f 4 -317 320 321 -401
		mu 0 4 170 127 129 172
		f 4 -313 401 317 318
		mu 0 4 248 169 171 250
		f 4 -315 400 319 -402
		mu 0 4 169 170 172 171
		f 4 -322 325 326 -403
		mu 0 4 172 129 131 174
		f 4 -318 403 322 323
		mu 0 4 250 171 173 252
		f 4 -320 402 324 -404
		mu 0 4 171 172 174 173
		f 4 -327 330 331 -405
		mu 0 4 174 131 133 176
		f 4 -323 405 327 328
		mu 0 4 252 173 175 254
		f 4 -325 404 329 -406
		mu 0 4 173 174 176 175
		f 4 -332 335 336 -407
		mu 0 4 176 133 135 178
		f 4 -328 407 332 333
		mu 0 4 254 175 177 256
		f 4 -330 406 334 -408
		mu 0 4 175 176 178 177
		f 4 -337 340 341 -409
		mu 0 4 178 135 137 180
		f 4 -333 409 337 338
		mu 0 4 256 177 179 258
		f 4 -335 408 339 -410
		mu 0 4 177 178 180 179
		f 4 -342 345 346 -411
		mu 0 4 180 137 139 182
		f 4 -338 411 342 343
		mu 0 4 258 179 181 260
		f 4 -340 410 344 -412
		mu 0 4 179 180 182 181
		f 4 -347 350 351 -413
		mu 0 4 182 139 141 184
		f 4 -343 413 347 348
		mu 0 4 260 181 183 262
		f 4 -345 412 349 -414
		mu 0 4 181 182 184 183
		f 4 -352 355 356 -415
		mu 0 4 184 141 143 186
		f 4 -348 415 352 353
		mu 0 4 262 183 185 264
		f 4 -350 414 354 -416
		mu 0 4 183 184 186 185
		f 4 -357 360 361 -417
		mu 0 4 186 143 145 188
		f 4 -353 417 357 358
		mu 0 4 264 185 187 266
		f 4 -355 416 359 -418
		mu 0 4 185 186 188 187
		f 4 -270 418 -362 260
		mu 0 4 146 147 188 145
		f 4 -267 419 -360 -419
		mu 0 4 147 149 187 188
		f 4 -263 261 -358 -420
		mu 0 4 149 268 266 187
		f 4 -1 520 -430 521
		mu 0 4 191 189 270 190
		f 4 -2 -522 -422 522
		mu 0 4 193 191 190 192
		f 4 -3 -523 -437 523
		mu 0 4 195 193 192 194
		f 4 -4 -524 -442 524
		mu 0 4 197 195 194 196
		f 4 -5 -525 -447 525
		mu 0 4 199 197 196 198
		f 4 -6 -526 -452 526
		mu 0 4 201 199 198 200
		f 4 -7 -527 -457 527
		mu 0 4 203 201 200 202
		f 4 -8 -528 -462 528
		mu 0 4 205 203 202 204
		f 4 -9 -529 -467 529
		mu 0 4 207 205 204 206
		f 4 -10 -530 -472 530
		mu 0 4 209 207 206 208
		f 4 -11 -531 -477 531
		mu 0 4 211 209 208 210
		f 4 -12 -532 -482 532
		mu 0 4 213 211 210 212
		f 4 -13 -533 -487 533
		mu 0 4 215 213 212 214
		f 4 -14 -534 -492 534
		mu 0 4 217 215 214 216
		f 4 -15 -535 -497 535
		mu 0 4 219 217 216 218
		f 4 -16 -536 -502 536
		mu 0 4 221 219 218 220
		f 4 -17 -537 -507 537
		mu 0 4 223 221 220 222
		f 4 -18 -538 -512 538
		mu 0 4 225 223 222 224
		f 4 -19 -539 -517 539
		mu 0 4 228 225 224 226
		f 4 -20 -540 -435 -521
		mu 0 4 227 228 226 229
		f 4 -266 540 -427 541
		mu 0 4 105 230 233 231
		f 4 -274 542 -424 -541
		mu 0 4 230 232 235 233
		f 4 -279 543 -439 -543
		mu 0 4 232 234 237 235
		f 4 -284 544 -444 -544
		mu 0 4 234 236 239 237
		f 4 -289 545 -449 -545
		mu 0 4 236 238 241 239
		f 4 -294 546 -454 -546
		mu 0 4 238 240 243 241
		f 4 -299 547 -459 -547
		mu 0 4 240 242 245 243
		f 4 -304 548 -464 -548
		mu 0 4 242 244 247 245
		f 4 -309 549 -469 -549
		mu 0 4 244 246 249 247
		f 4 -314 550 -474 -550
		mu 0 4 246 248 251 249
		f 4 -319 551 -479 -551
		mu 0 4 248 250 253 251
		f 4 -324 552 -484 -552
		mu 0 4 250 252 255 253
		f 4 -329 553 -489 -553
		mu 0 4 252 254 257 255
		f 4 -334 554 -494 -554
		mu 0 4 254 256 259 257
		f 4 -339 555 -499 -555
		mu 0 4 256 258 261 259
		f 4 -344 556 -504 -556
		mu 0 4 258 260 263 261
		f 4 -349 557 -509 -557
		mu 0 4 260 262 265 263
		f 4 -354 558 -514 -558
		mu 0 4 262 264 267 265
		f 4 -359 559 -519 -559
		mu 0 4 264 266 269 267
		f 4 -262 -542 -432 -560
		mu 0 4 266 268 271 269
		f 4 -426 423 424 -561
		mu 0 4 273 233 235 279
		f 4 -429 561 420 421
		mu 0 4 190 272 278 192
		f 4 -428 560 422 -562
		mu 0 4 272 273 279 278
		f 4 425 562 -431 426
		mu 0 4 233 273 277 231
		f 4 427 563 -433 -563
		mu 0 4 273 272 275 277
		f 4 428 429 -434 -564
		mu 0 4 272 190 270 275
		f 4 -425 438 439 -565
		mu 0 4 279 235 237 281
		f 4 -421 565 435 436
		mu 0 4 192 278 280 194
		f 4 -423 564 437 -566
		mu 0 4 278 279 281 280
		f 4 -440 443 444 -567
		mu 0 4 281 237 239 283
		f 4 -436 567 440 441
		mu 0 4 194 280 282 196
		f 4 -438 566 442 -568
		mu 0 4 280 281 283 282
		f 4 -445 448 449 -569
		mu 0 4 283 239 241 285
		f 4 -441 569 445 446
		mu 0 4 196 282 284 198
		f 4 -443 568 447 -570
		mu 0 4 282 283 285 284
		f 4 -450 453 454 -571
		mu 0 4 285 241 243 287
		f 4 -446 571 450 451
		mu 0 4 198 284 286 200
		f 4 -448 570 452 -572
		mu 0 4 284 285 287 286
		f 4 -455 458 459 -573
		mu 0 4 287 243 245 289
		f 4 -451 573 455 456
		mu 0 4 200 286 288 202
		f 4 -453 572 457 -574
		mu 0 4 286 287 289 288
		f 4 -460 463 464 -575
		mu 0 4 289 245 247 291
		f 4 -456 575 460 461
		mu 0 4 202 288 290 204
		f 4 -458 574 462 -576
		mu 0 4 288 289 291 290
		f 4 -465 468 469 -577
		mu 0 4 291 247 249 293
		f 4 -461 577 465 466
		mu 0 4 204 290 292 206
		f 4 -463 576 467 -578
		mu 0 4 290 291 293 292
		f 4 -470 473 474 -579
		mu 0 4 293 249 251 295
		f 4 -466 579 470 471
		mu 0 4 206 292 294 208
		f 4 -468 578 472 -580
		mu 0 4 292 293 295 294
		f 4 -475 478 479 -581
		mu 0 4 295 251 253 297
		f 4 -471 581 475 476
		mu 0 4 208 294 296 210
		f 4 -473 580 477 -582
		mu 0 4 294 295 297 296
		f 4 -480 483 484 -583
		mu 0 4 297 253 255 299
		f 4 -476 583 480 481
		mu 0 4 210 296 298 212
		f 4 -478 582 482 -584
		mu 0 4 296 297 299 298
		f 4 -485 488 489 -585
		mu 0 4 299 255 257 301
		f 4 -481 585 485 486
		mu 0 4 212 298 300 214
		f 4 -483 584 487 -586
		mu 0 4 298 299 301 300
		f 4 -490 493 494 -587
		mu 0 4 301 257 259 303
		f 4 -486 587 490 491
		mu 0 4 214 300 302 216
		f 4 -488 586 492 -588
		mu 0 4 300 301 303 302
		f 4 -495 498 499 -589
		mu 0 4 303 259 261 305
		f 4 -491 589 495 496
		mu 0 4 216 302 304 218
		f 4 -493 588 497 -590
		mu 0 4 302 303 305 304
		f 4 -500 503 504 -591
		mu 0 4 305 261 263 307
		f 4 -496 591 500 501
		mu 0 4 218 304 306 220
		f 4 -498 590 502 -592
		mu 0 4 304 305 307 306
		f 4 -505 508 509 -593
		mu 0 4 307 263 265 309
		f 4 -501 593 505 506
		mu 0 4 220 306 308 222
		f 4 -503 592 507 -594
		mu 0 4 306 307 309 308
		f 4 -510 513 514 -595
		mu 0 4 309 265 267 311
		f 4 -506 595 510 511
		mu 0 4 222 308 310 224
		f 4 -508 594 512 -596
		mu 0 4 308 309 311 310
		f 4 -515 518 519 -597
		mu 0 4 311 267 269 313
		f 4 -511 597 515 516
		mu 0 4 224 310 312 226
		f 4 -513 596 517 -598
		mu 0 4 310 311 313 312
		f 4 430 598 -520 431
		mu 0 4 271 276 313 269
		f 4 432 599 -518 -599
		mu 0 4 276 274 312 313
		f 4 433 434 -516 -600
		mu 0 4 274 229 226 312
		f 4 0 700 -610 701
		mu 0 4 352 314 317 315
		f 4 1 702 -615 -701
		mu 0 4 314 316 319 317
		f 4 2 703 -620 -703
		mu 0 4 316 318 321 319
		f 4 3 704 -625 -704
		mu 0 4 318 320 323 321
		f 4 4 705 -630 -705
		mu 0 4 320 322 325 323
		f 4 5 706 -635 -706
		mu 0 4 322 324 327 325
		f 4 6 707 -640 -707
		mu 0 4 324 326 329 327
		f 4 7 708 -645 -708
		mu 0 4 326 328 331 329
		f 4 8 709 -650 -709
		mu 0 4 328 330 333 331
		f 4 9 710 -655 -710
		mu 0 4 330 332 335 333
		f 4 10 711 -660 -711
		mu 0 4 332 334 337 335
		f 4 11 712 -665 -712
		mu 0 4 334 336 339 337
		f 4 12 713 -670 -713
		mu 0 4 336 338 341 339
		f 4 13 714 -675 -714
		mu 0 4 338 340 343 341
		f 4 14 715 -680 -715
		mu 0 4 340 342 345 343
		f 4 15 716 -685 -716
		mu 0 4 342 344 347 345
		f 4 16 717 -690 -717
		mu 0 4 344 346 349 347
		f 4 17 718 -695 -718
		mu 0 4 346 348 351 349
		f 4 18 719 -700 -719
		mu 0 4 348 350 353 351
		f 4 19 -702 -602 -720
		mu 0 4 350 352 315 353
		f 4 -607 720 -21 721
		mu 0 4 392 354 357 355
		f 4 -612 722 -24 -721
		mu 0 4 354 356 359 357
		f 4 -617 723 -26 -723
		mu 0 4 356 358 361 359
		f 4 -622 724 -28 -724
		mu 0 4 358 360 363 361
		f 4 -627 725 -30 -725
		mu 0 4 360 362 365 363
		f 4 -632 726 -32 -726
		mu 0 4 362 364 367 365
		f 4 -637 727 -34 -727
		mu 0 4 364 366 369 367
		f 4 -642 728 -36 -728
		mu 0 4 366 368 371 369
		f 4 -647 729 -38 -729
		mu 0 4 368 370 373 371
		f 4 -652 730 -40 -730
		mu 0 4 370 372 375 373
		f 4 -657 731 -42 -731
		mu 0 4 372 374 377 375
		f 4 -662 732 -44 -732
		mu 0 4 374 376 379 377
		f 4 -667 733 -46 -733
		mu 0 4 376 378 381 379
		f 4 -672 734 -48 -734
		mu 0 4 378 380 383 381
		f 4 -677 735 -50 -735
		mu 0 4 380 382 385 383
		f 4 -682 736 -52 -736
		mu 0 4 382 384 387 385
		f 4 -687 737 -54 -737
		mu 0 4 384 386 389 387
		f 4 -692 738 -56 -738
		mu 0 4 386 388 391 389
		f 4 -697 739 -58 -739
		mu 0 4 388 390 393 391
		f 4 -604 -722 -60 -740
		mu 0 4 390 392 355 393
		f 4 -606 603 604 -741
		mu 0 4 395 392 390 433
		f 4 -609 741 600 601
		mu 0 4 315 394 432 353
		f 4 -608 740 602 -742
		mu 0 4 394 395 433 432
		f 4 605 742 -611 606
		mu 0 4 392 395 397 354
		f 4 607 743 -613 -743
		mu 0 4 395 394 396 397
		f 4 608 609 -614 -744
		mu 0 4 394 315 317 396
		f 4 610 744 -616 611
		mu 0 4 354 397 399 356
		f 4 612 745 -618 -745
		mu 0 4 397 396 398 399
		f 4 613 614 -619 -746
		mu 0 4 396 317 319 398
		f 4 615 746 -621 616
		mu 0 4 356 399 401 358
		f 4 617 747 -623 -747
		mu 0 4 399 398 400 401
		f 4 618 619 -624 -748
		mu 0 4 398 319 321 400
		f 4 620 748 -626 621
		mu 0 4 358 401 403 360
		f 4 622 749 -628 -749
		mu 0 4 401 400 402 403
		f 4 623 624 -629 -750
		mu 0 4 400 321 323 402
		f 4 625 750 -631 626
		mu 0 4 360 403 405 362
		f 4 627 751 -633 -751
		mu 0 4 403 402 404 405
		f 4 628 629 -634 -752
		mu 0 4 402 323 325 404
		f 4 630 752 -636 631
		mu 0 4 362 405 407 364
		f 4 632 753 -638 -753
		mu 0 4 405 404 406 407
		f 4 633 634 -639 -754
		mu 0 4 404 325 327 406
		f 4 635 754 -641 636
		mu 0 4 364 407 409 366
		f 4 637 755 -643 -755
		mu 0 4 407 406 408 409
		f 4 638 639 -644 -756
		mu 0 4 406 327 329 408
		f 4 640 756 -646 641
		mu 0 4 366 409 411 368
		f 4 642 757 -648 -757
		mu 0 4 409 408 410 411
		f 4 643 644 -649 -758
		mu 0 4 408 329 331 410
		f 4 645 758 -651 646
		mu 0 4 368 411 413 370
		f 4 647 759 -653 -759
		mu 0 4 411 410 412 413
		f 4 648 649 -654 -760
		mu 0 4 410 331 333 412
		f 4 650 760 -656 651
		mu 0 4 370 413 415 372
		f 4 652 761 -658 -761
		mu 0 4 413 412 414 415
		f 4 653 654 -659 -762
		mu 0 4 412 333 335 414
		f 4 655 762 -661 656
		mu 0 4 372 415 417 374
		f 4 657 763 -663 -763
		mu 0 4 415 414 416 417
		f 4 658 659 -664 -764
		mu 0 4 414 335 337 416
		f 4 660 764 -666 661
		mu 0 4 374 417 419 376
		f 4 662 765 -668 -765
		mu 0 4 417 416 418 419
		f 4 663 664 -669 -766
		mu 0 4 416 337 339 418
		f 4 665 766 -671 666
		mu 0 4 376 419 421 378
		f 4 667 767 -673 -767
		mu 0 4 419 418 420 421
		f 4 668 669 -674 -768
		mu 0 4 418 339 341 420
		f 4 670 768 -676 671
		mu 0 4 378 421 423 380
		f 4 672 769 -678 -769
		mu 0 4 421 420 422 423
		f 4 673 674 -679 -770
		mu 0 4 420 341 343 422
		f 4 675 770 -681 676
		mu 0 4 380 423 425 382
		f 4 677 771 -683 -771
		mu 0 4 423 422 424 425
		f 4 678 679 -684 -772
		mu 0 4 422 343 345 424
		f 4 680 772 -686 681
		mu 0 4 382 425 427 384
		f 4 682 773 -688 -773
		mu 0 4 425 424 426 427
		f 4 683 684 -689 -774
		mu 0 4 424 345 347 426
		f 4 685 774 -691 686
		mu 0 4 384 427 429 386
		f 4 687 775 -693 -775
		mu 0 4 427 426 428 429
		f 4 688 689 -694 -776
		mu 0 4 426 347 349 428
		f 4 690 776 -696 691
		mu 0 4 386 429 431 388
		f 4 692 777 -698 -777
		mu 0 4 429 428 430 431
		f 4 693 694 -699 -778
		mu 0 4 428 349 351 430
		f 4 695 778 -605 696
		mu 0 4 388 431 433 390
		f 4 697 779 -603 -779
		mu 0 4 431 430 432 433
		f 4 698 699 -601 -780
		mu 0 4 430 351 353 432;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder11";
	rename -uid "4A2E88F0-4F02-E5A0-3F44-B89A38EDA49A";
	setAttr ".t" -type "double3" 34.421559403047652 25.297789716629381 7.789380823245537 ;
	setAttr ".r" -type "double3" 199.40490552790371 32.119716961595628 103.38448893032523 ;
	setAttr ".s" -type "double3" 0.8061733841915012 0.8061733841915012 0.8061733841915012 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder11Shape" -p "pCylinder11";
	rename -uid "BEC7498E-405F-1822-8D4A-1D94DF478ECB";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:519]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 607 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.64860266 0.10796607 0.62640899
		 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607
		 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997
		 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161
		 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146
		 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998 0.3125
		 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.375
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.38749999 0.3125 0.39999998 0.68843985
		 0.39999998 0.3125 0.41249996 0.68843985 0.41249996 0.3125 0.42499995 0.68843985 0.42499995
		 0.3125 0.43749994 0.68843985 0.43749994 0.3125 0.44999993 0.68843985 0.44999993 0.3125
		 0.46249992 0.68843985 0.46249992 0.3125 0.4749999 0.68843985 0.4749999 0.3125 0.48749989
		 0.68843985 0.48749989 0.3125 0.49999988 0.68843985 0.49999988 0.3125 0.51249987 0.68843985
		 0.51249987 0.3125 0.52499986 0.68843985 0.52499986 0.3125 0.53749985 0.68843985 0.53749985
		 0.3125 0.54999983 0.68843985 0.54999983 0.3125 0.56249982 0.68843985 0.56249982 0.3125
		 0.57499981 0.68843985 0.57499981 0.3125 0.5874998 0.68843985 0.5874998 0.3125 0.59999979
		 0.68843985 0.59999979 0.3125 0.61249977 0.68843985 0.61249977 0.3125 0.62499976 0.68843985
		 0.62499976 0.3125 0.62640899 0.064408496 0.5 0.15000001 0.64860266 0.10796607 0.59184152
		 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607 0.0076473504 0.40815851
		 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997 0.15625 0.3513974
		 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161 0.3048526 0.5 0.3125
		 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146 0.6486026 0.2045339
		 0.65625 0.15625 0.6486026 0.89203393 0.5 0.83749998 0.62640893 0.93559146 0.59184146
		 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893 0.37359107
		 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607 0.37359107
		 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994 0.54828393
		 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607 0.65625
		 0.84375 0 0.050000001 0.050000001 0.050000001 0.050000001 0.1 0 0.1 0.1 0.050000001
		 0.1 0.1 0.15000001 0.050000001 0.15000001 0.1 0.2 0.050000001 0.2 0.1 0.25 0.050000001
		 0.25 0.1 0.30000001 0.050000001 0.30000001 0.1 0.35000002 0.050000001 0.35000002
		 0.1 0.40000004 0.050000001 0.40000004 0.1 0.45000005 0.050000001 0.45000005 0.1 0.50000006
		 0.050000001 0.50000006 0.1 0.55000007 0.050000001 0.55000007 0.1 0.60000008 0.050000001
		 0.60000008 0.1 0.6500001 0.050000001 0.6500001 0.1 0.70000011 0.050000001 0.70000011
		 0.1 0.75000012 0.050000001 0.75000012 0.1 0.80000013 0.050000001 0.80000013 0.1 0.85000014
		 0.050000001 0.85000014 0.1 0.90000015 0.050000001 0.90000015 0.1 0.95000017 0.050000001
		 0.95000017 0.1 1.000000119209 0.050000001 1.000000119209 0.1 0.050000001 0.15000001
		 0 0.15000001 0.1 0.15000001 0.15000001 0.15000001 0.2 0.15000001 0.25 0.15000001
		 0.30000001 0.15000001 0.35000002 0.15000001 0.40000004 0.15000001 0.45000005 0.15000001
		 0.50000006 0.15000001 0.55000007 0.15000001 0.60000008 0.15000001 0.6500001 0.15000001
		 0.70000011 0.15000001 0.75000012 0.15000001 0.80000013 0.15000001 0.85000014 0.15000001
		 0.90000015 0.15000001 0.95000017 0.15000001 1.000000119209 0.15000001 0.050000001
		 0.2 0 0.2 0.1 0.2 0.15000001 0.2 0.2 0.2 0.25 0.2 0.30000001 0.2 0.35000002 0.2 0.40000004
		 0.2 0.45000005 0.2 0.50000006 0.2 0.55000007 0.2 0.60000008 0.2 0.6500001 0.2 0.70000011
		 0.2 0.75000012 0.2 0.80000013 0.2 0.85000014 0.2 0.90000015 0.2;
	setAttr ".uvst[0].uvsp[250:499]" 0.95000017 0.2 1.000000119209 0.2 0.050000001
		 0.25 0 0.25 0.1 0.25 0.15000001 0.25 0.2 0.25 0.25 0.25 0.30000001 0.25 0.35000002
		 0.25 0.40000004 0.25 0.45000005 0.25 0.50000006 0.25 0.55000007 0.25 0.60000008 0.25
		 0.6500001 0.25 0.70000011 0.25 0.75000012 0.25 0.80000013 0.25 0.85000014 0.25 0.90000015
		 0.25 0.95000017 0.25 1.000000119209 0.25 0.050000001 0.30000001 0 0.30000001 0.1
		 0.30000001 0.15000001 0.30000001 0.2 0.30000001 0.25 0.30000001 0.30000001 0.30000001
		 0.35000002 0.30000001 0.40000004 0.30000001 0.45000005 0.30000001 0.50000006 0.30000001
		 0.55000007 0.30000001 0.60000008 0.30000001 0.6500001 0.30000001 0.70000011 0.30000001
		 0.75000012 0.30000001 0.80000013 0.30000001 0.85000014 0.30000001 0.90000015 0.30000001
		 0.95000017 0.30000001 1.000000119209 0.30000001 0.050000001 0.35000002 0 0.35000002
		 0.1 0.35000002 0.15000001 0.35000002 0.2 0.35000002 0.25 0.35000002 0.30000001 0.35000002
		 0.35000002 0.35000002 0.40000004 0.35000002 0.45000005 0.35000002 0.50000006 0.35000002
		 0.55000007 0.35000002 0.60000008 0.35000002 0.6500001 0.35000002 0.70000011 0.35000002
		 0.75000012 0.35000002 0.80000013 0.35000002 0.85000014 0.35000002 0.90000015 0.35000002
		 0.95000017 0.35000002 1.000000119209 0.35000002 0.050000001 0.40000004 0 0.40000004
		 0.1 0.40000004 0.15000001 0.40000004 0.2 0.40000004 0.25 0.40000004 0.30000001 0.40000004
		 0.35000002 0.40000004 0.40000004 0.40000004 0.45000005 0.40000004 0.50000006 0.40000004
		 0.55000007 0.40000004 0.60000008 0.40000004 0.6500001 0.40000004 0.70000011 0.40000004
		 0.75000012 0.40000004 0.80000013 0.40000004 0.85000014 0.40000004 0.90000015 0.40000004
		 0.95000017 0.40000004 1.000000119209 0.40000004 0.050000001 0.45000005 0 0.45000005
		 0.1 0.45000005 0.15000001 0.45000005 0.2 0.45000005 0.25 0.45000005 0.30000001 0.45000005
		 0.35000002 0.45000005 0.40000004 0.45000005 0.45000005 0.45000005 0.50000006 0.45000005
		 0.55000007 0.45000005 0.60000008 0.45000005 0.6500001 0.45000005 0.70000011 0.45000005
		 0.75000012 0.45000005 0.80000013 0.45000005 0.85000014 0.45000005 0.90000015 0.45000005
		 0.95000017 0.45000005 1.000000119209 0.45000005 0.050000001 0.50000006 0 0.50000006
		 0.1 0.50000006 0.15000001 0.50000006 0.2 0.50000006 0.25 0.50000006 0.30000001 0.50000006
		 0.35000002 0.50000006 0.40000004 0.50000006 0.45000005 0.50000006 0.50000006 0.50000006
		 0.55000007 0.50000006 0.60000008 0.50000006 0.6500001 0.50000006 0.70000011 0.50000006
		 0.75000012 0.50000006 0.80000013 0.50000006 0.85000014 0.50000006 0.90000015 0.50000006
		 0.95000017 0.50000006 1.000000119209 0.50000006 0.050000001 0.55000007 0 0.55000007
		 0.1 0.55000007 0.15000001 0.55000007 0.2 0.55000007 0.25 0.55000007 0.30000001 0.55000007
		 0.35000002 0.55000007 0.40000004 0.55000007 0.45000005 0.55000007 0.50000006 0.55000007
		 0.55000007 0.55000007 0.60000008 0.55000007 0.6500001 0.55000007 0.70000011 0.55000007
		 0.75000012 0.55000007 0.80000013 0.55000007 0.85000014 0.55000007 0.90000015 0.55000007
		 0.95000017 0.55000007 1.000000119209 0.55000007 0.050000001 0.60000008 0 0.60000008
		 0.1 0.60000008 0.15000001 0.60000008 0.2 0.60000008 0.25 0.60000008 0.30000001 0.60000008
		 0.35000002 0.60000008 0.40000004 0.60000008 0.45000005 0.60000008 0.50000006 0.60000008
		 0.55000007 0.60000008 0.60000008 0.60000008 0.6500001 0.60000008 0.70000011 0.60000008
		 0.75000012 0.60000008 0.80000013 0.60000008 0.85000014 0.60000008 0.90000015 0.60000008
		 0.95000017 0.60000008 1.000000119209 0.60000008 0.050000001 0.6500001 0 0.6500001
		 0.1 0.6500001 0.15000001 0.6500001 0.2 0.6500001 0.25 0.6500001 0.30000001 0.6500001
		 0.35000002 0.6500001 0.40000004 0.6500001 0.45000005 0.6500001 0.50000006 0.6500001
		 0.55000007 0.6500001 0.60000008 0.6500001 0.6500001 0.6500001 0.70000011 0.6500001
		 0.75000012 0.6500001 0.80000013 0.6500001 0.85000014 0.6500001 0.90000015 0.6500001
		 0.95000017 0.6500001 1.000000119209 0.6500001 0.050000001 0.70000011 0 0.70000011
		 0.1 0.70000011 0.15000001 0.70000011 0.2 0.70000011 0.25 0.70000011 0.30000001 0.70000011
		 0.35000002 0.70000011 0.40000004 0.70000011 0.45000005 0.70000011 0.50000006 0.70000011
		 0.55000007 0.70000011 0.60000008 0.70000011 0.6500001 0.70000011 0.70000011 0.70000011
		 0.75000012 0.70000011 0.80000013 0.70000011 0.85000014 0.70000011 0.90000015 0.70000011
		 0.95000017 0.70000011 1.000000119209 0.70000011 0.050000001 0.75000012 0 0.75000012
		 0.1 0.75000012 0.15000001 0.75000012 0.2 0.75000012 0.25 0.75000012 0.30000001 0.75000012
		 0.35000002 0.75000012 0.40000004 0.75000012 0.45000005 0.75000012 0.50000006 0.75000012
		 0.55000007 0.75000012 0.60000008 0.75000012 0.6500001 0.75000012 0.70000011 0.75000012
		 0.75000012 0.75000012 0.80000013 0.75000012 0.85000014 0.75000012 0.90000015 0.75000012
		 0.95000017 0.75000012 1.000000119209 0.75000012 0.050000001 0.80000013 0 0.80000013
		 0.1 0.80000013 0.15000001 0.80000013 0.2 0.80000013 0.25 0.80000013 0.30000001 0.80000013
		 0.35000002 0.80000013 0.40000004 0.80000013 0.45000005 0.80000013 0.50000006 0.80000013
		 0.55000007 0.80000013 0.60000008 0.80000013 0.6500001 0.80000013 0.70000011 0.80000013
		 0.75000012 0.80000013 0.80000013 0.80000013;
	setAttr ".uvst[0].uvsp[500:606]" 0.85000014 0.80000013 0.90000015 0.80000013
		 0.95000017 0.80000013 1.000000119209 0.80000013 0.050000001 0.85000014 0 0.85000014
		 0.1 0.85000014 0.15000001 0.85000014 0.2 0.85000014 0.25 0.85000014 0.30000001 0.85000014
		 0.35000002 0.85000014 0.40000004 0.85000014 0.45000005 0.85000014 0.50000006 0.85000014
		 0.55000007 0.85000014 0.60000008 0.85000014 0.6500001 0.85000014 0.70000011 0.85000014
		 0.75000012 0.85000014 0.80000013 0.85000014 0.85000014 0.85000014 0.90000015 0.85000014
		 0.95000017 0.85000014 1.000000119209 0.85000014 0.050000001 0.90000015 0 0.90000015
		 0.1 0.90000015 0.15000001 0.90000015 0.2 0.90000015 0.25 0.90000015 0.30000001 0.90000015
		 0.35000002 0.90000015 0.40000004 0.90000015 0.45000005 0.90000015 0.50000006 0.90000015
		 0.55000007 0.90000015 0.60000008 0.90000015 0.6500001 0.90000015 0.70000011 0.90000015
		 0.75000012 0.90000015 0.80000013 0.90000015 0.85000014 0.90000015 0.90000015 0.90000015
		 0.95000017 0.90000015 1.000000119209 0.90000015 0.050000001 0.95000017 0 0.95000017
		 0.1 0.95000017 0.15000001 0.95000017 0.2 0.95000017 0.25 0.95000017 0.30000001 0.95000017
		 0.35000002 0.95000017 0.40000004 0.95000017 0.45000005 0.95000017 0.50000006 0.95000017
		 0.55000007 0.95000017 0.60000008 0.95000017 0.6500001 0.95000017 0.70000011 0.95000017
		 0.75000012 0.95000017 0.80000013 0.95000017 0.85000014 0.95000017 0.90000015 0.95000017
		 0.95000017 0.95000017 1.000000119209 0.95000017 0.025 0 0.075000003 0 0.125 0 0.175
		 0 0.22500001 0 0.27500001 0 0.32500002 0 0.375 0 0.42500001 0 0.47499999 0 0.52500004
		 0 0.57499999 0 0.625 0 0.67500001 0 0.72500002 0 0.77500004 0 0.82499999 0 0.875
		 0 0.92500001 0 0.97500002 0 0.025 1 0.075000003 1 0.125 1 0.175 1 0.22500001 1 0.27500001
		 1 0.32500002 1 0.375 1 0.42500001 1 0.47499999 1 0.52500004 1 0.57499999 1 0.625
		 1 0.67500001 1 0.72500002 1 0.77500004 1 0.82499999 1 0.875 1 0.92500001 1 0.97500002
		 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 466 ".vt";
	setAttr ".vt[0:165]"  -31.42638397 0.15162086 89.97360229 -31.61052513 0.26360402 89.74130249
		 -31.94010735 0.35247436 89.46312714 -32.38286972 0.40953261 89.16630554 -32.89547348 0.4291935 88.87989807
		 -33.42773819 0.40953258 88.63193512 -33.92756271 0.3524743 88.44669342 -34.34602356 0.26360396 88.34230042
		 -34.64215469 0.15162082 88.32897949 -34.78697205 0.0274866 88.40803528 -34.7663002 -0.09664762 88.57172394
		 -34.58216095 -0.20863073 88.80403137 -34.25257874 -0.29750097 89.082206726 -33.80981445 -0.35455921 89.37902069
		 -33.29721451 -0.37422013 89.66543579 -32.7649498 -0.35455918 89.91339111 -32.26512146 -0.29750094 90.098632813
		 -31.84666252 -0.2086307 90.20302582 -31.55052948 -0.096647598 90.21634674 -31.40571213 0.0274866 90.13729095
		 -31.40359116 0.096647523 89.929039 -31.58773232 0.20863067 89.69673157 -31.91731453 0.297501 89.41855621
		 -32.3600769 0.35455924 89.12174225 -32.87267685 0.37422013 88.83532715 -33.40494156 0.35455921 88.58737183
		 -33.9047699 0.29750094 88.4021225 -34.32322693 0.20863059 88.29773712 -34.61936188 0.096647471 88.2844162
		 -34.76417923 -0.027486745 88.36347198 -34.74350739 -0.15162097 88.52716064 -34.55936432 -0.26360404 88.75946045
		 -34.2297821 -0.35247433 89.037635803 -33.78702164 -0.40953258 89.3344574 -33.27441788 -0.4291935 89.62086487
		 -32.74215698 -0.40953255 89.86882782 -32.24232864 -0.3524743 90.054069519 -31.82386971 -0.26360402 90.15846252
		 -31.52773666 -0.15162094 90.17178345 -31.38291931 -0.027486745 90.092727661 -33.096343994 0.0274866 89.27266693
		 -33.073547363 -0.027486745 89.22809601 -38.24187469 2.49435377 89.47029114 -38.0036354065 2.42841434 89.27202606
		 -37.675354 2.24016404 89.046867371 -37.28915024 1.94802976 88.81686401 -36.88284302 1.58060765 88.60451508
		 -36.49619675 1.17386389 88.43061066 -36.16706085 0.76761329 88.31217194 -35.92765427 0.40162241 88.26081085
		 -35.80141068 0.11171699 88.28153229 -35.80068588 -0.073725104 88.37231445 -35.92555618 -0.1365515 88.5242691
		 -36.16379166 -0.070612133 88.72253418 -36.49207687 0.11763823 88.94768524 -36.87827682 0.40977228 89.17769623
		 -37.28458405 0.77719414 89.39004517 -37.67123032 1.18393755 89.56394958 -38.00036621094 1.59018826 89.68238068
		 -38.2397728 1.95617914 89.73374939 -38.36601639 2.24608445 89.71302795 -38.36674118 2.43152666 89.62224579
		 -38.19311142 2.51918554 89.52061462 -37.95487595 2.45324612 89.32236481 -37.62659073 2.26499557 89.097198486
		 -37.24039078 1.97286129 88.86719513 -36.83408356 1.60543942 88.65484619 -36.44743729 1.19869566 88.48094177
		 -36.11829758 0.792445 88.36251068 -35.87889481 0.42645407 88.31114197 -35.75265121 0.13654858 88.33187103
		 -35.75192642 -0.048893452 88.42264557 -35.87679672 -0.11171985 88.57460022 -36.1150322 -0.04578054 88.7728653
		 -36.4433136 0.14246988 88.99801636 -36.82951355 0.43460399 89.22803497 -37.23582458 0.80202574 89.44038391
		 -37.62246704 1.20876932 89.6142807 -37.95160675 1.6150198 89.73271179 -38.19100952 1.98101068 89.78408051
		 -38.31725311 2.27091622 89.76335907 -38.31797791 2.45635819 89.67258453 -37.083713531 1.17890096 88.99727631
		 -37.034954071 1.20373249 89.047615051 -34.97560501 -0.98768836 88.25870514 -34.99782562 -0.98768836 88.21509552
		 -35.032432556 -0.98768836 88.18048859 -35.076042175 -0.98768836 88.15826416 -35.12438202 -0.98768836 88.15061188
		 -35.17272186 -0.98768836 88.15826416 -35.21633148 -0.98768836 88.18048859 -35.25093842 -0.98768836 88.21509552
		 -35.27315903 -0.98768836 88.25870514 -35.28081512 -0.98768836 88.30704498 -35.27315903 -0.98768836 88.35538483
		 -35.25093842 -0.98768836 88.39899445 -35.21633148 -0.98768836 88.43360138 -35.17272186 -0.98768836 88.45582581
		 -35.12438202 -0.98768836 88.46347809 -35.076042175 -0.98768836 88.45582581 -35.032432556 -0.98768836 88.43360138
		 -34.99782562 -0.98768836 88.39899445 -34.97560501 -0.98768836 88.35538483 -34.96794891 -0.98768836 88.30704498
		 -34.83049011 -0.95105654 88.21155548 -34.87438202 -0.95105654 88.12541199 -34.94274521 -0.95105654 88.057044983
		 -35.028888702 -0.95105654 88.013153076 -35.12438202 -0.95105654 87.99803162 -35.21987534 -0.95105654 88.013153076
		 -35.30601883 -0.95105654 88.057044983 -35.37438202 -0.95105654 88.12541199 -35.41827393 -0.95105654 88.21155548
		 -35.4333992 -0.95105654 88.30704498 -35.41827393 -0.95105654 88.40253448 -35.37438202 -0.95105654 88.48867798
		 -35.30601883 -0.95105654 88.55704498 -35.21987534 -0.95105654 88.60093689 -35.12438202 -0.95105654 88.61605835
		 -35.028888702 -0.95105654 88.60093689 -34.94274521 -0.95105654 88.55704498 -34.87438202 -0.95105654 88.48867798
		 -34.83049011 -0.95105654 88.40253448 -34.81536484 -0.95105654 88.30704498 -34.69261169 -0.89100653 88.16675568
		 -34.75709534 -0.89100653 88.04019928 -34.8575325 -0.89100653 87.9397583 -34.98409271 -0.89100653 87.87527466
		 -35.12438202 -0.89100653 87.85305786 -35.26467133 -0.89100653 87.87527466 -35.39123154 -0.89100653 87.9397583
		 -35.4916687 -0.89100653 88.04019928 -35.55615234 -0.89100653 88.16675568 -35.57837296 -0.89100653 88.30704498
		 -35.55615234 -0.89100653 88.44733429 -35.4916687 -0.89100653 88.57389069 -35.39123154 -0.89100653 88.67433167
		 -35.26467133 -0.89100653 88.73881531 -35.12438202 -0.89100653 88.7610321 -34.98409271 -0.89100653 88.73881531
		 -34.8575325 -0.89100653 88.67433167 -34.75709534 -0.89100653 88.57389069 -34.69261169 -0.89100653 88.44733429
		 -34.67039108 -0.89100653 88.30704498 -34.56536484 -0.809017 88.12541199 -34.6488533 -0.809017 87.96155548
		 -34.7788887 -0.809017 87.83152008 -34.94274521 -0.809017 87.74802399 -35.12438202 -0.809017 87.71926117
		 -35.30601883 -0.809017 87.74802399 -35.46987534 -0.809017 87.83152008 -35.59991074 -0.809017 87.96155548
		 -35.6833992 -0.809017 88.12541199 -35.71216583 -0.809017 88.30704498 -35.6833992 -0.809017 88.48867798
		 -35.59991074 -0.809017 88.65253448 -35.46987534 -0.809017 88.78256989 -35.30601883 -0.809017 88.86605835
		 -35.12438202 -0.809017 88.8948288 -34.94274521 -0.809017 88.86605835 -34.7788887 -0.809017 88.78256989
		 -34.6488533 -0.809017 88.65253448 -34.56536484 -0.809017 88.48867798 -34.53659821 -0.809017 88.30704498
		 -34.45188141 -0.70710677 88.088539124 -34.55231857 -0.70710677 87.89141846;
	setAttr ".vt[166:331]" -34.70875549 -0.70710677 87.73498535 -34.90587234 -0.70710677 87.63454437
		 -35.12438202 -0.70710677 87.59993744 -35.34289169 -0.70710677 87.63454437 -35.54000854 -0.70710677 87.73498535
		 -35.69644165 -0.70710677 87.89141846 -35.79688263 -0.70710677 88.088539124 -35.83148956 -0.70710677 88.30704498
		 -35.79688263 -0.70710677 88.52555084 -35.69644165 -0.70710677 88.72267151 -35.54000854 -0.70710677 88.87910461
		 -35.34289169 -0.70710677 88.97954559 -35.12438202 -0.70710677 89.014152527 -34.90587234 -0.70710677 88.97954559
		 -34.70875549 -0.70710677 88.87910461 -34.55232239 -0.70710677 88.72267151 -34.45188522 -0.70710677 88.52555084
		 -34.41727448 -0.70710677 88.30704498 -34.3549614 -0.58778524 88.057044983 -34.46987152 -0.58778524 87.83152008
		 -34.6488533 -0.58778524 87.65253448 -34.87438202 -0.58778524 87.53762054 -35.12438202 -0.58778524 87.49802399
		 -35.37438202 -0.58778524 87.53762054 -35.59991074 -0.58778524 87.65253448 -35.77889252 -0.58778524 87.83152008
		 -35.89380264 -0.58778524 88.057044983 -35.9333992 -0.58778524 88.30704498 -35.89380264 -0.58778524 88.55704498
		 -35.77889252 -0.58778524 88.78256989 -35.59991074 -0.58778524 88.96155548 -35.37438202 -0.58778524 89.076469421
		 -35.12438202 -0.58778524 89.11605835 -34.87438202 -0.58778524 89.076469421 -34.6488533 -0.58778524 88.96155548
		 -34.46987534 -0.58778524 88.78256989 -34.3549614 -0.58778524 88.55704498 -34.31536484 -0.58778524 88.30704498
		 -34.27698517 -0.45399052 88.031707764 -34.40354156 -0.45399052 87.7833252 -34.60066223 -0.45399052 87.58620453
		 -34.8490448 -0.45399052 87.45964813 -35.12438202 -0.45399052 87.41603851 -35.39971924 -0.45399052 87.45964813
		 -35.64810181 -0.45399052 87.58620453 -35.84522247 -0.45399052 87.7833252 -35.97177887 -0.45399052 88.031707764
		 -36.015388489 -0.45399052 88.30704498 -35.97177887 -0.45399052 88.5823822 -35.84522247 -0.45399052 88.83076477
		 -35.64810181 -0.45399052 89.027885437 -35.39971924 -0.45399052 89.15444183 -35.12438202 -0.45399052 89.19805145
		 -34.8490448 -0.45399052 89.15444183 -34.60066223 -0.45399052 89.027885437 -34.40354156 -0.45399052 88.83076477
		 -34.27698517 -0.45399052 88.5823822 -34.23337555 -0.45399052 88.30704498 -34.21987152 -0.30901697 88.013153076
		 -34.3549614 -0.30901697 87.74802399 -34.56536484 -0.30901697 87.53762054 -34.83049011 -0.30901697 87.40253448
		 -35.12438202 -0.30901697 87.35598755 -35.41827393 -0.30901697 87.40253448 -35.6833992 -0.30901697 87.53762054
		 -35.89380264 -0.30901697 87.74803162 -36.028892517 -0.30901697 88.013153076 -36.075439453 -0.30901697 88.30704498
		 -36.028892517 -0.30901697 88.60093689 -35.89380264 -0.30901697 88.86605835 -35.6833992 -0.30901697 89.076469421
		 -35.41827393 -0.30901697 89.21155548 -35.12438202 -0.30901697 89.25810242 -34.83049011 -0.30901697 89.21155548
		 -34.56536484 -0.30901697 89.076469421 -34.3549614 -0.30901697 88.86605835 -34.21987534 -0.30901697 88.60093689
		 -34.17332458 -0.30901697 88.30704498 -34.18503571 -0.15643437 88.0018310547 -34.32532501 -0.15643437 87.72649384
		 -34.54383469 -0.15643437 87.50798798 -34.81916809 -0.15643437 87.36769867 -35.12438202 -0.15643437 87.31935883
		 -35.42959595 -0.15643437 87.36769867 -35.70492935 -0.15643437 87.50798798 -35.92343903 -0.15643437 87.72649384
		 -36.063728333 -0.15643437 88.0018310547 -36.11207199 -0.15643437 88.30704498 -36.063728333 -0.15643437 88.61225891
		 -35.92343903 -0.15643437 88.88759613 -35.70492935 -0.15643437 89.10610199 -35.42959595 -0.15643437 89.2463913
		 -35.12438202 -0.15643437 89.29473114 -34.81916809 -0.15643437 89.2463913 -34.54383469 -0.15643437 89.10610199
		 -34.32532501 -0.15643437 88.88759613 -34.18503571 -0.15643437 88.61225891 -34.13669205 -0.15643437 88.30704498
		 -34.17332458 0 87.99803162 -34.31536484 0 87.71926117 -34.53659821 0 87.49802399
		 -34.81536484 0 87.35598755 -35.12438202 0 87.30704498 -35.4333992 0 87.35598755 -35.71216583 0 87.49802399
		 -35.9333992 0 87.71926117 -36.075439453 0 87.99803162 -36.12438202 0 88.30704498
		 -36.075439453 0 88.61605835 -35.9333992 0 88.8948288 -35.71216583 0 89.11605835 -35.4333992 0 89.25810242
		 -35.12438202 0 89.30704498 -34.81536484 0 89.25810242 -34.53659821 0 89.11605835
		 -34.31536484 0 88.8948288 -34.17332458 0 88.61605835 -34.12438202 0 88.30704498 -34.18503571 0.15643437 88.0018310547
		 -34.32532501 0.15643437 87.72649384 -34.54383469 0.15643437 87.50798798 -34.81916809 0.15643437 87.36769867
		 -35.12438202 0.15643437 87.31935883 -35.42959595 0.15643437 87.36769867 -35.70492935 0.15643437 87.50798798
		 -35.92343903 0.15643437 87.72649384 -36.063728333 0.15643437 88.0018310547 -36.11207199 0.15643437 88.30704498
		 -36.063728333 0.15643437 88.61225891 -35.92343903 0.15643437 88.88759613 -35.70492935 0.15643437 89.10610199
		 -35.42959595 0.15643437 89.2463913 -35.12438202 0.15643437 89.29473114 -34.81916809 0.15643437 89.2463913
		 -34.54383469 0.15643437 89.10610199 -34.32532501 0.15643437 88.88759613 -34.18503571 0.15643437 88.61225891
		 -34.13669205 0.15643437 88.30704498 -34.21987152 0.30901697 88.013153076 -34.3549614 0.30901697 87.74802399
		 -34.56536484 0.30901697 87.53762054 -34.83049011 0.30901697 87.40253448 -35.12438202 0.30901697 87.35598755
		 -35.41827393 0.30901697 87.40253448 -35.6833992 0.30901697 87.53762054 -35.89380264 0.30901697 87.74803162
		 -36.028892517 0.30901697 88.013153076 -36.075439453 0.30901697 88.30704498 -36.028892517 0.30901697 88.60093689
		 -35.89380264 0.30901697 88.86605835 -35.6833992 0.30901697 89.076469421 -35.41827393 0.30901697 89.21155548
		 -35.12438202 0.30901697 89.25810242 -34.83049011 0.30901697 89.21155548 -34.56536484 0.30901697 89.076469421
		 -34.3549614 0.30901697 88.86605835 -34.21987534 0.30901697 88.60093689 -34.17332458 0.30901697 88.30704498
		 -34.27698517 0.45399052 88.031707764 -34.40354156 0.45399052 87.7833252 -34.60066223 0.45399052 87.58620453
		 -34.8490448 0.45399052 87.45964813 -35.12438202 0.45399052 87.41603851 -35.39971924 0.45399052 87.45964813
		 -35.64810181 0.45399052 87.58620453 -35.84522247 0.45399052 87.7833252;
	setAttr ".vt[332:465]" -35.97177887 0.45399052 88.031707764 -36.015388489 0.45399052 88.30704498
		 -35.97177887 0.45399052 88.5823822 -35.84522247 0.45399052 88.83076477 -35.64810181 0.45399052 89.027885437
		 -35.39971924 0.45399052 89.15444183 -35.12438202 0.45399052 89.19805145 -34.8490448 0.45399052 89.15444183
		 -34.60066223 0.45399052 89.027885437 -34.40354156 0.45399052 88.83076477 -34.27698517 0.45399052 88.5823822
		 -34.23337555 0.45399052 88.30704498 -34.3549614 0.58778524 88.057044983 -34.46987152 0.58778524 87.83152008
		 -34.6488533 0.58778524 87.65253448 -34.87438202 0.58778524 87.53762054 -35.12438202 0.58778524 87.49802399
		 -35.37438202 0.58778524 87.53762054 -35.59991074 0.58778524 87.65253448 -35.77889252 0.58778524 87.83152008
		 -35.89380264 0.58778524 88.057044983 -35.9333992 0.58778524 88.30704498 -35.89380264 0.58778524 88.55704498
		 -35.77889252 0.58778524 88.78256989 -35.59991074 0.58778524 88.96155548 -35.37438202 0.58778524 89.076469421
		 -35.12438202 0.58778524 89.11605835 -34.87438202 0.58778524 89.076469421 -34.6488533 0.58778524 88.96155548
		 -34.46987534 0.58778524 88.78256989 -34.3549614 0.58778524 88.55704498 -34.31536484 0.58778524 88.30704498
		 -34.45188141 0.70710677 88.088539124 -34.55231857 0.70710677 87.89141846 -34.70875549 0.70710677 87.73498535
		 -34.90587234 0.70710677 87.63454437 -35.12438202 0.70710677 87.59993744 -35.34289169 0.70710677 87.63454437
		 -35.54000854 0.70710677 87.73498535 -35.69644165 0.70710677 87.89141846 -35.79688263 0.70710677 88.088539124
		 -35.83148956 0.70710677 88.30704498 -35.79688263 0.70710677 88.52555084 -35.69644165 0.70710677 88.72267151
		 -35.54000854 0.70710677 88.87910461 -35.34289169 0.70710677 88.97954559 -35.12438202 0.70710677 89.014152527
		 -34.90587234 0.70710677 88.97954559 -34.70875549 0.70710677 88.87910461 -34.55232239 0.70710677 88.72267151
		 -34.45188522 0.70710677 88.52555084 -34.41727448 0.70710677 88.30704498 -34.56536484 0.809017 88.12541199
		 -34.6488533 0.809017 87.96155548 -34.7788887 0.809017 87.83152008 -34.94274521 0.809017 87.74802399
		 -35.12438202 0.809017 87.71926117 -35.30601883 0.809017 87.74802399 -35.46987534 0.809017 87.83152008
		 -35.59991074 0.809017 87.96155548 -35.6833992 0.809017 88.12541199 -35.71216583 0.809017 88.30704498
		 -35.6833992 0.809017 88.48867798 -35.59991074 0.809017 88.65253448 -35.46987534 0.809017 88.78256989
		 -35.30601883 0.809017 88.86605835 -35.12438202 0.809017 88.8948288 -34.94274521 0.809017 88.86605835
		 -34.7788887 0.809017 88.78256989 -34.6488533 0.809017 88.65253448 -34.56536484 0.809017 88.48867798
		 -34.53659821 0.809017 88.30704498 -34.69261169 0.89100653 88.16675568 -34.75709534 0.89100653 88.04019928
		 -34.8575325 0.89100653 87.9397583 -34.98409271 0.89100653 87.87527466 -35.12438202 0.89100653 87.85305786
		 -35.26467133 0.89100653 87.87527466 -35.39123154 0.89100653 87.9397583 -35.4916687 0.89100653 88.04019928
		 -35.55615234 0.89100653 88.16675568 -35.57837296 0.89100653 88.30704498 -35.55615234 0.89100653 88.44733429
		 -35.4916687 0.89100653 88.57389069 -35.39123154 0.89100653 88.67433167 -35.26467133 0.89100653 88.73881531
		 -35.12438202 0.89100653 88.7610321 -34.98409271 0.89100653 88.73881531 -34.8575325 0.89100653 88.67433167
		 -34.75709534 0.89100653 88.57389069 -34.69261169 0.89100653 88.44733429 -34.67039108 0.89100653 88.30704498
		 -34.83049011 0.95105654 88.21155548 -34.87438202 0.95105654 88.12541199 -34.94274521 0.95105654 88.057044983
		 -35.028888702 0.95105654 88.013153076 -35.12438202 0.95105654 87.99803162 -35.21987534 0.95105654 88.013153076
		 -35.30601883 0.95105654 88.057044983 -35.37438202 0.95105654 88.12541199 -35.41827393 0.95105654 88.21155548
		 -35.4333992 0.95105654 88.30704498 -35.41827393 0.95105654 88.40253448 -35.37438202 0.95105654 88.48867798
		 -35.30601883 0.95105654 88.55704498 -35.21987534 0.95105654 88.60093689 -35.12438202 0.95105654 88.61605835
		 -35.028888702 0.95105654 88.60093689 -34.94274521 0.95105654 88.55704498 -34.87438202 0.95105654 88.48867798
		 -34.83049011 0.95105654 88.40253448 -34.81536484 0.95105654 88.30704498 -34.97560501 0.98768836 88.25870514
		 -34.99782562 0.98768836 88.21509552 -35.032432556 0.98768836 88.18048859 -35.076042175 0.98768836 88.15826416
		 -35.12438202 0.98768836 88.15061188 -35.17272186 0.98768836 88.15826416 -35.21633148 0.98768836 88.18048859
		 -35.25093842 0.98768836 88.21509552 -35.27315903 0.98768836 88.25870514 -35.28081512 0.98768836 88.30704498
		 -35.27315903 0.98768836 88.35538483 -35.25093842 0.98768836 88.39899445 -35.21633148 0.98768836 88.43360138
		 -35.17272186 0.98768836 88.45582581 -35.12438202 0.98768836 88.46347809 -35.076042175 0.98768836 88.45582581
		 -35.032432556 0.98768836 88.43360138 -34.99782562 0.98768836 88.39899445 -34.97560501 0.98768836 88.35538483
		 -34.96794891 0.98768836 88.30704498 -35.12438202 -1 88.30704498 -35.12438202 1 88.30704498;
	setAttr -s 980 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0 0 20 1 1 21 1
		 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1
		 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1 40 3 1 40 4 1
		 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1 40 14 1 40 15 1
		 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1 25 41 1 26 41 1
		 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1 36 41 1 37 41 1
		 38 41 1 39 41 1 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 62 0 42 62 1 43 63 1
		 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1
		 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 82 42 1 82 43 1 82 44 1 82 45 1
		 82 46 1 82 47 1;
	setAttr ".ed[166:331]" 82 48 1 82 49 1 82 50 1 82 51 1 82 52 1 82 53 1 82 54 1
		 82 55 1 82 56 1 82 57 1 82 58 1 82 59 1 82 60 1 82 61 1 62 83 1 63 83 1 64 83 1 65 83 1
		 66 83 1 67 83 1 68 83 1 69 83 1 70 83 1 71 83 1 72 83 1 73 83 1 74 83 1 75 83 1 76 83 1
		 77 83 1 78 83 1 79 83 1 80 83 1 81 83 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1
		 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 84 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1
		 109 110 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 104 1 124 125 1 125 126 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 134 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 124 1 144 145 1
		 145 146 1 146 147 1 147 148 1 148 149 1 149 150 1 150 151 1 151 152 1 152 153 1 153 154 1
		 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1
		 163 144 1 164 165 1 165 166 1 166 167 1 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 180 1 180 181 1
		 181 182 1 182 183 1 183 164 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 190 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 200 1 200 201 1 201 202 1 202 203 1 203 184 1 204 205 1 205 206 1 206 207 1 207 208 1
		 208 209 1 209 210 1 210 211 1 211 212 1 212 213 1 213 214 1 214 215 1 215 216 1;
	setAttr ".ed[332:497]" 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1
		 222 223 1 223 204 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 232 1 232 233 1 233 234 1 234 235 1 235 236 1 236 237 1 237 238 1 238 239 1 239 240 1
		 240 241 1 241 242 1 242 243 1 243 224 1 244 245 1 245 246 1 246 247 1 247 248 1 248 249 1
		 249 250 1 250 251 1 251 252 1 252 253 1 253 254 1 254 255 1 255 256 1 256 257 1 257 258 1
		 258 259 1 259 260 1 260 261 1 261 262 1 262 263 1 263 244 1 264 265 1 265 266 1 266 267 1
		 267 268 1 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1 274 275 1 275 276 1
		 276 277 1 277 278 1 278 279 1 279 280 1 280 281 1 281 282 1 282 283 1 283 264 1 284 285 1
		 285 286 1 286 287 1 287 288 1 288 289 1 289 290 1 290 291 1 291 292 1 292 293 1 293 294 1
		 294 295 1 295 296 1 296 297 1 297 298 1 298 299 1 299 300 1 300 301 1 301 302 1 302 303 1
		 303 284 1 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1
		 312 313 1 313 314 1 314 315 1 315 316 1 316 317 1 317 318 1 318 319 1 319 320 1 320 321 1
		 321 322 1 322 323 1 323 304 1 324 325 1 325 326 1 326 327 1 327 328 1 328 329 1 329 330 1
		 330 331 1 331 332 1 332 333 1 333 334 1 334 335 1 335 336 1 336 337 1 337 338 1 338 339 1
		 339 340 1 340 341 1 341 342 1 342 343 1 343 324 1 344 345 1 345 346 1 346 347 1 347 348 1
		 348 349 1 349 350 1 350 351 1 351 352 1 352 353 1 353 354 1 354 355 1 355 356 1 356 357 1
		 357 358 1 358 359 1 359 360 1 360 361 1 361 362 1 362 363 1 363 344 1 364 365 1 365 366 1
		 366 367 1 367 368 1 368 369 1 369 370 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1
		 375 376 1 376 377 1 377 378 1 378 379 1 379 380 1 380 381 1 381 382 1;
	setAttr ".ed[498:663]" 382 383 1 383 364 1 384 385 1 385 386 1 386 387 1 387 388 1
		 388 389 1 389 390 1 390 391 1 391 392 1 392 393 1 393 394 1 394 395 1 395 396 1 396 397 1
		 397 398 1 398 399 1 399 400 1 400 401 1 401 402 1 402 403 1 403 384 1 404 405 1 405 406 1
		 406 407 1 407 408 1 408 409 1 409 410 1 410 411 1 411 412 1 412 413 1 413 414 1 414 415 1
		 415 416 1 416 417 1 417 418 1 418 419 1 419 420 1 420 421 1 421 422 1 422 423 1 423 404 1
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 432 1 432 433 1
		 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 440 1 440 441 1 441 442 1
		 442 443 1 443 424 1 444 445 1 445 446 1 446 447 1 447 448 1 448 449 1 449 450 1 450 451 1
		 451 452 1 452 453 1 453 454 1 454 455 1 455 456 1 456 457 1 457 458 1 458 459 1 459 460 1
		 460 461 1 461 462 1 462 463 1 463 444 1 84 104 1 85 105 1 86 106 1 87 107 1 88 108 1
		 89 109 1 90 110 1 91 111 1 92 112 1 93 113 1 94 114 1 95 115 1 96 116 1 97 117 1
		 98 118 1 99 119 1 100 120 1 101 121 1 102 122 1 103 123 1 104 124 1 105 125 1 106 126 1
		 107 127 1 108 128 1 109 129 1 110 130 1 111 131 1 112 132 1 113 133 1 114 134 1 115 135 1
		 116 136 1 117 137 1 118 138 1 119 139 1 120 140 1 121 141 1 122 142 1 123 143 1 124 144 1
		 125 145 1 126 146 1 127 147 1 128 148 1 129 149 1 130 150 1 131 151 1 132 152 1 133 153 1
		 134 154 1 135 155 1 136 156 1 137 157 1 138 158 1 139 159 1 140 160 1 141 161 1 142 162 1
		 143 163 1 144 164 1 145 165 1 146 166 1 147 167 1 148 168 1 149 169 1 150 170 1 151 171 1
		 152 172 1 153 173 1 154 174 1 155 175 1 156 176 1 157 177 1 158 178 1 159 179 1 160 180 1
		 161 181 1 162 182 1 163 183 1 164 184 1 165 185 1 166 186 1 167 187 1;
	setAttr ".ed[664:829]" 168 188 1 169 189 1 170 190 1 171 191 1 172 192 1 173 193 1
		 174 194 1 175 195 1 176 196 1 177 197 1 178 198 1 179 199 1 180 200 1 181 201 1 182 202 1
		 183 203 1 184 204 1 185 205 1 186 206 1 187 207 1 188 208 1 189 209 1 190 210 1 191 211 1
		 192 212 1 193 213 1 194 214 1 195 215 1 196 216 1 197 217 1 198 218 1 199 219 1 200 220 1
		 201 221 1 202 222 1 203 223 1 204 224 1 205 225 1 206 226 1 207 227 1 208 228 1 209 229 1
		 210 230 1 211 231 1 212 232 1 213 233 1 214 234 1 215 235 1 216 236 1 217 237 1 218 238 1
		 219 239 1 220 240 1 221 241 1 222 242 1 223 243 1 224 244 1 225 245 1 226 246 1 227 247 1
		 228 248 1 229 249 1 230 250 1 231 251 1 232 252 1 233 253 1 234 254 1 235 255 1 236 256 1
		 237 257 1 238 258 1 239 259 1 240 260 1 241 261 1 242 262 1 243 263 1 244 264 1 245 265 1
		 246 266 1 247 267 1 248 268 1 249 269 1 250 270 1 251 271 1 252 272 1 253 273 1 254 274 1
		 255 275 1 256 276 1 257 277 1 258 278 1 259 279 1 260 280 1 261 281 1 262 282 1 263 283 1
		 264 284 1 265 285 1 266 286 1 267 287 1 268 288 1 269 289 1 270 290 1 271 291 1 272 292 1
		 273 293 1 274 294 1 275 295 1 276 296 1 277 297 1 278 298 1 279 299 1 280 300 1 281 301 1
		 282 302 1 283 303 1 284 304 1 285 305 1 286 306 1 287 307 1 288 308 1 289 309 1 290 310 1
		 291 311 1 292 312 1 293 313 1 294 314 1 295 315 1 296 316 1 297 317 1 298 318 1 299 319 1
		 300 320 1 301 321 1 302 322 1 303 323 1 304 324 1 305 325 1 306 326 1 307 327 1 308 328 1
		 309 329 1 310 330 1 311 331 1 312 332 1 313 333 1 314 334 1 315 335 1 316 336 1 317 337 1
		 318 338 1 319 339 1 320 340 1 321 341 1 322 342 1 323 343 1 324 344 1 325 345 1 326 346 1
		 327 347 1 328 348 1 329 349 1 330 350 1 331 351 1 332 352 1 333 353 1;
	setAttr ".ed[830:979]" 334 354 1 335 355 1 336 356 1 337 357 1 338 358 1 339 359 1
		 340 360 1 341 361 1 342 362 1 343 363 1 344 364 1 345 365 1 346 366 1 347 367 1 348 368 1
		 349 369 1 350 370 1 351 371 1 352 372 1 353 373 1 354 374 1 355 375 1 356 376 1 357 377 1
		 358 378 1 359 379 1 360 380 1 361 381 1 362 382 1 363 383 1 364 384 1 365 385 1 366 386 1
		 367 387 1 368 388 1 369 389 1 370 390 1 371 391 1 372 392 1 373 393 1 374 394 1 375 395 1
		 376 396 1 377 397 1 378 398 1 379 399 1 380 400 1 381 401 1 382 402 1 383 403 1 384 404 1
		 385 405 1 386 406 1 387 407 1 388 408 1 389 409 1 390 410 1 391 411 1 392 412 1 393 413 1
		 394 414 1 395 415 1 396 416 1 397 417 1 398 418 1 399 419 1 400 420 1 401 421 1 402 422 1
		 403 423 1 404 424 1 405 425 1 406 426 1 407 427 1 408 428 1 409 429 1 410 430 1 411 431 1
		 412 432 1 413 433 1 414 434 1 415 435 1 416 436 1 417 437 1 418 438 1 419 439 1 420 440 1
		 421 441 1 422 442 1 423 443 1 424 444 1 425 445 1 426 446 1 427 447 1 428 448 1 429 449 1
		 430 450 1 431 451 1 432 452 1 433 453 1 434 454 1 435 455 1 436 456 1 437 457 1 438 458 1
		 439 459 1 440 460 1 441 461 1 442 462 1 443 463 1 464 84 1 464 85 1 464 86 1 464 87 1
		 464 88 1 464 89 1 464 90 1 464 91 1 464 92 1 464 93 1 464 94 1 464 95 1 464 96 1
		 464 97 1 464 98 1 464 99 1 464 100 1 464 101 1 464 102 1 464 103 1 444 465 1 445 465 1
		 446 465 1 447 465 1 448 465 1 449 465 1 450 465 1 451 465 1 452 465 1 453 465 1 454 465 1
		 455 465 1 456 465 1 457 465 1 458 465 1 459 465 1 460 465 1 461 465 1 462 465 1 463 465 1;
	setAttr -s 520 -ch 1960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80
		f 4 140 120 -142 -101
		mu 0 4 84 85 86 87
		f 4 141 121 -143 -102
		mu 0 4 87 86 88 89
		f 4 142 122 -144 -103
		mu 0 4 89 88 90 91
		f 4 143 123 -145 -104
		mu 0 4 91 90 92 93
		f 4 144 124 -146 -105
		mu 0 4 93 92 94 95
		f 4 145 125 -147 -106
		mu 0 4 95 94 96 97
		f 4 146 126 -148 -107
		mu 0 4 97 96 98 99
		f 4 147 127 -149 -108
		mu 0 4 99 98 100 101
		f 4 148 128 -150 -109
		mu 0 4 101 100 102 103
		f 4 149 129 -151 -110
		mu 0 4 103 102 104 105
		f 4 150 130 -152 -111
		mu 0 4 105 104 106 107
		f 4 151 131 -153 -112
		mu 0 4 107 106 108 109
		f 4 152 132 -154 -113
		mu 0 4 109 108 110 111
		f 4 153 133 -155 -114
		mu 0 4 111 110 112 113
		f 4 154 134 -156 -115
		mu 0 4 113 112 114 115
		f 4 155 135 -157 -116
		mu 0 4 115 114 116 117
		f 4 156 136 -158 -117
		mu 0 4 117 116 118 119
		f 4 157 137 -159 -118
		mu 0 4 119 118 120 121
		f 4 158 138 -160 -119
		mu 0 4 121 120 122 123
		f 4 159 139 -141 -120
		mu 0 4 123 122 124 125
		f 3 -162 160 100
		mu 0 3 126 127 128
		f 3 -163 161 101
		mu 0 3 129 127 126
		f 3 -164 162 102
		mu 0 3 130 127 129
		f 3 -165 163 103
		mu 0 3 131 127 130
		f 3 -166 164 104
		mu 0 3 132 127 131
		f 3 -167 165 105
		mu 0 3 133 127 132
		f 3 -168 166 106
		mu 0 3 134 127 133
		f 3 -169 167 107
		mu 0 3 135 127 134
		f 3 -170 168 108
		mu 0 3 136 127 135
		f 3 -171 169 109
		mu 0 3 137 127 136
		f 3 -172 170 110
		mu 0 3 138 127 137
		f 3 -173 171 111
		mu 0 3 139 127 138
		f 3 -174 172 112
		mu 0 3 140 127 139
		f 3 -175 173 113
		mu 0 3 141 127 140
		f 3 -176 174 114
		mu 0 3 142 127 141
		f 3 -177 175 115
		mu 0 3 143 127 142
		f 3 -178 176 116
		mu 0 3 144 127 143
		f 3 -179 177 117
		mu 0 3 145 127 144
		f 3 -180 178 118
		mu 0 3 146 127 145
		f 3 -161 179 119
		mu 0 3 128 127 146
		f 3 180 -182 -121
		mu 0 3 147 148 149
		f 3 181 -183 -122
		mu 0 3 149 148 150
		f 3 182 -184 -123
		mu 0 3 150 148 151
		f 3 183 -185 -124
		mu 0 3 151 148 152
		f 3 184 -186 -125
		mu 0 3 152 148 153
		f 3 185 -187 -126
		mu 0 3 153 148 154
		f 3 186 -188 -127
		mu 0 3 154 148 155
		f 3 187 -189 -128
		mu 0 3 155 148 156
		f 3 188 -190 -129
		mu 0 3 156 148 157
		f 3 189 -191 -130
		mu 0 3 157 148 158
		f 3 190 -192 -131
		mu 0 3 158 148 159
		f 3 191 -193 -132
		mu 0 3 159 148 160
		f 3 192 -194 -133
		mu 0 3 160 148 161
		f 3 193 -195 -134
		mu 0 3 161 148 162
		f 3 194 -196 -135
		mu 0 3 162 148 163
		f 3 195 -197 -136
		mu 0 3 163 148 164
		f 3 196 -198 -137
		mu 0 3 164 148 165
		f 3 197 -199 -138
		mu 0 3 165 148 166
		f 3 198 -200 -139
		mu 0 3 166 148 167
		f 3 199 -181 -140
		mu 0 3 167 148 147
		f 4 200 581 -221 -581
		mu 0 4 168 169 170 171
		f 4 201 582 -222 -582
		mu 0 4 169 172 173 170
		f 4 202 583 -223 -583
		mu 0 4 172 174 175 173
		f 4 203 584 -224 -584
		mu 0 4 174 176 177 175
		f 4 204 585 -225 -585
		mu 0 4 176 178 179 177
		f 4 205 586 -226 -586
		mu 0 4 178 180 181 179
		f 4 206 587 -227 -587
		mu 0 4 180 182 183 181
		f 4 207 588 -228 -588
		mu 0 4 182 184 185 183
		f 4 208 589 -229 -589
		mu 0 4 184 186 187 185
		f 4 209 590 -230 -590
		mu 0 4 186 188 189 187
		f 4 210 591 -231 -591
		mu 0 4 188 190 191 189
		f 4 211 592 -232 -592
		mu 0 4 190 192 193 191
		f 4 212 593 -233 -593
		mu 0 4 192 194 195 193
		f 4 213 594 -234 -594
		mu 0 4 194 196 197 195
		f 4 214 595 -235 -595
		mu 0 4 196 198 199 197
		f 4 215 596 -236 -596
		mu 0 4 198 200 201 199
		f 4 216 597 -237 -597
		mu 0 4 200 202 203 201
		f 4 217 598 -238 -598
		mu 0 4 202 204 205 203
		f 4 218 599 -239 -599
		mu 0 4 204 206 207 205
		f 4 219 580 -240 -600
		mu 0 4 206 208 209 207
		f 4 220 601 -241 -601
		mu 0 4 171 170 210 211
		f 4 221 602 -242 -602
		mu 0 4 170 173 212 210
		f 4 222 603 -243 -603
		mu 0 4 173 175 213 212
		f 4 223 604 -244 -604
		mu 0 4 175 177 214 213
		f 4 224 605 -245 -605
		mu 0 4 177 179 215 214
		f 4 225 606 -246 -606
		mu 0 4 179 181 216 215
		f 4 226 607 -247 -607
		mu 0 4 181 183 217 216
		f 4 227 608 -248 -608
		mu 0 4 183 185 218 217
		f 4 228 609 -249 -609
		mu 0 4 185 187 219 218
		f 4 229 610 -250 -610
		mu 0 4 187 189 220 219
		f 4 230 611 -251 -611
		mu 0 4 189 191 221 220
		f 4 231 612 -252 -612
		mu 0 4 191 193 222 221
		f 4 232 613 -253 -613
		mu 0 4 193 195 223 222
		f 4 233 614 -254 -614
		mu 0 4 195 197 224 223
		f 4 234 615 -255 -615
		mu 0 4 197 199 225 224
		f 4 235 616 -256 -616
		mu 0 4 199 201 226 225
		f 4 236 617 -257 -617
		mu 0 4 201 203 227 226
		f 4 237 618 -258 -618
		mu 0 4 203 205 228 227
		f 4 238 619 -259 -619
		mu 0 4 205 207 229 228
		f 4 239 600 -260 -620
		mu 0 4 207 209 230 229
		f 4 240 621 -261 -621
		mu 0 4 211 210 231 232
		f 4 241 622 -262 -622
		mu 0 4 210 212 233 231
		f 4 242 623 -263 -623
		mu 0 4 212 213 234 233
		f 4 243 624 -264 -624
		mu 0 4 213 214 235 234
		f 4 244 625 -265 -625
		mu 0 4 214 215 236 235
		f 4 245 626 -266 -626
		mu 0 4 215 216 237 236
		f 4 246 627 -267 -627
		mu 0 4 216 217 238 237
		f 4 247 628 -268 -628
		mu 0 4 217 218 239 238
		f 4 248 629 -269 -629
		mu 0 4 218 219 240 239
		f 4 249 630 -270 -630
		mu 0 4 219 220 241 240
		f 4 250 631 -271 -631
		mu 0 4 220 221 242 241
		f 4 251 632 -272 -632
		mu 0 4 221 222 243 242
		f 4 252 633 -273 -633
		mu 0 4 222 223 244 243
		f 4 253 634 -274 -634
		mu 0 4 223 224 245 244
		f 4 254 635 -275 -635
		mu 0 4 224 225 246 245
		f 4 255 636 -276 -636
		mu 0 4 225 226 247 246
		f 4 256 637 -277 -637
		mu 0 4 226 227 248 247
		f 4 257 638 -278 -638
		mu 0 4 227 228 249 248
		f 4 258 639 -279 -639
		mu 0 4 228 229 250 249
		f 4 259 620 -280 -640
		mu 0 4 229 230 251 250
		f 4 260 641 -281 -641
		mu 0 4 232 231 252 253
		f 4 261 642 -282 -642
		mu 0 4 231 233 254 252
		f 4 262 643 -283 -643
		mu 0 4 233 234 255 254
		f 4 263 644 -284 -644
		mu 0 4 234 235 256 255
		f 4 264 645 -285 -645
		mu 0 4 235 236 257 256
		f 4 265 646 -286 -646
		mu 0 4 236 237 258 257
		f 4 266 647 -287 -647
		mu 0 4 237 238 259 258
		f 4 267 648 -288 -648
		mu 0 4 238 239 260 259
		f 4 268 649 -289 -649
		mu 0 4 239 240 261 260
		f 4 269 650 -290 -650
		mu 0 4 240 241 262 261
		f 4 270 651 -291 -651
		mu 0 4 241 242 263 262
		f 4 271 652 -292 -652
		mu 0 4 242 243 264 263
		f 4 272 653 -293 -653
		mu 0 4 243 244 265 264
		f 4 273 654 -294 -654
		mu 0 4 244 245 266 265
		f 4 274 655 -295 -655
		mu 0 4 245 246 267 266
		f 4 275 656 -296 -656
		mu 0 4 246 247 268 267
		f 4 276 657 -297 -657
		mu 0 4 247 248 269 268
		f 4 277 658 -298 -658
		mu 0 4 248 249 270 269
		f 4 278 659 -299 -659
		mu 0 4 249 250 271 270
		f 4 279 640 -300 -660
		mu 0 4 250 251 272 271
		f 4 280 661 -301 -661
		mu 0 4 253 252 273 274
		f 4 281 662 -302 -662
		mu 0 4 252 254 275 273
		f 4 282 663 -303 -663
		mu 0 4 254 255 276 275
		f 4 283 664 -304 -664
		mu 0 4 255 256 277 276
		f 4 284 665 -305 -665
		mu 0 4 256 257 278 277
		f 4 285 666 -306 -666
		mu 0 4 257 258 279 278
		f 4 286 667 -307 -667
		mu 0 4 258 259 280 279
		f 4 287 668 -308 -668
		mu 0 4 259 260 281 280
		f 4 288 669 -309 -669
		mu 0 4 260 261 282 281
		f 4 289 670 -310 -670
		mu 0 4 261 262 283 282
		f 4 290 671 -311 -671
		mu 0 4 262 263 284 283
		f 4 291 672 -312 -672
		mu 0 4 263 264 285 284
		f 4 292 673 -313 -673
		mu 0 4 264 265 286 285
		f 4 293 674 -314 -674
		mu 0 4 265 266 287 286
		f 4 294 675 -315 -675
		mu 0 4 266 267 288 287
		f 4 295 676 -316 -676
		mu 0 4 267 268 289 288
		f 4 296 677 -317 -677
		mu 0 4 268 269 290 289
		f 4 297 678 -318 -678
		mu 0 4 269 270 291 290
		f 4 298 679 -319 -679
		mu 0 4 270 271 292 291
		f 4 299 660 -320 -680
		mu 0 4 271 272 293 292
		f 4 300 681 -321 -681
		mu 0 4 274 273 294 295
		f 4 301 682 -322 -682
		mu 0 4 273 275 296 294
		f 4 302 683 -323 -683
		mu 0 4 275 276 297 296
		f 4 303 684 -324 -684
		mu 0 4 276 277 298 297
		f 4 304 685 -325 -685
		mu 0 4 277 278 299 298
		f 4 305 686 -326 -686
		mu 0 4 278 279 300 299
		f 4 306 687 -327 -687
		mu 0 4 279 280 301 300
		f 4 307 688 -328 -688
		mu 0 4 280 281 302 301
		f 4 308 689 -329 -689
		mu 0 4 281 282 303 302
		f 4 309 690 -330 -690
		mu 0 4 282 283 304 303
		f 4 310 691 -331 -691
		mu 0 4 283 284 305 304
		f 4 311 692 -332 -692
		mu 0 4 284 285 306 305
		f 4 312 693 -333 -693
		mu 0 4 285 286 307 306
		f 4 313 694 -334 -694
		mu 0 4 286 287 308 307
		f 4 314 695 -335 -695
		mu 0 4 287 288 309 308
		f 4 315 696 -336 -696
		mu 0 4 288 289 310 309
		f 4 316 697 -337 -697
		mu 0 4 289 290 311 310
		f 4 317 698 -338 -698
		mu 0 4 290 291 312 311
		f 4 318 699 -339 -699
		mu 0 4 291 292 313 312
		f 4 319 680 -340 -700
		mu 0 4 292 293 314 313
		f 4 320 701 -341 -701
		mu 0 4 295 294 315 316
		f 4 321 702 -342 -702
		mu 0 4 294 296 317 315
		f 4 322 703 -343 -703
		mu 0 4 296 297 318 317
		f 4 323 704 -344 -704
		mu 0 4 297 298 319 318
		f 4 324 705 -345 -705
		mu 0 4 298 299 320 319
		f 4 325 706 -346 -706
		mu 0 4 299 300 321 320
		f 4 326 707 -347 -707
		mu 0 4 300 301 322 321
		f 4 327 708 -348 -708
		mu 0 4 301 302 323 322
		f 4 328 709 -349 -709
		mu 0 4 302 303 324 323
		f 4 329 710 -350 -710
		mu 0 4 303 304 325 324
		f 4 330 711 -351 -711
		mu 0 4 304 305 326 325
		f 4 331 712 -352 -712
		mu 0 4 305 306 327 326
		f 4 332 713 -353 -713
		mu 0 4 306 307 328 327
		f 4 333 714 -354 -714
		mu 0 4 307 308 329 328
		f 4 334 715 -355 -715
		mu 0 4 308 309 330 329
		f 4 335 716 -356 -716
		mu 0 4 309 310 331 330
		f 4 336 717 -357 -717
		mu 0 4 310 311 332 331
		f 4 337 718 -358 -718
		mu 0 4 311 312 333 332
		f 4 338 719 -359 -719
		mu 0 4 312 313 334 333
		f 4 339 700 -360 -720
		mu 0 4 313 314 335 334
		f 4 340 721 -361 -721
		mu 0 4 316 315 336 337
		f 4 341 722 -362 -722
		mu 0 4 315 317 338 336
		f 4 342 723 -363 -723
		mu 0 4 317 318 339 338
		f 4 343 724 -364 -724
		mu 0 4 318 319 340 339
		f 4 344 725 -365 -725
		mu 0 4 319 320 341 340
		f 4 345 726 -366 -726
		mu 0 4 320 321 342 341
		f 4 346 727 -367 -727
		mu 0 4 321 322 343 342
		f 4 347 728 -368 -728
		mu 0 4 322 323 344 343
		f 4 348 729 -369 -729
		mu 0 4 323 324 345 344
		f 4 349 730 -370 -730
		mu 0 4 324 325 346 345
		f 4 350 731 -371 -731
		mu 0 4 325 326 347 346
		f 4 351 732 -372 -732
		mu 0 4 326 327 348 347
		f 4 352 733 -373 -733
		mu 0 4 327 328 349 348
		f 4 353 734 -374 -734
		mu 0 4 328 329 350 349
		f 4 354 735 -375 -735
		mu 0 4 329 330 351 350
		f 4 355 736 -376 -736
		mu 0 4 330 331 352 351
		f 4 356 737 -377 -737
		mu 0 4 331 332 353 352
		f 4 357 738 -378 -738
		mu 0 4 332 333 354 353
		f 4 358 739 -379 -739
		mu 0 4 333 334 355 354
		f 4 359 720 -380 -740
		mu 0 4 334 335 356 355
		f 4 360 741 -381 -741
		mu 0 4 337 336 357 358
		f 4 361 742 -382 -742
		mu 0 4 336 338 359 357
		f 4 362 743 -383 -743
		mu 0 4 338 339 360 359
		f 4 363 744 -384 -744
		mu 0 4 339 340 361 360
		f 4 364 745 -385 -745
		mu 0 4 340 341 362 361
		f 4 365 746 -386 -746
		mu 0 4 341 342 363 362
		f 4 366 747 -387 -747
		mu 0 4 342 343 364 363
		f 4 367 748 -388 -748
		mu 0 4 343 344 365 364
		f 4 368 749 -389 -749
		mu 0 4 344 345 366 365
		f 4 369 750 -390 -750
		mu 0 4 345 346 367 366
		f 4 370 751 -391 -751
		mu 0 4 346 347 368 367
		f 4 371 752 -392 -752
		mu 0 4 347 348 369 368
		f 4 372 753 -393 -753
		mu 0 4 348 349 370 369
		f 4 373 754 -394 -754
		mu 0 4 349 350 371 370
		f 4 374 755 -395 -755
		mu 0 4 350 351 372 371
		f 4 375 756 -396 -756
		mu 0 4 351 352 373 372
		f 4 376 757 -397 -757
		mu 0 4 352 353 374 373
		f 4 377 758 -398 -758
		mu 0 4 353 354 375 374
		f 4 378 759 -399 -759
		mu 0 4 354 355 376 375
		f 4 379 740 -400 -760
		mu 0 4 355 356 377 376
		f 4 380 761 -401 -761
		mu 0 4 358 357 378 379
		f 4 381 762 -402 -762
		mu 0 4 357 359 380 378
		f 4 382 763 -403 -763
		mu 0 4 359 360 381 380
		f 4 383 764 -404 -764
		mu 0 4 360 361 382 381
		f 4 384 765 -405 -765
		mu 0 4 361 362 383 382
		f 4 385 766 -406 -766
		mu 0 4 362 363 384 383
		f 4 386 767 -407 -767
		mu 0 4 363 364 385 384
		f 4 387 768 -408 -768
		mu 0 4 364 365 386 385
		f 4 388 769 -409 -769
		mu 0 4 365 366 387 386
		f 4 389 770 -410 -770
		mu 0 4 366 367 388 387
		f 4 390 771 -411 -771
		mu 0 4 367 368 389 388
		f 4 391 772 -412 -772
		mu 0 4 368 369 390 389
		f 4 392 773 -413 -773
		mu 0 4 369 370 391 390
		f 4 393 774 -414 -774
		mu 0 4 370 371 392 391
		f 4 394 775 -415 -775
		mu 0 4 371 372 393 392
		f 4 395 776 -416 -776
		mu 0 4 372 373 394 393
		f 4 396 777 -417 -777
		mu 0 4 373 374 395 394
		f 4 397 778 -418 -778
		mu 0 4 374 375 396 395
		f 4 398 779 -419 -779
		mu 0 4 375 376 397 396
		f 4 399 760 -420 -780
		mu 0 4 376 377 398 397
		f 4 400 781 -421 -781
		mu 0 4 379 378 399 400
		f 4 401 782 -422 -782
		mu 0 4 378 380 401 399
		f 4 402 783 -423 -783
		mu 0 4 380 381 402 401
		f 4 403 784 -424 -784
		mu 0 4 381 382 403 402
		f 4 404 785 -425 -785
		mu 0 4 382 383 404 403
		f 4 405 786 -426 -786
		mu 0 4 383 384 405 404
		f 4 406 787 -427 -787
		mu 0 4 384 385 406 405
		f 4 407 788 -428 -788
		mu 0 4 385 386 407 406
		f 4 408 789 -429 -789
		mu 0 4 386 387 408 407
		f 4 409 790 -430 -790
		mu 0 4 387 388 409 408
		f 4 410 791 -431 -791
		mu 0 4 388 389 410 409
		f 4 411 792 -432 -792
		mu 0 4 389 390 411 410
		f 4 412 793 -433 -793
		mu 0 4 390 391 412 411
		f 4 413 794 -434 -794
		mu 0 4 391 392 413 412
		f 4 414 795 -435 -795
		mu 0 4 392 393 414 413
		f 4 415 796 -436 -796
		mu 0 4 393 394 415 414
		f 4 416 797 -437 -797
		mu 0 4 394 395 416 415
		f 4 417 798 -438 -798
		mu 0 4 395 396 417 416
		f 4 418 799 -439 -799
		mu 0 4 396 397 418 417
		f 4 419 780 -440 -800
		mu 0 4 397 398 419 418
		f 4 420 801 -441 -801
		mu 0 4 400 399 420 421
		f 4 421 802 -442 -802
		mu 0 4 399 401 422 420
		f 4 422 803 -443 -803
		mu 0 4 401 402 423 422
		f 4 423 804 -444 -804
		mu 0 4 402 403 424 423
		f 4 424 805 -445 -805
		mu 0 4 403 404 425 424
		f 4 425 806 -446 -806
		mu 0 4 404 405 426 425
		f 4 426 807 -447 -807
		mu 0 4 405 406 427 426
		f 4 427 808 -448 -808
		mu 0 4 406 407 428 427
		f 4 428 809 -449 -809
		mu 0 4 407 408 429 428
		f 4 429 810 -450 -810
		mu 0 4 408 409 430 429
		f 4 430 811 -451 -811
		mu 0 4 409 410 431 430
		f 4 431 812 -452 -812
		mu 0 4 410 411 432 431
		f 4 432 813 -453 -813
		mu 0 4 411 412 433 432
		f 4 433 814 -454 -814
		mu 0 4 412 413 434 433
		f 4 434 815 -455 -815
		mu 0 4 413 414 435 434
		f 4 435 816 -456 -816
		mu 0 4 414 415 436 435
		f 4 436 817 -457 -817
		mu 0 4 415 416 437 436
		f 4 437 818 -458 -818
		mu 0 4 416 417 438 437
		f 4 438 819 -459 -819
		mu 0 4 417 418 439 438
		f 4 439 800 -460 -820
		mu 0 4 418 419 440 439
		f 4 440 821 -461 -821
		mu 0 4 421 420 441 442
		f 4 441 822 -462 -822
		mu 0 4 420 422 443 441
		f 4 442 823 -463 -823
		mu 0 4 422 423 444 443
		f 4 443 824 -464 -824
		mu 0 4 423 424 445 444
		f 4 444 825 -465 -825
		mu 0 4 424 425 446 445
		f 4 445 826 -466 -826
		mu 0 4 425 426 447 446
		f 4 446 827 -467 -827
		mu 0 4 426 427 448 447
		f 4 447 828 -468 -828
		mu 0 4 427 428 449 448
		f 4 448 829 -469 -829
		mu 0 4 428 429 450 449
		f 4 449 830 -470 -830
		mu 0 4 429 430 451 450
		f 4 450 831 -471 -831
		mu 0 4 430 431 452 451
		f 4 451 832 -472 -832
		mu 0 4 431 432 453 452
		f 4 452 833 -473 -833
		mu 0 4 432 433 454 453
		f 4 453 834 -474 -834
		mu 0 4 433 434 455 454
		f 4 454 835 -475 -835
		mu 0 4 434 435 456 455
		f 4 455 836 -476 -836
		mu 0 4 435 436 457 456
		f 4 456 837 -477 -837
		mu 0 4 436 437 458 457
		f 4 457 838 -478 -838
		mu 0 4 437 438 459 458
		f 4 458 839 -479 -839
		mu 0 4 438 439 460 459
		f 4 459 820 -480 -840
		mu 0 4 439 440 461 460
		f 4 460 841 -481 -841
		mu 0 4 442 441 462 463
		f 4 461 842 -482 -842
		mu 0 4 441 443 464 462
		f 4 462 843 -483 -843
		mu 0 4 443 444 465 464
		f 4 463 844 -484 -844
		mu 0 4 444 445 466 465
		f 4 464 845 -485 -845
		mu 0 4 445 446 467 466
		f 4 465 846 -486 -846
		mu 0 4 446 447 468 467
		f 4 466 847 -487 -847
		mu 0 4 447 448 469 468
		f 4 467 848 -488 -848
		mu 0 4 448 449 470 469
		f 4 468 849 -489 -849
		mu 0 4 449 450 471 470
		f 4 469 850 -490 -850
		mu 0 4 450 451 472 471
		f 4 470 851 -491 -851
		mu 0 4 451 452 473 472
		f 4 471 852 -492 -852
		mu 0 4 452 453 474 473
		f 4 472 853 -493 -853
		mu 0 4 453 454 475 474
		f 4 473 854 -494 -854
		mu 0 4 454 455 476 475
		f 4 474 855 -495 -855
		mu 0 4 455 456 477 476
		f 4 475 856 -496 -856
		mu 0 4 456 457 478 477
		f 4 476 857 -497 -857
		mu 0 4 457 458 479 478
		f 4 477 858 -498 -858
		mu 0 4 458 459 480 479
		f 4 478 859 -499 -859
		mu 0 4 459 460 481 480
		f 4 479 840 -500 -860
		mu 0 4 460 461 482 481
		f 4 480 861 -501 -861
		mu 0 4 463 462 483 484
		f 4 481 862 -502 -862
		mu 0 4 462 464 485 483
		f 4 482 863 -503 -863
		mu 0 4 464 465 486 485
		f 4 483 864 -504 -864
		mu 0 4 465 466 487 486
		f 4 484 865 -505 -865
		mu 0 4 466 467 488 487
		f 4 485 866 -506 -866
		mu 0 4 467 468 489 488
		f 4 486 867 -507 -867
		mu 0 4 468 469 490 489
		f 4 487 868 -508 -868
		mu 0 4 469 470 491 490
		f 4 488 869 -509 -869
		mu 0 4 470 471 492 491
		f 4 489 870 -510 -870
		mu 0 4 471 472 493 492
		f 4 490 871 -511 -871
		mu 0 4 472 473 494 493
		f 4 491 872 -512 -872
		mu 0 4 473 474 495 494
		f 4 492 873 -513 -873
		mu 0 4 474 475 496 495
		f 4 493 874 -514 -874
		mu 0 4 475 476 497 496
		f 4 494 875 -515 -875
		mu 0 4 476 477 498 497
		f 4 495 876 -516 -876
		mu 0 4 477 478 499 498
		f 4 496 877 -517 -877
		mu 0 4 478 479 500 499
		f 4 497 878 -518 -878
		mu 0 4 479 480 501 500
		f 4 498 879 -519 -879
		mu 0 4 480 481 502 501
		f 4 499 860 -520 -880
		mu 0 4 481 482 503 502
		f 4 500 881 -521 -881
		mu 0 4 484 483 504 505
		f 4 501 882 -522 -882
		mu 0 4 483 485 506 504
		f 4 502 883 -523 -883
		mu 0 4 485 486 507 506
		f 4 503 884 -524 -884
		mu 0 4 486 487 508 507
		f 4 504 885 -525 -885
		mu 0 4 487 488 509 508
		f 4 505 886 -526 -886
		mu 0 4 488 489 510 509
		f 4 506 887 -527 -887
		mu 0 4 489 490 511 510
		f 4 507 888 -528 -888
		mu 0 4 490 491 512 511
		f 4 508 889 -529 -889
		mu 0 4 491 492 513 512
		f 4 509 890 -530 -890
		mu 0 4 492 493 514 513
		f 4 510 891 -531 -891
		mu 0 4 493 494 515 514
		f 4 511 892 -532 -892
		mu 0 4 494 495 516 515
		f 4 512 893 -533 -893
		mu 0 4 495 496 517 516
		f 4 513 894 -534 -894
		mu 0 4 496 497 518 517
		f 4 514 895 -535 -895
		mu 0 4 497 498 519 518
		f 4 515 896 -536 -896
		mu 0 4 498 499 520 519
		f 4 516 897 -537 -897
		mu 0 4 499 500 521 520
		f 4 517 898 -538 -898
		mu 0 4 500 501 522 521
		f 4 518 899 -539 -899
		mu 0 4 501 502 523 522
		f 4 519 880 -540 -900
		mu 0 4 502 503 524 523
		f 4 520 901 -541 -901
		mu 0 4 505 504 525 526
		f 4 521 902 -542 -902
		mu 0 4 504 506 527 525
		f 4 522 903 -543 -903
		mu 0 4 506 507 528 527
		f 4 523 904 -544 -904
		mu 0 4 507 508 529 528
		f 4 524 905 -545 -905
		mu 0 4 508 509 530 529
		f 4 525 906 -546 -906
		mu 0 4 509 510 531 530
		f 4 526 907 -547 -907
		mu 0 4 510 511 532 531
		f 4 527 908 -548 -908
		mu 0 4 511 512 533 532
		f 4 528 909 -549 -909
		mu 0 4 512 513 534 533
		f 4 529 910 -550 -910
		mu 0 4 513 514 535 534
		f 4 530 911 -551 -911
		mu 0 4 514 515 536 535
		f 4 531 912 -552 -912
		mu 0 4 515 516 537 536
		f 4 532 913 -553 -913
		mu 0 4 516 517 538 537
		f 4 533 914 -554 -914
		mu 0 4 517 518 539 538
		f 4 534 915 -555 -915
		mu 0 4 518 519 540 539
		f 4 535 916 -556 -916
		mu 0 4 519 520 541 540
		f 4 536 917 -557 -917
		mu 0 4 520 521 542 541
		f 4 537 918 -558 -918
		mu 0 4 521 522 543 542
		f 4 538 919 -559 -919
		mu 0 4 522 523 544 543
		f 4 539 900 -560 -920
		mu 0 4 523 524 545 544
		f 4 540 921 -561 -921
		mu 0 4 526 525 546 547
		f 4 541 922 -562 -922
		mu 0 4 525 527 548 546
		f 4 542 923 -563 -923
		mu 0 4 527 528 549 548
		f 4 543 924 -564 -924
		mu 0 4 528 529 550 549
		f 4 544 925 -565 -925
		mu 0 4 529 530 551 550
		f 4 545 926 -566 -926
		mu 0 4 530 531 552 551
		f 4 546 927 -567 -927
		mu 0 4 531 532 553 552
		f 4 547 928 -568 -928
		mu 0 4 532 533 554 553
		f 4 548 929 -569 -929
		mu 0 4 533 534 555 554
		f 4 549 930 -570 -930
		mu 0 4 534 535 556 555
		f 4 550 931 -571 -931
		mu 0 4 535 536 557 556
		f 4 551 932 -572 -932
		mu 0 4 536 537 558 557
		f 4 552 933 -573 -933
		mu 0 4 537 538 559 558
		f 4 553 934 -574 -934
		mu 0 4 538 539 560 559
		f 4 554 935 -575 -935
		mu 0 4 539 540 561 560
		f 4 555 936 -576 -936
		mu 0 4 540 541 562 561
		f 4 556 937 -577 -937
		mu 0 4 541 542 563 562
		f 4 557 938 -578 -938
		mu 0 4 542 543 564 563
		f 4 558 939 -579 -939
		mu 0 4 543 544 565 564
		f 4 559 920 -580 -940
		mu 0 4 544 545 566 565
		f 3 -201 -941 941
		mu 0 3 169 168 567
		f 3 -202 -942 942
		mu 0 3 172 169 568
		f 3 -203 -943 943
		mu 0 3 174 172 569
		f 3 -204 -944 944
		mu 0 3 176 174 570
		f 3 -205 -945 945
		mu 0 3 178 176 571
		f 3 -206 -946 946
		mu 0 3 180 178 572
		f 3 -207 -947 947
		mu 0 3 182 180 573
		f 3 -208 -948 948
		mu 0 3 184 182 574
		f 3 -209 -949 949
		mu 0 3 186 184 575
		f 3 -210 -950 950
		mu 0 3 188 186 576
		f 3 -211 -951 951
		mu 0 3 190 188 577
		f 3 -212 -952 952
		mu 0 3 192 190 578
		f 3 -213 -953 953
		mu 0 3 194 192 579
		f 3 -214 -954 954
		mu 0 3 196 194 580
		f 3 -215 -955 955
		mu 0 3 198 196 581
		f 3 -216 -956 956
		mu 0 3 200 198 582
		f 3 -217 -957 957
		mu 0 3 202 200 583
		f 3 -218 -958 958
		mu 0 3 204 202 584
		f 3 -219 -959 959
		mu 0 3 206 204 585
		f 3 -220 -960 940
		mu 0 3 208 206 586;
	setAttr ".fc[500:519]"
		f 3 560 961 -961
		mu 0 3 547 546 587
		f 3 561 962 -962
		mu 0 3 546 548 588
		f 3 562 963 -963
		mu 0 3 548 549 589
		f 3 563 964 -964
		mu 0 3 549 550 590
		f 3 564 965 -965
		mu 0 3 550 551 591
		f 3 565 966 -966
		mu 0 3 551 552 592
		f 3 566 967 -967
		mu 0 3 552 553 593
		f 3 567 968 -968
		mu 0 3 553 554 594
		f 3 568 969 -969
		mu 0 3 554 555 595
		f 3 569 970 -970
		mu 0 3 555 556 596
		f 3 570 971 -971
		mu 0 3 556 557 597
		f 3 571 972 -972
		mu 0 3 557 558 598
		f 3 572 973 -973
		mu 0 3 558 559 599
		f 3 573 974 -974
		mu 0 3 559 560 600
		f 3 574 975 -975
		mu 0 3 560 561 601
		f 3 575 976 -976
		mu 0 3 561 562 602
		f 3 576 977 -977
		mu 0 3 562 563 603
		f 3 577 978 -978
		mu 0 3 563 564 604
		f 3 578 979 -979
		mu 0 3 564 565 605
		f 3 579 960 -980
		mu 0 3 565 566 606;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder12";
	rename -uid "FF761E7F-4505-4B05-EC77-1793EEB0F425";
	setAttr ".t" -type "double3" 24.507762122492004 7.3057473987216284 30.451090949824703 ;
	setAttr ".r" -type "double3" 8.3512114485640208 -77.471296561390318 160.2561599892226 ;
	setAttr ".s" -type "double3" 0.57444915339394054 0.57444915339394054 0.57444915339394054 ;
	setAttr ".rp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
	setAttr ".sp" -type "double3" -34.87483024597168 0.75959277153015137 88.761695861816406 ;
createNode mesh -n "pCylinder12Shape" -p "pCylinder12";
	rename -uid "B62ECBFB-4CE8-C97A-55A2-898AE778B069";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:519]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 607 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.64860266 0.10796607 0.62640899
		 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607
		 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997
		 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161
		 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146
		 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998 0.3125
		 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.375
		 0.3125 0.375 0.68843985 0.38749999 0.68843985 0.38749999 0.3125 0.39999998 0.68843985
		 0.39999998 0.3125 0.41249996 0.68843985 0.41249996 0.3125 0.42499995 0.68843985 0.42499995
		 0.3125 0.43749994 0.68843985 0.43749994 0.3125 0.44999993 0.68843985 0.44999993 0.3125
		 0.46249992 0.68843985 0.46249992 0.3125 0.4749999 0.68843985 0.4749999 0.3125 0.48749989
		 0.68843985 0.48749989 0.3125 0.49999988 0.68843985 0.49999988 0.3125 0.51249987 0.68843985
		 0.51249987 0.3125 0.52499986 0.68843985 0.52499986 0.3125 0.53749985 0.68843985 0.53749985
		 0.3125 0.54999983 0.68843985 0.54999983 0.3125 0.56249982 0.68843985 0.56249982 0.3125
		 0.57499981 0.68843985 0.57499981 0.3125 0.5874998 0.68843985 0.5874998 0.3125 0.59999979
		 0.68843985 0.59999979 0.3125 0.61249977 0.68843985 0.61249977 0.3125 0.62499976 0.68843985
		 0.62499976 0.3125 0.62640899 0.064408496 0.5 0.15000001 0.64860266 0.10796607 0.59184152
		 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-008 0.45171607 0.0076473504 0.40815851
		 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608 0.34374997 0.15625 0.3513974
		 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893 0.4517161 0.3048526 0.5 0.3125
		 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893 0.24809146 0.6486026 0.2045339
		 0.65625 0.15625 0.6486026 0.89203393 0.5 0.83749998 0.62640893 0.93559146 0.59184146
		 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893 0.37359107
		 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607 0.37359107
		 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994 0.54828393
		 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607 0.65625
		 0.84375 0 0.050000001 0.050000001 0.050000001 0.050000001 0.1 0 0.1 0.1 0.050000001
		 0.1 0.1 0.15000001 0.050000001 0.15000001 0.1 0.2 0.050000001 0.2 0.1 0.25 0.050000001
		 0.25 0.1 0.30000001 0.050000001 0.30000001 0.1 0.35000002 0.050000001 0.35000002
		 0.1 0.40000004 0.050000001 0.40000004 0.1 0.45000005 0.050000001 0.45000005 0.1 0.50000006
		 0.050000001 0.50000006 0.1 0.55000007 0.050000001 0.55000007 0.1 0.60000008 0.050000001
		 0.60000008 0.1 0.6500001 0.050000001 0.6500001 0.1 0.70000011 0.050000001 0.70000011
		 0.1 0.75000012 0.050000001 0.75000012 0.1 0.80000013 0.050000001 0.80000013 0.1 0.85000014
		 0.050000001 0.85000014 0.1 0.90000015 0.050000001 0.90000015 0.1 0.95000017 0.050000001
		 0.95000017 0.1 1.000000119209 0.050000001 1.000000119209 0.1 0.050000001 0.15000001
		 0 0.15000001 0.1 0.15000001 0.15000001 0.15000001 0.2 0.15000001 0.25 0.15000001
		 0.30000001 0.15000001 0.35000002 0.15000001 0.40000004 0.15000001 0.45000005 0.15000001
		 0.50000006 0.15000001 0.55000007 0.15000001 0.60000008 0.15000001 0.6500001 0.15000001
		 0.70000011 0.15000001 0.75000012 0.15000001 0.80000013 0.15000001 0.85000014 0.15000001
		 0.90000015 0.15000001 0.95000017 0.15000001 1.000000119209 0.15000001 0.050000001
		 0.2 0 0.2 0.1 0.2 0.15000001 0.2 0.2 0.2 0.25 0.2 0.30000001 0.2 0.35000002 0.2 0.40000004
		 0.2 0.45000005 0.2 0.50000006 0.2 0.55000007 0.2 0.60000008 0.2 0.6500001 0.2 0.70000011
		 0.2 0.75000012 0.2 0.80000013 0.2 0.85000014 0.2 0.90000015 0.2;
	setAttr ".uvst[0].uvsp[250:499]" 0.95000017 0.2 1.000000119209 0.2 0.050000001
		 0.25 0 0.25 0.1 0.25 0.15000001 0.25 0.2 0.25 0.25 0.25 0.30000001 0.25 0.35000002
		 0.25 0.40000004 0.25 0.45000005 0.25 0.50000006 0.25 0.55000007 0.25 0.60000008 0.25
		 0.6500001 0.25 0.70000011 0.25 0.75000012 0.25 0.80000013 0.25 0.85000014 0.25 0.90000015
		 0.25 0.95000017 0.25 1.000000119209 0.25 0.050000001 0.30000001 0 0.30000001 0.1
		 0.30000001 0.15000001 0.30000001 0.2 0.30000001 0.25 0.30000001 0.30000001 0.30000001
		 0.35000002 0.30000001 0.40000004 0.30000001 0.45000005 0.30000001 0.50000006 0.30000001
		 0.55000007 0.30000001 0.60000008 0.30000001 0.6500001 0.30000001 0.70000011 0.30000001
		 0.75000012 0.30000001 0.80000013 0.30000001 0.85000014 0.30000001 0.90000015 0.30000001
		 0.95000017 0.30000001 1.000000119209 0.30000001 0.050000001 0.35000002 0 0.35000002
		 0.1 0.35000002 0.15000001 0.35000002 0.2 0.35000002 0.25 0.35000002 0.30000001 0.35000002
		 0.35000002 0.35000002 0.40000004 0.35000002 0.45000005 0.35000002 0.50000006 0.35000002
		 0.55000007 0.35000002 0.60000008 0.35000002 0.6500001 0.35000002 0.70000011 0.35000002
		 0.75000012 0.35000002 0.80000013 0.35000002 0.85000014 0.35000002 0.90000015 0.35000002
		 0.95000017 0.35000002 1.000000119209 0.35000002 0.050000001 0.40000004 0 0.40000004
		 0.1 0.40000004 0.15000001 0.40000004 0.2 0.40000004 0.25 0.40000004 0.30000001 0.40000004
		 0.35000002 0.40000004 0.40000004 0.40000004 0.45000005 0.40000004 0.50000006 0.40000004
		 0.55000007 0.40000004 0.60000008 0.40000004 0.6500001 0.40000004 0.70000011 0.40000004
		 0.75000012 0.40000004 0.80000013 0.40000004 0.85000014 0.40000004 0.90000015 0.40000004
		 0.95000017 0.40000004 1.000000119209 0.40000004 0.050000001 0.45000005 0 0.45000005
		 0.1 0.45000005 0.15000001 0.45000005 0.2 0.45000005 0.25 0.45000005 0.30000001 0.45000005
		 0.35000002 0.45000005 0.40000004 0.45000005 0.45000005 0.45000005 0.50000006 0.45000005
		 0.55000007 0.45000005 0.60000008 0.45000005 0.6500001 0.45000005 0.70000011 0.45000005
		 0.75000012 0.45000005 0.80000013 0.45000005 0.85000014 0.45000005 0.90000015 0.45000005
		 0.95000017 0.45000005 1.000000119209 0.45000005 0.050000001 0.50000006 0 0.50000006
		 0.1 0.50000006 0.15000001 0.50000006 0.2 0.50000006 0.25 0.50000006 0.30000001 0.50000006
		 0.35000002 0.50000006 0.40000004 0.50000006 0.45000005 0.50000006 0.50000006 0.50000006
		 0.55000007 0.50000006 0.60000008 0.50000006 0.6500001 0.50000006 0.70000011 0.50000006
		 0.75000012 0.50000006 0.80000013 0.50000006 0.85000014 0.50000006 0.90000015 0.50000006
		 0.95000017 0.50000006 1.000000119209 0.50000006 0.050000001 0.55000007 0 0.55000007
		 0.1 0.55000007 0.15000001 0.55000007 0.2 0.55000007 0.25 0.55000007 0.30000001 0.55000007
		 0.35000002 0.55000007 0.40000004 0.55000007 0.45000005 0.55000007 0.50000006 0.55000007
		 0.55000007 0.55000007 0.60000008 0.55000007 0.6500001 0.55000007 0.70000011 0.55000007
		 0.75000012 0.55000007 0.80000013 0.55000007 0.85000014 0.55000007 0.90000015 0.55000007
		 0.95000017 0.55000007 1.000000119209 0.55000007 0.050000001 0.60000008 0 0.60000008
		 0.1 0.60000008 0.15000001 0.60000008 0.2 0.60000008 0.25 0.60000008 0.30000001 0.60000008
		 0.35000002 0.60000008 0.40000004 0.60000008 0.45000005 0.60000008 0.50000006 0.60000008
		 0.55000007 0.60000008 0.60000008 0.60000008 0.6500001 0.60000008 0.70000011 0.60000008
		 0.75000012 0.60000008 0.80000013 0.60000008 0.85000014 0.60000008 0.90000015 0.60000008
		 0.95000017 0.60000008 1.000000119209 0.60000008 0.050000001 0.6500001 0 0.6500001
		 0.1 0.6500001 0.15000001 0.6500001 0.2 0.6500001 0.25 0.6500001 0.30000001 0.6500001
		 0.35000002 0.6500001 0.40000004 0.6500001 0.45000005 0.6500001 0.50000006 0.6500001
		 0.55000007 0.6500001 0.60000008 0.6500001 0.6500001 0.6500001 0.70000011 0.6500001
		 0.75000012 0.6500001 0.80000013 0.6500001 0.85000014 0.6500001 0.90000015 0.6500001
		 0.95000017 0.6500001 1.000000119209 0.6500001 0.050000001 0.70000011 0 0.70000011
		 0.1 0.70000011 0.15000001 0.70000011 0.2 0.70000011 0.25 0.70000011 0.30000001 0.70000011
		 0.35000002 0.70000011 0.40000004 0.70000011 0.45000005 0.70000011 0.50000006 0.70000011
		 0.55000007 0.70000011 0.60000008 0.70000011 0.6500001 0.70000011 0.70000011 0.70000011
		 0.75000012 0.70000011 0.80000013 0.70000011 0.85000014 0.70000011 0.90000015 0.70000011
		 0.95000017 0.70000011 1.000000119209 0.70000011 0.050000001 0.75000012 0 0.75000012
		 0.1 0.75000012 0.15000001 0.75000012 0.2 0.75000012 0.25 0.75000012 0.30000001 0.75000012
		 0.35000002 0.75000012 0.40000004 0.75000012 0.45000005 0.75000012 0.50000006 0.75000012
		 0.55000007 0.75000012 0.60000008 0.75000012 0.6500001 0.75000012 0.70000011 0.75000012
		 0.75000012 0.75000012 0.80000013 0.75000012 0.85000014 0.75000012 0.90000015 0.75000012
		 0.95000017 0.75000012 1.000000119209 0.75000012 0.050000001 0.80000013 0 0.80000013
		 0.1 0.80000013 0.15000001 0.80000013 0.2 0.80000013 0.25 0.80000013 0.30000001 0.80000013
		 0.35000002 0.80000013 0.40000004 0.80000013 0.45000005 0.80000013 0.50000006 0.80000013
		 0.55000007 0.80000013 0.60000008 0.80000013 0.6500001 0.80000013 0.70000011 0.80000013
		 0.75000012 0.80000013 0.80000013 0.80000013;
	setAttr ".uvst[0].uvsp[500:606]" 0.85000014 0.80000013 0.90000015 0.80000013
		 0.95000017 0.80000013 1.000000119209 0.80000013 0.050000001 0.85000014 0 0.85000014
		 0.1 0.85000014 0.15000001 0.85000014 0.2 0.85000014 0.25 0.85000014 0.30000001 0.85000014
		 0.35000002 0.85000014 0.40000004 0.85000014 0.45000005 0.85000014 0.50000006 0.85000014
		 0.55000007 0.85000014 0.60000008 0.85000014 0.6500001 0.85000014 0.70000011 0.85000014
		 0.75000012 0.85000014 0.80000013 0.85000014 0.85000014 0.85000014 0.90000015 0.85000014
		 0.95000017 0.85000014 1.000000119209 0.85000014 0.050000001 0.90000015 0 0.90000015
		 0.1 0.90000015 0.15000001 0.90000015 0.2 0.90000015 0.25 0.90000015 0.30000001 0.90000015
		 0.35000002 0.90000015 0.40000004 0.90000015 0.45000005 0.90000015 0.50000006 0.90000015
		 0.55000007 0.90000015 0.60000008 0.90000015 0.6500001 0.90000015 0.70000011 0.90000015
		 0.75000012 0.90000015 0.80000013 0.90000015 0.85000014 0.90000015 0.90000015 0.90000015
		 0.95000017 0.90000015 1.000000119209 0.90000015 0.050000001 0.95000017 0 0.95000017
		 0.1 0.95000017 0.15000001 0.95000017 0.2 0.95000017 0.25 0.95000017 0.30000001 0.95000017
		 0.35000002 0.95000017 0.40000004 0.95000017 0.45000005 0.95000017 0.50000006 0.95000017
		 0.55000007 0.95000017 0.60000008 0.95000017 0.6500001 0.95000017 0.70000011 0.95000017
		 0.75000012 0.95000017 0.80000013 0.95000017 0.85000014 0.95000017 0.90000015 0.95000017
		 0.95000017 0.95000017 1.000000119209 0.95000017 0.025 0 0.075000003 0 0.125 0 0.175
		 0 0.22500001 0 0.27500001 0 0.32500002 0 0.375 0 0.42500001 0 0.47499999 0 0.52500004
		 0 0.57499999 0 0.625 0 0.67500001 0 0.72500002 0 0.77500004 0 0.82499999 0 0.875
		 0 0.92500001 0 0.97500002 0 0.025 1 0.075000003 1 0.125 1 0.175 1 0.22500001 1 0.27500001
		 1 0.32500002 1 0.375 1 0.42500001 1 0.47499999 1 0.52500004 1 0.57499999 1 0.625
		 1 0.67500001 1 0.72500002 1 0.77500004 1 0.82499999 1 0.875 1 0.92500001 1 0.97500002
		 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 466 ".vt";
	setAttr ".vt[0:165]"  -31.42638397 0.15162086 89.97360229 -31.61052513 0.26360402 89.74130249
		 -31.94010735 0.35247436 89.46312714 -32.38286972 0.40953261 89.16630554 -32.89547348 0.4291935 88.87989807
		 -33.42773819 0.40953258 88.63193512 -33.92756271 0.3524743 88.44669342 -34.34602356 0.26360396 88.34230042
		 -34.64215469 0.15162082 88.32897949 -34.78697205 0.0274866 88.40803528 -34.7663002 -0.09664762 88.57172394
		 -34.58216095 -0.20863073 88.80403137 -34.25257874 -0.29750097 89.082206726 -33.80981445 -0.35455921 89.37902069
		 -33.29721451 -0.37422013 89.66543579 -32.7649498 -0.35455918 89.91339111 -32.26512146 -0.29750094 90.098632813
		 -31.84666252 -0.2086307 90.20302582 -31.55052948 -0.096647598 90.21634674 -31.40571213 0.0274866 90.13729095
		 -31.40359116 0.096647523 89.929039 -31.58773232 0.20863067 89.69673157 -31.91731453 0.297501 89.41855621
		 -32.3600769 0.35455924 89.12174225 -32.87267685 0.37422013 88.83532715 -33.40494156 0.35455921 88.58737183
		 -33.9047699 0.29750094 88.4021225 -34.32322693 0.20863059 88.29773712 -34.61936188 0.096647471 88.2844162
		 -34.76417923 -0.027486745 88.36347198 -34.74350739 -0.15162097 88.52716064 -34.55936432 -0.26360404 88.75946045
		 -34.2297821 -0.35247433 89.037635803 -33.78702164 -0.40953258 89.3344574 -33.27441788 -0.4291935 89.62086487
		 -32.74215698 -0.40953255 89.86882782 -32.24232864 -0.3524743 90.054069519 -31.82386971 -0.26360402 90.15846252
		 -31.52773666 -0.15162094 90.17178345 -31.38291931 -0.027486745 90.092727661 -33.096343994 0.0274866 89.27266693
		 -33.073547363 -0.027486745 89.22809601 -38.24187469 2.49435377 89.47029114 -38.0036354065 2.42841434 89.27202606
		 -37.675354 2.24016404 89.046867371 -37.28915024 1.94802976 88.81686401 -36.88284302 1.58060765 88.60451508
		 -36.49619675 1.17386389 88.43061066 -36.16706085 0.76761329 88.31217194 -35.92765427 0.40162241 88.26081085
		 -35.80141068 0.11171699 88.28153229 -35.80068588 -0.073725104 88.37231445 -35.92555618 -0.1365515 88.5242691
		 -36.16379166 -0.070612133 88.72253418 -36.49207687 0.11763823 88.94768524 -36.87827682 0.40977228 89.17769623
		 -37.28458405 0.77719414 89.39004517 -37.67123032 1.18393755 89.56394958 -38.00036621094 1.59018826 89.68238068
		 -38.2397728 1.95617914 89.73374939 -38.36601639 2.24608445 89.71302795 -38.36674118 2.43152666 89.62224579
		 -38.19311142 2.51918554 89.52061462 -37.95487595 2.45324612 89.32236481 -37.62659073 2.26499557 89.097198486
		 -37.24039078 1.97286129 88.86719513 -36.83408356 1.60543942 88.65484619 -36.44743729 1.19869566 88.48094177
		 -36.11829758 0.792445 88.36251068 -35.87889481 0.42645407 88.31114197 -35.75265121 0.13654858 88.33187103
		 -35.75192642 -0.048893452 88.42264557 -35.87679672 -0.11171985 88.57460022 -36.1150322 -0.04578054 88.7728653
		 -36.4433136 0.14246988 88.99801636 -36.82951355 0.43460399 89.22803497 -37.23582458 0.80202574 89.44038391
		 -37.62246704 1.20876932 89.6142807 -37.95160675 1.6150198 89.73271179 -38.19100952 1.98101068 89.78408051
		 -38.31725311 2.27091622 89.76335907 -38.31797791 2.45635819 89.67258453 -37.083713531 1.17890096 88.99727631
		 -37.034954071 1.20373249 89.047615051 -34.97560501 -0.98768836 88.25870514 -34.99782562 -0.98768836 88.21509552
		 -35.032432556 -0.98768836 88.18048859 -35.076042175 -0.98768836 88.15826416 -35.12438202 -0.98768836 88.15061188
		 -35.17272186 -0.98768836 88.15826416 -35.21633148 -0.98768836 88.18048859 -35.25093842 -0.98768836 88.21509552
		 -35.27315903 -0.98768836 88.25870514 -35.28081512 -0.98768836 88.30704498 -35.27315903 -0.98768836 88.35538483
		 -35.25093842 -0.98768836 88.39899445 -35.21633148 -0.98768836 88.43360138 -35.17272186 -0.98768836 88.45582581
		 -35.12438202 -0.98768836 88.46347809 -35.076042175 -0.98768836 88.45582581 -35.032432556 -0.98768836 88.43360138
		 -34.99782562 -0.98768836 88.39899445 -34.97560501 -0.98768836 88.35538483 -34.96794891 -0.98768836 88.30704498
		 -34.83049011 -0.95105654 88.21155548 -34.87438202 -0.95105654 88.12541199 -34.94274521 -0.95105654 88.057044983
		 -35.028888702 -0.95105654 88.013153076 -35.12438202 -0.95105654 87.99803162 -35.21987534 -0.95105654 88.013153076
		 -35.30601883 -0.95105654 88.057044983 -35.37438202 -0.95105654 88.12541199 -35.41827393 -0.95105654 88.21155548
		 -35.4333992 -0.95105654 88.30704498 -35.41827393 -0.95105654 88.40253448 -35.37438202 -0.95105654 88.48867798
		 -35.30601883 -0.95105654 88.55704498 -35.21987534 -0.95105654 88.60093689 -35.12438202 -0.95105654 88.61605835
		 -35.028888702 -0.95105654 88.60093689 -34.94274521 -0.95105654 88.55704498 -34.87438202 -0.95105654 88.48867798
		 -34.83049011 -0.95105654 88.40253448 -34.81536484 -0.95105654 88.30704498 -34.69261169 -0.89100653 88.16675568
		 -34.75709534 -0.89100653 88.04019928 -34.8575325 -0.89100653 87.9397583 -34.98409271 -0.89100653 87.87527466
		 -35.12438202 -0.89100653 87.85305786 -35.26467133 -0.89100653 87.87527466 -35.39123154 -0.89100653 87.9397583
		 -35.4916687 -0.89100653 88.04019928 -35.55615234 -0.89100653 88.16675568 -35.57837296 -0.89100653 88.30704498
		 -35.55615234 -0.89100653 88.44733429 -35.4916687 -0.89100653 88.57389069 -35.39123154 -0.89100653 88.67433167
		 -35.26467133 -0.89100653 88.73881531 -35.12438202 -0.89100653 88.7610321 -34.98409271 -0.89100653 88.73881531
		 -34.8575325 -0.89100653 88.67433167 -34.75709534 -0.89100653 88.57389069 -34.69261169 -0.89100653 88.44733429
		 -34.67039108 -0.89100653 88.30704498 -34.56536484 -0.809017 88.12541199 -34.6488533 -0.809017 87.96155548
		 -34.7788887 -0.809017 87.83152008 -34.94274521 -0.809017 87.74802399 -35.12438202 -0.809017 87.71926117
		 -35.30601883 -0.809017 87.74802399 -35.46987534 -0.809017 87.83152008 -35.59991074 -0.809017 87.96155548
		 -35.6833992 -0.809017 88.12541199 -35.71216583 -0.809017 88.30704498 -35.6833992 -0.809017 88.48867798
		 -35.59991074 -0.809017 88.65253448 -35.46987534 -0.809017 88.78256989 -35.30601883 -0.809017 88.86605835
		 -35.12438202 -0.809017 88.8948288 -34.94274521 -0.809017 88.86605835 -34.7788887 -0.809017 88.78256989
		 -34.6488533 -0.809017 88.65253448 -34.56536484 -0.809017 88.48867798 -34.53659821 -0.809017 88.30704498
		 -34.45188141 -0.70710677 88.088539124 -34.55231857 -0.70710677 87.89141846;
	setAttr ".vt[166:331]" -34.70875549 -0.70710677 87.73498535 -34.90587234 -0.70710677 87.63454437
		 -35.12438202 -0.70710677 87.59993744 -35.34289169 -0.70710677 87.63454437 -35.54000854 -0.70710677 87.73498535
		 -35.69644165 -0.70710677 87.89141846 -35.79688263 -0.70710677 88.088539124 -35.83148956 -0.70710677 88.30704498
		 -35.79688263 -0.70710677 88.52555084 -35.69644165 -0.70710677 88.72267151 -35.54000854 -0.70710677 88.87910461
		 -35.34289169 -0.70710677 88.97954559 -35.12438202 -0.70710677 89.014152527 -34.90587234 -0.70710677 88.97954559
		 -34.70875549 -0.70710677 88.87910461 -34.55232239 -0.70710677 88.72267151 -34.45188522 -0.70710677 88.52555084
		 -34.41727448 -0.70710677 88.30704498 -34.3549614 -0.58778524 88.057044983 -34.46987152 -0.58778524 87.83152008
		 -34.6488533 -0.58778524 87.65253448 -34.87438202 -0.58778524 87.53762054 -35.12438202 -0.58778524 87.49802399
		 -35.37438202 -0.58778524 87.53762054 -35.59991074 -0.58778524 87.65253448 -35.77889252 -0.58778524 87.83152008
		 -35.89380264 -0.58778524 88.057044983 -35.9333992 -0.58778524 88.30704498 -35.89380264 -0.58778524 88.55704498
		 -35.77889252 -0.58778524 88.78256989 -35.59991074 -0.58778524 88.96155548 -35.37438202 -0.58778524 89.076469421
		 -35.12438202 -0.58778524 89.11605835 -34.87438202 -0.58778524 89.076469421 -34.6488533 -0.58778524 88.96155548
		 -34.46987534 -0.58778524 88.78256989 -34.3549614 -0.58778524 88.55704498 -34.31536484 -0.58778524 88.30704498
		 -34.27698517 -0.45399052 88.031707764 -34.40354156 -0.45399052 87.7833252 -34.60066223 -0.45399052 87.58620453
		 -34.8490448 -0.45399052 87.45964813 -35.12438202 -0.45399052 87.41603851 -35.39971924 -0.45399052 87.45964813
		 -35.64810181 -0.45399052 87.58620453 -35.84522247 -0.45399052 87.7833252 -35.97177887 -0.45399052 88.031707764
		 -36.015388489 -0.45399052 88.30704498 -35.97177887 -0.45399052 88.5823822 -35.84522247 -0.45399052 88.83076477
		 -35.64810181 -0.45399052 89.027885437 -35.39971924 -0.45399052 89.15444183 -35.12438202 -0.45399052 89.19805145
		 -34.8490448 -0.45399052 89.15444183 -34.60066223 -0.45399052 89.027885437 -34.40354156 -0.45399052 88.83076477
		 -34.27698517 -0.45399052 88.5823822 -34.23337555 -0.45399052 88.30704498 -34.21987152 -0.30901697 88.013153076
		 -34.3549614 -0.30901697 87.74802399 -34.56536484 -0.30901697 87.53762054 -34.83049011 -0.30901697 87.40253448
		 -35.12438202 -0.30901697 87.35598755 -35.41827393 -0.30901697 87.40253448 -35.6833992 -0.30901697 87.53762054
		 -35.89380264 -0.30901697 87.74803162 -36.028892517 -0.30901697 88.013153076 -36.075439453 -0.30901697 88.30704498
		 -36.028892517 -0.30901697 88.60093689 -35.89380264 -0.30901697 88.86605835 -35.6833992 -0.30901697 89.076469421
		 -35.41827393 -0.30901697 89.21155548 -35.12438202 -0.30901697 89.25810242 -34.83049011 -0.30901697 89.21155548
		 -34.56536484 -0.30901697 89.076469421 -34.3549614 -0.30901697 88.86605835 -34.21987534 -0.30901697 88.60093689
		 -34.17332458 -0.30901697 88.30704498 -34.18503571 -0.15643437 88.0018310547 -34.32532501 -0.15643437 87.72649384
		 -34.54383469 -0.15643437 87.50798798 -34.81916809 -0.15643437 87.36769867 -35.12438202 -0.15643437 87.31935883
		 -35.42959595 -0.15643437 87.36769867 -35.70492935 -0.15643437 87.50798798 -35.92343903 -0.15643437 87.72649384
		 -36.063728333 -0.15643437 88.0018310547 -36.11207199 -0.15643437 88.30704498 -36.063728333 -0.15643437 88.61225891
		 -35.92343903 -0.15643437 88.88759613 -35.70492935 -0.15643437 89.10610199 -35.42959595 -0.15643437 89.2463913
		 -35.12438202 -0.15643437 89.29473114 -34.81916809 -0.15643437 89.2463913 -34.54383469 -0.15643437 89.10610199
		 -34.32532501 -0.15643437 88.88759613 -34.18503571 -0.15643437 88.61225891 -34.13669205 -0.15643437 88.30704498
		 -34.17332458 0 87.99803162 -34.31536484 0 87.71926117 -34.53659821 0 87.49802399
		 -34.81536484 0 87.35598755 -35.12438202 0 87.30704498 -35.4333992 0 87.35598755 -35.71216583 0 87.49802399
		 -35.9333992 0 87.71926117 -36.075439453 0 87.99803162 -36.12438202 0 88.30704498
		 -36.075439453 0 88.61605835 -35.9333992 0 88.8948288 -35.71216583 0 89.11605835 -35.4333992 0 89.25810242
		 -35.12438202 0 89.30704498 -34.81536484 0 89.25810242 -34.53659821 0 89.11605835
		 -34.31536484 0 88.8948288 -34.17332458 0 88.61605835 -34.12438202 0 88.30704498 -34.18503571 0.15643437 88.0018310547
		 -34.32532501 0.15643437 87.72649384 -34.54383469 0.15643437 87.50798798 -34.81916809 0.15643437 87.36769867
		 -35.12438202 0.15643437 87.31935883 -35.42959595 0.15643437 87.36769867 -35.70492935 0.15643437 87.50798798
		 -35.92343903 0.15643437 87.72649384 -36.063728333 0.15643437 88.0018310547 -36.11207199 0.15643437 88.30704498
		 -36.063728333 0.15643437 88.61225891 -35.92343903 0.15643437 88.88759613 -35.70492935 0.15643437 89.10610199
		 -35.42959595 0.15643437 89.2463913 -35.12438202 0.15643437 89.29473114 -34.81916809 0.15643437 89.2463913
		 -34.54383469 0.15643437 89.10610199 -34.32532501 0.15643437 88.88759613 -34.18503571 0.15643437 88.61225891
		 -34.13669205 0.15643437 88.30704498 -34.21987152 0.30901697 88.013153076 -34.3549614 0.30901697 87.74802399
		 -34.56536484 0.30901697 87.53762054 -34.83049011 0.30901697 87.40253448 -35.12438202 0.30901697 87.35598755
		 -35.41827393 0.30901697 87.40253448 -35.6833992 0.30901697 87.53762054 -35.89380264 0.30901697 87.74803162
		 -36.028892517 0.30901697 88.013153076 -36.075439453 0.30901697 88.30704498 -36.028892517 0.30901697 88.60093689
		 -35.89380264 0.30901697 88.86605835 -35.6833992 0.30901697 89.076469421 -35.41827393 0.30901697 89.21155548
		 -35.12438202 0.30901697 89.25810242 -34.83049011 0.30901697 89.21155548 -34.56536484 0.30901697 89.076469421
		 -34.3549614 0.30901697 88.86605835 -34.21987534 0.30901697 88.60093689 -34.17332458 0.30901697 88.30704498
		 -34.27698517 0.45399052 88.031707764 -34.40354156 0.45399052 87.7833252 -34.60066223 0.45399052 87.58620453
		 -34.8490448 0.45399052 87.45964813 -35.12438202 0.45399052 87.41603851 -35.39971924 0.45399052 87.45964813
		 -35.64810181 0.45399052 87.58620453 -35.84522247 0.45399052 87.7833252;
	setAttr ".vt[332:465]" -35.97177887 0.45399052 88.031707764 -36.015388489 0.45399052 88.30704498
		 -35.97177887 0.45399052 88.5823822 -35.84522247 0.45399052 88.83076477 -35.64810181 0.45399052 89.027885437
		 -35.39971924 0.45399052 89.15444183 -35.12438202 0.45399052 89.19805145 -34.8490448 0.45399052 89.15444183
		 -34.60066223 0.45399052 89.027885437 -34.40354156 0.45399052 88.83076477 -34.27698517 0.45399052 88.5823822
		 -34.23337555 0.45399052 88.30704498 -34.3549614 0.58778524 88.057044983 -34.46987152 0.58778524 87.83152008
		 -34.6488533 0.58778524 87.65253448 -34.87438202 0.58778524 87.53762054 -35.12438202 0.58778524 87.49802399
		 -35.37438202 0.58778524 87.53762054 -35.59991074 0.58778524 87.65253448 -35.77889252 0.58778524 87.83152008
		 -35.89380264 0.58778524 88.057044983 -35.9333992 0.58778524 88.30704498 -35.89380264 0.58778524 88.55704498
		 -35.77889252 0.58778524 88.78256989 -35.59991074 0.58778524 88.96155548 -35.37438202 0.58778524 89.076469421
		 -35.12438202 0.58778524 89.11605835 -34.87438202 0.58778524 89.076469421 -34.6488533 0.58778524 88.96155548
		 -34.46987534 0.58778524 88.78256989 -34.3549614 0.58778524 88.55704498 -34.31536484 0.58778524 88.30704498
		 -34.45188141 0.70710677 88.088539124 -34.55231857 0.70710677 87.89141846 -34.70875549 0.70710677 87.73498535
		 -34.90587234 0.70710677 87.63454437 -35.12438202 0.70710677 87.59993744 -35.34289169 0.70710677 87.63454437
		 -35.54000854 0.70710677 87.73498535 -35.69644165 0.70710677 87.89141846 -35.79688263 0.70710677 88.088539124
		 -35.83148956 0.70710677 88.30704498 -35.79688263 0.70710677 88.52555084 -35.69644165 0.70710677 88.72267151
		 -35.54000854 0.70710677 88.87910461 -35.34289169 0.70710677 88.97954559 -35.12438202 0.70710677 89.014152527
		 -34.90587234 0.70710677 88.97954559 -34.70875549 0.70710677 88.87910461 -34.55232239 0.70710677 88.72267151
		 -34.45188522 0.70710677 88.52555084 -34.41727448 0.70710677 88.30704498 -34.56536484 0.809017 88.12541199
		 -34.6488533 0.809017 87.96155548 -34.7788887 0.809017 87.83152008 -34.94274521 0.809017 87.74802399
		 -35.12438202 0.809017 87.71926117 -35.30601883 0.809017 87.74802399 -35.46987534 0.809017 87.83152008
		 -35.59991074 0.809017 87.96155548 -35.6833992 0.809017 88.12541199 -35.71216583 0.809017 88.30704498
		 -35.6833992 0.809017 88.48867798 -35.59991074 0.809017 88.65253448 -35.46987534 0.809017 88.78256989
		 -35.30601883 0.809017 88.86605835 -35.12438202 0.809017 88.8948288 -34.94274521 0.809017 88.86605835
		 -34.7788887 0.809017 88.78256989 -34.6488533 0.809017 88.65253448 -34.56536484 0.809017 88.48867798
		 -34.53659821 0.809017 88.30704498 -34.69261169 0.89100653 88.16675568 -34.75709534 0.89100653 88.04019928
		 -34.8575325 0.89100653 87.9397583 -34.98409271 0.89100653 87.87527466 -35.12438202 0.89100653 87.85305786
		 -35.26467133 0.89100653 87.87527466 -35.39123154 0.89100653 87.9397583 -35.4916687 0.89100653 88.04019928
		 -35.55615234 0.89100653 88.16675568 -35.57837296 0.89100653 88.30704498 -35.55615234 0.89100653 88.44733429
		 -35.4916687 0.89100653 88.57389069 -35.39123154 0.89100653 88.67433167 -35.26467133 0.89100653 88.73881531
		 -35.12438202 0.89100653 88.7610321 -34.98409271 0.89100653 88.73881531 -34.8575325 0.89100653 88.67433167
		 -34.75709534 0.89100653 88.57389069 -34.69261169 0.89100653 88.44733429 -34.67039108 0.89100653 88.30704498
		 -34.83049011 0.95105654 88.21155548 -34.87438202 0.95105654 88.12541199 -34.94274521 0.95105654 88.057044983
		 -35.028888702 0.95105654 88.013153076 -35.12438202 0.95105654 87.99803162 -35.21987534 0.95105654 88.013153076
		 -35.30601883 0.95105654 88.057044983 -35.37438202 0.95105654 88.12541199 -35.41827393 0.95105654 88.21155548
		 -35.4333992 0.95105654 88.30704498 -35.41827393 0.95105654 88.40253448 -35.37438202 0.95105654 88.48867798
		 -35.30601883 0.95105654 88.55704498 -35.21987534 0.95105654 88.60093689 -35.12438202 0.95105654 88.61605835
		 -35.028888702 0.95105654 88.60093689 -34.94274521 0.95105654 88.55704498 -34.87438202 0.95105654 88.48867798
		 -34.83049011 0.95105654 88.40253448 -34.81536484 0.95105654 88.30704498 -34.97560501 0.98768836 88.25870514
		 -34.99782562 0.98768836 88.21509552 -35.032432556 0.98768836 88.18048859 -35.076042175 0.98768836 88.15826416
		 -35.12438202 0.98768836 88.15061188 -35.17272186 0.98768836 88.15826416 -35.21633148 0.98768836 88.18048859
		 -35.25093842 0.98768836 88.21509552 -35.27315903 0.98768836 88.25870514 -35.28081512 0.98768836 88.30704498
		 -35.27315903 0.98768836 88.35538483 -35.25093842 0.98768836 88.39899445 -35.21633148 0.98768836 88.43360138
		 -35.17272186 0.98768836 88.45582581 -35.12438202 0.98768836 88.46347809 -35.076042175 0.98768836 88.45582581
		 -35.032432556 0.98768836 88.43360138 -34.99782562 0.98768836 88.39899445 -34.97560501 0.98768836 88.35538483
		 -34.96794891 0.98768836 88.30704498 -35.12438202 -1 88.30704498 -35.12438202 1 88.30704498;
	setAttr -s 980 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 0 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0 0 20 1 1 21 1
		 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1
		 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1 40 3 1 40 4 1
		 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1 40 14 1 40 15 1
		 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1 25 41 1 26 41 1
		 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1 36 41 1 37 41 1
		 38 41 1 39 41 1 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 62 0 42 62 1 43 63 1
		 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1
		 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 82 42 1 82 43 1 82 44 1 82 45 1
		 82 46 1 82 47 1;
	setAttr ".ed[166:331]" 82 48 1 82 49 1 82 50 1 82 51 1 82 52 1 82 53 1 82 54 1
		 82 55 1 82 56 1 82 57 1 82 58 1 82 59 1 82 60 1 82 61 1 62 83 1 63 83 1 64 83 1 65 83 1
		 66 83 1 67 83 1 68 83 1 69 83 1 70 83 1 71 83 1 72 83 1 73 83 1 74 83 1 75 83 1 76 83 1
		 77 83 1 78 83 1 79 83 1 80 83 1 81 83 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1
		 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 84 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1
		 109 110 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 104 1 124 125 1 125 126 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 134 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 124 1 144 145 1
		 145 146 1 146 147 1 147 148 1 148 149 1 149 150 1 150 151 1 151 152 1 152 153 1 153 154 1
		 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 160 1 160 161 1 161 162 1 162 163 1
		 163 144 1 164 165 1 165 166 1 166 167 1 167 168 1 168 169 1 169 170 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 180 1 180 181 1
		 181 182 1 182 183 1 183 164 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 190 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 200 1 200 201 1 201 202 1 202 203 1 203 184 1 204 205 1 205 206 1 206 207 1 207 208 1
		 208 209 1 209 210 1 210 211 1 211 212 1 212 213 1 213 214 1 214 215 1 215 216 1;
	setAttr ".ed[332:497]" 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1
		 222 223 1 223 204 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 232 1 232 233 1 233 234 1 234 235 1 235 236 1 236 237 1 237 238 1 238 239 1 239 240 1
		 240 241 1 241 242 1 242 243 1 243 224 1 244 245 1 245 246 1 246 247 1 247 248 1 248 249 1
		 249 250 1 250 251 1 251 252 1 252 253 1 253 254 1 254 255 1 255 256 1 256 257 1 257 258 1
		 258 259 1 259 260 1 260 261 1 261 262 1 262 263 1 263 244 1 264 265 1 265 266 1 266 267 1
		 267 268 1 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1 274 275 1 275 276 1
		 276 277 1 277 278 1 278 279 1 279 280 1 280 281 1 281 282 1 282 283 1 283 264 1 284 285 1
		 285 286 1 286 287 1 287 288 1 288 289 1 289 290 1 290 291 1 291 292 1 292 293 1 293 294 1
		 294 295 1 295 296 1 296 297 1 297 298 1 298 299 1 299 300 1 300 301 1 301 302 1 302 303 1
		 303 284 1 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1
		 312 313 1 313 314 1 314 315 1 315 316 1 316 317 1 317 318 1 318 319 1 319 320 1 320 321 1
		 321 322 1 322 323 1 323 304 1 324 325 1 325 326 1 326 327 1 327 328 1 328 329 1 329 330 1
		 330 331 1 331 332 1 332 333 1 333 334 1 334 335 1 335 336 1 336 337 1 337 338 1 338 339 1
		 339 340 1 340 341 1 341 342 1 342 343 1 343 324 1 344 345 1 345 346 1 346 347 1 347 348 1
		 348 349 1 349 350 1 350 351 1 351 352 1 352 353 1 353 354 1 354 355 1 355 356 1 356 357 1
		 357 358 1 358 359 1 359 360 1 360 361 1 361 362 1 362 363 1 363 344 1 364 365 1 365 366 1
		 366 367 1 367 368 1 368 369 1 369 370 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1
		 375 376 1 376 377 1 377 378 1 378 379 1 379 380 1 380 381 1 381 382 1;
	setAttr ".ed[498:663]" 382 383 1 383 364 1 384 385 1 385 386 1 386 387 1 387 388 1
		 388 389 1 389 390 1 390 391 1 391 392 1 392 393 1 393 394 1 394 395 1 395 396 1 396 397 1
		 397 398 1 398 399 1 399 400 1 400 401 1 401 402 1 402 403 1 403 384 1 404 405 1 405 406 1
		 406 407 1 407 408 1 408 409 1 409 410 1 410 411 1 411 412 1 412 413 1 413 414 1 414 415 1
		 415 416 1 416 417 1 417 418 1 418 419 1 419 420 1 420 421 1 421 422 1 422 423 1 423 404 1
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 432 1 432 433 1
		 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 440 1 440 441 1 441 442 1
		 442 443 1 443 424 1 444 445 1 445 446 1 446 447 1 447 448 1 448 449 1 449 450 1 450 451 1
		 451 452 1 452 453 1 453 454 1 454 455 1 455 456 1 456 457 1 457 458 1 458 459 1 459 460 1
		 460 461 1 461 462 1 462 463 1 463 444 1 84 104 1 85 105 1 86 106 1 87 107 1 88 108 1
		 89 109 1 90 110 1 91 111 1 92 112 1 93 113 1 94 114 1 95 115 1 96 116 1 97 117 1
		 98 118 1 99 119 1 100 120 1 101 121 1 102 122 1 103 123 1 104 124 1 105 125 1 106 126 1
		 107 127 1 108 128 1 109 129 1 110 130 1 111 131 1 112 132 1 113 133 1 114 134 1 115 135 1
		 116 136 1 117 137 1 118 138 1 119 139 1 120 140 1 121 141 1 122 142 1 123 143 1 124 144 1
		 125 145 1 126 146 1 127 147 1 128 148 1 129 149 1 130 150 1 131 151 1 132 152 1 133 153 1
		 134 154 1 135 155 1 136 156 1 137 157 1 138 158 1 139 159 1 140 160 1 141 161 1 142 162 1
		 143 163 1 144 164 1 145 165 1 146 166 1 147 167 1 148 168 1 149 169 1 150 170 1 151 171 1
		 152 172 1 153 173 1 154 174 1 155 175 1 156 176 1 157 177 1 158 178 1 159 179 1 160 180 1
		 161 181 1 162 182 1 163 183 1 164 184 1 165 185 1 166 186 1 167 187 1;
	setAttr ".ed[664:829]" 168 188 1 169 189 1 170 190 1 171 191 1 172 192 1 173 193 1
		 174 194 1 175 195 1 176 196 1 177 197 1 178 198 1 179 199 1 180 200 1 181 201 1 182 202 1
		 183 203 1 184 204 1 185 205 1 186 206 1 187 207 1 188 208 1 189 209 1 190 210 1 191 211 1
		 192 212 1 193 213 1 194 214 1 195 215 1 196 216 1 197 217 1 198 218 1 199 219 1 200 220 1
		 201 221 1 202 222 1 203 223 1 204 224 1 205 225 1 206 226 1 207 227 1 208 228 1 209 229 1
		 210 230 1 211 231 1 212 232 1 213 233 1 214 234 1 215 235 1 216 236 1 217 237 1 218 238 1
		 219 239 1 220 240 1 221 241 1 222 242 1 223 243 1 224 244 1 225 245 1 226 246 1 227 247 1
		 228 248 1 229 249 1 230 250 1 231 251 1 232 252 1 233 253 1 234 254 1 235 255 1 236 256 1
		 237 257 1 238 258 1 239 259 1 240 260 1 241 261 1 242 262 1 243 263 1 244 264 1 245 265 1
		 246 266 1 247 267 1 248 268 1 249 269 1 250 270 1 251 271 1 252 272 1 253 273 1 254 274 1
		 255 275 1 256 276 1 257 277 1 258 278 1 259 279 1 260 280 1 261 281 1 262 282 1 263 283 1
		 264 284 1 265 285 1 266 286 1 267 287 1 268 288 1 269 289 1 270 290 1 271 291 1 272 292 1
		 273 293 1 274 294 1 275 295 1 276 296 1 277 297 1 278 298 1 279 299 1 280 300 1 281 301 1
		 282 302 1 283 303 1 284 304 1 285 305 1 286 306 1 287 307 1 288 308 1 289 309 1 290 310 1
		 291 311 1 292 312 1 293 313 1 294 314 1 295 315 1 296 316 1 297 317 1 298 318 1 299 319 1
		 300 320 1 301 321 1 302 322 1 303 323 1 304 324 1 305 325 1 306 326 1 307 327 1 308 328 1
		 309 329 1 310 330 1 311 331 1 312 332 1 313 333 1 314 334 1 315 335 1 316 336 1 317 337 1
		 318 338 1 319 339 1 320 340 1 321 341 1 322 342 1 323 343 1 324 344 1 325 345 1 326 346 1
		 327 347 1 328 348 1 329 349 1 330 350 1 331 351 1 332 352 1 333 353 1;
	setAttr ".ed[830:979]" 334 354 1 335 355 1 336 356 1 337 357 1 338 358 1 339 359 1
		 340 360 1 341 361 1 342 362 1 343 363 1 344 364 1 345 365 1 346 366 1 347 367 1 348 368 1
		 349 369 1 350 370 1 351 371 1 352 372 1 353 373 1 354 374 1 355 375 1 356 376 1 357 377 1
		 358 378 1 359 379 1 360 380 1 361 381 1 362 382 1 363 383 1 364 384 1 365 385 1 366 386 1
		 367 387 1 368 388 1 369 389 1 370 390 1 371 391 1 372 392 1 373 393 1 374 394 1 375 395 1
		 376 396 1 377 397 1 378 398 1 379 399 1 380 400 1 381 401 1 382 402 1 383 403 1 384 404 1
		 385 405 1 386 406 1 387 407 1 388 408 1 389 409 1 390 410 1 391 411 1 392 412 1 393 413 1
		 394 414 1 395 415 1 396 416 1 397 417 1 398 418 1 399 419 1 400 420 1 401 421 1 402 422 1
		 403 423 1 404 424 1 405 425 1 406 426 1 407 427 1 408 428 1 409 429 1 410 430 1 411 431 1
		 412 432 1 413 433 1 414 434 1 415 435 1 416 436 1 417 437 1 418 438 1 419 439 1 420 440 1
		 421 441 1 422 442 1 423 443 1 424 444 1 425 445 1 426 446 1 427 447 1 428 448 1 429 449 1
		 430 450 1 431 451 1 432 452 1 433 453 1 434 454 1 435 455 1 436 456 1 437 457 1 438 458 1
		 439 459 1 440 460 1 441 461 1 442 462 1 443 463 1 464 84 1 464 85 1 464 86 1 464 87 1
		 464 88 1 464 89 1 464 90 1 464 91 1 464 92 1 464 93 1 464 94 1 464 95 1 464 96 1
		 464 97 1 464 98 1 464 99 1 464 100 1 464 101 1 464 102 1 464 103 1 444 465 1 445 465 1
		 446 465 1 447 465 1 448 465 1 449 465 1 450 465 1 451 465 1 452 465 1 453 465 1 454 465 1
		 455 465 1 456 465 1 457 465 1 458 465 1 459 465 1 460 465 1 461 465 1 462 465 1 463 465 1;
	setAttr -s 520 -ch 1960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 40 20 -42 -1
		mu 0 4 20 41 42 21
		f 4 41 21 -43 -2
		mu 0 4 21 42 43 22
		f 4 42 22 -44 -3
		mu 0 4 22 43 44 23
		f 4 43 23 -45 -4
		mu 0 4 23 44 45 24
		f 4 44 24 -46 -5
		mu 0 4 24 45 46 25
		f 4 45 25 -47 -6
		mu 0 4 25 46 47 26
		f 4 46 26 -48 -7
		mu 0 4 26 47 48 27
		f 4 47 27 -49 -8
		mu 0 4 27 48 49 28
		f 4 48 28 -50 -9
		mu 0 4 28 49 50 29
		f 4 49 29 -51 -10
		mu 0 4 29 50 51 30
		f 4 50 30 -52 -11
		mu 0 4 30 51 52 31
		f 4 51 31 -53 -12
		mu 0 4 31 52 53 32
		f 4 52 32 -54 -13
		mu 0 4 32 53 54 33
		f 4 53 33 -55 -14
		mu 0 4 33 54 55 34
		f 4 54 34 -56 -15
		mu 0 4 34 55 56 35
		f 4 55 35 -57 -16
		mu 0 4 35 56 57 36
		f 4 56 36 -58 -17
		mu 0 4 36 57 58 37
		f 4 57 37 -59 -18
		mu 0 4 37 58 59 38
		f 4 58 38 -60 -19
		mu 0 4 38 59 60 39
		f 4 59 39 -41 -20
		mu 0 4 39 60 61 40
		f 3 -62 60 0
		mu 0 3 1 82 0
		f 3 -63 61 1
		mu 0 3 2 82 1
		f 3 -64 62 2
		mu 0 3 3 82 2
		f 3 -65 63 3
		mu 0 3 4 82 3
		f 3 -66 64 4
		mu 0 3 5 82 4
		f 3 -67 65 5
		mu 0 3 6 82 5
		f 3 -68 66 6
		mu 0 3 7 82 6
		f 3 -69 67 7
		mu 0 3 8 82 7
		f 3 -70 68 8
		mu 0 3 9 82 8
		f 3 -71 69 9
		mu 0 3 10 82 9
		f 3 -72 70 10
		mu 0 3 11 82 10
		f 3 -73 71 11
		mu 0 3 12 82 11
		f 3 -74 72 12
		mu 0 3 13 82 12
		f 3 -75 73 13
		mu 0 3 14 82 13
		f 3 -76 74 14
		mu 0 3 15 82 14
		f 3 -77 75 15
		mu 0 3 16 82 15
		f 3 -78 76 16
		mu 0 3 17 82 16
		f 3 -79 77 17
		mu 0 3 18 82 17
		f 3 -80 78 18
		mu 0 3 19 82 18
		f 3 -61 79 19
		mu 0 3 0 82 19
		f 3 80 -82 -21
		mu 0 3 80 83 79
		f 3 81 -83 -22
		mu 0 3 79 83 78
		f 3 82 -84 -23
		mu 0 3 78 83 77
		f 3 83 -85 -24
		mu 0 3 77 83 76
		f 3 84 -86 -25
		mu 0 3 76 83 75
		f 3 85 -87 -26
		mu 0 3 75 83 74
		f 3 86 -88 -27
		mu 0 3 74 83 73
		f 3 87 -89 -28
		mu 0 3 73 83 72
		f 3 88 -90 -29
		mu 0 3 72 83 71
		f 3 89 -91 -30
		mu 0 3 71 83 70
		f 3 90 -92 -31
		mu 0 3 70 83 69
		f 3 91 -93 -32
		mu 0 3 69 83 68
		f 3 92 -94 -33
		mu 0 3 68 83 67
		f 3 93 -95 -34
		mu 0 3 67 83 66
		f 3 94 -96 -35
		mu 0 3 66 83 65
		f 3 95 -97 -36
		mu 0 3 65 83 64
		f 3 96 -98 -37
		mu 0 3 64 83 63
		f 3 97 -99 -38
		mu 0 3 63 83 62
		f 3 98 -100 -39
		mu 0 3 62 83 81
		f 3 99 -81 -40
		mu 0 3 81 83 80
		f 4 140 120 -142 -101
		mu 0 4 84 85 86 87
		f 4 141 121 -143 -102
		mu 0 4 87 86 88 89
		f 4 142 122 -144 -103
		mu 0 4 89 88 90 91
		f 4 143 123 -145 -104
		mu 0 4 91 90 92 93
		f 4 144 124 -146 -105
		mu 0 4 93 92 94 95
		f 4 145 125 -147 -106
		mu 0 4 95 94 96 97
		f 4 146 126 -148 -107
		mu 0 4 97 96 98 99
		f 4 147 127 -149 -108
		mu 0 4 99 98 100 101
		f 4 148 128 -150 -109
		mu 0 4 101 100 102 103
		f 4 149 129 -151 -110
		mu 0 4 103 102 104 105
		f 4 150 130 -152 -111
		mu 0 4 105 104 106 107
		f 4 151 131 -153 -112
		mu 0 4 107 106 108 109
		f 4 152 132 -154 -113
		mu 0 4 109 108 110 111
		f 4 153 133 -155 -114
		mu 0 4 111 110 112 113
		f 4 154 134 -156 -115
		mu 0 4 113 112 114 115
		f 4 155 135 -157 -116
		mu 0 4 115 114 116 117
		f 4 156 136 -158 -117
		mu 0 4 117 116 118 119
		f 4 157 137 -159 -118
		mu 0 4 119 118 120 121
		f 4 158 138 -160 -119
		mu 0 4 121 120 122 123
		f 4 159 139 -141 -120
		mu 0 4 123 122 124 125
		f 3 -162 160 100
		mu 0 3 126 127 128
		f 3 -163 161 101
		mu 0 3 129 127 126
		f 3 -164 162 102
		mu 0 3 130 127 129
		f 3 -165 163 103
		mu 0 3 131 127 130
		f 3 -166 164 104
		mu 0 3 132 127 131
		f 3 -167 165 105
		mu 0 3 133 127 132
		f 3 -168 166 106
		mu 0 3 134 127 133
		f 3 -169 167 107
		mu 0 3 135 127 134
		f 3 -170 168 108
		mu 0 3 136 127 135
		f 3 -171 169 109
		mu 0 3 137 127 136
		f 3 -172 170 110
		mu 0 3 138 127 137
		f 3 -173 171 111
		mu 0 3 139 127 138
		f 3 -174 172 112
		mu 0 3 140 127 139
		f 3 -175 173 113
		mu 0 3 141 127 140
		f 3 -176 174 114
		mu 0 3 142 127 141
		f 3 -177 175 115
		mu 0 3 143 127 142
		f 3 -178 176 116
		mu 0 3 144 127 143
		f 3 -179 177 117
		mu 0 3 145 127 144
		f 3 -180 178 118
		mu 0 3 146 127 145
		f 3 -161 179 119
		mu 0 3 128 127 146
		f 3 180 -182 -121
		mu 0 3 147 148 149
		f 3 181 -183 -122
		mu 0 3 149 148 150
		f 3 182 -184 -123
		mu 0 3 150 148 151
		f 3 183 -185 -124
		mu 0 3 151 148 152
		f 3 184 -186 -125
		mu 0 3 152 148 153
		f 3 185 -187 -126
		mu 0 3 153 148 154
		f 3 186 -188 -127
		mu 0 3 154 148 155
		f 3 187 -189 -128
		mu 0 3 155 148 156
		f 3 188 -190 -129
		mu 0 3 156 148 157
		f 3 189 -191 -130
		mu 0 3 157 148 158
		f 3 190 -192 -131
		mu 0 3 158 148 159
		f 3 191 -193 -132
		mu 0 3 159 148 160
		f 3 192 -194 -133
		mu 0 3 160 148 161
		f 3 193 -195 -134
		mu 0 3 161 148 162
		f 3 194 -196 -135
		mu 0 3 162 148 163
		f 3 195 -197 -136
		mu 0 3 163 148 164
		f 3 196 -198 -137
		mu 0 3 164 148 165
		f 3 197 -199 -138
		mu 0 3 165 148 166
		f 3 198 -200 -139
		mu 0 3 166 148 167
		f 3 199 -181 -140
		mu 0 3 167 148 147
		f 4 200 581 -221 -581
		mu 0 4 168 169 170 171
		f 4 201 582 -222 -582
		mu 0 4 169 172 173 170
		f 4 202 583 -223 -583
		mu 0 4 172 174 175 173
		f 4 203 584 -224 -584
		mu 0 4 174 176 177 175
		f 4 204 585 -225 -585
		mu 0 4 176 178 179 177
		f 4 205 586 -226 -586
		mu 0 4 178 180 181 179
		f 4 206 587 -227 -587
		mu 0 4 180 182 183 181
		f 4 207 588 -228 -588
		mu 0 4 182 184 185 183
		f 4 208 589 -229 -589
		mu 0 4 184 186 187 185
		f 4 209 590 -230 -590
		mu 0 4 186 188 189 187
		f 4 210 591 -231 -591
		mu 0 4 188 190 191 189
		f 4 211 592 -232 -592
		mu 0 4 190 192 193 191
		f 4 212 593 -233 -593
		mu 0 4 192 194 195 193
		f 4 213 594 -234 -594
		mu 0 4 194 196 197 195
		f 4 214 595 -235 -595
		mu 0 4 196 198 199 197
		f 4 215 596 -236 -596
		mu 0 4 198 200 201 199
		f 4 216 597 -237 -597
		mu 0 4 200 202 203 201
		f 4 217 598 -238 -598
		mu 0 4 202 204 205 203
		f 4 218 599 -239 -599
		mu 0 4 204 206 207 205
		f 4 219 580 -240 -600
		mu 0 4 206 208 209 207
		f 4 220 601 -241 -601
		mu 0 4 171 170 210 211
		f 4 221 602 -242 -602
		mu 0 4 170 173 212 210
		f 4 222 603 -243 -603
		mu 0 4 173 175 213 212
		f 4 223 604 -244 -604
		mu 0 4 175 177 214 213
		f 4 224 605 -245 -605
		mu 0 4 177 179 215 214
		f 4 225 606 -246 -606
		mu 0 4 179 181 216 215
		f 4 226 607 -247 -607
		mu 0 4 181 183 217 216
		f 4 227 608 -248 -608
		mu 0 4 183 185 218 217
		f 4 228 609 -249 -609
		mu 0 4 185 187 219 218
		f 4 229 610 -250 -610
		mu 0 4 187 189 220 219
		f 4 230 611 -251 -611
		mu 0 4 189 191 221 220
		f 4 231 612 -252 -612
		mu 0 4 191 193 222 221
		f 4 232 613 -253 -613
		mu 0 4 193 195 223 222
		f 4 233 614 -254 -614
		mu 0 4 195 197 224 223
		f 4 234 615 -255 -615
		mu 0 4 197 199 225 224
		f 4 235 616 -256 -616
		mu 0 4 199 201 226 225
		f 4 236 617 -257 -617
		mu 0 4 201 203 227 226
		f 4 237 618 -258 -618
		mu 0 4 203 205 228 227
		f 4 238 619 -259 -619
		mu 0 4 205 207 229 228
		f 4 239 600 -260 -620
		mu 0 4 207 209 230 229
		f 4 240 621 -261 -621
		mu 0 4 211 210 231 232
		f 4 241 622 -262 -622
		mu 0 4 210 212 233 231
		f 4 242 623 -263 -623
		mu 0 4 212 213 234 233
		f 4 243 624 -264 -624
		mu 0 4 213 214 235 234
		f 4 244 625 -265 -625
		mu 0 4 214 215 236 235
		f 4 245 626 -266 -626
		mu 0 4 215 216 237 236
		f 4 246 627 -267 -627
		mu 0 4 216 217 238 237
		f 4 247 628 -268 -628
		mu 0 4 217 218 239 238
		f 4 248 629 -269 -629
		mu 0 4 218 219 240 239
		f 4 249 630 -270 -630
		mu 0 4 219 220 241 240
		f 4 250 631 -271 -631
		mu 0 4 220 221 242 241
		f 4 251 632 -272 -632
		mu 0 4 221 222 243 242
		f 4 252 633 -273 -633
		mu 0 4 222 223 244 243
		f 4 253 634 -274 -634
		mu 0 4 223 224 245 244
		f 4 254 635 -275 -635
		mu 0 4 224 225 246 245
		f 4 255 636 -276 -636
		mu 0 4 225 226 247 246
		f 4 256 637 -277 -637
		mu 0 4 226 227 248 247
		f 4 257 638 -278 -638
		mu 0 4 227 228 249 248
		f 4 258 639 -279 -639
		mu 0 4 228 229 250 249
		f 4 259 620 -280 -640
		mu 0 4 229 230 251 250
		f 4 260 641 -281 -641
		mu 0 4 232 231 252 253
		f 4 261 642 -282 -642
		mu 0 4 231 233 254 252
		f 4 262 643 -283 -643
		mu 0 4 233 234 255 254
		f 4 263 644 -284 -644
		mu 0 4 234 235 256 255
		f 4 264 645 -285 -645
		mu 0 4 235 236 257 256
		f 4 265 646 -286 -646
		mu 0 4 236 237 258 257
		f 4 266 647 -287 -647
		mu 0 4 237 238 259 258
		f 4 267 648 -288 -648
		mu 0 4 238 239 260 259
		f 4 268 649 -289 -649
		mu 0 4 239 240 261 260
		f 4 269 650 -290 -650
		mu 0 4 240 241 262 261
		f 4 270 651 -291 -651
		mu 0 4 241 242 263 262
		f 4 271 652 -292 -652
		mu 0 4 242 243 264 263
		f 4 272 653 -293 -653
		mu 0 4 243 244 265 264
		f 4 273 654 -294 -654
		mu 0 4 244 245 266 265
		f 4 274 655 -295 -655
		mu 0 4 245 246 267 266
		f 4 275 656 -296 -656
		mu 0 4 246 247 268 267
		f 4 276 657 -297 -657
		mu 0 4 247 248 269 268
		f 4 277 658 -298 -658
		mu 0 4 248 249 270 269
		f 4 278 659 -299 -659
		mu 0 4 249 250 271 270
		f 4 279 640 -300 -660
		mu 0 4 250 251 272 271
		f 4 280 661 -301 -661
		mu 0 4 253 252 273 274
		f 4 281 662 -302 -662
		mu 0 4 252 254 275 273
		f 4 282 663 -303 -663
		mu 0 4 254 255 276 275
		f 4 283 664 -304 -664
		mu 0 4 255 256 277 276
		f 4 284 665 -305 -665
		mu 0 4 256 257 278 277
		f 4 285 666 -306 -666
		mu 0 4 257 258 279 278
		f 4 286 667 -307 -667
		mu 0 4 258 259 280 279
		f 4 287 668 -308 -668
		mu 0 4 259 260 281 280
		f 4 288 669 -309 -669
		mu 0 4 260 261 282 281
		f 4 289 670 -310 -670
		mu 0 4 261 262 283 282
		f 4 290 671 -311 -671
		mu 0 4 262 263 284 283
		f 4 291 672 -312 -672
		mu 0 4 263 264 285 284
		f 4 292 673 -313 -673
		mu 0 4 264 265 286 285
		f 4 293 674 -314 -674
		mu 0 4 265 266 287 286
		f 4 294 675 -315 -675
		mu 0 4 266 267 288 287
		f 4 295 676 -316 -676
		mu 0 4 267 268 289 288
		f 4 296 677 -317 -677
		mu 0 4 268 269 290 289
		f 4 297 678 -318 -678
		mu 0 4 269 270 291 290
		f 4 298 679 -319 -679
		mu 0 4 270 271 292 291
		f 4 299 660 -320 -680
		mu 0 4 271 272 293 292
		f 4 300 681 -321 -681
		mu 0 4 274 273 294 295
		f 4 301 682 -322 -682
		mu 0 4 273 275 296 294
		f 4 302 683 -323 -683
		mu 0 4 275 276 297 296
		f 4 303 684 -324 -684
		mu 0 4 276 277 298 297
		f 4 304 685 -325 -685
		mu 0 4 277 278 299 298
		f 4 305 686 -326 -686
		mu 0 4 278 279 300 299
		f 4 306 687 -327 -687
		mu 0 4 279 280 301 300
		f 4 307 688 -328 -688
		mu 0 4 280 281 302 301
		f 4 308 689 -329 -689
		mu 0 4 281 282 303 302
		f 4 309 690 -330 -690
		mu 0 4 282 283 304 303
		f 4 310 691 -331 -691
		mu 0 4 283 284 305 304
		f 4 311 692 -332 -692
		mu 0 4 284 285 306 305
		f 4 312 693 -333 -693
		mu 0 4 285 286 307 306
		f 4 313 694 -334 -694
		mu 0 4 286 287 308 307
		f 4 314 695 -335 -695
		mu 0 4 287 288 309 308
		f 4 315 696 -336 -696
		mu 0 4 288 289 310 309
		f 4 316 697 -337 -697
		mu 0 4 289 290 311 310
		f 4 317 698 -338 -698
		mu 0 4 290 291 312 311
		f 4 318 699 -339 -699
		mu 0 4 291 292 313 312
		f 4 319 680 -340 -700
		mu 0 4 292 293 314 313
		f 4 320 701 -341 -701
		mu 0 4 295 294 315 316
		f 4 321 702 -342 -702
		mu 0 4 294 296 317 315
		f 4 322 703 -343 -703
		mu 0 4 296 297 318 317
		f 4 323 704 -344 -704
		mu 0 4 297 298 319 318
		f 4 324 705 -345 -705
		mu 0 4 298 299 320 319
		f 4 325 706 -346 -706
		mu 0 4 299 300 321 320
		f 4 326 707 -347 -707
		mu 0 4 300 301 322 321
		f 4 327 708 -348 -708
		mu 0 4 301 302 323 322
		f 4 328 709 -349 -709
		mu 0 4 302 303 324 323
		f 4 329 710 -350 -710
		mu 0 4 303 304 325 324
		f 4 330 711 -351 -711
		mu 0 4 304 305 326 325
		f 4 331 712 -352 -712
		mu 0 4 305 306 327 326
		f 4 332 713 -353 -713
		mu 0 4 306 307 328 327
		f 4 333 714 -354 -714
		mu 0 4 307 308 329 328
		f 4 334 715 -355 -715
		mu 0 4 308 309 330 329
		f 4 335 716 -356 -716
		mu 0 4 309 310 331 330
		f 4 336 717 -357 -717
		mu 0 4 310 311 332 331
		f 4 337 718 -358 -718
		mu 0 4 311 312 333 332
		f 4 338 719 -359 -719
		mu 0 4 312 313 334 333
		f 4 339 700 -360 -720
		mu 0 4 313 314 335 334
		f 4 340 721 -361 -721
		mu 0 4 316 315 336 337
		f 4 341 722 -362 -722
		mu 0 4 315 317 338 336
		f 4 342 723 -363 -723
		mu 0 4 317 318 339 338
		f 4 343 724 -364 -724
		mu 0 4 318 319 340 339
		f 4 344 725 -365 -725
		mu 0 4 319 320 341 340
		f 4 345 726 -366 -726
		mu 0 4 320 321 342 341
		f 4 346 727 -367 -727
		mu 0 4 321 322 343 342
		f 4 347 728 -368 -728
		mu 0 4 322 323 344 343
		f 4 348 729 -369 -729
		mu 0 4 323 324 345 344
		f 4 349 730 -370 -730
		mu 0 4 324 325 346 345
		f 4 350 731 -371 -731
		mu 0 4 325 326 347 346
		f 4 351 732 -372 -732
		mu 0 4 326 327 348 347
		f 4 352 733 -373 -733
		mu 0 4 327 328 349 348
		f 4 353 734 -374 -734
		mu 0 4 328 329 350 349
		f 4 354 735 -375 -735
		mu 0 4 329 330 351 350
		f 4 355 736 -376 -736
		mu 0 4 330 331 352 351
		f 4 356 737 -377 -737
		mu 0 4 331 332 353 352
		f 4 357 738 -378 -738
		mu 0 4 332 333 354 353
		f 4 358 739 -379 -739
		mu 0 4 333 334 355 354
		f 4 359 720 -380 -740
		mu 0 4 334 335 356 355
		f 4 360 741 -381 -741
		mu 0 4 337 336 357 358
		f 4 361 742 -382 -742
		mu 0 4 336 338 359 357
		f 4 362 743 -383 -743
		mu 0 4 338 339 360 359
		f 4 363 744 -384 -744
		mu 0 4 339 340 361 360
		f 4 364 745 -385 -745
		mu 0 4 340 341 362 361
		f 4 365 746 -386 -746
		mu 0 4 341 342 363 362
		f 4 366 747 -387 -747
		mu 0 4 342 343 364 363
		f 4 367 748 -388 -748
		mu 0 4 343 344 365 364
		f 4 368 749 -389 -749
		mu 0 4 344 345 366 365
		f 4 369 750 -390 -750
		mu 0 4 345 346 367 366
		f 4 370 751 -391 -751
		mu 0 4 346 347 368 367
		f 4 371 752 -392 -752
		mu 0 4 347 348 369 368
		f 4 372 753 -393 -753
		mu 0 4 348 349 370 369
		f 4 373 754 -394 -754
		mu 0 4 349 350 371 370
		f 4 374 755 -395 -755
		mu 0 4 350 351 372 371
		f 4 375 756 -396 -756
		mu 0 4 351 352 373 372
		f 4 376 757 -397 -757
		mu 0 4 352 353 374 373
		f 4 377 758 -398 -758
		mu 0 4 353 354 375 374
		f 4 378 759 -399 -759
		mu 0 4 354 355 376 375
		f 4 379 740 -400 -760
		mu 0 4 355 356 377 376
		f 4 380 761 -401 -761
		mu 0 4 358 357 378 379
		f 4 381 762 -402 -762
		mu 0 4 357 359 380 378
		f 4 382 763 -403 -763
		mu 0 4 359 360 381 380
		f 4 383 764 -404 -764
		mu 0 4 360 361 382 381
		f 4 384 765 -405 -765
		mu 0 4 361 362 383 382
		f 4 385 766 -406 -766
		mu 0 4 362 363 384 383
		f 4 386 767 -407 -767
		mu 0 4 363 364 385 384
		f 4 387 768 -408 -768
		mu 0 4 364 365 386 385
		f 4 388 769 -409 -769
		mu 0 4 365 366 387 386
		f 4 389 770 -410 -770
		mu 0 4 366 367 388 387
		f 4 390 771 -411 -771
		mu 0 4 367 368 389 388
		f 4 391 772 -412 -772
		mu 0 4 368 369 390 389
		f 4 392 773 -413 -773
		mu 0 4 369 370 391 390
		f 4 393 774 -414 -774
		mu 0 4 370 371 392 391
		f 4 394 775 -415 -775
		mu 0 4 371 372 393 392
		f 4 395 776 -416 -776
		mu 0 4 372 373 394 393
		f 4 396 777 -417 -777
		mu 0 4 373 374 395 394
		f 4 397 778 -418 -778
		mu 0 4 374 375 396 395
		f 4 398 779 -419 -779
		mu 0 4 375 376 397 396
		f 4 399 760 -420 -780
		mu 0 4 376 377 398 397
		f 4 400 781 -421 -781
		mu 0 4 379 378 399 400
		f 4 401 782 -422 -782
		mu 0 4 378 380 401 399
		f 4 402 783 -423 -783
		mu 0 4 380 381 402 401
		f 4 403 784 -424 -784
		mu 0 4 381 382 403 402
		f 4 404 785 -425 -785
		mu 0 4 382 383 404 403
		f 4 405 786 -426 -786
		mu 0 4 383 384 405 404
		f 4 406 787 -427 -787
		mu 0 4 384 385 406 405
		f 4 407 788 -428 -788
		mu 0 4 385 386 407 406
		f 4 408 789 -429 -789
		mu 0 4 386 387 408 407
		f 4 409 790 -430 -790
		mu 0 4 387 388 409 408
		f 4 410 791 -431 -791
		mu 0 4 388 389 410 409
		f 4 411 792 -432 -792
		mu 0 4 389 390 411 410
		f 4 412 793 -433 -793
		mu 0 4 390 391 412 411
		f 4 413 794 -434 -794
		mu 0 4 391 392 413 412
		f 4 414 795 -435 -795
		mu 0 4 392 393 414 413
		f 4 415 796 -436 -796
		mu 0 4 393 394 415 414
		f 4 416 797 -437 -797
		mu 0 4 394 395 416 415
		f 4 417 798 -438 -798
		mu 0 4 395 396 417 416
		f 4 418 799 -439 -799
		mu 0 4 396 397 418 417
		f 4 419 780 -440 -800
		mu 0 4 397 398 419 418
		f 4 420 801 -441 -801
		mu 0 4 400 399 420 421
		f 4 421 802 -442 -802
		mu 0 4 399 401 422 420
		f 4 422 803 -443 -803
		mu 0 4 401 402 423 422
		f 4 423 804 -444 -804
		mu 0 4 402 403 424 423
		f 4 424 805 -445 -805
		mu 0 4 403 404 425 424
		f 4 425 806 -446 -806
		mu 0 4 404 405 426 425
		f 4 426 807 -447 -807
		mu 0 4 405 406 427 426
		f 4 427 808 -448 -808
		mu 0 4 406 407 428 427
		f 4 428 809 -449 -809
		mu 0 4 407 408 429 428
		f 4 429 810 -450 -810
		mu 0 4 408 409 430 429
		f 4 430 811 -451 -811
		mu 0 4 409 410 431 430
		f 4 431 812 -452 -812
		mu 0 4 410 411 432 431
		f 4 432 813 -453 -813
		mu 0 4 411 412 433 432
		f 4 433 814 -454 -814
		mu 0 4 412 413 434 433
		f 4 434 815 -455 -815
		mu 0 4 413 414 435 434
		f 4 435 816 -456 -816
		mu 0 4 414 415 436 435
		f 4 436 817 -457 -817
		mu 0 4 415 416 437 436
		f 4 437 818 -458 -818
		mu 0 4 416 417 438 437
		f 4 438 819 -459 -819
		mu 0 4 417 418 439 438
		f 4 439 800 -460 -820
		mu 0 4 418 419 440 439
		f 4 440 821 -461 -821
		mu 0 4 421 420 441 442
		f 4 441 822 -462 -822
		mu 0 4 420 422 443 441
		f 4 442 823 -463 -823
		mu 0 4 422 423 444 443
		f 4 443 824 -464 -824
		mu 0 4 423 424 445 444
		f 4 444 825 -465 -825
		mu 0 4 424 425 446 445
		f 4 445 826 -466 -826
		mu 0 4 425 426 447 446
		f 4 446 827 -467 -827
		mu 0 4 426 427 448 447
		f 4 447 828 -468 -828
		mu 0 4 427 428 449 448
		f 4 448 829 -469 -829
		mu 0 4 428 429 450 449
		f 4 449 830 -470 -830
		mu 0 4 429 430 451 450
		f 4 450 831 -471 -831
		mu 0 4 430 431 452 451
		f 4 451 832 -472 -832
		mu 0 4 431 432 453 452
		f 4 452 833 -473 -833
		mu 0 4 432 433 454 453
		f 4 453 834 -474 -834
		mu 0 4 433 434 455 454
		f 4 454 835 -475 -835
		mu 0 4 434 435 456 455
		f 4 455 836 -476 -836
		mu 0 4 435 436 457 456
		f 4 456 837 -477 -837
		mu 0 4 436 437 458 457
		f 4 457 838 -478 -838
		mu 0 4 437 438 459 458
		f 4 458 839 -479 -839
		mu 0 4 438 439 460 459
		f 4 459 820 -480 -840
		mu 0 4 439 440 461 460
		f 4 460 841 -481 -841
		mu 0 4 442 441 462 463
		f 4 461 842 -482 -842
		mu 0 4 441 443 464 462
		f 4 462 843 -483 -843
		mu 0 4 443 444 465 464
		f 4 463 844 -484 -844
		mu 0 4 444 445 466 465
		f 4 464 845 -485 -845
		mu 0 4 445 446 467 466
		f 4 465 846 -486 -846
		mu 0 4 446 447 468 467
		f 4 466 847 -487 -847
		mu 0 4 447 448 469 468
		f 4 467 848 -488 -848
		mu 0 4 448 449 470 469
		f 4 468 849 -489 -849
		mu 0 4 449 450 471 470
		f 4 469 850 -490 -850
		mu 0 4 450 451 472 471
		f 4 470 851 -491 -851
		mu 0 4 451 452 473 472
		f 4 471 852 -492 -852
		mu 0 4 452 453 474 473
		f 4 472 853 -493 -853
		mu 0 4 453 454 475 474
		f 4 473 854 -494 -854
		mu 0 4 454 455 476 475
		f 4 474 855 -495 -855
		mu 0 4 455 456 477 476
		f 4 475 856 -496 -856
		mu 0 4 456 457 478 477
		f 4 476 857 -497 -857
		mu 0 4 457 458 479 478
		f 4 477 858 -498 -858
		mu 0 4 458 459 480 479
		f 4 478 859 -499 -859
		mu 0 4 459 460 481 480
		f 4 479 840 -500 -860
		mu 0 4 460 461 482 481
		f 4 480 861 -501 -861
		mu 0 4 463 462 483 484
		f 4 481 862 -502 -862
		mu 0 4 462 464 485 483
		f 4 482 863 -503 -863
		mu 0 4 464 465 486 485
		f 4 483 864 -504 -864
		mu 0 4 465 466 487 486
		f 4 484 865 -505 -865
		mu 0 4 466 467 488 487
		f 4 485 866 -506 -866
		mu 0 4 467 468 489 488
		f 4 486 867 -507 -867
		mu 0 4 468 469 490 489
		f 4 487 868 -508 -868
		mu 0 4 469 470 491 490
		f 4 488 869 -509 -869
		mu 0 4 470 471 492 491
		f 4 489 870 -510 -870
		mu 0 4 471 472 493 492
		f 4 490 871 -511 -871
		mu 0 4 472 473 494 493
		f 4 491 872 -512 -872
		mu 0 4 473 474 495 494
		f 4 492 873 -513 -873
		mu 0 4 474 475 496 495
		f 4 493 874 -514 -874
		mu 0 4 475 476 497 496
		f 4 494 875 -515 -875
		mu 0 4 476 477 498 497
		f 4 495 876 -516 -876
		mu 0 4 477 478 499 498
		f 4 496 877 -517 -877
		mu 0 4 478 479 500 499
		f 4 497 878 -518 -878
		mu 0 4 479 480 501 500
		f 4 498 879 -519 -879
		mu 0 4 480 481 502 501
		f 4 499 860 -520 -880
		mu 0 4 481 482 503 502
		f 4 500 881 -521 -881
		mu 0 4 484 483 504 505
		f 4 501 882 -522 -882
		mu 0 4 483 485 506 504
		f 4 502 883 -523 -883
		mu 0 4 485 486 507 506
		f 4 503 884 -524 -884
		mu 0 4 486 487 508 507
		f 4 504 885 -525 -885
		mu 0 4 487 488 509 508
		f 4 505 886 -526 -886
		mu 0 4 488 489 510 509
		f 4 506 887 -527 -887
		mu 0 4 489 490 511 510
		f 4 507 888 -528 -888
		mu 0 4 490 491 512 511
		f 4 508 889 -529 -889
		mu 0 4 491 492 513 512
		f 4 509 890 -530 -890
		mu 0 4 492 493 514 513
		f 4 510 891 -531 -891
		mu 0 4 493 494 515 514
		f 4 511 892 -532 -892
		mu 0 4 494 495 516 515
		f 4 512 893 -533 -893
		mu 0 4 495 496 517 516
		f 4 513 894 -534 -894
		mu 0 4 496 497 518 517
		f 4 514 895 -535 -895
		mu 0 4 497 498 519 518
		f 4 515 896 -536 -896
		mu 0 4 498 499 520 519
		f 4 516 897 -537 -897
		mu 0 4 499 500 521 520
		f 4 517 898 -538 -898
		mu 0 4 500 501 522 521
		f 4 518 899 -539 -899
		mu 0 4 501 502 523 522
		f 4 519 880 -540 -900
		mu 0 4 502 503 524 523
		f 4 520 901 -541 -901
		mu 0 4 505 504 525 526
		f 4 521 902 -542 -902
		mu 0 4 504 506 527 525
		f 4 522 903 -543 -903
		mu 0 4 506 507 528 527
		f 4 523 904 -544 -904
		mu 0 4 507 508 529 528
		f 4 524 905 -545 -905
		mu 0 4 508 509 530 529
		f 4 525 906 -546 -906
		mu 0 4 509 510 531 530
		f 4 526 907 -547 -907
		mu 0 4 510 511 532 531
		f 4 527 908 -548 -908
		mu 0 4 511 512 533 532
		f 4 528 909 -549 -909
		mu 0 4 512 513 534 533
		f 4 529 910 -550 -910
		mu 0 4 513 514 535 534
		f 4 530 911 -551 -911
		mu 0 4 514 515 536 535
		f 4 531 912 -552 -912
		mu 0 4 515 516 537 536
		f 4 532 913 -553 -913
		mu 0 4 516 517 538 537
		f 4 533 914 -554 -914
		mu 0 4 517 518 539 538
		f 4 534 915 -555 -915
		mu 0 4 518 519 540 539
		f 4 535 916 -556 -916
		mu 0 4 519 520 541 540
		f 4 536 917 -557 -917
		mu 0 4 520 521 542 541
		f 4 537 918 -558 -918
		mu 0 4 521 522 543 542
		f 4 538 919 -559 -919
		mu 0 4 522 523 544 543
		f 4 539 900 -560 -920
		mu 0 4 523 524 545 544
		f 4 540 921 -561 -921
		mu 0 4 526 525 546 547
		f 4 541 922 -562 -922
		mu 0 4 525 527 548 546
		f 4 542 923 -563 -923
		mu 0 4 527 528 549 548
		f 4 543 924 -564 -924
		mu 0 4 528 529 550 549
		f 4 544 925 -565 -925
		mu 0 4 529 530 551 550
		f 4 545 926 -566 -926
		mu 0 4 530 531 552 551
		f 4 546 927 -567 -927
		mu 0 4 531 532 553 552
		f 4 547 928 -568 -928
		mu 0 4 532 533 554 553
		f 4 548 929 -569 -929
		mu 0 4 533 534 555 554
		f 4 549 930 -570 -930
		mu 0 4 534 535 556 555
		f 4 550 931 -571 -931
		mu 0 4 535 536 557 556
		f 4 551 932 -572 -932
		mu 0 4 536 537 558 557
		f 4 552 933 -573 -933
		mu 0 4 537 538 559 558
		f 4 553 934 -574 -934
		mu 0 4 538 539 560 559
		f 4 554 935 -575 -935
		mu 0 4 539 540 561 560
		f 4 555 936 -576 -936
		mu 0 4 540 541 562 561
		f 4 556 937 -577 -937
		mu 0 4 541 542 563 562
		f 4 557 938 -578 -938
		mu 0 4 542 543 564 563
		f 4 558 939 -579 -939
		mu 0 4 543 544 565 564
		f 4 559 920 -580 -940
		mu 0 4 544 545 566 565
		f 3 -201 -941 941
		mu 0 3 169 168 567
		f 3 -202 -942 942
		mu 0 3 172 169 568
		f 3 -203 -943 943
		mu 0 3 174 172 569
		f 3 -204 -944 944
		mu 0 3 176 174 570
		f 3 -205 -945 945
		mu 0 3 178 176 571
		f 3 -206 -946 946
		mu 0 3 180 178 572
		f 3 -207 -947 947
		mu 0 3 182 180 573
		f 3 -208 -948 948
		mu 0 3 184 182 574
		f 3 -209 -949 949
		mu 0 3 186 184 575
		f 3 -210 -950 950
		mu 0 3 188 186 576
		f 3 -211 -951 951
		mu 0 3 190 188 577
		f 3 -212 -952 952
		mu 0 3 192 190 578
		f 3 -213 -953 953
		mu 0 3 194 192 579
		f 3 -214 -954 954
		mu 0 3 196 194 580
		f 3 -215 -955 955
		mu 0 3 198 196 581
		f 3 -216 -956 956
		mu 0 3 200 198 582
		f 3 -217 -957 957
		mu 0 3 202 200 583
		f 3 -218 -958 958
		mu 0 3 204 202 584
		f 3 -219 -959 959
		mu 0 3 206 204 585
		f 3 -220 -960 940
		mu 0 3 208 206 586;
	setAttr ".fc[500:519]"
		f 3 560 961 -961
		mu 0 3 547 546 587
		f 3 561 962 -962
		mu 0 3 546 548 588
		f 3 562 963 -963
		mu 0 3 548 549 589
		f 3 563 964 -964
		mu 0 3 549 550 590
		f 3 564 965 -965
		mu 0 3 550 551 591
		f 3 565 966 -966
		mu 0 3 551 552 592
		f 3 566 967 -967
		mu 0 3 552 553 593
		f 3 567 968 -968
		mu 0 3 553 554 594
		f 3 568 969 -969
		mu 0 3 554 555 595
		f 3 569 970 -970
		mu 0 3 555 556 596
		f 3 570 971 -971
		mu 0 3 556 557 597
		f 3 571 972 -972
		mu 0 3 557 558 598
		f 3 572 973 -973
		mu 0 3 558 559 599
		f 3 573 974 -974
		mu 0 3 559 560 600
		f 3 574 975 -975
		mu 0 3 560 561 601
		f 3 575 976 -976
		mu 0 3 561 562 602
		f 3 576 977 -977
		mu 0 3 562 563 603
		f 3 577 978 -978
		mu 0 3 563 564 604
		f 3 578 979 -979
		mu 0 3 564 565 605
		f 3 579 960 -980
		mu 0 3 565 566 606;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder13";
	rename -uid "C7B93DC6-449D-3EF3-D755-FA86BA107AE2";
	setAttr ".t" -type "double3" 0 -66.666666666666671 12.663419087727867 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 4.567347796000913 5.167393270877116 4.567347796000913 ;
	setAttr ".rp" -type "double3" 0 -12.663419087727867 3.12177338585966e-031 ;
	setAttr ".rpt" -type "double3" 0 12.663419087727863 -12.663419087727867 ;
	setAttr ".sp" -type "double3" 0 -1.0000001411444472 2.4651903288156619e-032 ;
	setAttr ".spt" -type "double3" 0 -11.66341894658342 2.8752543529780938e-031 ;
createNode mesh -n "pCylinderShape13" -p "pCylinder13";
	rename -uid "E4918F24-473A-27AB-DC4F-68A9A5A5C20D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.84374997019767761 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 434 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.5 0.83749998 0.63374424 0.11216897
		 0.61376965 0.072966613 0.58265835 0.041855596 0.54345608 0.021881036 0.5 0.01499831
		 0.45654392 0.021881066 0.41734159 0.041855577 0.38623047 0.072966672 0.36625603 0.11216895
		 0.35937339 0.15562506 0.36625603 0.19908123 0.3862305 0.23828337 0.41734159 0.26939455
		 0.45654395 0.28936902 0.5 0.29625177 0.54345608 0.28936917 0.58265841 0.26939464
		 0.61376959 0.2382835 0.63374388 0.19908109 0.50000006 0.15000001 0.64062685 0.15562505
		 0.38749999 0.32853267 0.375 0.66310376 0.39999998 0.32853201 0.41249996 0.32853183
		 0.42499995 0.32853258 0.43749994 0.32853195 0.44999993 0.32853198 0.46249992 0.32853082
		 0.47499993 0.32853261 0.48749989 0.32853189 0.49999988 0.32853198 0.51249987 0.32853222
		 0.52499986 0.32853246 0.5374999 0.32853079 0.54999989 0.32853091 0.56249982 0.32853264
		 0.57499981 0.32853082 0.5874998 0.32853225 0.59999979 0.32853219 0.61249977 0.3285321
		 0.62499976 0.32853195 0.375 0.32853246 0.62499976 0.3125 0.64860266 0.10796607 0.375
		 0.3125 0.64130044 0.11004051 0.62640899 0.064408496 0.38749999 0.3125 0.62019736
		 0.068623349 0.59184152 0.029841021 0.39999998 0.3125 0.58732837 0.035754576 0.54828393
		 0.0076473355 0.41249996 0.3125 0.54591125 0.014651495 0.5 -7.4505806e-008 0.42499995
		 0.3125 0.5 0.0073798853 0.45171607 0.0076473504 0.43749994 0.3125 0.45408881 0.014651514
		 0.40815851 0.029841051 0.44999993 0.3125 0.4126716 0.035754576 0.37359107 0.064408526
		 0.46249992 0.3125 0.37980279 0.068623401 0.3513974 0.10796608 0.4749999 0.3125 0.35869971
		 0.11004052 0.34374997 0.15625 0.48749989 0.3125 0.35142815 0.15595177 0.3513974 0.20453392
		 0.49999988 0.3125 0.35869971 0.20186305 0.37359107 0.24809146 0.51249987 0.3125 0.37980279
		 0.2432801 0.40815854 0.28265893 0.52499986 0.3125 0.4126716 0.27614895 0.4517161
		 0.3048526 0.53749985 0.3125 0.45408881 0.29725197 0.5 0.3125 0.54999983 0.3125 0.5
		 0.30452359 0.54828387 0.3048526 0.56249982 0.3125 0.54591125 0.29725206 0.59184146
		 0.28265893 0.57499981 0.3125 0.58732837 0.27614897 0.62640893 0.24809146 0.5874998
		 0.3125 0.62019724 0.24328017 0.6486026 0.2045339 0.59999979 0.3125 0.6413002 0.20186298
		 0.65625 0.15625 0.61249977 0.3125 0.64857185 0.15595175 0.375 0.66463947 0.38749999
		 0.66310376 0.375 0.66310376 0.39999998 0.66310376 0.38749999 0.66310376 0.41249996
		 0.66310376 0.39999998 0.66310376 0.42499995 0.66310376 0.41249996 0.66310376 0.43749991
		 0.66310376 0.42499995 0.66310376 0.4499999 0.66310376 0.43749994 0.66310376 0.46249992
		 0.66310376 0.44999993 0.66310376 0.4749999 0.66310376 0.46249995 0.66310376 0.48749989
		 0.66310376 0.4749999 0.66310376 0.49999988 0.66310376 0.48749989 0.66310376 0.51249987
		 0.66310376 0.49999988 0.66310376 0.52499986 0.66310376 0.51249987 0.66310376 0.5374999
		 0.66310376 0.52499986 0.66310376 0.54999983 0.66310376 0.53749985 0.6631037 0.56249982
		 0.66310376 0.54999983 0.66310376 0.57499981 0.66310376 0.56249982 0.66310382 0.5874998
		 0.66310376 0.57499981 0.66310376 0.59999979 0.66310376 0.5874998 0.66310376 0.61249977
		 0.66310376 0.59999979 0.66310376 0.62499976 0.66310376 0.61249977 0.6631037 0.6249997
		 0.66310376 0.6249997 0.66364413 0.375 0.66364413 0.62499976 0.66415751 0.375 0.66415751
		 0.38749999 0.66415846 0.38749999 0.66364604 0.39999998 0.6641584 0.39999998 0.66364598
		 0.41249996 0.6641584 0.41249996 0.66364604 0.42499995 0.6641584 0.42499995 0.66364604
		 0.43749994 0.66415846 0.43749994 0.66364604 0.44999993 0.6641584 0.4499999 0.66364598
		 0.46249989 0.6641584 0.46249992 0.66364598 0.4749999 0.6641584 0.4749999 0.66364604
		 0.48749989 0.66415846 0.48749989 0.66364604 0.49999988 0.6641584 0.49999988 0.66364604
		 0.51249987 0.66415846 0.51249987 0.66364604 0.52499986 0.6641584 0.52499986 0.66364598
		 0.5374999 0.6641584 0.53749985 0.66364604 0.54999983 0.66415846 0.54999983 0.66364604
		 0.56249982 0.66415852 0.56249982 0.6636461 0.57499981 0.6641584 0.57499981 0.66364604
		 0.5874998 0.6641584 0.5874998 0.66364598 0.59999979 0.6641584 0.59999979 0.66364604
		 0.61249977 0.6641584 0.61249977 0.66364598 0.375 0.68843985 0.38749999 0.68843985
		 0.38749999 0.68843985 0.40000001 0.68843985 0.39999998 0.68843985 0.41249993 0.68843991
		 0.41249996 0.68843985 0.42499995 0.68843979 0.42499995 0.68843985 0.43749991 0.68843985
		 0.43749994 0.68843985 0.44999993 0.68843985 0.44999993 0.68843985 0.46249995 0.68843985
		 0.46249992 0.68843985 0.4749999 0.68843985 0.4749999 0.68843985 0.48749989 0.68843985
		 0.48749989 0.68843985 0.49999988 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985
		 0.51249987 0.68843985 0.52499986 0.68843985 0.52499986 0.68843985 0.5374999 0.68843985
		 0.53749985 0.68843985 0.54999983 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985
		 0.56249982 0.68843985 0.57499981 0.68843985 0.57499981 0.68843985 0.5874998 0.68843985
		 0.5874998 0.68843985 0.59999979 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985
		 0.62499976 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.38749999 0.66463947
		 0.375 0.68686891 0.39999998 0.66463947 0.38749999 0.68686891 0.41249996 0.66463947
		 0.39999998 0.68686891 0.42499995 0.66463947 0.41249996 0.68686891 0.43749994 0.66463947
		 0.42499995 0.68686885 0.44999987 0.66463947 0.43749994 0.68686891 0.46249986 0.66463947
		 0.44999993 0.68686885 0.4749999 0.66463947 0.46249992 0.68686891 0.48749989 0.66463947
		 0.47499987 0.68686891 0.49999988 0.66463947 0.48749989 0.68686891;
	setAttr ".uvst[0].uvsp[250:433]" 0.51249987 0.66463953 0.49999988 0.68686891
		 0.52499986 0.66463947 0.51249987 0.68686897 0.5374999 0.66463947 0.52499986 0.68686891
		 0.54999983 0.66463947 0.53749985 0.68686891 0.56249982 0.66463953 0.54999983 0.68686891
		 0.57499981 0.66463947 0.56249982 0.68686891 0.5874998 0.66463947 0.57499981 0.68686897
		 0.59999979 0.66463947 0.5874998 0.68686891 0.61249977 0.66463947 0.59999973 0.68686891
		 0.62499976 0.66463947 0.61249977 0.68686885 0.375 0.68843985 0.62499976 0.68686885
		 0.38749999 0.68788332 0.38749999 0.68736029 0.62499976 0.68788445 0.375 0.68815887
		 0.62499976 0.68736058 0.375 0.68789387 0.39999998 0.68815893 0.39999998 0.68789518
		 0.41249996 0.68815899 0.41249996 0.68789506 0.42499995 0.68815887 0.42499995 0.68789494
		 0.43749994 0.68815899 0.43749994 0.68789518 0.44999993 0.68815899 0.44999993 0.68789512
		 0.46249992 0.68815899 0.46249992 0.68789518 0.4749999 0.68815899 0.4749999 0.68789512
		 0.48749989 0.68815893 0.48749989 0.687895 0.49999988 0.68815899 0.49999988 0.68789506
		 0.51249987 0.68815899 0.51249987 0.68789512 0.52499986 0.68815893 0.52499986 0.68789512
		 0.53749985 0.68815899 0.53749985 0.68789518 0.54999983 0.68815899 0.54999983 0.68789512
		 0.56249982 0.68815899 0.56249982 0.68789512 0.57499981 0.68815893 0.57499981 0.68789506
		 0.5874998 0.68815905 0.5874998 0.68789518 0.59999979 0.68815887 0.59999979 0.68789506
		 0.61249977 0.68788326 0.61249977 0.68736029 0.62640893 0.93559146 0.6486026 0.89203393
		 0.59184152 0.97015887 0.62640893 0.9355914 0.54828387 0.9923526 0.59184146 0.97015893
		 0.5 1 0.54828399 0.99235255 0.4517161 0.9923526 0.5 1 0.40815854 0.97015893 0.45171615
		 0.99235266 0.37359107 0.93559146 0.40815854 0.97015893 0.3513974 0.89203393 0.37359107
		 0.93559146 0.34374997 0.84375 0.3513974 0.89203393 0.3513974 0.79546607 0.34374997
		 0.84375 0.37359107 0.75190854 0.3513974 0.79546607 0.40815851 0.71734107 0.37359104
		 0.75190854 0.45171607 0.69514734 0.40815857 0.71734107 0.5 0.68749994 0.45171613
		 0.69514734 0.54828393 0.69514734 0.5 0.68749994 0.59184152 0.71734101 0.54828399
		 0.69514734 0.62640899 0.75190848 0.59184152 0.71734101 0.64860266 0.79546607 0.62640899
		 0.75190848 0.65625 0.84375 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375
		 0.62640887 0.93559152 0.6486026 0.89203393 0.59184146 0.97015893 0.62640893 0.93559146
		 0.54828399 0.99235255 0.59184146 0.97015893 0.5 1 0.54828387 0.9923526 0.45171601
		 0.99235255 0.5 1 0.40815863 0.97015899 0.4517161 0.9923526 0.37359095 0.93559128
		 0.40815854 0.97015893 0.35139742 0.89203393 0.37359107 0.93559146 0.34374997 0.84375
		 0.3513974 0.89203393 0.35139745 0.79546595 0.34374997 0.84375 0.37359107 0.75190854
		 0.3513974 0.79546607 0.4081586 0.71734101 0.37359107 0.75190854 0.45171607 0.69514734
		 0.40815851 0.71734107 0.5 0.68749994 0.45171607 0.69514734 0.54828393 0.69514734
		 0.5 0.68749994 0.59184152 0.71734101 0.54828393 0.69514734 0.62640899 0.75190848
		 0.59184158 0.71734107 0.64860266 0.79546607 0.62640899 0.75190854 0.65625 0.84375
		 0.64860266 0.79546607 0.6486026 0.89203393 0.65625 0.84375 0.64854199 0.89201421
		 0.64854008 0.89201355 0.62635726 0.93555391 0.62635559 0.93555278 0.59180397 0.97010726
		 0.59180272 0.97010565 0.54826427 0.99229181 0.54826361 0.9922899 0.5 0.99993616 0.5
		 0.99993414 0.45173576 0.99229187 0.45173642 0.9922899 0.40819615 0.97010732 0.40819737
		 0.97010571 0.37364268 0.93555385 0.37364432 0.93555266 0.35145813 0.89201421 0.35146007
		 0.89201355 0.34381381 0.84375 0.34381586 0.84375 0.35145813 0.79548573 0.3514601
		 0.79548633 0.37364265 0.75194609 0.37364438 0.75194722 0.40819615 0.71739268 0.40819734
		 0.71739429 0.45173576 0.69520807 0.45173645 0.69520998 0.5 0.68756378 0.5 0.6875658
		 0.54826427 0.69520807 0.54826361 0.69520998 0.59180403 0.71739268 0.59180278 0.71739429
		 0.62635732 0.75194603 0.62635571 0.75194722 0.64854205 0.79548579 0.64854014 0.79548645
		 0.65620756 0.84375 0.65622658 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 23 ".pt";
	setAttr ".pt[21]" -type "float3" 0.34371245 1.7802787 -0.11167892 ;
	setAttr ".pt[22]" -type "float3" 0.29237911 1.7802787 -0.21242613 ;
	setAttr ".pt[23]" -type "float3" -8.5274382e-007 1.7802787 -1.2215905e-007 ;
	setAttr ".pt[24]" -type "float3" 0.21242596 1.7802787 -0.29237935 ;
	setAttr ".pt[25]" -type "float3" 0.11167759 1.7802787 -0.34371218 ;
	setAttr ".pt[26]" -type "float3" -8.5274382e-007 1.7802787 -0.36140072 ;
	setAttr ".pt[27]" -type "float3" -0.11167943 1.7802787 -0.34371218 ;
	setAttr ".pt[28]" -type "float3" -0.21242803 1.7802787 -0.29237911 ;
	setAttr ".pt[29]" -type "float3" -0.29237941 1.7802787 -0.2124261 ;
	setAttr ".pt[30]" -type "float3" -0.34371328 1.7802787 -0.11167891 ;
	setAttr ".pt[31]" -type "float3" -0.36140051 1.7802787 -1.1922219e-007 ;
	setAttr ".pt[32]" -type "float3" -0.34371328 1.7802787 0.11167881 ;
	setAttr ".pt[33]" -type "float3" -0.29237941 1.7802787 0.21242566 ;
	setAttr ".pt[34]" -type "float3" -0.21242803 1.7802787 0.29237884 ;
	setAttr ".pt[35]" -type "float3" -0.11167943 1.7802787 0.34371206 ;
	setAttr ".pt[36]" -type "float3" -8.5274382e-007 1.7802787 0.36140025 ;
	setAttr ".pt[37]" -type "float3" 0.11167759 1.7802787 0.34371206 ;
	setAttr ".pt[38]" -type "float3" 0.2124256 1.7802787 0.29237884 ;
	setAttr ".pt[39]" -type "float3" 0.29237911 1.7802787 0.21242566 ;
	setAttr ".pt[40]" -type "float3" 0.34371245 1.7802787 0.11167877 ;
	setAttr ".pt[41]" -type "float3" 0.36140051 1.7802787 -1.2664059e-007 ;
	setAttr -s 382 ".vt";
	setAttr ".vt[0:165]"  1.42563295 1.33444607 -0.46321517 1.21271753 1.33444607 -0.88108784
		 0.88109303 1.33444607 -1.21271324 0.46321726 1.33444607 -1.42562973 2.8610229e-006 1.33444607 -1.49899578
		 -0.46321487 1.33444607 -1.42562973 -0.8810873 1.33444607 -1.212713 -1.21270943 1.33444607 -0.8810876
		 -1.42562485 1.33444607 -0.4632149 -1.49898911 1.33444607 8.9227264e-008 -1.42562485 1.33444607 0.46321523
		 -1.21270943 1.33444607 0.88108778 -0.8810873 1.33444607 1.21271241 -0.46321487 1.33444607 1.42562973
		 2.8610229e-006 1.33444607 1.49899507 0.46321726 1.33444607 1.42562962 0.88109255 1.33444607 1.21271241
		 1.21271753 1.33444607 0.88108766 1.42563295 1.33444607 0.46321511 1.49899721 1.33444607 8.9227264e-008
		 2.8610229e-006 -1 0 0.86489248 -0.79374325 -0.28101873 0.73572159 -0.79374325 -0.53452981
		 2.8610229e-006 -0.79374325 2.4323526e-008 0.53453493 -0.79374325 -0.73571658 0.28102064 -0.79374325 -0.86488682
		 2.8610229e-006 -0.79374325 -0.90939558 -0.28101492 -0.79374325 -0.86488682 -0.53452969 -0.79374325 -0.73571599
		 -0.73571301 -0.79374325 -0.53452951 -0.86488438 -0.79374325 -0.28101861 -0.90939045 -0.79374319 3.1713974e-008
		 -0.86488438 -0.79374319 0.28101885 -0.73571301 -0.79374319 0.53452951 -0.53452969 -0.79374319 0.73571599
		 -0.28101492 -0.79374319 0.86488682 2.8610229e-006 -0.79374325 0.90939504 0.28102064 -0.79374319 0.86488682
		 0.5345335 -0.79374319 0.73571599 0.73572159 -0.79374319 0.53452951 0.86489248 -0.79374319 0.28101867
		 0.90940046 -0.79374325 1.3047155e-008 1.39365196 0.86521232 -0.45282313 1.1855092 0.86521232 -0.86132091
		 0.86132622 0.86521232 -1.18550658 0.45282841 0.86521232 -1.39364624 2.8610229e-006 0.86521232 -1.46536624
		 -0.45281887 0.86521232 -1.39364624 -0.86131573 0.86521232 -1.18550622 -1.1855011 0.86521232 -0.86132079
		 -1.39363956 0.86521232 -0.45282283 -1.46536255 0.86521232 8.3213898e-008 -1.39363956 0.86521232 0.45282313
		 -1.1855011 0.86521232 0.86132091 -0.86131573 0.86521232 1.18550599 -0.45281887 0.86521232 1.39364624
		 2.8610229e-006 0.86521232 1.46536601 0.45282793 0.86521232 1.39364624 0.86132574 0.86521232 1.18550551
		 1.1855092 0.86521232 0.86132079 1.39364767 0.86521232 0.45282307 1.46537066 0.86521232 8.3213898e-008
		 0.97129774 -0.91470987 -0.31559283 0.94874239 -0.95866162 -0.30826336 0.90754414 -0.98911774 -0.29487693
		 0.855968 -1 -0.2781187 0.82623386 -0.91470993 -0.6002934 0.80704784 -0.95866162 -0.58635175
		 0.77200222 -0.98911774 -0.56088918 0.72812748 -1 -0.52901357 0.6002984 -0.91470987 -0.82623309
		 0.58635712 -0.95866162 -0.80704379 0.56089449 -0.98911774 -0.77199805 0.52901888 -1 -0.72812486
		 0.31559801 -0.91470987 -0.97129512 0.30826712 -0.95866162 -0.94873714 0.29487896 -0.98911774 -0.90753806
		 0.27812338 -1 -0.8559621 2.8610229e-006 -0.91470987 -1.021279812 2.8610229e-006 -0.95866162 -0.99756092
		 2.8610229e-006 -0.98911774 -0.95424163 2.8610229e-006 -1 -0.90001106 -0.31559038 -0.91470993 -0.971295
		 -0.30826044 -0.95866162 -0.94873697 -0.29487324 -0.98911774 -0.90753788 -0.27811241 -1 -0.85596156
		 -0.60029316 -0.91470987 -0.82623267 -0.58634663 -0.95866162 -0.80704373 -0.56088448 -0.98911774 -0.77199745
		 -0.52901363 -1 -0.72812426 -0.826231 -0.91470987 -0.60029334 -0.80704308 -0.95866162 -0.58635169
		 -0.7719965 -0.98911774 -0.56088918 -0.72812462 -1 -0.52901351 -0.97128963 -0.91470987 -0.31559283
		 -0.94873428 -0.95866162 -0.30826333 -0.90753746 -0.98911774 -0.29487702 -0.85596085 -1 -0.27811921
		 -1.021273613 -0.91470987 3.805102e-009 -0.99755764 -0.95866162 1.8442573e-009 -0.95423889 -0.98911774 4.8549836e-010
		 -0.90000725 -1 -2.2201951e-017 -0.97128963 -0.91470987 0.31559283 -0.94873428 -0.95866162 0.30826333
		 -0.90753746 -0.98911774 0.29487702 -0.85596085 -1 0.27811921 -0.826231 -0.91470987 0.60029328
		 -0.80704308 -0.95866162 0.58635163 -0.7719965 -0.98911774 0.56088918 -0.72812462 -1 0.52901345
		 -0.60029316 -0.91470987 0.82623267 -0.58634663 -0.95866162 0.80704367 -0.56088448 -0.98911774 0.77199745
		 -0.52901363 -1 0.72812426 -0.31559038 -0.91470993 0.97129476 -0.30826044 -0.95866162 0.94873673
		 -0.29487324 -0.98911774 0.90753746 -0.27811241 -1 0.85596156 2.8610229e-006 -0.91470987 1.021279335
		 2.8610229e-006 -0.95866162 0.9975608 2.8610229e-006 -0.98911774 0.95424145 2.8610229e-006 -1 0.90001106
		 0.31559753 -0.91470987 0.97129476 0.30826712 -0.95866162 0.94873673 0.29487896 -0.98911774 0.90753746
		 0.27812004 -1 0.85596156 0.60029793 -0.91470987 0.82623267 0.58635664 -0.95866162 0.80704367
		 0.56089401 -0.98911774 0.77199745 0.5290184 -1 0.72812426 0.82623386 -0.91470993 0.60029316
		 0.80704784 -0.95866162 0.58635151 0.77200222 -0.98911774 0.56088918 0.72812748 -1 0.5290134
		 0.97129774 -0.91470987 0.3155928 0.9487381 -0.95866162 0.30826315 0.90754271 -0.98911774 0.29487672
		 0.85596657 -1 0.27811867 1.021281719 -0.91470987 3.805102e-009 0.99756575 -0.95866162 -2.4780354e-008
		 0.95424461 -0.98911774 -1.0071276e-007 0.90001535 -1 -2.0874633e-007 1.42490864 0.86441112 -0.46297929
		 1.44089222 0.86781001 -0.46817297 1.45301914 0.87795174 -0.47211251 1.45818567 0.89224756 -0.47379145
		 1.24040747 0.89224756 -0.90120488 1.23601103 0.87795174 -0.89801139 1.22569799 0.86781001 -0.89051795
		 1.21210098 0.86441112 -0.88063872 0.90120983 0.89224756 -1.24040222 0.89801502 0.87795174 -1.23600626
		 0.89052296 0.86781001 -1.22569287 0.88064384 0.86441112 -1.21209562 0.47379684 0.89224756 -1.45818019
		 0.47211552 0.87795174 -1.45301294 0.46817732 0.86781001 -1.44088817 0.46298218 0.86441112 -1.42490375
		 2.8610229e-006 0.89224756 -1.53322101 2.8610229e-006 0.87795174 -1.52778733 2.8610229e-006 0.86781001 -1.51503921
		 2.8610229e-006 0.86441112 -1.49823213 -0.47378826 0.89224756 -1.45818019 -0.47210884 0.87795174 -1.45301294
		 -0.46816921 0.86781001 -1.44088817 -0.4629755 0.86441112 -1.42490375;
	setAttr ".vt[166:331]" -0.90119743 0.89224756 -1.24040139 -0.8980093 0.87795174 -1.23600626
		 -0.89051533 0.86781001 -1.22569251 -0.88063717 0.86441112 -1.21209478 -1.2403965 0.89224756 -0.90120476
		 -1.23600292 0.87795174 -0.89801133 -1.22568989 0.86781001 -0.89051741 -1.21209335 0.86441112 -0.88063872
		 -1.45817661 0.89224756 -0.47379112 -1.45300579 0.87795174 -0.47211215 -1.4408865 0.86781001 -0.46817264
		 -1.42490196 0.86441112 -0.46297896 -1.53321838 0.89224756 9.8020692e-008 -1.52778053 0.87795174 9.7921699e-008
		 -1.51503658 0.86781001 9.8037063e-008 -1.49822998 0.86441112 9.833721e-008 -1.45817661 0.89224756 0.47379145
		 -1.45300579 0.87795174 0.47211251 -1.4408865 0.86781001 0.46817297 -1.42490196 0.86441112 0.46297929
		 -1.2403965 0.89224756 0.90120488 -1.23600292 0.87795174 0.89801139 -1.22568989 0.86781001 0.89051783
		 -1.21209335 0.86441112 0.88063872 -0.90119743 0.89224756 1.24040139 -0.8980093 0.87795174 1.2360059
		 -0.89051533 0.86781001 1.22569168 -0.88063717 0.86441112 1.21209478 -0.47378826 0.89224756 1.45818007
		 -0.47210884 0.87795174 1.45301294 -0.46816921 0.86781001 1.44088757 -0.4629755 0.86441112 1.42490363
		 2.8610229e-006 0.89224756 1.53322089 2.8610229e-006 0.87795174 1.52778733 2.8610229e-006 0.86781001 1.51503897
		 2.8610229e-006 0.86441112 1.49823189 0.47379637 0.89224756 1.45817995 0.47211552 0.87795174 1.45301282
		 0.46817732 0.86781001 1.44088757 0.46298218 0.86441112 1.42490304 0.90120935 0.89224756 1.24040115
		 0.89801502 0.87795174 1.23600566 0.89052248 0.86781001 1.22569168 0.88064337 0.86441112 1.21209455
		 1.24040461 0.89224756 0.90120476 1.23601103 0.87795174 0.89801133 1.22569799 0.86781001 0.89051777
		 1.21209812 0.86441112 0.88063872 1.45818377 0.89224756 0.47379136 1.45301676 0.87795174 0.47211242
		 1.44089222 0.86781001 0.46817291 1.42490768 0.86441112 0.46297923 1.53322601 0.89224756 9.8020685e-008
		 1.52779293 0.87795174 8.7899743e-008 1.51504326 0.86781001 6.0506437e-008 1.49823475 0.86441112 2.2840377e-008
		 1.23991776 1.33524764 -0.90084934 1.2530694 1.3316251 -0.91040558 1.2624855 1.32107341 -0.91724694
		 1.26534891 1.30675614 -0.91932589 1.45760965 1.33524764 -0.47360447 1.47307205 1.3316251 -0.47862846
		 1.48414135 1.32107341 -0.48222515 1.48750448 1.30675614 -0.48331812 0.90085316 1.33524764 -1.23991215
		 0.91040945 1.3316251 -1.25306535 0.91725063 1.32107341 -1.26248169 0.91932821 1.30675614 -1.26534307
		 0.47360992 1.33524764 -1.45760477 0.47863388 1.33162534 -1.47306669 0.48223066 1.32107341 -1.48413658
		 0.48332357 1.30675614 -1.48750043 2.8610229e-006 1.33524764 -1.53261542 2.8610229e-006 1.3316251 -1.54887342
		 2.8610229e-006 1.32107341 -1.5605129 2.8610229e-006 1.30675614 -1.5640496 -0.47359943 1.33524764 -1.45760477
		 -0.4786272 1.3316251 -1.47306669 -0.48222351 1.32107341 -1.48413658 -0.48331642 1.30675614 -1.48750043
		 -0.90084839 1.33524764 -1.23991215 -0.91040421 1.3316251 -1.25306535 -0.91724586 1.32107341 -1.26248169
		 -0.91932106 1.30675614 -1.26534307 -1.23990726 1.33524764 -0.90084922 -1.25306034 1.33162534 -0.9104054
		 -1.26247978 1.32107341 -0.9172467 -1.26534081 1.30675614 -0.91932577 -1.45759964 1.33524764 -0.47360426
		 -1.4730587 1.3316251 -0.47862822 -1.484128 1.32107341 -0.48222488 -1.48749542 1.30675614 -0.48331785
		 -1.53260708 1.33524764 1.0484373e-007 -1.54886723 1.33162534 1.0433673e-007 -1.56050491 1.32107341 1.0385151e-007
		 -1.56404495 1.30675614 1.0353334e-007 -1.45759964 1.33524764 0.47360468 -1.4730587 1.3316251 0.47862861
		 -1.484128 1.32107341 0.48222521 -1.48749542 1.30675614 0.48331824 -1.23990726 1.33524764 0.90084934
		 -1.25306034 1.33162534 0.9104057 -1.26247978 1.32107341 0.91724694 -1.26534081 1.30675614 0.91932589
		 -0.90084839 1.33524764 1.23991203 -0.91040421 1.3316251 1.25306511 -0.91724586 1.32107341 1.26248133
		 -0.91932106 1.30675614 1.26534283 -0.47359943 1.33524764 1.45760465 -0.4786272 1.3316251 1.47306657
		 -0.48222351 1.32107341 1.48413646 -0.48331642 1.30675614 1.48750043 2.8610229e-006 1.33524764 1.53261542
		 2.8610229e-006 1.3316251 1.54887342 2.8610229e-006 1.32107341 1.5605129 2.8610229e-006 1.30675614 1.5640496
		 0.47360992 1.33524764 1.45760453 0.47863388 1.33162534 1.47306645 0.48223066 1.32107341 1.48413634
		 0.48332357 1.30675614 1.48750031 0.90085316 1.33524764 1.23991203 0.91040945 1.3316251 1.25306499
		 0.91725063 1.32107341 1.26248121 0.91932821 1.30675614 1.26534271 1.23991537 1.33524764 0.90084922
		 1.2530694 1.3316251 0.9104054 1.2624855 1.32107341 0.91724682 1.26534796 1.30675614 0.91932589
		 1.45760918 1.33524764 0.47360441 1.47307014 1.3316251 0.47862846 1.48414087 1.32107341 0.48222515
		 1.48750448 1.30675614 0.48331812 1.53261948 1.33524764 8.8115613e-008 1.54887819 1.33162534 9.5848442e-008
		 1.56051731 1.32107341 1.0155723e-007 1.56405306 1.30675614 1.0353334e-007 1.29838896 1.33444607 -0.42187101
		 1.27576256 1.32949889 -0.41451934 1.25800467 1.31572211 -0.40874937 1.24892759 1.29606164 -0.4058001
		 1.10447693 1.33444607 -0.8024466 1.085229874 1.32949889 -0.78846288 1.070124149 1.31572211 -0.77748775
		 1.062402725 1.29606164 -0.77187777 0.80245066 1.33444607 -1.10447288 0.78846693 1.32949889 -1.085225821
		 0.77749157 1.31572211 -1.070119977 0.77188158 1.29606164 -1.062398553 0.42187643 1.33444607 -1.29838562
		 0.41452456 1.32949889 -1.27575958 0.40875483 1.31572211 -1.25800133 0.40580559 1.29606164 -1.24892437
		 2.8610229e-006 1.33444607 -1.3652029 2.8610229e-006 1.32949889 -1.34141243 2.8610229e-006 1.31572211 -1.32274055
		 2.8610229e-006 1.29606164 -1.31319606 -0.42186928 1.33444607 -1.29838562 -0.41451836 1.32949889 -1.27575958
		 -0.4087472 1.31572211 -1.25800133 -0.40579748 1.29606164 -1.24892437 -0.80244541 1.33444607 -1.10447228
		 -0.78845978 1.32949889 -1.085224986 -0.77748489 1.31572211 -1.070119143 -0.77187538 1.29606164 -1.062397838
		 -1.10447121 1.33444607 -0.80244642 -1.085224152 1.32949889 -0.78846252;
	setAttr ".vt[332:381]" -1.070116997 1.31572211 -0.77748752 -1.062396049 1.29606187 -0.77187753
		 -1.29838371 1.33444607 -0.42187077 -1.27575874 1.32949889 -0.41451913 -1.25800037 1.31572211 -0.40874916
		 -1.2489233 1.29606187 -0.40579984 -1.365201 1.33444607 1.4558977e-007 -1.34141064 1.32949889 1.2206371e-007
		 -1.32273865 1.31572211 1.0523855e-007 -1.31319427 1.29606187 9.8728684e-008 -1.29838276 1.33444607 0.42187101
		 -1.27575779 1.32949889 0.41451937 -1.25799942 1.31572211 0.40874943 -1.24892139 1.29606164 0.40580013
		 -1.10447121 1.33444607 0.80244654 -1.085226059 1.32949889 0.78846282 -1.07011795 1.31572211 0.7774877
		 -1.062397957 1.29606187 0.77187771 -0.80244541 1.33444607 1.10447228 -0.78845978 1.32949889 1.085224986
		 -0.77748489 1.31572211 1.070119143 -0.77187538 1.29606164 1.062397838 -0.42186928 1.33444607 1.29838538
		 -0.41451836 1.32949889 1.27575934 -0.4087472 1.31572211 1.25800133 -0.40579748 1.29606164 1.24892414
		 2.8610229e-006 1.33444607 1.3652029 2.8610229e-006 1.32949889 1.34141243 2.8610229e-006 1.31572211 1.32274055
		 2.8610229e-006 1.29606164 1.31319606 0.42187595 1.33444607 1.29838538 0.41452456 1.32949889 1.27575934
		 0.40875435 1.31572211 1.25800133 0.40580511 1.29606164 1.24892414 0.80245066 1.33444607 1.10447228
		 0.78846693 1.32949889 1.085225224 0.77749157 1.31572211 1.070119143 0.77188158 1.29606164 1.062397838
		 1.10447693 1.33444607 0.80244654 1.085229874 1.32949889 0.7884627 1.070124149 1.31572211 0.77748758
		 1.062402725 1.29606164 0.77187759 1.29838896 1.33444607 0.42187077 1.27576256 1.32949889 0.41451913
		 1.25800467 1.31572211 0.40874922 1.24892759 1.29606164 0.40579987 1.36520576 1.33444607 7.5683509e-008
		 1.34141493 1.32949889 7.4094409e-008 1.32274294 1.31572211 7.2690582e-008 1.31319809 1.29606164 7.177325e-008;
	setAttr -s 780 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 8 1 8 9 1
		 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1 17 18 1 18 19 1 19 0 1
		 21 22 0 22 23 1 21 23 1 22 24 0 24 23 1 24 25 0 25 23 1 25 26 0 26 23 1 26 27 0 27 23 1
		 27 28 0 28 23 1 28 29 0 29 23 1 29 30 0 30 23 1 30 31 0 31 23 1 31 32 0 32 23 1 32 33 0
		 33 23 1 33 34 0 34 23 1 34 35 0 35 23 1 35 36 0 36 23 1 36 37 0 37 23 1 37 38 0 38 23 1
		 38 39 0 39 23 1 39 40 0 40 23 1 40 41 0 41 23 1 41 21 0 42 43 0 43 44 0 44 45 0 45 46 0
		 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0
		 57 58 0 58 59 0 59 60 0 60 61 0 61 42 0 67 66 1 66 62 1 68 67 1 65 69 1 69 68 1 65 64 1
		 141 65 1 64 63 1 63 62 1 62 138 1 71 70 1 70 66 1 72 71 1 69 73 1 73 72 1 75 74 1
		 74 70 1 76 75 1 73 77 1 77 76 1 79 78 1 78 74 1 80 79 1 77 81 1 81 80 1 83 82 1 82 78 1
		 84 83 1 81 85 1 85 84 1 87 86 1 86 82 1 88 87 1 85 89 1 89 88 1 91 90 1 90 86 1 92 91 1
		 89 93 1 93 92 1 95 94 1 94 90 1 96 95 1 93 97 1 97 96 1 99 98 1 98 94 1 100 99 1
		 97 101 1 101 100 1 103 102 1 102 98 1 104 103 1 101 105 1 105 104 1 107 106 1 106 102 1
		 108 107 1 105 109 1 109 108 1 111 110 1 110 106 1 112 111 1 109 113 1 113 112 1 115 114 1
		 114 110 1 116 115 1 113 117 1 117 116 1 119 118 1 118 114 1 120 119 1 117 121 1 121 120 1
		 123 122 1 122 118 1 124 123 1 121 125 1 125 124 1 127 126 1 126 122 1 128 127 1 125 129 1
		 129 128 1 131 130 1;
	setAttr ".ed[166:331]" 130 126 1 132 131 1 129 133 1 133 132 1 135 134 1 134 130 1
		 136 135 1 133 137 1 137 136 1 139 138 1 138 134 1 140 139 1 137 141 1 141 140 1 65 20 1
		 20 69 1 20 73 1 20 77 1 20 81 1 20 85 1 20 89 1 20 93 1 20 97 1 20 101 1 20 105 1
		 20 109 1 20 113 1 20 117 1 20 121 1 20 125 1 20 129 1 20 133 1 20 137 1 20 141 1
		 66 43 1 42 62 1 70 44 1 74 45 1 78 46 1 82 47 1 86 48 1 90 49 1 94 50 1 98 51 1 102 52 1
		 106 53 1 110 54 1 114 55 1 118 56 1 122 57 1 126 58 1 130 59 1 134 60 1 138 61 1
		 64 68 1 63 67 1 68 72 1 67 71 1 72 76 1 71 75 1 76 80 1 75 79 1 80 84 1 79 83 1 84 88 1
		 83 87 1 88 92 1 87 91 1 92 96 1 91 95 1 96 100 1 95 99 1 100 104 1 99 103 1 104 108 1
		 103 107 1 108 112 1 107 111 1 112 116 1 111 115 1 116 120 1 115 119 1 120 124 1 119 123 1
		 124 128 1 123 127 1 128 132 1 127 131 1 132 136 1 131 135 1 136 140 1 135 139 1 64 140 1
		 63 139 1 221 142 1 145 218 1 145 144 1 144 147 1 147 146 1 146 145 1 144 143 1 143 148 1
		 148 147 1 143 142 1 142 149 1 149 148 1 151 150 1 150 146 1 152 151 1 149 153 1 153 152 1
		 155 154 1 154 150 1 156 155 1 153 157 1 157 156 1 159 158 1 158 154 1 160 159 1 157 161 1
		 161 160 1 163 162 1 162 158 1 164 163 1 161 165 1 165 164 1 167 166 1 166 162 1 168 167 1
		 165 169 1 169 168 1 171 170 1 170 166 1 172 171 1 169 173 1 173 172 1 175 174 1 174 170 1
		 176 175 1 173 177 1 177 176 1 179 178 1 178 174 1 180 179 1 177 181 1 181 180 1 183 182 1
		 182 178 1 184 183 1 181 185 1 185 184 1 187 186 1 186 182 1 188 187 1 185 189 1 189 188 1
		 191 190 1 190 186 1 192 191 1 189 193 1 193 192 1 195 194 1 194 190 1 196 195 1 193 197 1
		 197 196 1;
	setAttr ".ed[332:497]" 199 198 1 198 194 1 200 199 1 197 201 1 201 200 1 203 202 1
		 202 198 1 204 203 1 201 205 1 205 204 1 207 206 1 206 202 1 208 207 1 205 209 1 209 208 1
		 211 210 1 210 206 1 212 211 1 209 213 1 213 212 1 215 214 1 214 210 1 216 215 1 213 217 1
		 217 216 1 219 218 1 218 214 1 220 219 1 217 221 1 221 220 1 43 149 1 142 42 1 44 153 1
		 45 157 1 46 161 1 47 165 1 48 169 1 49 173 1 50 177 1 51 181 1 52 185 1 53 189 1
		 54 193 1 55 197 1 56 201 1 57 205 1 58 209 1 59 213 1 60 217 1 61 221 1 148 152 1
		 147 151 1 152 156 1 151 155 1 156 160 1 155 159 1 160 164 1 159 163 1 164 168 1 163 167 1
		 168 172 1 167 171 1 172 176 1 171 175 1 176 180 1 175 179 1 180 184 1 179 183 1 184 188 1
		 183 187 1 188 192 1 187 191 1 192 196 1 191 195 1 196 200 1 195 199 1 200 204 1 199 203 1
		 204 208 1 203 207 1 208 212 1 207 211 1 212 216 1 211 215 1 216 220 1 215 219 1 143 220 1
		 144 219 1 231 230 1 230 222 1 232 231 1 225 233 1 233 232 1 225 224 1 229 225 1 224 223 1
		 223 222 1 222 226 1 229 228 1 301 229 1 228 227 1 227 226 1 226 298 1 235 234 1 234 230 1
		 236 235 1 233 237 1 237 236 1 239 238 1 238 234 1 240 239 1 237 241 1 241 240 1 243 242 1
		 242 238 1 244 243 1 241 245 1 245 244 1 247 246 1 246 242 1 248 247 1 245 249 1 249 248 1
		 251 250 1 250 246 1 252 251 1 249 253 1 253 252 1 255 254 1 254 250 1 256 255 1 253 257 1
		 257 256 1 259 258 1 258 254 1 260 259 1 257 261 1 261 260 1 263 262 1 262 258 1 264 263 1
		 261 265 1 265 264 1 267 266 1 266 262 1 268 267 1 265 269 1 269 268 1 271 270 1 270 266 1
		 272 271 1 269 273 1 273 272 1 275 274 1 274 270 1 276 275 1 273 277 1 277 276 1 279 278 1
		 278 274 1 280 279 1 277 281 1 281 280 1 283 282 1 282 278 1 284 283 1;
	setAttr ".ed[498:663]" 281 285 1 285 284 1 287 286 1 286 282 1 288 287 1 285 289 1
		 289 288 1 291 290 1 290 286 1 292 291 1 289 293 1 293 292 1 295 294 1 294 290 1 296 295 1
		 293 297 1 297 296 1 299 298 1 298 294 1 300 299 1 297 301 1 301 300 1 0 226 1 222 1 1
		 230 2 1 234 3 1 238 4 1 242 5 1 246 6 1 250 7 1 254 8 1 258 9 1 262 10 1 266 11 1
		 270 12 1 274 13 1 278 14 1 282 15 1 286 16 1 290 17 1 294 18 1 298 19 1 146 225 1
		 229 145 1 150 233 1 154 237 1 158 241 1 162 245 1 166 249 1 170 253 1 174 257 1 178 261 1
		 182 265 1 186 269 1 190 273 1 194 277 1 198 281 1 202 285 1 206 289 1 210 293 1 214 297 1
		 218 301 1 224 232 0 223 231 0 224 228 0 223 227 0 232 236 0 231 235 0 236 240 0 235 239 0
		 240 244 0 239 243 0 244 248 0 243 247 0 248 252 0 247 251 0 252 256 0 251 255 0 256 260 0
		 255 259 0 260 264 0 259 263 0 264 268 0 263 267 0 268 272 0 267 271 0 272 276 0 271 275 0
		 276 280 0 275 279 0 280 284 0 279 283 0 284 288 0 283 287 0 288 292 0 287 291 0 292 296 0
		 291 295 0 296 300 0 295 299 0 228 300 0 227 299 0 379 378 1 378 302 1 380 379 1 305 381 1
		 381 380 1 305 304 1 309 305 1 304 303 1 303 302 1 302 306 1 309 308 1 313 309 1 308 307 1
		 307 306 1 306 310 1 313 312 1 317 313 1 312 311 1 311 310 1 310 314 1 317 316 1 321 317 1
		 316 315 1 315 314 1 314 318 1 321 320 1 325 321 1 320 319 1 319 318 1 318 322 1 325 324 1
		 329 325 1 324 323 1 323 322 1 322 326 1 329 328 1 333 329 1 328 327 1 327 326 1 326 330 1
		 333 332 1 337 333 1 332 331 1 331 330 1 330 334 1 337 336 1 341 337 1 336 335 1 335 334 1
		 334 338 1 341 340 1 345 341 1 340 339 1 339 338 1 338 342 1 345 344 1 349 345 1 344 343 1
		 343 342 1 342 346 1 349 348 1 353 349 1 348 347 1 347 346 1;
	setAttr ".ed[664:779]" 346 350 1 353 352 1 357 353 1 352 351 1 351 350 1 350 354 1
		 357 356 1 361 357 1 356 355 1 355 354 1 354 358 1 361 360 1 365 361 1 360 359 1 359 358 1
		 358 362 1 365 364 1 369 365 1 364 363 1 363 362 1 362 366 1 369 368 1 373 369 1 368 367 1
		 367 366 1 366 370 1 373 372 1 377 373 1 372 371 1 371 370 1 370 374 1 377 376 1 381 377 1
		 376 375 1 375 374 1 374 378 1 1 306 1 302 0 1 2 310 1 3 314 1 4 318 1 5 322 1 6 326 1
		 7 330 1 8 334 1 9 338 1 10 342 1 11 346 1 12 350 1 13 354 1 14 358 1 15 362 1 16 366 1
		 17 370 1 18 374 1 19 378 1 309 22 1 21 305 1 313 24 1 317 25 1 321 26 1 325 27 1
		 329 28 1 333 29 1 337 30 1 341 31 1 345 32 1 349 33 1 353 34 1 357 35 1 361 36 1
		 365 37 1 369 38 1 373 39 1 377 40 1 381 41 1 304 380 1 303 379 1 304 308 1 303 307 1
		 308 312 1 307 311 1 312 316 1 311 315 1 316 320 1 315 319 1 320 324 1 319 323 1 324 328 1
		 323 327 1 328 332 1 327 331 1 332 336 1 331 335 1 336 340 1 335 339 1 340 344 1 339 343 1
		 344 348 1 343 347 1 348 352 1 347 351 1 352 356 1 351 355 1 356 360 1 355 359 1 360 364 1
		 359 363 1 364 368 1 363 367 1 368 372 1 367 371 1 372 376 1 371 375 1 376 380 1 375 379 1;
	setAttr -s 400 -ch 1560 ".fc[0:399]" -type "polyFaces" 
		f 3 20 21 -23
		mu 0 3 355 357 0
		f 3 23 24 -22
		mu 0 3 357 359 0
		f 3 25 26 -25
		mu 0 3 359 361 0
		f 3 27 28 -27
		mu 0 3 361 363 0
		f 3 29 30 -29
		mu 0 3 363 365 0
		f 3 31 32 -31
		mu 0 3 365 367 0
		f 3 33 34 -33
		mu 0 3 367 369 0
		f 3 35 36 -35
		mu 0 3 369 371 0
		f 3 37 38 -37
		mu 0 3 371 373 0
		f 3 39 40 -39
		mu 0 3 373 375 0
		f 3 41 42 -41
		mu 0 3 375 377 0
		f 3 43 44 -43
		mu 0 3 377 379 0
		f 3 45 46 -45
		mu 0 3 379 381 0
		f 3 47 48 -47
		mu 0 3 381 383 0
		f 3 49 50 -49
		mu 0 3 383 385 0
		f 3 51 52 -51
		mu 0 3 385 387 0
		f 3 53 54 -53
		mu 0 3 387 389 0
		f 3 55 56 -55
		mu 0 3 389 391 0
		f 3 57 58 -57
		mu 0 3 391 393 0
		f 3 59 22 -59
		mu 0 3 393 355 0
		f 3 -84 180 181
		mu 0 3 2 1 20
		f 3 -94 -182 182
		mu 0 3 3 2 20
		f 3 -99 -183 183
		mu 0 3 4 3 20
		f 3 -104 -184 184
		mu 0 3 5 4 20
		f 3 -109 -185 185
		mu 0 3 6 5 20
		f 3 -114 -186 186
		mu 0 3 7 6 20
		f 3 -119 -187 187
		mu 0 3 8 7 20
		f 3 -124 -188 188
		mu 0 3 9 8 20
		f 3 -129 -189 189
		mu 0 3 10 9 20
		f 3 -134 -190 190
		mu 0 3 11 10 20
		f 3 -139 -191 191
		mu 0 3 12 11 20
		f 3 -144 -192 192
		mu 0 3 13 12 20
		f 3 -149 -193 193
		mu 0 3 14 13 20
		f 3 -154 -194 194
		mu 0 3 15 14 20
		f 3 -159 -195 195
		mu 0 3 16 15 20
		f 3 -164 -196 196
		mu 0 3 17 16 20
		f 3 -169 -197 197
		mu 0 3 18 17 20
		f 3 -174 -198 198
		mu 0 3 19 18 20
		f 3 -179 -199 199
		mu 0 3 21 19 20
		f 3 -87 -200 -181
		mu 0 3 1 21 20
		f 4 -82 200 -61 201
		mu 0 4 43 22 106 23
		f 4 -92 202 -62 -201
		mu 0 4 22 24 108 106
		f 4 -97 203 -63 -203
		mu 0 4 24 25 110 108
		f 4 -102 204 -64 -204
		mu 0 4 25 26 112 110
		f 4 -107 205 -65 -205
		mu 0 4 26 27 114 112
		f 4 -112 206 -66 -206
		mu 0 4 27 28 116 114
		f 4 -117 207 -67 -207
		mu 0 4 28 29 118 116
		f 4 -122 208 -68 -208
		mu 0 4 29 30 120 118
		f 4 -127 209 -69 -209
		mu 0 4 30 31 122 120
		f 4 -132 210 -70 -210
		mu 0 4 31 32 124 122
		f 4 -137 211 -71 -211
		mu 0 4 32 33 126 124
		f 4 -142 212 -72 -212
		mu 0 4 33 34 128 126
		f 4 -147 213 -73 -213
		mu 0 4 34 35 130 128
		f 4 -152 214 -74 -214
		mu 0 4 35 36 132 130
		f 4 -157 215 -75 -215
		mu 0 4 36 37 134 132
		f 4 -162 216 -76 -216
		mu 0 4 37 38 136 134
		f 4 -167 217 -77 -217
		mu 0 4 38 39 138 136
		f 4 -172 218 -78 -218
		mu 0 4 39 40 140 138
		f 4 -177 219 -79 -219
		mu 0 4 40 41 142 140
		f 4 -90 -202 -80 -220
		mu 0 4 41 42 144 142
		f 4 -86 83 84 -221
		mu 0 4 47 1 2 50
		f 4 -89 221 80 81
		mu 0 4 43 46 49 22
		f 4 -88 220 82 -222
		mu 0 4 45 47 50 48
		f 4 -85 93 94 -223
		mu 0 4 50 2 3 53
		f 4 -81 223 90 91
		mu 0 4 22 49 52 24
		f 4 -83 222 92 -224
		mu 0 4 48 50 53 51
		f 4 -95 98 99 -225
		mu 0 4 53 3 4 56
		f 4 -91 225 95 96
		mu 0 4 24 52 55 25
		f 4 -93 224 97 -226
		mu 0 4 51 53 56 54
		f 4 -100 103 104 -227
		mu 0 4 56 4 5 59
		f 4 -96 227 100 101
		mu 0 4 25 55 58 26
		f 4 -98 226 102 -228
		mu 0 4 54 56 59 57
		f 4 -105 108 109 -229
		mu 0 4 59 5 6 62
		f 4 -101 229 105 106
		mu 0 4 26 58 61 27
		f 4 -103 228 107 -230
		mu 0 4 57 59 62 60
		f 4 -110 113 114 -231
		mu 0 4 62 6 7 65
		f 4 -106 231 110 111
		mu 0 4 27 61 64 28
		f 4 -108 230 112 -232
		mu 0 4 60 62 65 63
		f 4 -115 118 119 -233
		mu 0 4 65 7 8 68
		f 4 -111 233 115 116
		mu 0 4 28 64 67 29
		f 4 -113 232 117 -234
		mu 0 4 63 65 68 66
		f 4 -120 123 124 -235
		mu 0 4 68 8 9 71
		f 4 -116 235 120 121
		mu 0 4 29 67 70 30
		f 4 -118 234 122 -236
		mu 0 4 66 68 71 69
		f 4 -125 128 129 -237
		mu 0 4 71 9 10 74
		f 4 -121 237 125 126
		mu 0 4 30 70 73 31
		f 4 -123 236 127 -238
		mu 0 4 69 71 74 72
		f 4 -130 133 134 -239
		mu 0 4 74 10 11 77
		f 4 -126 239 130 131
		mu 0 4 31 73 76 32
		f 4 -128 238 132 -240
		mu 0 4 72 74 77 75
		f 4 -135 138 139 -241
		mu 0 4 77 11 12 80
		f 4 -131 241 135 136
		mu 0 4 32 76 79 33
		f 4 -133 240 137 -242
		mu 0 4 75 77 80 78
		f 4 -140 143 144 -243
		mu 0 4 80 12 13 83
		f 4 -136 243 140 141
		mu 0 4 33 79 82 34
		f 4 -138 242 142 -244
		mu 0 4 78 80 83 81
		f 4 -145 148 149 -245
		mu 0 4 83 13 14 86
		f 4 -141 245 145 146
		mu 0 4 34 82 85 35
		f 4 -143 244 147 -246
		mu 0 4 81 83 86 84
		f 4 -150 153 154 -247
		mu 0 4 86 14 15 89
		f 4 -146 247 150 151
		mu 0 4 35 85 88 36
		f 4 -148 246 152 -248
		mu 0 4 84 86 89 87
		f 4 -155 158 159 -249
		mu 0 4 89 15 16 92
		f 4 -151 249 155 156
		mu 0 4 36 88 91 37
		f 4 -153 248 157 -250
		mu 0 4 87 89 92 90
		f 4 -160 163 164 -251
		mu 0 4 92 16 17 95
		f 4 -156 251 160 161
		mu 0 4 37 91 94 38
		f 4 -158 250 162 -252
		mu 0 4 90 92 95 93
		f 4 -165 168 169 -253
		mu 0 4 95 17 18 98
		f 4 -161 253 165 166
		mu 0 4 38 94 97 39
		f 4 -163 252 167 -254
		mu 0 4 93 95 98 96
		f 4 -170 173 174 -255
		mu 0 4 98 18 19 101
		f 4 -166 255 170 171
		mu 0 4 39 97 100 40
		f 4 -168 254 172 -256
		mu 0 4 96 98 101 99
		f 4 -175 178 179 -257
		mu 0 4 101 19 21 104
		f 4 -171 257 175 176
		mu 0 4 40 100 103 41
		f 4 -173 256 177 -258
		mu 0 4 99 101 104 102
		f 4 85 258 -180 86
		mu 0 4 1 47 104 21
		f 4 87 259 -178 -259
		mu 0 4 47 45 102 104
		f 4 88 89 -176 -260
		mu 0 4 44 42 41 103
		f 4 262 263 264 265
		mu 0 4 105 150 151 230
		f 4 266 267 268 -264
		mu 0 4 150 148 152 151
		f 4 269 270 271 -268
		mu 0 4 148 107 109 152
		f 4 60 362 -271 363
		mu 0 4 23 106 109 107
		f 4 61 364 -276 -363
		mu 0 4 106 108 111 109
		f 4 62 365 -281 -365
		mu 0 4 108 110 113 111
		f 4 63 366 -286 -366
		mu 0 4 110 112 115 113
		f 4 64 367 -291 -367
		mu 0 4 112 114 117 115
		f 4 65 368 -296 -368
		mu 0 4 114 116 119 117
		f 4 66 369 -301 -369
		mu 0 4 116 118 121 119
		f 4 67 370 -306 -370
		mu 0 4 118 120 123 121
		f 4 68 371 -311 -371
		mu 0 4 120 122 125 123
		f 4 69 372 -316 -372
		mu 0 4 122 124 127 125
		f 4 70 373 -321 -373
		mu 0 4 124 126 129 127
		f 4 71 374 -326 -374
		mu 0 4 126 128 131 129
		f 4 72 375 -331 -375
		mu 0 4 128 130 133 131
		f 4 73 376 -336 -376
		mu 0 4 130 132 135 133
		f 4 74 377 -341 -377
		mu 0 4 132 134 137 135
		f 4 75 378 -346 -378
		mu 0 4 134 136 139 137
		f 4 76 379 -351 -379
		mu 0 4 136 138 141 139
		f 4 77 380 -356 -380
		mu 0 4 138 140 143 141
		f 4 78 381 -361 -381
		mu 0 4 140 142 145 143
		f 4 79 -364 -261 -382
		mu 0 4 142 144 146 145
		f 4 -272 275 276 -383
		mu 0 4 152 109 111 154
		f 4 -265 383 272 273
		mu 0 4 230 151 153 232
		f 4 -269 382 274 -384
		mu 0 4 151 152 154 153
		f 4 -277 280 281 -385
		mu 0 4 154 111 113 156
		f 4 -273 385 277 278
		mu 0 4 232 153 155 234
		f 4 -275 384 279 -386
		mu 0 4 153 154 156 155
		f 4 -282 285 286 -387
		mu 0 4 156 113 115 158
		f 4 -278 387 282 283
		mu 0 4 234 155 157 236
		f 4 -280 386 284 -388
		mu 0 4 155 156 158 157
		f 4 -287 290 291 -389
		mu 0 4 158 115 117 160
		f 4 -283 389 287 288
		mu 0 4 236 157 159 238
		f 4 -285 388 289 -390
		mu 0 4 157 158 160 159
		f 4 -292 295 296 -391
		mu 0 4 160 117 119 162
		f 4 -288 391 292 293
		mu 0 4 238 159 161 240
		f 4 -290 390 294 -392
		mu 0 4 159 160 162 161
		f 4 -297 300 301 -393
		mu 0 4 162 119 121 164
		f 4 -293 393 297 298
		mu 0 4 240 161 163 242
		f 4 -295 392 299 -394
		mu 0 4 161 162 164 163
		f 4 -302 305 306 -395
		mu 0 4 164 121 123 166
		f 4 -298 395 302 303
		mu 0 4 242 163 165 244
		f 4 -300 394 304 -396
		mu 0 4 163 164 166 165
		f 4 -307 310 311 -397
		mu 0 4 166 123 125 168
		f 4 -303 397 307 308
		mu 0 4 244 165 167 246
		f 4 -305 396 309 -398
		mu 0 4 165 166 168 167
		f 4 -312 315 316 -399
		mu 0 4 168 125 127 170
		f 4 -308 399 312 313
		mu 0 4 246 167 169 248
		f 4 -310 398 314 -400
		mu 0 4 167 168 170 169
		f 4 -317 320 321 -401
		mu 0 4 170 127 129 172
		f 4 -313 401 317 318
		mu 0 4 248 169 171 250
		f 4 -315 400 319 -402
		mu 0 4 169 170 172 171
		f 4 -322 325 326 -403
		mu 0 4 172 129 131 174
		f 4 -318 403 322 323
		mu 0 4 250 171 173 252
		f 4 -320 402 324 -404
		mu 0 4 171 172 174 173
		f 4 -327 330 331 -405
		mu 0 4 174 131 133 176
		f 4 -323 405 327 328
		mu 0 4 252 173 175 254
		f 4 -325 404 329 -406
		mu 0 4 173 174 176 175
		f 4 -332 335 336 -407
		mu 0 4 176 133 135 178
		f 4 -328 407 332 333
		mu 0 4 254 175 177 256
		f 4 -330 406 334 -408
		mu 0 4 175 176 178 177
		f 4 -337 340 341 -409
		mu 0 4 178 135 137 180
		f 4 -333 409 337 338
		mu 0 4 256 177 179 258
		f 4 -335 408 339 -410
		mu 0 4 177 178 180 179
		f 4 -342 345 346 -411
		mu 0 4 180 137 139 182
		f 4 -338 411 342 343
		mu 0 4 258 179 181 260
		f 4 -340 410 344 -412
		mu 0 4 179 180 182 181
		f 4 -347 350 351 -413
		mu 0 4 182 139 141 184
		f 4 -343 413 347 348
		mu 0 4 260 181 183 262
		f 4 -345 412 349 -414
		mu 0 4 181 182 184 183
		f 4 -352 355 356 -415
		mu 0 4 184 141 143 186
		f 4 -348 415 352 353
		mu 0 4 262 183 185 264
		f 4 -350 414 354 -416
		mu 0 4 183 184 186 185
		f 4 -357 360 361 -417
		mu 0 4 186 143 145 188
		f 4 -353 417 357 358
		mu 0 4 264 185 187 266
		f 4 -355 416 359 -418
		mu 0 4 185 186 188 187
		f 4 -270 418 -362 260
		mu 0 4 146 147 188 145
		f 4 -267 419 -360 -419
		mu 0 4 147 149 187 188
		f 4 -263 261 -358 -420
		mu 0 4 149 268 266 187
		f 4 -1 520 -430 521
		mu 0 4 191 189 270 190
		f 4 -2 -522 -422 522
		mu 0 4 193 191 190 192
		f 4 -3 -523 -437 523
		mu 0 4 195 193 192 194
		f 4 -4 -524 -442 524
		mu 0 4 197 195 194 196
		f 4 -5 -525 -447 525
		mu 0 4 199 197 196 198
		f 4 -6 -526 -452 526
		mu 0 4 201 199 198 200
		f 4 -7 -527 -457 527
		mu 0 4 203 201 200 202
		f 4 -8 -528 -462 528
		mu 0 4 205 203 202 204
		f 4 -9 -529 -467 529
		mu 0 4 207 205 204 206
		f 4 -10 -530 -472 530
		mu 0 4 209 207 206 208
		f 4 -11 -531 -477 531
		mu 0 4 211 209 208 210
		f 4 -12 -532 -482 532
		mu 0 4 213 211 210 212
		f 4 -13 -533 -487 533
		mu 0 4 215 213 212 214
		f 4 -14 -534 -492 534
		mu 0 4 217 215 214 216
		f 4 -15 -535 -497 535
		mu 0 4 219 217 216 218
		f 4 -16 -536 -502 536
		mu 0 4 221 219 218 220
		f 4 -17 -537 -507 537
		mu 0 4 223 221 220 222
		f 4 -18 -538 -512 538
		mu 0 4 225 223 222 224
		f 4 -19 -539 -517 539
		mu 0 4 228 225 224 226
		f 4 -20 -540 -435 -521
		mu 0 4 227 228 226 229
		f 4 -266 540 -427 541
		mu 0 4 105 230 233 231
		f 4 -274 542 -424 -541
		mu 0 4 230 232 235 233
		f 4 -279 543 -439 -543
		mu 0 4 232 234 237 235
		f 4 -284 544 -444 -544
		mu 0 4 234 236 239 237
		f 4 -289 545 -449 -545
		mu 0 4 236 238 241 239
		f 4 -294 546 -454 -546
		mu 0 4 238 240 243 241
		f 4 -299 547 -459 -547
		mu 0 4 240 242 245 243
		f 4 -304 548 -464 -548
		mu 0 4 242 244 247 245
		f 4 -309 549 -469 -549
		mu 0 4 244 246 249 247
		f 4 -314 550 -474 -550
		mu 0 4 246 248 251 249
		f 4 -319 551 -479 -551
		mu 0 4 248 250 253 251
		f 4 -324 552 -484 -552
		mu 0 4 250 252 255 253
		f 4 -329 553 -489 -553
		mu 0 4 252 254 257 255
		f 4 -334 554 -494 -554
		mu 0 4 254 256 259 257
		f 4 -339 555 -499 -555
		mu 0 4 256 258 261 259
		f 4 -344 556 -504 -556
		mu 0 4 258 260 263 261
		f 4 -349 557 -509 -557
		mu 0 4 260 262 265 263
		f 4 -354 558 -514 -558
		mu 0 4 262 264 267 265
		f 4 -359 559 -519 -559
		mu 0 4 264 266 269 267
		f 4 -262 -542 -432 -560
		mu 0 4 266 268 271 269
		f 4 -426 423 424 -561
		mu 0 4 273 233 235 279
		f 4 -429 561 420 421
		mu 0 4 190 272 278 192
		f 4 -428 560 422 -562
		mu 0 4 272 273 279 278
		f 4 425 562 -431 426
		mu 0 4 233 273 277 231
		f 4 427 563 -433 -563
		mu 0 4 273 272 275 277
		f 4 428 429 -434 -564
		mu 0 4 272 190 270 275
		f 4 -425 438 439 -565
		mu 0 4 279 235 237 281
		f 4 -421 565 435 436
		mu 0 4 192 278 280 194
		f 4 -423 564 437 -566
		mu 0 4 278 279 281 280
		f 4 -440 443 444 -567
		mu 0 4 281 237 239 283
		f 4 -436 567 440 441
		mu 0 4 194 280 282 196
		f 4 -438 566 442 -568
		mu 0 4 280 281 283 282
		f 4 -445 448 449 -569
		mu 0 4 283 239 241 285
		f 4 -441 569 445 446
		mu 0 4 196 282 284 198
		f 4 -443 568 447 -570
		mu 0 4 282 283 285 284
		f 4 -450 453 454 -571
		mu 0 4 285 241 243 287
		f 4 -446 571 450 451
		mu 0 4 198 284 286 200
		f 4 -448 570 452 -572
		mu 0 4 284 285 287 286
		f 4 -455 458 459 -573
		mu 0 4 287 243 245 289
		f 4 -451 573 455 456
		mu 0 4 200 286 288 202
		f 4 -453 572 457 -574
		mu 0 4 286 287 289 288
		f 4 -460 463 464 -575
		mu 0 4 289 245 247 291
		f 4 -456 575 460 461
		mu 0 4 202 288 290 204
		f 4 -458 574 462 -576
		mu 0 4 288 289 291 290
		f 4 -465 468 469 -577
		mu 0 4 291 247 249 293
		f 4 -461 577 465 466
		mu 0 4 204 290 292 206
		f 4 -463 576 467 -578
		mu 0 4 290 291 293 292
		f 4 -470 473 474 -579
		mu 0 4 293 249 251 295
		f 4 -466 579 470 471
		mu 0 4 206 292 294 208
		f 4 -468 578 472 -580
		mu 0 4 292 293 295 294
		f 4 -475 478 479 -581
		mu 0 4 295 251 253 297
		f 4 -471 581 475 476
		mu 0 4 208 294 296 210
		f 4 -473 580 477 -582
		mu 0 4 294 295 297 296
		f 4 -480 483 484 -583
		mu 0 4 297 253 255 299
		f 4 -476 583 480 481
		mu 0 4 210 296 298 212
		f 4 -478 582 482 -584
		mu 0 4 296 297 299 298
		f 4 -485 488 489 -585
		mu 0 4 299 255 257 301
		f 4 -481 585 485 486
		mu 0 4 212 298 300 214
		f 4 -483 584 487 -586
		mu 0 4 298 299 301 300
		f 4 -490 493 494 -587
		mu 0 4 301 257 259 303
		f 4 -486 587 490 491
		mu 0 4 214 300 302 216
		f 4 -488 586 492 -588
		mu 0 4 300 301 303 302
		f 4 -495 498 499 -589
		mu 0 4 303 259 261 305
		f 4 -491 589 495 496
		mu 0 4 216 302 304 218
		f 4 -493 588 497 -590
		mu 0 4 302 303 305 304
		f 4 -500 503 504 -591
		mu 0 4 305 261 263 307
		f 4 -496 591 500 501
		mu 0 4 218 304 306 220
		f 4 -498 590 502 -592
		mu 0 4 304 305 307 306
		f 4 -505 508 509 -593
		mu 0 4 307 263 265 309
		f 4 -501 593 505 506
		mu 0 4 220 306 308 222
		f 4 -503 592 507 -594
		mu 0 4 306 307 309 308
		f 4 -510 513 514 -595
		mu 0 4 309 265 267 311
		f 4 -506 595 510 511
		mu 0 4 222 308 310 224
		f 4 -508 594 512 -596
		mu 0 4 308 309 311 310
		f 4 -515 518 519 -597
		mu 0 4 311 267 269 313
		f 4 -511 597 515 516
		mu 0 4 224 310 312 226
		f 4 -513 596 517 -598
		mu 0 4 310 311 313 312
		f 4 430 598 -520 431
		mu 0 4 271 276 313 269
		f 4 432 599 -518 -599
		mu 0 4 276 274 312 313
		f 4 433 434 -516 -600
		mu 0 4 274 229 226 312
		f 4 0 700 -610 701
		mu 0 4 352 314 317 315
		f 4 1 702 -615 -701
		mu 0 4 314 316 319 317
		f 4 2 703 -620 -703
		mu 0 4 316 318 321 319
		f 4 3 704 -625 -704
		mu 0 4 318 320 323 321
		f 4 4 705 -630 -705
		mu 0 4 320 322 325 323
		f 4 5 706 -635 -706
		mu 0 4 322 324 327 325
		f 4 6 707 -640 -707
		mu 0 4 324 326 329 327
		f 4 7 708 -645 -708
		mu 0 4 326 328 331 329
		f 4 8 709 -650 -709
		mu 0 4 328 330 333 331
		f 4 9 710 -655 -710
		mu 0 4 330 332 335 333
		f 4 10 711 -660 -711
		mu 0 4 332 334 337 335
		f 4 11 712 -665 -712
		mu 0 4 334 336 339 337
		f 4 12 713 -670 -713
		mu 0 4 336 338 341 339
		f 4 13 714 -675 -714
		mu 0 4 338 340 343 341
		f 4 14 715 -680 -715
		mu 0 4 340 342 345 343
		f 4 15 716 -685 -716
		mu 0 4 342 344 347 345
		f 4 16 717 -690 -717
		mu 0 4 344 346 349 347
		f 4 17 718 -695 -718
		mu 0 4 346 348 351 349
		f 4 18 719 -700 -719
		mu 0 4 348 350 353 351
		f 4 19 -702 -602 -720
		mu 0 4 350 352 315 353
		f 4 -607 720 -21 721
		mu 0 4 392 354 357 355
		f 4 -612 722 -24 -721
		mu 0 4 354 356 359 357
		f 4 -617 723 -26 -723
		mu 0 4 356 358 361 359
		f 4 -622 724 -28 -724
		mu 0 4 358 360 363 361
		f 4 -627 725 -30 -725
		mu 0 4 360 362 365 363
		f 4 -632 726 -32 -726
		mu 0 4 362 364 367 365
		f 4 -637 727 -34 -727
		mu 0 4 364 366 369 367
		f 4 -642 728 -36 -728
		mu 0 4 366 368 371 369
		f 4 -647 729 -38 -729
		mu 0 4 368 370 373 371
		f 4 -652 730 -40 -730
		mu 0 4 370 372 375 373
		f 4 -657 731 -42 -731
		mu 0 4 372 374 377 375
		f 4 -662 732 -44 -732
		mu 0 4 374 376 379 377
		f 4 -667 733 -46 -733
		mu 0 4 376 378 381 379
		f 4 -672 734 -48 -734
		mu 0 4 378 380 383 381
		f 4 -677 735 -50 -735
		mu 0 4 380 382 385 383
		f 4 -682 736 -52 -736
		mu 0 4 382 384 387 385
		f 4 -687 737 -54 -737
		mu 0 4 384 386 389 387
		f 4 -692 738 -56 -738
		mu 0 4 386 388 391 389
		f 4 -697 739 -58 -739
		mu 0 4 388 390 393 391
		f 4 -604 -722 -60 -740
		mu 0 4 390 392 355 393
		f 4 -606 603 604 -741
		mu 0 4 395 392 390 433
		f 4 -609 741 600 601
		mu 0 4 315 394 432 353
		f 4 -608 740 602 -742
		mu 0 4 394 395 433 432
		f 4 605 742 -611 606
		mu 0 4 392 395 397 354
		f 4 607 743 -613 -743
		mu 0 4 395 394 396 397
		f 4 608 609 -614 -744
		mu 0 4 394 315 317 396
		f 4 610 744 -616 611
		mu 0 4 354 397 399 356
		f 4 612 745 -618 -745
		mu 0 4 397 396 398 399
		f 4 613 614 -619 -746
		mu 0 4 396 317 319 398
		f 4 615 746 -621 616
		mu 0 4 356 399 401 358
		f 4 617 747 -623 -747
		mu 0 4 399 398 400 401
		f 4 618 619 -624 -748
		mu 0 4 398 319 321 400
		f 4 620 748 -626 621
		mu 0 4 358 401 403 360
		f 4 622 749 -628 -749
		mu 0 4 401 400 402 403
		f 4 623 624 -629 -750
		mu 0 4 400 321 323 402
		f 4 625 750 -631 626
		mu 0 4 360 403 405 362
		f 4 627 751 -633 -751
		mu 0 4 403 402 404 405
		f 4 628 629 -634 -752
		mu 0 4 402 323 325 404
		f 4 630 752 -636 631
		mu 0 4 362 405 407 364
		f 4 632 753 -638 -753
		mu 0 4 405 404 406 407
		f 4 633 634 -639 -754
		mu 0 4 404 325 327 406
		f 4 635 754 -641 636
		mu 0 4 364 407 409 366
		f 4 637 755 -643 -755
		mu 0 4 407 406 408 409
		f 4 638 639 -644 -756
		mu 0 4 406 327 329 408
		f 4 640 756 -646 641
		mu 0 4 366 409 411 368
		f 4 642 757 -648 -757
		mu 0 4 409 408 410 411
		f 4 643 644 -649 -758
		mu 0 4 408 329 331 410
		f 4 645 758 -651 646
		mu 0 4 368 411 413 370
		f 4 647 759 -653 -759
		mu 0 4 411 410 412 413
		f 4 648 649 -654 -760
		mu 0 4 410 331 333 412
		f 4 650 760 -656 651
		mu 0 4 370 413 415 372
		f 4 652 761 -658 -761
		mu 0 4 413 412 414 415
		f 4 653 654 -659 -762
		mu 0 4 412 333 335 414
		f 4 655 762 -661 656
		mu 0 4 372 415 417 374
		f 4 657 763 -663 -763
		mu 0 4 415 414 416 417
		f 4 658 659 -664 -764
		mu 0 4 414 335 337 416
		f 4 660 764 -666 661
		mu 0 4 374 417 419 376
		f 4 662 765 -668 -765
		mu 0 4 417 416 418 419
		f 4 663 664 -669 -766
		mu 0 4 416 337 339 418
		f 4 665 766 -671 666
		mu 0 4 376 419 421 378
		f 4 667 767 -673 -767
		mu 0 4 419 418 420 421
		f 4 668 669 -674 -768
		mu 0 4 418 339 341 420
		f 4 670 768 -676 671
		mu 0 4 378 421 423 380
		f 4 672 769 -678 -769
		mu 0 4 421 420 422 423
		f 4 673 674 -679 -770
		mu 0 4 420 341 343 422
		f 4 675 770 -681 676
		mu 0 4 380 423 425 382
		f 4 677 771 -683 -771
		mu 0 4 423 422 424 425
		f 4 678 679 -684 -772
		mu 0 4 422 343 345 424
		f 4 680 772 -686 681
		mu 0 4 382 425 427 384
		f 4 682 773 -688 -773
		mu 0 4 425 424 426 427
		f 4 683 684 -689 -774
		mu 0 4 424 345 347 426
		f 4 685 774 -691 686
		mu 0 4 384 427 429 386
		f 4 687 775 -693 -775
		mu 0 4 427 426 428 429
		f 4 688 689 -694 -776
		mu 0 4 426 347 349 428
		f 4 690 776 -696 691
		mu 0 4 386 429 431 388
		f 4 692 777 -698 -777
		mu 0 4 429 428 430 431
		f 4 693 694 -699 -778
		mu 0 4 428 349 351 430
		f 4 695 778 -605 696
		mu 0 4 388 431 433 390
		f 4 697 779 -603 -779
		mu 0 4 431 430 432 433
		f 4 698 699 -601 -780
		mu 0 4 430 351 353 432;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane2";
	rename -uid "915EC454-4AEF-A402-E973-57A7110FD05C";
	setAttr ".t" -type "double3" 0 -66.666666666666671 12.79490592154734 ;
	setAttr ".s" -type "double3" 1 1 51.15779181143504 ;
createNode mesh -n "pPlaneShape2" -p "pPlane2";
	rename -uid "6387679B-49DA-7316-6621-359D82DFD988";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane3";
	rename -uid "72D48238-4B5E-DD5C-E1A3-32BF30F0F5C2";
	setAttr ".t" -type "double3" 2.0851477918970573e-005 -65.322763731947632 37.795613108943478 ;
	setAttr ".s" -type "double3" 2.5244213279315177 2.5244213279315177 2.5244213279315177 ;
createNode mesh -n "pPlaneShape3" -p "pPlane3";
	rename -uid "7FB892AB-4BF7-8C60-01DD-B3B181C4B2FC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane4";
	rename -uid "4C055B9B-40C1-A290-E0FA-E9A9FDB781D9";
	setAttr ".t" -type "double3" 0 -71.744645329388504 37.764892889361008 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".s" -type "double3" 4.5384921227277202 4.5384921227277202 4.5384921227277202 ;
createNode mesh -n "pPlaneShape4" -p "pPlane4";
	rename -uid "624C947A-4BEA-969A-A2BC-F9A05AE236C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "pPlane4";
	rename -uid "F100F941-4B83-7519-7EEB-6D833277F141";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 0 1.1292964e-015 
		0.48037246 0 1.1292964e-015 0 0 0.48037246 0 0 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane6";
	rename -uid "01902A68-4294-598F-76A2-C3A13EBE95F9";
	setAttr ".t" -type "double3" 0 -64.181088845496902 39.345277298417408 ;
	setAttr ".r" -type "double3" -52.980626216174777 0 0 ;
	setAttr ".s" -type "double3" 1.0619562408045629 1.0619562408045629 3.0235440362896706 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785544 -2.2844628207786339 ;
	setAttr ".rpt" -type "double3" 0 -2.1125033591296987 0.70407810048621156 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455575793 -1.5289048420094549 ;
createNode mesh -n "pPlaneShape6" -p "pPlane6";
	rename -uid "C4261AFB-4CA9-A5CC-A973-05B2AD6ECF10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane6";
	rename -uid "A90CCACE-4CCB-9757-2D26-21AFEDF7D93E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane7";
	rename -uid "DF78AB24-4A2E-AF63-E9CE-96B101A8544C";
	setAttr ".t" -type "double3" 0 -64.575982128745366 39.345277298417408 ;
	setAttr ".r" -type "double3" -52.980626216174777 20 0 ;
	setAttr ".s" -type "double3" 1.0619562408045629 1.0619562408045629 3.0235440362896706 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785544 -2.2844628207786339 ;
	setAttr ".rpt" -type "double3" 0 -2.1125033591296987 0.70407810048621156 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455575793 -1.5289048420094549 ;
createNode mesh -n "pPlaneShape7" -p "pPlane7";
	rename -uid "A43E34D7-48D5-FD93-236E-788584B9B2A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane7";
	rename -uid "0002076E-4B02-FB97-3D88-A29F8F4DA5F4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane8";
	rename -uid "E9BAB95C-4C80-8351-2880-73850A9BB695";
	setAttr ".t" -type "double3" 0.83198016019874554 -64.510026652242942 38.756407923008943 ;
	setAttr ".r" -type "double3" -52.980626216174791 39.999999999999993 4.1519192653023774e-015 ;
	setAttr ".s" -type "double3" 1.0619562408045629 1.0619562408045631 3.0235440362896706 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785367 -2.2844628207786322 ;
	setAttr ".rpt" -type "double3" -0.83198016019874466 -1.7835655523836682 1.2929474758946722 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455558203 -1.5289048420094531 ;
createNode mesh -n "pPlaneShape8" -p "pPlane8";
	rename -uid "F55DAB48-4808-DFDC-5E07-DF9AE00D0584";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane8";
	rename -uid "BB7A9B4C-4813-203E-2342-D3BD96513E1F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane10";
	rename -uid "15692515-4F80-2B3C-8173-749FF2622A17";
	setAttr ".t" -type "double3" 1.2746675570109742 -64.510026652242942 37.989650859859772 ;
	setAttr ".r" -type "double3" -52.980626216174961 80 0 ;
	setAttr ".s" -type "double3" 1.0619562408045631 1.0619562408045637 3.0235440362896711 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785374 -2.2844628207786322 ;
	setAttr ".rpt" -type "double3" -1.2746675570109662 -1.7835655523836713 2.0597045390438526 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455558827 -1.5289048420094533 ;
createNode mesh -n "pPlaneShape10" -p "pPlane10";
	rename -uid "70C62D31-4C3C-E2D2-83FC-EA908DE5006C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane10";
	rename -uid "8684F562-497E-A044-E946-1C863C289F2E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane11";
	rename -uid "3C3D15E1-40EE-4CF7-9246-68B6D70F1E29";
	setAttr ".t" -type "double3" 1.2746675570109756 -64.904919935491421 37.540134296390207 ;
	setAttr ".r" -type "double3" 127.01937378382557 80 -179.99999999999969 ;
	setAttr ".s" -type "double3" 1.0619562408045633 1.061956240804564 3.0235440362896719 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785376 -2.2844628207786326 ;
	setAttr ".rpt" -type "double3" -1.2746675570109711 -1.7835655523836684 2.5092211025134143 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455558992 -1.5289048420094538 ;
createNode mesh -n "pPlaneShape11" -p "pPlane11";
	rename -uid "2CCBB6DB-4CD1-066E-B42D-F88FCCB2252B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane11";
	rename -uid "E4A8C139-4069-C097-4F26-12A1B6581077";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane12";
	rename -uid "EE1FC38E-4393-3857-B929-8A8DD5390950";
	setAttr ".t" -type "double3" 1.1209238375458579 -64.510026652242956 37.117726898776816 ;
	setAttr ".r" -type "double3" 127.01937378382539 59.999999999999986 -179.99999999999991 ;
	setAttr ".s" -type "double3" 1.0619562408045635 1.0619562408045642 3.0235440362896724 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785378 -2.2844628207786339 ;
	setAttr ".rpt" -type "double3" -1.1209238375458521 -1.7835655523836671 2.9316285001268034 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455559296 -1.5289048420094544 ;
createNode mesh -n "pPlaneShape12" -p "pPlane12";
	rename -uid "6152154D-4737-7470-7D8B-7A94CF9B89CD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane12";
	rename -uid "E1BBAAAF-4218-75F4-08ED-6A8EABF537D6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane13";
	rename -uid "6CC36538-4E03-8ECB-B85A-5A935E41F053";
	setAttr ".t" -type "double3" 0.83198016019875409 -64.904919935491421 36.773377233241021 ;
	setAttr ".r" -type "double3" 127.01937378382533 39.999999999999964 -180 ;
	setAttr ".s" -type "double3" 1.0619562408045637 1.0619562408045644 3.0235440362896724 ;
	setAttr ".rp" -type "double3" 0 -0.1015860149578538 -2.2844628207786339 ;
	setAttr ".rpt" -type "double3" -0.83198016019874643 -1.7835655523836662 3.2759781656626004 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455559461 -1.5289048420094544 ;
createNode mesh -n "pPlaneShape13" -p "pPlane13";
	rename -uid "896C0780-46F4-D503-EA3F-4DB3C21AC713";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane13";
	rename -uid "8B46EC78-474E-1179-5C71-26AA2F51A38C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane14";
	rename -uid "72890F87-4A8F-6BE6-A009-8996A09307B0";
	setAttr ".t" -type "double3" 0.44268739681223668 -77.972671235181593 21.899925505687992 ;
	setAttr ".r" -type "double3" 23.499482838544555 -52.039300162441791 163.34931595790414 ;
	setAttr ".s" -type "double3" 1.061956240804564 1.0619562408045644 3.0235440362896724 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785381 -2.2844628207786339 ;
	setAttr ".rpt" -type "double3" -0.44268739681222724 -1.7835655523836667 3.5007364473973785 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455559461 -1.5289048420094544 ;
createNode mesh -n "pPlaneShape14" -p "pPlane14";
	rename -uid "0DEDF567-428A-10D4-65AD-38BF04DFE9E4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane14";
	rename -uid "A8B1BA74-4C0D-45F5-30FA-F7B4399B3A03";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane15";
	rename -uid "B1FB681C-4695-2CD9-9418-3FB90325FC01";
	setAttr ".t" -type "double3" 1.0658141036401503e-014 -64.904919935491421 36.470561219428653 ;
	setAttr ".r" -type "double3" 127.01937378382529 0 179.99999999999994 ;
	setAttr ".s" -type "double3" 1.061956240804564 1.0619562408045646 3.0235440362896728 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785384 -2.2844628207786344 ;
	setAttr ".rpt" -type "double3" -1.9052154232618498e-015 -1.7835655523836698 3.5787941794749702 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455559617 -1.5289048420094546 ;
createNode mesh -n "pPlaneShape15" -p "pPlane15";
	rename -uid "D27FFAC8-4798-5904-B60B-0BA38D89646A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane15";
	rename -uid "9384B18A-4399-6372-4B25-98A2F5C90779";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane16";
	rename -uid "059DA87D-4336-ABAD-E107-189583F68073";
	setAttr ".t" -type "double3" -0.44268739681221619 -64.510026652242956 36.548618951506242 ;
	setAttr ".r" -type "double3" 127.01937378382529 -20.000000000000004 179.99999999999991 ;
	setAttr ".s" -type "double3" 1.061956240804564 1.0619562408045651 3.0235440362896737 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785388 -2.2844628207786348 ;
	setAttr ".rpt" -type "double3" 0.44268739681222502 -1.78356555238367 3.5007364473973803 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455560085 -1.5289048420094553 ;
createNode mesh -n "pPlaneShape16" -p "pPlane16";
	rename -uid "80681EA7-4570-663E-A969-398DB15586D8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane16";
	rename -uid "7640481C-41E3-CA88-EC97-97B0957F8F4D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane17";
	rename -uid "5E96CD50-42F4-6164-1983-7AA7D83C9E1E";
	setAttr ".t" -type "double3" -0.83198016019873577 -64.904919935491421 36.773377233241021 ;
	setAttr ".r" -type "double3" 127.01937378382529 -40 179.99999999999989 ;
	setAttr ".s" -type "double3" 1.0619562408045644 1.0619562408045655 3.0235440362896746 ;
	setAttr ".rp" -type "double3" 0 -0.1015860149578539 -2.2844628207786357 ;
	setAttr ".rpt" -type "double3" 0.83198016019874366 -1.7835655523836706 3.275978165662599 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455560406 -1.5289048420094558 ;
createNode mesh -n "pPlaneShape17" -p "pPlane17";
	rename -uid "752AB6BB-4946-A6C8-E324-F2BD00D5D65B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane17";
	rename -uid "4B63A958-495D-1DC8-4132-2DBCC24FF00E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane18";
	rename -uid "728377D3-4997-1E53-7473-269E42699776";
	setAttr ".t" -type "double3" -1.120923837545843 -64.510026652242971 37.117726898776816 ;
	setAttr ".r" -type "double3" 127.01937378382532 -59.999999999999993 179.99999999999983 ;
	setAttr ".s" -type "double3" 1.0619562408045646 1.0619562408045657 3.0235440362896755 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785392 -2.2844628207786366 ;
	setAttr ".rpt" -type "double3" 1.1209238375458486 -1.7835655523836726 2.9316285001268048 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.005926692045556071 -1.5289048420094566 ;
createNode mesh -n "pPlaneShape18" -p "pPlane18";
	rename -uid "7948674B-46A3-A950-32CD-19A5056B2220";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane18";
	rename -uid "31BC6671-43E5-799E-DE28-769404FDE835";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane19";
	rename -uid "BD678588-4E34-3FAF-67A5-91AE4BED17B3";
	setAttr ".t" -type "double3" -1.2746675570109656 -64.904919935491435 37.5401342963902 ;
	setAttr ".r" -type "double3" 127.01937378382522 -80.000000000000014 179.99999999999969 ;
	setAttr ".s" -type "double3" 1.0619562408045649 1.061956240804566 3.0235440362896759 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785394 -2.2844628207786366 ;
	setAttr ".rpt" -type "double3" 1.2746675570109647 -1.7835655523836764 2.5092211025134152 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455561022 -1.5289048420094571 ;
createNode mesh -n "pPlaneShape19" -p "pPlane19";
	rename -uid "C39024B6-420D-4044-8785-BBBC4D68FC3A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane19";
	rename -uid "74BB1EB4-4D53-FAE2-DB16-F287E701EB51";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane20";
	rename -uid "6A71F045-4D22-CEEE-0323-1790C9FACA47";
	setAttr ".t" -type "double3" -1.2746675570109656 -64.510026652242971 37.989650859859758 ;
	setAttr ".r" -type "double3" -52.98062621617462 -79.999999999999986 1.8316084419636363e-014 ;
	setAttr ".s" -type "double3" 1.0619562408045649 1.061956240804566 3.0235440362896755 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785392 -2.2844628207786366 ;
	setAttr ".rpt" -type "double3" 1.2746675570109796 -1.7835655523836667 2.0597045390438558 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455561022 -1.5289048420094566 ;
createNode mesh -n "pPlaneShape20" -p "pPlane20";
	rename -uid "B719D319-45D2-F879-1E83-88BB620461F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane20";
	rename -uid "2519771B-4BC9-C28D-C4D2-7194E39DED70";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane21";
	rename -uid "399B373B-4DB1-6240-0D37-05BA55B4AF2A";
	setAttr ".t" -type "double3" 1.7830736950451289 -76.212445613809123 -0.094357070779814478 ;
	setAttr ".r" -type "double3" 58.921693782907418 -7.8371614221289017 -144.36278595277238 ;
	setAttr ".s" -type "double3" 1.0619562408045649 1.061956240804566 3.0235440362896751 ;
	setAttr ".rp" -type "double3" 1.2659504900033055e-007 -2.8992869015283174 0.24158431177705086 ;
	setAttr ".rpt" -type "double3" -2.13071496860141 1.3465582795561459 0.98858462200102504 ;
	setAttr ".sp" -type "double3" 1.1920928955078125e-007 -2.730137825012207 0.079901039600372314 ;
	setAttr ".spt" -type "double3" 7.3857594495493009e-009 -0.16914907651611033 0.16168327217667855 ;
createNode mesh -n "pPlaneShape21" -p "pPlane21";
	rename -uid "4574B9F5-4336-A7B0-9BC6-CF8FE1B4EAEA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane21";
	rename -uid "B5079980-452E-CAD1-F242-3781C9FACBF9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane22";
	rename -uid "2A4F88FF-4CB3-2435-2F8D-059B3CDDC1F3";
	setAttr ".t" -type "double3" -0.83198016019874055 -64.510026652242971 38.756407923008943 ;
	setAttr ".r" -type "double3" -52.980626216174706 -39.999999999999957 0 ;
	setAttr ".s" -type "double3" 1.0619562408045651 1.0619562408045662 3.0235440362896759 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785395 -2.2844628207786375 ;
	setAttr ".rpt" -type "double3" 0.83198016019874788 -1.78356555238367 1.2929474758946724 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455561178 -1.5289048420094571 ;
createNode mesh -n "pPlaneShape22" -p "pPlane22";
	rename -uid "5A22CD05-4F7B-F999-7D90-B9AB33C763E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane22";
	rename -uid "ACACE233-4F01-D61A-3FCF-0C9D987F610E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane23";
	rename -uid "C550E0F6-424F-5681-7E2F-C29E9F3BDE41";
	setAttr ".t" -type "double3" -0.4426873968122218 -64.904919935491435 38.981166204743722 ;
	setAttr ".r" -type "double3" -52.980626216174741 -19.999999999999943 0 ;
	setAttr ".s" -type "double3" 1.0619562408045651 1.0619562408045664 3.0235440362896764 ;
	setAttr ".rp" -type "double3" 0 -0.10158601495785398 -2.2844628207786379 ;
	setAttr ".rpt" -type "double3" 0.4426873968122268 -1.7835655523836711 1.0681891941598931 ;
	setAttr ".sp" -type "double3" 0 -0.095659322912298381 -0.75555797876918063 ;
	setAttr ".spt" -type "double3" 0 -0.0059266920455561334 -1.5289048420094573 ;
createNode mesh -n "pPlaneShape23" -p "pPlane23";
	rename -uid "07E1E1BD-49BD-17BB-5E5C-DCB687472B52";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane23";
	rename -uid "28384544-4B90-FC92-542A-F0A434D8833F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane24";
	rename -uid "C2289763-4388-4628-A01A-7F858D864C54";
	setAttr ".t" -type "double3" -0.0027425877561202405 -68.167958525409361 24.984494606607569 ;
	setAttr ".r" -type "double3" -70.745765317414254 -6.3644113409435201 262.04013071173051 ;
	setAttr ".s" -type "double3" 0.79220139939932155 0.7922013993993231 2.2555127270399051 ;
	setAttr ".rp" -type "double3" 4.9701798143599956e-008 -2.1379254720972551 -3.9986145153767616 ;
	setAttr ".rpt" -type "double3" 0.10140151775729776 4.891554514033964 1.4753587698531758 ;
	setAttr ".sp" -type "double3" 7.9113328332880428e-008 -2.3331257983800171 -0.69668456544582114 ;
	setAttr ".spt" -type "double3" -2.9411530189280472e-008 0.19520032628276196 -3.3019299499309405 ;
createNode mesh -n "pPlaneShape24" -p "pPlane24";
	rename -uid "D6F938DC-4E7C-80F7-AC06-39AD39ED9E64";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[0]" -type "float3" 2.2538006e-007 0.12251098 0.057061836 ;
	setAttr ".pt[1]" -type "float3" 2.2538006e-007 0.12251098 0.057061836 ;
	setAttr ".pt[2]" -type "float3" 2.2538006e-007 0.34309304 0.15980211 ;
	setAttr ".pt[3]" -type "float3" 2.2538006e-007 0.22058222 0.10274008 ;
	setAttr ".pt[5]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[6]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[8]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[9]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane24";
	rename -uid "ADFFB947-44D5-436C-32A6-838F5093B7A6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane25";
	rename -uid "3B03C6BF-472D-74C8-B535-01982D45C2C3";
	setAttr ".t" -type "double3" 1.0658141036401503e-014 -69.002548454392439 17.516397914074439 ;
	setAttr ".r" -type "double3" -80.24740227674404 3.8106402409080138 49.81527848936171 ;
	setAttr ".s" -type "double3" 1.1135484591705351 1.1135484591705374 3.1704345937020952 ;
	setAttr ".rp" -type "double3" 4.9701798143599956e-008 -2.1379254720972551 -3.9986145153767616 ;
	setAttr ".rpt" -type "double3" 0.10140151775729776 4.891554514033964 1.4753587698531758 ;
	setAttr ".sp" -type "double3" 7.9113328332880428e-008 -2.3331257983800171 -0.69668456544582114 ;
	setAttr ".spt" -type "double3" -2.9411530189280472e-008 0.19520032628276196 -3.3019299499309405 ;
createNode mesh -n "pPlaneShape25" -p "pPlane25";
	rename -uid "167725F0-4A21-99BD-BFD6-DAB09432EF66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[0]" -type "float3" 2.2538006e-007 0.12251098 0.057061836 ;
	setAttr ".pt[1]" -type "float3" 2.2538006e-007 0.12251098 0.057061836 ;
	setAttr ".pt[2]" -type "float3" 2.2538006e-007 0.34309304 0.15980211 ;
	setAttr ".pt[3]" -type "float3" 2.2538006e-007 0.22058222 0.10274008 ;
	setAttr ".pt[5]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[6]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[8]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr ".pt[9]" -type "float3" 2.2538006e-007 7.4505806e-009 2.3283064e-009 ;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane25";
	rename -uid "4914BA28-4411-4816-0F55-50BB1B3BE02E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane26";
	rename -uid "4E34E10F-4969-7717-26A7-81B47F515E56";
	setAttr ".t" -type "double3" -0.89264868105088446 -76.212445613809123 0.26904382560024631 ;
	setAttr ".r" -type "double3" 47.733425545740452 -10.411331918681739 -215.15029784095054 ;
	setAttr ".s" -type "double3" 1.0619562408045649 1.061956240804566 3.0235440362896751 ;
	setAttr ".rp" -type "double3" 1.2659504900033055e-007 -2.8992869015283174 0.24158431177705086 ;
	setAttr ".rpt" -type "double3" -2.13071496860141 1.3465582795561459 0.98858462200102504 ;
	setAttr ".sp" -type "double3" 1.1920928955078125e-007 -2.730137825012207 0.079901039600372314 ;
	setAttr ".spt" -type "double3" 7.3857594495493009e-009 -0.16914907651611033 0.16168327217667855 ;
createNode mesh -n "pPlaneShape26" -p "pPlane26";
	rename -uid "894C2E49-4EC3-2D0A-F226-A2BB539D28CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane26";
	rename -uid "6DA98BDD-4EEA-9B84-8117-86AD1391858C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane27";
	rename -uid "52A24E64-4C8D-D819-B59A-87947B81AA03";
	setAttr ".t" -type "double3" 4.5423508875406977 -74.017768368631309 0.26904382560024631 ;
	setAttr ".r" -type "double3" 71.32287389585214 -10.411331918681741 -215.15029784094932 ;
	setAttr ".s" -type "double3" 1.0619562408045649 1.061956240804566 3.0235440362896751 ;
	setAttr ".rp" -type "double3" 1.2659504900033055e-007 -2.8992869015283174 0.24158431177705086 ;
	setAttr ".rpt" -type "double3" -2.13071496860141 1.3465582795561459 0.98858462200102504 ;
	setAttr ".sp" -type "double3" 1.1920928955078125e-007 -2.730137825012207 0.079901039600372314 ;
	setAttr ".spt" -type "double3" 7.3857594495493009e-009 -0.16914907651611033 0.16168327217667855 ;
createNode mesh -n "pPlaneShape27" -p "pPlane27";
	rename -uid "9FB2D627-4E58-C81E-AA2A-619B39862B2E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1 0 0.5 0.5 0.5 0.5 1 0.5 0 1 0.5 0.5 1 0 0.5 0.5 0 1 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 0.12251113 0.057061877 
		0 0.12251113 0.057061877 0 0.34309289 0.15980205 0 0.22058177 0.10274011;
	setAttr -s 10 ".vt[0:9]"  -1.23598707 -5.035713196 9.5367432e-007 1.23598707 -5.035713196 9.5367432e-007
		 0 -5.035713196 1.23598766 0 -4.17469788 -1.9073486e-006 0 -0.86102295 -1.23598766
		 -1.48082399 -4.68424988 0.068631172 9.5914771e-008 -3.76216125 0.088582993 0 -0.54707336 -1.13215256
		 2.1047705e-007 -4.64673615 1.26941586 1.48082423 -4.68424988 0.068631172;
	setAttr -s 20 ".ed[0:19]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1 0 5 0 5 6 1 4 7 0 6 7 1 5 7 0 2 8 0 5 8 0 8 6 1 1 9 0 8 9 0 6 9 1 7 9 0;
	setAttr -s 12 -ch 40 ".fc[0:11]" -type "polyFaces" 
		f 3 9 11 -13
		mu 0 3 11 3 10
		f 3 14 15 -10
		mu 0 3 11 12 3
		f 3 17 -19 -16
		mu 0 3 12 13 3
		f 3 -12 18 -20
		mu 0 3 10 3 13
		f 3 2 -8 -4
		mu 0 3 5 7 6
		f 3 3 -6 -1
		mu 0 3 5 6 8
		f 3 5 4 -2
		mu 0 3 8 6 9
		f 3 6 -5 7
		mu 0 3 7 9 6
		f 4 -3 8 12 -11
		mu 0 4 4 0 11 10
		f 4 0 13 -15 -9
		mu 0 4 0 2 12 11
		f 4 1 16 -18 -14
		mu 0 4 2 1 13 12
		f 4 -7 10 19 -17
		mu 0 4 1 4 10 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pPlane27";
	rename -uid "4A44AA8A-407A-67F2-CBE9-558CBD035F46";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0 0.5 1 0.5 0.5 0
		 0.5 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[0:4]" -type "float3"  -0.48037246 -4.1746907 1.1292964e-015 
		0.48037246 -4.1746907 1.1292964e-015 0 -4.1746907 0.48037246 0 -4.1746907 0 0 0 -0.48037246;
	setAttr -s 5 ".vt[0:4]"  -0.75561452 -0.8610236 0 0.75561452 -0.8610236 0
		 0 -0.8610236 0.75561452 0 0 0 0 -0.8610236 -0.75561452;
	setAttr -s 8 ".ed[0:7]"  0 2 0 2 1 0 0 4 0 0 3 1 3 1 1 2 3 1 4 1 0
		 3 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 3 7 -3
		mu 0 3 0 3 4
		f 3 0 5 -4
		mu 0 3 0 2 3
		f 3 1 -5 -6
		mu 0 3 2 1 3
		f 3 -8 4 -7
		mu 0 3 4 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane28";
	rename -uid "76D62EAE-4BEF-3490-39E8-D088B0FA2295";
	setAttr ".t" -type "double3" 0 -0.97016139126386536 0.92264100186539499 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 35.16918398241242 33.143823592928122 35.16918398241242 ;
createNode mesh -n "pPlaneShape28" -p "pPlane28";
	rename -uid "5CD4E2A0-4982-A5B6-5007-AA867EDB12CE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DFE60449-4987-69A8-237E-258C4103C097";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "64B2627C-46D5-43D4-0AB4-A3872C00BB9A";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "FA259E3E-476E-1F9F-00A6-499E580B273A";
createNode displayLayerManager -n "layerManager";
	rename -uid "88A2FB8D-4DDC-6F8B-880D-9EAF2A455245";
	setAttr ".cdl" 1;
	setAttr -s 4 ".dli[1:3]"  1 2 3;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "190D05FE-4631-4243-41FD-269871CB2D2B";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "859B6868-42FE-B1CE-66B4-48A3E88FB23F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "AAFF5C3B-4AC5-520F-07AF-67B1E21EC9CD";
	setAttr ".g" yes;
createNode objectSet -n "set1";
	rename -uid "96D100F5-40A9-3D83-6B64-0AA9FBA545AA";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr -s 7 ".gn";
createNode polyPlane -n "polyPlane2";
	rename -uid "71169814-48F3-7DC2-0310-CEA90C38ABF3";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode polyNormal -n "polyNormal1";
	rename -uid "601A7C12-454F-67B0-99B4-29B46281ACB9";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".unm" no;
createNode polyTweak -n "polyTweak1";
	rename -uid "CE05D030-499C-0973-89F9-468A537A0AD7";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -7.8333354 0 99.5 7.8333354
		 0 99.5 -7.8333354 0 0.5 7.8333354 0 0.5;
createNode polySplit -n "polySplit1";
	rename -uid "7BC70EF1-4329-8242-E063-FD80530763EB";
	setAttr -s 2 ".e[0:1]"  0.38555399 0.38555399;
	setAttr -s 2 ".d[0:1]"  -2147483647 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "0CAB359F-4E1E-A298-1687-749271372655";
	setAttr -s 2 ".e[0:1]"  0.330883 0.330883;
	setAttr -s 2 ".d[0:1]"  -2147483644 -2147483643;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "4C30E384-49DF-F710-2247-559A0B68C7B7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[4:5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -2.8402074e-018 51.279118 ;
	setAttr ".rs" 35393;
	setAttr ".lt" -type "double3" 7.6206674267988498e-014 40.795650163542227 -9.0584540232235164e-015 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.3333358764648437 -2.5412123309429433e-017 41.113628387451172 ;
	setAttr ".cbx" -type "double3" 8.3333358764648437 1.9731708606905281e-017 61.444602966308594 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "CAB942B4-4DC8-CB04-D8C8-29B20804AFF8";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk";
	setAttr ".tk[10]" -type "float3" -2.771616e-006 0 1.9073486e-005 ;
	setAttr ".tk[11]" -type "float3" -2.771616e-006 0 1.9073486e-005 ;
createNode polySplit -n "polySplit3";
	rename -uid "AFF64698-4C1A-947B-F885-A6BEC2A9E0A7";
	setAttr -s 2 ".e[0:1]"  0.285092 0.285092;
	setAttr -s 2 ".d[0:1]"  -2147483641 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "A88A2785-4408-2618-3B10-499FF2E0D3EC";
	setAttr -s 2 ".e[0:1]"  0.754098 0.754098;
	setAttr -s 2 ".d[0:1]"  -2147483647 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "72BCA2D5-4A80-F8B0-8AE5-2D845015ED66";
	setAttr -s 2 ".e[0:1]"  0.199716 0.199716;
	setAttr -s 2 ".d[0:1]"  -2147483635 -2147483634;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "A7AE70F8-4646-66D3-7370-7595E3E53357";
	setAttr -s 2 ".e[0:1]"  0.199415 0.199415;
	setAttr -s 2 ".d[0:1]"  -2147483638 -2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "B9210CE5-428C-CCB8-217C-73AB49127A4F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[8]" -type "float3" 17.609053 0 -44.633873 ;
	setAttr ".tk[9]" -type "float3" 33.679432 0 -25.37394 ;
	setAttr ".tk[18]" -type "float3" 0 5.5722575e-018 0.44809246 ;
	setAttr ".tk[19]" -type "float3" 0 -5.5722575e-018 7.3850155 ;
createNode polySplit -n "polySplit7";
	rename -uid "6B6151D3-4B7B-6306-07D1-FEBA5B73264D";
	setAttr -s 2 ".e[0:1]"  0.580136 0.580136;
	setAttr -s 2 ".d[0:1]"  -2147483623 -2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "06398558-4250-C0FE-6374-16B43A97F0EE";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[19:21]" -type "float3"  3.85984087 0 -1.86766505 -2.36570883
		 0 9.2138176 -0.49804401 0 6.22554779;
createNode polySplit -n "polySplit8";
	rename -uid "899E519A-4881-750F-EB4C-FF9A47292C3E";
	setAttr -s 6 ".e[0:5]"  0.54209298 0.54209298 0.54209298 0.54209298
		 0.54209298 0.54209298;
	setAttr -s 6 ".d[0:5]"  -2147483648 -2147483627 -2147483642 -2147483639 -2147483630 -2147483645;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "AB2FD966-46DF-2040-F2ED-A393274D0627";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[0]" -type "float3" -1.4814818 0 1.7911496 ;
	setAttr ".tk[1]" -type "float3" 1.4814818 0 1.7911496 ;
	setAttr ".tk[2]" -type "float3" -2.0370374 0 0 ;
	setAttr ".tk[3]" -type "float3" 2.0370374 0 0 ;
	setAttr ".tk[12]" -type "float3" -3.8888876 0 0 ;
	setAttr ".tk[13]" -type "float3" 3.8888876 0 0 ;
	setAttr ".tk[14]" -type "float3" 0.88761657 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.88761657 0 0 ;
	setAttr ".tk[22]" -type "float3" -0.82788229 0 13.931162 ;
	setAttr ".tk[23]" -type "float3" -0.90260714 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.82788229 0 0 ;
	setAttr ".tk[25]" -type "float3" -0.82788229 0 0 ;
	setAttr ".tk[26]" -type "float3" -0.50049245 0 0 ;
	setAttr ".tk[27]" -type "float3" -0.82788229 0 19.866127 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "368B2B27-48E3-1212-0D93-9DA217843A8B";
	setAttr ".dc" -type "componentList" 2 "f[0:2]" "f[4:7]";
createNode groupId -n "groupId1";
	rename -uid "7AA11C1F-466A-4F74-8244-BB8CA1FCF07D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "00E4B157-4E38-2F29-8734-B2AAF1223E9D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[2]" "e[5]" "e[8]" "e[13:15]" "e[17]" "e[20]" "e[22:24]" "e[31:41]";
createNode polyMirror -n "polyMirror1";
	rename -uid "2B4CEAF7-4392-F76D-83D7-F4AA6E7367D5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".ad" 0;
	setAttr ".mm" 0;
	setAttr ".cm" yes;
	setAttr ".fnf" 8;
	setAttr ".lnf" 15;
createNode polySeparate -n "polySeparate1";
	rename -uid "70ED6848-4E81-DA28-3F89-E5AD2CB4DC77";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
	setAttr ".uss" yes;
	setAttr ".inp" yes;
createNode groupId -n "groupId2";
	rename -uid "9D148511-4B81-796B-9496-8E9F5A1C28D7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "12DC1A95-4761-7DF2-FF46-D3B827DCF2E3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode groupId -n "groupId3";
	rename -uid "655C6CF8-41EA-94E7-4BE9-30A93CD11D6A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "0D33EFBF-4320-9141-9D09-E9A780B25D7D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "BBE0EF2E-479A-4F85-A08C-478AAC5EA98B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[20:24]" "e[39]" "e[41]" "e[44]" "e[46]" "e[52]";
createNode groupId -n "groupId5";
	rename -uid "DFED79B8-4D48-F755-D5E0-F3A5651BD562";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "929AE82B-403A-6F09-7A10-C48896046B67";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode groupId -n "groupId6";
	rename -uid "EABAE918-44CB-3FBB-8CD7-5EBD554CBF5C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "BB205F15-4A3E-381C-E2F6-858CDC04DB67";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[20:24]" "e[39]" "e[41]" "e[44]" "e[46]" "e[52]";
createNode groupId -n "groupId7";
	rename -uid "BEDDEC88-4B06-B73A-4835-C5913F657807";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "ED3D27DB-43E2-F60F-5046-D994E805B194";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode polyUnite -n "polyUnite1";
	rename -uid "FD21EE46-41EA-C3E2-5D99-469E9C6B3DDC";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId8";
	rename -uid "622A29E6-43FC-B735-3F8C-949F1495AD7E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "D00BA73F-4B56-E08F-83D1-0A96434AA662";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[20:24]" "e[47:51]";
createNode groupId -n "groupId9";
	rename -uid "4E9D8612-45EC-226E-A8F8-00BB44444614";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "9B10B85B-4B49-F4AB-99FC-A99CB9B4D586";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "0556A063-44F6-2C2C-9839-5D9173DBF7B4";
	setAttr ".ics" -type "componentList" 2 "vtx[12]" "vtx[32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak6";
	rename -uid "E1A17F1E-43E3-CEB1-C33D-928EE558BE53";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[12]" -type "float3" 0.12633252 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.12633252 0 0 ;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "850DD9BD-4CD2-69AF-E5A4-42A59AE41471";
	setAttr ".ics" -type "componentList" 2 "vtx[13]" "vtx[32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak7";
	rename -uid "B39F44AE-4758-2CEA-F743-20843DE6FD56";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[13]" -type "float3" 0.20105737 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.20105737 0 0 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "FA8A8A4B-4D07-AEFC-6709-96A4C5E685BB";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak8";
	rename -uid "3D658DAD-4575-4080-5F03-659A94FB0466";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[14]" -type "float3" 0.12633252 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.12633252 0 0 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "33277040-4D26-FDD6-4E21-5D97F83E58A8";
	setAttr ".ics" -type "componentList" 4 "vtx[15]" "vtx[18]" "vtx[32]" "vtx[35]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak9";
	rename -uid "A1946E94-4984-4133-0260-0EAA0251F980";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[15]" -type "float3" 0.12633252 5.021471e-018 -2.2614708 ;
	setAttr ".tk[18]" -type "float3" -7.4505806e-009 -5.021471e-018 2.261467 ;
	setAttr ".tk[32]" -type "float3" -0.12633252 5.021471e-018 -2.2614708 ;
	setAttr ".tk[35]" -type "float3" 7.4505806e-009 -5.021471e-018 2.261467 ;
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "9855DFB6-4883-EE81-8053-548788BA8FB7";
	setAttr ".ics" -type "componentList" 2 "vtx[17]" "vtx[32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "B4451BB4-4DBC-7BEE-CCFB-86A11BCB4177";
	setAttr ".ics" -type "componentList" 2 "vtx[16]" "vtx[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak10";
	rename -uid "D05B3052-402E-F27A-A920-2283B4EE1513";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[16]" -type "float3" 0.12633252 0 0 ;
	setAttr ".tk[17]" -type "float3" -2.9802322e-008 0 0 ;
	setAttr ".tk[31]" -type "float3" -0.12633252 0 0 ;
	setAttr ".tk[32]" -type "float3" 2.9802322e-008 0 0 ;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "8AA4902A-4311-D448-415F-B0BF111B875F";
	setAttr ".ics" -type "componentList" 2 "vtx[18]" "vtx[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "2ED4B33B-4D83-822D-97A8-76A808DD8924";
	setAttr ".dc" -type "componentList" 2 "e[23]" "e[25]";
createNode polySplit -n "polySplit9";
	rename -uid "7D077D7F-4242-87C6-C023-EE935B8BF136";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483637 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "AA5513D7-457B-11F5-F4A1-7E9764587B1C";
	setAttr -s 3 ".e[0:2]"  0.59546697 0.59546697 0.59546697;
	setAttr -s 3 ".d[0:2]"  -2147483647 -2147483628 -2147483614;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "E3905F4A-4AEE-FA9A-A6F6-FC8B74676279";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[7]" -type "float3" 0.56134748 0 0 ;
	setAttr ".tk[18]" -type "float3" 7.4505806e-009 0 0 ;
	setAttr ".tk[25]" -type "float3" -0.56134748 0 0 ;
	setAttr ".tk[30]" -type "float3" -1.6766378 0 -0.48988083 ;
	setAttr ".tk[31]" -type "float3" -8.936988e-009 0 0.48988083 ;
	setAttr ".tk[32]" -type "float3" 1.6766378 0 -0.48988083 ;
createNode polySplit -n "polySplit11";
	rename -uid "530EF23A-4DA9-DD87-74A5-5CADA30948A0";
	setAttr -s 7 ".e[0:6]"  0.42028499 0.42028499 0.42028499 0.42028499
		 0.42028499 0.57971501 0.42028499;
	setAttr -s 7 ".d[0:6]"  -2147483606 -2147483608 -2147483609 -2147483610 -2147483613 -2147483599 
		-2147483612;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "634AD029-48A0-BA81-DD4E-25827E1A2C72";
	setAttr -s 7 ".e[0:6]"  0.47623 0.47623 0.47623 0.47623 0.47623 0.47623
		 0.47623;
	setAttr -s 7 ".d[0:6]"  -2147483648 -2147483600 -2147483635 -2147483644 -2147483642 -2147483637 
		-2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "CD136257-42C3-0CB6-7888-D1B74FA0436E";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[3]" -type "float3" 0.18518525 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.27160496 0 0 ;
	setAttr ".tk[9]" -type "float3" 0 0 4.2147245 ;
	setAttr ".tk[17]" -type "float3" 6.6227418e-010 0 0 ;
	setAttr ".tk[21]" -type "float3" -0.18518525 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.27160496 0 0 ;
	setAttr ".tk[27]" -type "float3" 0 0 4.2147245 ;
	setAttr ".tk[33]" -type "float3" -0.13359694 0 0 ;
	setAttr ".tk[34]" -type "float3" -0.15745348 0 0 ;
	setAttr ".tk[39]" -type "float3" 3.0084732 0 0 ;
	setAttr ".tk[40]" -type "float3" -3.0084732 0 0 ;
	setAttr ".tk[45]" -type "float3" 0.14225851 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.12070419 0 0 ;
createNode polySplit -n "polySplit13";
	rename -uid "C29BF37C-45F1-E96D-23E8-29AFBABA47C0";
	setAttr -s 11 ".e[0:10]"  0.52855802 0.52855802 0.52855802 0.52855802
		 0.47144201 0.52855802 0.52855802 0.52855802 0.52855802 0.52855802 0.52855802;
	setAttr -s 11 ".d[0:10]"  -2147483616 -2147483619 -2147483622 -2147483624 -2147483589 -2147483626 
		-2147483575 -2147483645 -2147483632 -2147483629 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit14";
	rename -uid "31738EB4-40D1-5FDD-98E2-069F4574BFE8";
	setAttr -s 11 ".e[0:10]"  0.65007299 0.65007299 0.65007299 0.65007299
		 0.65007299 0.65007299 0.34992701 0.65007299 0.65007299 0.65007299 0.65007299;
	setAttr -s 11 ".d[0:10]"  -2147483562 -2147483563 -2147483564 -2147483565 -2147483566 -2147483567 
		-2147483589 -2147483569 -2147483570 -2147483571 -2147483572;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit15";
	rename -uid "84CEFBB8-42F0-3FA9-EC52-0C810B07FD23";
	setAttr -s 11 ".e[0:10]"  0.50808901 0.50808901 0.50808901 0.50808901
		 0.49191099 0.50808901 0.50808901 0.50808901 0.50808901 0.50808901 0.50808901;
	setAttr -s 11 ".d[0:10]"  -2147483616 -2147483619 -2147483622 -2147483624 -2147483568 -2147483626 
		-2147483575 -2147483645 -2147483632 -2147483629 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode objectSet -n "set2";
	rename -uid "318DCD43-4513-C2B7-5679-89AA9D2265CE";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr -s 4 ".gn";
createNode groupId -n "groupId10";
	rename -uid "A0FECFE8-45C1-2FDE-EC16-B58E92C62ED4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "DC5DEE0C-4118-E1D4-7A1F-95B314C9333D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 8 "e[20:44]" "e[46:47]" "e[49:62]" "e[76:81]" "e[87:91]" "e[102:107]" "e[113:123]" "e[129:133]";
createNode polyTweak -n "polyTweak13";
	rename -uid "EC440457-40E5-AE18-94A2-E79D060EC14B";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[4]" -type "float3" 0.11356676 4.4886224e-018 -0.64625037 ;
	setAttr ".tk[5]" -type "float3" -2.1773407 -4.4886224e-018 -0.26873672 ;
	setAttr ".tk[8]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[9]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[10]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[11]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[55]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[56]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[57]" -type "float3" -1.5755932 -2.5657723e-019 -10.409134 ;
	setAttr ".tk[58]" -type "float3" -1.2547735 -3.0077278e-018 -4.7405143 ;
	setAttr ".tk[59]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[60]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[77]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[78]" -type "float3" -1.484328 0 -0.53975564 ;
	setAttr ".tk[79]" -type "float3" -2.9401913 2.0777065e-018 -4.7542167 ;
createNode deleteComponent -n "deleteComponent3";
	rename -uid "CB8DEDFA-46E5-B740-3FC9-95B8A9A3A599";
	setAttr ".dc" -type "componentList" 4 "f[7:14]" "f[17:23]" "f[30:34]" "f[45:54]";
createNode polyMirror -n "polyMirror2";
	rename -uid "D8E9745F-4D13-ED3D-44D8-07B03E432103";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".ad" 0;
	setAttr ".mm" 0;
	setAttr ".cm" yes;
	setAttr ".fnf" 30;
	setAttr ".lnf" 59;
createNode polySeparate -n "polySeparate2";
	rename -uid "63A90A15-4038-DB4D-0FF5-C08E1D20A9E1";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
	setAttr ".uss" yes;
	setAttr ".inp" yes;
createNode groupId -n "groupId11";
	rename -uid "6D9C4740-448C-AD22-D144-E39E96FD2161";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "F2965911-41E7-8F9E-2F2A-F7B89DAD02B5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 11 "e[20:22]" "e[26]" "e[41]" "e[57]" "e[63]" "e[86]" "e[88]" "e[93]" "e[104]" "e[119]" "e[138:139]";
createNode groupId -n "groupId12";
	rename -uid "5A8B0187-45F7-22B8-6046-EC9ECDE15257";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "C16F0C4D-4BE0-1A89-2EEF-A38527DD6C79";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:59]";
createNode groupId -n "groupId13";
	rename -uid "F4571217-4BD9-B025-2BDF-BCAD4177D972";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "EA53BA84-48C5-66E3-6CE0-A28AA4B6AE28";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 13 "e[20:24]" "e[26]" "e[41]" "e[57]" "e[63]" "e[86]" "e[88]" "e[93]" "e[96]" "e[100]" "e[104]" "e[119]" "e[138:139]";
createNode groupId -n "groupId14";
	rename -uid "7CF02D45-4E89-EA1A-4EC9-02AA1B76D66D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "BCFE236D-4480-F51E-01AF-FAAAC034CF79";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 11 "e[20:22]" "e[26]" "e[41]" "e[57]" "e[63]" "e[86]" "e[88]" "e[93]" "e[104]" "e[119]" "e[138:139]";
createNode groupId -n "groupId15";
	rename -uid "2E73255B-4727-3EB1-4211-49A98FFB60A4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "B9D797D6-4752-472D-DAB0-1FB84A6D879D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:59]";
createNode groupId -n "groupId16";
	rename -uid "18BBDA24-4857-EF64-2C6D-57951ECF6D0D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "FD1494A1-4160-BAE6-A4CB-C4A5CCEC2A1B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 13 "e[20:24]" "e[26]" "e[41]" "e[57]" "e[63]" "e[86]" "e[88]" "e[93]" "e[96]" "e[100]" "e[104]" "e[119]" "e[138:139]";
createNode polyUnite -n "polyUnite2";
	rename -uid "E79BBABF-495E-26E9-700A-99914E1802D1";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId17";
	rename -uid "E76C186D-4296-9B92-6A56-05B346A7B063";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "4C9974C8-457D-E38D-28D0-38B1615589AB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 10 "e[20:22]" "e[26]" "e[41]" "e[57]" "e[63]" "e[94:96]" "e[100]" "e[115]" "e[131]" "e[137]";
createNode groupId -n "groupId18";
	rename -uid "76CBDA08-4923-76C5-7703-EC80DCC161A6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "44CE4A22-437E-3E33-8FD9-BA9E0AF052B5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:59]";
createNode groupId -n "groupId19";
	rename -uid "77546520-407D-9233-B5BC-19AB5545A7B2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "9B588D1E-4A4C-D601-81DC-C3A861A19A4F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 10 "e[20:24]" "e[26]" "e[41]" "e[57]" "e[63]" "e[94:98]" "e[100]" "e[115]" "e[131]" "e[137]";
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "5A379625-46B9-101A-CFAF-EC8759493ED4";
	setAttr ".ics" -type "componentList" 2 "vtx[12]" "vtx[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "EE602571-4174-4AE8-4B5A-7FB627029868";
	setAttr ".ics" -type "componentList" 2 "vtx[19]" "vtx[63]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "8479880C-4E94-6D39-9C60-11994CD8A56B";
	setAttr ".ics" -type "componentList" 2 "vtx[13]" "vtx[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "3081D22A-41D9-580C-C736-5D88D32D3A2C";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "115555B7-45FD-6E78-229F-9EBACF3EB431";
	setAttr ".ics" -type "componentList" 2 "vtx[39]" "vtx[80]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "8DF6BD92-4D0C-7A91-80EF-60B9B50FBBCD";
	setAttr ".ics" -type "componentList" 2 "vtx[27]" "vtx[68]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "F49C1F45-4D31-1810-CE93-85948E98C9E8";
	setAttr ".ics" -type "componentList" 2 "vtx[38]" "vtx[78]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "23D31811-4796-90C6-E1EB-0097177FB544";
	setAttr ".ics" -type "componentList" 2 "vtx[15]" "vtx[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "8932EB54-4376-97D2-9F3D-72A5F1AA90EC";
	setAttr ".ics" -type "componentList" 2 "vtx[17]" "vtx[58]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "43001828-4899-4B2A-AE64-C5AA535C82B6";
	setAttr ".ics" -type "componentList" 2 "vtx[16]" "vtx[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "CD33760D-4299-8747-ABAA-67B85318CA88";
	setAttr ".ics" -type "componentList" 1 "f[0:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 4.4813379e-015 56.96558 ;
	setAttr ".rs" 61213;
	setAttr ".lt" -type "double3" 1.7763568394002505e-015 3.7129315070804777e-015 15.278443048260188 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -31.406370162963867 -1.1102230246251565e-016 -1.7763568394002505e-015 ;
	setAttr ".cbx" -type "double3" 31.406370162963867 9.0736981459265537e-015 113.93115997314453 ;
createNode polyTweak -n "polyTweak14";
	rename -uid "EA654296-4456-7E1C-2D70-538C24E24A1F";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk";
	setAttr ".tk[7]" -type "float3" 1.5298605 0 0 ;
	setAttr ".tk[12]" -type "float3" 4.4408921e-016 0 0 ;
	setAttr ".tk[13]" -type "float3" -1.0434799e-016 0 0 ;
	setAttr ".tk[14]" -type "float3" 4.0638457e-017 0 0 ;
	setAttr ".tk[15]" -type "float3" -3.0388186e-016 0 0 ;
	setAttr ".tk[16]" -type "float3" 1.8112598e-016 0 0 ;
	setAttr ".tk[17]" -type "float3" 2.9140049e-008 0 0 ;
	setAttr ".tk[19]" -type "float3" 8.936988e-009 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.80129504 0 0 ;
	setAttr ".tk[27]" -type "float3" 1.110223e-016 0 0 ;
	setAttr ".tk[38]" -type "float3" 1.110223e-016 0 0 ;
	setAttr ".tk[39]" -type "float3" 9.606429e-017 0 0 ;
	setAttr ".tk[52]" -type "float3" -1.5298605 0 0 ;
	setAttr ".tk[57]" -type "float3" -2.6296323e-016 0 0 ;
	setAttr ".tk[58]" -type "float3" -2.9140049e-008 0 0 ;
	setAttr ".tk[60]" -type "float3" -0.80129504 0 0 ;
	setAttr ".tk[63]" -type "float3" -8.936988e-009 0 0 ;
	setAttr ".tk[68]" -type "float3" -1.110223e-016 0 0 ;
	setAttr ".tk[78]" -type "float3" -1.110223e-016 0 0 ;
	setAttr ".tk[80]" -type "float3" -9.606429e-017 0 0 ;
createNode polySpinEdge -n "polySpinEdge1";
	rename -uid "C5467453-4741-C098-48C2-98852540F61D";
	setAttr ".ics" -type "componentList" 1 "e[260]";
	setAttr ".off" 2;
createNode polySpinEdge -n "polySpinEdge2";
	rename -uid "22383DC0-491E-4596-507B-0DA7259CACBC";
	setAttr ".ics" -type "componentList" 1 "e[172]";
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "D07ED779-4879-5209-E837-81B2030E0473";
	setAttr ".ics" -type "componentList" 2 "e[171]" "e[258]";
	setAttr ".cv" yes;
createNode polySpinEdge -n "polySpinEdge3";
	rename -uid "AD389DD3-4A23-B520-7536-ED8D8F2945BC";
	setAttr ".ics" -type "componentList" 1 "e[40]";
	setAttr ".off" 2;
createNode polySpinEdge -n "polySpinEdge4";
	rename -uid "8CBFED7E-4FBE-DE26-2BEA-D188B91154F9";
	setAttr ".ics" -type "componentList" 1 "e[96]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "E30904D9-4947-C9B3-B152-7FBBE857F34F";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[75]" -type "float2" -0.25816685 0 ;
	setAttr ".uvtk[84]" -type "float2" -0.0011128362 -0.00040783026 ;
	setAttr ".uvtk[175]" -type "float2" -0.26013497 -0.00072344515 ;
	setAttr ".uvtk[184]" -type "float2" -0.0019740046 -0.00072344515 ;
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "A705304C-422D-A433-575F-F2A277B71FE2";
	setAttr ".ics" -type "componentList" 2 "vtx[46]" "vtx[64]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak15";
	rename -uid "740024ED-499A-17A0-8190-FD992321954E";
	setAttr ".uopa" yes;
	setAttr -s 45 ".tk";
	setAttr ".tk[12]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[13]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[14]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[15]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[16]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[17]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[19]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[27]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[38]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[39]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[46]" -type "float3" 0 -1.110223e-016 -2.2364317e-016 ;
	setAttr ".tk[64]" -type "float3" 5.0593872 -1.110223e-016 -9.4608459 ;
	setAttr ".tk[90]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[91]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[92]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[93]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[97]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[98]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[100]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[101]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[104]" -type "float3" 0 1.8626451e-009 0 ;
	setAttr ".tk[113]" -type "float3" 0 1.8626451e-009 0 ;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "3BA4CB9F-48E0-05AA-5809-AAABF5408AB6";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" -5.757428e-006 -9.0341068e-012 ;
	setAttr ".uvtk[25]" -type "float2" 0.28392628 -9.0341068e-012 ;
	setAttr ".uvtk[74]" -type "float2" -0.00013989604 -5.0976854e-005 ;
	setAttr ".uvtk[123]" -type "float2" -0.011529644 -0.067086659 ;
	setAttr ".uvtk[124]" -type "float2" 0.28208482 -0.00067694014 ;
	setAttr ".uvtk[173]" -type "float2" -0.011099716 -0.068412952 ;
createNode polyMergeVert -n "polyMergeVert19";
	rename -uid "0D0563E8-47CB-5833-7F02-08B279D0CF63";
	setAttr ".ics" -type "componentList" 2 "vtx[16]" "vtx[26]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak16";
	rename -uid "2A51B3DB-4AA5-DD89-3C02-CCA25D2EE2A3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[1]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[6]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[16]" -type "float3" -4.0317058e-016 -1.110223e-016 4.7683716e-007 ;
	setAttr ".tk[26]" -type "float3" 5.3109856 1.8626449e-009 10.405281 ;
createNode objectSet -n "set3";
	rename -uid "272A38E8-44F4-5134-81A2-1B9EF3E0BE40";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "F52FEEBB-49E8-FC7A-1CD6-A1896382BECA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "17356E32-4253-41EF-5C8B-F4B7AAC73DEE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 7 "e[2]" "e[10]" "e[39]" "e[167]" "e[169]" "e[187]" "e[190:192]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "C2DC3965-4B19-3410-BF83-F281A69D7227";
	setAttr ".dc" -type "componentList" 3 "f[14]" "f[67]" "f[127:128]";
createNode objectSet -n "set4";
	rename -uid "15563274-4222-3955-8B2B-4F99D9A55E87";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "E895F96D-4636-F34F-342D-26868F44F7FB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "3E6DA43F-4B06-E8EE-54A1-3784A0B72F63";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[93:94]" "e[108]" "e[165]" "e[249]" "e[264]" "e[269:271]";
createNode deleteComponent -n "deleteComponent5";
	rename -uid "2C3FD874-466E-22A8-6CBE-C1B2FF4EF505";
	setAttr ".dc" -type "componentList" 4 "f[36]" "f[102]" "f[136]" "f[142]";
createNode objectSet -n "set5";
	rename -uid "297454F8-4F5B-2980-8B92-35AC330B372F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "B184B536-44B0-E4AA-CCD7-F299294DF475";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "7383AFAE-4EF2-F17C-B666-5EB1A2CE6FEB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 11 "e[9]" "e[22]" "e[31]" "e[37]" "e[92:93]" "e[106]" "e[162:164]" "e[184]" "e[243]" "e[246]" "e[262]";
createNode deleteComponent -n "deleteComponent6";
	rename -uid "ECA9A6C6-457F-5E06-E144-4897972B0C4C";
	setAttr ".dc" -type "componentList" 4 "f[7]" "f[42]" "f[71]" "f[94]";
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "E547C9B6-4483-88A6-965D-118165BA4BB5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[9]" "e[30]" "e[90]" "e[103]" "e[159]" "e[177]" "e[179]" "e[238]" "e[255:256]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 0 -29.392453511555978 ;
	setAttr ".pvt" -type "float3" 0 -7.6392221 0 ;
	setAttr ".rs" 44330;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -11.950618743896484 -15.278444290161133 29.392459869384766 ;
	setAttr ".cbx" -type "double3" 11.950618743896484 1.862645149230957e-009 29.392461776733398 ;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "60969231-4CBD-8495-0A49-F2BC21F753D1";
	setAttr ".ics" -type "componentList" 8 "e[295]" "e[297]" "e[299]" "e[301]" "e[304]" "e[307]" "e[309]" "e[312]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 150;
	setAttr ".sv2" 159;
	setAttr ".d" 1;
	setAttr ".sd" 1;
	setAttr ".td" 1;
createNode polyTweak -n "polyTweak17";
	rename -uid "56552C9C-4BDA-B197-D628-6EA0BF3FE0D8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[15]" -type "float3" 0 0 -9.5367432e-007 ;
	setAttr ".tk[96]" -type "float3" 0 0 -9.5367432e-007 ;
	setAttr ".tk[152]" -type "float3" 0 0 25.283735 ;
	setAttr ".tk[156]" -type "float3" 0 0 25.283735 ;
createNode polySplit -n "polySplit16";
	rename -uid "7982CD00-4BD5-5181-7981-36B3A50B459F";
	setAttr -s 11 ".e[0:10]"  1 0.141021 0.142143 0.181173 0.16146401 1
		 0.15875299 0.17599601 0.15909401 0.1499 1;
	setAttr -s 11 ".d[0:10]"  -2147483345 -2147483340 -2147483338 -2147483348 -2147483350 -2147483349 
		-2147483354 -2147483355 -2147483343 -2147483346 -2147483344;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode objectSet -n "set6";
	rename -uid "8652F672-43A0-8AD4-48A0-F0B2962948B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "19EF6359-46BB-3C49-D003-EF8819DB2303";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts22";
	rename -uid "6457419B-4C16-0076-4692-738B6DED910B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 7 "e[0:68]" "e[130:215]" "e[293:297]" "e[302:307]" "e[313:314]" "e[320:323]" "e[329:333]";
createNode polyTweak -n "polyTweak18";
	rename -uid "D2F6E032-4840-2ED4-B0C5-FA896B4D8572";
	setAttr ".uopa" yes;
	setAttr -s 121 ".tk";
	setAttr ".tk[1]" -type "float3" -7.1525574e-007 1.1175871e-007 0 ;
	setAttr ".tk[2]" -type "float3" 0 -3.7252903e-008 0 ;
	setAttr ".tk[6]" -type "float3" 0.933649 -1.9988729 0 ;
	setAttr ".tk[8]" -type "float3" 0 -2.7939677e-008 0 ;
	setAttr ".tk[11]" -type "float3" 0 3.4570694e-006 0 ;
	setAttr ".tk[12]" -type "float3" -3.2585752e-018 -1.9988716 0 ;
	setAttr ".tk[13]" -type "float3" -6.6174449e-024 3.9339066e-006 0 ;
	setAttr ".tk[14]" -type "float3" 0 2.9802322e-006 0 ;
	setAttr ".tk[15]" -type "float3" 0 3.4570694e-006 0 ;
	setAttr ".tk[16]" -type "float3" 0 2.5877416 0 ;
	setAttr ".tk[17]" -type "float3" 0 2.5877423 0 ;
	setAttr ".tk[19]" -type "float3" 0 2.5877421 0 ;
	setAttr ".tk[20]" -type "float3" 0.48901731 -1.9988744 0 ;
	setAttr ".tk[21]" -type "float3" -2.3841858e-007 1.013279e-006 0 ;
	setAttr ".tk[22]" -type "float3" 0 7.1525574e-007 0 ;
	setAttr ".tk[23]" -type "float3" 0 -3.054738e-007 0 ;
	setAttr ".tk[24]" -type "float3" 0 3.9339066e-006 0 ;
	setAttr ".tk[25]" -type "float3" 0 1.013279e-006 0 ;
	setAttr ".tk[26]" -type "float3" 0 -2.0861626e-007 0 ;
	setAttr ".tk[32]" -type "float3" 0 -6.519258e-009 0 ;
	setAttr ".tk[33]" -type "float3" 0 1.7136335e-007 0 ;
	setAttr ".tk[34]" -type "float3" 0 2.1457672e-006 0 ;
	setAttr ".tk[35]" -type "float3" 0 3.9339066e-006 0 ;
	setAttr ".tk[36]" -type "float3" 0 3.9339066e-006 0 ;
	setAttr ".tk[37]" -type "float3" 0 7.1525574e-007 0 ;
	setAttr ".tk[38]" -type "float3" 0 -2.0861626e-007 0 ;
	setAttr ".tk[43]" -type "float3" 7.1525574e-007 1.1175871e-007 0 ;
	setAttr ".tk[44]" -type "float3" 0 -3.7252903e-008 0 ;
	setAttr ".tk[47]" -type "float3" 0 0 -2.5033951e-006 ;
	setAttr ".tk[48]" -type "float3" -0.933649 -1.9988729 0 ;
	setAttr ".tk[49]" -type "float3" 0 -3.6081851 -2.3841858e-007 ;
	setAttr ".tk[50]" -type "float3" 0 -3.6081846 2.3841858e-007 ;
	setAttr ".tk[51]" -type "float3" 0 -3.2238834 0 ;
	setAttr ".tk[52]" -type "float3" 0 -3.2238834 0 ;
	setAttr ".tk[53]" -type "float3" 0 2.5877416 0 ;
	setAttr ".tk[55]" -type "float3" 0 2.5877421 0 ;
	setAttr ".tk[56]" -type "float3" -0.48901731 -1.9988744 0 ;
	setAttr ".tk[57]" -type "float3" 2.3841858e-007 1.013279e-006 0 ;
	setAttr ".tk[58]" -type "float3" 0 7.1525574e-007 0 ;
	setAttr ".tk[59]" -type "float3" 0 -3.054738e-007 0 ;
	setAttr ".tk[60]" -type "float3" 0 1.013279e-006 0 ;
	setAttr ".tk[61]" -type "float3" 0 -2.0861626e-007 0 ;
	setAttr ".tk[62]" -type "float3" 0 -3.6081851 0 ;
	setAttr ".tk[63]" -type "float3" 0 -3.2238834 0 ;
	setAttr ".tk[66]" -type "float3" 0 -3.2238834 0 ;
	setAttr ".tk[67]" -type "float3" 0 -3.6081851 0 ;
	setAttr ".tk[68]" -type "float3" 0 1.7136335e-007 0 ;
	setAttr ".tk[69]" -type "float3" 0 2.1457672e-006 0 ;
	setAttr ".tk[70]" -type "float3" 0 7.1525574e-007 0 ;
	setAttr ".tk[71]" -type "float3" 0 -2.0861626e-007 0 ;
	setAttr ".tk[72]" -type "float3" 0 -3.6081851 0 ;
	setAttr ".tk[73]" -type "float3" 0 -3.2238834 0 ;
	setAttr ".tk[75]" -type "float3" 0 2.2351742e-008 0 ;
	setAttr ".tk[76]" -type "float3" 0 1.937151e-007 0 ;
	setAttr ".tk[77]" -type "float3" 0 -1.6391277e-007 0 ;
	setAttr ".tk[84]" -type "float3" 0 -2.5877399 0 ;
	setAttr ".tk[85]" -type "float3" 0 -2.587729 0 ;
	setAttr ".tk[86]" -type "float3" 0 -4.529953e-006 0 ;
	setAttr ".tk[87]" -type "float3" -3.129962e-016 1.9988731 0 ;
	setAttr ".tk[88]" -type "float3" -3.7057691e-022 -9.894371e-006 0 ;
	setAttr ".tk[89]" -type "float3" 0.48901731 1.9988713 0 ;
	setAttr ".tk[90]" -type "float3" -2.3841858e-007 -6.5565109e-007 0 ;
	setAttr ".tk[91]" -type "float3" 0 2.3841858e-007 0 ;
	setAttr ".tk[92]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[93]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[94]" -type "float3" 0 -2.8610229e-006 0 ;
	setAttr ".tk[95]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[96]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[97]" -type "float3" 0 1.7881393e-006 0 ;
	setAttr ".tk[98]" -type "float3" 0 -3.2782555e-007 0 ;
	setAttr ".tk[100]" -type "float3" 0 -2.5877411 0 ;
	setAttr ".tk[101]" -type "float3" 0.933649 1.998873 0 ;
	setAttr ".tk[102]" -type "float3" -7.1525574e-007 -1.7881393e-007 0 ;
	setAttr ".tk[103]" -type "float3" 0 9.6857548e-008 0 ;
	setAttr ".tk[105]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[106]" -type "float3" 0 -8.3446503e-007 0 ;
	setAttr ".tk[107]" -type "float3" 0 -1.937151e-007 0 ;
	setAttr ".tk[113]" -type "float3" 0 -2.0489097e-008 0 ;
	setAttr ".tk[117]" -type "float3" 0 3.6081858 0 ;
	setAttr ".tk[118]" -type "float3" 0 3.6081858 0 ;
	setAttr ".tk[119]" -type "float3" 0 -1.6391277e-007 0 ;
	setAttr ".tk[120]" -type "float3" 0 7.4505806e-009 0 ;
	setAttr ".tk[121]" -type "float3" 0 3.2238824 0 ;
	setAttr ".tk[122]" -type "float3" 0 3.2238824 0 ;
	setAttr ".tk[126]" -type "float3" 0 -2.5877399 0 ;
	setAttr ".tk[127]" -type "float3" 2.3841858e-007 -6.5565109e-007 0 ;
	setAttr ".tk[128]" -type "float3" -0.48901731 1.9988713 0 ;
	setAttr ".tk[129]" -type "float3" 0 -2.8610229e-006 0 ;
	setAttr ".tk[130]" -type "float3" 0 2.3841858e-007 0 ;
	setAttr ".tk[131]" -type "float3" 0 -3.2782555e-007 0 ;
	setAttr ".tk[132]" -type "float3" 0 2.0265579e-006 0 ;
	setAttr ".tk[134]" -type "float3" 0 -2.5877411 0 ;
	setAttr ".tk[135]" -type "float3" -0.933649 1.998873 0 ;
	setAttr ".tk[136]" -type "float3" 7.1525574e-007 -1.7881393e-007 0 ;
	setAttr ".tk[137]" -type "float3" 0 0 -2.6226044e-006 ;
	setAttr ".tk[138]" -type "float3" 0 1.1175871e-007 0 ;
	setAttr ".tk[139]" -type "float3" 0 -8.3446503e-007 0 ;
	setAttr ".tk[140]" -type "float3" 0 -1.937151e-007 0 ;
	setAttr ".tk[141]" -type "float3" 0 3.6081858 0 ;
	setAttr ".tk[142]" -type "float3" 0 3.2238824 0 ;
	setAttr ".tk[144]" -type "float3" 0 3.2238824 0 ;
	setAttr ".tk[146]" -type "float3" 0 3.6081853 2.3841858e-007 ;
	setAttr ".tk[147]" -type "float3" 0 3.6081867 -2.3841858e-007 ;
	setAttr ".tk[148]" -type "float3" 0 3.2238834 0 ;
	setAttr ".tk[152]" -type "float3" 0 3.4570694e-006 0 ;
	setAttr ".tk[153]" -type "float3" 2.1713667 -5.8291426 7.2770575e-007 ;
	setAttr ".tk[154]" -type "float3" -2.1713655 -5.8291426 -7.2770575e-007 ;
	setAttr ".tk[156]" -type "float3" 0 -9.894371e-006 0 ;
	setAttr ".tk[158]" -type "float3" 2.1713667 5.8291435 7.2770575e-007 ;
	setAttr ".tk[159]" -type "float3" -2.1713655 5.8291445 -7.2770575e-007 ;
	setAttr ".tk[160]" -type "float3" 0 -3.3527613e-007 0 ;
	setAttr ".tk[162]" -type "float3" 0 0 1.1471863 ;
	setAttr ".tk[163]" -type "float3" 0 -3.054738e-007 0.60086823 ;
	setAttr ".tk[164]" -type "float3" 0 -3.054738e-007 0.55742455 ;
	setAttr ".tk[165]" -type "float3" 0 0 1.0642366 ;
	setAttr ".tk[166]" -type "float3" 0 0 0.56744289 ;
	setAttr ".tk[167]" -type "float3" 0 -3.3527613e-007 0.29721451 ;
createNode deleteComponent -n "deleteComponent7";
	rename -uid "E7C05F78-4CAD-2302-B321-688812D3380A";
	setAttr ".dc" -type "componentList" 7 "f[0:27]" "f[56:83]" "f[112:127]" "f[144:145]" "f[148:150]" "f[154:155]" "f[163:167]";
createNode polyMirror -n "polyMirror3";
	rename -uid "A88FB842-484E-CB08-5AE7-D4AFF1E4EDE2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".mtt" 1;
	setAttr ".mt" 24.863273620605469;
	setAttr ".cm" yes;
	setAttr ".fnf" 84;
	setAttr ".lnf" 167;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "FFF9424B-479E-B887-9439-6D86CEC6C236";
	setAttr ".ics" -type "componentList" 4 "e[168]" "e[171]" "e[330]" "e[333]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak19";
	rename -uid "02B411D4-42A7-3C82-F9DC-3C92EA3BD023";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[5]" -type "float3" 0 0 -5.8907928 ;
	setAttr ".tk[22]" -type "float3" 0 0 -2.8053665 ;
	setAttr ".tk[42]" -type "float3" 0 0 -5.8907928 ;
	setAttr ".tk[60]" -type "float3" 0 0 -2.8053665 ;
	setAttr ".tk[107]" -type "float3" 0 0 -2.8053665 ;
	setAttr ".tk[136]" -type "float3" 0 0 -2.8053665 ;
createNode polyTweak -n "polyTweak20";
	rename -uid "2C2E1DD7-4815-EB2C-2159-519E665C5005";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[85]" -type "float3" -1.877665 0 0 ;
	setAttr ".tk[88]" -type "float3" -1.877665 0 0 ;
	setAttr ".tk[160]" -type "float3" 1.877665 0 0 ;
	setAttr ".tk[162]" -type "float3" 1.877665 0 0 ;
createNode polySplit -n "polySplit17";
	rename -uid "4343AAC0-4DEE-F870-CBED-6CA22A495857";
	setAttr -s 4 ".e[0:3]"  1 0.5 0.5 1;
	setAttr -s 4 ".d[0:3]"  -2147483478 -2147483483 -2147483489 -2147483475;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit18";
	rename -uid "1D22F7BD-4245-6EDE-D178-98AA449A61C0";
	setAttr -s 2 ".e[0:1]"  0.5 0;
	setAttr -s 2 ".d[0:1]"  -2147483323 -2147483334;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit19";
	rename -uid "B0BDC16E-47EB-37D6-6795-BDA6E7DCCB67";
	setAttr -s 3 ".e[0:2]"  1 0.5 1;
	setAttr -s 3 ".d[0:2]"  -2147483323 -2147483321 -2147483331;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7A20FB75-4FC0-DF6C-D66C-7DAA33C87439";
	setAttr ".r" 20;
	setAttr ".h" 66;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "7E6307F6-4B78-45D9-8139-0A8F7BFB3209";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.6515735387802124;
	setAttr ".dr" no;
	setAttr ".re" 55;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "DBE542E7-4B95-26D3-954B-D9B8C0F7BF4E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[140:141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[175]" "e[177]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.52542400360107422;
	setAttr ".dr" no;
	setAttr ".re" 151;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak21";
	rename -uid "29C03B95-4374-6338-C628-1D85EBAECBCD";
	setAttr ".uopa" yes;
	setAttr -s 80 ".tk";
	setAttr ".tk[3]" -type "float3" -1.1920929e-007 0 0 ;
	setAttr ".tk[6]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".tk[8]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[9]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[10]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[11]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[13]" -type "float3" 1.1920929e-007 0 0 ;
	setAttr ".tk[14]" -type "float3" -1.4210855e-014 0 0 ;
	setAttr ".tk[16]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[17]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[18]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[19]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[23]" -type "float3" -1.1920929e-007 0 0 ;
	setAttr ".tk[26]" -type "float3" -9.5367432e-007 0 0 ;
	setAttr ".tk[28]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[29]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[30]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[31]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[33]" -type "float3" 1.1920929e-007 0 0 ;
	setAttr ".tk[34]" -type "float3" -1.4210855e-014 0 0 ;
	setAttr ".tk[36]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[37]" -type "float3" -2.3841858e-007 0 0 ;
	setAttr ".tk[38]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[39]" -type "float3" 2.3841858e-007 0 0 ;
	setAttr ".tk[42]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[43]" -type "float3" -1.4210855e-014 19.555555 0 ;
	setAttr ".tk[44]" -type "float3" 1.1920929e-007 19.555555 0 ;
	setAttr ".tk[45]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[46]" -type "float3" 2.3841858e-007 19.555555 0 ;
	setAttr ".tk[47]" -type "float3" -2.3841858e-007 19.555555 0 ;
	setAttr ".tk[48]" -type "float3" -2.3841858e-007 19.555555 0 ;
	setAttr ".tk[49]" -type "float3" -2.3841858e-007 19.555555 0 ;
	setAttr ".tk[50]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[51]" -type "float3" -9.5367432e-007 19.555555 0 ;
	setAttr ".tk[52]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[53]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[54]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[55]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[56]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[57]" -type "float3" 0 19.555555 0 ;
	setAttr ".tk[58]" -type "float3" 2.3841858e-007 19.555555 0 ;
	setAttr ".tk[59]" -type "float3" 2.3841858e-007 19.555555 0 ;
	setAttr ".tk[60]" -type "float3" -2.3841858e-007 19.555555 0 ;
	setAttr ".tk[61]" -type "float3" 2.3841858e-007 19.555555 0 ;
	setAttr ".tk[62]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[63]" -type "float3" -1.4210855e-014 -19.555555 0 ;
	setAttr ".tk[64]" -type "float3" 1.1920929e-007 -19.555555 0 ;
	setAttr ".tk[65]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[66]" -type "float3" 2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[67]" -type "float3" -2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[68]" -type "float3" -2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[69]" -type "float3" -2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[70]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[71]" -type "float3" -9.5367432e-007 -19.555555 0 ;
	setAttr ".tk[72]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[73]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[74]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[75]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[76]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[77]" -type "float3" 0 -19.555555 0 ;
	setAttr ".tk[78]" -type "float3" 2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[79]" -type "float3" 2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[80]" -type "float3" -2.3841858e-007 -19.555555 0 ;
	setAttr ".tk[81]" -type "float3" 2.3841858e-007 -19.555555 0 ;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "8588C51D-4998-AF0D-F149-09B186C2EAFC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[220:221]" "e[223]" "e[225]" "e[227]" "e[229]" "e[231]" "e[233]" "e[235]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.49280259013175964;
	setAttr ".re" 251;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak22";
	rename -uid "2FEB68E0-4DE5-8506-851C-058A7706583A";
	setAttr ".uopa" yes;
	setAttr -s 120 ".tk";
	setAttr ".tk[0]" -type "float3" -3.9730439 1.1920929e-007 1.2909178 ;
	setAttr ".tk[1]" -type "float3" -3.3796716 1.1920929e-007 2.4554756 ;
	setAttr ".tk[2]" -type "float3" -2.4554753 1.1920929e-007 3.3796711 ;
	setAttr ".tk[3]" -type "float3" -1.2909201 1.1920929e-007 3.9730401 ;
	setAttr ".tk[4]" -type "float3" -3.9839773e-007 1.1920929e-007 4.1775041 ;
	setAttr ".tk[5]" -type "float3" 1.2909188 1.1920929e-007 3.9730401 ;
	setAttr ".tk[6]" -type "float3" 2.4554756 1.1920929e-007 3.3796673 ;
	setAttr ".tk[7]" -type "float3" 3.3796692 1.1920929e-007 2.4554729 ;
	setAttr ".tk[8]" -type "float3" 3.9730382 1.1920929e-007 1.2909188 ;
	setAttr ".tk[9]" -type "float3" 4.1775017 1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[10]" -type "float3" 3.9730382 1.1920929e-007 -1.2909201 ;
	setAttr ".tk[11]" -type "float3" 3.3796673 1.1920929e-007 -2.4554775 ;
	setAttr ".tk[12]" -type "float3" 2.4554729 1.1920929e-007 -3.3796711 ;
	setAttr ".tk[13]" -type "float3" 1.2909175 1.1920929e-007 -3.9730401 ;
	setAttr ".tk[14]" -type "float3" -2.7389837e-007 1.1920929e-007 -4.1775041 ;
	setAttr ".tk[15]" -type "float3" -1.2909188 1.1920929e-007 -3.9730401 ;
	setAttr ".tk[16]" -type "float3" -2.4554737 1.1920929e-007 -3.3796711 ;
	setAttr ".tk[17]" -type "float3" -3.3796673 1.1920929e-007 -2.4554765 ;
	setAttr ".tk[18]" -type "float3" -3.9730382 1.1920929e-007 -1.2909194 ;
	setAttr ".tk[19]" -type "float3" -4.1775017 1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[20]" -type "float3" -3.9730439 -1.1920929e-007 1.2909178 ;
	setAttr ".tk[21]" -type "float3" -3.3796716 -1.1920929e-007 2.4554756 ;
	setAttr ".tk[22]" -type "float3" -2.4554753 -1.1920929e-007 3.3796711 ;
	setAttr ".tk[23]" -type "float3" -1.2909201 -1.1920929e-007 3.9730401 ;
	setAttr ".tk[24]" -type "float3" -3.9839773e-007 -1.1920929e-007 4.1775041 ;
	setAttr ".tk[25]" -type "float3" 1.2909188 -1.1920929e-007 3.9730401 ;
	setAttr ".tk[26]" -type "float3" 2.4554756 -1.1920929e-007 3.3796673 ;
	setAttr ".tk[27]" -type "float3" 3.3796692 -1.1920929e-007 2.4554729 ;
	setAttr ".tk[28]" -type "float3" 3.9730382 -1.1920929e-007 1.2909188 ;
	setAttr ".tk[29]" -type "float3" 4.1775017 -1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[30]" -type "float3" 3.9730382 -1.1920929e-007 -1.2909201 ;
	setAttr ".tk[31]" -type "float3" 3.3796673 -1.1920929e-007 -2.4554775 ;
	setAttr ".tk[32]" -type "float3" 2.4554729 -1.1920929e-007 -3.3796711 ;
	setAttr ".tk[33]" -type "float3" 1.2909175 -1.1920929e-007 -3.9730401 ;
	setAttr ".tk[34]" -type "float3" -2.7389837e-007 -1.1920929e-007 -4.1775041 ;
	setAttr ".tk[35]" -type "float3" -1.2909188 -1.1920929e-007 -3.9730401 ;
	setAttr ".tk[36]" -type "float3" -2.4554737 -1.1920929e-007 -3.3796711 ;
	setAttr ".tk[37]" -type "float3" -3.3796673 -1.1920929e-007 -2.4554765 ;
	setAttr ".tk[38]" -type "float3" -3.9730382 -1.1920929e-007 -1.2909194 ;
	setAttr ".tk[39]" -type "float3" -4.1775017 -1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[42]" -type "float3" -1.2909188 -1.1920929e-007 -3.9730401 ;
	setAttr ".tk[43]" -type "float3" -2.7389837e-007 -1.1920929e-007 -4.1775041 ;
	setAttr ".tk[44]" -type "float3" 1.2909175 -1.1920929e-007 -3.9730401 ;
	setAttr ".tk[45]" -type "float3" 2.4554729 -1.1920929e-007 -3.3796711 ;
	setAttr ".tk[46]" -type "float3" 3.3796673 -1.1920929e-007 -2.4554775 ;
	setAttr ".tk[47]" -type "float3" 3.9730382 -1.1920929e-007 -1.2909201 ;
	setAttr ".tk[48]" -type "float3" 4.1775017 -1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[49]" -type "float3" 3.9730382 -1.1920929e-007 1.2909188 ;
	setAttr ".tk[50]" -type "float3" 3.3796692 -1.1920929e-007 2.4554729 ;
	setAttr ".tk[51]" -type "float3" 2.4554756 -1.1920929e-007 3.3796673 ;
	setAttr ".tk[52]" -type "float3" 1.2909187 -1.1920929e-007 3.9730401 ;
	setAttr ".tk[53]" -type "float3" -3.9839773e-007 -1.1920929e-007 4.1775041 ;
	setAttr ".tk[54]" -type "float3" -1.2909201 -1.1920929e-007 3.9730401 ;
	setAttr ".tk[55]" -type "float3" -2.4554753 -1.1920929e-007 3.3796711 ;
	setAttr ".tk[56]" -type "float3" -3.3796716 -1.1920929e-007 2.4554756 ;
	setAttr ".tk[57]" -type "float3" -3.9730439 -1.1920929e-007 1.2909178 ;
	setAttr ".tk[58]" -type "float3" -4.1775017 -1.1920929e-007 -7.9679546e-007 ;
	setAttr ".tk[59]" -type "float3" -3.9730382 -1.1920929e-007 -1.2909194 ;
	setAttr ".tk[60]" -type "float3" -3.3796673 -1.1920929e-007 -2.4554765 ;
	setAttr ".tk[61]" -type "float3" -2.4554737 -1.1920929e-007 -3.3796711 ;
	setAttr ".tk[62]" -type "float3" -1.2909188 2.3841858e-007 -3.9730401 ;
	setAttr ".tk[63]" -type "float3" -2.7389837e-007 2.3841858e-007 -4.1775041 ;
	setAttr ".tk[64]" -type "float3" 1.2909175 2.3841858e-007 -3.9730401 ;
	setAttr ".tk[65]" -type "float3" 2.4554729 2.3841858e-007 -3.3796711 ;
	setAttr ".tk[66]" -type "float3" 3.3796673 2.3841858e-007 -2.4554775 ;
	setAttr ".tk[67]" -type "float3" 3.9730382 2.3841858e-007 -1.2909201 ;
	setAttr ".tk[68]" -type "float3" 4.1775017 2.3841858e-007 -7.9679546e-007 ;
	setAttr ".tk[69]" -type "float3" 3.9730382 2.3841858e-007 1.2909188 ;
	setAttr ".tk[70]" -type "float3" 3.3796692 2.3841858e-007 2.4554729 ;
	setAttr ".tk[71]" -type "float3" 2.4554756 2.3841858e-007 3.3796673 ;
	setAttr ".tk[72]" -type "float3" 1.2909187 2.3841858e-007 3.9730401 ;
	setAttr ".tk[73]" -type "float3" -3.9839773e-007 2.3841858e-007 4.1775041 ;
	setAttr ".tk[74]" -type "float3" -1.2909201 2.3841858e-007 3.9730401 ;
	setAttr ".tk[75]" -type "float3" -2.4554753 2.3841858e-007 3.3796711 ;
	setAttr ".tk[76]" -type "float3" -3.3796716 2.3841858e-007 2.4554756 ;
	setAttr ".tk[77]" -type "float3" -3.9730439 2.3841858e-007 1.2909178 ;
	setAttr ".tk[78]" -type "float3" -4.1775017 2.3841858e-007 -7.9679546e-007 ;
	setAttr ".tk[79]" -type "float3" -3.9730382 2.3841858e-007 -1.2909194 ;
	setAttr ".tk[80]" -type "float3" -3.3796673 2.3841858e-007 -2.4554765 ;
	setAttr ".tk[81]" -type "float3" -2.4554737 2.3841858e-007 -3.3796711 ;
	setAttr ".tk[82]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[83]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[84]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[85]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[86]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[87]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[88]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[89]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[90]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[91]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[92]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[93]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[94]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[95]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[96]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[97]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[98]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[99]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[100]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[101]" -type "float3" 0 7.8689713 0 ;
	setAttr ".tk[102]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[103]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[104]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[105]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[106]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[107]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[108]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[109]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[110]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[111]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[112]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[113]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[114]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[115]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[116]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[117]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[118]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[119]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[120]" -type "float3" 0 -7.8689713 0 ;
	setAttr ".tk[121]" -type "float3" 0 -7.8689713 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "82D1D6B6-494B-0DB4-D850-9D816700255C";
	setAttr ".ics" -type "componentList" 1 "f[160:179]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 66.666664 3.8146973e-006 33.333328 ;
	setAttr ".rs" 35709;
	setAttr ".lt" -type "double3" 9.4368957093138306e-016 6.9905941996055316e-015 0.70917301903257168 ;
	setAttr ".ls" -type "double3" 1 0.88056814482846291 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 44.438477198282882 -22.228189468383789 30.360655864079792 ;
	setAttr ".cbx" -type "double3" 88.894852320353195 22.22819709777832 36.306000789006561 ;
createNode polyTweak -n "polyTweak23";
	rename -uid "413AC647-4022-4DD5-EB90-ECADED82334B";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk";
	setAttr ".tk[122]" -type "float3" -0.68854761 -3.0453789 2.1191318 ;
	setAttr ".tk[123]" -type "float3" -1.3096951 -3.0453789 1.8026413 ;
	setAttr ".tk[124]" -type "float3" -1.8026412 -3.0453789 1.3096962 ;
	setAttr ".tk[125]" -type "float3" -2.1191313 -3.0453789 0.68854815 ;
	setAttr ".tk[126]" -type "float3" -2.2281864 -3.0453789 4.2499292e-007 ;
	setAttr ".tk[127]" -type "float3" -2.1191313 -3.0453789 -0.68854749 ;
	setAttr ".tk[128]" -type "float3" -1.8026413 -3.0453789 -1.3096951 ;
	setAttr ".tk[129]" -type "float3" -1.3096954 -3.0453789 -1.8026412 ;
	setAttr ".tk[130]" -type "float3" -0.68854791 -3.0453789 -2.1191318 ;
	setAttr ".tk[131]" -type "float3" 2.1249646e-007 -3.0453789 -2.2281868 ;
	setAttr ".tk[132]" -type "float3" 0.68854809 -3.0453789 -2.1191318 ;
	setAttr ".tk[133]" -type "float3" 1.3096966 -3.0453789 -1.8026413 ;
	setAttr ".tk[134]" -type "float3" 1.802642 -3.0453789 -1.3096954 ;
	setAttr ".tk[135]" -type "float3" 2.1191337 -3.0453789 -0.68854761 ;
	setAttr ".tk[136]" -type "float3" 2.2281864 -3.0453789 4.2499292e-007 ;
	setAttr ".tk[137]" -type "float3" 2.1191313 -3.0453789 0.68854803 ;
	setAttr ".tk[138]" -type "float3" 1.8026412 -3.0453789 1.3096962 ;
	setAttr ".tk[139]" -type "float3" 1.3096954 -3.0453789 1.8026413 ;
	setAttr ".tk[140]" -type "float3" 0.68854791 -3.0453789 2.1191318 ;
	setAttr ".tk[141]" -type "float3" 1.460913e-007 -3.0453789 2.2281868 ;
	setAttr ".tk[142]" -type "float3" -0.68854761 3.0453789 2.1191318 ;
	setAttr ".tk[143]" -type "float3" -1.3096951 3.0453789 1.8026413 ;
	setAttr ".tk[144]" -type "float3" -1.8026412 3.0453789 1.3096962 ;
	setAttr ".tk[145]" -type "float3" -2.1191313 3.0453789 0.68854815 ;
	setAttr ".tk[146]" -type "float3" -2.2281864 3.0453789 4.2499292e-007 ;
	setAttr ".tk[147]" -type "float3" -2.1191313 3.0453789 -0.68854749 ;
	setAttr ".tk[148]" -type "float3" -1.8026413 3.0453789 -1.3096951 ;
	setAttr ".tk[149]" -type "float3" -1.3096954 3.0453789 -1.8026412 ;
	setAttr ".tk[150]" -type "float3" -0.68854791 3.0453789 -2.1191318 ;
	setAttr ".tk[151]" -type "float3" 2.1249646e-007 3.0453789 -2.2281868 ;
	setAttr ".tk[152]" -type "float3" 0.68854809 3.0453789 -2.1191318 ;
	setAttr ".tk[153]" -type "float3" 1.3096966 3.0453789 -1.8026413 ;
	setAttr ".tk[154]" -type "float3" 1.802642 3.0453789 -1.3096954 ;
	setAttr ".tk[155]" -type "float3" 2.1191337 3.0453789 -0.68854761 ;
	setAttr ".tk[156]" -type "float3" 2.2281864 3.0453789 4.2499292e-007 ;
	setAttr ".tk[157]" -type "float3" 2.1191313 3.0453789 0.68854803 ;
	setAttr ".tk[158]" -type "float3" 1.8026412 3.0453789 1.3096962 ;
	setAttr ".tk[159]" -type "float3" 1.3096954 3.0453789 1.8026413 ;
	setAttr ".tk[160]" -type "float3" 0.68854791 3.0453789 2.1191318 ;
	setAttr ".tk[161]" -type "float3" 1.460913e-007 3.0453789 2.2281868 ;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "811AD2A3-473D-539B-3B70-FFB4B6A5FF7D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 38 "e[102]" "e[104]" "e[106]" "e[108]" "e[110]" "e[112]" "e[114]" "e[116]" "e[118]" "e[120]" "e[122]" "e[124]" "e[126]" "e[128]" "e[130]" "e[132]" "e[134]" "e[136]" "e[138:139]" "e[142]" "e[144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[162]" "e[164]" "e[166]" "e[168]" "e[170]" "e[172]" "e[174]" "e[176]" "e[178:179]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".a" 0;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "3F22BC0D-4084-BE43-42B3-8C8D7E79CD13";
	setAttr ".ics" -type "componentList" 1 "f[20:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 66.666664 3.8146973e-006 33.333332 ;
	setAttr ".rs" 33393;
	setAttr ".ls" -type "double3" 0.75001276455284771 0.75001276455284771 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 50.844164530436203 -15.82249832153321 0.3333333333333357 ;
	setAttr ".cbx" -type "double3" 82.48916880289714 15.822505950927741 66.333329518636077 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "67AAEF41-430A-EB00-CBCE-E1A8AFCAFE65";
	setAttr ".ics" -type "componentList" 1 "f[20:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 66.666664 3.8146973e-006 33.333332 ;
	setAttr ".rs" 53313;
	setAttr ".lt" -type "double3" 0 3.46959941788412e-016 -1.6001157163894988 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 52.60928599039714 -14.057373046875007 0.3333333333333357 ;
	setAttr ".cbx" -type "double3" 80.724047342936203 14.057380676269538 66.333329518636077 ;
createNode polyTweak -n "polyTweak24";
	rename -uid "5914535F-42CC-BAE3-0D6D-89A2A43D3194";
	setAttr ".uopa" yes;
	setAttr -s 45 ".tk";
	setAttr ".tk[240]" -type "float3" -0.47745231 0 0.15513383 ;
	setAttr ".tk[241]" -type "float3" -0.40614554 0 0.29508179 ;
	setAttr ".tk[242]" -type "float3" 0 0 -1.3623226e-007 ;
	setAttr ".tk[243]" -type "float3" -0.29508179 0 0.40614575 ;
	setAttr ".tk[244]" -type "float3" -0.15513359 0 0.47745231 ;
	setAttr ".tk[245]" -type "float3" 0 0 0.5020231 ;
	setAttr ".tk[246]" -type "float3" 0.15513393 0 0.4774529 ;
	setAttr ".tk[247]" -type "float3" 0.29508239 0 0.40614554 ;
	setAttr ".tk[248]" -type "float3" 0.40614575 0 0.29508212 ;
	setAttr ".tk[249]" -type "float3" 0.47745231 0 0.15513371 ;
	setAttr ".tk[250]" -type "float3" 0.5020237 0 -1.1175294e-007 ;
	setAttr ".tk[251]" -type "float3" 0.47745231 0 -0.15513384 ;
	setAttr ".tk[252]" -type "float3" 0.40614575 0 -0.29508188 ;
	setAttr ".tk[253]" -type "float3" 0.29508179 0 -0.4061456 ;
	setAttr ".tk[254]" -type "float3" 0.15513393 0 -0.47745302 ;
	setAttr ".tk[255]" -type "float3" 0 0 -0.5020231 ;
	setAttr ".tk[256]" -type "float3" -0.15513393 0 -0.47745231 ;
	setAttr ".tk[257]" -type "float3" -0.29508179 0 -0.4061456 ;
	setAttr ".tk[258]" -type "float3" -0.40614575 0 -0.29508188 ;
	setAttr ".tk[259]" -type "float3" -0.47745231 0 -0.15513374 ;
	setAttr ".tk[260]" -type "float3" -0.5020237 0 -1.0536712e-007 ;
	setAttr ".tk[261]" -type "float3" -0.47745231 0 0.15513383 ;
	setAttr ".tk[262]" -type "float3" -0.40614554 0 0.29508179 ;
	setAttr ".tk[263]" -type "float3" 0 0 -1.3623226e-007 ;
	setAttr ".tk[264]" -type "float3" -0.29508179 0 0.40614575 ;
	setAttr ".tk[265]" -type "float3" -0.15513359 0 0.47745231 ;
	setAttr ".tk[266]" -type "float3" 0 0 0.5020231 ;
	setAttr ".tk[267]" -type "float3" 0.15513362 0 0.4774529 ;
	setAttr ".tk[268]" -type "float3" 0.29508239 0 0.40614554 ;
	setAttr ".tk[269]" -type "float3" 0.40614575 0 0.29508212 ;
	setAttr ".tk[270]" -type "float3" 0.47745231 0 0.15513371 ;
	setAttr ".tk[271]" -type "float3" 0.5020237 0 -1.1175294e-007 ;
	setAttr ".tk[272]" -type "float3" 0.47745231 0 -0.15513384 ;
	setAttr ".tk[273]" -type "float3" 0.40614575 0 -0.29508188 ;
	setAttr ".tk[274]" -type "float3" 0.29508179 0 -0.4061456 ;
	setAttr ".tk[275]" -type "float3" 0.15513393 0 -0.47745302 ;
	setAttr ".tk[276]" -type "float3" 0 0 -0.5020231 ;
	setAttr ".tk[277]" -type "float3" -0.15513393 0 -0.47745231 ;
	setAttr ".tk[278]" -type "float3" -0.29508179 0 -0.4061456 ;
	setAttr ".tk[279]" -type "float3" -0.40614575 0 -0.29508188 ;
	setAttr ".tk[280]" -type "float3" -0.47745231 0 -0.15513374 ;
	setAttr ".tk[281]" -type "float3" -0.5020237 0 -1.0536712e-007 ;
createNode deleteComponent -n "deleteComponent8";
	rename -uid "17157163-4971-9D43-7218-EFA6DD863BBF";
	setAttr ".dc" -type "componentList" 1 "f[120:219]";
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "D4B1C5EF-4216-97A0-C2AF-CBA8ADE44D93";
	setAttr ".ics" -type "componentList" 19 "e[142]" "e[144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[162]" "e[164]" "e[166]" "e[168]" "e[170]" "e[172]" "e[174]" "e[176]" "e[178:199]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 81;
	setAttr ".sv2" 101;
	setAttr ".d" 1;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "81D3EDDB-40A4-3CE4-B7F1-EFAF0169933A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[400:419]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.47527080774307251;
	setAttr ".re" 414;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "8B3FF7F5-480A-C1C3-7C6E-789F7225C0B3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[460:461]" "e[463]" "e[465]" "e[467]" "e[469]" "e[471]" "e[473]" "e[475]" "e[477]" "e[479]" "e[481]" "e[483]" "e[485]" "e[487]" "e[489]" "e[491]" "e[493]" "e[495]" "e[497]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.49385032057762146;
	setAttr ".re" 463;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak25";
	rename -uid "F89E4DF6-4279-A548-8C53-D3A4E651A584";
	setAttr ".uopa" yes;
	setAttr -s 43 ".tk";
	setAttr ".tk[202]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[203]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[204]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[205]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[206]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[207]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[208]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[209]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[210]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[211]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[212]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[213]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[214]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[215]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[216]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[217]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[218]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[219]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[220]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[221]" -type "float3" 0 -7.890336 0 ;
	setAttr ".tk[222]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[223]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[224]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[225]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[226]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[227]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[228]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[229]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[230]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[231]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[232]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[233]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[234]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[235]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[236]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[237]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[238]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[239]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[240]" -type "float3" 0 7.890336 0 ;
	setAttr ".tk[241]" -type "float3" 0 7.890336 0 ;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "840704C0-4C7D-2CFB-CFDB-A6BFB68A7B36";
	setAttr ".ics" -type "componentList" 38 "e[502]" "e[504]" "e[506]" "e[508]" "e[510]" "e[512]" "e[514]" "e[516]" "e[518]" "e[520]" "e[522]" "e[524]" "e[526]" "e[528]" "e[530]" "e[532]" "e[534]" "e[536]" "e[538:539]" "e[542]" "e[544]" "e[546]" "e[548]" "e[550]" "e[552]" "e[554]" "e[556]" "e[558]" "e[560]" "e[562]" "e[564]" "e[566]" "e[568]" "e[570]" "e[572]" "e[574]" "e[576]" "e[578:579]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak26";
	rename -uid "A10F9659-4634-F22B-E575-C3AD5A49E042";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[242:281]" -type "float3"  0 2.26655221 0 0 2.26655221
		 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221
		 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221
		 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221 0 0 2.26655221
		 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0
		 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0
		 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221
		 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0 0 -2.26655221 0;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "25C5FD30-41F5-5F47-2F59-1E91FDAD8B0B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[461]" "e[481:499]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".wt" 0.47160691022872925;
	setAttr ".re" 499;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "69F18829-4E58-D9E6-C673-17AEBB56B955";
	setAttr ".ics" -type "componentList" 2 "f[200:239]" "f[280:299]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 66.666664 3.8146973e-006 33.333328 ;
	setAttr ".rs" 55757;
	setAttr ".lt" -type "double3" 1.0404871408908889e-014 6.987466161234579e-015 0.42305260484478791 ;
	setAttr ".ls" -type "double3" 0.86666666895625877 0.86666666895625877 0.86666666895625877 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 45.777773539225265 -20.000001907348636 11.267140706380207 ;
	setAttr ".cbx" -type "double3" 87.555555979410812 20.000009536743168 55.399518330891937 ;
createNode polyTweak -n "polyTweak27";
	rename -uid "237A9CA8-4514-0A66-B7FA-928BAACA870F";
	setAttr ".uopa" yes;
	setAttr -s 120 ".tk";
	setAttr ".tk[80]" -type "float3" 1.7777778 4.0120344 0 ;
	setAttr ".tk[81]" -type "float3" 1.6907673 4.0120344 5.9604645e-008 ;
	setAttr ".tk[82]" -type "float3" 1.4382529 4.0120344 -1.1920929e-007 ;
	setAttr ".tk[83]" -type "float3" 1.0449518 4.0120344 0 ;
	setAttr ".tk[84]" -type "float3" 0.54936373 4.0120344 0 ;
	setAttr ".tk[85]" -type "float3" -1.6954215e-007 4.0120344 0 ;
	setAttr ".tk[86]" -type "float3" -0.54936397 4.0120344 2.3841858e-007 ;
	setAttr ".tk[87]" -type "float3" -1.0449526 4.0120344 -2.3841858e-007 ;
	setAttr ".tk[88]" -type "float3" -1.4382536 4.0120344 -1.1920929e-007 ;
	setAttr ".tk[89]" -type "float3" -1.6907682 4.0120344 0 ;
	setAttr ".tk[90]" -type "float3" -1.7777778 4.0120344 0 ;
	setAttr ".tk[91]" -type "float3" -1.6907674 4.0120344 -5.9604645e-008 ;
	setAttr ".tk[92]" -type "float3" -1.4382522 4.0120344 0 ;
	setAttr ".tk[93]" -type "float3" -1.0449518 4.0120344 2.3841858e-007 ;
	setAttr ".tk[94]" -type "float3" -0.54936337 4.0120344 0 ;
	setAttr ".tk[95]" -type "float3" -1.6954215e-007 4.0120344 0 ;
	setAttr ".tk[96]" -type "float3" 0.54936337 4.0120344 -2.3841858e-007 ;
	setAttr ".tk[97]" -type "float3" 1.0449517 4.0120344 2.3841858e-007 ;
	setAttr ".tk[98]" -type "float3" 1.4382529 4.0120344 0 ;
	setAttr ".tk[99]" -type "float3" 1.6907673 4.0120344 5.9604645e-008 ;
	setAttr ".tk[100]" -type "float3" 1.7777778 -4.0120344 0 ;
	setAttr ".tk[101]" -type "float3" 1.6907673 -4.0120344 5.9604645e-008 ;
	setAttr ".tk[102]" -type "float3" 1.4382529 -4.0120344 -1.1920929e-007 ;
	setAttr ".tk[103]" -type "float3" 1.0449518 -4.0120344 0 ;
	setAttr ".tk[104]" -type "float3" 0.54936373 -4.0120344 0 ;
	setAttr ".tk[105]" -type "float3" -1.6954215e-007 -4.0120344 0 ;
	setAttr ".tk[106]" -type "float3" -0.54936397 -4.0120344 2.3841858e-007 ;
	setAttr ".tk[107]" -type "float3" -1.0449526 -4.0120344 -2.3841858e-007 ;
	setAttr ".tk[108]" -type "float3" -1.4382536 -4.0120344 -1.1920929e-007 ;
	setAttr ".tk[109]" -type "float3" -1.6907682 -4.0120344 0 ;
	setAttr ".tk[110]" -type "float3" -1.7777778 -4.0120344 0 ;
	setAttr ".tk[111]" -type "float3" -1.6907674 -4.0120344 -5.9604645e-008 ;
	setAttr ".tk[112]" -type "float3" -1.4382522 -4.0120344 0 ;
	setAttr ".tk[113]" -type "float3" -1.0449518 -4.0120344 2.3841858e-007 ;
	setAttr ".tk[114]" -type "float3" -0.54936337 -4.0120344 0 ;
	setAttr ".tk[115]" -type "float3" -1.6954215e-007 -4.0120344 0 ;
	setAttr ".tk[116]" -type "float3" 0.54936337 -4.0120344 -2.3841858e-007 ;
	setAttr ".tk[117]" -type "float3" 1.0449517 -4.0120344 2.3841858e-007 ;
	setAttr ".tk[118]" -type "float3" 1.4382529 -4.0120344 0 ;
	setAttr ".tk[119]" -type "float3" 1.6907673 -4.0120344 5.9604645e-008 ;
	setAttr ".tk[202]" -type "float3" -8.4771045e-008 -3.0907521 0 ;
	setAttr ".tk[203]" -type "float3" -0.27468169 -3.0907521 0 ;
	setAttr ".tk[204]" -type "float3" -0.52247596 -3.0907521 2.3841858e-007 ;
	setAttr ".tk[205]" -type "float3" -0.71912622 -3.0907521 0 ;
	setAttr ".tk[206]" -type "float3" -0.84538388 -3.0907521 -5.9604645e-008 ;
	setAttr ".tk[207]" -type "float3" -0.88888901 -3.0907521 0 ;
	setAttr ".tk[208]" -type "float3" -0.84538418 -3.0907521 0 ;
	setAttr ".tk[209]" -type "float3" -0.7191267 -3.0907521 -1.1920929e-007 ;
	setAttr ".tk[210]" -type "float3" -0.52247626 -3.0907521 -2.3841858e-007 ;
	setAttr ".tk[211]" -type "float3" -0.27468204 -3.0907521 2.3841858e-007 ;
	setAttr ".tk[212]" -type "float3" -8.4771045e-008 -3.0907521 0 ;
	setAttr ".tk[213]" -type "float3" 0.27468187 -3.0907521 0 ;
	setAttr ".tk[214]" -type "float3" 0.52247596 -3.0907521 0 ;
	setAttr ".tk[215]" -type "float3" 0.71912628 -3.0907521 -1.1920929e-007 ;
	setAttr ".tk[216]" -type "float3" 0.84538358 -3.0907521 5.9604645e-008 ;
	setAttr ".tk[217]" -type "float3" 0.88888901 -3.0907521 0 ;
	setAttr ".tk[218]" -type "float3" 0.84538358 -3.0907521 5.9604645e-008 ;
	setAttr ".tk[219]" -type "float3" 0.71912628 -3.0907521 0 ;
	setAttr ".tk[220]" -type "float3" 0.52247572 -3.0907521 2.3841858e-007 ;
	setAttr ".tk[221]" -type "float3" 0.27468169 -3.0907521 -2.3841858e-007 ;
	setAttr ".tk[222]" -type "float3" -8.4771045e-008 3.0907531 0 ;
	setAttr ".tk[223]" -type "float3" -0.27468169 3.0907531 0 ;
	setAttr ".tk[224]" -type "float3" -0.52247596 3.0907531 2.3841858e-007 ;
	setAttr ".tk[225]" -type "float3" -0.71912622 3.0907531 0 ;
	setAttr ".tk[226]" -type "float3" -0.84538388 3.0907531 -5.9604645e-008 ;
	setAttr ".tk[227]" -type "float3" -0.88888901 3.0907531 0 ;
	setAttr ".tk[228]" -type "float3" -0.84538418 3.0907531 0 ;
	setAttr ".tk[229]" -type "float3" -0.7191267 3.0907531 -1.1920929e-007 ;
	setAttr ".tk[230]" -type "float3" -0.52247626 3.0907531 -2.3841858e-007 ;
	setAttr ".tk[231]" -type "float3" -0.27468204 3.0907531 2.3841858e-007 ;
	setAttr ".tk[232]" -type "float3" -8.4771045e-008 3.0907531 0 ;
	setAttr ".tk[233]" -type "float3" 0.27468187 3.0907531 0 ;
	setAttr ".tk[234]" -type "float3" 0.52247596 3.0907531 0 ;
	setAttr ".tk[235]" -type "float3" 0.71912628 3.0907531 -1.1920929e-007 ;
	setAttr ".tk[236]" -type "float3" 0.84538358 3.0907531 5.9604645e-008 ;
	setAttr ".tk[237]" -type "float3" 0.88888901 3.0907531 0 ;
	setAttr ".tk[238]" -type "float3" 0.84538358 3.0907531 5.9604645e-008 ;
	setAttr ".tk[239]" -type "float3" 0.71912628 3.0907531 0 ;
	setAttr ".tk[240]" -type "float3" 0.52247572 3.0907531 2.3841858e-007 ;
	setAttr ".tk[241]" -type "float3" 0.27468169 3.0907531 -2.3841858e-007 ;
	setAttr ".tk[242]" -type "float3" 0.27468169 1.5453762 0 ;
	setAttr ".tk[243]" -type "float3" 0.52247584 1.5453762 2.3841858e-007 ;
	setAttr ".tk[244]" -type "float3" 0.71912593 1.5453762 0 ;
	setAttr ".tk[245]" -type "float3" 0.84538364 1.5453762 -5.9604645e-008 ;
	setAttr ".tk[246]" -type "float3" 0.88888913 1.5453762 0 ;
	setAttr ".tk[247]" -type "float3" 0.84538424 1.5453762 0 ;
	setAttr ".tk[248]" -type "float3" 0.71912724 1.5453762 -1.1920929e-007 ;
	setAttr ".tk[249]" -type "float3" 0.52247643 1.5453762 -2.3841858e-007 ;
	setAttr ".tk[250]" -type "float3" 0.27468193 1.5453762 2.3841858e-007 ;
	setAttr ".tk[251]" -type "float3" 8.4771102e-008 1.5453762 0 ;
	setAttr ".tk[252]" -type "float3" -0.27468187 1.5453762 0 ;
	setAttr ".tk[253]" -type "float3" -0.52247584 1.5453762 0 ;
	setAttr ".tk[254]" -type "float3" -0.71912658 1.5453762 -1.1920929e-007 ;
	setAttr ".tk[255]" -type "float3" -0.84538376 1.5453762 5.9604645e-008 ;
	setAttr ".tk[256]" -type "float3" -0.88888913 1.5453762 0 ;
	setAttr ".tk[257]" -type "float3" -0.84538376 1.5453762 5.9604645e-008 ;
	setAttr ".tk[258]" -type "float3" -0.71912658 1.5453762 0 ;
	setAttr ".tk[259]" -type "float3" -0.52247596 1.5453762 2.3841858e-007 ;
	setAttr ".tk[260]" -type "float3" -0.27468169 1.5453762 -2.3841858e-007 ;
	setAttr ".tk[261]" -type "float3" 8.4771102e-008 1.5453762 0 ;
	setAttr ".tk[262]" -type "float3" 0.27468169 -1.5453762 0 ;
	setAttr ".tk[263]" -type "float3" 0.52247584 -1.5453762 2.3841858e-007 ;
	setAttr ".tk[264]" -type "float3" 0.71912593 -1.5453762 0 ;
	setAttr ".tk[265]" -type "float3" 0.84538364 -1.5453762 -5.9604645e-008 ;
	setAttr ".tk[266]" -type "float3" 0.88888913 -1.5453762 0 ;
	setAttr ".tk[267]" -type "float3" 0.84538424 -1.5453762 0 ;
	setAttr ".tk[268]" -type "float3" 0.71912724 -1.5453762 -1.1920929e-007 ;
	setAttr ".tk[269]" -type "float3" 0.52247643 -1.5453762 -2.3841858e-007 ;
	setAttr ".tk[270]" -type "float3" 0.27468193 -1.5453762 2.3841858e-007 ;
	setAttr ".tk[271]" -type "float3" 8.4771102e-008 -1.5453762 0 ;
	setAttr ".tk[272]" -type "float3" -0.27468187 -1.5453762 0 ;
	setAttr ".tk[273]" -type "float3" -0.52247584 -1.5453762 0 ;
	setAttr ".tk[274]" -type "float3" -0.71912658 -1.5453762 -1.1920929e-007 ;
	setAttr ".tk[275]" -type "float3" -0.84538376 -1.5453762 5.9604645e-008 ;
	setAttr ".tk[276]" -type "float3" -0.88888913 -1.5453762 0 ;
	setAttr ".tk[277]" -type "float3" -0.84538376 -1.5453762 5.9604645e-008 ;
	setAttr ".tk[278]" -type "float3" -0.71912658 -1.5453762 0 ;
	setAttr ".tk[279]" -type "float3" -0.52247596 -1.5453762 2.3841858e-007 ;
	setAttr ".tk[280]" -type "float3" -0.27468169 -1.5453762 -2.3841858e-007 ;
	setAttr ".tk[281]" -type "float3" 8.4771102e-008 -1.5453762 0 ;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "DE937CBC-4A67-31C9-7330-189CB1D40F3D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 21 "e[100:101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]" "e[113]" "e[115]" "e[117]" "e[119]" "e[121]" "e[123]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]" "e[140:199]" "e[400:819]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "FA21AB80-4DB9-CE92-B778-A6A317B7B191";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 156 "e[142]" "e[144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[162]" "e[164]" "e[166]" "e[168]" "e[170]" "e[172]" "e[174]" "e[176]" "e[178:179]" "e[420]" "e[422:440]" "e[462]" "e[464]" "e[466]" "e[468]" "e[470]" "e[472]" "e[474]" "e[476]" "e[478]" "e[480]" "e[482]" "e[484]" "e[486]" "e[488]" "e[490]" "e[492]" "e[494]" "e[496]" "e[498:519]" "e[522]" "e[526]" "e[529]" "e[532]" "e[534]" "e[537]" "e[539]" "e[542]" "e[544]" "e[547]" "e[549]" "e[552]" "e[554]" "e[557]" "e[559]" "e[562]" "e[564]" "e[567]" "e[569]" "e[572]" "e[574]" "e[577]" "e[579]" "e[582]" "e[584]" "e[587]" "e[589]" "e[592]" "e[594]" "e[597]" "e[599]" "e[602]" "e[604]" "e[607]" "e[609]" "e[612]" "e[614]" "e[617:619]" "e[622]" "e[626]" "e[629]" "e[631]" "e[634]" "e[636]" "e[639]" "e[641]" "e[644]" "e[646]" "e[649]" "e[651]" "e[654]" "e[656]" "e[659]" "e[661]" "e[664]" "e[666]" "e[669]" "e[671]" "e[674]" "e[676]" "e[679]" "e[681]" "e[684]" "e[686]" "e[689]" "e[691]" "e[694]" "e[696]" "e[699]" "e[701]" "e[704]" "e[706]" "e[709]" "e[711]" "e[714]" "e[716]" "e[718:719]" "e[724]" "e[727]" "e[729]" "e[731]" "e[734]" "e[736]" "e[739]" "e[741]" "e[744]" "e[746]" "e[749]" "e[751]" "e[754]" "e[756]" "e[759]" "e[761]" "e[764]" "e[766]" "e[769]" "e[771]" "e[774]" "e[776]" "e[779]" "e[781]" "e[784]" "e[786]" "e[789]" "e[791]" "e[794]" "e[796]" "e[799]" "e[801]" "e[804]" "e[806]" "e[809]" "e[811]" "e[814]" "e[816]" "e[818:819]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0
		 66.666666666666671 0 33.333333333333336 1;
	setAttr ".a" 0;
createNode polyCylinder -n "polyCylinder2";
	rename -uid "E1B5C98A-4296-4298-1DB0-A48D3A7FD5F2";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "96CC02D2-46F1-2318-DB96-CC820959B42A";
	setAttr ".ics" -type "componentList" 1 "f[20:39]";
	setAttr ".ix" -type "matrix" 2.4284985619715957 0 0 0 0 1 0 0 0 0 2.4284985619715957 0
		 66.666664123535156 -22.457215523514002 24.334165454384095 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 66.666664 -23.457216 24.334166 ;
	setAttr ".rs" 56806;
	setAttr ".lt" -type "double3" 0 -8.4734471337515323e-017 0.38161013354106998 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 64.238164982564385 -23.457215523514002 21.905665734414146 ;
	setAttr ".cbx" -type "double3" 69.09516268550675 -23.457215523514002 26.762664305855278 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "5C94093F-47E0-E22C-4436-D3AE68FE07B7";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[20]" -type "float3" -0.13585833 0 0.044143043 ;
	setAttr ".tk[21]" -type "float3" -0.11556807 0 0.083965063 ;
	setAttr ".tk[22]" -type "float3" -0.083964914 0 0.11556833 ;
	setAttr ".tk[23]" -type "float3" -0.044143073 0 0.13585842 ;
	setAttr ".tk[24]" -type "float3" -1.7029009e-008 0 0.14284986 ;
	setAttr ".tk[25]" -type "float3" 0.044143073 0 0.13585836 ;
	setAttr ".tk[26]" -type "float3" 0.083965033 0 0.11556815 ;
	setAttr ".tk[27]" -type "float3" 0.11556815 0 0.083965026 ;
	setAttr ".tk[28]" -type "float3" 0.13585836 0 0.044143006 ;
	setAttr ".tk[29]" -type "float3" 0.14285013 0 -2.5543535e-008 ;
	setAttr ".tk[30]" -type "float3" 0.13585836 0 -0.044143058 ;
	setAttr ".tk[31]" -type "float3" 0.11556809 0 -0.083965085 ;
	setAttr ".tk[32]" -type "float3" 0.083965018 0 -0.11556833 ;
	setAttr ".tk[33]" -type "float3" 0.044143036 0 -0.13585842 ;
	setAttr ".tk[34]" -type "float3" -1.2771788e-008 0 -0.14284986 ;
	setAttr ".tk[35]" -type "float3" -0.044142991 0 -0.13585836 ;
	setAttr ".tk[36]" -type "float3" -0.083965033 0 -0.11556827 ;
	setAttr ".tk[37]" -type "float3" -0.11556815 0 -0.083965093 ;
	setAttr ".tk[38]" -type "float3" -0.13585836 0 -0.044143081 ;
	setAttr ".tk[39]" -type "float3" -0.14285013 0 -2.5543535e-008 ;
	setAttr ".tk[41]" -type "float3" -1.7029009e-008 0 -2.5543535e-008 ;
createNode polyCylinder -n "polyCylinder3";
	rename -uid "7F94E715-4FAE-51F0-A185-3EA9307F51AB";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "759FF826-48EB-7DF8-DEA7-7AB4ED941CB6";
	setAttr ".ics" -type "componentList" 1 "f[40:59]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -66.666664 1.0484741e-006 19.901466 ;
	setAttr ".rs" 62773;
	setAttr ".ls" -type "double3" 0.73333332356055658 0.73333332356055658 0.73333332356055658 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -79.850690258573891 -13.184023591907225 19.90146687324712 ;
	setAttr ".cbx" -type "double3" -53.482645171707759 13.184025688855536 19.901466873247127 ;
createNode polyTweak -n "polyTweak29";
	rename -uid "D05AC32B-4265-7000-04CD-A99918124952";
	setAttr ".uopa" yes;
	setAttr -s 42 ".tk[20:41]" -type "float3"  0.47457331 0 -0.15419798 0.40369594
		 0 -0.29330236 0.29330221 0 -0.40369576 0.15419817 0 -0.47457311 5.9484844e-008 0
		 -0.4989953 -0.15419799 0 -0.47457311 -0.2933023 0 -0.4036957 -0.40369573 0 -0.29330218
		 -0.47457296 0 -0.15419786 -0.49899527 0 8.9227299e-008 -0.47457296 0 0.15419817 -0.40369552
		 0 0.29330239 -0.29330218 0 0.40369576 -0.15419798 0 0.47457305 4.4613657e-008 0 0.49899536
		 0.15419805 0 0.47457305 0.2933023 0 0.40369576 0.40369573 0 0.29330236 0.47457296
		 0 0.15419814 0.49899527 0 8.9227299e-008 0 0 0 5.9484844e-008 0 8.9227299e-008;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "850A0B61-4658-C5D3-6E18-62AD67941976";
	setAttr ".ics" -type "componentList" 1 "f[40:59]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -66.666664 1.5727112e-006 19.901468 ;
	setAttr ".rs" 35344;
	setAttr ".lt" -type "double3" 0 1.3322676295501878e-015 -17.849060030123855 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -78.281772404555426 -11.615107834837065 19.9014680594669 ;
	setAttr ".cbx" -type "double3" -55.051556734881295 11.615110980259532 19.901468059466907 ;
createNode polyTweak -n "polyTweak30";
	rename -uid "08D99EF1-4446-EC01-C0D6-B78A48747C64";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk[61:81]" -type "float3"  -0.39109117 3.1153171e-008
		 0.12707321 -0.33268225 3.1153171e-008 0.24170756 -1.4847939e-007 3.1153171e-008 -6.4903759e-008
		 -0.24170752 3.1153171e-008 0.33268225 -0.12707306 3.1153171e-008 0.39109111 -1.4847939e-007
		 3.1153171e-008 0.41121781 0.12707271 3.1153171e-008 0.39109111 0.24170797 3.1153171e-008
		 0.33268222 0.33268225 3.1153171e-008 0.24170752 0.39109078 3.1153171e-008 0.12707321
		 0.41121775 -1.1092606e-008 -6.8245612e-008 0.39109078 -3.1153171e-008 -0.12707324
		 0.3326824 -3.1153171e-008 -0.24170758 0.24170797 -3.1153171e-008 -0.33268225 0.12707324
		 -1.1092606e-008 -0.39109111 -1.4847939e-007 3.1153171e-008 -0.41121781 -0.12707318
		 -1.1092606e-008 -0.39109111 -0.2417074 -3.1153171e-008 -0.33268225 -0.33268234 -3.1153171e-008
		 -0.24170758 -0.39109117 -1.1092606e-008 -0.12707324 -0.41121775 3.1153171e-008 -5.9804755e-008;
createNode polySplit -n "polySplit20";
	rename -uid "A2D30FB0-43BE-AB38-23A3-59B36A47D4F8";
	setAttr -s 21 ".e[0:20]"  0.93260598 0.93260598 0.93260598 0.93260598
		 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598
		 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598 0.93260598
		 0.93260598;
	setAttr -s 21 ".d[0:20]"  -2147483608 -2147483607 -2147483606 -2147483605 -2147483604 -2147483603 
		-2147483602 -2147483601 -2147483600 -2147483599 -2147483598 -2147483597 -2147483596 -2147483595 -2147483594 -2147483593 -2147483592 -2147483591 
		-2147483590 -2147483589 -2147483608;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "CEABDAA6-4F5C-D673-051A-A2AC5D2FC06C";
	setAttr ".ics" -type "componentList" 1 "f[0:19]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".s" -type "double3" 1.0448522977020382 1.0448522977020382 1.0237759635608286 ;
	setAttr ".pvt" -type "float3" -66.666656 2.0969483e-006 19.230848 ;
	setAttr ".rs" 59958;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -79.850675579935711 -13.184021494958914 18.560228333991862 ;
	setAttr ".cbx" -type "double3" -53.482636783914515 13.184025688855536 19.901468059466907 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "146F4A70-4704-4176-A81E-82A02E8B5A86";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:19]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.099999999999999978;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak31";
	rename -uid "011419ED-4A0E-DBDC-6D65-82A9BACCFBA7";
	setAttr ".uopa" yes;
	setAttr -s 62 ".tk";
	setAttr ".tk[20]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[21]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[22]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[23]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[29]" -type "float3" 0 0.33444566 8.4018138e-017 ;
	setAttr ".tk[30]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[32]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[33]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[36]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[37]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.33444566 8.4018138e-017 ;
	setAttr ".tk[41]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[44]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[45]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[48]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.33444566 8.4018138e-017 ;
	setAttr ".tk[51]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[56]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.33444566 8.4018138e-017 ;
	setAttr ".tk[104]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[105]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[107]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[109]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[111]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[113]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[115]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[117]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[119]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[121]" -type "float3" 0 0.33444566 8.4018138e-017 ;
	setAttr ".tk[123]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[125]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[127]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[129]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[131]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[133]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[135]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[137]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[139]" -type "float3" 0 0.33444566 0 ;
	setAttr ".tk[141]" -type "float3" 0 0.33444566 8.4018138e-017 ;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "810B3545-4B2E-B852-5CCB-B782339AD900";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "e[142]" "e[149]" "e[154]" "e[159]" "e[164]" "e[169]" "e[174]" "e[179]" "e[184]" "e[189]" "e[194]" "e[199]" "e[204]" "e[209]" "e[214]" "e[219]" "e[224]" "e[229]" "e[234]" "e[238]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "9D08A730-47D4-7D62-B6E7-F68BCCD99821";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[142]" "e[144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[162]" "e[164]" "e[166]" "e[168]" "e[170]" "e[172]" "e[174]" "e[176]" "e[178:179]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel4";
	rename -uid "EB9CD466-4748-B121-429E-3296A4FA358A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[22]" "e[24]" "e[26]" "e[28]" "e[30]" "e[32]" "e[34]" "e[36]" "e[38]" "e[40]" "e[42]" "e[44]" "e[46]" "e[48]" "e[50]" "e[52]" "e[54]" "e[56]" "e[58:59]";
	setAttr ".ix" -type "matrix" 8.7952386900952995 0 0 0 0 2.2095065187195897e-015 9.9507327343782261 0
		 0 -8.7952386900952995 1.9529353001635606e-015 0 -66.666666666666671 -1.3432068482213348e-015 9.9507341388688975 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.25;
	setAttr ".sg" 3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyPlatonicSolid -n "polyPlatonicSolid1";
	rename -uid "2E774AEB-4C70-3094-BD05-F8BBD18A43E1";
	setAttr ".l" 0.71369999647140503;
	setAttr ".cuv" 4;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "94A0DC7D-496A-B072-1CCB-61B9FC7D7CC5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 17.750326768686048 0 0 0 0 17.750326768686048 0 0 0 0 17.750326768686048 0
		 -48.612238922870375 63.100869111049747 0 1;
	setAttr ".a" 0;
createNode animCurveTL -n "pSolid4_translateX";
	rename -uid "44824308-414F-8076-75DE-9E9C3CA40767";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 -17.963963403772439;
createNode animCurveTL -n "pSolid4_translateY";
	rename -uid "93C029C2-433F-1746-8EBD-6690608F6164";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 -54.919093406553991;
createNode animCurveTL -n "pSolid4_translateZ";
	rename -uid "32D8E004-4C2B-699B-851E-918EFFA1B01D";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 0.82767768474474224;
createNode animCurveTU -n "pSolid4_visibility";
	rename -uid "5196E7FC-4CAC-B6B8-B244-68BB4079F984";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  -1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pSolid4_rotateX";
	rename -uid "C5A4D555-4237-DAFD-8F90-9392B47EC93B";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 71.017605141488659;
createNode animCurveTA -n "pSolid4_rotateY";
	rename -uid "45AEF0CF-41F9-C095-FEF1-76A57E141E1A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 -47.214782201164539;
createNode animCurveTA -n "pSolid4_rotateZ";
	rename -uid "968B04EB-4224-68B6-488A-7A8319176F28";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 25.576562964166911;
createNode animCurveTU -n "pSolid4_scaleX";
	rename -uid "9B808F6F-4C3B-7C37-FB50-B2B6747D5B90";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 16.439165081028111;
createNode animCurveTU -n "pSolid4_scaleY";
	rename -uid "2941A238-4B05-2DE4-256E-FAA800B6F947";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 12.990494991919265;
createNode animCurveTU -n "pSolid4_scaleZ";
	rename -uid "87A67A83-4B6D-CEC8-2E42-F8943B206450";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  -1 18.404530199948024;
createNode polySphere -n "polySphere1";
	rename -uid "0C7A16C3-40FE-8F61-0A26-D8AC1AFB187D";
createNode polyCylinder -n "polyCylinder4";
	rename -uid "A693E5EC-44F4-1E40-44E2-A5BE62C4A8F8";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "9F0EE299-4344-E662-3F62-40B700171415";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -35.124383575035026 0 88.307041944406393 1;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "B813E8A4-4EE5-E926-8780-769B2B6138DF";
	setAttr ".txf" -type "matrix" 1.6906294073882779 0 0.86462884285541364 0 0.011396506457971261 -0.027486673177833511 -0.022283861009895291 0
		 -0.20087042958797374 -0.40170669648287383 0.39276674395296979 0 -33.084947095643024 -7.1830754303298766e-008 89.250379200314697 1;
	setAttr ".rn" yes;
createNode polyUnite -n "polyUnite3";
	rename -uid "C8B49935-45D8-F474-7708-36A14C928240";
	setAttr -s 3 ".ip";
	setAttr -s 3 ".im";
createNode groupId -n "groupId24";
	rename -uid "6619E072-468D-FAB4-CC4C-F2995735A181";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "EF89839A-43B0-2C62-BE43-D3AF79696DFC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:59]";
createNode groupId -n "groupId25";
	rename -uid "74DA0E45-4A19-2EDB-9709-7DB7DA368DED";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "9F198E95-47F2-FB40-B4E1-D0BBFF7877BE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "7601B7B1-4286-4AC5-4836-59BE5F1BB1F0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "260B563B-41CE-C65B-96E1-23BE8A58DF79";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "D357B91C-4271-E363-0254-3D887FF374C8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:399]";
createNode groupId -n "groupId29";
	rename -uid "5ECE9EC1-4BD2-13DD-ADE5-C391755865D4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId30";
	rename -uid "55BE731B-4AF7-2EE4-B433-1889ECBD65A3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "3C73412E-4642-CE78-54FC-93B02B3296EE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:519]";
createNode groupId -n "groupId31";
	rename -uid "CF26981B-41ED-BC58-8ABB-0EAF74DB32AB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId32";
	rename -uid "2A7DDBCE-4C6A-978F-47B2-CEBEF20CE12E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "90E007A6-4AED-63FA-E551-5E8F9E18DCF4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId34";
	rename -uid "7F3FDE3D-44A8-4C19-498F-E4AC81B508EB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "40EF0BE0-48A1-0AF2-49FE-B191E1AB7707";
	setAttr ".ihi" 0;
createNode polyPlane -n "polyPlane4";
	rename -uid "842F6ECB-407D-15A5-4591-C990F42DE0D7";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode polySplit -n "polySplit21";
	rename -uid "21E4E5FD-47AC-B726-7A2F-3280A5904A9F";
	setAttr -s 2 ".e[0:1]"  0.23097 0.23097;
	setAttr -s 2 ".d[0:1]"  -2147483647 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak32";
	rename -uid "290DDD01-4E28-B569-0CB3-36AC9896A157";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk";
	setAttr ".tk[4]" -type "float3" 0 7.9014478 0.18057118 ;
	setAttr ".tk[5]" -type "float3" 0 7.9014478 0.18057118 ;
createNode polySplit -n "polySplit22";
	rename -uid "90551D46-4E97-B3A9-702F-6BA6B394A827";
	setAttr -s 2 ".e[0:1]"  0.28521001 0.28521001;
	setAttr -s 2 ".d[0:1]"  -2147483644 -2147483643;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "AAA619E5-4479-2EA6-404C-2F8F8CF6181E";
	setAttr ".ics" -type "componentList" 1 "f[0:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 51.15779181143504 0 0 -66.666666666666671 12.79490592154734 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -65.081268 12.090837 ;
	setAttr ".rs" 61618;
	setAttr ".lt" -type "double3" 0 -2.0262957978189888e-013 2.4112041194543381 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.88739371299743652 -68.439424196879074 -12.78398998417018 ;
	setAttr ".cbx" -type "double3" 0.88739371299743652 -61.723108927408859 36.9656649170213 ;
createNode polyTweak -n "polyTweak33";
	rename -uid "82A2D791-4049-589E-1A67-3AA90E85C4C6";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" -0.02043407 -1.1978668 -0.027525375 ;
	setAttr ".tk[1]" -type "float3" -0.02043407 -1.1978668 -0.027525375 ;
	setAttr ".tk[4]" -type "float3" -0.23289664 -2.9578903 0 ;
	setAttr ".tk[5]" -type "float3" 0.23289664 -2.9578903 0 ;
	setAttr ".tk[6]" -type "float3" -0.38739374 -7.4206333 0 ;
	setAttr ".tk[7]" -type "float3" 0.38739374 -7.4206333 0 ;
createNode polyPlane -n "polyPlane5";
	rename -uid "B7782738-41D1-67A5-49DD-61958FBC6954";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "55205DD2-49C3-FFDC-FC4F-BE9DD20CE28C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -68.636948916087817 38.113624572753906 1;
	setAttr ".wt" 0.36716470122337341;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "F3A8D193-4021-9EB8-3D24-0E9D852A0FB6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[0]" "e[3]" "e[6]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -68.636948916087817 38.113624572753906 1;
	setAttr ".wt" 0.39323562383651733;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak34";
	rename -uid "42764B9E-43BF-7563-BF4B-9DB664E62A60";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[0]" -type "float3" 0 -0.8610236 0 ;
	setAttr ".tk[1]" -type "float3" 0 -0.8610236 0 ;
	setAttr ".tk[2]" -type "float3" 0 -0.8610236 0 ;
	setAttr ".tk[3]" -type "float3" 0 -0.8610236 0 ;
	setAttr ".tk[4]" -type "float3" -0.25561455 -0.8610236 0 ;
	setAttr ".tk[5]" -type "float3" 0.25561455 -0.8610236 0 ;
	setAttr ".tk[6]" -type "float3" 0 -0.8610236 0.25561455 ;
	setAttr ".tk[8]" -type "float3" 0 -0.8610236 -0.25561455 ;
createNode deleteComponent -n "deleteComponent9";
	rename -uid "36E40B21-4E7D-BA85-C599-FABB81699AD9";
	setAttr ".dc" -type "componentList" 0;
createNode deleteComponent -n "deleteComponent10";
	rename -uid "FDCEEB0D-4C30-B2C3-94CC-44930140394D";
	setAttr ".dc" -type "componentList" 1 "vtx[3]";
createNode deleteComponent -n "deleteComponent11";
	rename -uid "87A155C3-42A7-52F8-1447-8BA8A5BD6923";
	setAttr ".dc" -type "componentList" 1 "vtx[1]";
createNode deleteComponent -n "deleteComponent12";
	rename -uid "05F3BC12-4C46-982B-6E22-379C248C4A85";
	setAttr ".dc" -type "componentList" 1 "vtx[0]";
createNode deleteComponent -n "deleteComponent13";
	rename -uid "2475D307-4BB7-3398-F216-01962D82C556";
	setAttr ".dc" -type "componentList" 1 "vtx[0]";
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "C48589F1-420C-63EC-49B9-0182830F1778";
	setAttr ".ics" -type "componentList" 1 "f[0:3]";
	setAttr ".ix" -type "matrix" 1.0619562408045629 0 -0 0 -0 0.63938796191249003 -0.84789981220964461 0
		 0 2.4140942178891591 1.8204306211819665 0 0 -64.132578750102383 39.059223936821311 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -66.017731 41.559147 ;
	setAttr ".rs" 52989;
	setAttr ".lt" -type "double3" -2.7061686225238191e-016 -1.5265566588595902e-015 
		0.5134740320388016 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.3125640529957148 -67.666895822564044 37.539257202153856 ;
	setAttr ".cbx" -type "double3" 1.3125640529957148 -64.368564812323768 45.579033506047359 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "40E06EA3-44DB-BC98-B455-8B94CE604BC6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[0:2]" "e[6]";
	setAttr ".ix" -type "matrix" 2.5244213279315177 0 0 0 0 2.5244213279315177 0 0 0 0 2.5244213279315177 0
		 2.0851477918970573e-005 -65.322763731947632 37.795613108943478 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 -0.74221674510550883 0 ;
	setAttr ".pvt" -type "float3" 2.0851478e-005 -68.238586 37.795612 ;
	setAttr ".rs" 57193;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.1201309611318537 -67.496350083933535 34.675461296333708 ;
	setAttr ".cbx" -type "double3" 3.1201726640876917 -67.496350083933535 40.915764921553247 ;
createNode polyTweak -n "polyTweak35";
	rename -uid "DD470A38-48BC-33D1-9006-DFBCCE72476F";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk[0:4]" -type "float3"  -0.48037246 0 1.1292964e-015
		 0.48037246 0 1.1292964e-015 0 0 0.48037246 0 0 0 0 0 -0.48037246;
createNode polyExtrudeEdge -n "polyExtrudeEdge4";
	rename -uid "843A7054-4FFA-E899-12EF-8EA3158C3873";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[0:2]" "e[6]";
	setAttr ".ix" -type "matrix" -4.5384921227277202 5.558049851053979e-016 0 0 -5.558049851053979e-016 -4.5384921227277202 0 0
		 0 0 4.5384921227277202 0 0 -71.744645329388504 37.764892889361008 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 0.49594760940735227 0 ;
	setAttr ".s" -type "double3" 0.26666668754237094 0.26666668754237094 0.26666668754237094 ;
	setAttr ".pvt" -type "float3" 8.8817842e-016 -67.34095 37.764893 ;
	setAttr ".rs" 44623;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.6095170273526627 -67.836896481204093 32.155375862008341 ;
	setAttr ".cbx" -type "double3" 5.6095170273526644 -67.836896481204093 43.374409916713674 ;
createNode polyPlane -n "polyPlane6";
	rename -uid "A24DBF2E-41D4-FB42-EE36-E5ADFE1F1147";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode displayLayer -n "layer1";
	rename -uid "AB448CEE-4C12-BA28-C19E-FD902E1D347F";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode polySplit -n "polySplit23";
	rename -uid "31CD9BB7-452E-E907-CB95-4CA334313BF8";
	setAttr -s 2 ".e[0:1]"  0.461588 0.461588;
	setAttr -s 2 ".d[0:1]"  -2147483648 -2147483645;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit24";
	rename -uid "1374447C-42E6-92D8-75BE-4CB0524CEA14";
	setAttr -s 3 ".e[0:2]"  0.41704401 0.41704401 0.41704401;
	setAttr -s 3 ".d[0:2]"  -2147483647 -2147483642 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode displayLayer -n "layer2";
	rename -uid "8CDE7EED-4EDB-165D-E289-509095C976F1";
	setAttr ".do" 2;
createNode polyPlane -n "polyPlane3";
	rename -uid "12A01B24-4F01-7D85-2F85-A4A1E2E75AF0";
	setAttr ".cuv" 2;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "15B19910-469F-1B90-C584-A1BFABFFE4A3";
	setAttr ".ics" -type "componentList" 2 "e[6]" "e[8]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "BE604668-4840-76A0-B1CC-0C8E53131446";
	setAttr ".ics" -type "componentList" 1 "e[6]";
	setAttr ".cv" yes;
createNode displayLayer -n "layer3";
	rename -uid "44BC57C9-4DB9-1623-E1D5-E8809E421927";
	setAttr ".do" 3;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "F6178594-4CD6-3B99-E73D-4298F6874F36";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".wt" 0.62779563665390015;
	setAttr ".dr" no;
	setAttr ".re" 2;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "A077F578-4DA6-F37E-720C-E681EB49D271";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[0]" "e[3]" "e[6]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".wt" 0.42211905121803284;
	setAttr ".re" 3;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "926BE267-4709-2E34-DDA1-9A9CD5048868";
	setAttr ".ics" -type "componentList" 2 "e[9]" "e[11]";
	setAttr ".cv" yes;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "6B84D234-49A4-DF68-BCAF-13815B1B4507";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[0]" "e[5:6]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".wt" 0.60531759262084961;
	setAttr ".dr" no;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak36";
	rename -uid "6F567286-4EB4-99E8-390D-CCB8214CBEBD";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[0]" -type "float3" 4.4703484e-008 -1.323489e-023 5.9604645e-008 ;
	setAttr ".tk[4]" -type "float3" 0 -5.0651247e-016 2.2811294 ;
	setAttr ".tk[5]" -type "float3" 0 5.9604645e-008 -0.51561397 ;
	setAttr ".tk[6]" -type "float3" 8.9406967e-008 0 0 ;
	setAttr ".tk[7]" -type "float3" 7.1054274e-015 0 0 ;
	setAttr ".tk[8]" -type "float3" 2.9802322e-008 0 0 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge5";
	rename -uid "E2EE006E-4B6D-6AB2-DF73-94A79A4286EF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[12]" "e[15]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 4.9388149e-007 -0.97016287 3.5214159 ;
	setAttr ".rs" 51257;
	setAttr ".lt" -type "double3" 1.697877542771245e-018 14.210272552824316 -2.8213079286264642e-017 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -16.571910808701144 -6.4941336363567448 3.5214159176521882 ;
	setAttr ".cbx" -type "double3" 16.571911796464061 4.5538078905402664 3.5214159176521882 ;
createNode polyTweak -n "polyTweak37";
	rename -uid "A79DD2C7-4F30-9064-D52A-FB92DA4CE512";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" -2.9802322e-008 0 0 ;
	setAttr ".tk[1]" -type "float3" 2.9802322e-008 0 0 ;
	setAttr ".tk[6]" -type "float3" -2.9802322e-008 0 0 ;
	setAttr ".tk[7]" -type "float3" 0 9.2511324e-017 -0.41663396 ;
	setAttr ".tk[8]" -type "float3" 2.9802322e-008 0 0 ;
	setAttr ".tk[10]" -type "float3" 0 2.9951188e-016 -1.3488816 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge6";
	rename -uid "A9B6E749-4416-32FD-E9E6-C5B1934B06B6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[2:3]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 9.8776286e-007 -17.542074 3.5214159 ;
	setAttr ".rs" 45983;
	setAttr ".lt" -type "double3" 4.3368256306009628e-016 47.661603699109875 3.5943504303557333e-016 ;
	setAttr ".ls" -type "double3" 1 1.8431459293730483 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -16.571908833175314 -17.542075163253756 3.5214159176521918 ;
	setAttr ".cbx" -type "double3" 16.571910808701144 -17.542073187727926 3.5214159176522086 ;
createNode polyTweak -n "polyTweak38";
	rename -uid "094959AA-4944-16F9-4ECD-758F1A69A7FD";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk";
	setAttr ".tk[4]" -type "float3" 0 5.0651241e-016 -2.2811291 ;
createNode polyTweak -n "polyTweak39";
	rename -uid "2AF23C2D-4B79-8245-3E78-209BD813B89D";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" 0.25794366 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.25794366 0 0 ;
	setAttr ".tk[4]" -type "float3" 1.5374646e-008 0 0 ;
	setAttr ".tk[16]" -type "float3" 0.36652046 0 0 ;
	setAttr ".tk[17]" -type "float3" 2.6722581e-008 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.36652046 0 0 ;
createNode polySplit -n "polySplit25";
	rename -uid "FD1EF65F-4B1E-9A4A-40E4-D6A6A20A3104";
	setAttr -s 3 ".e[0:2]"  0.38114801 0.38114801 0.38114801;
	setAttr -s 3 ".d[0:2]"  -2147483625 -2147483624 -2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak40";
	rename -uid "CB63B1B8-46AB-4CC6-72CE-C5A1A95FE375";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" 0.17973399 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.17973399 0 0 ;
	setAttr ".tk[4]" -type "float3" 1.0712982e-008 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.26064229 1.3076675e-016 -0.58892089 ;
	setAttr ".tk[20]" -type "float3" -1.0862108e-008 1.3076675e-016 -0.58892089 ;
	setAttr ".tk[21]" -type "float3" 0.26064229 1.3076675e-016 -0.58892089 ;
createNode polySplit -n "polySplit26";
	rename -uid "F87B4586-4086-7C4A-87EB-649C0894F89E";
	setAttr -s 3 ".e[0:2]"  0.36166301 0.36166301 0.36166301;
	setAttr -s 3 ".d[0:2]"  -2147483620 -2147483619 -2147483618;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak41";
	rename -uid "BD75368F-4E14-B464-A2BA-35B5BF7E09C2";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[22:24]" -type "float3"  0.31591219 0 0 8.4159826e-009
		 0 0 -0.31591219 0 0;
createNode polySplit -n "polySplit27";
	rename -uid "EF3F68DE-47C8-BE69-F55D-30AE826892CB";
	setAttr -s 3 ".e[0:2]"  0.70751899 0.70751899 0.70751899;
	setAttr -s 3 ".d[0:2]"  -2147483620 -2147483619 -2147483618;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak42";
	rename -uid "DD26C118-4DE4-4F94-84F8-FFA993EECB16";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[0]" -type "float3" 0.062321607 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.062321607 0 0 ;
	setAttr ".tk[4]" -type "float3" 3.7146592e-009 0 0 ;
	setAttr ".tk[19]" -type "float3" 0.13002317 -3.9794859e-017 0.17922013 ;
	setAttr ".tk[20]" -type "float3" 5.4186344e-009 -3.9794859e-017 0.17922013 ;
	setAttr ".tk[21]" -type "float3" -0.13002317 -3.9794859e-017 0.17922013 ;
	setAttr ".tk[22]" -type "float3" -0.19243138 0 0 ;
	setAttr ".tk[23]" -type "float3" -5.163121e-009 0 0 ;
	setAttr ".tk[24]" -type "float3" 0.19243138 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.15391232 0 0 ;
	setAttr ".tk[26]" -type "float3" 6.7654935e-009 0 0 ;
	setAttr ".tk[27]" -type "float3" -0.15391231 0 0 ;
createNode polySplit -n "polySplit28";
	rename -uid "7E63BFA5-4FEE-0415-D365-7FACE9C3BDB0";
	setAttr -s 3 ".e[0:2]"  0.392746 0.392746 0.392746;
	setAttr -s 3 ".d[0:2]"  -2147483615 -2147483614 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "E04EF94B-4AB8-1A70-293B-C9AADC6E6E8F";
	setAttr -s 3 ".e[0:2]"  0.35634601 0.35634601 0.35634601;
	setAttr -s 3 ".d[0:2]"  -2147483605 -2147483604 -2147483603;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "81974B93-40F6-D842-8C31-7C8145D92FB4";
	setAttr -s 3 ".e[0:2]"  0.48901799 0.48901799 0.48901799;
	setAttr -s 3 ".d[0:2]"  -2147483600 -2147483599 -2147483598;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "00E2CE7A-4746-CB4D-D16A-4B98EFAB6C9F";
	setAttr -s 3 ".e[0:2]"  0.477305 0.477305 0.477305;
	setAttr -s 3 ".d[0:2]"  -2147483605 -2147483604 -2147483603;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit32";
	rename -uid "3B3AFEBD-489B-3663-14F0-459D37B04A18";
	setAttr -s 3 ".e[0:2]"  0.49056301 0.49056301 0.49056301;
	setAttr -s 3 ".d[0:2]"  -2147483615 -2147483614 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak43";
	rename -uid "F22F03F6-4879-8094-1B07-5490DFC187B4";
	setAttr ".uopa" yes;
	setAttr -s 25 ".tk";
	setAttr ".tk[16]" -type "float3" 0.077082314 0 0 ;
	setAttr ".tk[17]" -type "float3" 5.6199823e-009 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.077082314 0 0 ;
	setAttr ".tk[22]" -type "float3" -0.31202412 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.31202412 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.36544272 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.20166139 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.20166139 0 0 ;
	setAttr ".tk[27]" -type "float3" 0.20166139 0 0 ;
	setAttr ".tk[28]" -type "float3" -0.028764199 0 0 ;
	setAttr ".tk[29]" -type "float3" -1.8893542e-009 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.028764199 0 0 ;
	setAttr ".tk[31]" -type "float3" -0.31049818 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.31049818 0 0 ;
	setAttr ".tk[33]" -type "float3" -0.38812828 1.4901161e-008 0 ;
	setAttr ".tk[34]" -type "float3" 0.15061256 0 0 ;
	setAttr ".tk[35]" -type "float3" -1.1910791e-008 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.15061261 0 0 ;
	setAttr ".tk[37]" -type "float3" 0.43996423 -1.339885e-017 -2.220446e-016 ;
	setAttr ".tk[38]" -type "float3" 0.26325062 -1.339885e-017 -2.220446e-016 ;
	setAttr ".tk[39]" -type "float3" 0.086537011 -1.339885e-017 -2.220446e-016 ;
	setAttr ".tk[40]" -type "float3" 0.20273933 0 0 ;
	setAttr ".tk[41]" -type "float3" 1.2912968e-008 0 0 ;
	setAttr ".tk[42]" -type "float3" -0.20273933 0 0 ;
createNode polySplit -n "polySplit33";
	rename -uid "9902AF47-4A92-79D4-492C-07959550CCC3";
	setAttr -s 3 ".e[0:2]"  0.54501402 0.54501402 0.54501402;
	setAttr -s 3 ".d[0:2]"  -2147483590 -2147483589 -2147483588;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak44";
	rename -uid "F3F49A29-4230-4F90-152D-B288CAA906D0";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk";
	setAttr ".tk[28]" -type "float3" 0.16386476 -1.339885e-017 0 ;
	setAttr ".tk[29]" -type "float3" 0.019608393 0 0 ;
	setAttr ".tk[30]" -type "float3" -0.068979442 0 0 ;
	setAttr ".tk[31]" -type "float3" 0.10888806 0 0 ;
	setAttr ".tk[37]" -type "float3" -0.15799809 -1.535635e-017 -0.051527314 ;
	setAttr ".tk[38]" -type "float3" -0.15799809 -1.535635e-017 -0.051527314 ;
	setAttr ".tk[39]" -type "float3" -0.15799809 -1.535635e-017 -0.051527314 ;
	setAttr ".tk[40]" -type "float3" -0.13086285 -2.3496079e-017 0.10581682 ;
	setAttr ".tk[41]" -type "float3" 0 -2.3496079e-017 0.10581682 ;
	setAttr ".tk[42]" -type "float3" 0 -2.3496079e-017 0.10581682 ;
	setAttr ".tk[45]" -type "float3" 0.040381208 -5.3595414e-017 0 ;
createNode polySplit -n "polySplit34";
	rename -uid "528C1ACF-4DE0-83FA-0B1C-8697BAEE7DE9";
	setAttr -s 3 ".e[0:2]"  0.59010398 0.59010398 0.59010398;
	setAttr -s 3 ".d[0:2]"  -2147483615 -2147483614 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge7";
	rename -uid "0EFB98B0-4CE0-3999-3184-2FA9E205C54B";
	setAttr ".ics" -type "componentList" 1 "e[66:67]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak45";
	rename -uid "57EB2592-4F80-5AB7-C507-46A801FD1A30";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[24]" -type "float3" 0.051738884 -0.0026922342 0.017607776 ;
	setAttr ".tk[40]" -type "float3" -0.051004123 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.051004123 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.051004123 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.073465146 0.0080529721 -0.044539869 ;
	setAttr ".tk[47]" -type "float3" -0.049559265 0 0 ;
	setAttr ".tk[48]" -type "float3" -0.11754006 0 0 ;
createNode polyDelEdge -n "polyDelEdge8";
	rename -uid "047B9103-4BE2-272D-E986-509F1B60DDB2";
	setAttr ".ics" -type "componentList" 1 "e[67:68]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge9";
	rename -uid "4EBDD0C9-4DFD-0CF1-4FB0-E19069674696";
	setAttr ".ics" -type "componentList" 11 "e[6]" "e[8]" "e[13]" "e[24]" "e[29]" "e[34]" "e[39]" "e[44]" "e[49]" "e[54]" "e[63:64]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak46";
	rename -uid "542F5B79-4A2E-C8F2-AA9C-4F833D1C809C";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[31]" -type "float3" 0.22326262 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.22326262 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.22326262 0 0 ;
createNode polySplit -n "polySplit35";
	rename -uid "547732D7-4AD4-CE84-0CA4-6B978CB16495";
	setAttr -s 2 ".e[0:1]"  0.54992503 0.54992503;
	setAttr -s 2 ".d[0:1]"  -2147483623 -2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit36";
	rename -uid "F85AB631-4DFF-E92E-3C15-1795D1C0392E";
	setAttr -s 2 ".e[0:1]"  0.35706499 0.35706499;
	setAttr -s 2 ".d[0:1]"  -2147483605 -2147483604;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge10";
	rename -uid "DC0DD822-4B42-8523-3E6B-17912B177F96";
	setAttr ".ics" -type "componentList" 1 "e[48]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak47";
	rename -uid "6153D7F8-4FAF-5202-B581-12AB67A73D00";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[30]" -type "float3" -0.064740956 0 0 ;
	setAttr ".tk[31]" -type "float3" 0.18929628 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.1300599 0 0 ;
	setAttr ".tk[33]" -type "float3" -0.18037775 0 0 ;
createNode polyTweak -n "polyTweak48";
	rename -uid "C100A5E1-4137-1F43-FF07-479C7CFDB31E";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[14]" -type "float3" 0 -2.5838872e-018 0.011636847 ;
	setAttr ".tk[15]" -type "float3" 0 -2.5838872e-018 0.011636847 ;
	setAttr ".tk[18]" -type "float3" -0.12005956 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.12005956 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.092517182 0.0071486896 0.003458295 ;
	setAttr ".tk[23]" -type "float3" 0.020293605 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.040387031 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.020293605 0 0 ;
	setAttr ".tk[26]" -type "float3" -0.069238208 0 0 ;
	setAttr ".tk[27]" -type "float3" 0.020293605 0 0 ;
	setAttr ".tk[28]" -type "float3" 0 -1.2143064e-017 0.052168101 ;
createNode polySplit -n "polySplit37";
	rename -uid "20E8C974-4D5E-404D-8C4B-B984FCDE5FF6";
	setAttr -s 14 ".e[0:13]"  0.52466202 0.47533801 0.47533801 0.52466202
		 0.52466202 0.52466202 0.52466202 0.52466202 0.52466202 0.52466202 0.52466202 0.52466202
		 0.52466202 0.52466202;
	setAttr -s 14 ".d[0:13]"  -2147483647 -2147483642 -2147483639 -2147483646 -2147483627 -2147483621 
		-2147483604 -2147483624 -2147483606 -2147483618 -2147483610 -2147483615 -2147483612 -2147483630;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak49";
	rename -uid "5D59D0ED-4E97-D56C-D7AA-28BC54CC91DF";
	setAttr ".uopa" yes;
	setAttr ".tk[32]" -type "float3"  0 1.4491237e-016 -0.65262729;
createNode polySplit -n "polySplit38";
	rename -uid "2B1689DA-4616-B38B-8183-D6943D2CEFBE";
	setAttr -s 5 ".e[0:4]"  0.47503799 0.47503799 0.52496201 0.47503799
		 0.47503799;
	setAttr -s 5 ".d[0:4]"  -2147483633 -2147483640 -2147483587 -2147483641 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit39";
	rename -uid "FB9A93D7-4B82-4444-8306-6B92B467A901";
	setAttr -s 15 ".e[0:14]"  0.590101 0.590101 0.590101 0.590101 0.590101
		 0.590101 0.590101 0.590101 0.590101 0.590101 0.590101 0.409899 0.590101 0.409899
		 0.590101;
	setAttr -s 15 ".d[0:14]"  -2147483589 -2147483590 -2147483591 -2147483592 -2147483593 -2147483594 
		-2147483595 -2147483596 -2147483597 -2147483598 -2147483599 -2147483639 -2147483568 -2147483642 -2147483602;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit40";
	rename -uid "D81D94DF-40EB-3142-62A5-8A8262191C4B";
	setAttr -s 15 ".e[0:14]"  0.282157 0.717843 0.282157 0.717843 0.282157
		 0.282157 0.282157 0.282157 0.282157 0.282157 0.282157 0.282157 0.282157 0.282157
		 0.282157;
	setAttr -s 15 ".d[0:14]"  -2147483647 -2147483601 -2147483569 -2147483600 -2147483646 -2147483627 
		-2147483621 -2147483604 -2147483624 -2147483606 -2147483618 -2147483610 -2147483615 -2147483612 -2147483630;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak50";
	rename -uid "2EE4EC3B-40A7-1217-CE3E-4A9B965A36E1";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[8]" -type "float3" -0.21443656 0 0 ;
	setAttr ".tk[9]" -type "float3" -0.21443659 0 0 ;
	setAttr ".tk[10]" -type "float3" 0.21443656 0 0 ;
	setAttr ".tk[11]" -type "float3" 0.21443656 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.022703031 0 0 ;
	setAttr ".tk[50]" -type "float3" -0.02270302 0 0 ;
	setAttr ".tk[65]" -type "float3" 0 -7.3276695e-017 0.39035192 ;
	setAttr ".tk[66]" -type "float3" 0 -7.3276695e-017 0.39035192 ;
createNode polySplit -n "polySplit41";
	rename -uid "D5B5343B-4F33-65C4-662B-C7963973822F";
	setAttr -s 5 ".e[0:4]"  0.57810497 0.421895 0.421895 0.57810497 0.57810497;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483519 -2147483586 -2147483541 -2147483645;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit42";
	rename -uid "018EB86B-456C-2FB6-247E-7A8ACA506A85";
	setAttr -s 5 ".e[0:4]"  0.45835599 0.54164398 0.54164398 0.45835599
		 0.45835599;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483507 -2147483506 -2147483541 -2147483645;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak51";
	rename -uid "B5EA5DA9-4A0A-3F5F-63C7-F0A9469B607C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[8]" -type "float3" -0.041849371 0 0 ;
	setAttr ".tk[9]" -type "float3" -0.041849371 0 0 ;
	setAttr ".tk[81]" -type "float3" -0.19270574 -5.2381388e-017 0.2359048 ;
	setAttr ".tk[85]" -type "float3" 0.34172523 -7.6258621e-017 0.3434383 ;
createNode polySplit -n "polySplit43";
	rename -uid "3293D5C9-4625-EE2F-231F-0787E0E3525D";
	setAttr -s 5 ".e[0:4]"  0.59500599 0.59500599 0.40499401 0.40499401
		 0.59500599;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483538 -2147483588 -2147483522 -2147483643;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak52";
	rename -uid "F10CCFE6-43E3-A994-700B-4E96CC607A55";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[91]" -type "float3" 0.37907726 2.7451977e-017 -0.12363271 ;
	setAttr ".tk[95]" -type "float3" -0.34997529 2.7451977e-017 -0.12363271 ;
createNode deleteComponent -n "deleteComponent14";
	rename -uid "7056064A-4234-4267-B6DA-2086933C0CCF";
	setAttr ".dc" -type "componentList" 1 "vtx[2]";
createNode polyTweak -n "polyTweak53";
	rename -uid "77734FE5-4E50-6EB4-4DF1-B6948A0A13C3";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[65]" -type "float3" 0.055705179 -0.077571623 -0.15076794 ;
createNode deleteComponent -n "deleteComponent15";
	rename -uid "7EE26809-468A-1C20-CBA3-19A2B280DA72";
	setAttr ".dc" -type "componentList" 1 "vtx[2]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "B8A524B1-4077-B88F-AFC2-13B2FD79FFE9";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 1\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 1\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n"
		+ "            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 1\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n"
		+ "            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 1\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n"
		+ "            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 1\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n"
		+ "            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 1\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n"
		+ "            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 560\n                -height 396\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 560\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 559\n                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n"
		+ "            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 559\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n"
		+ "            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 560\n"
		+ "                -height 395\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n"
		+ "            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n"
		+ "            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n"
		+ "            -shadows 0\n            -captureSequenceNumber -1\n            -width 560\n            -height 395\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n"
		+ "                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n"
		+ "                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n"
		+ "                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1126\n                -height 836\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1126\n            -height 836\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n"
		+ "                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n"
		+ "                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n"
		+ "            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n"
		+ "\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1126\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1126\\n    -height 836\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 10000 -divisions 3 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "DDEC42B7-469B-C722-5651-61A17C22C0E4";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyTweak -n "polyTweak54";
	rename -uid "ACE540C0-4D69-CFF6-B119-C7A00EE34CAA";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[63]" -type "float3" -0.040782422 -1.5481749e-017 0.069723599 ;
	setAttr ".tk[64]" -type "float3" 0 0.077571638 0 ;
	setAttr ".tk[89]" -type "float3" -0.24649332 2.5250635e-018 -0.011371875 ;
createNode deleteComponent -n "deleteComponent16";
	rename -uid "FC315954-414D-061D-9432-F1B330ABB9DB";
	setAttr ".dc" -type "componentList" 1 "vtx[6]";
createNode deleteComponent -n "deleteComponent17";
	rename -uid "F3BB6312-43CB-9978-6353-C6B6CAE4B335";
	setAttr ".dc" -type "componentList" 1 "vtx[6]";
createNode deleteComponent -n "deleteComponent18";
	rename -uid "74074AE6-4181-4E1E-7CE3-BCB82381ECCB";
	setAttr ".dc" -type "componentList" 1 "vtx[6]";
createNode deleteComponent -n "deleteComponent19";
	rename -uid "72A7FF1F-4B52-1D8A-00E5-43AF986A4015";
	setAttr ".dc" -type "componentList" 1 "vtx[6]";
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "C1AFA51F-46FD-6E9A-4037-D1B92671D54A";
	setAttr ".ics" -type "componentList" 1 "f[0:71]";
	setAttr ".ix" -type "matrix" 33.143823592928122 0 0 0 0 7.3594072153966565e-015 33.143823592928122 0
		 0 -33.143823592928122 7.3594072153966565e-015 0 0 -0.97016139126386536 3.5214159176521882 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.9755257e-006 -13.985684 3.6102526 ;
	setAttr ".rs" 37313;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -30.029713325670773 -65.203680523571563 3.4321840858784682 ;
	setAttr ".cbx" -type "double3" 30.029717276722437 37.232312178005223 3.7883212489420641 ;
createNode polyNormal -n "polyNormal2";
	rename -uid "3D66E09B-43AB-BC2D-5A3B-3286A6F71A5F";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".unm" no;
createNode polyTweak -n "polyTweak55";
	rename -uid "F389D7EE-449E-60B8-B594-65B389AB3C82";
	setAttr ".uopa" yes;
	setAttr -s 95 ".tk";
	setAttr ".tk[90]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[91]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[92]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[94]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[95]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[96]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[97]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[98]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[99]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[100]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[101]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[102]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[103]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[104]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[105]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[106]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[107]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[108]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[109]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[110]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[111]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[112]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[113]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[114]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[115]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[116]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[117]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[118]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[119]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[120]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[121]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[122]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[123]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[124]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[125]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[126]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[127]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[128]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[129]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[130]" -type "float3" 0 -0.049497765 -1.7347235e-018 ;
	setAttr ".tk[131]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[132]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[133]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[134]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[135]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[136]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[137]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[138]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[139]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[140]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[141]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[142]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[143]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[144]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[145]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[146]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[147]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[148]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[149]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[150]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[151]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[152]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[153]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[154]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[155]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[156]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[157]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[158]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[159]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[160]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[161]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[162]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[163]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[164]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[165]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[166]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[167]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[168]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[169]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[170]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[171]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[172]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[173]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[174]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[175]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[176]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[177]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[178]" -type "float3" 0 -0.049497765 0 ;
	setAttr ".tk[179]" -type "float3" 0 -0.049497765 0 ;
select -ne :time1;
	setAttr ".o" -1;
	setAttr ".unw" -1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 61 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 20 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupParts4.og" "|Cactus|Cactus2|transform3|Cactus2Shape.i";
connectAttr "groupId4.id" "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[0].gid"
		;
connectAttr "set1.mwc" "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[0].gco";
connectAttr "groupId5.id" "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[1].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[1].gco"
		;
connectAttr "groupId1.id" "CactusShape.iog.og[1].gid";
connectAttr "set1.mwc" "CactusShape.iog.og[1].gco";
connectAttr "groupId2.id" "CactusShape.iog.og[3].gid";
connectAttr ":initialShadingGroup.mwc" "CactusShape.iog.og[3].gco";
connectAttr "groupParts2.og" "CactusShape.i";
connectAttr "groupId3.id" "CactusShape.ciog.cog[1].cgid";
connectAttr "groupParts6.og" "Cactus4Shape.i";
connectAttr "groupId6.id" "Cactus4Shape.iog.og[0].gid";
connectAttr "set1.mwc" "Cactus4Shape.iog.og[0].gco";
connectAttr "groupId7.id" "Cactus4Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "Cactus4Shape.iog.og[1].gco";
connectAttr "groupParts12.og" "|Cactus2|Cactus6|transform6|Cactus6Shape.i";
connectAttr "groupId11.id" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[0].gid"
		;
connectAttr "set1.mwc" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[0].gco";
connectAttr "groupId12.id" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[1].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[1].gco"
		;
connectAttr "groupId13.id" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[2].gid"
		;
connectAttr "set2.mwc" "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[2].gco";
connectAttr "polyMirror2.out" "|Cactus2|transform4|Cactus2Shape.i";
connectAttr "groupId8.id" "|Cactus2|transform4|Cactus2Shape.iog.og[0].gid";
connectAttr "set1.mwc" "|Cactus2|transform4|Cactus2Shape.iog.og[0].gco";
connectAttr "groupId9.id" "|Cactus2|transform4|Cactus2Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "|Cactus2|transform4|Cactus2Shape.iog.og[1].gco"
		;
connectAttr "groupId10.id" "|Cactus2|transform4|Cactus2Shape.iog.og[2].gid";
connectAttr "set2.mwc" "|Cactus2|transform4|Cactus2Shape.iog.og[2].gco";
connectAttr "groupParts15.og" "Cactus8Shape.i";
connectAttr "groupId14.id" "Cactus8Shape.iog.og[0].gid";
connectAttr "set1.mwc" "Cactus8Shape.iog.og[0].gco";
connectAttr "groupId15.id" "Cactus8Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "Cactus8Shape.iog.og[1].gco";
connectAttr "groupId16.id" "Cactus8Shape.iog.og[2].gid";
connectAttr "set2.mwc" "Cactus8Shape.iog.og[2].gco";
connectAttr "layer1.di" "|Cactus6.do";
connectAttr "polySplit19.out" "|Cactus6|Cactus6Shape.i";
connectAttr "groupId17.id" "|Cactus6|Cactus6Shape.iog.og[0].gid";
connectAttr "set1.mwc" "|Cactus6|Cactus6Shape.iog.og[0].gco";
connectAttr "groupId18.id" "|Cactus6|Cactus6Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "|Cactus6|Cactus6Shape.iog.og[1].gco";
connectAttr "groupId19.id" "|Cactus6|Cactus6Shape.iog.og[2].gid";
connectAttr "set2.mwc" "|Cactus6|Cactus6Shape.iog.og[2].gco";
connectAttr "groupId20.id" "|Cactus6|Cactus6Shape.iog.og[4].gid";
connectAttr "set3.mwc" "|Cactus6|Cactus6Shape.iog.og[4].gco";
connectAttr "groupId21.id" "|Cactus6|Cactus6Shape.iog.og[5].gid";
connectAttr "set4.mwc" "|Cactus6|Cactus6Shape.iog.og[5].gco";
connectAttr "groupId22.id" "|Cactus6|Cactus6Shape.iog.og[6].gid";
connectAttr "set5.mwc" "|Cactus6|Cactus6Shape.iog.og[6].gco";
connectAttr "groupId23.id" "|Cactus6|Cactus6Shape.iog.og[9].gid";
connectAttr "set6.mwc" "|Cactus6|Cactus6Shape.iog.og[9].gco";
connectAttr "polyTweakUV2.uvtk[0]" "|Cactus6|Cactus6Shape.uvst[0].uvtw";
connectAttr "layer2.di" "pPlane1.do";
connectAttr "polyPlane3.out" "pPlaneShape1.i";
connectAttr "polySoftEdge3.out" "pCylinderShape1.i";
connectAttr "polyExtrudeFace6.out" "pCylinderShape2.i";
connectAttr "polyBevel4.out" "pCylinderShape3.i";
connectAttr "polySoftEdge4.out" "pSolidShape1.i";
connectAttr "pSolid4_translateX.o" "pSolid4.tx";
connectAttr "pSolid4_translateY.o" "pSolid4.ty";
connectAttr "pSolid4_translateZ.o" "pSolid4.tz";
connectAttr "pSolid4_visibility.o" "pSolid4.v";
connectAttr "pSolid4_rotateX.o" "pSolid4.rx";
connectAttr "pSolid4_rotateY.o" "pSolid4.ry";
connectAttr "pSolid4_rotateZ.o" "pSolid4.rz";
connectAttr "pSolid4_scaleX.o" "pSolid4.sx";
connectAttr "pSolid4_scaleY.o" "pSolid4.sy";
connectAttr "pSolid4_scaleZ.o" "pSolid4.sz";
connectAttr "groupId28.id" "pSphereShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pSphereShape1.iog.og[0].gco";
connectAttr "groupParts24.og" "pSphereShape1.i";
connectAttr "groupId29.id" "pSphereShape1.ciog.cog[0].cgid";
connectAttr "groupId26.id" "pCylinderShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape6.iog.og[0].gco";
connectAttr "groupId27.id" "pCylinderShape6.ciog.cog[0].cgid";
connectAttr "groupId24.id" "pCylinderShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape5.iog.og[0].gco";
connectAttr "groupParts23.og" "pCylinderShape5.i";
connectAttr "groupId25.id" "pCylinderShape5.ciog.cog[0].cgid";
connectAttr "groupParts25.og" "pCylinder5Shape.i";
connectAttr "groupId30.id" "pCylinder5Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder5Shape.iog.og[0].gco";
connectAttr "groupId31.id" "pCylinder7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder7Shape.iog.og[0].gco";
connectAttr "groupId32.id" "pCylinder8Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder8Shape.iog.og[0].gco";
connectAttr "groupId33.id" "pCylinder9Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder9Shape.iog.og[0].gco";
connectAttr "groupId34.id" "pCylinder11Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder11Shape.iog.og[0].gco";
connectAttr "groupId35.id" "pCylinder12Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder12Shape.iog.og[0].gco";
connectAttr "layer3.di" "pCylinder13.do";
connectAttr "layer3.di" "pPlane2.do";
connectAttr "polyExtrudeFace10.out" "pPlaneShape2.i";
connectAttr "layer3.di" "pPlane3.do";
connectAttr "polyExtrudeEdge3.out" "pPlaneShape3.i";
connectAttr "layer3.di" "pPlane4.do";
connectAttr "polyExtrudeEdge4.out" "pPlaneShape4.i";
connectAttr "layer3.di" "pPlane6.do";
connectAttr "polyExtrudeFace11.out" "pPlaneShape6.i";
connectAttr "layer3.di" "pPlane7.do";
connectAttr "layer3.di" "pPlane8.do";
connectAttr "layer3.di" "pPlane10.do";
connectAttr "layer3.di" "pPlane11.do";
connectAttr "layer3.di" "pPlane12.do";
connectAttr "layer3.di" "pPlane13.do";
connectAttr "layer3.di" "pPlane14.do";
connectAttr "layer3.di" "pPlane15.do";
connectAttr "layer3.di" "pPlane16.do";
connectAttr "layer3.di" "pPlane17.do";
connectAttr "layer3.di" "pPlane18.do";
connectAttr "layer3.di" "pPlane19.do";
connectAttr "layer3.di" "pPlane20.do";
connectAttr "layer3.di" "pPlane21.do";
connectAttr "layer3.di" "pPlane22.do";
connectAttr "layer3.di" "pPlane23.do";
connectAttr "layer3.di" "pPlane24.do";
connectAttr "layer3.di" "pPlane25.do";
connectAttr "layer3.di" "pPlane26.do";
connectAttr "layer3.di" "pPlane27.do";
connectAttr "polyNormal2.out" "pPlaneShape28.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "groupId4.msg" "set1.gn" -na;
connectAttr "groupId6.msg" "set1.gn" -na;
connectAttr "groupId8.msg" "set1.gn" -na;
connectAttr "groupId11.msg" "set1.gn" -na;
connectAttr "groupId14.msg" "set1.gn" -na;
connectAttr "groupId17.msg" "set1.gn" -na;
connectAttr "CactusShape.iog.og[1]" "set1.dsm" -na;
connectAttr "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "Cactus4Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "|Cactus2|transform4|Cactus2Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "Cactus8Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "polyPlane2.out" "polyNormal1.ip";
connectAttr "polyNormal1.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polyExtrudeEdge1.ip";
connectAttr "CactusShape.wm" "polyExtrudeEdge1.mp";
connectAttr "polyExtrudeEdge1.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "polySplit8.ip";
connectAttr "groupParts1.og" "polyTweak5.ip";
connectAttr "polyTweak5.out" "deleteComponent1.ig";
connectAttr "polySplit8.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "deleteComponent1.og" "polyMirror1.ip";
connectAttr "Cactus.sp" "polyMirror1.sp";
connectAttr "CactusShape.wm" "polyMirror1.mp";
connectAttr "CactusShape.o" "polySeparate1.ip";
connectAttr "polyMirror1.fnf" "polySeparate1.sf";
connectAttr "polyMirror1.lnf" "polySeparate1.ef";
connectAttr "polyMirror1.out" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "polySeparate1.out[0]" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "groupParts3.og" "groupParts4.ig";
connectAttr "groupId5.id" "groupParts4.gi";
connectAttr "polySeparate1.out[1]" "groupParts5.ig";
connectAttr "groupId6.id" "groupParts5.gi";
connectAttr "groupParts5.og" "groupParts6.ig";
connectAttr "groupId7.id" "groupParts6.gi";
connectAttr "|Cactus|Cactus2|transform3|Cactus2Shape.o" "polyUnite1.ip[0]";
connectAttr "Cactus4Shape.o" "polyUnite1.ip[1]";
connectAttr "|Cactus|Cactus2|transform3|Cactus2Shape.wm" "polyUnite1.im[0]";
connectAttr "Cactus4Shape.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts7.ig";
connectAttr "groupId8.id" "groupParts7.gi";
connectAttr "groupParts7.og" "groupParts8.ig";
connectAttr "groupId9.id" "groupParts8.gi";
connectAttr "polyTweak6.out" "polyMergeVert1.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert1.mp";
connectAttr "groupParts8.og" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyMergeVert2.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polyMergeVert3.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert2.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyMergeVert4.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert3.out" "polyTweak9.ip";
connectAttr "polyMergeVert4.out" "polyMergeVert5.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert5.mp";
connectAttr "polyTweak10.out" "polyMergeVert6.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert6.mp";
connectAttr "polyMergeVert5.out" "polyTweak10.ip";
connectAttr "polyMergeVert6.out" "polyMergeVert7.ip";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMergeVert7.mp";
connectAttr "polyMergeVert7.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polySplit9.ip";
connectAttr "polySplit9.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polyTweak11.ip";
connectAttr "polyTweak11.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polyTweak12.ip";
connectAttr "polyTweak12.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polySplit15.ip";
connectAttr "groupId10.msg" "set2.gn" -na;
connectAttr "groupId13.msg" "set2.gn" -na;
connectAttr "groupId16.msg" "set2.gn" -na;
connectAttr "groupId19.msg" "set2.gn" -na;
connectAttr "|Cactus2|transform4|Cactus2Shape.iog.og[2]" "set2.dsm" -na;
connectAttr "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[2]" "set2.dsm" -na;
connectAttr "Cactus8Shape.iog.og[2]" "set2.dsm" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[2]" "set2.dsm" -na;
connectAttr "polySplit15.out" "groupParts9.ig";
connectAttr "groupId10.id" "groupParts9.gi";
connectAttr "groupParts9.og" "polyTweak13.ip";
connectAttr "polyTweak13.out" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "polyMirror2.ip";
connectAttr "|Cactus2.sp" "polyMirror2.sp";
connectAttr "|Cactus2|transform4|Cactus2Shape.wm" "polyMirror2.mp";
connectAttr "|Cactus2|transform4|Cactus2Shape.o" "polySeparate2.ip";
connectAttr "polyMirror2.fnf" "polySeparate2.sf";
connectAttr "polyMirror2.lnf" "polySeparate2.ef";
connectAttr "polySeparate2.out[0]" "groupParts10.ig";
connectAttr "groupId11.id" "groupParts10.gi";
connectAttr "groupParts10.og" "groupParts11.ig";
connectAttr "groupId12.id" "groupParts11.gi";
connectAttr "groupParts11.og" "groupParts12.ig";
connectAttr "groupId13.id" "groupParts12.gi";
connectAttr "polySeparate2.out[1]" "groupParts13.ig";
connectAttr "groupId14.id" "groupParts13.gi";
connectAttr "groupParts13.og" "groupParts14.ig";
connectAttr "groupId15.id" "groupParts14.gi";
connectAttr "groupParts14.og" "groupParts15.ig";
connectAttr "groupId16.id" "groupParts15.gi";
connectAttr "|Cactus2|Cactus6|transform6|Cactus6Shape.o" "polyUnite2.ip[0]";
connectAttr "Cactus8Shape.o" "polyUnite2.ip[1]";
connectAttr "|Cactus2|Cactus6|transform6|Cactus6Shape.wm" "polyUnite2.im[0]";
connectAttr "Cactus8Shape.wm" "polyUnite2.im[1]";
connectAttr "polyUnite2.out" "groupParts16.ig";
connectAttr "groupId17.id" "groupParts16.gi";
connectAttr "groupParts16.og" "groupParts17.ig";
connectAttr "groupId18.id" "groupParts17.gi";
connectAttr "groupParts17.og" "groupParts18.ig";
connectAttr "groupId19.id" "groupParts18.gi";
connectAttr "groupParts18.og" "polyMergeVert8.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert8.mp";
connectAttr "polyMergeVert8.out" "polyMergeVert9.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert9.mp";
connectAttr "polyMergeVert9.out" "polyMergeVert10.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert10.mp";
connectAttr "polyMergeVert10.out" "polyMergeVert11.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert11.mp";
connectAttr "polyMergeVert11.out" "polyMergeVert12.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert12.mp";
connectAttr "polyMergeVert12.out" "polyMergeVert13.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert13.mp";
connectAttr "polyMergeVert13.out" "polyMergeVert14.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert14.mp";
connectAttr "polyMergeVert14.out" "polyMergeVert15.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert15.mp";
connectAttr "polyMergeVert15.out" "polyMergeVert16.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert16.mp";
connectAttr "polyMergeVert16.out" "polyMergeVert17.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert17.mp";
connectAttr "polyTweak14.out" "polyExtrudeFace1.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyExtrudeFace1.mp";
connectAttr "polyMergeVert17.out" "polyTweak14.ip";
connectAttr "polyExtrudeFace1.out" "polySpinEdge1.ip";
connectAttr "polySpinEdge1.out" "polySpinEdge2.ip";
connectAttr "polySpinEdge2.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polySpinEdge3.ip";
connectAttr "polySpinEdge3.out" "polySpinEdge4.ip";
connectAttr "polySpinEdge4.out" "polyTweakUV1.ip";
connectAttr "polyTweak15.out" "polyMergeVert18.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert18.mp";
connectAttr "polyTweakUV1.out" "polyTweak15.ip";
connectAttr "polyMergeVert18.out" "polyTweakUV2.ip";
connectAttr "polyTweak16.out" "polyMergeVert19.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMergeVert19.mp";
connectAttr "polyTweakUV2.out" "polyTweak16.ip";
connectAttr "groupId20.msg" "set3.gn" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[4]" "set3.dsm" -na;
connectAttr "polyMergeVert19.out" "groupParts19.ig";
connectAttr "groupId20.id" "groupParts19.gi";
connectAttr "groupParts19.og" "deleteComponent4.ig";
connectAttr "groupId21.msg" "set4.gn" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[5]" "set4.dsm" -na;
connectAttr "deleteComponent4.og" "groupParts20.ig";
connectAttr "groupId21.id" "groupParts20.gi";
connectAttr "groupParts20.og" "deleteComponent5.ig";
connectAttr "groupId22.msg" "set5.gn" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[6]" "set5.dsm" -na;
connectAttr "deleteComponent5.og" "groupParts21.ig";
connectAttr "groupId22.id" "groupParts21.gi";
connectAttr "groupParts21.og" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "polyExtrudeEdge2.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polyBridgeEdge1.ip";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyTweak17.ip";
connectAttr "polyTweak17.out" "polySplit16.ip";
connectAttr "groupId23.msg" "set6.gn" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[9]" "set6.dsm" -na;
connectAttr "polySplit16.out" "groupParts22.ig";
connectAttr "groupId23.id" "groupParts22.gi";
connectAttr "groupParts22.og" "polyTweak18.ip";
connectAttr "polyTweak18.out" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "polyMirror3.ip";
connectAttr "|Cactus6.sp" "polyMirror3.sp";
connectAttr "|Cactus6|Cactus6Shape.wm" "polyMirror3.mp";
connectAttr "polyTweak19.out" "polyDelEdge2.ip";
connectAttr "polyMirror3.out" "polyTweak19.ip";
connectAttr "polyDelEdge2.out" "polyTweak20.ip";
connectAttr "polyTweak20.out" "polySplit17.ip";
connectAttr "polySplit17.out" "polySplit18.ip";
connectAttr "polySplit18.out" "polySplit19.ip";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polyTweak21.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing2.out" "polyTweak22.ip";
connectAttr "polyTweak23.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polySplitRing3.out" "polyTweak23.ip";
connectAttr "polyExtrudeFace2.out" "polySoftEdge1.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge1.mp";
connectAttr "polySoftEdge1.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace3.out" "polyExtrudeFace4.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak24.ip";
connectAttr "polyTweak24.out" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polyBridgeEdge2.ip";
connectAttr "pCylinderShape1.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polyTweak25.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak25.ip";
connectAttr "polyTweak26.out" "polyDelEdge3.ip";
connectAttr "polySplitRing5.out" "polyTweak26.ip";
connectAttr "polyDelEdge3.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polyTweak27.out" "polyExtrudeFace5.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polySplitRing6.out" "polyTweak27.ip";
connectAttr "polyExtrudeFace5.out" "polySoftEdge2.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge3.mp";
connectAttr "polyTweak28.out" "polyExtrudeFace6.ip";
connectAttr "pCylinderShape2.wm" "polyExtrudeFace6.mp";
connectAttr "polyCylinder2.out" "polyTweak28.ip";
connectAttr "polyTweak29.out" "polyExtrudeFace7.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace7.mp";
connectAttr "polyCylinder3.out" "polyTweak29.ip";
connectAttr "polyExtrudeFace7.out" "polyExtrudeFace8.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak30.ip";
connectAttr "polyTweak30.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polyExtrudeFace9.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace9.mp";
connectAttr "polyTweak31.out" "polyBevel1.ip";
connectAttr "pCylinderShape3.wm" "polyBevel1.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak31.ip";
connectAttr "polyBevel1.out" "polyBevel2.ip";
connectAttr "pCylinderShape3.wm" "polyBevel2.mp";
connectAttr "polyBevel2.out" "polyBevel3.ip";
connectAttr "pCylinderShape3.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyBevel4.ip";
connectAttr "pCylinderShape3.wm" "polyBevel4.mp";
connectAttr "polyPlatonicSolid1.out" "polySoftEdge4.ip";
connectAttr "pSolidShape1.wm" "polySoftEdge4.mp";
connectAttr "polySphere1.out" "transformGeometry1.ig";
connectAttr "polyCylinder4.out" "transformGeometry2.ig";
connectAttr "pCylinderShape5.o" "polyUnite3.ip[0]";
connectAttr "pCylinderShape6.o" "polyUnite3.ip[1]";
connectAttr "pSphereShape1.o" "polyUnite3.ip[2]";
connectAttr "pCylinderShape5.wm" "polyUnite3.im[0]";
connectAttr "pCylinderShape6.wm" "polyUnite3.im[1]";
connectAttr "pSphereShape1.wm" "polyUnite3.im[2]";
connectAttr "transformGeometry2.og" "groupParts23.ig";
connectAttr "groupId24.id" "groupParts23.gi";
connectAttr "transformGeometry1.og" "groupParts24.ig";
connectAttr "groupId28.id" "groupParts24.gi";
connectAttr "polyUnite3.out" "groupParts25.ig";
connectAttr "groupId30.id" "groupParts25.gi";
connectAttr "polyPlane4.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polyTweak32.ip";
connectAttr "polyTweak32.out" "polySplit22.ip";
connectAttr "polyTweak33.out" "polyExtrudeFace10.ip";
connectAttr "pPlaneShape2.wm" "polyExtrudeFace10.mp";
connectAttr "polySplit22.out" "polyTweak33.ip";
connectAttr "polyPlane5.out" "polySplitRing7.ip";
connectAttr "pPlaneShape3.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pPlaneShape3.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polyTweak34.ip";
connectAttr "polyTweak34.out" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "deleteComponent13.ig";
connectAttr "|pPlane6|polySurfaceShape1.o" "polyExtrudeFace11.ip";
connectAttr "pPlaneShape6.wm" "polyExtrudeFace11.mp";
connectAttr "polyTweak35.out" "polyExtrudeEdge3.ip";
connectAttr "pPlaneShape3.wm" "polyExtrudeEdge3.mp";
connectAttr "deleteComponent13.og" "polyTweak35.ip";
connectAttr "polySurfaceShape2.o" "polyExtrudeEdge4.ip";
connectAttr "pPlaneShape4.wm" "polyExtrudeEdge4.mp";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "polyPlane6.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polySplit24.ip";
connectAttr "layerManager.dli[2]" "layer2.id";
connectAttr "polySplit24.out" "polyDelEdge4.ip";
connectAttr "polyDelEdge4.out" "polyDelEdge5.ip";
connectAttr "layerManager.dli[3]" "layer3.id";
connectAttr "polyDelEdge5.out" "polySplitRing9.ip";
connectAttr "pPlaneShape28.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pPlaneShape28.wm" "polySplitRing10.mp";
connectAttr "polySplitRing10.out" "polyDelEdge6.ip";
connectAttr "polyTweak36.out" "polySplitRing11.ip";
connectAttr "pPlaneShape28.wm" "polySplitRing11.mp";
connectAttr "polyDelEdge6.out" "polyTweak36.ip";
connectAttr "polyTweak37.out" "polyExtrudeEdge5.ip";
connectAttr "pPlaneShape28.wm" "polyExtrudeEdge5.mp";
connectAttr "polySplitRing11.out" "polyTweak37.ip";
connectAttr "polyTweak38.out" "polyExtrudeEdge6.ip";
connectAttr "pPlaneShape28.wm" "polyExtrudeEdge6.mp";
connectAttr "polyExtrudeEdge5.out" "polyTweak38.ip";
connectAttr "polyExtrudeEdge6.out" "polyTweak39.ip";
connectAttr "polyTweak39.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polyTweak40.ip";
connectAttr "polyTweak40.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polyTweak41.ip";
connectAttr "polyTweak41.out" "polySplit27.ip";
connectAttr "polySplit27.out" "polyTweak42.ip";
connectAttr "polyTweak42.out" "polySplit28.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit32.out" "polyTweak43.ip";
connectAttr "polyTweak43.out" "polySplit33.ip";
connectAttr "polySplit33.out" "polyTweak44.ip";
connectAttr "polyTweak44.out" "polySplit34.ip";
connectAttr "polyTweak45.out" "polyDelEdge7.ip";
connectAttr "polySplit34.out" "polyTweak45.ip";
connectAttr "polyDelEdge7.out" "polyDelEdge8.ip";
connectAttr "polyTweak46.out" "polyDelEdge9.ip";
connectAttr "polyDelEdge8.out" "polyTweak46.ip";
connectAttr "polyDelEdge9.out" "polySplit35.ip";
connectAttr "polySplit35.out" "polySplit36.ip";
connectAttr "polyTweak47.out" "polyDelEdge10.ip";
connectAttr "polySplit36.out" "polyTweak47.ip";
connectAttr "polyDelEdge10.out" "polyTweak48.ip";
connectAttr "polyTweak48.out" "polySplit37.ip";
connectAttr "polySplit37.out" "polyTweak49.ip";
connectAttr "polyTweak49.out" "polySplit38.ip";
connectAttr "polySplit38.out" "polySplit39.ip";
connectAttr "polySplit39.out" "polySplit40.ip";
connectAttr "polySplit40.out" "polyTweak50.ip";
connectAttr "polyTweak50.out" "polySplit41.ip";
connectAttr "polySplit41.out" "polySplit42.ip";
connectAttr "polySplit42.out" "polyTweak51.ip";
connectAttr "polyTweak51.out" "polySplit43.ip";
connectAttr "polySplit43.out" "polyTweak52.ip";
connectAttr "polyTweak52.out" "deleteComponent14.ig";
connectAttr "deleteComponent14.og" "polyTweak53.ip";
connectAttr "polyTweak53.out" "deleteComponent15.ig";
connectAttr "deleteComponent15.og" "polyTweak54.ip";
connectAttr "polyTweak54.out" "deleteComponent16.ig";
connectAttr "deleteComponent16.og" "deleteComponent17.ig";
connectAttr "deleteComponent17.og" "deleteComponent18.ig";
connectAttr "deleteComponent18.og" "deleteComponent19.ig";
connectAttr "deleteComponent19.og" "polyExtrudeFace12.ip";
connectAttr "pPlaneShape28.wm" "polyExtrudeFace12.mp";
connectAttr "polyTweak55.out" "polyNormal2.ip";
connectAttr "polyExtrudeFace12.out" "polyTweak55.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "CactusShape.iog.og[3]" ":initialShadingGroup.dsm" -na;
connectAttr "CactusShape.ciog.cog[1]" ":initialShadingGroup.dsm" -na;
connectAttr "|Cactus|Cactus2|transform3|Cactus2Shape.iog.og[1]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "Cactus4Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "|Cactus2|transform4|Cactus2Shape.iog.og[1]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Cactus2|Cactus6|transform6|Cactus6Shape.iog.og[1]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "Cactus8Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "|Cactus6|Cactus6Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSolidShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pSphereShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pSphereShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder5Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder8Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder9Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder11Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder12Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape12.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape14.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape16.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape17.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape20.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape21.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape22.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape24.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape25.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape26.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape27.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape28.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId15.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId18.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId24.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId25.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId26.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId27.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId28.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId29.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId30.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId31.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId32.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId33.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId34.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId35.msg" ":initialShadingGroup.gn" -na;
// End of cactusScene2.ma
