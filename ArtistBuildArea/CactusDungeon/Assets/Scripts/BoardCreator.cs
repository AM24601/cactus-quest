﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;

public class BoardCreator : MonoBehaviour
{
    static string templateFolder = "Assets/RoomTemplates/";

    static DirectoryInfo info = new DirectoryInfo(templateFolder);
    FileInfo[] fileInfo = info.GetFiles();
    string[] fileList;

    public GameObject room;
    public GameObject player;
    public GameObject enemy1;
    public GameObject room1;
    public GameObject room2;
    public GameObject room3;

    public int[,] tileValues;
    public int[,] levelTiles;
    private Vector2[] positions = { new Vector2(0.0f, 0.0f), new Vector2(14.0f,0.0f), new Vector2(14.0f, 14.0f), new Vector2(0.0f, 14.0f), new Vector2(28.0f, 14.0f) };
    public int maxLevelDemension = 42;
    public int maxRoomDemension = 14;

    public bool playerTurn = true;
    public bool playerCourtesy = true;
    private bool isCoroutineExecuting = false;

    static int numRooms = 5;

    GameObject[] level = new GameObject[numRooms];

	// Use this for initialization
	void Start ()
    {
        MakeTemplateList();
        LoadData(fileList);
        CreateLevelArray();
        BuildRooms();
        SetPlayer();
        SetEnemies();
	}

    void Update()
    {
        if (!playerTurn)
        {
            if(enemy1)
                enemy1.GetComponent<Enemy>().TakeTurn();
            playerCourtesy = false;
            StartCoroutine(Wait());
            toggleTurn();
        }
    }

    IEnumerator Wait()
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;

        yield return new WaitForSeconds(0.15f);

        playerCourtesy = true;

        //toggleTurn();

        isCoroutineExecuting = false;
    }



    public void SetEnemies()
    {
        Vector3 ePos = new Vector3(6f, 1f, 6f);
        enemy1 = Instantiate(enemy1, ePos, Quaternion.identity) as GameObject;
        enemy1.GetComponent<Enemy>().setLevel(levelTiles);
        enemy1.GetComponent<Enemy>().setPos(ePos);
    }

    public bool getPlayerTurn()
    {
        return playerTurn;
    }

    public bool getPlayerCourtesy()
    {
        return playerCourtesy;
    }

    public void toggleTurn()
    {
        playerTurn = !playerTurn;
    }

    void SetPlayer()
    {
        player = Instantiate(player, new Vector3(1f, 1f, 1f), Quaternion.identity) as GameObject;
        player.GetComponent<Player>().setLevel(levelTiles);
        player.GetComponent<Player>().setBoard(this);
    }

    void BuildRooms()
    {
        for (int i = 0; i < numRooms; i++)
        {
            //level[i].GetComponent<Room>().CreateTiles();
            switch (i)
            {
                case 0:
                    room1 = Instantiate(room1, new Vector3(positions[0].x, 0f, positions[0].y), Quaternion.identity) as GameObject;
                    level[0].GetComponent<Room>().setPreFab(room1);

                    break;
                case 1:
                    room2 = Instantiate(room2, new Vector3(positions[1].x, 0f, positions[1].y), Quaternion.identity) as GameObject;
                    level[1].GetComponent<Room>().setPreFab(room2);
                    break;
                case 2:
                    room2 = Instantiate(room3, new Vector3(positions[2].x, 0f, positions[2].y), Quaternion.identity) as GameObject;
                    level[2].GetComponent<Room>().setPreFab(room3);
                    break;
            }

        }
    }

    private void MakeTemplateList()
    {
        fileList = new string[numRooms];
        int count = 0;
        for (int i = 0; count < numRooms; i++)
        {
            if (i % 2 == 0)
            {
                fileList[count] = fileInfo[i].ToString();
                count++;
            }

        }
    }

    private bool LoadData(string[] fileList)
    {

        for (int i = 0; i < numRooms; i++)
        {

            StreamReader inputStream = new StreamReader(fileList[i]);

            //reads in the first two lines which should be the dimensions of the grid
            int width = Int32.Parse(inputStream.ReadLine());
            int height = Int32.Parse(inputStream.ReadLine());

            //set tileValue array to the newly found dimensions
            tileValues = new int[width, height];
            //reads in the actual grid
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    char character = (char)inputStream.Read();
                    int temp = Int32.Parse(character.ToString());
                    if (temp > -1 && temp < 2)
                        tileValues[y, x] = temp;
                }

                int skip = inputStream.Read();
                int skip2 = inputStream.Read();
            }
            inputStream.Close();

            //level[i] = Instantiate(room, new Vector3((i*14.0f), 1f, 1f), Quaternion.identity) as GameObject;
            level[i] = Instantiate(room, new Vector3((positions[i].x), positions[i].y, 1f), Quaternion.identity) as GameObject;
            level[i].GetComponent<Room>().setValues(width, height, tileValues);

        }


        return true;
    }

    void CreateLevelArray()
    {
        levelTiles = new int[maxLevelDemension, maxLevelDemension];
        for (int x = 0; x < maxLevelDemension; x++)
        {
            for (int y = 0; y < maxLevelDemension; y++)
            {
                if(x > -1 && x < maxRoomDemension && y > -1 && y < maxRoomDemension)
                {
                    levelTiles[x, y] = level[0].GetComponent<Room>().tileValues[x, y];
                }
                else if(x > (maxRoomDemension - 1) && x < (maxRoomDemension * 2) && y > -1 && y < maxRoomDemension)
                {
                    int tmpX = x - maxRoomDemension;
                    levelTiles[x, y] = level[1].GetComponent<Room>().tileValues[tmpX, y];
                }
                else if (x > (maxRoomDemension - 1) && x < (maxRoomDemension * 2) && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpX = x - maxRoomDemension;
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[2].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else if (x > -1 && x < (maxRoomDemension * 2) && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[3].GetComponent<Room>().tileValues[x, tmpY];
                }
                else if (x > 27 && x < 41 && y > 13 && y < 28)
                {
                    int tmpX = x - (maxRoomDemension *2);
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[4].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else
                {
                    levelTiles[x, y] = 1;
                }
            }
        }
    }

}
