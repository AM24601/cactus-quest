﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour
{
    private Transform boardHolder;

    private int xSize;
    private int ySize;

    public int[,] tileValues;
    public GameObject[,] tiles;

    public GameObject FloorTile;
    public GameObject WallTile;

    public Vector2[] entrances;
    public GameObject tile;

    public GameObject prefab;


    // Use this for initialization
    void Start ()
    {
        
	}

    public void setPreFab(GameObject room)
    {
        prefab = room;
    }

    public void buildPrefab()
    {

    }

    public void setValues(int x, int y, int[,] array)
    {

        xSize = x;
        ySize = y;
        tileValues = array;
        tiles = new GameObject[xSize, ySize];

        DefineTileValues();

    }

    private void DefineTileValues()
    {
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                tiles[x,y] = Instantiate(tile, new Vector3(1f, 1f, 1f), Quaternion.identity) as GameObject;
                tiles[x,y].GetComponent<Tile>().SetType(tileValues[x,y]);   
            }
        }
    }



    public void CreateTiles()
    {


        boardHolder = GameObject.Find("Board").transform;

        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {

                GameObject toInstantiate = tiles[x, y].GetComponent<Tile>().GetTileType();
                GameObject instance;
                if (tiles[x,y].GetComponent<Tile>().getIsPassable())
                {
                    instance = Instantiate(toInstantiate, new Vector3(transform.position.x + x, 0f, transform.position.y + y), Quaternion.identity) as GameObject;
                }
                else
                {
                    instance = Instantiate(toInstantiate, new Vector3(transform.position.x + x, 1f, transform.position.y + y), Quaternion.identity) as GameObject;
                }
                instance.transform.SetParent(boardHolder);
            }
        }
    }

}
