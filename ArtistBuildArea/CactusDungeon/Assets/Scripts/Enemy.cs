﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour
{
    public int health = 2;

    int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, 5, -7);
    public Vector3 southPos = new Vector3(7, 5, 25);
    public Vector3 eastPos = new Vector3(25, 5, 7);
    public Vector3 westPos = new Vector3(-7, 5, 7);
    public Vector3 speed = new Vector3(2, 0, 2);

    //1 is every other turn
    private int moveCount = 1;
    public int damage = 3;

    //Audio
    AudioSource EnemyAttack;

    private bool isCoroutineExecuting = false;

    // Use this for initialization
    void Start()
    {
        north = true;
        south = false;
        east = false;
        west = false;
        currentLevel = 0;

        AudioSource[] audios = GetComponents<AudioSource>();
        EnemyAttack = audios[0];
    }

    void Update()
    {
        if (health <= 0)
            Destroy(gameObject);
    }

    public void Hit(int dmg)
    {
        //System.Collections.Generic.List<Color>();
        health -= dmg;
        //StartCoroutine(Flasher());
        GetComponent<MeshRenderer>().material.color = Color.red;
        //GetComponents<Mesh>()[0].SetColors
    }

    IEnumerator Flasher()
    {
        Color normalColor = GetComponent<Renderer>().material.color;
        Color hitColor = Color.red;

        for (int i = 0; i < 5; i++)
        {
            GetComponent<Renderer>().material.color = hitColor;
            yield return new WaitForSeconds(.1f);
            GetComponent<Renderer>().material.color = normalColor;
            yield return new WaitForSeconds(.1f);
        }
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;
    }


    public void setPos(Vector3 vec)
    {
        mPosX = (int)vec.x;
        mPosZ = (int)vec.z;
    }

    IEnumerator Wait(bool a, bool b, bool c, bool d)
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;

        yield return new WaitForSeconds(0.15f);

        Move(a,b,c,d);

        isCoroutineExecuting = false;
    }
    //


    // Update is called once per frame
    public void TakeTurn()
    {
        

        bool forward, backward, right, left;
        forward = false;
        backward = false;
        right = false;
        left = false;
        System.Random rnd = new System.Random();
        int dir = rnd.Next(4);

        switch (dir)
            {
            case 0:
                forward = true;
                break;
            case 1:
                backward = true;
                break;
            case 2:
                right = true;
                break;
            case 3:
                left = true;
                break;
            default:
                forward = true;
                break;
            }


        if (mPosX > -1 && mPosX < 14 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(7, 5, -7);
            southPos = new Vector3(7, 5, 20);
            eastPos = new Vector3(20, 5, 7);
            westPos = new Vector3(-7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(21, 5, -7);
            southPos = new Vector3(21, 5, 20);
            eastPos = new Vector3(34, 5, 7);
            westPos = new Vector3(7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(21, 5, 7);
            southPos = new Vector3(21, 5, 34);
            eastPos = new Vector3(34, 5, 21);
            westPos = new Vector3(7, 5, 21);
        }
        else if (mPosX > -1 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(7, 5, 7);
            southPos = new Vector3(7, 5, 34);
            eastPos = new Vector3(20, 5, 21);
            westPos = new Vector3(-7, 5, 21);
        }
        else if (mPosX > 27 && mPosX < 41 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(34, 5, 7);
            southPos = new Vector3(34, 5, 34);
            eastPos = new Vector3(49, 5, 21);
            westPos = new Vector3(21, 5, 21);
        }

        if (forward || backward || right || left)
        {
            if (moveCount == 0)
            {
                StartCoroutine(Wait(forward, backward, right, left));
                moveCount = 1;
            }
            else
            {
                moveCount--;
            }
        }

    }

    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }



    void Move(bool forward, bool backward, bool left, bool right)
    {
        int layerMask = 1 << 9;
        Vector3 start = transform.position;
        if (north)
        {


            if (forward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0 )
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ += 1;
                    }

                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
        }
        if (south)
        {
            if (forward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
        }
        if (west)
        {
            if (forward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
        }
        if (east)
        {
            if (forward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        hitColliders[0].GetComponent<Player>().Hit(damage);
                        EnemyAttack.Play();
                    }
                    else
                    {
                        transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
        }
    }
}
