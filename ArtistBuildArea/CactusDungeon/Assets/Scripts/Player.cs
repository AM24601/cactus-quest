﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    static public int camHeight = 10;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, camHeight, -7);
    public Vector3 southPos = new Vector3(7, camHeight, 25);
    public Vector3 eastPos = new Vector3(25, camHeight, 7);
    public Vector3 westPos = new Vector3(-7, camHeight, 7);
    public Vector3 speed = new Vector3(2,0,2);
    public BoardCreator board;

    //Audio
    AudioSource cameraMoveAudio;
    AudioSource playerMoveAudio;
    AudioSource playerAttack;

    public int water = 100;
    public int damage = 1;

    // Use this for initialization
    void Start ()
    {
        north = true;
        south = false;
        east = false;
        west = false;
        mPosX = 1;
        mPosZ = 1;
        currentLevel = 0;
        Camera.main.transform.position = northPos;
        AudioSource[] audios = GetComponents<AudioSource>();
        cameraMoveAudio = audios[0];
        playerMoveAudio = audios[1];
        playerAttack = audios[2];
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;
    }

    public void setBoard(BoardCreator b)
    {
        board = b;
    }

    // Update is called once per frame
    void Update()
    {
        bool cameraLeft;
        bool cameraRight;
        bool forward, backward, right, left;
        cameraLeft = Input.GetKeyUp(KeyCode.Q);
        cameraRight = Input.GetKeyUp(KeyCode.E);
        forward = Input.GetKeyUp(KeyCode.W);
        backward = Input.GetKeyUp(KeyCode.S);
        right = Input.GetKeyUp(KeyCode.A);
        left = Input.GetKeyUp(KeyCode.D);
        if(mPosX > -1 && mPosX < 14 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(7, camHeight, -7);
            southPos = new Vector3(7, camHeight, 20);
            eastPos = new Vector3(20, camHeight, 7);
            westPos = new Vector3(-7, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(7, 3, 7));
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(21, camHeight, -7);
            southPos = new Vector3(21, camHeight, 20);
            eastPos = new Vector3(34, camHeight, 7);
            westPos = new Vector3(7, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(21, 3, 7));
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(21, camHeight, 7);
            southPos = new Vector3(21, camHeight, 34);
            eastPos = new Vector3(34, camHeight, 21);
            westPos = new Vector3(7, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(21, 3, 21));
        }
        else if (mPosX > -1 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(7, camHeight, 7);
            southPos = new Vector3(7, camHeight, 34);
            eastPos = new Vector3(20, camHeight, 21);
            westPos = new Vector3(-7, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(7, 3, 21));
        }
        else if (mPosX > 27 && mPosX < 41 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(34, camHeight, 7);
            southPos = new Vector3(34, camHeight, 34);
            eastPos = new Vector3(49, camHeight, 21);
            westPos = new Vector3(21, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(35, 3, 21));
        }

        if ((forward || backward || right || left) && (board.GetComponent<BoardCreator>().getPlayerCourtesy()))
        {
            Move(forward, backward, right, left);
        }
        
        if(cameraRight || cameraLeft)
        {
            MoveCamera(cameraLeft, cameraRight);
        }

        if( north)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, northPos, ref speed, 0.25f);
        }
        else if (east)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, eastPos, ref speed, 0.25f);
        }
        else if (south)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, southPos, ref speed, 0.25f);
        }
        else if (west)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, westPos, ref speed, 0.25f);
        }
    }
    
    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }

    public void Hit(int dmg)
    {
        water -= dmg;
    }

    void MoveCamera(bool left, bool right)
    {
        cameraMoveAudio.Play();
        if (left)
        {
            if (north)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (east)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (west)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
        }

        if(right)
        {
            if (north)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (east)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (west)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
        }
    }

    void Move(bool forward, bool backward, bool left, bool right)
    {
        int layerMask = 1 << 9;

        playerMoveAudio.Play();
        if (board.GetComponent<BoardCreator>().getPlayerCourtesy())
        {

            board.GetComponent<BoardCreator>().toggleTurn();
            Vector3 start = transform.position;

            if (north)
            {


                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {                            
                            transform.position = start + move;
                            mPosZ += 1;
                        }

                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));
                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));
                        
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
            }
            if (south)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
            }
            if (west)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
            }
            if (east)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            hitColliders[0].GetComponent<Enemy>().Hit(damage);
                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
            }
        }
    }
}
