﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    bool isEmpty;
    bool isPassable;

    GameObject type;
    public GameObject floorTile;
    public GameObject wallTile;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setIsEmpty(bool b)
    {
        isEmpty = b;
    }

    public bool getIsEmpty()
    {
        return isEmpty;
    }

    public void setIsPassable(bool b)
    {
        isPassable = b;
    }

    public bool getIsPassable()
    {
        return isPassable;
    }

    public void SetType(int tileType)
    {
        switch (tileType)
        {
            case 0:
                type = floorTile;
                isPassable = true;
                break;
            case 1:
                type = wallTile;
                isPassable = false;
                break;
            default:
                type = wallTile;
                isPassable = false;
                break;
        }
    }
    public GameObject GetTileType()
    {
        return type;
    }

}
