﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    public int mainMenu;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(mainMenu == 1)
        {
            if (Input.anyKey)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("instruction");
            }
        }
        else if(mainMenu == 2)
        {
             if(Input.anyKey)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("scene1");
            }
        }
        else
        {
            if (Input.anyKey)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("mainMenu");
            }
        }
    }
}
