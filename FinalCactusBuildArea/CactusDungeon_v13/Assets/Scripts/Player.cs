﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    static public int camHeight = 10;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, camHeight, 22);
    public Vector3 southPos = new Vector3(7, camHeight, 47);
    public Vector3 eastPos = new Vector3(19, camHeight, 34);
    public Vector3 westPos = new Vector3(-7, camHeight, 34);
    public Vector3 speed = new Vector3(2,0,2);
    public BoardCreator board;

    public int room;

    private Text waterText;
    private Text deathText;

    private bool flowerIsActive = false;

    //Audio
    AudioSource cameraMoveAudio;
    AudioSource playerMoveAudio;
    AudioSource playerAttack;

    public int water = 100;
    public int damage = 1;
    public int flowerDamage = 3;

    // Use this for initialization
    void Start ()
    {
        transform.FindChild("Basic_Flower_Mesh").GetComponent<Renderer>().enabled = false;

        north = true;
        south = false;
        east = false;
        west = false;
        mPosX = 2;
        mPosZ = 30;
        currentLevel = 0;
        Camera.main.transform.position = northPos;
        AudioSource[] audios = GetComponents<AudioSource>();
        cameraMoveAudio = audios[0];
        playerMoveAudio = audios[1];
        playerAttack = audios[2];

        waterText.text = "Water: " + water;
        deathText.text = " ";
    }


    public int getRoom()
    {
        return room;
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;
    }

    public void replenishWater(int amt)
    {
        water += amt;
    }

    public void setBoard(BoardCreator b)
    {
        board = b;
    }

    public void setText(Text txt)
    {
        waterText = txt;
    }
    public void setDeathText(Text txt)
    {
        deathText = txt;
    }

    // Update is called once per frame
    void Update()
    {
        if (water > 100)
            water = 100;

        if(water <= 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("endScreen");
            //deathText.text = "No Water!! You Died!!";
        }
        if (flowerIsActive)
        {
            damage = flowerDamage;
            playerAttack.pitch = 0;
        }

        waterText.text = "Water: " + water;

        bool cameraLeft;
        bool cameraRight;
        bool forward, backward, right, left;
        cameraLeft = Input.GetKeyUp(KeyCode.Q);
        cameraRight = Input.GetKeyUp(KeyCode.E);
        forward = Input.GetKeyUp(KeyCode.W);
        backward = Input.GetKeyUp(KeyCode.S);
        right = Input.GetKeyUp(KeyCode.A);
        left = Input.GetKeyUp(KeyCode.D);
        if(mPosX > -1 && mPosX < 14 && mPosZ > 27 && mPosZ < 42)
        {
            northPos = new Vector3(7, camHeight, 20);
            southPos = new Vector3(7, camHeight, 49);
            eastPos = new Vector3(21, camHeight, 34);
            westPos = new Vector3(-7, camHeight, 34);
            Camera.main.transform.LookAt(new Vector3(7, 3, 34));
            if (room != 1)
            {
                room = 1;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 27 && mPosZ < 42)
        {
            northPos = new Vector3(21, camHeight, 20);
            southPos = new Vector3(21, camHeight, 49);
            eastPos = new Vector3(35, camHeight, 34);
            westPos = new Vector3(6, camHeight, 34);
            Camera.main.transform.LookAt(new Vector3(21, 3, 34));
            if (room != 2)
            {
                room = 2;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 41 && mPosZ < 56)
        {
            northPos = new Vector3(21, camHeight, 34);
            southPos = new Vector3(21, camHeight, 49);
            eastPos = new Vector3(35, camHeight, 48);
            westPos = new Vector3(6, camHeight, 48);
            Camera.main.transform.LookAt(new Vector3(21, 3, 48));
            if (room != 3)
            {
                room = 3;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > -1 && mPosX < 14 && mPosZ > 41 && mPosZ < 56)
        {
            northPos = new Vector3(7, camHeight, 34);
            southPos = new Vector3(7, camHeight, 49);
            eastPos = new Vector3(21, camHeight, 48);
            westPos = new Vector3(-7, camHeight, 48);
            Camera.main.transform.LookAt(new Vector3(7, 3, 48));
            if (room != 4)
            {
                room = 4;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 27 && mPosX < 42 && mPosZ > 41 && mPosZ < 56)
        {
            northPos = new Vector3(34, camHeight, 34);
            southPos = new Vector3(34, camHeight, 49);
            eastPos = new Vector3(49, camHeight, 48);
            westPos = new Vector3(20, camHeight, 48);
            Camera.main.transform.LookAt(new Vector3(34, 3, 48));
            if (room != 5)
            {
                room = 5;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 27 && mPosX < 42 && mPosZ > 27 && mPosZ < 42)
        {
            northPos = new Vector3(34, camHeight, 20);
            southPos = new Vector3(34, camHeight, 39);
            eastPos = new Vector3(49, camHeight, 34);
            westPos = new Vector3(20, camHeight, 34);
            Camera.main.transform.LookAt(new Vector3(34, 3, 34));
            if (room != 6)
            {
                room = 6;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 41 && mPosX < 56 && mPosZ > 41 && mPosZ < 56)
        {
            northPos = new Vector3(48, camHeight, 34);
            southPos = new Vector3(48, camHeight, 49);
            eastPos = new Vector3(63, camHeight, 48);
            westPos = new Vector3(54, camHeight, 48);
            Camera.main.transform.LookAt(new Vector3(48, 3, 48));
            if (room != 7)
            {
                room = 7;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 41 && mPosX < 56 && mPosZ > 27 && mPosZ < 42)
        {
            northPos = new Vector3(48, camHeight, 20);
            southPos = new Vector3(48, camHeight, 39);
            eastPos = new Vector3(63, camHeight, 34);
            westPos = new Vector3(54, camHeight, 34);
            Camera.main.transform.LookAt(new Vector3(48, 3, 34));
            if (room != 8)
            {
                room = 8;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > 41 && mPosZ < 56)
        {
            northPos = new Vector3(62, camHeight, 34);
            southPos = new Vector3(62, camHeight, 49);
            eastPos = new Vector3(77, camHeight, 48);
            westPos = new Vector3(48, camHeight, 48);
            Camera.main.transform.LookAt(new Vector3(62, 3, 48));
            if (room != 9)
            {
                room = 9;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > 27 && mPosZ < 42)
        {
            northPos = new Vector3(62, camHeight, 20);
            southPos = new Vector3(62, camHeight, 39);
            eastPos = new Vector3(77, camHeight, 34);
            westPos = new Vector3(48, camHeight, 34);
            Camera.main.transform.LookAt(new Vector3(62, 3, 34));
            if (room != 10)
            {
                room = 10;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(62, camHeight, 7);
            southPos = new Vector3(62, camHeight, 37);
            eastPos = new Vector3(77, camHeight, 21);
            westPos = new Vector3(48, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(62, 3, 21));
            if (room != 11)
            {
                room = 11;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 41 && mPosX < 56 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(48, camHeight, 7);
            southPos = new Vector3(48, camHeight, 37);
            eastPos = new Vector3(63, camHeight, 21);
            westPos = new Vector3(54, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(48, 3, 21));
            if (room != 12)
            {
                room = 12;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(62, camHeight, -7);
            southPos = new Vector3(62, camHeight, 21);
            eastPos = new Vector3(77, camHeight, 7);
            westPos = new Vector3(48, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(62, 3, 7));
            if (room != 13)
            {
                room = 13;
                board.checkActiveRoom();
            }
        }

        if ((forward || backward || right || left) && (board.GetComponent<BoardCreator>().getPlayerCourtesy()))
        {
            Move(forward, backward, right, left);
        }
        
        if(cameraRight || cameraLeft)
        {
            MoveCamera(cameraLeft, cameraRight);
        }

        if( north)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, northPos, ref speed, 0.25f);
        }
        else if (east)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, eastPos, ref speed, 0.25f);
        }
        else if (south)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, southPos, ref speed, 0.25f);
        }
        else if (west)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, westPos, ref speed, 0.25f);
        }
    }
    
    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }

    public void Hit(int dmg)
    {
        water -= dmg;
    }

    void MoveCamera(bool left, bool right)
    {
        cameraMoveAudio.Play();
        if (left)
        {
            if (north)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (east)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (west)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
        }

        if(right)
        {
            if (north)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (east)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (west)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
        }
    }

    void Move(bool forward, bool backward, bool left, bool right)
    {
        int layerMask = 1 << 9;

        water--;

        playerMoveAudio.Play();
        if (board.GetComponent<BoardCreator>().getPlayerCourtesy())
        {

            board.GetComponent<BoardCreator>().toggleTurn();
            Vector3 start = transform.position;

            if (north)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if(hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {                            
                            transform.position = start + move;
                            mPosZ += 1;
                        }

                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));
                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));
                        
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
            }
            if (south)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
            }
            if (west)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
            }
            if (east)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            if (hitColliders[0].tag == "Enemy")
                                hitColliders[0].GetComponent<Enemy>().Hit(damage);

                            if (hitColliders[0].tag == "PoweredEnemy")
                                hitColliders[0].GetComponent<PoweredEnemy>().Hit(damage);

                            playerAttack.Play();
                        }
                        else
                        {
                            transform.position = start + move;
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
            }
        }
    }
}
