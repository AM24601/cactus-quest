﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    public Slider healthSlider;

    private float shakeAmt = .0025f;

    private Vector3 originalPosition;

	// Use this for initialization
	void Start ()
    {
        healthSlider.value = 1;
        originalPosition = transform.FindChild("PlayerHealth").transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        float water = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().water;
        healthSlider.value = (water / 100.0f);
        //transform.FindChild("PlayerHealth").transform.position += new Vector3(1, 0, 0);
        if (water < 80)
        {
            float r = ((100 - water) / 100);
            float g = (water / 100);

            Shake(water);
            transform.FindChild("PlayerHealth").FindChild("Background").GetComponent<Image>().color = new Color(r, g, g, 1);
            transform.FindChild("PlayerHealth").FindChild("Fill Area").FindChild("Fill").GetComponent<Image>().color = new Color(r, g, g, 1);
            //print((100 - water) / 100);
           // print(water );
        }
    }

    void Shake(float m)
    {
        float magnitude = Mathf.Clamp((((m - 100) * -1) / 8) / 2, 1, 7);
        //print(magnitude);

        var randomInt = Random.Range(0, 4);
        Vector3 vec;

        switch (randomInt)
        {
            case 0:
                vec = new Vector3(0, magnitude, 0);
                break;
            case 1:
                vec = new Vector3(0, -magnitude, 0);
                break;
            case 2:
                vec = new Vector3(0, 0, magnitude);
                break;
            case 3:
                vec = new Vector3(0, 0, -magnitude);
                break;
            default:
                vec = new Vector3(0, magnitude, 0);
                break;
        }

        transform.FindChild("PlayerHealth").transform.position = originalPosition + vec;

    }
}
