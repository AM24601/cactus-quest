﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    static public int camHeight = 7;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, camHeight, -7);
    public Vector3 southPos = new Vector3(7, camHeight, 25);
    public Vector3 eastPos = new Vector3(25, camHeight, 7);
    public Vector3 westPos = new Vector3(-7, camHeight, 7);
    public Vector3 speed = new Vector3(2,0,2);
    public BoardCreator board;

    private string[] commands = {
        "up",
        "up",
        "up",
        "up",
        "up",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "right",
        "up",
        "up",
        "right",
        "right",
        "",
        "",
        "right",
        "",
        "",
    };

    private int commandPos = 0;
    private float commandTime = 0.25f; //number of seconds between turns
    private float timer = 5;

    public int room;

    private Text waterText;

    public GameObject chunks;
    public GameObject spores;

    private bool flowerIsActive = false;
    private bool betterPot = false;

    //Audio
    AudioSource cameraMoveAudio;
    AudioSource playerMoveAudio;
    AudioSource playerAttack;
    AudioSource powerUp;
    AudioSource getPot;
    AudioSource specialAttack;
    AudioSource slurp;

    public int water = 100;
    public int damage;
    public int flowerDamage;
    private int countWater = 0;

    private Vector3 destination;

    private float attackDelay = .2f;
    private float moveDelay = 0f;

    private Color defaultPot;

    // Use this for initialization
    void Start ()
    {
        transform.FindChild("Joe").FindChild("jnt_origin").FindChild("jnt_body").FindChild("jnt_pot").FindChild("spine01").FindChild("jnt_torso").FindChild("jnt_head").FindChild("Flower_Mesh").GetComponent<Renderer>().enabled = false;
        damage = 2;
        flowerDamage = 3;
        north = true;
        south = false;
        east = false;
        west = false;
        mPosX = 7;
        mPosZ = 7;
        currentLevel = 0;
        Camera.main.transform.position = northPos;
        AudioSource[] audios = GetComponents<AudioSource>();
        cameraMoveAudio = audios[0];
        playerMoveAudio = audios[1];
        playerAttack = audios[2];
        powerUp = audios[3];
        getPot = audios[4];
        specialAttack = audios[5];
        slurp = audios[6];

        destination = transform.position;

        defaultPot = transform.FindChild("Joe").FindChild("Pot_Mesh").GetComponent<Renderer>().material.color;

        waterText.text = "Water: " + water;
    }

    IEnumerator Flasher()
    {
        Color normalColor = transform.FindChild("Joe").FindChild("Joe_Body_Mesh2").GetComponent<Renderer>().material.color;
        Color hitColor = Color.red;

        for (int i = 0; i < 2; i++)
        {
            transform.FindChild("Joe").FindChild("Joe_Body_Mesh2").GetComponent<Renderer>().material.color = hitColor;
            yield return new WaitForSeconds(.02f);
            transform.FindChild("Joe").FindChild("Joe_Body_Mesh2").GetComponent<Renderer>().material.color = normalColor;
            yield return new WaitForSeconds(.06f);
        }
    }

    public void changePot(string type)
    {
        Color superPot = Color.black;
        switch (type)
        {
            case "new":
                transform.FindChild("Joe").FindChild("Pot_Mesh").GetComponent<Renderer>().material.color = superPot;
                getPot.Play();
                break;
            default:
                transform.FindChild("Joe").FindChild("Pot_Mesh").GetComponent<Renderer>().material.color = defaultPot;
                break;
        }
    }
    public void eatEnemy()
    {
        slurp.Play();
    }
    public void setFlowerOn(bool hasFlower)
    {
        flowerIsActive = hasFlower;
        powerUp.Play();
    }
    public int getRoom()
    {
        return room;
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;
    }

    public void replenishWater(int amt)
    {
        water += amt;
    }

    public void setBoard(BoardCreator b)
    {
        board = b;
    }

    public void setText(Text txt)
    {
        waterText = txt;
    }

    // Update is called once per frame
    void Update()
    {
        //if (flowerIsActive)
        //powerUp.Play();



        if (water > 100)
            water = 100;

        if(water <= 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("endScreen");
        }
        if (flowerIsActive)
        {
            damage = flowerDamage;
            playerAttack.pitch = 0;
        }

        if(transform.position == destination)
            GetComponent<Animator>().SetTrigger("StopMove");

        transform.position = Vector3.Lerp(transform.position, destination, 10f * Time.deltaTime);

        waterText.text = "Water: " + water;

        //camera bullshit
        #region

        bool cameraLeft;
        bool cameraRight;
        bool forward, backward, right, left;
        cameraLeft = Input.GetKeyUp(KeyCode.Q);
        cameraRight = Input.GetKeyUp(KeyCode.E);
        forward = Input.GetKeyUp(KeyCode.W);
        backward = Input.GetKeyUp(KeyCode.S);
        right = Input.GetKeyUp(KeyCode.A);
        left = Input.GetKeyUp(KeyCode.D);

        /*
        //COMMAND LIST
        timer -= Time.deltaTime;
        if (timer <= 0 && commandPos < commands.Length)
        {
            timer = commandTime;
            switch (commands[commandPos])
            {
                case "up":
                    forward = true;
                    break;
                case "down":
                    backward = true;
                    break;
                case "left":
                    right = true;
                    break;
                case "right":
                    left = true;
                    break;
                case "camLeft":
                    cameraLeft = true;
                    break;
                case "camRight":
                    cameraRight = true;
                    break;
            }

            commandPos++;

        }*/

        if (mPosX > -1 && mPosX < 14 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(7, camHeight, -5);
            southPos = new Vector3(7, camHeight, 18);
            eastPos = new Vector3(18, camHeight, 7);
            westPos = new Vector3(-5, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(7, 0, 7));
            if (room != 1)
            {
                room = 1;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(21, camHeight, -5);
            southPos = new Vector3(21, camHeight, 18);
            eastPos = new Vector3(32, camHeight, 7);
            westPos = new Vector3(9, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(21, 0, 7));
            if (room != 2)
            {
                room = 2;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(21, camHeight, 9);
            southPos = new Vector3(21, camHeight, 32);
            eastPos = new Vector3(32, camHeight, 21);
            westPos = new Vector3(9, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(21, 0, 21));
            if (room != 3)
            {
                room = 3;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > -1 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(7, camHeight, 9);
            southPos = new Vector3(7, camHeight, 32);
            eastPos = new Vector3(18, camHeight, 21);
            westPos = new Vector3(-5, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(7, 0, 21));
            if (room != 4)
            {
                room = 4;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 27 && mPosX < 42 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(34, camHeight, 9);
            southPos = new Vector3(34, camHeight, 32);
            eastPos = new Vector3(46, camHeight, 21);
            westPos = new Vector3(23, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(34, 0, 21));
            if (room != 5)
            {
                room = 5;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 27 && mPosX < 42 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(34, camHeight, -5);
            southPos = new Vector3(34, camHeight, 18);
            eastPos = new Vector3(46, camHeight, 7);
            westPos = new Vector3(23, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(34, 0, 7));
            if (room != 6)
            {
                room = 6;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 41 && mPosX < 56 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(48, camHeight, 9);
            southPos = new Vector3(48, camHeight, 32);
            eastPos = new Vector3(60, camHeight, 21);
            westPos = new Vector3(37, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(48, 0, 21));
            if (room != 7)
            {
                room = 7;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 41 && mPosX < 56 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(48, camHeight, -5);
            southPos = new Vector3(48, camHeight, 18);
            eastPos = new Vector3(60
                , camHeight, 7);
            westPos = new Vector3(37, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(48, 0, 7));
            if (room != 8)
            {
                room = 8;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(62, camHeight, 9);
            southPos = new Vector3(62, camHeight, 32);
            eastPos = new Vector3(74, camHeight, 21);
            westPos = new Vector3(51, camHeight, 21);
            Camera.main.transform.LookAt(new Vector3(62, 0, 21));
            if (room != 9)
            {
                room = 9;
                board.checkActiveRoom();
            }
        }
        else if (mPosX > 55 && mPosX < 70 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(62, camHeight, -5);
            southPos = new Vector3(62, camHeight, 18);
            eastPos = new Vector3(74, camHeight, 7);
            westPos = new Vector3(51, camHeight, 7);
            Camera.main.transform.LookAt(new Vector3(62, 0, 7));
            if (room != 10)
            {
                room = 10;
                board.checkActiveRoom();
            }
        }
        #endregion

        if ((forward || backward || right || left) && (board.GetComponent<BoardCreator>().getPlayerCourtesy()))
        {
            transform.position = destination;
            GetComponent<Animator>().SetTrigger("StopMove");
            Move(forward, backward, right, left);
        }
        
        if(cameraRight || cameraLeft)
        {
            MoveCamera(cameraLeft, cameraRight);
        }

        if( north)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, northPos, ref speed, 0.25f);
        }
        else if (east)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, eastPos, ref speed, 0.25f);
        }
        else if (south)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, southPos, ref speed, 0.25f);
        }
        else if (west)
        {
            Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, westPos, ref speed, 0.25f);
        }
    }
    
    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }

    public void Hit(int dmg)
    {
        water -= dmg;
        StartCoroutine(Flasher());
        GameObject temp = Instantiate(chunks, transform.position, chunks.transform.rotation) as GameObject;
        Destroy(temp, 1);
    }

    IEnumerator WaitAttack(float time, Collider target, string type)
    {
        GetComponent<Animator>().SetTrigger("StopMove");

        if (!flowerIsActive)
            GetComponent<Animator>().SetTrigger("Attack");
        else
        {
            GetComponent<Animator>().SetTrigger("PowerAttack");

            GameObject temp = Instantiate(spores, transform.FindChild("Joe").FindChild("jnt_origin").FindChild("jnt_body").FindChild("jnt_pot").FindChild("spine01").FindChild("jnt_torso").FindChild("jnt_head").FindChild("Flower_Mesh").transform.position, spores.transform.rotation) as GameObject;
            Destroy(temp, 2);
        }

        yield return new WaitForSeconds(time);

        switch (type)
        {
            case "Enemy":
                target.GetComponent<Enemy>().Hit(damage);
                //playerAttack.Play();                    
                break;
            case "PoweredEnemy":
                target.GetComponent<PoweredEnemy>().Hit(damage);
                //playerAttack.Play();
                break;
            case "Pot":
                betterPot = true;
                changePot("new");
                target.GetComponent<Item>().setTaken(true);
                break;
        }
        if (!flowerIsActive)
            playerAttack.Play();
        else
            specialAttack.Play();

        //yield return new WaitForSeconds(1.633f * 1.5f);

    }

    IEnumerator WaitMove(float time, Vector3 pos)
    {
        GetComponent<Animator>().SetTrigger("StopMove");
        transform.position = destination;
        GetComponent<Animator>().SetTrigger("Move");

        yield return new WaitForSeconds(time);

        destination = pos;
        //transform.position = pos;

    }

    void MoveCamera(bool left, bool right)
    {
        cameraMoveAudio.Play();
        if (left)
        {
            if (north)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (east)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (west)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
        }

        if(right)
        {
            if (north)
            {
                north = false;
                south = false;
                east = true;
                west = false;
            }
            else if (east)
            {
                north = false;
                south = true;
                east = false;
                west = false;
            }
            else if (south)
            {
                north = false;
                south = false;
                east = false;
                west = true;
            }
            else if (west)
            {
                north = true;
                south = false;
                east = false;
                west = false;
            }
        }
    }

    void Move(bool forward, bool backward, bool left, bool right)
    {
        int layerMask = 1 << 9;
        if(betterPot)
        {
            countWater++;
            if(countWater % 2 == 0)
            {
                water--;
            }
        }
        else
        {
            water--;
        }

        playerMoveAudio.Play();
        if (board.GetComponent<BoardCreator>().getPlayerCourtesy())
        {

            board.GetComponent<BoardCreator>().toggleTurn();
            Vector3 start = transform.position;

            if (north)
            {


                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ += 1;
                        }

                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                    }
                    else if(levelArray[mPosX, mPosZ + 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));
                    }
                    else if (levelArray[mPosX, mPosZ - 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));
                        
                    }
                    else if (levelArray[mPosX - 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                    else if (levelArray[mPosX + 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
            }
            if (south)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                    else if (levelArray[mPosX, mPosZ - 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                    else if (levelArray[mPosX + 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                    else if (levelArray[mPosX - 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
            }
            if (west)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                    else if (levelArray[mPosX + 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                    else if (levelArray[mPosX - 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                    else if (levelArray[mPosX, mPosZ + 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                    else if (levelArray[mPosX, mPosZ - 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
            }
            if (east)
            {
                if (forward)
                {
                    Vector3 move = new Vector3(-1, 0, 0);
                    if (levelArray[mPosX - 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                    }
                    else if (levelArray[mPosX - 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (backward)
                {
                    Vector3 move = new Vector3(1, 0, 0);
                    if (levelArray[mPosX + 1, mPosZ] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosX += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                    }
                    else if (levelArray[mPosX + 1, mPosZ] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (left)
                {
                    Vector3 move = new Vector3(0, 0, -1);
                    if (levelArray[mPosX, mPosZ - 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ -= 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                    }
                    else if (levelArray[mPosX, mPosZ - 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
                else if (right)
                {
                    Vector3 move = new Vector3(0, 0, 1);
                    if (levelArray[mPosX, mPosZ + 1] == 0)
                    {
                        var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                        if (hitColliders.Length > 0)
                        {
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0], hitColliders[0].tag));
                        }
                        else
                        {
                            StartCoroutine(WaitMove(moveDelay, start + move));
                            mPosZ += 1;
                        }
                        transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                    }
                    else if (levelArray[mPosX, mPosZ + 1] == 2)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("winScreen");
                    }
                }
            }
        }
    }
}
