﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    public int mainMenu;
    public Vector3[] wayPoints;
    Vector3[] lookPoints;
    public float cameraHeight;
    public Vector3 speed;
    int currentIndex;
    public GameObject targetLook;

    public GameObject menuButton;
    public GameObject instructionButton;
    public GameObject playButon;
    public GameObject quitButton;
    public GameObject title;
    public GameObject instrucText;

    // Use this for initialization
    void Start ()
    {
        menuButton.SetActive(false);
        instructionButton.SetActive(true);
        playButon.SetActive(true);
        quitButton.SetActive(true);
        title.SetActive(true);
        instrucText.SetActive(false);

        currentIndex = 0;
        speed = new Vector3(0.01f, 0f, 0.01f);
        cameraHeight = 1.5f;
        wayPoints = new Vector3[16];
        wayPoints[0] = new Vector3(10, cameraHeight, 6.5f);
        wayPoints[1] = new Vector3(5f, cameraHeight, 11);
        wayPoints[2] = new Vector3(6.5f, cameraHeight, 19);
        wayPoints[3] = new Vector3(6.5f, cameraHeight, 30);
        wayPoints[4] = new Vector3(10, cameraHeight, 36f);
        wayPoints[5] = new Vector3(21, cameraHeight, 34.5f);
        wayPoints[6] = new Vector3(36.5f, cameraHeight, 34.5f);
        wayPoints[7] = new Vector3(34.5f, cameraHeight, 22);
        wayPoints[8] = new Vector3(34.5f, cameraHeight, 11);
        wayPoints[9] = new Vector3(37, cameraHeight, 7);
        wayPoints[10] = new Vector3(34.5f, cameraHeight, 3);
        wayPoints[11] = new Vector3(30, cameraHeight, 7);
        wayPoints[12] = new Vector3(25, cameraHeight, 6.5f);
        wayPoints[13] = new Vector3(25, cameraHeight, 3);
        wayPoints[14] = new Vector3(17, cameraHeight, 3);
        wayPoints[15] = new Vector3(17, cameraHeight, 8f);

        lookPoints = new Vector3[16];
        lookPoints[0] = new Vector3(9, cameraHeight, 8);
        lookPoints[1] = new Vector3(6.5f, cameraHeight, 15);
        lookPoints[2] = new Vector3(6.5f, cameraHeight, 25);
        lookPoints[3] = new Vector3(6.5f, cameraHeight, 32);
        lookPoints[4] = new Vector3(13, cameraHeight, 34.5f);
        lookPoints[5] = new Vector3(24, cameraHeight, 34.5f);
        lookPoints[6] = new Vector3(37, cameraHeight, 34.5f);
        lookPoints[7] = new Vector3(34.5f, cameraHeight, 24);
        lookPoints[8] = new Vector3(34.5f, cameraHeight, 8);
        lookPoints[9] = new Vector3(37, cameraHeight, 4);
        lookPoints[10] = new Vector3(32, cameraHeight, 3);
        lookPoints[11] = new Vector3(27, cameraHeight, 6.5f);
        lookPoints[12] = new Vector3(22, cameraHeight, 6.5f);
        lookPoints[13] = new Vector3(25, cameraHeight, 1);
        lookPoints[14] = new Vector3(15, cameraHeight, 3f);
        lookPoints[15] = new Vector3(15, cameraHeight, 6.5f);

        Camera.main.transform.position = wayPoints[0];
        targetLook.transform.position = lookPoints[0];
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, wayPoints[1], ref speed, 5.0f);
        Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, wayPoints[currentIndex], ref speed, 6.0f);
        targetLook.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, lookPoints[currentIndex], ref speed, 6.0f);
        Camera.main.transform.LookAt(targetLook.transform.position);
        Vector3 diff = wayPoints[currentIndex] - Camera.main.transform.position;
        if(diff.magnitude < 2f)
        {
            currentIndex++;
        }
        if (currentIndex == 16)
        {
            //Camera.main.transform.LookAt(wayPoints[0]);
            currentIndex = 0;
        }
    }

    public void showInstructions()
    {
        playButon.SetActive(false);
        instructionButton.SetActive(false);
        menuButton.SetActive(true);
        quitButton.SetActive(false);
        title.SetActive(false);
        instrucText.SetActive(true);
    }

    public void playGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("scene1");
    }

    public void showMainMenu()
    {
        playButon.SetActive(true);
        instructionButton.SetActive(true);
        menuButton.SetActive(false);
        quitButton.SetActive(true);
        title.SetActive(true);
        instrucText.SetActive(false);
    }

    public void goToMain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("mainMenu");
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
