﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour
{
    public int health;

    int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, 5, -7);
    public Vector3 southPos = new Vector3(7, 5, 25);
    public Vector3 eastPos = new Vector3(25, 5, 7);
    public Vector3 westPos = new Vector3(-7, 5, 7);
    public Vector3 speed = new Vector3(2, 0, 2);

    //1 is every other turn
    private int moveCount = 1;
    private int damage = 5;
    private int worth = 20;

    public BoardCreator board;

    private bool turnComplete = true;

    // 0.2f seconds is the time in the animation where the enemy hits, this value is not arbitrary
    private float attackDelay = .2f;
    private float moveDelay = 0f;

    public int room;

    System.Random rnd;

    //Audio
    AudioSource EnemyAttack;

    public GameObject chunks;
    public GameObject water;

    private Vector3 destination;

    private bool isCoroutineExecuting = false;

    public void setBoard(BoardCreator b)
    {
        board = b;
    }

    public int getRoom()
    {
        return room;
    }

    // Use this for initialization
    void Start()
    {
        health = 6;
        north = true;
        south = false;
        east = false;
        west = false;
        currentLevel = 0;
        destination = transform.position;

        rnd = new System.Random(GetInstanceID());

        AudioSource[] audios = GetComponents<AudioSource>();
        EnemyAttack = audios[0];
    }

    void Update()
    {
        if (health <= 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().eatEnemy();
            Destroy(gameObject);
            GameObject temp = Instantiate(water, transform.position, water.transform.rotation) as GameObject;
            Destroy(temp, 1);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().replenishWater(worth);
        }

        //if(transform.position != destination)
            transform.position = Vector3.Lerp(transform.position, destination, 5f * Time.deltaTime);

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom() != room)
        {
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("CactusEnemy_Body").GetComponent<Renderer>().enabled = false;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Enemy_Mouth_Low").GetComponent<Renderer>().enabled = false;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Left_Eye_Low").GetComponent<Renderer>().enabled = false;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Right_Eye_Low").GetComponent<Renderer>().enabled = false;
        }
        else
        {
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("CactusEnemy_Body").GetComponent<Renderer>().enabled = true;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Enemy_Mouth_Low").GetComponent<Renderer>().enabled = true;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Left_Eye_Low").GetComponent<Renderer>().enabled = true;
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("Right_Eye_Low").GetComponent<Renderer>().enabled = true;
        }
    }

    public void Hit(int dmg)
    {
        health -= dmg;
        StartCoroutine(Flasher());
        GameObject temp = Instantiate(chunks, transform.position, chunks.transform.rotation) as GameObject;
        Destroy(temp, 2);
    }

    IEnumerator Flasher()
    {
        Color normalColor = transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color;
        Color hitColor = Color.red;

        for (int i = 0; i < 2; i++)
        {
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color = hitColor;
            yield return new WaitForSeconds(.02f);
            transform.FindChild("EnemyOne_Mesh").FindChild("Enemy").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color = normalColor;
            yield return new WaitForSeconds(.06f);
        }
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;

    }


    public void setPos(Vector3 vec)
    {
        mPosX = (int)vec.x;
        mPosZ = (int)vec.z;
    }

    IEnumerator Wait(bool a, bool b, bool c, bool d)
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;
        Transform target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().transform;

        yield return new WaitForSeconds(0.15f);
        if(Vector3.Distance(transform.position, target.position) < 7 && GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom() == room)
            Move(a,b,c,d);

        isCoroutineExecuting = false;
    }

    IEnumerator WaitAttack(float time, Collider target)
    {

        board.GetComponent<BoardCreator>().addTime(1f);

        yield return new WaitForSeconds(time);

        target.GetComponent<Player>().Hit(damage);
        EnemyAttack.Play();
        //target.GetComponent<CamShakeSimple>().shake();

        

        yield return new WaitForSeconds(1.633f * 1.5f);

        turnComplete = true;
    }

    IEnumerator WaitMove(float time, Vector3 pos)
    {

        yield return new WaitForSeconds(time);

        destination = pos;
        //transform.position = pos;

    }




    public void setRoom(int index)
    {
        room = index;
    }

    public bool turnOver()
    {
        return turnComplete;
    }

    // Update is called once per frame
    public void TakeTurn()
    {

        turnComplete = false;
        transform.position = destination;

        bool forward, backward, right, left;
        forward = false;
        backward = false;
        right = false;
        left = false;

        int xDir = 0;
        int yDir = 0;

        Transform target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().transform;

        if (Mathf.Abs(target.position.x - destination.x) < float.Epsilon)
            yDir = target.position.z > destination.z ? 1 : -1;
        else
            xDir = target.position.x > destination.x ? 1 : -1;

        //int dir = rnd.Next(4);

        switch (xDir)
            {
            case -1:
                right = true;
                break;
            case 1:
                left = true;
                break;
            }

        switch (yDir)
        {
            case -1:
                backward = true;
                break;
            case 1:
                forward = true;
                break;
        }


        if (mPosX > -1 && mPosX < 14 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(7, 5, -7);
            southPos = new Vector3(7, 5, 20);
            eastPos = new Vector3(20, 5, 7);
            westPos = new Vector3(-7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(21, 5, -7);
            southPos = new Vector3(21, 5, 20);
            eastPos = new Vector3(34, 5, 7);
            westPos = new Vector3(7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(21, 5, 7);
            southPos = new Vector3(21, 5, 34);
            eastPos = new Vector3(34, 5, 21);
            westPos = new Vector3(7, 5, 21);
        }
        else if (mPosX > -1 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(7, 5, 7);
            southPos = new Vector3(7, 5, 34);
            eastPos = new Vector3(20, 5, 21);
            westPos = new Vector3(-7, 5, 21);
        }
        else if (mPosX > 27 && mPosX < 41 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(34, 5, 7);
            southPos = new Vector3(34, 5, 34);
            eastPos = new Vector3(49, 5, 21);
            westPos = new Vector3(21, 5, 21);
        }

        if ((forward || backward || right || left) && gameObject.activeSelf)
        {
            if (moveCount == 0)
            {
                StartCoroutine(Wait(forward, backward, right, left));
                moveCount = 1;
            }
            else
            {
                moveCount--;
            }
        }

    }

    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }



    void Move(bool forward, bool backward, bool left, bool right)
    {

        int layerMask = 1 << 9;
        Vector3 start = transform.position;
        if (north)
        {


            if (forward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0 )
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ += 1;
                    }

                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));                            
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
        }
        if (south)
        {
            if (forward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
        }
        if (west)
        {
            if (forward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
        }
        if (east)
        {
            if (forward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
        }
    }
}
