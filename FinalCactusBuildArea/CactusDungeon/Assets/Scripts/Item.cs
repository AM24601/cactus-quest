﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour
{
    public int room;
    public bool taken;
    // Use this for initialization
    void Start ()
    {
        taken = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //print("Room in: " + room);
        //print(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom());
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom() == room)
        {
            transform.GetComponent<Renderer>().enabled = true;
            transform.FindChild("Particle System").GetComponent<ParticleSystem>().enableEmission = true;
        }
        else
        {
            transform.GetComponent<Renderer>().enabled = false;
            transform.FindChild("Particle System").GetComponent<ParticleSystem>().enableEmission = false;
        }

        if(taken)
        {
            Destroy(gameObject);
        }
    }

    public void setRoom(int currentRoom)
    {
        room = currentRoom;
    }

    public void setTaken(bool take)
    {
        taken = take;
    }
}
