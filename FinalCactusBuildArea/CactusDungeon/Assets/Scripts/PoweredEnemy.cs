﻿using UnityEngine;
using System.Collections;
using System;

public class PoweredEnemy : MonoBehaviour
{
    public int health;

    int mPosX, mPosZ;
    bool north, south, east, west;
    int currentLevel;
    int[,] levelArray;
    GameObject[] level = new GameObject[5];
    public Vector3 northPos = new Vector3(7, 5, -7);
    public Vector3 southPos = new Vector3(7, 5, 25);
    public Vector3 eastPos = new Vector3(25, 5, 7);
    public Vector3 westPos = new Vector3(-7, 5, 7);
    public Vector3 speed = new Vector3(2, 0, 2);

    //1 is every other turn
    private int moveCount = 0;
    public int damage = 7;
    private int worth = 30;

    private float moveDelay = 0f;

    public int room;

    public GameObject chunk;
    public GameObject water;
    public GameObject spores;

    private Vector3 destination;

    public BoardCreator board;

    System.Random rnd;

    //Audio
    AudioSource EnemyAttack;
    AudioSource powerUp;

    private bool isCoroutineExecuting = false;

    private float attackDelay = 0.1f;

    public void setBoard(BoardCreator b)
    {
        board = b;
    }

    // Use this for initialization
    void Start()
    {
        health = 7;
        north = true;
        south = false;
        east = false;
        west = false;
        currentLevel = 0;
        rnd = new System.Random(GetInstanceID());
        destination = transform.position;

        AudioSource[] audios = GetComponents<AudioSource>();
        EnemyAttack = audios[0];
        powerUp = audios[1];


        

    }

    public int getRoom()
    {
        return room;
    }

    void Update()
    {
        if (health <= 0)
        {

            //powerUp.Play();
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().eatEnemy();
            GameObject temp = Instantiate(water, transform.position, water.transform.rotation) as GameObject;
            Destroy(temp, 1);

            Destroy(gameObject);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().replenishWater(worth);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().transform.FindChild("Joe").FindChild("jnt_origin").FindChild("jnt_body").FindChild("jnt_pot").FindChild("spine01").FindChild("jnt_torso").FindChild("jnt_head").FindChild("Flower_Mesh").GetComponent<Renderer>().enabled = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().setFlowerOn(true);

        }

        transform.position = Vector3.Lerp(transform.position, destination, 5f * Time.deltaTime);

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom() != room)
        {
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").GetComponent<Renderer>().enabled = false;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("Enemy_Mouth").GetComponent<Renderer>().enabled = false;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("LeftEye").GetComponent<Renderer>().enabled = false;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("LeftEye1").GetComponent<Renderer>().enabled = false;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("Flower").GetComponent<Renderer>().enabled = false;
        }
        else
        {
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").GetComponent<Renderer>().enabled = true;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("Enemy_Mouth").GetComponent<Renderer>().enabled = true;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("LeftEye").GetComponent<Renderer>().enabled = true;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").FindChild("LeftEye1").GetComponent<Renderer>().enabled = true;
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("Flower").GetComponent<Renderer>().enabled = true;
        }
    }

    public void Hit(int dmg)
    {
        health -= dmg;
        StartCoroutine(Flasher());

        GameObject temp = Instantiate(chunk, transform.position, chunk.transform.rotation) as GameObject;
        Destroy(temp, 2);
    }

    IEnumerator Flasher()
    {
        Color normalColor = transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color;
        Color hitColor = Color.red;

        for (int i = 0; i < 2; i++)
        {
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color = hitColor;
            yield return new WaitForSeconds(.02f);
            transform.FindChild("Power_Enemy_mesh").FindChild("Power_Enemey").FindChild("CactusEnemy_Body").GetComponent<Renderer>().material.color = normalColor;
            yield return new WaitForSeconds(.06f);
        }
    }

    public void setRoom(int index)
    {
        room = index;
    }

    public void setLevel(int[,] level)
    {
        levelArray = level;
    }


    public void setPos(Vector3 vec)
    {
        mPosX = (int)vec.x;
        mPosZ = (int)vec.z;
    }

    IEnumerator Wait(bool a, bool b, bool c, bool d)
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;
        Transform target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().transform;

        yield return new WaitForSeconds(0.15f);
        if (Vector3.Distance(transform.position, target.position) < 7 && GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getRoom() == room)
            Move(a, b, c, d);

        isCoroutineExecuting = false;
    }


    IEnumerator WaitMove(float time, Vector3 pos)
    {

        yield return new WaitForSeconds(time);

        destination = pos;
        //transform.position = pos;

    }

    IEnumerator WaitAttack(float time, Collider target)
    {
        EnemyAttack.Play();
        board.GetComponent<BoardCreator>().addTime(1f);

        yield return new WaitForSeconds(0.5f);

        GameObject temp = Instantiate(spores, transform.position, spores.transform.rotation) as GameObject;
        temp.transform.LookAt(target.transform.FindChild("Cube"));
        Destroy(temp, 2);

        target.GetComponent<Player>().Hit(damage);
        
    }


    // Update is called once per frame
    public void TakeTurn()
    {
        //GetComponent<Animator>().SetTrigger("Attack");

        transform.position = destination;

        bool forward, backward, right, left;
        forward = false;
        backward = false;
        right = false;
        left = false;



        int xDir = 0;
        int yDir = 0;

        Transform target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().transform;

        if (Mathf.Abs(target.position.x - destination.x) < float.Epsilon)
            yDir = target.position.z > destination.z ? 1 : -1;
        else
            xDir = target.position.x > destination.x ? 1 : -1;

        switch (xDir)
        {
            case -1:
                right = true;
                break;
            case 1:
                left = true;
                break;
        }

        switch (yDir)
        {
            case -1:
                backward = true;
                break;
            case 1:
                forward = true;
                break;
        }


        if (mPosX > -1 && mPosX < 14 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(7, 5, -7);
            southPos = new Vector3(7, 5, 20);
            eastPos = new Vector3(20, 5, 7);
            westPos = new Vector3(-7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > -1 && mPosZ < 14)
        {
            northPos = new Vector3(21, 5, -7);
            southPos = new Vector3(21, 5, 20);
            eastPos = new Vector3(34, 5, 7);
            westPos = new Vector3(7, 5, 7);
        }
        else if (mPosX > 13 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(21, 5, 7);
            southPos = new Vector3(21, 5, 34);
            eastPos = new Vector3(34, 5, 21);
            westPos = new Vector3(7, 5, 21);
        }
        else if (mPosX > -1 && mPosX < 28 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(7, 5, 7);
            southPos = new Vector3(7, 5, 34);
            eastPos = new Vector3(20, 5, 21);
            westPos = new Vector3(-7, 5, 21);
        }
        else if (mPosX > 27 && mPosX < 41 && mPosZ > 13 && mPosZ < 28)
        {
            northPos = new Vector3(34, 5, 7);
            southPos = new Vector3(34, 5, 34);
            eastPos = new Vector3(49, 5, 21);
            westPos = new Vector3(21, 5, 21);
        }

        if ((forward || backward || right || left) && gameObject.activeSelf)
        {
            if (moveCount == 0)
            {
                StartCoroutine(Wait(forward, backward, right, left));
                moveCount = 0;
            }
            else
            {
                moveCount--;
            }
        }

    }

    public void SetStartLocation(int x, int y)
    {
        mPosX = x;
        mPosZ = y;
    }



    void Move(bool forward, bool backward, bool left, bool right)
    {
        int layerMask = 1 << 9;
        Vector3 start = transform.position;
        if (north)
        {


            if (forward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {                            
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ += 1;
                    }

                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));
                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                       // transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
        }
        if (south)
        {
            if (forward)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
        }
        if (west)
        {
            if (forward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
        }
        if (east)
        {
            if (forward)
            {
                Vector3 move = new Vector3(-1, 0, 0);
                if (levelArray[mPosX - 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(1, 0, 0));

                }
            }
            else if (backward)
            {
                Vector3 move = new Vector3(1, 0, 0);
                if (levelArray[mPosX + 1, mPosZ] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosX += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(-1, 0, 0));

                }
            }
            else if (left)
            {
                Vector3 move = new Vector3(0, 0, -1);
                if (levelArray[mPosX, mPosZ - 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ -= 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, 1));

                }
            }
            else if (right)
            {
                Vector3 move = new Vector3(0, 0, 1);
                if (levelArray[mPosX, mPosZ + 1] == 0)
                {
                    var hitColliders = Physics.OverlapSphere(start + move, 0.1f, layerMask);
                    if (hitColliders.Length > 0)
                    {
                        if (hitColliders[0].CompareTag("Player"))
                        {
                            GetComponent<Animator>().SetTrigger("Attack");
                            StartCoroutine(WaitAttack(attackDelay, hitColliders[0]));
                        }
                    }
                    else
                    {
                        GetComponent<Animator>().SetTrigger("Move");
                        StartCoroutine(WaitMove(moveDelay, start + move));
                        //transform.position = start + move;
                        mPosZ += 1;
                    }
                    transform.LookAt(transform.localPosition + new Vector3(0, 0, -1));

                }
            }
        }
    }
}
