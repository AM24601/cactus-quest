﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;

public class BoardCreator : MonoBehaviour
{
    //static string templateFolder = "Assets/RoomTemplates/";

    //static DirectoryInfo info = new DirectoryInfo(templateFolder);
    //FileInfo[] fileInfo = info.GetFiles();
    string[] fileList;

    public GameObject room;
    public GameObject player;
    public GameObject enemy1;
    public GameObject room1;
    public GameObject room2;
    public GameObject room3;
    public GameObject room4;
    public GameObject room5;
    public GameObject room6;
    public GameObject room7;
    public GameObject room8;
    public GameObject room9;
    public GameObject room10;

    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;
    public GameObject enemy5;
    public GameObject enemy6;
    public GameObject enemy7;
    public GameObject enemy8;
    public GameObject enemy9;
    public GameObject enemy10;
    public GameObject enemy11;
    public GameObject enemy12;
    public GameObject enemy13;
    public GameObject enemy14;
    public GameObject enemy15;
    public GameObject enemy16;

    public GameObject pot;

    public int[,] tileValues;
    public int[,] levelTiles;
    private Vector2[] positions = { new Vector2(0.0f, 0.0f),
                                    new Vector2(14.0f,0.0f),
                                    new Vector2(14.0f, 14.0f),
                                    new Vector2(0.0f, 14.0f),
                                    new Vector2(28.0f, 14.0f),
                                    new Vector2(28.0f, 0.0f),
                                    new Vector2(42.0f, 14.0f),
                                    new Vector2(42.0f, 0.0f),
                                    new Vector2(56.0f, 14.0f),
                                    new Vector2(56.0f, 0.0f) };
    public int maxLevelDemensionX = 100;
    public int maxLevelDemensionY = 28;
    public int maxRoomDemension = 14;

    public bool playerTurn = true;
    public bool playerCourtesy = true;
    private bool isCoroutineExecuting = false;
    public Text waterText;
    public Text deathText;

    private float courtesyCounter = 0f;

    static int numRooms = 10;

    GameObject[] level = new GameObject[numRooms];

	// Use this for initialization
	void Start ()
    {
        string templateFolder = Application.dataPath + "/RoomTemplates/";
        DirectoryInfo info = new DirectoryInfo(templateFolder);
        FileInfo[] fileInfo = info.GetFiles();

        MakeTemplateList(fileInfo);
        LoadData(fileList);
        CreateLevelArray();
        BuildRooms();
        SetPlayer();
        SetEnemies();
        SetItems();
	}

    public void checkActiveRoom()
    {
        int playerRoom = player.GetComponent<Player>().getRoom();

        switch (playerRoom)
        {
            case 1:
                room1.SetActive(true);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 2:
                room1.SetActive(false);
                room2.SetActive(true);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 3:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(true);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 4:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(true);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 5:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(true);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 6:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(true);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 7:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(true);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 8:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(true);
                room9.SetActive(false);
                room10.SetActive(false);
                break;
            case 9:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(true);
                room10.SetActive(false);
                break;
            case 10:
                room1.SetActive(false);
                room2.SetActive(false);
                room3.SetActive(false);
                room4.SetActive(false);
                room5.SetActive(false);
                room6.SetActive(false);
                room7.SetActive(false);
                room8.SetActive(false);
                room9.SetActive(false);
                room10.SetActive(true);
                break;
        }

    }

    void Update()
    {       
        /*if(player.GetComponent<Player>().room == 4)
        {
            transform.FindChild("Pot_Low").GetComponent<Renderer>().enabled = true;
        }
        else
        {
            transform.FindChild("Pot_Low").GetComponent<Renderer>().enabled = false;
        }*/
        if (!playerTurn)
        {

            if (enemy1)
                enemy1.GetComponent<Enemy>().TakeTurn();

            if (enemy2)
                enemy2.GetComponent<PoweredEnemy>().TakeTurn();

            if (enemy3)
                enemy3.GetComponent<Enemy>().TakeTurn();

            if (enemy4)
                enemy4.GetComponent<Enemy>().TakeTurn();

            if (enemy5)
                enemy5.GetComponent<Enemy>().TakeTurn();

            if (enemy6)
                enemy6.GetComponent<Enemy>().TakeTurn();

            if (enemy7)
                enemy7.GetComponent<Enemy>().TakeTurn();

            if (enemy8)
                enemy8.GetComponent<Enemy>().TakeTurn();

            if (enemy9)
                enemy9.GetComponent<Enemy>().TakeTurn();

            if (enemy10)
                enemy10.GetComponent<Enemy>().TakeTurn();

            if (enemy11)
                enemy11.GetComponent<Enemy>().TakeTurn();

            if (enemy12)
                enemy12.GetComponent<Enemy>().TakeTurn();

            if (enemy13)
                enemy13.GetComponent<Enemy>().TakeTurn();

            if (enemy14)
                enemy14.GetComponent<Enemy>().TakeTurn();

            if (enemy15)
                enemy15.GetComponent<Enemy>().TakeTurn();

            if (enemy16)
                enemy16.GetComponent<Enemy>().TakeTurn();

            playerCourtesy = false;
            //StartCoroutine(Wait(courtesyCounter));
            //playerCourtesy = true;

            /*
            if (enemy1.GetComponent<Enemy>().turnOver() && enemy3.GetComponent<Enemy>().turnOver() && enemy4.GetComponent<Enemy>().turnOver() && enemy5.GetComponent<Enemy>().turnOver() && enemy6.GetComponent<Enemy>().turnOver() && enemy7.GetComponent<Enemy>().turnOver() && enemy8.GetComponent<Enemy>().turnOver() && enemy9.GetComponent<Enemy>().turnOver() && enemy10.GetComponent<Enemy>().turnOver() && enemy11.GetComponent<Enemy>().turnOver() && enemy12.GetComponent<Enemy>().turnOver() && enemy13.GetComponent<Enemy>().turnOver() && enemy14.GetComponent<Enemy>().turnOver() && enemy15.GetComponent<Enemy>().turnOver() && enemy16.GetComponent<Enemy>().turnOver() && playerTurn == false)
            {
                StartCoroutine(Wait(1.633f * 1.5f));
                toggleTurn();
            }
            else
            {
                toggleTurn();
            }
            */

            toggleTurn();
            if (courtesyCounter > 0)
                StartCoroutine(Wait(0.5f));
            else
                playerCourtesy = true;


        }

    }

    public void addTime(float t)
    {
        courtesyCounter = courtesyCounter + t;
    }


    IEnumerator Wait(float t)
    {

        yield return new WaitForSeconds(0);

        playerCourtesy = true;
        courtesyCounter = 0;
    }



    public void SetEnemies()
    {
        //enemy 1
        Vector3 ePos = new Vector3(35f, 1f, 3f);
        enemy1 = Instantiate(enemy1, ePos, Quaternion.identity) as GameObject;
        enemy1.GetComponent<Enemy>().setLevel(levelTiles);
        enemy1.GetComponent<Enemy>().setPos(ePos);
        enemy1.GetComponent<Enemy>().setRoom(6);
        enemy1.GetComponent<Enemy>().setBoard(this);

        //enemy 2
        Vector3 ePos2 = new Vector3(36f, 1f, 25f);
        enemy2 = Instantiate(enemy2, ePos2, Quaternion.identity) as GameObject;
        enemy2.GetComponent<PoweredEnemy>().setLevel(levelTiles);
        enemy2.GetComponent<PoweredEnemy>().setPos(ePos2);
        enemy2.GetComponent<PoweredEnemy>().setRoom(5);
        enemy2.GetComponent<PoweredEnemy>().setBoard(this);

        //enemy 3
        Vector3 ePos3 = new Vector3(21f, 1f, 8f);
        enemy3 = Instantiate(enemy3, ePos3, Quaternion.identity) as GameObject;
        enemy3.GetComponent<Enemy>().setLevel(levelTiles);
        enemy3.GetComponent<Enemy>().setPos(ePos3);
        enemy3.GetComponent<Enemy>().setRoom(2);
        enemy3.GetComponent<Enemy>().setBoard(this);

        //enemy 4
        Vector3 ePos4 = new Vector3(25f, 1f, 22f);
        enemy4 = Instantiate(enemy4, ePos4, Quaternion.identity) as GameObject;
        enemy4.GetComponent<Enemy>().setLevel(levelTiles);
        enemy4.GetComponent<Enemy>().setPos(ePos4);
        enemy4.GetComponent<Enemy>().setRoom(3);
        enemy4.GetComponent<Enemy>().setBoard(this);

        //enemy 5
        Vector3 ePos5 = new Vector3(15f, 1f, 23f);
        enemy5 = Instantiate(enemy5, ePos5, Quaternion.identity) as GameObject;
        enemy5.GetComponent<Enemy>().setLevel(levelTiles);
        enemy5.GetComponent<Enemy>().setPos(ePos5);
        enemy5.GetComponent<Enemy>().setRoom(3);
        enemy5.GetComponent<Enemy>().setBoard(this);

        //enemy 6
        Vector3 ePos6 = new Vector3(30f, 1f, 12f);
        enemy6 = Instantiate(enemy6, ePos6, Quaternion.identity) as GameObject;
        enemy6.GetComponent<Enemy>().setLevel(levelTiles);
        enemy6.GetComponent<Enemy>().setPos(ePos6);
        enemy6.GetComponent<Enemy>().setRoom(6);
        enemy6.GetComponent<Enemy>().setBoard(this);

        //enemy 7
        Vector3 ePos7 = new Vector3(44f, 1f, 26f);
        enemy7 = Instantiate(enemy7, ePos7, Quaternion.identity) as GameObject;
        enemy7.GetComponent<Enemy>().setLevel(levelTiles);
        enemy7.GetComponent<Enemy>().setPos(ePos7);
        enemy7.GetComponent<Enemy>().setRoom(7);
        enemy7.GetComponent<Enemy>().setBoard(this);

        //enemy 8
        Vector3 ePos8 = new Vector3(51f, 1f, 26f);
        enemy8 = Instantiate(enemy8, ePos8, Quaternion.identity) as GameObject;
        enemy8.GetComponent<Enemy>().setLevel(levelTiles);
        enemy8.GetComponent<Enemy>().setPos(ePos8);
        enemy8.GetComponent<Enemy>().setRoom(7);
        enemy8.GetComponent<Enemy>().setBoard(this);

        //enemy 9
        Vector3 ePos9 = new Vector3(51f, 1f, 12f);
        enemy9 = Instantiate(enemy9, ePos9, Quaternion.identity) as GameObject;
        enemy9.GetComponent<Enemy>().setLevel(levelTiles);
        enemy9.GetComponent<Enemy>().setPos(ePos9);
        enemy9.GetComponent<Enemy>().setRoom(8);
        enemy9.GetComponent<Enemy>().setBoard(this);

        //enemy 10
        Vector3 ePos10 = new Vector3(8f, 1f, 26f);
        enemy10 = Instantiate(enemy10, ePos10, Quaternion.identity) as GameObject;
        enemy10.GetComponent<Enemy>().setLevel(levelTiles);
        enemy10.GetComponent<Enemy>().setPos(ePos10);
        enemy10.GetComponent<Enemy>().setRoom(4);
        enemy10.GetComponent<Enemy>().setBoard(this);

        //enemy 11
        Vector3 ePos11 = new Vector3(64f, 1f, 24f);
        enemy11 = Instantiate(enemy11, ePos11, Quaternion.identity) as GameObject;
        enemy11.GetComponent<Enemy>().setLevel(levelTiles);
        enemy11.GetComponent<Enemy>().setPos(ePos11);
        enemy11.GetComponent<Enemy>().setRoom(9);
        enemy11.GetComponent<Enemy>().setBoard(this);

        //enemy 12
        Vector3 ePos12 = new Vector3(65f, 1f, 17f);
        enemy12 = Instantiate(enemy12, ePos12, Quaternion.identity) as GameObject;
        enemy12.GetComponent<Enemy>().setLevel(levelTiles);
        enemy12.GetComponent<Enemy>().setPos(ePos12);
        enemy12.GetComponent<Enemy>().setRoom(9);
        enemy12.GetComponent<Enemy>().setBoard(this);

        //enemy 13
        Vector3 ePos13 = new Vector3(58f, 1f, 18f);
        enemy13 = Instantiate(enemy13, ePos13, Quaternion.identity) as GameObject;
        enemy13.GetComponent<Enemy>().setLevel(levelTiles);
        enemy13.GetComponent<Enemy>().setPos(ePos13);
        enemy13.GetComponent<Enemy>().setRoom(9);
        enemy13.GetComponent<Enemy>().setBoard(this);

        //enemy 14
        Vector3 ePos14 = new Vector3(64f, 1f, 7f);
        enemy14 = Instantiate(enemy14, ePos14, Quaternion.identity) as GameObject;
        enemy14.GetComponent<Enemy>().setLevel(levelTiles);
        enemy14.GetComponent<Enemy>().setPos(ePos14);
        enemy14.GetComponent<Enemy>().setRoom(10);
        enemy14.GetComponent<Enemy>().setBoard(this);

        //enemy 15
        Vector3 ePos15 = new Vector3(65f, 1f, 2f);
        enemy15 = Instantiate(enemy15, ePos15, Quaternion.identity) as GameObject;
        enemy15.GetComponent<Enemy>().setLevel(levelTiles);
        enemy15.GetComponent<Enemy>().setPos(ePos15);
        enemy15.GetComponent<Enemy>().setRoom(10);
        enemy15.GetComponent<Enemy>().setBoard(this);

        //enemy 16
        Vector3 ePos16 = new Vector3(58f, 1f, 3f);
        enemy16 = Instantiate(enemy16, ePos16, Quaternion.identity) as GameObject;
        enemy16.GetComponent<Enemy>().setLevel(levelTiles);
        enemy16.GetComponent<Enemy>().setPos(ePos16);
        enemy16.GetComponent<Enemy>().setRoom(10);
        enemy16.GetComponent<Enemy>().setBoard(this);
    }

    public void SetItems()
    {
        //pot
        Vector3 potPos = new Vector3(2f, 0.5f, 25f);
        pot = Instantiate(pot, potPos, Quaternion.identity) as GameObject;
        pot.GetComponent<Item>().setRoom(4);
    }

    public bool getPlayerTurn()
    {
        return playerTurn;
    }

    public bool getPlayerCourtesy()
    {
        return playerCourtesy;
    }

    public void toggleTurn()
    {
        playerTurn = !playerTurn;
    }



    void SetPlayer()
    {
        player = Instantiate(player, new Vector3(7f, 1f, 7f), Quaternion.identity) as GameObject;
        player.GetComponent<Player>().setLevel(levelTiles);
        player.GetComponent<Player>().setBoard(this);
        player.GetComponent<Player>().setText(waterText);
    }

    void BuildRooms()
    {
        for (int i = 0; i < numRooms; i++)
        {
            //level[i].GetComponent<Room>().CreateTiles();
            switch (i)
            {
                case 0:
                    room1 = Instantiate(room1, new Vector3(positions[0].x, 0.25f, positions[0].y), Quaternion.identity) as GameObject;
                    level[0].GetComponent<Room>().setPreFab(room1);
                    break;
                case 1:
                    room2 = Instantiate(room2, new Vector3(positions[1].x, 0.25f, positions[1].y), Quaternion.identity) as GameObject;
                    level[1].GetComponent<Room>().setPreFab(room2);
                    break;
                case 2:
                    room3 = Instantiate(room3, new Vector3(positions[2].x, 0.25f, positions[2].y), Quaternion.identity) as GameObject;
                    level[2].GetComponent<Room>().setPreFab(room3);
                    break;
                case 3:
                    room4 = Instantiate(room4, new Vector3(positions[3].x, 0.25f, positions[3].y), Quaternion.identity) as GameObject;
                    level[3].GetComponent<Room>().setPreFab(room4);
                    break;
                case 4:
                    room5 = Instantiate(room5, new Vector3(positions[4].x, 0.25f, positions[4].y), Quaternion.identity) as GameObject;
                    level[4].GetComponent<Room>().setPreFab(room5);
                    break;
                case 5:
                    room6 = Instantiate(room6, new Vector3(positions[5].x, 0.25f, positions[5].y), Quaternion.identity) as GameObject;
                    level[5].GetComponent<Room>().setPreFab(room6);
                    break;
                case 6:
                    room7 = Instantiate(room7, new Vector3(positions[6].x, 0.25f, positions[6].y), Quaternion.identity) as GameObject;
                    level[6].GetComponent<Room>().setPreFab(room7);
                    break;
                case 7:
                    room8 = Instantiate(room8, new Vector3(positions[7].x, 0.25f, positions[7].y), Quaternion.identity) as GameObject;
                    level[7].GetComponent<Room>().setPreFab(room8);
                    break;
                case 8:
                    room9 = Instantiate(room9, new Vector3(positions[8].x, 0.25f, positions[8].y), Quaternion.identity) as GameObject;
                    level[8].GetComponent<Room>().setPreFab(room9);
                    break;
                case 9:
                    room10 = Instantiate(room10, new Vector3(positions[9].x, 0.25f, positions[9].y), Quaternion.identity) as GameObject;
                    level[9].GetComponent<Room>().setPreFab(room10);
                    break;
            }

        }
    }

    private void MakeTemplateList(FileInfo[] fileInfo)
    {
        fileList = new string[numRooms];
        int count = 0;
        for (int i = 0; count < numRooms; i++)
        {
            if (i % 2 == 0)
            {
                fileList[count] = fileInfo[i].ToString();
                count++;
            }

        }
    }

    private bool LoadData(string[] fileList)
    {

        for (int i = 0; i < numRooms; i++)
        {

            StreamReader inputStream = new StreamReader(fileList[i]);

            //reads in the first two lines which should be the dimensions of the grid
            int width = Int32.Parse(inputStream.ReadLine());
            int height = Int32.Parse(inputStream.ReadLine());

            //set tileValue array to the newly found dimensions
            tileValues = new int[width, height];
            //reads in the actual grid
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    char character = (char)inputStream.Read();
                    int temp = Int32.Parse(character.ToString());
                    if (temp > -1 && temp < 3)
                        tileValues[y, x] = temp;
                }

                int skip = inputStream.Read();
                int skip2 = inputStream.Read();
            }
            inputStream.Close();

            //level[i] = Instantiate(room, new Vector3((i*14.0f), 1f, 1f), Quaternion.identity) as GameObject;
            level[i] = Instantiate(room, new Vector3((positions[i].x), positions[i].y, 1f), Quaternion.identity) as GameObject;
            level[i].GetComponent<Room>().setValues(width, height, tileValues);

        }


        return true;
    }

    void CreateLevelArray()
    {
        levelTiles = new int[maxLevelDemensionX, maxLevelDemensionY];
        for (int x = 0; x < maxLevelDemensionX; x++)
        {
            for (int y = 0; y < maxLevelDemensionY; y++)
            {
                if(x > -1 && x < maxRoomDemension && y > -1 && y < maxRoomDemension)
                {
                    levelTiles[x, y] = level[0].GetComponent<Room>().tileValues[x, y];
                }
                else if(x > (maxRoomDemension - 1) && x < (maxRoomDemension * 2) && y > -1 && y < maxRoomDemension)
                {
                    int tmpX = x - maxRoomDemension;
                    levelTiles[x, y] = level[1].GetComponent<Room>().tileValues[tmpX, y];
                }
                else if (x > (maxRoomDemension - 1) && x < (maxRoomDemension * 2) && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpX = x - maxRoomDemension;
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[2].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else if (x > -1 && x < (maxRoomDemension * 2) && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[3].GetComponent<Room>().tileValues[x, tmpY];
                }
                else if (x > 27 && x < 42 && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpX = x - (maxRoomDemension *2);
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[4].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else if (x > 27 && x < 42 && y > - 1 && y < maxRoomDemension)
                {
                    int tmpX = x - (maxRoomDemension * 2);
                    //int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[5].GetComponent<Room>().tileValues[tmpX, y];
                }
                else if (x > 41 && x < 56 && y > (maxRoomDemension - 1) && y < (maxRoomDemension * 2))
                {
                    int tmpX = x - (maxRoomDemension * 3);
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[6].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else if (x > 41 && x < 56 && y > -1 && y < maxRoomDemension)
                {
                    int tmpX = x - (maxRoomDemension * 3);
                    //int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[7].GetComponent<Room>().tileValues[tmpX, y];
                }
                else if (x > 55 && x < 70 && y > 13 && y < 28)
                {
                    int tmpX = x - (maxRoomDemension * 4);
                    int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[8].GetComponent<Room>().tileValues[tmpX, tmpY];
                }
                else if (x > 55 && x < 70 && y > -1 && y < maxRoomDemension)
                {
                    int tmpX = x - (maxRoomDemension * 4);
                    //int tmpY = y - maxRoomDemension;
                    levelTiles[x, y] = level[9].GetComponent<Room>().tileValues[tmpX, y];
                }
                else
                {
                    levelTiles[x, y] = 1;
                }
            }
        }
    }

}
